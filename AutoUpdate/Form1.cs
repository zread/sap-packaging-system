﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Diagnostics;

namespace AutoUpdate
{
    public partial class Form1 : Form
    {
        public Form1()
        {

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            //1：确保包装系统的进程已经杀掉
            System.Threading.Thread.Sleep(1000);
            AutoUpdateHelper.KillProcess("CSICPR");

            //2:从数据库获取程序发布的路径，用户名，密码
            #region 从数据库获取程序发布的路径，用户名，密码
            List<string> lst = new List<string>();
            lst = AutoUpdateHelper.GetAutoUpdateInfo();

            if (lst.Count != 3)
            {
                MessageBox.Show("自动更新配置不正确，但不影响系统使用。请联系系统管理员！", "包装系统", MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblUpdate.Text = "更新出现异常，请联系管理员！";
                return;
            }

            string sharedPath = lst[0];
            string UserName = lst[1];
            string Password = lst[2];
            string SharedCSICPRFile = sharedPath + @"\CSICPR.exe";

            if (string.IsNullOrEmpty(sharedPath) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Password))
            {
                MessageBox.Show("自动更新配置不正确，但不影响系统使用。请联系系统管理员！", "包装系统", MessageBoxButtons.OK, MessageBoxIcon.Information);
                lblUpdate.Text = "更新出现异常，请联系管理员！";
                return;
            }
            #endregion

            //3：检查远程访问权限，如果没权限访问共享路径，则用net use赋权限
            #region 必要时用net use赋权限
            if (!File.Exists(SharedCSICPRFile))
            {
                try
                {
                    AutoUpdateHelper.Delete(sharedPath);
                    AutoUpdateHelper.Connect(sharedPath, UserName, Password);
                }
                catch (Exception ex)
                {
                    //AutoUpdateHelper.Delete(sharedPath);
                    MessageBox.Show(ex.Message, "包装系统", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lblUpdate.Text = "更新出现异常，请联系管理员！";
                    return;
                }
            }
            #endregion
            
            //4：检查远程共享中是否有UpdateList.xml文件
            if (!File.Exists(sharedPath + @"\UpdateList.xml"))
            {
                MessageBox.Show("在共享路径下没有找到UpdateList.xml文件，请联系系统管理员！", "自动更新", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //AutoUpdateHelper.Delete(sharedPath);
                lblUpdate.Text = "更新出现异常，请联系管理员！";
                return;
            }

            //5：自动更新
            if (UpdateFiles(sharedPath))
            {
                lblUpdate.Text = "更新成功，点击OK启用*新*版本！" + "\r\n" + "点击Cancel退出更新！";
                lblUpdate.ForeColor = Color.Green;
            }
            else
            {
                lblUpdate.Text = "更新失败，点击OK启用*旧*版本！" + "\r\n" +"点击Cancel退出更新！"+ "\r\n" + "或联系IT，分机66400！";
                lblUpdate.ForeColor = Color.Red;
            }
                        
        }

        
        /// <summary>
        ///  自动更新，通过覆盖主要文件的方式
        /// </summary>
        /// <param name="sharedPath"></param>
        /// <returns></returns>
        private bool UpdateFiles(string sharedPath)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(sharedPath + @"\UpdateList.xml");

            XmlElement root = doc.DocumentElement;
            XmlNode updateNode = root.SelectSingleNode("filelist");
            int count = int.Parse(updateNode.Attributes["count"].Value);
            
            List<string> backuped = new List<string>(); //记录已经更新的文件，方便发生错误时回滚
            try
            {
                for (int i = 0; i < count; i++)
                {
                    lblUpdate.Text = "更新第" + i.ToString() + "/" + count.ToString() + "个文件...";

                    XmlNode itemNode = updateNode.ChildNodes[i];

                    string fileName = itemNode.Attributes["name"].Value;
                    string localLocation = Application.StartupPath + @"\" + fileName;
                    string localLocationBak = Application.StartupPath + @"\" + fileName + ".bak";
                    string sharedLocation = sharedPath + @"\" + fileName;

                    if (File.Exists(localLocation))
                    {
                        File.Copy(localLocation, localLocationBak, true);
                        backuped.Add(fileName);
                        File.Copy(sharedLocation, localLocation, true);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "自动更新", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (backuped.Count > 0)
                {
                    foreach (string s in backuped)
                    {
                        File.Copy(Application.StartupPath + @"\" + s + ".bak", Application.StartupPath + @"\" + s, true);
                    }
                }

                return false;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("CSICPR.exe");
            Close();
            Application.Exit();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
            Application.Exit();
        }

    }
}
