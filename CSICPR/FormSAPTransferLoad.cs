﻿using System;
using System.ComponentModel;
//using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Collections.Generic;

namespace CSICPR
{
    public partial class FormSAPTransferLoad : Form
    {
        private static List<LanguageItemModel> LanMessList;//定义语言集
        #region 共有变量
        private static FormSAPTransferLoad theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormSAPTransferLoad();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        //物料编码
        private string Mcode = "";
        //项目号
        private string Mitem = "";
        //年份
        private string Year = "";

        public FormSAPTransferLoad()
        {
            InitializeComponent();
            txtMitem.Text = "001";
            txtMitem.Visible = false;
            label4.Visible = false;
        }

        private void FormSAPMASTERLoad_Load(object sender, EventArgs e)
        {
            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion
            this.lstView.Columns.Add(LanguageHelper.GetMessage(LanMessList, "Message1", "提示信息"), 1000, HorizontalAlignment.Left);
        }

        /// <summary>
        /// 提供数据复制功能
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (lstView.SelectedItems.Count > 0)
                {
                    if (lstView.SelectedItems[0].Text != "")
                        Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            #region
            if (this.txtMcode.Text.Trim().Equals(""))
            {
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message2", "物料编码不能为空，请输入！"), "ABNORMAL", lstView);
                this.txtMcode.Focus();
                return;
            }
            else
                Mcode = this.txtMcode.Text.Trim();

            if (this.txtMitem.Text.Trim().Equals(""))
            {
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message3", "行项目不能为空，请输入!"), "ABNORMAL", lstView);
                this.txtMitem.Focus();
                return;
            }
            else
                Mitem = this.txtMitem.Text.Trim();

            if (this.txtYear.Text.Trim().Equals(""))
            {
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message4", "年份不能为空，请输入!"), "ABNORMAL", lstView);
                this.txtYear.Focus();
                return;
            }
            else
            {
                if (this.txtYear.Text.Trim().Length != 4)
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message5", "年份长度不对，必须是四位数字，例如 2012，请确认！"), "ABNORMAL", lstView);
                    this.txtYear.Focus();
                    return;
                }
                else
                    Year = this.txtYear.Text.Trim();
            }
            #endregion

            string msg = "";
            try
            {
                SapStorageService.SapMesInterfaceClient service = new SapStorageService.SapMesInterfaceClient();
                service.Endpoint.Address = new System.ServiceModel.EndpointAddress(FormCover.WebServiceAddress);
                msg = service.toStorageDataSaptoMes("", Mcode,Year,Mitem);
                if (msg.Equals("0"))
                {
                    ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message6", "物料编码:{0};行项目:{1};年份:{2};所对应的转储数据不存在！"), Mcode, Mitem, Year), "ABNORMAL", lstView);
                    this.txtMcode.SelectAll();
                }
                else
                {
                    ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message7","物料编码:{0};行项目:{1};年份:{2}所对应的转储数据下载成功！"), Mcode, Mitem, Year), "NORMAL", lstView);
                    this.txtMcode.Clear();
                    this.txtMitem.Clear();
                    this.txtYear.Clear();
                    this.txtMcode.Focus();
                }
            }
            catch (Exception ex)
            {
                ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message8", "物料编码:{0};行项目:{1};年份:{2}下载时发生了异常"), Mcode, Mitem, Year) + ex.Message, "ABNORMAL", lstView);
                this.txtMcode.SelectAll();
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            this.txtMcode.Clear();
            this.txtMitem.Clear();
            this.txtYear.Clear();
            if (lstView.Items.Count > 100)
                lstView.Items.Clear();
            this.txtMcode.Focus();
        }

    }

}