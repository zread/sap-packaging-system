﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSICPR
{
    public partial class FormReason : Form
    {
        private bool flag;

        public FormReason()
        {
            InitializeComponent();
            this.flag = false;
        }

        public bool exit
        {
            get { return flag; }
        }

        public string reason
        {
            get { return comboBox1.Text.Trim(); }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text.Trim().Length > 0)
            {
                string sql = @"SELECT SN,BoxID,CTMGrade FROM [Product] WITH(NOLOCK) 
                                   WHERE InterID = (SELECT MAX(InterID) FROM [Product] with(nolock) WHERE SN='{0}') ";

                sql = string.Format(sql, this.comboBox1.Text.Trim());

                DataSet dt = DbHelperSQL.Query(FormCover.connectionBase, sql);

                if (dt != null && dt.Tables[0].Rows.Count>0)
                {
                    DataRow row = dt.Tables[0].Rows[0];
                    if (!string.IsNullOrEmpty(row["BoxID"].ToString().Trim()))
                    {
                        MessageBox.Show(this.comboBox1.Text.Trim()+"：此组件已经打包过");
                        this.comboBox1.SelectAll();
                        this.comboBox1.Focus();
                        return;
                    }
                    if (row["CTMGrade"].ToString().Trim().Equals("NG"))
                    {
                        MessageBox.Show(this.comboBox1.Text.Trim() + "：的CTM判等NG，不能打包!");
                        this.comboBox1.SelectAll();
                        this.comboBox1.Focus();
                        return;
                    }
                    else if (row["CTMGrade"].ToString().Trim().Equals("W") || string.IsNullOrEmpty(row["CTMGrade"].ToString().Trim()))
                    {
                        MessageBox.Show(this.comboBox1.Text.Trim() + "： 的CTM没有判等，不能打包!");
                        this.comboBox1.SelectAll();
                        this.comboBox1.Focus();
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("输入的组件号不存在");
                    this.comboBox1.SelectAll();
                    this.comboBox1.Focus();
                    return;
                }

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
                MessageBox.Show("Incorrect Inuput Module Number", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning); ;
        }

        public void request(string question = "Please Input Unprohibited Reason!", string title = "why", string[] reazons = null)
        {
            this.Text = title;
            label1.Text = question;
            if (reazons != null && reazons.Length > 0)
            {
                foreach (string str in reazons)
                {
                    comboBox1.Items.Add(str);
                }
            }
            this.ShowDialog();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            this.flag = true;
        }

        private void FormReason_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            //if (comboBox1.Text.Trim().Equals(""))
            //    st = "";
            //comboBox1.Text = st;
        }

        private void comboBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar >= 'a' && e.KeyChar <= 'z')
            //{
            //    st = st + e.KeyChar.ToString().ToUpper();
            //}
        }
    }
}
