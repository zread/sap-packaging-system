﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Data.SqlClient;
using System.Xml;
using System.IO;

namespace CSICPR
{
    public partial class FormNormalStorageCarton : Form
    {
        private static List<LanguageItemModel> LanMessList;
        // private List<string> _ListCartonSucess = new List<string>();
        public FormNormalStorageCarton()
        {
            InitializeComponent();
        }

        private void ProductStorage_Load(object sender, EventArgs e)
        {
            //工单类型
            this.ddlWoType.Items.Insert(0, "");
            this.ddlWoType.Items.Insert(1, "转常规工单");
            this.ddlWoType.Items.Insert(2, "外协工单");
            this.ddlWoType.Items.Insert(3, "返工工单");
            this.ddlWoType.SelectedIndex = 0;

            //提示信息
            this.lstView.Columns.Add("提示信息", 630, HorizontalAlignment.Left);

            //界面Grid列头禁止排序
            for (int i = 0; i < this.dataGridView1.ColumnCount; i++)
            {
                this.dataGridView1.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            this.txtCarton.Focus();

            this.checkBox2.Checked = false;
            this.txtWO1.Enabled = false;
            this.pictureBox1.Enabled = false;

            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            LanguageHelper.GetCombomBox(this, this.ddlWoType);
            # endregion
        }

        #region 公有变量
        private static FormNormalStorageCarton theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormNormalStorageCarton();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        #region 私有变量
        /// <summary>
        /// 工单类型
        /// </summary>
        private string WoType = "";
        /// <summary>
        /// 工单状态
        /// </summary>
        private string WoStatus = "";
        /// <summary>
        /// 包装方式
        /// </summary>
        private string PackingPatternValue = "";
        /// <summary>
        /// 设置：记录选中的记录
        /// </summary>
        private Dictionary<int, string> dgvIndex = new Dictionary<int, string>();
        /// <summary>
        /// 设置：记录选中的托号码
        /// </summary>
        private List<string> CartonArrayS = new List<string>();
        /// <summary>
        /// 传Sap参数
        /// </summary>
        private List<SapWoModule> _ListSapWo = new List<SapWoModule>();
        /// <summary>
        /// 暂存修改过的组件电池片信息
        /// </summary>
        private List<CellSerialNumberTemp> _ListPackingSNCellInfo = new List<CellSerialNumberTemp>();
        /// <summary>
        /// 电流分档
        /// </summary>
        private string ByIm_flag = "";

        /// <summary>
        /// 撤销入库标识
        /// </summary>
        private string _CancelStorageFlag = "";
        /// <summary>
        /// 是否拼托
        /// </summary>
        private string _IsOnlyPacking = "";
        /// <summary>
        ///记录上传入库成功的做记录
        /// </summary>
        private List<SapWoModule> _ListSapWoLog = new List<SapWoModule>();
        #endregion

        #region 私有方法
        /// <summary>
        /// 初始化界面：工单信息维护
        /// </summary>
        private void ClearData(bool flag)
        {
            this.txtWo.Clear();
            this.txtFactory.Clear();
            this.txtMitemCode.Clear();
            this.txtlocation.Clear();
            this.txtPlanCode.Clear();
            this.txtSalesItemNo.Clear();
            this.txtSalesOrderNo.Clear();

            if (flag)
                this.ddlWoType.SelectedIndex = -1;

            this.checkBox2.Checked = false;
            this.txtWO1.Clear();
            this.txtWO1.Enabled = false;
            this.pictureBox1.Enabled = false;
        }

        #endregion

        #region 工单查询
        private void txtWoOrder_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ddlWoType.SelectedIndex == 0)
                {
                    ToolsClass.Log("Please select production order type!", "ABNORMAL", lstView);
                    this.ddlWoType.Focus();
                    this.ddlWoType.SelectAll();
                    this.txtWoOrder.Clear();
                    return;
                }

                if (string.IsNullOrEmpty(Convert.ToString(this.txtWoOrder.Text.Trim())))
                {
                    ToolsClass.Log("Production order cannot be empty, please enter!", "ABNORMAL", lstView);
                    this.txtWoOrder.Focus();
                    this.txtWoOrder.SelectAll();
                    return;
                }
                if (dgvIndex.Count > 0)
                    dgvIndex.Clear();
                SetWOData(true);
            }
        }
        private void txtWoOrder_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                if (this.ddlWoType.SelectedIndex == 0)
                {
                    ToolsClass.Log("Please select production order type", "ABNORMAL", lstView);
                    this.ddlWoType.Focus();
                    return;
                }
                PicBoxWO_Click(null, null);
            }

        }
        private void PicBoxWO_Click(object sender, EventArgs e)
        {
            if (this.ddlWoType.SelectedIndex == 0)
            {
                ToolsClass.Log("Please select production order type!", "ABNORMAL", lstView);
                this.ddlWoType.Focus();
                return;
            }

            var frm = new FormWOInfoQuery(this.txtWoOrder.Text.Trim().ToString(), WoType);
            frm.ShowDialog();
            this.txtWoOrder.Text = frm.Wo;
            if (frm != null)
                frm.Dispose();
            SetWOData(false);
        }
        private void SetWOData(bool flag)
        {
            ClearData(false);

            if (Convert.ToString(this.txtWoOrder.Text.Trim()).Equals(""))
            {

                ToolsClass.Log("Production oerder cannot be empty！", "ABNORMAL", lstView);
                this.txtWoOrder.SelectAll();
                this.txtWoOrder.Focus();
                return;
            }

            DataTable wo = ProductStorageDAL.GetWoMasterIno(Convert.ToString(this.txtWoOrder.Text.Trim()), WoType);
            if (wo != null && wo.Rows.Count > 0)
            {
                SetWoInfo(wo, WoType);
            }
            else
            {
                if (flag)
                {
                    ToolsClass.Log("Production order：" + Convert.ToString(this.txtWoOrder.Text.Trim()) + " was not download from SAP or wrong order type chosen!", "ABNORMAL", lstView);
                    this.txtWoOrder.SelectAll();
                    this.txtWoOrder.Focus();
                    return;
                }
                else
                {
                    ToolsClass.Log("Production order:" + Convert.ToString(this.txtWoOrder.Text.Trim()) + "does not have the corresponding material transfer!", "ABNORMAL", lstView);
                    this.txtWoOrder.SelectAll();
                    this.txtWoOrder.Focus();
                    return;
                }
            }
            this.txtWoOrder.Clear();
        }
        private void SetWoInfo(DataTable wo, string _Wotype)
        {
            DataRow row = wo.Rows[0];
            this.txtWo.Text = Convert.ToString(row["TWO_NO"]);
            this.txtFactory.Text = Convert.ToString(row["TWO_WORKSHOP"]);
            this.txtMitemCode.Text = Convert.ToString(row["TWO_PRODUCT_NAME"]);
            this.txtlocation.Text = Convert.ToString(row["RESV04"]);
            this.txtPlanCode.Text = Convert.ToString(row["RESV06"]);
            this.txtSalesOrderNo.Text = Convert.ToString(row["TWO_ORDER_NO"]);
            this.txtSalesItemNo.Text = Convert.ToString(row["TWO_ORDER_ITEM"]);
        }
        #endregion

        #region 托号查询
        private void txtCartonQuery_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (Convert.ToString(this.txtCarton.Text.Trim()).Equals(""))
                {
                    ToolsClass.Log("Carton No cannot be empty!", "ABNORMAL", lstView);
                    this.txtCarton.SelectAll();
                    this.txtCarton.Focus();
                    return;
                }
                DataTable dt = ProductStorageDAL.GetCartonStorageInfoByCarton(Convert.ToString(this.txtCarton.Text.Trim()));
                if (dt != null && dt.Rows.Count > 0)
                    SetDataGridView(dt);
                else
                {
                    ToolsClass.Log("No record found!", "ABNORMAL", lstView);
                    this.txtCarton.SelectAll();
                    this.txtCarton.Focus();
                    return;
                }
            }
        }
        private void txtCartonQuery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                PicBoxCarton_Click(null, null);
            }
        }
        private void PicBoxCarton_Click(object sender, EventArgs e)
        {
            var frm = new FormCartonInfoQuery(this.txtCarton.Text.Trim().ToString());
            frm.ShowDialog();
            DataTable dt = frm.CartonList;
            if (frm != null)
                frm.Dispose();
            SetDataGridView(dt);
        }

        private bool checkrepeat(string carton)
        {
            foreach (DataGridViewRow datarow in dataGridView1.Rows)
            {
                if (datarow.Cells[2].Value != null && datarow.Cells[2].Value.Equals(carton))
                {
                    ToolsClass.Log("Carton No：" + carton + " is already in list row" + (datarow.Index + 1), "ABNORMAL", lstView);
                    return false;
                }
            }
            return true;
        }
        private void SetDataGridView(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string cartonStatus = "";
                    if (Convert.ToString(row["CartonStatus"]).Equals("0"))
                        cartonStatus = "已测试";
                    else if (Convert.ToString(row["CartonStatus"]).Equals("1"))
                        cartonStatus = "已包装";
                    else if (Convert.ToString(row["CartonStatus"]).Equals("2"))
                        cartonStatus = "已入库";
                    else
                        cartonStatus = Convert.ToString(row["CartonStatus"]);

                    if (checkrepeat(Convert.ToString(row["CartonID"])))
                        dataGridView1.Rows.Insert(dataGridView1.Rows.Count, new object[] { false, this.dataGridView1.Rows.Count + 1, Convert.ToString(row["CartonID"]), cartonStatus, Convert.ToString(row["Cust_BoxID"]), Convert.ToString(row["SNQTY"]) });

                }
                this.txtCarton.Clear();
                this.txtCarton.Focus();
            }
            else
            {
                ToolsClass.Log("No record selected!", "ABNORMAL", lstView);
                return;
            }
        }
        #endregion

        private void ShowBatchInfo(string _BatchNo, string MitemType, object ddl, object txt, string _wo)
        {
            //Control ddl_text = ddl as Control;
            //Control text = txt as Control;
            //var frm = new FormMitemBatch(_BatchNo, MitemType, _wo);
            //frm.ShowDialog();
            //ddl_text.Text = frm.Batch;
            //text.Text = frm.MitemCode;
            //if (frm != null)
            //    frm.Dispose();
        }
        private void setMitemCode(string batchno, object label, bool flag, string MitemType)
        {
            if (this.txtWo.Text.Trim().Equals(""))
            {
                ToolsClass.Log("Please select production order!", "ABNORMAL", lstView);
                this.txtWoOrder.Focus();
                return;
            }

            Control c = label as Control;
            c.Text = "";
            if (!batchno.Equals(""))
            {
                string MitemCode = "";
                if (!ProductStorageDAL.GetMitemCode(batchno, MitemType, this.txtWo.Text.Trim(), WoType, out MitemCode).Equals(""))
                {
                    c.Text = MitemCode;
                }
                else
                {
                    MessageBox.Show("此批次:" + batchno + " 没找到所对应的物料，请确认！", MitemType);
                    c.Text = "";
                    return;
                }
            }
            else
            {
                if (flag)
                {
                    MessageBox.Show("输入的批次号不能为空", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
        }
        private void SetMitemBatch(string batch, string type, object ddl, object txt)
        {
            if (!this.txtWo.Text.Equals(""))
                ShowBatchInfo(batch, type, ddl, txt, this.txtWo.Text.Trim());
            else
            {
                ToolsClass.Log("Please select production order!", "ABNORMAL", lstView);
                this.txtWoOrder.Focus();
                return;
            }
        }

        #region 窗体事件
        /// <summary>
        /// 工单选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ddlWoType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlWoType.SelectedIndex == 0)
            {
                WoType = "";
                this.LblWo.Text = "工       单";
                this.label3.Text = "工       单";
                this.txtWoOrder.Clear();
                this.txtWoOrder.Focus();
                Reset1();
            }
            else if (this.ddlWoType.SelectedIndex == 1) //转常规工单
            {
                WoType = "ZP" + FormCover.CurrentFactory.Trim().ToUpper();
                WoType = WoType.Replace("M", "");
                this.LblWo.Text = "转常规 工单";
                this.label3.Text = "转常规 工单";
                this.txtWoOrder.Clear();
                this.txtWoOrder.Focus();
                Reset1();
            }
            else if (this.ddlWoType.SelectedIndex == 2)//外协工单
            {
                WoType = "ZP" + FormCover.CurrentFactory.Trim().ToUpper();
                WoType = WoType.Replace("M", "");
                this.LblWo.Text = "外协后 工单";
                this.label3.Text = "外协后 工单";
                this.txtWoOrder.Clear();
                this.txtWoOrder.Focus();
                Reset1();
            }
            else if (this.ddlWoType.SelectedIndex == 3)//返工工单
            {
                WoType = "ZP11";
                this.LblWo.Text = "返 工 工 单";
                this.label3.Text = "返 工 工 单";
                this.txtWoOrder.Clear();
                this.txtWoOrder.Focus();
                Reset1();
            }
        }

        private void Reset1()
        {
            this.txtWo.Clear();
            this.txtMitemCode.Clear();
            this.txtSalesItemNo.Clear();
            this.txtSalesOrderNo.Clear();
            this.txtFactory.Clear();
            this.txtlocation.Clear();
            this.txtPlanCode.Clear();
        }
        private void chbSelected_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
            {
                dgvRow.Cells[0].Value = this.chbSelected.Checked;
            }
        }
        /// <summary>
        /// 对于重工工单，如果托里混有新组建，必须要输入物料特性
        /// </summary>
        /// <param name="Cartons">托号清单</param>
        private bool CheckNewSnIsNull(List<string> Cartons)
        {
            foreach (string carton in Cartons)
            {
                DataTable dt = ProductStorageDAL.GetSNInfoFromPacking(carton);
                if ((dt != null) && (dt.Rows.Count > 0))
                {
                    #region
                    foreach (DataRow row in dt.Rows)
                    {
                        string sn = Convert.ToString(row["SN"]).Trim();
                        if (!sn.Equals(""))
                        {
                            DataTable dtnew = ProductStorageDAL.GetSNInfoFromInterface(sn);
                            //SAP已经入库过,物料可以不用输入
                            if ((dtnew != null) && (dtnew.Rows.Count > 0))
                                continue;
                            else
                            {
                                int configQty = ProductStorageDAL.GetSNStroageInfoFromConfig(sn);
                                if (configQty == 0)
                                {
                                    ToolsClass.Log("Module " + sn + " is newly added in rework, please maintain material infomation", "ABNORMAL", lstView);
                                    return false;
                                }
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    ToolsClass.Log("Getting carton：" + carton + " information failed", "ABNORMAL", lstView);
                    return false;
                }
            }
            return true;
        }
        private void Set_Click(object sender, EventArgs e)
        {
            try
            {
                #region
                if (this.ddlWoType.SelectedIndex == 0)
                {
                    ToolsClass.Log("Please select production order type", "ABNORMAL", lstView);
                    return;
                }
                else
                {
                    if (this.txtWo.Text.Trim().Equals(""))
                    {
                        if (this.ddlWoType.SelectedIndex == 1)
                        {
                            ToolsClass.Log("转常规工单不能为空", "ABNORMAL", lstView);
                            return;
                        }
                        else if (this.ddlWoType.SelectedIndex == 2)
                        {
                            ToolsClass.Log("外协后工单不能为空", "ABNORMAL", lstView);
                            return;
                        }
                        else if (this.ddlWoType.SelectedIndex == 3)
                        {
                            ToolsClass.Log("返工工单不能为空", "ABNORMAL", lstView);
                            return;
                        }
                    }
                }

                if (this.dataGridView1.Rows.Count > 0)
                {
                    if (dgvIndex.Count > 0)
                    {
                        dgvIndex.Clear();
                    }
                    if (CartonArrayS.Count > 0)
                    {
                        CartonArrayS.Clear();
                    }
                    for (int i = 0; i < this.dataGridView1.Rows.Count; i++)
                    {
                        if (this.dataGridView1.Rows[i].Cells[0].EditedFormattedValue.ToString().ToUpper() == "TRUE")
                        {
                            dgvIndex.Add(i, this.dataGridView1.Rows[i].Cells[0].EditedFormattedValue.ToString());
                            CartonArrayS.Add(this.dataGridView1.Rows[i].Cells[2].EditedFormattedValue.ToString());
                        }
                    }
                    if (dgvIndex.Count < 1)
                    {
                        ToolsClass.Log("没有选中要设置的数据,请确认!", "ABNORMAL", lstView);
                        return;
                    }
                }
                else
                {
                    ToolsClass.Log("没有要设置的数据,请确认!", "ABNORMAL", lstView);
                    return;
                }
                if (this.ddlWoType.SelectedIndex == 3)
                {
                    if (!CheckNewSnIsNull(CartonArrayS))
                        return;
                }

                foreach (int RowIndex in dgvIndex.Keys)
                {
                    this.dataGridView1.Rows[RowIndex].Cells["CartonStatus"].Value = "待入库";
                    this.dataGridView1.Rows[RowIndex].Cells["ProductCode"].Value = this.txtMitemCode.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["Factory"].Value = this.txtPlanCode.Text.Trim().ToString(); //界面工厂
                    this.dataGridView1.Rows[RowIndex].Cells["Workshop"].Value = this.txtFactory.Text.Trim().ToString(); //界面车间
                    this.dataGridView1.Rows[RowIndex].Cells["PackingLocation"].Value = this.txtlocation.Text.Trim().ToString();
                    this.dataGridView1.Rows[RowIndex].Cells["SalesOrderNo"].Value = Convert.ToString(this.txtSalesOrderNo.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["SalesItemNo"].Value = Convert.ToString(this.txtSalesItemNo.Text.Trim());
                    this.dataGridView1.Rows[RowIndex].Cells["NormalWo"].Value = "";
                    this.dataGridView1.Rows[RowIndex].Cells["WaiXieWo"].Value = "";
                    this.dataGridView1.Rows[RowIndex].Cells["ReworkWo"].Value = "";
                    if (this.ddlWoType.SelectedIndex == 1) //转常规工单
                        this.dataGridView1.Rows[RowIndex].Cells["NormalWo"].Value = this.txtWo.Text.Trim();
                    else if (this.ddlWoType.SelectedIndex == 2) //外协工单
                        this.dataGridView1.Rows[RowIndex].Cells["WaiXieWo"].Value = this.txtWo.Text.Trim().ToString();
                    else if (this.ddlWoType.SelectedIndex == 3) //返工工单
                        this.dataGridView1.Rows[RowIndex].Cells["ReworkWo"].Value = this.txtWo.Text.Trim().ToString();
                }

                ToolsClass.Log("设置成功!", "NORMAL", lstView);
                Reset_Click(null, null);
                #endregion
            }
            catch (Exception ex)
            {
                ToolsClass.Log("设置数据时发生异常：" + ex.Message + "!", "ABNORMAL", lstView);
                return;
            }
        }

        private void Reset_Click(object sender, EventArgs e)
        {
            if (dgvIndex.Count > 0)
                dgvIndex.Clear();
            ClearData(true);
            this.txtWoOrder.SelectAll();
            this.txtWoOrder.Focus();
            return;
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation == ScrollOrientation.HorizontalScroll)
            {
                int i = e.NewValue;
                int j = e.OldValue;
                chbSelected.Left = chbSelected.Left - i + j;
            }

            else if (e.ScrollOrientation == ScrollOrientation.VerticalScroll)
            {

                int i = e.NewValue;
                int j = e.OldValue;
                chbSelected.Top = chbSelected.Top - i + j;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex >= 0) && (e.ColumnIndex == 2))
            {
                string CartonNumber = Convert.ToString(this.dataGridView1.Rows[e.RowIndex].Cells["CartonID"].Value);
                if (!string.IsNullOrEmpty(CartonNumber))
                {
                    var frm = new FormSNDetail(CartonNumber);
                    if (frm.ShowDialog() != DialogResult.No)
                        frm.Show();
                    if (frm != null)
                    {
                        frm.Dispose();
                    }
                }
            }

            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                DataGridViewCheckBoxCell dgcb = (DataGridViewCheckBoxCell)dataGridView1.Rows[e.RowIndex].Cells["Selected"];
                if ((bool)dgcb.FormattedValue)
                {
                    dgcb.Value = false;
                }
                else
                {
                    dgcb.Value = true;
                }
            }
        }
        private void SetIsEnable(bool flag)
        {
            if (flag)
            {
                this.Save.Enabled = true;
                this.Set.Enabled = true;
                this.Reset.Enabled = true;
                this.ddlWoType.Enabled = true;
                this.txtWoOrder.Enabled = true;
                this.PicBoxWO.Enabled = true;

                this.txtCarton.Enabled = true;
                this.PicBoxCarton.Enabled = true;
                this.button1.Enabled = true;
            }
            else
            {
                this.Save.Enabled = false;
                this.Set.Enabled = false;
                this.Reset.Enabled = false;
                this.ddlWoType.Enabled = false;
                this.txtWoOrder.Enabled = false;
                this.PicBoxWO.Enabled = false;

                this.txtCarton.Enabled = false;
                this.PicBoxCarton.Enabled = false;
                this.button1.Enabled = false;
            }
        }
        /// <summary>
        /// 存储要入库OK的箱号
        /// </summary>
        List<string> cartonList = new List<string>();
        /// <summary>
        /// 存储待入库的托号和工单类型以及工单信息
        /// </summary>
        List<List<string>> cartonStroageList = new List<List<string>>();
        private void Save_Click(object sender, EventArgs e)
        {
            try
            {
                SetIsEnable(false);

                #region
                if (this.dataGridView1.Rows.Count < 1)
                {
                    SetIsEnable(true);
                    ToolsClass.Log("没有选中要入库的数据!", "ABNORMAL", lstView);
                    return;
                }

                if (cartonList.Count > 0)
                    cartonList.Clear();

                if (cartonStroageList.Count > 0)
                    cartonStroageList.Clear();

                #region 统计选中待入库的数据并保存到cartonStroageList中
                int StorageCount = 0;
                foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
                {
                    List<string> CartonWoList = new List<string>();
                    if (Convert.ToString(dgvRow.Cells[0].Value).ToUpper() == "TRUE")
                    {
                        StorageCount = StorageCount + 1;
                        if (!Convert.ToString(dgvRow.Cells[6].Value).Trim().Equals("")) //转常规工单
                        {
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[2].Value).Trim()); //内部托号
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[6].Value));  //转常规工单
                            CartonWoList.Add("N");
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[9].Value).Trim()); //产品物料编码
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[10].Value).Trim()); //销售订单
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[11].Value).Trim());//销售订单项目
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[12].Value).Trim()); //车间
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[13].Value).Trim());//入库地点
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[14].Value).Trim());//工厂
                            cartonStroageList.Add(CartonWoList);
                        }
                        else if (!Convert.ToString(dgvRow.Cells[7].Value).Trim().Equals("")) //返工工单
                        {
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[2].Value).Trim()); //内部托号
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[7].Value).Trim()); //返工工单
                            CartonWoList.Add("Y");
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[9].Value).Trim()); //产品物料编码
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[10].Value).Trim()); //销售订单
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[11].Value).Trim());//销售订单项目
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[12].Value).Trim()); //车间
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[13].Value).Trim());//入库地点
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[14].Value).Trim());//工厂
                            cartonStroageList.Add(CartonWoList);
                        }
                        else if (!Convert.ToString(dgvRow.Cells[8].Value).Trim().Equals("")) //外协后工单
                        {
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[2].Value).Trim()); //内部托号
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[8].Value).Trim()); //外协后工单
                            CartonWoList.Add("N");
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[9].Value).Trim()); //产品物料编码
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[10].Value).Trim()); //销售订单
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[11].Value).Trim());//销售订单项目
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[12].Value).Trim()); //车间
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[13].Value).Trim());//入库地点
                            CartonWoList.Add(Convert.ToString(dgvRow.Cells[14].Value).Trim());//工厂
                            cartonStroageList.Add(CartonWoList);
                        }
                    }
                }
                if (cartonStroageList.Count < 1)
                {
                    SetIsEnable(true);
                    ToolsClass.Log("没有设置要入库的数据!", "ABNORMAL", lstView);
                    return;
                }

                if (StorageCount < 1)
                {
                    SetIsEnable(true);
                    ToolsClass.Log("没有选中要入库的数据", "ABNORMAL", lstView);
                    return;
                }
                #endregion

                #region 每次最多值允许上传的托数量
                int maxQty = ProductStorageDAL.GetUploadStorageQty("MAXSTORAGEQTY", FormCover.CurrentFactory);
                if (maxQty <= 0)
                {
                    SetIsEnable(true);
                    return;
                }
                else
                {
                    if (StorageCount > maxQty)
                    {
                        SetIsEnable(true);
                        ToolsClass.Log("每次最多只能上传 " + maxQty + " 笔数据", "ABNORMAL", lstView);
                        return;
                    }
                }
                #endregion

                #region 检查数据是否维护齐全,重工工单的可以不用维护,并XML文件
                if (_ListSapWo.Count > 0)
                    _ListSapWo.Clear();
                DateTime CurrentTime = DateTime.Now;
                string PostedOn = CurrentTime.ToString("yyyy-MM-dd HH:mm:ss.fff"); //上传日期
                string PostKey = "004" + CurrentTime.ToString("yyyymmddhhmmssfff"); //上传条目号码

                var tsapReceiptUploadModules = new List<TsapReceiptUploadModule>();
                var createdOn = DT.DateTime().LongDateTime;
                var createdBy = FormCover.CurrUserName;
                var groupHistKey = FormCover.CurrentFactory + Guid.NewGuid().ToString("N");
                for (int i = 0; i < cartonStroageList.Count; i++)
                {
                    string carton = Convert.ToString(cartonStroageList[i][0]).Trim();
                    string Flag = Convert.ToString(cartonStroageList[i][2]).Trim();
                    if (!Flag.Equals("Y")) //Y表示返工工单
                    {
                        int stroageQty = ProductStorageDAL.GetSNStroageQty(carton);
                        if (stroageQty == 0)
                        {
                            ToolsClass.Log("托号已经 " + carton + " 入库或者没打包", "ABNORMAL", lstView);
                            continue;
                        }
                        else
                        {
                            int configQty = ProductStorageDAL.GetSNStroageQtyFromConfig(carton);
                            if (configQty == 0)
                            {
                                ToolsClass.Log("托号 " + carton + " 所对应的组件没有维护物料信息", "ABNORMAL", lstView);
                                continue;
                            }
                            else if (configQty != stroageQty)
                            {
                                ToolsClass.Log("托号 " + carton + " 所对应的组件中,有部分组件没有维护物料信息,请单击内部托号的连接查询", "ABNORMAL", lstView);
                                continue;
                            }
                        }
                    }

                    #region
                    //获取打包的数据
                    DataTable newdt = ProductStorageDAL.GetSAPCartonStorageInfo(carton);
                    if (newdt == null || newdt.Rows.Count == 0)
                    {
                        ToolsClass.Log("托号：" + carton + "获取组件入库信息失败", "ABNORMAL", lstView);
                        continue;
                    }

                    foreach (DataRow newdtrow in newdt.Rows)
                    {
                        SapWoModule SapWo = new SapWoModule();
                        SapWo.CellCode = Convert.ToString(newdtrow["电池片物料代码"]);
                        SapWo.CellBatch = Convert.ToString(newdtrow["电池片批次"]);
                        SapWo.ActionCode = "I";
                        SapWo.SysId = Convert.ToString(newdtrow["组件序列号"]);
                        SapWo.PostedOn = PostedOn;
                        SapWo.FinishedOn = Convert.ToDateTime(newdtrow["包装日期"]).ToString("yyyy-MM-dd HH:mm:ss.fff");
                        SapWo.Unit = "PC";
                        //SapWo.OrderNo = Convert.ToString(newdtrow["生产订单号"]);
                        SapWo.OrderNo = Convert.ToString(cartonStroageList[i][1]).Trim(); //工单
                        //SapWo.ProductCode = Convert.ToString(newdtrow["产品物料代码"]);
                        SapWo.ProductCode = Convert.ToString(cartonStroageList[i][3]).Trim(); //产品物料编码
                        //SapWo.Factory = Convert.ToString(newdtrow["工厂"]);
                        SapWo.Factory = Convert.ToString(cartonStroageList[i][8]).Trim(); //工厂
                        //SapWo.Workshop = Convert.ToString(newdtrow["车间"]);
                        SapWo.Workshop = Convert.ToString(cartonStroageList[i][6]).Trim(); //车间
                        //SapWo.PackingLocation = Convert.ToString(newdtrow["入库地点"]);
                        SapWo.PackingLocation = Convert.ToString(cartonStroageList[i][7]).Trim(); //入库地点
                        //SapWo.SalesItemNo = Convert.ToString(newdtrow["销售订单项目"]);
                        SapWo.SalesItemNo = Convert.ToString(cartonStroageList[i][5]).Trim(); //销售订单项目
                        //SapWo.SalesOrderNo = Convert.ToString(newdtrow["销售订单"]);
                        SapWo.SalesOrderNo = Convert.ToString(cartonStroageList[i][4]).Trim(); //销售订单
                        SapWo.CellEff = Convert.ToString(newdtrow["电池片转换效率"]);
                        SapWo.ModuleSN = Convert.ToString(newdtrow["组件序列号"]);
                        SapWo.CartonNo = Convert.ToString(newdtrow["内部托号"]);
                        SapWo.TestPower = Convert.ToString(newdtrow["实测功率"]);
                        SapWo.StdPower = Convert.ToString(newdtrow["标称功率"]);
                        SapWo.OrderStatus = "1";
                        SapWo.PostKey = PostKey;
                        SapWo.ModuleGrade = Convert.ToString(newdtrow["组件等级"]);
                        SapWo.ByIm = Convert.ToString(newdtrow["电流分档"]);
                        SapWo.CellPrintMode = Convert.ToString(newdtrow["电池片网版"]);
                        SapWo.GlassCode = Convert.ToString(newdtrow["玻璃物料代码"]);
                        SapWo.GlassBatch = Convert.ToString(newdtrow["玻璃批次"]);
                        SapWo.EvaCode = Convert.ToString(newdtrow["EVA 物料代码"]);
                        SapWo.EvaBatch = Convert.ToString(newdtrow["EVA 批次号"]);
                        SapWo.TptCode = Convert.ToString(newdtrow["TPT物料代码"]);
                        SapWo.TptBatch = Convert.ToString(newdtrow["TPT 批次"]);
                        SapWo.ConBoxCode = Convert.ToString(newdtrow["接线盒物料代码"]);
                        SapWo.ConBoxBatch = Convert.ToString(newdtrow["接线盒批次"]);
                        SapWo.LongFrameCode = Convert.ToString(newdtrow["长边框物料代码"]);
                        SapWo.LongFrameBatch = Convert.ToString(newdtrow["长边框批次"]);
                        SapWo.ShortFrameBatch = Convert.ToString(newdtrow["短边框批次"]);
                        SapWo.ShortFrameCode = Convert.ToString(newdtrow["短边框物料代码"]);
                        SapWo.PackingMode = Convert.ToString(newdtrow["包装方式"]);
                        SapWo.GlassThickness = Convert.ToString(newdtrow["玻璃厚度"]);
                        SapWo.IsCancelPacking = "1";
                        SapWo.IsOnlyPacking = "1";
                        SapWo.CustomerCartonNo = Convert.ToString(newdtrow["客户托号"]);
                        SapWo.InnerJobNo = "";
                        SapWo.RESV01 = Convert.ToString(newdtrow["生产订单号"]);
                        _ListSapWo.Add(SapWo);

                        var tsapReceiptUploadModule = new TsapReceiptUploadModule
                        {
                            Sysid = Guid.NewGuid().ToString(""),
                            CreatedOn = createdOn,
                            CreatedBy = createdBy,
                            GroupHistKey = groupHistKey,
                            CartonNo = SapWo.CartonNo,
                            CustomerCartonNo = SapWo.CustomerCartonNo,
                            FinishedOn = SapWo.FinishedOn,
                            ModuleColor = Convert.ToString(newdtrow["组件颜色"]),
                            ModuleSn = SapWo.ModuleSN,
                            OrderNo = SapWo.OrderNo,
                            SalesOrderNo = SapWo.SalesOrderNo,
                            SalesItemNo = SapWo.SalesItemNo,
                            OrderStatus = SapWo.OrderStatus,
                            ProductCode = SapWo.ProductCode,
                            Unit = SapWo.Unit,
                            Factory = SapWo.Factory,
                            Workshop = SapWo.Workshop,
                            PackingLocation = SapWo.PackingLocation,
                            PackingMode = SapWo.PackingMode,
                            CellEff = SapWo.CellEff,
                            TestPower = SapWo.TestPower,
                            StdPower = SapWo.StdPower,
                            ModuleGrade = SapWo.ModuleGrade,
                            ByIm = SapWo.ByIm,
                            Tolerance = Convert.ToString(newdtrow["公差"]),
                            CellCode = SapWo.CellCode,
                            CellBatch = SapWo.CellBatch,
                            CellPrintMode = SapWo.CellPrintMode,
                            GlassCode = SapWo.GlassCode,
                            GlassBatch = SapWo.GlassBatch,
                            EvaCode = SapWo.EvaCode,
                            EvaBatch = SapWo.EvaBatch,
                            TptCode = SapWo.TptCode,
                            TptBatch = SapWo.TptBatch,
                            ConboxCode = SapWo.ConBoxCode,
                            ConboxBatch = SapWo.ConBoxBatch,
                            ConboxType = string.Empty,
                            LongFrameCode = SapWo.LongFrameCode,
                            LongFrameBatch = SapWo.LongFrameBatch,
                            ShortFrameCode = SapWo.ShortFrameCode,
                            ShortFrameBatch = SapWo.ShortFrameBatch,
                            GlassThickness = SapWo.GlassThickness,
                            IsRework = Convert.ToString(newdtrow["是否重工"]),
                            IsByProduction = "false",
                            Resv01 = Convert.ToString(newdtrow["StdPowerLevel"])
                        };
                        if (Flag.Equals("Y"))
                            tsapReceiptUploadModule.IsRework = "Y";
                        tsapReceiptUploadModules.Add(tsapReceiptUploadModule);
                    }
                    #endregion

                    if (!cartonList.Contains(Convert.ToString(carton)))
                        cartonList.Add(Convert.ToString(carton));
                }
                #endregion

                if (_ListSapWo.Count > 0)
                {
                    string msg = "";
                    if (!ProductStorageDAL.CheckWoStorage(_ListSapWo, out msg))
                    {
                        SetIsEnable(true);
                        ToolsClass.Log(msg, "ABNORMAL", lstView);
                        return;
                    }
                    if (!DataAccess.QueryReworkOrgBatch(tsapReceiptUploadModules, out msg))
                    {
                        SetIsEnable(true);
                        ToolsClass.Log(msg, "ABNORMAL", lstView);
                        return;
                    }
                    //var findAll =
                    //    tsapReceiptUploadModules.FindAll(
                    //        p =>
                    //        string.IsNullOrEmpty(p.ProductCode) || string.IsNullOrEmpty(p.OrderNo) ||
                    //        string.IsNullOrEmpty(p.CellCode) || string.IsNullOrEmpty(p.CellBatch) ||
                    //        string.IsNullOrEmpty(p.GlassCode) || string.IsNullOrEmpty(p.GlassBatch) ||
                    //        string.IsNullOrEmpty(p.EvaCode) || string.IsNullOrEmpty(p.EvaBatch) ||
                    //        string.IsNullOrEmpty(p.TptCode) || string.IsNullOrEmpty(p.TptBatch) ||
                    //        string.IsNullOrEmpty(p.ConboxCode) || string.IsNullOrEmpty(p.ConboxBatch) ||
                    //        string.IsNullOrEmpty(p.LongFrameCode) || string.IsNullOrEmpty(p.LongFrameBatch) ||
                    //        string.IsNullOrEmpty(p.ShortFrameCode) || string.IsNullOrEmpty(p.ShortFrameBatch) ||
                    //        string.IsNullOrEmpty(p.PackingMode) || string.IsNullOrEmpty(p.ModuleGrade) ||
                    //        string.IsNullOrEmpty(p.ByIm) || string.IsNullOrEmpty(p.StdPower) ||
                    //        string.IsNullOrEmpty(p.ModuleColor) || string.IsNullOrEmpty(p.Tolerance));
                    //if (findAll.Count > 0)
                    //{
                    //    const string errorMsg =
                    //        "SAP要求以下数据不可为空，请检查：产品物料代码、生产订单号、电池片物料号、电池片批次、玻璃物料号、玻璃批次、EVA物料号、EVA批次、背板物料号、背板批次、接线盒物料号、接线盒批次、长边框物料号、长边框批次、短边框物料号、短边框批次、包装方式、组件等级、电流分档、标称功率、组件颜色、公差";
                    //    SetIsEnable(true);
                    //    ToolsClass.Log(errorMsg, "ABNORMAL", lstView);
                    //    return;
                    //}

                    List<string> listMessages = SapMessages(tsapReceiptUploadModules);
                    if (listMessages.Count > 0)
                    {
                        SetIsEnable(true);
                        foreach (var errorMsg in listMessages)
                        {
                            ToolsClass.Log(errorMsg, "ABNORMAL", lstView);
                        }
                        return;
                    }

                    if (
                        !DataAccess.IsSapInventoryUseManulUpload(FormCover.CurrentFactory.Trim(),
                            FormCover.InterfaceConnString))
                    {
                        //入库
                        SaveXml(_ListSapWo);

                        //对上传入库成功的做log记录
                        ProductStorageDAL.SaveStorageInfoLog(_ListSapWoLog);
                    }
                    else
                    {
                        //保存数据供手动上传使用
                        SavePreUploadData(_ListSapWo, tsapReceiptUploadModules);
                    }
                    if (_ListSapWoLog.Count > 0)
                        _ListSapWoLog.Clear();
                }

                SetIsEnable(true);
                this.chbSelected.Checked = false;

                #endregion
            }
            catch (Exception ex)
            {
                SetIsEnable(true);
                ToolsClass.Log("保存待上传SAP数据发生异常(处理包装数据):" + ex.Message + "!", "ABNORMAL", lstView);
                return;
            }
        }
        private List<string> SapMessages(List<TsapReceiptUploadModule> sapReceipt)
        {
            List<string> messages = new List<string>();
            try
            {

                //"SAP要求以下数据不可为空，请检查：产品物料代码、生产订单号、电池片物料号、电池片批次、
                //玻璃物料号、玻璃批次、EVA物料号、EVA批次、背板物料号、背板批次、接线盒物料号、
                //接线盒批次、长边框物料号、长边框批次、短边框物料号、短边框批次、包装方式、组件等级、
                //电流分档、标称功率、组件颜色、公差";

                if (sapReceipt != null && sapReceipt.Count > 0)
                {
                    StringBuilder strtmp;
                    int i = 1;
                    foreach (TsapReceiptUploadModule sapModule in sapReceipt)
                    {
                        strtmp = new StringBuilder();
                        bool blntmp = true;

                        if (!string.IsNullOrEmpty(sapModule.OrderNo))
                        {
                            strtmp.Append("工单号:" + sapModule.OrderNo + "--");
                        }
                        else
                        {
                            strtmp.Append("【第 " + i.ToString() + "行工单号为空】");
                            blntmp = false;
                        }
                        if (!string.IsNullOrEmpty(sapModule.ModuleSn))
                        {
                            strtmp.Append("序列号:" + sapModule.ModuleSn + "   缺失的值【");

                        }
                        if (string.IsNullOrEmpty(sapModule.ProductCode))
                        {
                            strtmp.Append("产品物料代码、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.CellCode))
                        {
                            strtmp.Append("电池片物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.CellBatch))
                        {
                            strtmp.Append("电池片批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.GlassCode))
                        {
                            strtmp.Append("玻璃批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.GlassBatch))
                        {
                            strtmp.Append("EVA物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.EvaCode))
                        {
                            strtmp.Append("EVA批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.EvaBatch))
                        {
                            strtmp.Append("背板物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.TptCode))
                        {
                            strtmp.Append("背板批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.TptBatch))
                        {
                            strtmp.Append("电池片物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ConboxCode))
                        {
                            strtmp.Append("接线盒物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ConboxBatch))
                        {
                            strtmp.Append("接线盒批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.LongFrameCode))
                        {
                            strtmp.Append("长边框物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.LongFrameBatch))
                        {
                            strtmp.Append("长边框批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ShortFrameCode))
                        {
                            strtmp.Append("短边框物料号、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ShortFrameBatch))
                        {
                            strtmp.Append("短边框批次、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.PackingMode))
                        {
                            strtmp.Append("包装方式、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ModuleGrade))
                        {
                            strtmp.Append("组件等级、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ByIm))
                        {
                            strtmp.Append("电流分档、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.StdPower))
                        {
                            strtmp.Append("标称功率、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.ModuleColor))
                        {
                            strtmp.Append("组件颜色、");
                            blntmp = false;
                        }
                        if (string.IsNullOrEmpty(sapModule.Tolerance))
                        {
                            strtmp.Append("公差、");
                            blntmp = false;
                        }
                        if (strtmp != null && !string.IsNullOrEmpty(strtmp.ToString()) && blntmp == false)
                        {
                            messages.Add(strtmp + "】 【第 " + i.ToString() + " 行】");
                        }
                        i = i + 1;
                    }
                }
            }
            catch (Exception ex)
            {
                messages.Add("SAP 数据检查异常:" + ex.Message.ToString());
            }
            return messages;
        }
        private bool SaveXml(List<SapWoModule> listWo)
        {
            try
            {
                #region
                DataTable dt = new DataTable();
                string SapStorageString = SerializerHelper.BuildXmlByObject<List<SapWoModule>>(listWo, Encoding.UTF8);
                SapStorageString = SapStorageString.Replace("ArrayOf", "Mes");
                string cars = "";
                if (cartonList.Count > 0)
                {
                    foreach (string carton in cartonList)
                        cars += "," + carton;
                }
                ToolsClass.Log("内部托号：" + cars + " 入库数据保存");
                ToolsClass.Log(SapStorageString);
                try
                {
                    SapStorageService.SapMesInterfaceClient service = new SapStorageService.SapMesInterfaceClient();
                    service.Endpoint.Address = new System.ServiceModel.EndpointAddress(FormCover.WebServiceAddress);
                    string aa = service.packingDataMesToSap(SapStorageString);
                    string bb;
                    dt = ProductStorageDAL.ReadXmlToDataTable(aa, "MessageDetail", out bb);
                }
                catch (Exception ex)
                {
                    this.Save.Enabled = true;
                    this.button1.Enabled = true;
                    ToolsClass.Log("连接SAP时,发生异常." + ex.Message + "", "ABNORMAL", lstView);
                    return false;
                }

                if (!listWo.Count.Equals(dt.Rows.Count))
                {
                    this.Save.Enabled = true;
                    this.button1.Enabled = true;
                    ToolsClass.Log("上传SAP和SAP返回的总数据不一致", "ABNORMAL", lstView);
                    return false;
                }

                try
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        //if (_ListCartonSucess.Count > 0)
                        //    _ListCartonSucess.Clear();

                        #region
                        List<SapWoModule> _ListCartonSapWo = new List<SapWoModule>();

                        string joinid = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");//交易ID
                        string posttime = DT.DateTime().LongDateTime;
                        foreach (string carton in cartonList)
                        {
                            string flag = "Y";
                            if (_ListCartonSapWo.Count > 0)
                                _ListCartonSapWo.Clear();
                            for (int mmm = 0; mmm < listWo.Count; mmm++)
                            {
                                if (carton == listWo[mmm].CartonNo)
                                {
                                    DataRow[] rows = dt.Select("key = '" + listWo[mmm].ModuleSN + "'");
                                    if (rows.Length > 0)
                                    {
                                        //上传失败
                                        if (Convert.ToString(rows[0]["Result"]).Equals("0"))
                                        {
                                            flag = "N";
                                            this.Save.Enabled = true;
                                            this.button1.Enabled = true;
                                            ToolsClass.Log("托号：" + carton + " 上传SAP保存失败.原因:" + Convert.ToString(rows[0]["Message"]) + "", "ABNORMAL", lstView);
                                            break;
                                        }
                                        else
                                            _ListCartonSapWo.Add(listWo[mmm]);
                                    }
                                    else //上传SAP失败
                                    {
                                        flag = "N";
                                        this.Save.Enabled = true;
                                        this.button1.Enabled = true;
                                        ToolsClass.Log("托号：" + carton + " 上传SAP保存失败.原因:SAP返回数据丢失", "ABNORMAL", lstView);
                                        break;
                                    }
                                }
                            }
                            //上传成功
                            if (flag.Equals("Y"))
                            {
                                if (ProductStorageDAL.updatepackingdatabySn(_ListCartonSapWo, joinid, posttime))
                                {

                                    //if (!_ListCartonSucess.Contains(carton))
                                    //    _ListCartonSucess.Add(carton);
                                    this.Save.Enabled = true;
                                    this.button1.Enabled = true;
                                    ToolsClass.Log("托号：" + carton + " 上传SAP保存成功", "NORMAL", lstView);

                                    try
                                    {
                                        #region 移除上传成功的托号
                                        foreach (DataGridViewRow dgr in this.dataGridView1.Rows)
                                        {
                                            if (dgr.Cells[2].Value != null && dgr.Cells[2].Value.Equals(carton))
                                            {
                                                this.dataGridView1.Rows.RemoveAt(dgr.Index);
                                                break;
                                            }
                                        }

                                        DataTable SN = ProductStorageDAL.GetSNInfoByCarton(carton);
                                        if (SN != null && SN.Rows.Count > 0)
                                        {
                                            foreach (DataRow newsn in SN.Rows)
                                            {
                                                //成功的托号赋值给一个新的List,做Log记录
                                                for (int p = 0; p < _ListSapWo.Count; p++)
                                                {
                                                    if (Convert.ToString(newsn["SN"]) == _ListSapWo[p].ModuleSN)
                                                    {
                                                        _ListSapWoLog.Add(_ListSapWo[p]);
                                                    }
                                                }

                                                for (int q = 0; q < _ListPackingSNCellInfo.Count; q++)
                                                {
                                                    if (Convert.ToString(newsn["SN"]) == _ListPackingSNCellInfo[q].SN)
                                                    {
                                                        _ListPackingSNCellInfo.RemoveAt(q);
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        #endregion
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                }
                                else
                                {
                                    this.Save.Enabled = true;
                                    this.button1.Enabled = true;
                                    ToolsClass.Log("托号：" + carton + "上传SAP保存失败(更新包装数据库错误)", "NORMAL", lstView);
                                }
                            }
                        }
                        //#region 写入库交易记录
                        //if (_ListCartonSucess.Count > 0)
                        //{
                        //    ProductStorageDAL.SaveStorageDataForReport(_ListCartonSucess, "Invertory");
                        //}
                        //#endregion

                        #endregion
                    }
                    else
                    {
                        this.Save.Enabled = true;
                        this.button1.Enabled = true;
                        ToolsClass.Log("上传SAP保存,返回数据错误", "ABNORMAL", lstView);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    this.Save.Enabled = true;
                    this.button1.Enabled = true;
                    ToolsClass.Log("处理SAP返回数据时发生异常:" + ex.Message + "!", "ABNORMAL", lstView);
                    return false;
                }
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                this.Save.Enabled = true;
                this.button1.Enabled = true;
                ToolsClass.Log("上传SAP时候发生异常(处理SAP返回数据):" + ex.Message + "!", "ABNORMAL", lstView);
                return false;
            }
        }

        private bool SavePreUploadData(List<SapWoModule> listWo, List<TsapReceiptUploadModule> tsapReceiptUploadModules)
        {
            try
            {
                #region

                var cars = string.Join(",", cartonList.ToArray());
                ToolsClass.Log("内部托号：" + cars + "入库数据保存");

                var joinId = Guid.NewGuid().ToString("N") + DateTime.Now.ToString("fff");//交易ID
                var postTime = DT.DateTime().LongDateTime;
                foreach (var carton in cartonList)
                {
                    var listCartonSapWo = listWo.FindAll(p => p.CartonNo == carton);
                    if (listCartonSapWo.Count <= 0)
                    {
                        Save.Enabled = true;
                        button1.Enabled = true;
                        ToolsClass.Log("数据出错!", "ABNORMAL", lstView);
                        return false;
                    }
                    var listTsapReceiptUploadModules = tsapReceiptUploadModules.FindAll(p => p.CartonNo == carton);
                    if (listTsapReceiptUploadModules.Count <= 0)
                    {
                        Save.Enabled = true;
                        button1.Enabled = true;
                        ToolsClass.Log("数据出错!", "ABNORMAL", lstView);
                        return false;
                    }
                    if (!ProductStorageDAL.updatepackingdatabySn(listTsapReceiptUploadModules, listCartonSapWo, joinId, postTime))
                    {
                        Save.Enabled = true;
                        button1.Enabled = true;
                        ToolsClass.Log("托号：" + carton + "待上传SAP数据保存失败(更新包装数据库错误)", "NORMAL", lstView);
                        return false;
                    }
                    this.Save.Enabled = true;
                    this.button1.Enabled = true;
                    ToolsClass.Log("托号：" + carton + "待上传SAP数据保存成功", "NORMAL", lstView);

                    #region 移除上传成功的托号

                    foreach (DataGridViewRow dgr in this.dataGridView1.Rows)
                    {
                        if (dgr.Cells[2].Value == null || !dgr.Cells[2].Value.Equals(carton))
                            continue;
                        dataGridView1.Rows.RemoveAt(dgr.Index);
                        break;
                    }

                    #endregion
                }

                return true;
                #endregion
            }
            catch (Exception ex)
            {
                Save.Enabled = true;
                button1.Enabled = true;
                ToolsClass.Log("待上传SAP数据保存发生异常:" + ex.Message + "!", "ABNORMAL", lstView);
                return false;
            }
        }

        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();

            if (_ListSapWo.Count > 0)
                _ListSapWo.Clear();

            if (_ListPackingSNCellInfo.Count > 0)
                _ListPackingSNCellInfo.Clear();

            if (cartonList.Count > 0)
                cartonList.Clear();

            this.txtCarton.Clear();
            this.txtCarton.Focus();
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {

        }
        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (lstView.SelectedItems.Count > 0)
                {
                    if (lstView.SelectedItems[0].Text != "")
                        Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
                }
            }
        }

        #region
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            this.txtWO1.Enabled = this.checkBox2.Checked;
            this.pictureBox1.Enabled = this.checkBox2.Checked;
            this.txtWO1.Clear();
            this.txtWO1.Focus();
        }

        private void txtWO1_Leave(object sender, EventArgs e)
        {
            //判断是否有外协工单
            if (!this.txtWO1.Text.Trim().ToString().Equals(""))
            {
                DataTable dt = ProductStorageDAL.GetWoMasterIno(this.txtWO1.Text.Trim());
                if (dt == null || dt.Rows.Count == 0)
                {
                    if (this.checkBox2.Checked)
                    {
                        ToolsClass.Log("输入的外协工单：" + this.txtWO1.Text.Trim() + " 没有从SAP下载", "ABNORMAL", lstView);
                        this.txtWO1.Clear();
                        this.txtWO1.Focus();
                        return;
                    }
                }
                else
                {
                    DataRow row = dt.Rows[0];
                    this.txtFactory.Text = Convert.ToString(row["TWO_WORKSHOP"]);
                    this.txtMitemCode.Text = Convert.ToString(row["TWO_PRODUCT_NAME"]);
                    this.txtlocation.Text = Convert.ToString(row["RESV04"]);
                    this.txtPlanCode.Text = Convert.ToString(row["RESV06"]);
                    this.txtSalesOrderNo.Text = Convert.ToString(row["TWO_ORDER_NO"]);
                    this.txtSalesItemNo.Text = Convert.ToString(row["TWO_ORDER_ITEM"]);
                }
            }
            else
            {
                if (this.checkBox2.Checked)
                {
                    ToolsClass.Log("请输入外协工单号码", "ABNORMAL", lstView);
                    this.txtWO1.Focus();
                }
            }
        }

        private void txtWO1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                txtWO1_Leave(null, null);
        }

        private void txtWO1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 114) //F3按钮
            {
                pictureBox1_Click(null, null);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            var frm = new FormWOInfoQuery(this.txtWoOrder.Text.Trim().ToString(), "");
            frm.ShowDialog();
            this.txtWO1.Text = frm.Wo;
            if (frm != null)
                frm.Dispose();
        }
        #endregion
    }
}
