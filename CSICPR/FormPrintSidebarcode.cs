﻿using System;
using System.ComponentModel;
//using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Collections.Generic;
using LablePLibrary;

namespace CSICPR
{
    public partial class FormPrintSidebarcode : Form     
    {
        #region 共有变量
        private static FormPrintSidebarcode theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormPrintSidebarcode();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        public FormPrintSidebarcode()
        {
            InitializeComponent();
        }

        private void FormPrintSidebarcode_Load(object sender, EventArgs e)
        {
            this.lstView.Columns.Add("提示信息", 1000, HorizontalAlignment.Left);
        }
 
        /// <summary>
        /// 提供数据复制功能
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (lstView.SelectedItems.Count > 0)
                {
                    if (lstView.SelectedItems[0].Text != "")
                        Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
                }
            }
        }

        private string SerialNumber = "";
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.txtSerialNumber.Text.Trim().Equals(""))
            {
                ToolsClass.Log("输入的组件号码不能为空！", "ABNORMAL", lstView);
                this.txtSerialNumber.Focus();
                this.txtSerialNumber.SelectAll();
                return;
            }
            else
                SerialNumber = this.txtSerialNumber.Text.Trim();

            try
            {
                string labelPath = Application.StartupPath + "\\Label\\SideFrame.lab";
                string msg = LablePrint.SidePrintLable(labelPath, SerialNumber,1);
                if (msg.Equals(""))
                {
                    ToolsClass.Log("组件号码:" + SerialNumber + " 列印成功", "NORMAL", lstView);
                    
                    this.txtSerialNumber.Clear();
                    this.txtSerialNumber.Focus();
                    this.txtSerialNumber.SelectAll();
                }
                else
                {
                    ToolsClass.Log("组件号码:" + SerialNumber + " 列印发生错误("+msg+")", "ABNORMAL", lstView);
                    this.txtSerialNumber.Focus();
                    this.txtSerialNumber.SelectAll();
                }

                if (lstView.Items.Count > 50)
                    lstView.Items.Clear();
            }
            catch (Exception ex)
            {
                ToolsClass.Log("组件号码: " + SerialNumber + " 在打印标签时发生异常 " + ex.Message + "", "ABNORMAL", lstView);
                this.txtSerialNumber.Focus();
                this.txtSerialNumber.SelectAll();
            }
        }
        private void txtSerialNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                this.btnSave_Click(null,null);
        }

    }
}
