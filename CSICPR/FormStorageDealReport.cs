﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormStorageDealReport : Form
    {
        private static List<LanguageItemModel> LanMessList;
        private static FormStorageDealReport theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormStorageDealReport();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }

        /// <summary>
        /// 托号
        /// </summary>
        private string Carton = "";
        /// <summary>
        /// 入库数据处理开始日期
        /// </summary>
        private DateTime DateFrom;
        /// <summary>
        /// 入库数据处理结束日期
        /// </summary>
        private DateTime DateTo;
        /// <summary>
        /// 查询类型
        /// </summary>
        private string QueryType = "";

        private void ClearDataGridViewData()
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();
        }

        private void SetDataGridViewData()
        {
            if (!QueryType.Trim().ToUpper().Equals("DATE"))
            {
                if (Convert.ToString(this.txtCarton.Text.Trim()).Equals(""))
                {
                    MessageBox.Show("托号不能为空,请输入！", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.txtCarton.SelectAll();
                    this.txtCarton.Focus();
                    return;
                }
                else
                    Carton = Convert.ToString(this.txtCarton.Text.Trim());
            }
            else
            {
                DateFrom = Convert.ToDateTime(this.dtpStorageDateFrom.Value);
                DateTo = Convert.ToDateTime(this.dtpStorageDateTo.Value);
            }

            DataTable dt = ProductStorageDAL.GetSapStorageDealResultInfo(Carton, QueryType, DateFrom, DateTo);
            if (dt != null && dt.Rows.Count > 0)
            {
                int RowNo = 0;
                int RowIndex = 1;
                foreach (DataRow row in dt.Rows)
                {
                    this.dataGridView1.Rows.Add();
                    this.dataGridView1.Rows[RowNo].Cells["RowIndex"].Value = RowIndex.ToString();
                    this.dataGridView1.Rows[RowNo].Cells["CartonNo"].Value = Convert.ToString(row["CartonNo"]);
                    this.dataGridView1.Rows[RowNo].Cells["Result"].Value = Convert.ToString(row["ProcessedResult"]);
                    this.dataGridView1.Rows[RowNo].Cells["OperatorDate"].Value = Convert.ToString(row["CreatedOn"]);
                    this.dataGridView1.Rows[RowNo].Cells["Factory"].Value = FormCover.CurrentFactory.Trim().ToString();
                    RowNo++;
                    RowIndex++;
                }
            }
            else
            {
                MessageBox.Show("没有查询到数据！", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.txtCarton.SelectAll();
                this.txtCarton.Focus();
                return;
            }
        }

        public FormStorageDealReport()
        {
            InitializeComponent();
        }
      
        private void btnQuery_Click(object sender, EventArgs e)
        {
            ClearDataGridViewData();
            SetDataGridViewData();
        }

        private void txtCarton_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                btnQuery_Click(null,null);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ToolsClass.DataToExcel(this.dataGridView1, true);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();
            this.txtCarton.Clear();
            this.txtCarton.Focus();
        }

        private void FormStorageDealReport_Load(object sender, EventArgs e)
        {
            //查询类型
            this.ddlQueryType.Items.Insert(0, "处理日期");
            this.ddlQueryType.Items.Insert(1, "内部托号");
            //默认查询类型
            this.ddlQueryType.SelectedIndex = 1;
            this.PalDealDate.Visible = false;
            this.PalCarton.Visible = true;
            QueryType = "Carton";


            #  region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            //cbQueryTerm 多语言
            LanguageHelper.GetCombomBox(this, this.ddlQueryType);

            # endregion

        }

        private void ddlQueryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearDataGridViewData();

            if (this.ddlQueryType.SelectedIndex == 0)
            {
                this.PalDealDate.Visible = true;
                this.PalCarton.Visible = false;
                this.dtpStorageDateFrom.Focus();
                QueryType = "Date";
            }
            else if (this.ddlQueryType.SelectedIndex == 1)
            {
                this.PalDealDate.Visible = false;
                this.PalCarton.Visible = true;
                this.txtCarton.Clear();
                this.txtCarton.Focus();
                QueryType = "Carton";
            }
        }            
    }
}
