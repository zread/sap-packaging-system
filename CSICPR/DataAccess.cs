﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace CSICPR
{
    public class DataAccess
    {
        public static bool IsQueryReworkDataFromEsb(string workshop, string connStr)
        {
            const string sql = @"
SELECT A.SYSID AS Sysid,A.FUNCTION_CODE AS FunctionCode,A.MAPPING_KEY_01 AS MappingKey01
    ,A.MAPPING_KEY_02 AS MappingKey02,A.MAPPING_KEY_03 AS MappingKey03,A.MAPPING_KEY_04 AS MappingKey04
    ,A.MAPPING_KEY_05 AS MappingKey05,A.MAPPING_KEY_06 AS MappingKey06,A.MAPPING_KEY_07 AS MappingKey07
    ,A.MAPPING_KEY_08 AS MappingKey08,A.MAPPING_KEY_09 AS MappingKey09,A.CREATED_BY AS CreatedBy
    ,A.CREATED_ON AS CreatedOn,A.MODIFIED_BY AS ModifiedBy,A.MODIFIED_ON AS ModifiedOn,A.STATUS AS Status
    ,A.RESV01 AS Resv01,A.RESV02 AS Resv02,A.RESV03 AS Resv03,A.RESV04 AS Resv04,A.RESV05 AS Resv05
    ,A.RESV06 AS Resv06,A.RESV07 AS Resv07,A.RESV08 AS Resv08,A.RESV09 AS Resv09,A.RESV10 AS Resv10
FROM T_SYS_MAPPING AS A WITH(NOLOCK) 
WHERE 1 = 1 
    AND A.FUNCTION_CODE = 'IsQueryReworkDataFromEsb' 
    AND A.MAPPING_KEY_02 = @Workshop 
ORDER BY A.SYSID ";

            var list = Dapper.Query<SysMapping>(sql, new { Workshop = workshop }, connStr);
            if (list == null || list.Count <= 0)
                return false;
            var mapping = list[0];
            return !string.IsNullOrEmpty(mapping.MappingKey01) && mapping.MappingKey01.ToUpper().Trim() == "Y";
        }

        public static bool IsSapInventoryUseManulUpload(string workshop, string connStr)
        {
            const string sql = @"
SELECT A.SYSID AS Sysid,A.FUNCTION_CODE AS FunctionCode,A.MAPPING_KEY_01 AS MappingKey01
    ,A.MAPPING_KEY_02 AS MappingKey02,A.MAPPING_KEY_03 AS MappingKey03,A.MAPPING_KEY_04 AS MappingKey04
    ,A.MAPPING_KEY_05 AS MappingKey05,A.MAPPING_KEY_06 AS MappingKey06,A.MAPPING_KEY_07 AS MappingKey07
    ,A.MAPPING_KEY_08 AS MappingKey08,A.MAPPING_KEY_09 AS MappingKey09,A.CREATED_BY AS CreatedBy
    ,A.CREATED_ON AS CreatedOn,A.MODIFIED_BY AS ModifiedBy,A.MODIFIED_ON AS ModifiedOn,A.STATUS AS Status
    ,A.RESV01 AS Resv01,A.RESV02 AS Resv02,A.RESV03 AS Resv03,A.RESV04 AS Resv04,A.RESV05 AS Resv05
    ,A.RESV06 AS Resv06,A.RESV07 AS Resv07,A.RESV08 AS Resv08,A.RESV09 AS Resv09,A.RESV10 AS Resv10
FROM T_SYS_MAPPING AS A WITH(NOLOCK) 
WHERE 1 = 1 
    AND A.FUNCTION_CODE = 'SapInventoryUseManulUpload' 
    AND A.MAPPING_KEY_01 = @Workshop 
ORDER BY A.SYSID ";

            var list = Dapper.Query<SysMapping>(sql, new { Workshop = workshop }, connStr);
            if (list == null || list.Count <= 0)
                return false;
            var mapping = list[0];
            return !string.IsNullOrEmpty(mapping.MappingKey02) && mapping.MappingKey02.ToUpper().Trim() == "Y";
        }



        public static bool IsCtmJudge(string fac,string workshop, string connStr)
        {
            const string sql = @"
SELECT A.SYSID AS Sysid,A.FUNCTION_CODE AS FunctionCode,A.MAPPING_KEY_01 AS MappingKey01
    ,A.MAPPING_KEY_02 AS MappingKey02,A.MAPPING_KEY_03 AS MappingKey03,A.MAPPING_KEY_04 AS MappingKey04
    ,A.MAPPING_KEY_05 AS MappingKey05,A.MAPPING_KEY_06 AS MappingKey06,A.MAPPING_KEY_07 AS MappingKey07
    ,A.MAPPING_KEY_08 AS MappingKey08,A.MAPPING_KEY_09 AS MappingKey09,A.CREATED_BY AS CreatedBy
    ,A.CREATED_ON AS CreatedOn,A.MODIFIED_BY AS ModifiedBy,A.MODIFIED_ON AS ModifiedOn,A.STATUS AS Status
    ,A.RESV01 AS Resv01,A.RESV02 AS Resv02,A.RESV03 AS Resv03,A.RESV04 AS Resv04,A.RESV05 AS Resv05
    ,A.RESV06 AS Resv06,A.RESV07 AS Resv07,A.RESV08 AS Resv08,A.RESV09 AS Resv09,A.RESV10 AS Resv10
FROM T_SYS_MAPPING AS A WITH(NOLOCK) 
WHERE 1 = 1 
    AND A.FUNCTION_CODE = 'CTMNotJudgement' 
    AND A.MAPPING_KEY_01 = @Fac    
    AND A.MAPPING_KEY_02 = @Workshop 
ORDER BY A.SYSID ";

            var list = Dapper.Query<SysMapping>(sql, new {@Fac =fac, @Workshop = workshop }, connStr);
            if (list == null || list.Count <= 0)
                return false;
            var mapping = list[0];
            return !string.IsNullOrEmpty(mapping.MappingKey03) && mapping.MappingKey03.ToUpper().Trim() == "Y";
        }

        public static int QueryModuleCntByReworkOrder(string workOrder, string connStr)
        {
            const string sql = @"
select COUNT(distinct sn) 
from Product with(nolock)
where ReWorkOrder=@WorkOrder ";

            return Dapper.QuerySingle<int>(sql, new { WorkOrder = workOrder }, connStr);
        }

        public static int QueryOrderQty(string workOrder, string connStr)
        {
            const string sql = @"
select top 1 isnull(TWO_WO_QTY,0)
from T_WORK_ORDERS with(nolock) 
where TWO_NO = @WorkOrder ";

            return Dapper.QuerySingle<int>(sql, new { WorkOrder = workOrder }, connStr);
        }

        public static bool InsertTsapReceiptUploadModule(TsapReceiptUploadModule tsapReceiptUploadModule, SqlConnection conn, SqlTransaction trans)
        {
            return InsertTsapReceiptUploadModule(string.Empty, tsapReceiptUploadModule, conn, trans);
        }

        public static bool InsertTsapReceiptUploadModule(string dbName, TsapReceiptUploadModule tsapReceiptUploadModule, SqlConnection conn, SqlTransaction trans)
        {
            var sql = @"
INSERT INTO {0}TSAP_RECEIPT_UPLOAD_MODULE(SYSID,CREATED_ON,CREATED_BY,JOB_NO,CARTON_NO,CUSTOMER_CARTON_NO,INNER_JOB_NO,FINISHED_ON,MODULE_COLOR,GROUP_HIST_KEY,MODULE_SN,ORDER_NO,SALES_ORDER_NO,SALES_ITEM_NO,ORDER_STATUS,PRODUCT_CODE,UNIT,FACTORY,WORKSHOP,PACKING_LOCATION,PACKING_MODE,CELL_EFF,TEST_POWER,STD_POWER,MODULE_GRADE,BY_IM,TOLERANCE,CELL_CODE,CELL_BATCH,CELL_PRINT_MODE,GLASS_CODE,GLASS_BATCH,EVA_CODE,EVA_BATCH,TPT_CODE,TPT_BATCH,CONBOX_CODE,CONBOX_BATCH,CONBOX_TYPE,LONG_FRAME_CODE,LONG_FRAME_BATCH,SHORT_FRAME_CODE,SHORT_FRAME_BATCH,GLASS_THICKNESS,IS_REWORK,IS_BY_PRODUCTION,UPLOAD_STATUS,RESV01,RESV02,RESV03,RESV04,RESV05,RESV06,RESV07,RESV08,RESV09,RESV10)
VALUES(@Sysid,CONVERT(NVARCHAR(50),GETDATE(),121),@CreatedBy,@JobNo,@CartonNo,@CustomerCartonNo,@InnerJobNo,@FinishedOn,@ModuleColor,@GroupHistKey,@ModuleSn,@OrderNo,@SalesOrderNo,@SalesItemNo,@OrderStatus,@ProductCode,@Unit,@Factory,@Workshop,@PackingLocation,@PackingMode,@CellEff,@TestPower,@StdPower,@ModuleGrade,@ByIm,@Tolerance,@CellCode,@CellBatch,@CellPrintMode,@GlassCode,@GlassBatch,@EvaCode,@EvaBatch,@TptCode,@TptBatch,@ConboxCode,@ConboxBatch,@ConboxType,@LongFrameCode,@LongFrameBatch,@ShortFrameCode,@ShortFrameBatch,@GlassThickness,@IsRework,@IsByProduction,@UploadStatus,@Resv01,@Resv02,@Resv03,@Resv04,@Resv05,@Resv06,@Resv07,@Resv08,@Resv09,@Resv10)

";
            if (!string.IsNullOrEmpty(dbName))
                dbName = string.Format("{0}.dbo.", dbName);
            sql = string.Format(sql, dbName);

            return Dapper.Save(tsapReceiptUploadModule, sql, conn, trans);
        }

        public static bool CanUploadToSap(string cartonNo, string connStr, out string msg)
        {
            const string sql = @"
SELECT TOP 1 ISNULL(UPLOAD_STATUS,'') 
FROM TSAP_RECEIPT_UPLOAD_MODULE WITH(NOLOCK) 
WHERE CARTON_NO = @CartonNo 
ORDER BY CREATED_ON DESC ";

            var list = Dapper.Query<string>(sql, new { CartonNo = cartonNo }, connStr);
            if (list == null || list.Count <= 0)
            {
                msg = "Carton:" + cartonNo + " have not stock in!";
                return false;
            }
            var uploadStatus = list[0];
            if (uploadStatus == "Finished")
            {
                msg = "Carton:" + cartonNo + " already uploaded to SAP";
                return false;
            }
            if (uploadStatus == "Canceled")
            {
                msg = "Carton:" + cartonNo + " have already cancelled stock in";
                return false;
            }
            
            msg = string.Empty;
            return true;
        }

        public static List<TsapReceiptUploadModule> QueryTsapReceiptUploadModuleByCartonNo(string cartonNo, string connStr)
        {
            const string sql = @"
SELECT A.SYSID AS Sysid,A.CREATED_ON AS CreatedOn,A.CREATED_BY AS CreatedBy,A.JOB_NO AS JobNo,A.CARTON_NO AS CartonNo
    ,A.CUSTOMER_CARTON_NO AS CustomerCartonNo,A.INNER_JOB_NO AS InnerJobNo,A.FINISHED_ON AS FinishedOn
    ,A.MODULE_COLOR AS ModuleColor,A.GROUP_HIST_KEY AS GroupHistKey,A.MODULE_SN AS ModuleSn,A.ORDER_NO AS OrderNo
    ,A.SALES_ORDER_NO AS SalesOrderNo,A.SALES_ITEM_NO AS SalesItemNo,A.ORDER_STATUS AS OrderStatus
    ,A.PRODUCT_CODE AS ProductCode,A.UNIT AS Unit,A.FACTORY AS Factory,A.WORKSHOP AS Workshop
    ,A.PACKING_LOCATION AS PackingLocation,A.PACKING_MODE AS PackingMode,A.CELL_EFF AS CellEff,A.TEST_POWER AS TestPower
    ,A.STD_POWER AS StdPower,A.MODULE_GRADE AS ModuleGrade,A.BY_IM AS ByIm,A.TOLERANCE AS Tolerance,A.CELL_CODE AS CellCode
    ,A.CELL_BATCH AS CellBatch,A.CELL_PRINT_MODE AS CellPrintMode,A.GLASS_CODE AS GlassCode,A.GLASS_BATCH AS GlassBatch
    ,A.EVA_CODE AS EvaCode,A.EVA_BATCH AS EvaBatch,A.TPT_CODE AS TptCode,A.TPT_BATCH AS TptBatch,A.CONBOX_CODE AS ConboxCode
    ,A.CONBOX_BATCH AS ConboxBatch,A.CONBOX_TYPE AS ConboxType,A.LONG_FRAME_CODE AS LongFrameCode
    ,A.LONG_FRAME_BATCH AS LongFrameBatch,A.SHORT_FRAME_CODE AS ShortFrameCode,A.SHORT_FRAME_BATCH AS ShortFrameBatch
    ,A.GLASS_THICKNESS AS GlassThickness,A.IS_REWORK AS IsRework,A.IS_BY_PRODUCTION AS IsByProduction
    ,A.UPLOAD_STATUS AS UploadStatus,A.RESV01 AS Resv01,A.RESV02 AS Resv02,A.RESV03 AS Resv03,A.RESV04 AS Resv04
    ,A.RESV05 AS Resv05,A.RESV06 AS Resv06,A.RESV07 AS Resv07,A.RESV08 AS Resv08,A.RESV09 AS Resv09,A.RESV10 AS Resv10
FROM TSAP_RECEIPT_UPLOAD_MODULE AS A WITH(NOLOCK) 
WHERE 1 = 1 
    AND A.CARTON_NO = @CartonNo 
    AND A.UPLOAD_STATUS = 'Ready' ";

            return Dapper.Query<TsapReceiptUploadModule>(sql, new { CartonNo = cartonNo }, connStr);
        }

        public static List<TsapReceiptUploadModule> QueryCancelInventoryModuleByCartonNo(string cartonNo, string connStr)
        {
            const string sql = @"
SELECT A.SYSID AS Sysid,A.CREATED_ON AS CreatedOn,A.CREATED_BY AS CreatedBy,A.JOB_NO AS JobNo,A.CARTON_NO AS CartonNo
    ,A.CUSTOMER_CARTON_NO AS CustomerCartonNo,A.INNER_JOB_NO AS InnerJobNo,A.FINISHED_ON AS FinishedOn
    ,A.MODULE_COLOR AS ModuleColor,A.GROUP_HIST_KEY AS GroupHistKey,A.MODULE_SN AS ModuleSn,A.ORDER_NO AS OrderNo
    ,A.SALES_ORDER_NO AS SalesOrderNo,A.SALES_ITEM_NO AS SalesItemNo,A.ORDER_STATUS AS OrderStatus
    ,A.PRODUCT_CODE AS ProductCode,A.UNIT AS Unit,A.FACTORY AS Factory,A.WORKSHOP AS Workshop
    ,A.PACKING_LOCATION AS PackingLocation,A.PACKING_MODE AS PackingMode,A.CELL_EFF AS CellEff,A.TEST_POWER AS TestPower
    ,A.STD_POWER AS StdPower,A.MODULE_GRADE AS ModuleGrade,A.BY_IM AS ByIm,A.TOLERANCE AS Tolerance,A.CELL_CODE AS CellCode
    ,A.CELL_BATCH AS CellBatch,A.CELL_PRINT_MODE AS CellPrintMode,A.GLASS_CODE AS GlassCode,A.GLASS_BATCH AS GlassBatch
    ,A.EVA_CODE AS EvaCode,A.EVA_BATCH AS EvaBatch,A.TPT_CODE AS TptCode,A.TPT_BATCH AS TptBatch,A.CONBOX_CODE AS ConboxCode
    ,A.CONBOX_BATCH AS ConboxBatch,A.CONBOX_TYPE AS ConboxType,A.LONG_FRAME_CODE AS LongFrameCode
    ,A.LONG_FRAME_BATCH AS LongFrameBatch,A.SHORT_FRAME_CODE AS ShortFrameCode,A.SHORT_FRAME_BATCH AS ShortFrameBatch
    ,A.GLASS_THICKNESS AS GlassThickness,A.IS_REWORK AS IsRework,A.IS_BY_PRODUCTION AS IsByProduction
    ,A.UPLOAD_STATUS AS UploadStatus,A.RESV01 AS Resv01,A.RESV02 AS Resv02,A.RESV03 AS Resv03,A.RESV04 AS Resv04
    ,A.RESV05 AS Resv05,A.RESV06 AS Resv06,A.RESV07 AS Resv07,A.RESV08 AS Resv08,A.RESV09 AS Resv09,A.RESV10 AS Resv10
FROM TSAP_RECEIPT_UPLOAD_MODULE AS A WITH(NOLOCK) 
WHERE 1 = 1 
    AND A.CARTON_NO = @CartonNo ";

            var list = Dapper.Query<TsapReceiptUploadModule>(sql, new { CartonNo = cartonNo }, connStr);
            var retList = new List<TsapReceiptUploadModule>();
            if (list != null && list.Count > 0)
            {
                foreach (var tsapReceiptUploadModule in list)
                {
                    var find = retList.Find(p => p.ModuleSn == tsapReceiptUploadModule.ModuleSn);
                    //依次添加到列表中
                    if (find == null)
                    {
                        retList.Add(tsapReceiptUploadModule);
                        continue;
                    }
                    //如果当前数据比列表数据旧，忽略
                    if (DateTime.Parse(find.CreatedOn) > DateTime.Parse(tsapReceiptUploadModule.CreatedOn))
                        continue;
                    //否则，移除列表中找到的记录，添加当前记录
                    retList.Remove(find);
                    retList.Add(tsapReceiptUploadModule);
                }
            }
            return retList;
        }

        public static string QueryEsbInterfaceAddress(string workshop, string connStr)
        {
            const string sql = @"
SELECT A.SYSID AS Sysid,A.FUNCTION_CODE AS FunctionCode,A.MAPPING_KEY_01 AS MappingKey01
    ,A.MAPPING_KEY_02 AS MappingKey02,A.MAPPING_KEY_03 AS MappingKey03,A.MAPPING_KEY_04 AS MappingKey04
    ,A.MAPPING_KEY_05 AS MappingKey05,A.MAPPING_KEY_06 AS MappingKey06,A.MAPPING_KEY_07 AS MappingKey07
    ,A.MAPPING_KEY_08 AS MappingKey08,A.MAPPING_KEY_09 AS MappingKey09,A.CREATED_BY AS CreatedBy
    ,A.CREATED_ON AS CreatedOn,A.MODIFIED_BY AS ModifiedBy,A.MODIFIED_ON AS ModifiedOn,A.STATUS AS Status
    ,A.RESV01 AS Resv01,A.RESV02 AS Resv02,A.RESV03 AS Resv03,A.RESV04 AS Resv04,A.RESV05 AS Resv05
    ,A.RESV06 AS Resv06,A.RESV07 AS Resv07,A.RESV08 AS Resv08,A.RESV09 AS Resv09,A.RESV10 AS Resv10
FROM T_SYS_MAPPING AS A WITH(NOLOCK) 
WHERE 1 = 1 
    AND A.FUNCTION_CODE = 'PackingSystemEsbInterfaceAddress' 
    AND A.MAPPING_KEY_01 = @Workshop 
ORDER BY A.SYSID ";

            var list = Dapper.Query<SysMapping>(sql, new { Workshop = workshop }, connStr);
            if (list == null || list.Count <= 0)
                return string.Empty;
            var mapping = list[0];
            return mapping.MappingKey02;
        }
         public static List<string> QueryEsbInterfaceAddressUserIdPW(string workshop, string connStr)
        {
            const string sql = @"
SELECT A.SYSID AS Sysid,A.FUNCTION_CODE AS FunctionCode,A.MAPPING_KEY_01 AS MappingKey01
    ,A.MAPPING_KEY_02 AS MappingKey02,A.MAPPING_KEY_03 AS MappingKey03,A.MAPPING_KEY_04 AS MappingKey04
    ,A.MAPPING_KEY_05 AS MappingKey05,A.MAPPING_KEY_06 AS MappingKey06,A.MAPPING_KEY_07 AS MappingKey07
    ,A.MAPPING_KEY_08 AS MappingKey08,A.MAPPING_KEY_09 AS MappingKey09,A.CREATED_BY AS CreatedBy
    ,A.CREATED_ON AS CreatedOn,A.MODIFIED_BY AS ModifiedBy,A.MODIFIED_ON AS ModifiedOn,A.STATUS AS Status
    ,A.RESV01 AS Resv01,A.RESV02 AS Resv02,A.RESV03 AS Resv03,A.RESV04 AS Resv04,A.RESV05 AS Resv05
    ,A.RESV06 AS Resv06,A.RESV07 AS Resv07,A.RESV08 AS Resv08,A.RESV09 AS Resv09,A.RESV10 AS Resv10
FROM T_SYS_MAPPING AS A WITH(NOLOCK) 
WHERE 1 = 1 
    AND A.FUNCTION_CODE = 'PackingSystemEsbInterfaceAddress' 
    AND A.MAPPING_KEY_01 = @Workshop 
ORDER BY A.SYSID ";

           var list = Dapper.Query<SysMapping>(sql, new { Workshop = workshop }, connStr);
            if (list == null || list.Count <= 0)
                return null;
            var mapping = list[0];
            
            List<string> T = new List<string>();
            T.Add(mapping.MappingKey02);
            T.Add(mapping.MappingKey04);
            T.Add(mapping.MappingKey05);
            T.Add(mapping.MappingKey06);
            
            return T;
        }

        public static bool UpdateTsapReceiptUploadModule(TsapReceiptUploadModule tsapReceiptUploadModule, SqlConnection conn, SqlTransaction trans)
        {
            const string sql = @"
UPDATE TSAP_RECEIPT_UPLOAD_MODULE
SET CREATED_BY=@CreatedBy,JOB_NO=@JobNo,CARTON_NO=@CartonNo,CUSTOMER_CARTON_NO=@CustomerCartonNo,INNER_JOB_NO=@InnerJobNo,FINISHED_ON=@FinishedOn,MODULE_COLOR=@ModuleColor,GROUP_HIST_KEY=@GroupHistKey,MODULE_SN=@ModuleSn,ORDER_NO=@OrderNo,SALES_ORDER_NO=@SalesOrderNo,SALES_ITEM_NO=@SalesItemNo,ORDER_STATUS=@OrderStatus,PRODUCT_CODE=@ProductCode,UNIT=@Unit,FACTORY=@Factory,WORKSHOP=@Workshop,PACKING_LOCATION=@PackingLocation,PACKING_MODE=@PackingMode,CELL_EFF=@CellEff,TEST_POWER=@TestPower,STD_POWER=@StdPower,MODULE_GRADE=@ModuleGrade,BY_IM=@ByIm,TOLERANCE=@Tolerance,CELL_CODE=@CellCode,CELL_BATCH=@CellBatch,CELL_PRINT_MODE=@CellPrintMode,GLASS_CODE=@GlassCode,GLASS_BATCH=@GlassBatch,EVA_CODE=@EvaCode,EVA_BATCH=@EvaBatch,TPT_CODE=@TptCode,TPT_BATCH=@TptBatch,CONBOX_CODE=@ConboxCode,CONBOX_BATCH=@ConboxBatch,CONBOX_TYPE=@ConboxType,LONG_FRAME_CODE=@LongFrameCode,LONG_FRAME_BATCH=@LongFrameBatch,SHORT_FRAME_CODE=@ShortFrameCode,SHORT_FRAME_BATCH=@ShortFrameBatch,GLASS_THICKNESS=@GlassThickness,IS_REWORK=@IsRework,IS_BY_PRODUCTION=@IsByProduction,UPLOAD_STATUS=@UploadStatus,RESV01=@Resv01,RESV02=@Resv02,RESV03=@Resv03,RESV04=@Resv04,RESV05=@Resv05,RESV06=@Resv06,RESV07=@Resv07,RESV08=@Resv08,RESV09=@Resv09,RESV10=@Resv10
WHERE SYSID=@Sysid

";

            return Dapper.Save(tsapReceiptUploadModule, sql, conn, trans);
        }

        public static bool InsertTsapReceiptUploadJobNo(TsapReceiptUploadJobNo tsapReceiptUploadJobNo, SqlConnection conn, SqlTransaction trans)
        {
            const string sql = @"
INSERT INTO TSAP_RECEIPT_UPLOAD_JOB_NO(SYSID,CREATED_ON,CREATED_BY,JOB_NO,INNER_JOB_NO,GROUP_HIST_KEY,RESV01,RESV02,RESV03,RESV04,RESV05,RESV06,RESV07,RESV08,RESV09,RESV10)
VALUES(@Sysid,CONVERT(NVARCHAR(50),GETDATE(),121),@CreatedBy,@JobNo,@InnerJobNo,@GroupHistKey,@Resv01,@Resv02,@Resv03,@Resv04,@Resv05,@Resv06,@Resv07,@Resv08,@Resv09,@Resv10)

";

            return Dapper.Save(tsapReceiptUploadJobNo, sql, conn, trans);
        }

        public static bool InsertTsapReceiptUploadJobNo(TsapReceiptUploadJobNo tsapReceiptUploadJobNo, string connStr)
        {
            const string sql = @"
INSERT INTO TSAP_RECEIPT_UPLOAD_JOB_NO(SYSID,CREATED_ON,CREATED_BY,JOB_NO,INNER_JOB_NO,GROUP_HIST_KEY,RESV01,RESV02,RESV03,RESV04,RESV05,RESV06,RESV07,RESV08,RESV09,RESV10)
VALUES(@Sysid,CONVERT(NVARCHAR(50),GETDATE(),121),@CreatedBy,@JobNo,@InnerJobNo,@GroupHistKey,@Resv01,@Resv02,@Resv03,@Resv04,@Resv05,@Resv06,@Resv07,@Resv08,@Resv09,@Resv10)

";

            return Dapper.Save(tsapReceiptUploadJobNo, sql, connStr);
        }

        public static bool QueryReworkOrgBatch(List<TsapReceiptUploadModule> tsapReceiptUploadModules, out string msg)
        {
            //组件在指定工单是否已经入库过
            var checkModuleResults = QueryMutiWorkOrderModuleSns(tsapReceiptUploadModules);
            if (checkModuleResults != null && checkModuleResults.Count > 0)
            {
                msg = "以下组件在指定工单已进行过入库，不可重复入库：";
                var retMsg = string.Empty;
                checkModuleResults.ForEach(p => retMsg += "\r\n组件序列号：" + p.ModuleSn + "，工单：" + p.OrderNo);
                msg += retMsg;
                return false;
            }
            msg = string.Empty;
            if (tsapReceiptUploadModules == null || tsapReceiptUploadModules.Count <= 0)
            {
                msg = "待处理列表为空";
                return false;
            }
            var findAll = tsapReceiptUploadModules.FindAll(p => p.IsRework == "Y");
            if (findAll.Count <= 0)
                return true;
            //marked by dandan.shi 20151223
            //var orderNos = findAll.Select(p => p.OrderNo).Distinct().ToArray();
            //var workOrderDatas = QueryWorkOrderData(orderNos);
            //if (workOrderDatas != null && workOrderDatas.Count > 0)
            //{
            //    foreach (var workOrderData in workOrderDatas)
            //    {
            //        WorkOrderData data = workOrderData;
            //        var findModules = findAll.FindAll(p => p.OrderNo == data.OrderNo);
            //        if (findModules.Count <= 0)
            //            continue;
            //        findModules.ForEach(p =>
            //        {
            //            p.ByIm = workOrderData.ByIm;
            //            p.Tolerance = workOrderData.Tolerance;
            //        });
            //    }
            //}
            if (IsQueryReworkDataFromEsb(FormCover.CurrentFactory.Trim(), FormCover.InterfaceConnString))
                return QueryReworkOrgBatchInEsb(findAll, out msg);

            var moduleSns = findAll.Select(p => p.ModuleSn).Distinct().ToArray();
            var moduleSnArray = string.Join("','", moduleSns);
            var sql = @"
SELECT A.SYSID AS Sysid,A.CREATED_ON AS CreatedOn,A.CREATED_BY AS CreatedBy,A.JOB_NO AS JobNo,A.CARTON_NO AS CartonNo
    ,A.CUSTOMER_CARTON_NO AS CustomerCartonNo,A.INNER_JOB_NO AS InnerJobNo,A.FINISHED_ON AS FinishedOn
    ,A.MODULE_COLOR AS ModuleColor,A.GROUP_HIST_KEY AS GroupHistKey,A.MODULE_SN AS ModuleSn,A.ORDER_NO AS OrderNo
    ,A.SALES_ORDER_NO AS SalesOrderNo,A.SALES_ITEM_NO AS SalesItemNo,A.ORDER_STATUS AS OrderStatus
    ,A.PRODUCT_CODE AS ProductCode,A.UNIT AS Unit,A.FACTORY AS Factory,A.WORKSHOP AS Workshop
    ,A.PACKING_LOCATION AS PackingLocation,A.PACKING_MODE AS PackingMode,A.CELL_EFF AS CellEff,A.TEST_POWER AS TestPower
    ,A.STD_POWER AS StdPower,A.MODULE_GRADE AS ModuleGrade,A.BY_IM AS ByIm,A.TOLERANCE AS Tolerance,A.CELL_CODE AS CellCode
    ,A.CELL_BATCH AS CellBatch,A.CELL_PRINT_MODE AS CellPrintMode,A.GLASS_CODE AS GlassCode,A.GLASS_BATCH AS GlassBatch
    ,A.EVA_CODE AS EvaCode,A.EVA_BATCH AS EvaBatch,A.TPT_CODE AS TptCode,A.TPT_BATCH AS TptBatch,A.CONBOX_CODE AS ConboxCode
    ,A.CONBOX_BATCH AS ConboxBatch,A.CONBOX_TYPE AS ConboxType,A.LONG_FRAME_CODE AS LongFrameCode
    ,A.LONG_FRAME_BATCH AS LongFrameBatch,A.SHORT_FRAME_CODE AS ShortFrameCode,A.SHORT_FRAME_BATCH AS ShortFrameBatch
    ,A.GLASS_THICKNESS AS GlassThickness,A.IS_REWORK AS IsRework,A.IS_BY_PRODUCTION AS IsByProduction
    ,A.UPLOAD_STATUS AS UploadStatus,A.RESV01 AS Resv01,A.RESV02 AS Resv02,A.RESV03 AS Resv03,A.RESV04 AS Resv04
    ,A.RESV05 AS Resv05,A.RESV06 AS Resv06,A.RESV07 AS Resv07,A.RESV08 AS Resv08,A.RESV09 AS Resv09,A.RESV10 AS Resv10  
FROM  
	(
		SELECT  NO = ROW_NUMBER() OVER (PARTITION BY MODULE_SN ORDER BY CREATED_ON DESC),* 
		FROM TSAP_RECEIPT_UPLOAD_MODULE WITH(NOLOCK) 
		  WHERE MODULE_SN IN 
		  (
		  '{0}'
		  )
		  AND CELL_CODE IS NOT NULL 
		  AND CELL_CODE <> '' 
	)A
WHERE NO<2 
ORDER BY MODULE_SN ";
            sql = string.Format(sql, moduleSnArray);
            var checkModules = Dapper.Query<TsapReceiptUploadModule>(sql, null, FormCover.InterfaceConnString);
            if (checkModules == null || checkModules.Count <= 0)
            {
                msg = "返工组件的原始批次信息获取失败";
                return false;
            }
            foreach (var checkModule in checkModules)
            {
                var findModule = findAll.Find(p => p.ModuleSn == checkModule.ModuleSn);
                if (findModule == null)
                    continue;
                findModule.CellCode = checkModule.CellCode;
                findModule.CellBatch = checkModule.CellBatch;
                findModule.GlassCode = checkModule.GlassCode;
                findModule.GlassBatch = checkModule.GlassBatch;
                findModule.EvaCode = checkModule.EvaCode;
                findModule.EvaBatch = checkModule.EvaBatch;
                findModule.TptCode = checkModule.TptCode;
                findModule.TptBatch = checkModule.TptBatch;
                findModule.ConboxCode = checkModule.ConboxCode;
                findModule.ConboxBatch = checkModule.ConboxBatch;
                findModule.LongFrameCode = checkModule.LongFrameCode;
                findModule.LongFrameBatch = checkModule.LongFrameBatch;
                findModule.ShortFrameCode = checkModule.ShortFrameCode;
                findModule.ShortFrameBatch = checkModule.ShortFrameBatch;
 

            }
            return true;
        }

        public static List<WorkOrderData> QueryWorkOrderData(string[] orderNos)
        {
            var sql = @"
SELECT DISTINCT WO.TWO_NO OrderNo,WO.RESV03 ByIm,WOA.VALUE18 Tolerance
FROM T_WORK_ORDERS WO WITH(NOLOCK) 
	INNER JOIN T_WORKORDER_ATTRIBUTE WOA WITH(NOLOCK) ON 
		WO.TWO_NO = WOA.ORDER_NO 
WHERE WO.TWO_NO IN ('{0}') 
ORDER BY WO.TWO_NO ";
            sql = string.Format(sql, string.Join("','", orderNos));

            return Dapper.Query<WorkOrderData>(sql, null, FormCover.InterfaceConnString);
        }

        public static List<CheckModuleResult> QueryMutiWorkOrderModuleSns(List<TsapReceiptUploadModule> tsapReceiptUploadModules)
        {
            if (tsapReceiptUploadModules == null || tsapReceiptUploadModules.Count <= 0)
                return new List<CheckModuleResult>();
            var sql = @"
SELECT DISTINCT ModuleSn,OrderNo 
FROM 
(
 {0} 
) AS A ";
            const string sqlFormat = @"SELECT DISTINCT MODULE_SN ModuleSn,ORDER_NO OrderNo FROM TSAP_RECEIPT_UPLOAD_MODULE WITH(NOLOCK) WHERE MODULE_SN = '{0}' AND ORDER_NO = '{1}' AND UPLOAD_STATUS <> 'Canceled' ";
            var builder = new StringBuilder();
            var i = 0;
            foreach (var tsapReceiptUploadModule in tsapReceiptUploadModules)
            {
                builder.Append(string.Format(sqlFormat, tsapReceiptUploadModule.ModuleSn,
                    tsapReceiptUploadModule.OrderNo));
                i++;
                if (i < tsapReceiptUploadModules.Count)
                    builder.Append(" UNION ALL ");
                builder.AppendLine();
            }
            sql = string.Format(sql, builder);
            ToolsClass.Log(sql);
            return Dapper.Query<CheckModuleResult>(sql, null, FormCover.connectionBase);
        }

        private static bool QueryReworkOrgBatchInEsb(List<TsapReceiptUploadModule> tsapReceiptUploadModules,
            out string msg)
        {
            msg = string.Empty;
            var moduleSns = tsapReceiptUploadModules.Select(p => p.ModuleSn).Distinct().ToArray();
            var checkModules = QueryBizModuleMaterials(moduleSns);
            if (checkModules == null || checkModules.Count <= 0)
            {
                msg = "返工组件的原始批次信息获取失败";
                return false;
            }
            foreach (var checkModule in checkModules)
            {
                var findModule = tsapReceiptUploadModules.Find(p => p.ModuleSn == checkModule.ModuleSn);
                if (findModule == null)
                    continue;
                findModule.CellCode = checkModule.CellCode;
                findModule.CellBatch = checkModule.CellBatch;
                findModule.GlassCode = checkModule.GlassCode;
                findModule.GlassBatch = checkModule.GlassBatch;
                findModule.EvaCode = checkModule.EvaCode;
                findModule.EvaBatch = checkModule.EvaBatch;
                findModule.TptCode = checkModule.TptCode;
                findModule.TptBatch = checkModule.TptBatch;
                findModule.ConboxCode = checkModule.ConboxCode;
                findModule.ConboxBatch = checkModule.ConboxBatch;
                findModule.LongFrameCode = checkModule.LongFrameCode;
                findModule.LongFrameBatch = checkModule.LongFrameBatch;
                findModule.ShortFrameCode = checkModule.ShortFrameCode;
                findModule.ShortFrameBatch = checkModule.ShortFrameBatch;
            }
            return true;
        }

        public static string QueryEsbSql(string functionCode)
        {
            const string sql = @"
SELECT ISNULL(A.MAPPING_KEY_01,'') AS MappingKey01
FROM T_SYS_MAPPING AS A WITH(NOLOCK) 
WHERE 1 = 1 
    AND A.FUNCTION_CODE = @FunctionCode
ORDER BY A.SYSID ";

            var list = Dapper.Query<string>(sql, new { FunctionCode = functionCode }, FormCover.InterfaceConnString);
            if (list == null || list.Count <= 0)
                return string.Empty;
            return list[0];
        }

        private static List<BizModuleMaterial> QueryBizModuleMaterials(string[] moduleSns)
        {
            var sql = QueryEsbSql("QueryBizModuleMaterialByModuleSnArray");
            if (string.IsNullOrEmpty(sql))
                return new List<BizModuleMaterial>();
            var moduleSnArray = string.Join("','", moduleSns);
            sql = string.Format(sql, moduleSnArray);

            var list = Dapper.QueryInMySql<BizModuleMaterial>(sql, null, FormCover.EsbConnString);
            var retList = new List<BizModuleMaterial>();
            if (list != null && list.Count > 0)
            {
                foreach (var bizModuleMaterial in list)
                {
                    var find = retList.Find(p => p.ModuleSn == bizModuleMaterial.ModuleSn);
                    //依次添加到列表中
                    if (find == null)
                    {
                        retList.Add(bizModuleMaterial);
                        continue;
                    }
                    //如果当前数据比列表数据旧，忽略
                    if (find.CreateOn > bizModuleMaterial.CreateOn)
                        continue;
                    //否则，移除列表中找到的记录，添加当前记录
                    retList.Remove(find);
                    retList.Add(bizModuleMaterial);
                }
            }
            return retList;
        }

        public static List<BizModule> QueryBizModuleByCartonNo(string cartonNo)
        {
            //var sql = QueryEsbSql("QueryBizModuleByCartonNo");
            var sql = @"  SELECT Null AS ID,SN AS ModuleSn,Null AS JobNo,Null AS InnerJobNo,BoxID AS CartonNo 
  ,Null AS CustomerCartonNo,Null AS ProductCode,Null AS BatchNo  
  ,Null AS Voucher,Null AS OrderNo,Null AS SalesOrderNo,Null AS SalesItemNo 
  ,Null AS Factory,Null AS Workshop,Null AS InvGuid,Null AS InvDate
  ,Null AS InvRecentGuid,Null AS InvRecentDate,Null AS IsRework 
  ,Null AS IsByProduction,Null AS IsScrap,Null AS IsExpire,Null AS StdPower  
  ,Null AS TestPower,Null AS PackingLocation,Null AS Unit,Null AS ActionType 
  ,Null AS ActionId,Null AS ActionDate,Null AS ModuleType,Null AS Fromsys 
  FROM Product  WHERE 1 = 1       AND Boxid= '{0}'";

            if (string.IsNullOrEmpty(sql))
                return new List<BizModule>();
            sql = string.Format(sql, cartonNo);
            //return Dapper.QueryInMySql<BizModule>(sql, null, FormCover.EsbConnString);
           	//string con = "Data Source= 10.127.34.15; Initial Catalog= labelprintdb;User ID=fastengine;Password=Csi456";
            string con ="Data Source=10.127.34.15;" +
			"Initial Catalog=LabelPrintDB;" +
			"User id=fastengine;" +
			"Password=Csi456;";            
           	return Dapper.QueryInSql<BizModule>(sql, null, con);
        }

        public static List<BizModule> QueryBizModuleByModuleSn(string moduleSn)
        {
        	var sql = @"  SELECT Null AS ID,SN AS ModuleSn,Null AS JobNo,Null AS InnerJobNo,BoxID AS CartonNo 
  ,Null AS CustomerCartonNo,Null AS ProductCode,Null AS BatchNo  
  ,Null AS Voucher,Null AS OrderNo,Null AS SalesOrderNo,Null AS SalesItemNo 
  ,Null AS Factory,Null AS Workshop,Null AS InvGuid,Null AS InvDate
  ,Null AS InvRecentGuid,Null AS InvRecentDate,Null AS IsRework 
  ,Null AS IsByProduction,Null AS IsScrap,Null AS IsExpire,Null AS StdPower  
  ,Null AS TestPower,Null AS PackingLocation,Null AS Unit,Null AS ActionType 
  ,Null AS ActionId,Null AS ActionDate,Null AS ModuleType,Null AS Fromsys 
  FROM Product  WHERE 1 = 1       AND SN= '{0}'";
            //var sql = QueryEsbSql("QueryBizModuleByModuleSn");
            if (string.IsNullOrEmpty(sql))
                return new List<BizModule>();
            sql = string.Format(sql, moduleSn);
             string con ="Data Source=10.127.34.15;" +
			"Initial Catalog=LabelPrintDB;" +
			"User id=fastengine;" +
			"Password=Csi456;";            
			return Dapper.QueryInSql<BizModule>(sql, null, con);             
            //return Dapper.QueryInMySql<BizModule>(sql, null, FormCover.EsbConnString);
        }

        public static DataTable QuerySapBatchDataByJobNo(string jobNo, string connStr)
        {
            var sql = QueryEsbSql("QuerySapBatchDataByJobNo");
            if (string.IsNullOrEmpty(sql))
                return null;
            sql = string.Format(sql, jobNo);

            var ds = MySqlHelper.ExecuteDataset(connStr, sql);
            if (ds == null || ds.Tables.Count <= 0)
                return null;
            return ds.Tables[0];
        }

        public static DataTable QuerySapBatchDataByCartonNos(string cartonNos, string connStr)
        {
            var sql = QueryEsbSql("QuerySapBatchDataByCartonNos");
            if (string.IsNullOrEmpty(sql))
                return null;
            sql = string.Format(sql, cartonNos);

            var ds = MySqlHelper.ExecuteDataset(connStr, sql);
            if (ds == null || ds.Tables.Count <= 0)
                return null;
            return ds.Tables[0];
        }

        public static DataTable QuerySapBatchDataByModuleSns(string moduleSns, string connStr)
        {
            var sql = QueryEsbSql("QuerySapBatchDataByModuleSns");
            if (string.IsNullOrEmpty(sql))
                return null;
            sql = string.Format(sql, moduleSns);

            var ds = MySqlHelper.ExecuteDataset(connStr, sql);
            if (ds == null || ds.Tables.Count <= 0)
                return null;
            return ds.Tables[0];
        }

        public static bool InsertRegModuleRework(RegModuleRework regModuleRework, SqlConnection conn, SqlTransaction trans)
        {
            const string sql = @"
INSERT INTO T_REG_MODULE_REWORK(SYSID,MODULE_SN,WORK_ORDER,CREATED_BY,CREATED_ON,RESV01,RESV02,RESV03,RESV04,RESV05,RESV06,RESV07,RESV08,RESV09,RESV10)
VALUES(@Sysid,@ModuleSn,@WorkOrder,@CreatedBy,CONVERT(NVARCHAR(50),GETDATE(),121),@Resv01,@Resv02,@Resv03,@Resv04,@Resv05,@Resv06,@Resv07,@Resv08,@Resv09,@Resv10)

";

            return Dapper.Save(regModuleRework, sql, conn, trans);
        }
    }

    public class WorkOrderData
    {
        public string OrderNo { get; set; }
        public string ByIm { get; set; }
        public string Tolerance { get; set; }
    }

    public class CheckModuleResult
    {
        public string ModuleSn { get; set; }
        public string OrderNo { get; set; }
    }
}