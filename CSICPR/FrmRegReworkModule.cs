﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Collections.Generic;
using CSICPR.Properties;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FrmRegReworkModule : Form
    {
        public FrmRegReworkModule()
        {
            InitializeComponent();
        }
        private static List<LanguageItemModel> LanMessList;//定义语言集
        #region 共有变量
        private static FrmRegReworkModule _theSingleton;
        public static void Instance(Form fm)
        {
            if (null == _theSingleton || _theSingleton.IsDisposed)
            {
                _theSingleton = new FrmRegReworkModule
                {
                    MdiParent = fm,
                    WindowState = FormWindowState.Maximized
                };
                _theSingleton.Show();
            }
            else
            {
                _theSingleton.Activate();
                if (_theSingleton.WindowState == FormWindowState.Minimized)
                    _theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        private List<BizModule> _bizModules = new List<BizModule>();

        private void FrmRegReworkModule_Load(object sender, EventArgs e)
        {
               # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            LanguageHelper.GetCombomBox(this, this.cmbType);
            # endregion
            lstView.Columns.Add(LanguageHelper.GetMessage(LanMessList, "Message1","提示信息"), 630, HorizontalAlignment.Left);

            dgvData.AutoGenerateColumns = false;

            cmbType.Items.Clear();
            cmbType.Items.Add(LanguageHelper.GetMessage(LanMessList, "Message2","根据托号注册"));
            cmbType.Items.Add(LanguageHelper.GetMessage(LanMessList, "Message3", "根据组件序列号注册"));
            cmbType.SelectedIndex = 0;

          
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            if (_bizModules == null)
                _bizModules = new List<BizModule>();
            lblModuleCntValue.Text = _bizModules.Select(p => p.ModuleSn).Distinct().Count().ToString(CultureInfo.InvariantCulture);
            if (cmbType.SelectedIndex == 0)
                QueryByCartonNo();
            if (cmbType.SelectedIndex == 1)
                QueryByModuleSn();
        }

        private void QueryByCartonNo()
        {
            var tempCartonNos = txtCartons.Lines;
            if (tempCartonNos.Length <= 0)
            {
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message4", "请输入托号"), "ABNORMAL", lstView);
                txtCartons.Clear();
                txtCartons.Focus();
                return;
            }
            var cartonNos = tempCartonNos.Where(cartonNo => !string.IsNullOrEmpty(cartonNo.Trim())).ToList();
            foreach (var cartonNo in cartonNos)
            {
                if (!CheckRepeatByCartonNo(cartonNo))
                    continue;
//                if(!IsInESB(cartonNo))
//                	continue;
                var bizModules = DataAccess.QueryBizModuleByCartonNo(cartonNo);
                if (bizModules != null && bizModules.Count > 0)
                {
                    _bizModules.AddRange(bizModules);
                    dgvData.DataSource = null;
                    dgvData.DataSource = _bizModules;
                    lblModuleCntValue.Text =
                        _bizModules.Select(p => p.ModuleSn).Distinct().Count().ToString(CultureInfo.InvariantCulture);
                }
            }
            txtCartons.Clear();
            txtCartons.Focus();
        }
        
        #region check to show
        private bool CheckToShow(string cartonNo)
        {
        	 if(!IsInESB(cartonNo))
        	 {
        	 	return IsRegistered(cartonNo);
        	 }
        	 else
        	 {
        	 	return IsCurrentMonth(cartonNo);
        	 }
        }
        private bool IsCurrentMonth(string cartonNo)
        {
        	string M = System.DateTime.Now.Month.ToString("MM");
        	string sql = @"select SN from product where boxid ='"+ cartonNo +"' and SN like '316"+ M +"%'";
        	SqlDataReader rd  = ToolsClass.GetDataReader(sql);
        	if(rd!=null && rd.Read())
        	{
        		return false;
        	}
        	else
        	{
        		return IsRegistered(cartonNo);
        	}
        }
        private bool IsRegistered(string cartonNo)
        {
        	string Reworkorder = "";
        	bool IsReg = false;
        	//do something
        	string[] SN = new string[26];
        	string sql = @"select SN from product where boxid ='"+ cartonNo +"'";
        	SqlDataReader rd  = ToolsClass.GetDataReader(sql);
        	if(rd!=null && rd.Read())
        	{
        		int i = 0;
        		while (rd.Read()) {
        			SN[i] = rd.GetString(0);
        			i++;
        		}
        	}
        	rd.Close();
        	if(string.IsNullOrEmpty(SN[0]))
        		return false;
        	else
        	{
        		sql = @"select WORK_ORDER from T_REG_MODULE_REWORK where SN in ('{0}')";
        		sql = string.Format(sql,string.Join("','",SN));        		
        		rd  = ToolsClass.GetDataReader(sql);
        		if(rd.Read() && rd != null)
        		{
        			IsReg = true;
        			Reworkorder = rd.GetString(0);
        		}
        		
        	}
        		
        	if(IsReg)
        	{
        		return NeedToRegister(Reworkorder);
        	}
        	else
        	{
        		return true;
        	}
        	
        }
        
        private bool NeedToRegister(string Reworkorder)
        {
        	DialogResult dialogResult = MessageBox.Show("This Carton has been registered under rework order: " +Reworkorder+ " do you still want to register?", "Attention", MessageBoxButtons.YesNo);
			if(dialogResult == DialogResult.Yes)
			{
			   return true;
			}
			else
			{
			   return false;
			}
        }
        
        
        #endregion
        private void QueryByModuleSn()
        {
            var tempModuleSns = txtModuleSns.Lines;            
            if (tempModuleSns.Length <= 0)
            {
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message5", "请输入组件序列号"), "ABNORMAL", lstView);
                txtModuleSns.Clear();
                txtModuleSns.Focus();
                return;
            }
            var moduleSns = tempModuleSns.Where(moduleSn => !string.IsNullOrEmpty(moduleSn.Trim())).ToList();
            foreach (var moduleSn in moduleSns)
            {
                if (!CheckRepeatByModuleSn(moduleSn))
                    continue;
                var bizModules = DataAccess.QueryBizModuleByModuleSn(moduleSn);
                if (bizModules != null && bizModules.Count > 0)
                {
                    _bizModules.AddRange(bizModules);
                    dgvData.DataSource = null;
                    dgvData.DataSource = _bizModules;
                    lblModuleCntValue.Text =
                        _bizModules.Select(p => p.ModuleSn).Distinct().Count().ToString(CultureInfo.InvariantCulture);
                }
            }
            txtModuleSns.Clear();
            txtModuleSns.Focus();
        }
		
        private bool IsInESB(string cartonno)
        {
        	List<string> T = DataAccess.QueryEsbInterfaceAddressUserIdPW(FormCover.CurrentFactory, FormCover.InterfaceConnString);
        	string host = T[0].Substring(7,12);
        	string user = T[1];
        	string password = T[2];
        	string dbcon = T[3];
        	        	
        	
        		string sql = " Select * from stockin where Carton_no = '{0}' and  process_flag = '3' and status = 'S'";
	               sql = string.Format(sql,cartonno);
	                    	 
					MySqlConnection con = new MySqlConnection("host="+ host +";user= "+ user +";password="+password+";database="+dbcon+";");
					MySqlCommand cmd = new MySqlCommand(sql, con);
					
					con.Open();
					
					MySqlDataReader  reader = cmd.ExecuteReader();
					if(reader!= null && reader.Read())
						return true;
					con.Close();
					return false;                
                	
        }
        private bool CheckRepeatByCartonNo(string cartonNo)
        {
            foreach (DataGridViewRow dataRow in dgvData.Rows)
            {
                var bizModule = dataRow.DataBoundItem as BizModule;
                if (bizModule == null)
                    continue;
                if (bizModule.CartonNo.Equals(cartonNo))
                {
                    ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message6", "托号：{0}已在列表第{1}行"), cartonNo, (dataRow.Index + 1)), "ABNORMAL", lstView);
                    return false;
                }
            }
            return true;
        }

        private bool CheckRepeatByModuleSn(string moduleSn)
        {
            foreach (DataGridViewRow dataRow in dgvData.Rows)
            {
                var bizModule = dataRow.DataBoundItem as BizModule;
                if (bizModule == null)
                    continue;
                if (bizModule.ModuleSn.Equals(moduleSn))
                {
                    ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message7", "组件序列号：{0}已在列表第{1}行"), moduleSn, (dataRow.Index + 1)), "ABNORMAL", lstView);
                    return false;
                }
            }
            return true;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            _bizModules = new List<BizModule>();
            lblModuleCntValue.Text = _bizModules.Select(p => p.ModuleSn).Distinct().Count().ToString(CultureInfo.InvariantCulture);
            dgvData.DataSource = null;
            dgvData.DataSource = _bizModules;
            cmbType.SelectedIndex = 0;
        }

        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (!e.Control || e.KeyCode != Keys.C)
                return;
            if (lstView.SelectedItems.Count <= 0)
                return;
            if (lstView.SelectedItems[0].Text != "")
                Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
        }

        private void dgvData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            BizModule bizModule;
            string msg;
            var columnIndex = e.ColumnIndex;
            if (columnIndex == 0)
            {
                bizModule = dgvData.Rows[e.RowIndex].DataBoundItem as BizModule;
                if (bizModule == null)
                    return;
                msg = string.Format(LanguageHelper.GetMessage(LanMessList, "Message8","确定要移除组件序列号{0}的数据吗？"), bizModule.ModuleSn);
                if (DialogResult.No == MessageBox.Show(msg, Resources.FrmRegReworkModule_dgvData_CellContentClick_Info, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    return;
                var findModuleSns = _bizModules.FindAll(p => p.ModuleSn == bizModule.ModuleSn);
                if (findModuleSns.Count <= 0)
                    return;
                findModuleSns.ForEach(p => _bizModules.Remove(p));
                dgvData.DataSource = null;
                dgvData.DataSource = _bizModules.Count <= 0 ? null : _bizModules;
                if (_bizModules == null)
                    _bizModules = new List<BizModule>();
                lblModuleCntValue.Text = _bizModules.Select(p => p.ModuleSn).Distinct().Count().ToString(CultureInfo.InvariantCulture);
                return;
            }
            if (columnIndex != 1)
                return;
            bizModule = dgvData.Rows[e.RowIndex].DataBoundItem as BizModule;
            if (bizModule == null)
                return;
            msg = string.Format(LanguageHelper.GetMessage(LanMessList, "Message9", "确定要移除内部托号{0}的数据吗？"), bizModule.CartonNo);
            if (DialogResult.No == MessageBox.Show(msg, Resources.FrmRegReworkModule_dgvData_CellContentClick_Info, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                return;
            var findAll = _bizModules.FindAll(p => p.CartonNo == bizModule.CartonNo);
            if (findAll.Count <= 0)
                return;
            findAll.ForEach(p => _bizModules.Remove(p));
            dgvData.DataSource = null;
            dgvData.DataSource = _bizModules.Count <= 0 ? null : _bizModules;
            if (_bizModules == null)
                _bizModules = new List<BizModule>();
            lblModuleCntValue.Text = _bizModules.Select(p => p.ModuleSn).Distinct().Count().ToString(CultureInfo.InvariantCulture);
        }

        private void cmbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbType.SelectedIndex < 0)
                return;
            pnlCartons.Visible = false;
            pnlModuleSns.Visible = false;
            txtCartons.Text = string.Empty;
            txtModuleSns.Text = string.Empty;
            if (cmbType.SelectedIndex == 0)
            {
                pnlCartons.Visible = true;
                pnlCartons.Focus();
                txtCartons.SelectAll();
                txtCartons.Focus();
            }
            if (cmbType.SelectedIndex == 1)
            {
                pnlModuleSns.Visible = true;
                pnlModuleSns.Focus();
                txtModuleSns.SelectAll();
                txtModuleSns.Focus();
            }
        }

        private void PicBoxWo_Click(object sender, EventArgs e)
        {
            var frm = new FrmSelectReworkOrder(txtWo.Text.Trim());
            frm.ShowDialog();
            txtWo.Text = frm.OrderNo;
        }

        private void txtWo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.F3)
                return;
            PicBoxWo_Click(null, null);
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            var orderNo = txtWo.Text.Trim();
            if (string.IsNullOrEmpty(orderNo))
            {
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message10","请输入重工工单号"), "ABNORMAL", lstView);
                return;
            }
            if (_bizModules == null || _bizModules.Count <= 0)
            {
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message11","请输入重工注册的组件序列号或者托号"), "ABNORMAL", lstView);
                return;
            }
            var moduleSns = _bizModules.Select(p => p.ModuleSn).Distinct().ToList();
            var orderQty = DataAccess.QueryOrderQty(orderNo, FormCover.InterfaceConnString);
            var moduleCnt = DataAccess.QueryModuleCntByReworkOrder(orderNo, FormCover.connectionBase);
            if (moduleSns.Count > orderQty)
            {
                ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message12","当前注册组件数量{0}不可大于重工工单{1}的计划数量{2}"), _bizModules.Count, orderNo, orderQty));
                return;
            }
            if (moduleCnt >= orderQty)
            {
            	MessageBox.Show(string.Format(LanguageHelper.GetMessage(LanMessList, "Message13","重工工单{0}的计划数量{1}，已注册组件数量{2}，不可继续进行组件重工注册"), orderNo, orderQty, moduleCnt));
                ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message13","重工工单{0}的计划数量{1}，已注册组件数量{2}，不可继续进行组件重工注册"), orderNo, orderQty, moduleCnt));
                return;
            }
            if (moduleCnt + moduleSns.Count > orderQty)
            {
                MessageBox.Show(string.Format(LanguageHelper.GetMessage(LanMessList, "Message14","重工工单{0}的计划数量{1}，已注册组件数量{2}，加上当前重工注册数量{3}后，将超过计划量，不可继续进行组件重工注册"), orderNo,
            	                              orderQty, moduleCnt,moduleSns.Count));
            	ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message14","重工工单{0}的计划数量{1}，已注册组件数量{2}，加上当前重工注册数量{3}后，将超过计划量，不可继续进行组件重工注册"), orderNo,
                    orderQty, moduleCnt, moduleSns.Count));
                return;
            }
            string msg;
           
            if (!PackinSystemg.PackinSystemgRework(orderNo, moduleSns, out msg))
            {
                MessageBox.Show(msg);
            }
            else
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message15", "确认成功"));
                btnReset_Click(null, null);
            }
        }
        
        
    }
}