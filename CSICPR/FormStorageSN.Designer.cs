﻿namespace CSICPR
{
    partial class FormStorageSN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormStorageSN));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CartonReset = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.GpbCartonQuery = new System.Windows.Forms.GroupBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.CartonQueryList = new System.Windows.Forms.Panel();
            this.txtCarton = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.CheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.RowIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SetFlag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ModuleSN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderNo1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SnStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CartonNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustomerCartonNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TestPower = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StdPower = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ModuleGrade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FinishedOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Factory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Workshop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PackingLocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CellEff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ByIm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CellCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CellBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CellPrintMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GlassCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GlassBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EvaCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EvaBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TptCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TptBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConBoxCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConBoxBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LongFrameCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LongFrameBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GlassThickness = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PackingMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShortFrameCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shortFrameBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsCancelPacking = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesOrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsOnlyPacking = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GpbWoQuery = new System.Windows.Forms.GroupBox();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.ddltxtAIFrameCode = new System.Windows.Forms.ComboBox();
            this.ddltxtShortAIFrameCode = new System.Windows.Forms.ComboBox();
            this.ddltxtConBoxCod = new System.Windows.Forms.ComboBox();
            this.ddltxtTPTCode = new System.Windows.Forms.ComboBox();
            this.ddltxtEVACode = new System.Windows.Forms.ComboBox();
            this.ddltxtGlassCode = new System.Windows.Forms.ComboBox();
            this.ddltxtCell = new System.Windows.Forms.ComboBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.txtWO1 = new System.Windows.Forms.TextBox();
            this.txtSalesItemNo = new System.Windows.Forms.TextBox();
            this.txtSalesOrderNo = new System.Windows.Forms.TextBox();
            this.PicBoxShortAIFrameBatch = new System.Windows.Forms.PictureBox();
            this.ddlShortAIFrameBatch = new System.Windows.Forms.ComboBox();
            this.lblShortAIFrameBatch = new System.Windows.Forms.Label();
            this.lblShortAIFrame = new System.Windows.Forms.Label();
            this.PicBoxCellBatch = new System.Windows.Forms.PictureBox();
            this.PicBoxAIFrameBatch = new System.Windows.Forms.PictureBox();
            this.PicBoxConBoxBatch = new System.Windows.Forms.PictureBox();
            this.PicBoxTPTBatch = new System.Windows.Forms.PictureBox();
            this.PicBoxEVABatch = new System.Windows.Forms.PictureBox();
            this.PicBoxGlassBatch = new System.Windows.Forms.PictureBox();
            this.ddlAIFrameBatch = new System.Windows.Forms.ComboBox();
            this.ddlConBoxBatch = new System.Windows.Forms.ComboBox();
            this.ddlTPTBatch = new System.Windows.Forms.ComboBox();
            this.ddlEVABatch = new System.Windows.Forms.ComboBox();
            this.ddlGlassBatch = new System.Windows.Forms.ComboBox();
            this.PicBoxWO = new System.Windows.Forms.PictureBox();
            this.Reset = new System.Windows.Forms.Button();
            this.lblGlassBatch = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtWo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblEVABatch = new System.Windows.Forms.Label();
            this.lblConBoxBatch = new System.Windows.Forms.Label();
            this.Set = new System.Windows.Forms.Button();
            this.lblTPTBatch = new System.Windows.Forms.Label();
            this.ddlWoType = new System.Windows.Forms.ComboBox();
            this.lblAIFrameBatch = new System.Windows.Forms.Label();
            this.txtWoOrder = new System.Windows.Forms.TextBox();
            this.LblWo = new System.Windows.Forms.Label();
            this.ddlCellTransfer = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtMitemCode = new System.Windows.Forms.TextBox();
            this.ddlCellBatch = new System.Windows.Forms.ComboBox();
            this.lblTPT = new System.Windows.Forms.Label();
            this.lblConBox = new System.Windows.Forms.Label();
            this.lblAIFrame = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblCell = new System.Windows.Forms.Label();
            this.lblEVA = new System.Windows.Forms.Label();
            this.lblGlass = new System.Windows.Forms.Label();
            this.lblCellBatch = new System.Windows.Forms.Label();
            this.lblWoType = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPlanCode = new System.Windows.Forms.TextBox();
            this.txtFactory = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ddlIsOnlyPacking = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.ddlCancelStorageFlag = new System.Windows.Forms.ComboBox();
            this.txtlocation = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ddlGlassLength = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ddlByIm = new System.Windows.Forms.ComboBox();
            this.ddlCellNetBoard = new System.Windows.Forms.ComboBox();
            this.ddlWostatus = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ddlPackingPattern = new System.Windows.Forms.ComboBox();
            this.lstView = new System.Windows.Forms.ListView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.SapSave = new System.Windows.Forms.Button();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.GpbCartonQuery.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.CartonQueryList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.GpbWoQuery.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxShortAIFrameBatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxCellBatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxAIFrameBatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxConBoxBatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxTPTBatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxEVABatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxGlassBatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxWO)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1020, 15);
            this.panel1.TabIndex = 3;
            // 
            // CartonReset
            // 
            this.CartonReset.Location = new System.Drawing.Point(297, 8);
            this.CartonReset.Name = "CartonReset";
            this.CartonReset.Size = new System.Drawing.Size(56, 22);
            this.CartonReset.TabIndex = 2;
            this.CartonReset.Tag = "button1";
            this.CartonReset.Text = "重置";
            this.CartonReset.UseVisualStyleBackColor = true;
            this.CartonReset.Click += new System.EventHandler(this.CartonReset_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.splitContainer1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 15);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1020, 583);
            this.panel3.TabIndex = 4;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.GpbCartonQuery);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.GpbWoQuery);
            this.splitContainer1.Size = new System.Drawing.Size(1020, 583);
            this.splitContainer1.SplitterDistance = 376;
            this.splitContainer1.TabIndex = 0;
            // 
            // GpbCartonQuery
            // 
            this.GpbCartonQuery.Controls.Add(this.splitContainer2);
            this.GpbCartonQuery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GpbCartonQuery.Location = new System.Drawing.Point(0, 0);
            this.GpbCartonQuery.Name = "GpbCartonQuery";
            this.GpbCartonQuery.Size = new System.Drawing.Size(376, 583);
            this.GpbCartonQuery.TabIndex = 2;
            this.GpbCartonQuery.TabStop = false;
            this.GpbCartonQuery.Text = "托号查询";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 17);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.pictureBox1);
            this.splitContainer2.Panel1.Controls.Add(this.CartonReset);
            this.splitContainer2.Panel1.Controls.Add(this.CartonQueryList);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dataGridView1);
            this.splitContainer2.Size = new System.Drawing.Size(370, 563);
            this.splitContainer2.SplitterDistance = 37;
            this.splitContainer2.TabIndex = 48;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(260, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(27, 18);
            this.pictureBox1.TabIndex = 223;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // CartonQueryList
            // 
            this.CartonQueryList.Controls.Add(this.txtCarton);
            this.CartonQueryList.Controls.Add(this.label2);
            this.CartonQueryList.Location = new System.Drawing.Point(30, 2);
            this.CartonQueryList.Name = "CartonQueryList";
            this.CartonQueryList.Size = new System.Drawing.Size(235, 32);
            this.CartonQueryList.TabIndex = 93;
            // 
            // txtCarton
            // 
            this.txtCarton.Location = new System.Drawing.Point(81, 6);
            this.txtCarton.Name = "txtCarton";
            this.txtCarton.Size = new System.Drawing.Size(148, 21);
            this.txtCarton.TabIndex = 94;
            this.txtCarton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCarton_KeyDown);
            this.txtCarton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCarton_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 12);
            this.label2.TabIndex = 94;
            this.label2.Text = "内 部 托 号";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.PeachPuff;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CheckBox,
            this.RowIndex,
            this.SetFlag,
            this.ModuleSN,
            this.OrderNo,
            this.OrderNo1,
            this.SnStatus,
            this.CartonNo,
            this.CustomerCartonNo,
            this.TestPower,
            this.StdPower,
            this.ModuleGrade,
            this.ProductCode,
            this.FinishedOn,
            this.Factory,
            this.Workshop,
            this.PackingLocation,
            this.CellEff,
            this.ByIm,
            this.OrderStatus,
            this.CellCode,
            this.CellBatch,
            this.CellPrintMode,
            this.GlassCode,
            this.GlassBatch,
            this.EvaCode,
            this.EvaBatch,
            this.TptCode,
            this.TptBatch,
            this.ConBoxCode,
            this.ConBoxBatch,
            this.LongFrameCode,
            this.LongFrameBatch,
            this.GlassThickness,
            this.PackingMode,
            this.ShortFrameCode,
            this.shortFrameBatch,
            this.IsCancelPacking,
            this.SalesOrderNo,
            this.SalesItemNo,
            this.IsOnlyPacking});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(370, 522);
            this.dataGridView1.TabIndex = 47;
            // 
            // CheckBox
            // 
            this.CheckBox.HeaderText = "";
            this.CheckBox.Name = "CheckBox";
            this.CheckBox.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CheckBox.Width = 45;
            // 
            // RowIndex
            // 
            this.RowIndex.HeaderText = "序号";
            this.RowIndex.Name = "RowIndex";
            this.RowIndex.ReadOnly = true;
            this.RowIndex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RowIndex.Width = 45;
            // 
            // SetFlag
            // 
            this.SetFlag.HeaderText = "是否设置";
            this.SetFlag.Name = "SetFlag";
            this.SetFlag.ReadOnly = true;
            this.SetFlag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SetFlag.Width = 60;
            // 
            // ModuleSN
            // 
            this.ModuleSN.HeaderText = "组件号";
            this.ModuleSN.Name = "ModuleSN";
            this.ModuleSN.ReadOnly = true;
            this.ModuleSN.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ModuleSN.Width = 110;
            // 
            // OrderNo
            // 
            this.OrderNo.HeaderText = "外协前工单";
            this.OrderNo.Name = "OrderNo";
            this.OrderNo.ReadOnly = true;
            this.OrderNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // OrderNo1
            // 
            this.OrderNo1.HeaderText = "外协后工单";
            this.OrderNo1.Name = "OrderNo1";
            // 
            // SnStatus
            // 
            this.SnStatus.HeaderText = "组件状态";
            this.SnStatus.Name = "SnStatus";
            this.SnStatus.ReadOnly = true;
            this.SnStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CartonNo
            // 
            this.CartonNo.HeaderText = "内部托号";
            this.CartonNo.Name = "CartonNo";
            this.CartonNo.ReadOnly = true;
            this.CartonNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CustomerCartonNo
            // 
            this.CustomerCartonNo.HeaderText = "客户托号";
            this.CustomerCartonNo.Name = "CustomerCartonNo";
            // 
            // TestPower
            // 
            this.TestPower.HeaderText = "实测功率";
            this.TestPower.Name = "TestPower";
            this.TestPower.ReadOnly = true;
            this.TestPower.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // StdPower
            // 
            this.StdPower.HeaderText = "标称功率";
            this.StdPower.Name = "StdPower";
            this.StdPower.ReadOnly = true;
            this.StdPower.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ModuleGrade
            // 
            this.ModuleGrade.HeaderText = "组件等级";
            this.ModuleGrade.Name = "ModuleGrade";
            this.ModuleGrade.ReadOnly = true;
            this.ModuleGrade.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ProductCode
            // 
            this.ProductCode.HeaderText = "物料代码";
            this.ProductCode.Name = "ProductCode";
            this.ProductCode.ReadOnly = true;
            // 
            // FinishedOn
            // 
            this.FinishedOn.HeaderText = "完工日期";
            this.FinishedOn.Name = "FinishedOn";
            this.FinishedOn.ReadOnly = true;
            // 
            // Factory
            // 
            this.Factory.HeaderText = "工厂";
            this.Factory.Name = "Factory";
            this.Factory.ReadOnly = true;
            // 
            // Workshop
            // 
            this.Workshop.HeaderText = "车间";
            this.Workshop.Name = "Workshop";
            this.Workshop.ReadOnly = true;
            // 
            // PackingLocation
            // 
            this.PackingLocation.HeaderText = "入库地点";
            this.PackingLocation.Name = "PackingLocation";
            this.PackingLocation.ReadOnly = true;
            // 
            // CellEff
            // 
            this.CellEff.HeaderText = "电池片转换效率";
            this.CellEff.Name = "CellEff";
            this.CellEff.ReadOnly = true;
            // 
            // ByIm
            // 
            this.ByIm.HeaderText = "电池分档";
            this.ByIm.Name = "ByIm";
            // 
            // OrderStatus
            // 
            this.OrderStatus.HeaderText = "工单状态";
            this.OrderStatus.Name = "OrderStatus";
            // 
            // CellCode
            // 
            this.CellCode.HeaderText = "电池片物料号";
            this.CellCode.Name = "CellCode";
            // 
            // CellBatch
            // 
            this.CellBatch.HeaderText = "电池片批次";
            this.CellBatch.Name = "CellBatch";
            // 
            // CellPrintMode
            // 
            this.CellPrintMode.HeaderText = "网版";
            this.CellPrintMode.Name = "CellPrintMode";
            // 
            // GlassCode
            // 
            this.GlassCode.HeaderText = "玻璃物料号";
            this.GlassCode.Name = "GlassCode";
            // 
            // GlassBatch
            // 
            this.GlassBatch.HeaderText = "玻璃批次";
            this.GlassBatch.Name = "GlassBatch";
            // 
            // EvaCode
            // 
            this.EvaCode.HeaderText = "EVA物料号";
            this.EvaCode.Name = "EvaCode";
            // 
            // EvaBatch
            // 
            this.EvaBatch.HeaderText = "EVA批次";
            this.EvaBatch.Name = "EvaBatch";
            // 
            // TptCode
            // 
            this.TptCode.HeaderText = "背板物料号";
            this.TptCode.Name = "TptCode";
            // 
            // TptBatch
            // 
            this.TptBatch.HeaderText = "背板批次";
            this.TptBatch.Name = "TptBatch";
            // 
            // ConBoxCode
            // 
            this.ConBoxCode.HeaderText = "接线盒物料号";
            this.ConBoxCode.Name = "ConBoxCode";
            // 
            // ConBoxBatch
            // 
            this.ConBoxBatch.HeaderText = "接线盒批次";
            this.ConBoxBatch.Name = "ConBoxBatch";
            // 
            // LongFrameCode
            // 
            this.LongFrameCode.HeaderText = "长边框物料号";
            this.LongFrameCode.Name = "LongFrameCode";
            // 
            // LongFrameBatch
            // 
            this.LongFrameBatch.HeaderText = "长边框批次";
            this.LongFrameBatch.Name = "LongFrameBatch";
            // 
            // GlassThickness
            // 
            this.GlassThickness.HeaderText = "玻璃厚度";
            this.GlassThickness.Name = "GlassThickness";
            // 
            // PackingMode
            // 
            this.PackingMode.HeaderText = "包装方式";
            this.PackingMode.Name = "PackingMode";
            // 
            // ShortFrameCode
            // 
            this.ShortFrameCode.HeaderText = "短边框物料号";
            this.ShortFrameCode.Name = "ShortFrameCode";
            // 
            // shortFrameBatch
            // 
            this.shortFrameBatch.HeaderText = "短边框批次";
            this.shortFrameBatch.Name = "shortFrameBatch";
            // 
            // IsCancelPacking
            // 
            this.IsCancelPacking.HeaderText = "撤销入库";
            this.IsCancelPacking.Name = "IsCancelPacking";
            // 
            // SalesOrderNo
            // 
            this.SalesOrderNo.HeaderText = "销售订单";
            this.SalesOrderNo.Name = "SalesOrderNo";
            // 
            // SalesItemNo
            // 
            this.SalesItemNo.HeaderText = "销售订单行项目";
            this.SalesItemNo.Name = "SalesItemNo";
            // 
            // IsOnlyPacking
            // 
            this.IsOnlyPacking.HeaderText = "是否拼托";
            this.IsOnlyPacking.Name = "IsOnlyPacking";
            // 
            // GpbWoQuery
            // 
            this.GpbWoQuery.Controls.Add(this.splitContainer3);
            this.GpbWoQuery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GpbWoQuery.Location = new System.Drawing.Point(0, 0);
            this.GpbWoQuery.Name = "GpbWoQuery";
            this.GpbWoQuery.Size = new System.Drawing.Size(640, 583);
            this.GpbWoQuery.TabIndex = 0;
            this.GpbWoQuery.TabStop = false;
            this.GpbWoQuery.Text = "工单查询";
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer3.Location = new System.Drawing.Point(3, 17);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.ddltxtAIFrameCode);
            this.splitContainer3.Panel1.Controls.Add(this.ddltxtShortAIFrameCode);
            this.splitContainer3.Panel1.Controls.Add(this.ddltxtConBoxCod);
            this.splitContainer3.Panel1.Controls.Add(this.ddltxtTPTCode);
            this.splitContainer3.Panel1.Controls.Add(this.ddltxtEVACode);
            this.splitContainer3.Panel1.Controls.Add(this.ddltxtGlassCode);
            this.splitContainer3.Panel1.Controls.Add(this.ddltxtCell);
            this.splitContainer3.Panel1.Controls.Add(this.pictureBox2);
            this.splitContainer3.Panel1.Controls.Add(this.checkBox2);
            this.splitContainer3.Panel1.Controls.Add(this.txtWO1);
            this.splitContainer3.Panel1.Controls.Add(this.txtSalesItemNo);
            this.splitContainer3.Panel1.Controls.Add(this.txtSalesOrderNo);
            this.splitContainer3.Panel1.Controls.Add(this.PicBoxShortAIFrameBatch);
            this.splitContainer3.Panel1.Controls.Add(this.ddlShortAIFrameBatch);
            this.splitContainer3.Panel1.Controls.Add(this.lblShortAIFrameBatch);
            this.splitContainer3.Panel1.Controls.Add(this.lblShortAIFrame);
            this.splitContainer3.Panel1.Controls.Add(this.PicBoxCellBatch);
            this.splitContainer3.Panel1.Controls.Add(this.PicBoxAIFrameBatch);
            this.splitContainer3.Panel1.Controls.Add(this.PicBoxConBoxBatch);
            this.splitContainer3.Panel1.Controls.Add(this.PicBoxTPTBatch);
            this.splitContainer3.Panel1.Controls.Add(this.PicBoxEVABatch);
            this.splitContainer3.Panel1.Controls.Add(this.PicBoxGlassBatch);
            this.splitContainer3.Panel1.Controls.Add(this.ddlAIFrameBatch);
            this.splitContainer3.Panel1.Controls.Add(this.ddlConBoxBatch);
            this.splitContainer3.Panel1.Controls.Add(this.ddlTPTBatch);
            this.splitContainer3.Panel1.Controls.Add(this.ddlEVABatch);
            this.splitContainer3.Panel1.Controls.Add(this.ddlGlassBatch);
            this.splitContainer3.Panel1.Controls.Add(this.PicBoxWO);
            this.splitContainer3.Panel1.Controls.Add(this.Reset);
            this.splitContainer3.Panel1.Controls.Add(this.lblGlassBatch);
            this.splitContainer3.Panel1.Controls.Add(this.label4);
            this.splitContainer3.Panel1.Controls.Add(this.txtWo);
            this.splitContainer3.Panel1.Controls.Add(this.label3);
            this.splitContainer3.Panel1.Controls.Add(this.lblEVABatch);
            this.splitContainer3.Panel1.Controls.Add(this.lblConBoxBatch);
            this.splitContainer3.Panel1.Controls.Add(this.Set);
            this.splitContainer3.Panel1.Controls.Add(this.lblTPTBatch);
            this.splitContainer3.Panel1.Controls.Add(this.ddlWoType);
            this.splitContainer3.Panel1.Controls.Add(this.lblAIFrameBatch);
            this.splitContainer3.Panel1.Controls.Add(this.txtWoOrder);
            this.splitContainer3.Panel1.Controls.Add(this.LblWo);
            this.splitContainer3.Panel1.Controls.Add(this.ddlCellTransfer);
            this.splitContainer3.Panel1.Controls.Add(this.label23);
            this.splitContainer3.Panel1.Controls.Add(this.txtMitemCode);
            this.splitContainer3.Panel1.Controls.Add(this.ddlCellBatch);
            this.splitContainer3.Panel1.Controls.Add(this.lblTPT);
            this.splitContainer3.Panel1.Controls.Add(this.lblConBox);
            this.splitContainer3.Panel1.Controls.Add(this.lblAIFrame);
            this.splitContainer3.Panel1.Controls.Add(this.label14);
            this.splitContainer3.Panel1.Controls.Add(this.lblCell);
            this.splitContainer3.Panel1.Controls.Add(this.lblEVA);
            this.splitContainer3.Panel1.Controls.Add(this.lblGlass);
            this.splitContainer3.Panel1.Controls.Add(this.lblCellBatch);
            this.splitContainer3.Panel1.Controls.Add(this.lblWoType);
            this.splitContainer3.Panel1.Controls.Add(this.label10);
            this.splitContainer3.Panel1.Controls.Add(this.txtPlanCode);
            this.splitContainer3.Panel1.Controls.Add(this.txtFactory);
            this.splitContainer3.Panel1.Controls.Add(this.label11);
            this.splitContainer3.Panel1.Controls.Add(this.ddlIsOnlyPacking);
            this.splitContainer3.Panel1.Controls.Add(this.label8);
            this.splitContainer3.Panel1.Controls.Add(this.label9);
            this.splitContainer3.Panel1.Controls.Add(this.ddlCancelStorageFlag);
            this.splitContainer3.Panel1.Controls.Add(this.txtlocation);
            this.splitContainer3.Panel1.Controls.Add(this.label1);
            this.splitContainer3.Panel1.Controls.Add(this.ddlGlassLength);
            this.splitContainer3.Panel1.Controls.Add(this.label6);
            this.splitContainer3.Panel1.Controls.Add(this.ddlByIm);
            this.splitContainer3.Panel1.Controls.Add(this.ddlCellNetBoard);
            this.splitContainer3.Panel1.Controls.Add(this.ddlWostatus);
            this.splitContainer3.Panel1.Controls.Add(this.label7);
            this.splitContainer3.Panel1.Controls.Add(this.label22);
            this.splitContainer3.Panel1.Controls.Add(this.label5);
            this.splitContainer3.Panel1.Controls.Add(this.ddlPackingPattern);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.lstView);
            this.splitContainer3.Size = new System.Drawing.Size(634, 563);
            this.splitContainer3.SplitterDistance = 461;
            this.splitContainer3.TabIndex = 0;
            // 
            // ddltxtAIFrameCode
            // 
            this.ddltxtAIFrameCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddltxtAIFrameCode.FormattingEnabled = true;
            this.ddltxtAIFrameCode.Location = new System.Drawing.Point(430, 396);
            this.ddltxtAIFrameCode.Name = "ddltxtAIFrameCode";
            this.ddltxtAIFrameCode.Size = new System.Drawing.Size(150, 20);
            this.ddltxtAIFrameCode.TabIndex = 261;
            // 
            // ddltxtShortAIFrameCode
            // 
            this.ddltxtShortAIFrameCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddltxtShortAIFrameCode.FormattingEnabled = true;
            this.ddltxtShortAIFrameCode.Location = new System.Drawing.Point(430, 363);
            this.ddltxtShortAIFrameCode.Name = "ddltxtShortAIFrameCode";
            this.ddltxtShortAIFrameCode.Size = new System.Drawing.Size(150, 20);
            this.ddltxtShortAIFrameCode.TabIndex = 260;
            // 
            // ddltxtConBoxCod
            // 
            this.ddltxtConBoxCod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddltxtConBoxCod.FormattingEnabled = true;
            this.ddltxtConBoxCod.Location = new System.Drawing.Point(431, 328);
            this.ddltxtConBoxCod.Name = "ddltxtConBoxCod";
            this.ddltxtConBoxCod.Size = new System.Drawing.Size(150, 20);
            this.ddltxtConBoxCod.TabIndex = 259;
            // 
            // ddltxtTPTCode
            // 
            this.ddltxtTPTCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddltxtTPTCode.FormattingEnabled = true;
            this.ddltxtTPTCode.Location = new System.Drawing.Point(430, 292);
            this.ddltxtTPTCode.Name = "ddltxtTPTCode";
            this.ddltxtTPTCode.Size = new System.Drawing.Size(150, 20);
            this.ddltxtTPTCode.TabIndex = 258;
            // 
            // ddltxtEVACode
            // 
            this.ddltxtEVACode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddltxtEVACode.FormattingEnabled = true;
            this.ddltxtEVACode.Location = new System.Drawing.Point(430, 257);
            this.ddltxtEVACode.Name = "ddltxtEVACode";
            this.ddltxtEVACode.Size = new System.Drawing.Size(150, 20);
            this.ddltxtEVACode.TabIndex = 257;
            // 
            // ddltxtGlassCode
            // 
            this.ddltxtGlassCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddltxtGlassCode.FormattingEnabled = true;
            this.ddltxtGlassCode.Location = new System.Drawing.Point(431, 223);
            this.ddltxtGlassCode.Name = "ddltxtGlassCode";
            this.ddltxtGlassCode.Size = new System.Drawing.Size(150, 20);
            this.ddltxtGlassCode.TabIndex = 256;
            // 
            // ddltxtCell
            // 
            this.ddltxtCell.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddltxtCell.FormattingEnabled = true;
            this.ddltxtCell.Location = new System.Drawing.Point(431, 189);
            this.ddltxtCell.Name = "ddltxtCell";
            this.ddltxtCell.Size = new System.Drawing.Size(150, 20);
            this.ddltxtCell.TabIndex = 255;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.InitialImage")));
            this.pictureBox2.Location = new System.Drawing.Point(582, 156);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(27, 18);
            this.pictureBox2.TabIndex = 254;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(341, 158);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(90, 16);
            this.checkBox2.TabIndex = 253;
            this.checkBox2.Text = "外协后 工单";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // txtWO1
            // 
            this.txtWO1.Location = new System.Drawing.Point(431, 155);
            this.txtWO1.Name = "txtWO1";
            this.txtWO1.Size = new System.Drawing.Size(150, 21);
            this.txtWO1.TabIndex = 251;
            this.txtWO1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtWO1_KeyDown);
            this.txtWO1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWO1_KeyPress);
            this.txtWO1.Leave += new System.EventHandler(this.txtWO1_Leave);
            // 
            // txtSalesItemNo
            // 
            this.txtSalesItemNo.BackColor = System.Drawing.SystemColors.Menu;
            this.txtSalesItemNo.Enabled = false;
            this.txtSalesItemNo.Location = new System.Drawing.Point(196, 428);
            this.txtSalesItemNo.Name = "txtSalesItemNo";
            this.txtSalesItemNo.Size = new System.Drawing.Size(150, 21);
            this.txtSalesItemNo.TabIndex = 247;
            this.txtSalesItemNo.Visible = false;
            // 
            // txtSalesOrderNo
            // 
            this.txtSalesOrderNo.BackColor = System.Drawing.SystemColors.Menu;
            this.txtSalesOrderNo.Enabled = false;
            this.txtSalesOrderNo.Location = new System.Drawing.Point(25, 428);
            this.txtSalesOrderNo.Name = "txtSalesOrderNo";
            this.txtSalesOrderNo.Size = new System.Drawing.Size(150, 21);
            this.txtSalesOrderNo.TabIndex = 246;
            this.txtSalesOrderNo.Visible = false;
            // 
            // PicBoxShortAIFrameBatch
            // 
            this.PicBoxShortAIFrameBatch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBoxShortAIFrameBatch.BackgroundImage")));
            this.PicBoxShortAIFrameBatch.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicBoxShortAIFrameBatch.InitialImage")));
            this.PicBoxShortAIFrameBatch.Location = new System.Drawing.Point(265, 364);
            this.PicBoxShortAIFrameBatch.Name = "PicBoxShortAIFrameBatch";
            this.PicBoxShortAIFrameBatch.Size = new System.Drawing.Size(27, 18);
            this.PicBoxShortAIFrameBatch.TabIndex = 243;
            this.PicBoxShortAIFrameBatch.TabStop = false;
            this.PicBoxShortAIFrameBatch.Visible = false;
            this.PicBoxShortAIFrameBatch.Click += new System.EventHandler(this.PicBoxShortAIFrameBatch_Click);
            // 
            // ddlShortAIFrameBatch
            // 
            this.ddlShortAIFrameBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlShortAIFrameBatch.FormattingEnabled = true;
            this.ddlShortAIFrameBatch.Location = new System.Drawing.Point(115, 363);
            this.ddlShortAIFrameBatch.Name = "ddlShortAIFrameBatch";
            this.ddlShortAIFrameBatch.Size = new System.Drawing.Size(150, 20);
            this.ddlShortAIFrameBatch.TabIndex = 242;
            this.ddlShortAIFrameBatch.SelectedIndexChanged += new System.EventHandler(this.ddlShortAIFrameBatch_SelectedIndexChanged);
            // 
            // lblShortAIFrameBatch
            // 
            this.lblShortAIFrameBatch.AutoSize = true;
            this.lblShortAIFrameBatch.Location = new System.Drawing.Point(38, 365);
            this.lblShortAIFrameBatch.Name = "lblShortAIFrameBatch";
            this.lblShortAIFrameBatch.Size = new System.Drawing.Size(71, 12);
            this.lblShortAIFrameBatch.TabIndex = 240;
            this.lblShortAIFrameBatch.Text = "短边框批 次";
            // 
            // lblShortAIFrame
            // 
            this.lblShortAIFrame.AutoSize = true;
            this.lblShortAIFrame.Location = new System.Drawing.Point(338, 366);
            this.lblShortAIFrame.Name = "lblShortAIFrame";
            this.lblShortAIFrame.Size = new System.Drawing.Size(89, 12);
            this.lblShortAIFrame.TabIndex = 239;
            this.lblShortAIFrame.Text = "短    边    框";
            // 
            // PicBoxCellBatch
            // 
            this.PicBoxCellBatch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBoxCellBatch.BackgroundImage")));
            this.PicBoxCellBatch.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicBoxCellBatch.InitialImage")));
            this.PicBoxCellBatch.Location = new System.Drawing.Point(265, 188);
            this.PicBoxCellBatch.Name = "PicBoxCellBatch";
            this.PicBoxCellBatch.Size = new System.Drawing.Size(27, 18);
            this.PicBoxCellBatch.TabIndex = 233;
            this.PicBoxCellBatch.TabStop = false;
            this.PicBoxCellBatch.Visible = false;
            this.PicBoxCellBatch.Click += new System.EventHandler(this.PicBoxCellBatch_Click);
            // 
            // PicBoxAIFrameBatch
            // 
            this.PicBoxAIFrameBatch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBoxAIFrameBatch.BackgroundImage")));
            this.PicBoxAIFrameBatch.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicBoxAIFrameBatch.InitialImage")));
            this.PicBoxAIFrameBatch.Location = new System.Drawing.Point(265, 397);
            this.PicBoxAIFrameBatch.Name = "PicBoxAIFrameBatch";
            this.PicBoxAIFrameBatch.Size = new System.Drawing.Size(27, 18);
            this.PicBoxAIFrameBatch.TabIndex = 232;
            this.PicBoxAIFrameBatch.TabStop = false;
            this.PicBoxAIFrameBatch.Visible = false;
            this.PicBoxAIFrameBatch.Click += new System.EventHandler(this.PicBoxAIFrameBatch_Click);
            // 
            // PicBoxConBoxBatch
            // 
            this.PicBoxConBoxBatch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBoxConBoxBatch.BackgroundImage")));
            this.PicBoxConBoxBatch.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicBoxConBoxBatch.InitialImage")));
            this.PicBoxConBoxBatch.Location = new System.Drawing.Point(265, 330);
            this.PicBoxConBoxBatch.Name = "PicBoxConBoxBatch";
            this.PicBoxConBoxBatch.Size = new System.Drawing.Size(27, 18);
            this.PicBoxConBoxBatch.TabIndex = 231;
            this.PicBoxConBoxBatch.TabStop = false;
            this.PicBoxConBoxBatch.Visible = false;
            this.PicBoxConBoxBatch.Click += new System.EventHandler(this.PicBoxConBoxBatch_Click);
            // 
            // PicBoxTPTBatch
            // 
            this.PicBoxTPTBatch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBoxTPTBatch.BackgroundImage")));
            this.PicBoxTPTBatch.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicBoxTPTBatch.InitialImage")));
            this.PicBoxTPTBatch.Location = new System.Drawing.Point(265, 295);
            this.PicBoxTPTBatch.Name = "PicBoxTPTBatch";
            this.PicBoxTPTBatch.Size = new System.Drawing.Size(27, 18);
            this.PicBoxTPTBatch.TabIndex = 230;
            this.PicBoxTPTBatch.TabStop = false;
            this.PicBoxTPTBatch.Visible = false;
            this.PicBoxTPTBatch.Click += new System.EventHandler(this.PicBoxTPTBatch_Click);
            // 
            // PicBoxEVABatch
            // 
            this.PicBoxEVABatch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBoxEVABatch.BackgroundImage")));
            this.PicBoxEVABatch.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicBoxEVABatch.InitialImage")));
            this.PicBoxEVABatch.Location = new System.Drawing.Point(266, 258);
            this.PicBoxEVABatch.Name = "PicBoxEVABatch";
            this.PicBoxEVABatch.Size = new System.Drawing.Size(27, 18);
            this.PicBoxEVABatch.TabIndex = 229;
            this.PicBoxEVABatch.TabStop = false;
            this.PicBoxEVABatch.Visible = false;
            this.PicBoxEVABatch.Click += new System.EventHandler(this.PicBoxEVABatch_Click);
            // 
            // PicBoxGlassBatch
            // 
            this.PicBoxGlassBatch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBoxGlassBatch.BackgroundImage")));
            this.PicBoxGlassBatch.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicBoxGlassBatch.InitialImage")));
            this.PicBoxGlassBatch.Location = new System.Drawing.Point(265, 221);
            this.PicBoxGlassBatch.Name = "PicBoxGlassBatch";
            this.PicBoxGlassBatch.Size = new System.Drawing.Size(27, 18);
            this.PicBoxGlassBatch.TabIndex = 228;
            this.PicBoxGlassBatch.TabStop = false;
            this.PicBoxGlassBatch.Visible = false;
            this.PicBoxGlassBatch.Click += new System.EventHandler(this.PicBoxGlassBatch_Click);
            // 
            // ddlAIFrameBatch
            // 
            this.ddlAIFrameBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlAIFrameBatch.FormattingEnabled = true;
            this.ddlAIFrameBatch.Location = new System.Drawing.Point(115, 396);
            this.ddlAIFrameBatch.Name = "ddlAIFrameBatch";
            this.ddlAIFrameBatch.Size = new System.Drawing.Size(150, 20);
            this.ddlAIFrameBatch.TabIndex = 227;
            this.ddlAIFrameBatch.SelectedIndexChanged += new System.EventHandler(this.ddlAIFrameBatch_SelectedIndexChanged);
            // 
            // ddlConBoxBatch
            // 
            this.ddlConBoxBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlConBoxBatch.FormattingEnabled = true;
            this.ddlConBoxBatch.Location = new System.Drawing.Point(115, 329);
            this.ddlConBoxBatch.Name = "ddlConBoxBatch";
            this.ddlConBoxBatch.Size = new System.Drawing.Size(150, 20);
            this.ddlConBoxBatch.TabIndex = 226;
            this.ddlConBoxBatch.SelectedIndexChanged += new System.EventHandler(this.ddlConBoxBatch_SelectedIndexChanged);
            // 
            // ddlTPTBatch
            // 
            this.ddlTPTBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlTPTBatch.FormattingEnabled = true;
            this.ddlTPTBatch.Location = new System.Drawing.Point(115, 294);
            this.ddlTPTBatch.Name = "ddlTPTBatch";
            this.ddlTPTBatch.Size = new System.Drawing.Size(150, 20);
            this.ddlTPTBatch.TabIndex = 225;
            this.ddlTPTBatch.SelectedIndexChanged += new System.EventHandler(this.ddlTPTBatch_SelectedIndexChanged);
            // 
            // ddlEVABatch
            // 
            this.ddlEVABatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlEVABatch.FormattingEnabled = true;
            this.ddlEVABatch.Location = new System.Drawing.Point(115, 258);
            this.ddlEVABatch.Name = "ddlEVABatch";
            this.ddlEVABatch.Size = new System.Drawing.Size(150, 20);
            this.ddlEVABatch.TabIndex = 224;
            this.ddlEVABatch.SelectedIndexChanged += new System.EventHandler(this.ddlEVABatch_SelectedIndexChanged);
            // 
            // ddlGlassBatch
            // 
            this.ddlGlassBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlGlassBatch.FormattingEnabled = true;
            this.ddlGlassBatch.Location = new System.Drawing.Point(115, 221);
            this.ddlGlassBatch.Name = "ddlGlassBatch";
            this.ddlGlassBatch.Size = new System.Drawing.Size(150, 20);
            this.ddlGlassBatch.TabIndex = 223;
            this.ddlGlassBatch.SelectedIndexChanged += new System.EventHandler(this.ddlGlassBatch_SelectedIndexChanged);
            // 
            // PicBoxWO
            // 
            this.PicBoxWO.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBoxWO.BackgroundImage")));
            this.PicBoxWO.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicBoxWO.InitialImage")));
            this.PicBoxWO.Location = new System.Drawing.Point(581, 9);
            this.PicBoxWO.Name = "PicBoxWO";
            this.PicBoxWO.Size = new System.Drawing.Size(27, 18);
            this.PicBoxWO.TabIndex = 222;
            this.PicBoxWO.TabStop = false;
            this.PicBoxWO.Click += new System.EventHandler(this.PicBoxWO_Click);
            // 
            // Reset
            // 
            this.Reset.Location = new System.Drawing.Point(514, 425);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(72, 24);
            this.Reset.TabIndex = 219;
            this.Reset.Tag = "button1";
            this.Reset.Text = "重置";
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // lblGlassBatch
            // 
            this.lblGlassBatch.AutoSize = true;
            this.lblGlassBatch.Location = new System.Drawing.Point(42, 223);
            this.lblGlassBatch.Name = "lblGlassBatch";
            this.lblGlassBatch.Size = new System.Drawing.Size(71, 12);
            this.lblGlassBatch.TabIndex = 184;
            this.lblGlassBatch.Text = "玻 璃 批 次";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 12);
            this.label4.TabIndex = 208;
            this.label4.Text = "车       间";
            // 
            // txtWo
            // 
            this.txtWo.BackColor = System.Drawing.SystemColors.Menu;
            this.txtWo.Enabled = false;
            this.txtWo.Location = new System.Drawing.Point(115, 47);
            this.txtWo.Name = "txtWo";
            this.txtWo.Size = new System.Drawing.Size(150, 21);
            this.txtWo.TabIndex = 207;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 12);
            this.label3.TabIndex = 206;
            this.label3.Text = "外协前 工单";
            // 
            // lblEVABatch
            // 
            this.lblEVABatch.AutoSize = true;
            this.lblEVABatch.ForeColor = System.Drawing.Color.Black;
            this.lblEVABatch.Location = new System.Drawing.Point(41, 260);
            this.lblEVABatch.Name = "lblEVABatch";
            this.lblEVABatch.Size = new System.Drawing.Size(71, 12);
            this.lblEVABatch.TabIndex = 188;
            this.lblEVABatch.Text = "EVA  批  次";
            // 
            // lblConBoxBatch
            // 
            this.lblConBoxBatch.AutoSize = true;
            this.lblConBoxBatch.Location = new System.Drawing.Point(40, 331);
            this.lblConBoxBatch.Name = "lblConBoxBatch";
            this.lblConBoxBatch.Size = new System.Drawing.Size(71, 12);
            this.lblConBoxBatch.TabIndex = 190;
            this.lblConBoxBatch.Text = "接线盒 批次";
            // 
            // Set
            // 
            this.Set.Location = new System.Drawing.Point(421, 426);
            this.Set.Name = "Set";
            this.Set.Size = new System.Drawing.Size(72, 24);
            this.Set.TabIndex = 204;
            this.Set.Tag = "button1";
            this.Set.Text = "设置";
            this.Set.UseVisualStyleBackColor = true;
            this.Set.Click += new System.EventHandler(this.Set_Click);
            // 
            // lblTPTBatch
            // 
            this.lblTPTBatch.AutoSize = true;
            this.lblTPTBatch.Location = new System.Drawing.Point(41, 296);
            this.lblTPTBatch.Name = "lblTPTBatch";
            this.lblTPTBatch.Size = new System.Drawing.Size(71, 12);
            this.lblTPTBatch.TabIndex = 192;
            this.lblTPTBatch.Text = "背 板 批 次";
            // 
            // ddlWoType
            // 
            this.ddlWoType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlWoType.FormattingEnabled = true;
            this.ddlWoType.Location = new System.Drawing.Point(115, 9);
            this.ddlWoType.Name = "ddlWoType";
            this.ddlWoType.Size = new System.Drawing.Size(150, 20);
            this.ddlWoType.TabIndex = 203;
            this.ddlWoType.SelectedIndexChanged += new System.EventHandler(this.ddlWoType_SelectedIndexChanged);
            // 
            // lblAIFrameBatch
            // 
            this.lblAIFrameBatch.AutoSize = true;
            this.lblAIFrameBatch.Location = new System.Drawing.Point(36, 398);
            this.lblAIFrameBatch.Name = "lblAIFrameBatch";
            this.lblAIFrameBatch.Size = new System.Drawing.Size(71, 12);
            this.lblAIFrameBatch.TabIndex = 194;
            this.lblAIFrameBatch.Text = "长边框批 次";
            // 
            // txtWoOrder
            // 
            this.txtWoOrder.Location = new System.Drawing.Point(431, 8);
            this.txtWoOrder.Name = "txtWoOrder";
            this.txtWoOrder.Size = new System.Drawing.Size(150, 21);
            this.txtWoOrder.TabIndex = 202;
            this.txtWoOrder.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtWoOrder_KeyDown);
            this.txtWoOrder.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWoOrder_KeyPress);
            // 
            // LblWo
            // 
            this.LblWo.AutoSize = true;
            this.LblWo.Location = new System.Drawing.Point(341, 11);
            this.LblWo.Name = "LblWo";
            this.LblWo.Size = new System.Drawing.Size(89, 12);
            this.LblWo.TabIndex = 201;
            this.LblWo.Text = "工          单";
            // 
            // ddlCellTransfer
            // 
            this.ddlCellTransfer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.ddlCellTransfer.FormattingEnabled = true;
            this.ddlCellTransfer.Location = new System.Drawing.Point(116, 152);
            this.ddlCellTransfer.Name = "ddlCellTransfer";
            this.ddlCellTransfer.Size = new System.Drawing.Size(150, 20);
            this.ddlCellTransfer.TabIndex = 200;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(37, 154);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(77, 12);
            this.label23.TabIndex = 198;
            this.label23.Text = "Cell转换效率";
            // 
            // txtMitemCode
            // 
            this.txtMitemCode.BackColor = System.Drawing.SystemColors.Menu;
            this.txtMitemCode.Enabled = false;
            this.txtMitemCode.Location = new System.Drawing.Point(432, 48);
            this.txtMitemCode.Name = "txtMitemCode";
            this.txtMitemCode.Size = new System.Drawing.Size(150, 21);
            this.txtMitemCode.TabIndex = 196;
            // 
            // ddlCellBatch
            // 
            this.ddlCellBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCellBatch.FormattingEnabled = true;
            this.ddlCellBatch.Location = new System.Drawing.Point(115, 187);
            this.ddlCellBatch.Name = "ddlCellBatch";
            this.ddlCellBatch.Size = new System.Drawing.Size(150, 20);
            this.ddlCellBatch.TabIndex = 195;
            this.ddlCellBatch.SelectedIndexChanged += new System.EventHandler(this.ddlCellBatch_SelectedIndexChanged);
            // 
            // lblTPT
            // 
            this.lblTPT.AutoSize = true;
            this.lblTPT.Location = new System.Drawing.Point(337, 298);
            this.lblTPT.Name = "lblTPT";
            this.lblTPT.Size = new System.Drawing.Size(89, 12);
            this.lblTPT.TabIndex = 193;
            this.lblTPT.Text = "背          板";
            // 
            // lblConBox
            // 
            this.lblConBox.AutoSize = true;
            this.lblConBox.Location = new System.Drawing.Point(338, 334);
            this.lblConBox.Name = "lblConBox";
            this.lblConBox.Size = new System.Drawing.Size(89, 12);
            this.lblConBox.TabIndex = 191;
            this.lblConBox.Text = "接    线    盒";
            // 
            // lblAIFrame
            // 
            this.lblAIFrame.AutoSize = true;
            this.lblAIFrame.Location = new System.Drawing.Point(338, 399);
            this.lblAIFrame.Name = "lblAIFrame";
            this.lblAIFrame.Size = new System.Drawing.Size(89, 12);
            this.lblAIFrame.TabIndex = 189;
            this.lblAIFrame.Text = "长    边    框";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(340, 51);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(89, 12);
            this.label14.TabIndex = 187;
            this.label14.Text = "产品  物料编码";
            // 
            // lblCell
            // 
            this.lblCell.AutoSize = true;
            this.lblCell.Location = new System.Drawing.Point(336, 193);
            this.lblCell.Name = "lblCell";
            this.lblCell.Size = new System.Drawing.Size(89, 12);
            this.lblCell.TabIndex = 186;
            this.lblCell.Text = "电    池    片";
            // 
            // lblEVA
            // 
            this.lblEVA.AutoSize = true;
            this.lblEVA.Location = new System.Drawing.Point(338, 262);
            this.lblEVA.Name = "lblEVA";
            this.lblEVA.Size = new System.Drawing.Size(89, 12);
            this.lblEVA.TabIndex = 185;
            this.lblEVA.Text = "EVA       物料";
            // 
            // lblGlass
            // 
            this.lblGlass.AutoSize = true;
            this.lblGlass.Location = new System.Drawing.Point(336, 226);
            this.lblGlass.Name = "lblGlass";
            this.lblGlass.Size = new System.Drawing.Size(89, 12);
            this.lblGlass.TabIndex = 182;
            this.lblGlass.Text = "玻          璃";
            // 
            // lblCellBatch
            // 
            this.lblCellBatch.AutoSize = true;
            this.lblCellBatch.Location = new System.Drawing.Point(42, 189);
            this.lblCellBatch.Name = "lblCellBatch";
            this.lblCellBatch.Size = new System.Drawing.Size(71, 12);
            this.lblCellBatch.TabIndex = 181;
            this.lblCellBatch.Text = "电池片 批次";
            // 
            // lblWoType
            // 
            this.lblWoType.AutoSize = true;
            this.lblWoType.Location = new System.Drawing.Point(43, 11);
            this.lblWoType.Name = "lblWoType";
            this.lblWoType.Size = new System.Drawing.Size(71, 12);
            this.lblWoType.TabIndex = 180;
            this.lblWoType.Text = "工 单 类 型";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(340, 85);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 12);
            this.label10.TabIndex = 210;
            this.label10.Text = "工       厂";
            // 
            // txtPlanCode
            // 
            this.txtPlanCode.BackColor = System.Drawing.SystemColors.Menu;
            this.txtPlanCode.Enabled = false;
            this.txtPlanCode.Location = new System.Drawing.Point(433, 82);
            this.txtPlanCode.Name = "txtPlanCode";
            this.txtPlanCode.Size = new System.Drawing.Size(150, 21);
            this.txtPlanCode.TabIndex = 211;
            // 
            // txtFactory
            // 
            this.txtFactory.BackColor = System.Drawing.SystemColors.Menu;
            this.txtFactory.Enabled = false;
            this.txtFactory.Location = new System.Drawing.Point(116, 81);
            this.txtFactory.Name = "txtFactory";
            this.txtFactory.Size = new System.Drawing.Size(150, 21);
            this.txtFactory.TabIndex = 209;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(339, 159);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 12);
            this.label11.TabIndex = 248;
            this.label11.Text = "是  否  拼  托";
            // 
            // ddlIsOnlyPacking
            // 
            this.ddlIsOnlyPacking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlIsOnlyPacking.FormattingEnabled = true;
            this.ddlIsOnlyPacking.Location = new System.Drawing.Point(431, 156);
            this.ddlIsOnlyPacking.Name = "ddlIsOnlyPacking";
            this.ddlIsOnlyPacking.Size = new System.Drawing.Size(150, 20);
            this.ddlIsOnlyPacking.TabIndex = 249;
            this.ddlIsOnlyPacking.SelectedIndexChanged += new System.EventHandler(this.ddlIsOnlyPacking_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(341, 159);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 12);
            this.label8.TabIndex = 245;
            this.label8.Text = "撤  销  入  库";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(343, 159);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 12);
            this.label9.TabIndex = 183;
            this.label9.Text = "入  库  地  点";
            this.label9.Visible = false;
            // 
            // ddlCancelStorageFlag
            // 
            this.ddlCancelStorageFlag.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCancelStorageFlag.FormattingEnabled = true;
            this.ddlCancelStorageFlag.Location = new System.Drawing.Point(431, 156);
            this.ddlCancelStorageFlag.Name = "ddlCancelStorageFlag";
            this.ddlCancelStorageFlag.Size = new System.Drawing.Size(150, 20);
            this.ddlCancelStorageFlag.TabIndex = 244;
            this.ddlCancelStorageFlag.Visible = false;
            // 
            // txtlocation
            // 
            this.txtlocation.BackColor = System.Drawing.SystemColors.Menu;
            this.txtlocation.Enabled = false;
            this.txtlocation.Location = new System.Drawing.Point(432, 156);
            this.txtlocation.Name = "txtlocation";
            this.txtlocation.Size = new System.Drawing.Size(150, 21);
            this.txtlocation.TabIndex = 205;
            this.txtlocation.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 12);
            this.label1.TabIndex = 235;
            this.label1.Text = "玻 璃 厚 度";
            // 
            // ddlGlassLength
            // 
            this.ddlGlassLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlGlassLength.FormattingEnabled = true;
            this.ddlGlassLength.Location = new System.Drawing.Point(116, 82);
            this.ddlGlassLength.Name = "ddlGlassLength";
            this.ddlGlassLength.Size = new System.Drawing.Size(150, 20);
            this.ddlGlassLength.TabIndex = 236;
            this.ddlGlassLength.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 120);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 12);
            this.label6.TabIndex = 220;
            this.label6.Text = "电 流 分 档";
            // 
            // ddlByIm
            // 
            this.ddlByIm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlByIm.FormattingEnabled = true;
            this.ddlByIm.Location = new System.Drawing.Point(116, 117);
            this.ddlByIm.Name = "ddlByIm";
            this.ddlByIm.Size = new System.Drawing.Size(150, 20);
            this.ddlByIm.TabIndex = 221;
            this.ddlByIm.SelectedIndexChanged += new System.EventHandler(this.ddlByIm_SelectedIndexChanged);
            // 
            // ddlCellNetBoard
            // 
            this.ddlCellNetBoard.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCellNetBoard.FormattingEnabled = true;
            this.ddlCellNetBoard.Location = new System.Drawing.Point(431, 120);
            this.ddlCellNetBoard.Name = "ddlCellNetBoard";
            this.ddlCellNetBoard.Size = new System.Drawing.Size(150, 20);
            this.ddlCellNetBoard.TabIndex = 199;
            this.ddlCellNetBoard.Visible = false;
            // 
            // ddlWostatus
            // 
            this.ddlWostatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlWostatus.FormattingEnabled = true;
            this.ddlWostatus.Location = new System.Drawing.Point(431, 120);
            this.ddlWostatus.Name = "ddlWostatus";
            this.ddlWostatus.Size = new System.Drawing.Size(150, 20);
            this.ddlWostatus.TabIndex = 218;
            this.ddlWostatus.Visible = false;
            this.ddlWostatus.SelectedIndexChanged += new System.EventHandler(this.ddlWostatus_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(338, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 12);
            this.label7.TabIndex = 237;
            this.label7.Text = "包  装  方  式";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label22.Location = new System.Drawing.Point(341, 122);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(89, 12);
            this.label22.TabIndex = 197;
            this.label22.Text = "网          版";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(341, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 12);
            this.label5.TabIndex = 212;
            this.label5.Text = "工  单  状  态";
            this.label5.Visible = false;
            // 
            // ddlPackingPattern
            // 
            this.ddlPackingPattern.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPackingPattern.FormattingEnabled = true;
            this.ddlPackingPattern.Location = new System.Drawing.Point(431, 120);
            this.ddlPackingPattern.Name = "ddlPackingPattern";
            this.ddlPackingPattern.Size = new System.Drawing.Size(150, 20);
            this.ddlPackingPattern.TabIndex = 238;
            this.ddlPackingPattern.SelectedIndexChanged += new System.EventHandler(this.ddlPackingPattern_SelectedIndexChanged);
            // 
            // lstView
            // 
            this.lstView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstView.FullRowSelect = true;
            this.lstView.Location = new System.Drawing.Point(0, 0);
            this.lstView.MultiSelect = false;
            this.lstView.Name = "lstView";
            this.lstView.Size = new System.Drawing.Size(634, 98);
            this.lstView.TabIndex = 1;
            this.lstView.UseCompatibleStateImageBehavior = false;
            this.lstView.View = System.Windows.Forms.View.Details;
            this.lstView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstView_KeyDown);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.SapSave);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 598);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1020, 38);
            this.panel2.TabIndex = 5;
            // 
            // SapSave
            // 
            this.SapSave.Location = new System.Drawing.Point(28, 3);
            this.SapSave.Name = "SapSave";
            this.SapSave.Size = new System.Drawing.Size(83, 32);
            this.SapSave.TabIndex = 50;
            this.SapSave.Tag = "button1";
            this.SapSave.Text = "入库";
            this.SapSave.UseVisualStyleBackColor = true;
            this.SapSave.Click += new System.EventHandler(this.SapSave_Click);
            // 
            // FormStorageSN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 636);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "FormStorageSN";
            this.Text = "零散件入库";
            this.Load += new System.EventHandler(this.SNStorage_Load);
            this.panel3.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.GpbCartonQuery.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.CartonQueryList.ResumeLayout(false);
            this.CartonQueryList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.GpbWoQuery.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxShortAIFrameBatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxCellBatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxAIFrameBatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxConBoxBatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxTPTBatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxEVABatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxGlassBatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxWO)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button CartonReset;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox GpbCartonQuery;
        private System.Windows.Forms.GroupBox GpbWoQuery;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.ListView lstView;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button SapSave;
        private System.Windows.Forms.Panel CartonQueryList;
        private System.Windows.Forms.TextBox txtCarton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.Button Set;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox PicBoxCellBatch;
        private System.Windows.Forms.PictureBox PicBoxAIFrameBatch;
        private System.Windows.Forms.PictureBox PicBoxConBoxBatch;
        private System.Windows.Forms.PictureBox PicBoxTPTBatch;
        private System.Windows.Forms.PictureBox PicBoxEVABatch;
        private System.Windows.Forms.PictureBox PicBoxGlassBatch;
        private System.Windows.Forms.ComboBox ddlAIFrameBatch;
        private System.Windows.Forms.ComboBox ddlConBoxBatch;
        private System.Windows.Forms.ComboBox ddlTPTBatch;
        private System.Windows.Forms.ComboBox ddlEVABatch;
        private System.Windows.Forms.ComboBox ddlGlassBatch;
        private System.Windows.Forms.PictureBox PicBoxWO;
        private System.Windows.Forms.Label lblGlassBatch;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtFactory;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtWo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblEVABatch;
        private System.Windows.Forms.TextBox txtlocation;
        private System.Windows.Forms.Label lblConBoxBatch;
        private System.Windows.Forms.Label lblTPTBatch;
        private System.Windows.Forms.ComboBox ddlWoType;
        private System.Windows.Forms.Label lblAIFrameBatch;
        private System.Windows.Forms.TextBox txtWoOrder;
        private System.Windows.Forms.Label LblWo;
        private System.Windows.Forms.ComboBox ddlCellTransfer;
        private System.Windows.Forms.ComboBox ddlCellNetBoard;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtMitemCode;
        private System.Windows.Forms.ComboBox ddlCellBatch;
        private System.Windows.Forms.Label lblTPT;
        private System.Windows.Forms.Label lblConBox;
        private System.Windows.Forms.Label lblAIFrame;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblCell;
        private System.Windows.Forms.Label lblEVA;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblGlass;
        private System.Windows.Forms.Label lblCellBatch;
        private System.Windows.Forms.Label lblWoType;
        private System.Windows.Forms.ComboBox ddlWostatus;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox ddlByIm;
        private System.Windows.Forms.TextBox txtPlanCode;
        private System.Windows.Forms.ComboBox ddlGlassLength;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox ddlPackingPattern;
        private System.Windows.Forms.PictureBox PicBoxShortAIFrameBatch;
        private System.Windows.Forms.ComboBox ddlShortAIFrameBatch;
        private System.Windows.Forms.Label lblShortAIFrameBatch;
        private System.Windows.Forms.Label lblShortAIFrame;
        private System.Windows.Forms.ComboBox ddlCancelStorageFlag;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSalesOrderNo;
        private System.Windows.Forms.TextBox txtSalesItemNo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox ddlIsOnlyPacking;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn SetFlag;
        private System.Windows.Forms.DataGridViewTextBoxColumn ModuleSN;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderNo1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SnStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn CartonNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerCartonNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn TestPower;
        private System.Windows.Forms.DataGridViewTextBoxColumn StdPower;
        private System.Windows.Forms.DataGridViewTextBoxColumn ModuleGrade;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn FinishedOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factory;
        private System.Windows.Forms.DataGridViewTextBoxColumn Workshop;
        private System.Windows.Forms.DataGridViewTextBoxColumn PackingLocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn CellEff;
        private System.Windows.Forms.DataGridViewTextBoxColumn ByIm;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn CellCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn CellBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn CellPrintMode;
        private System.Windows.Forms.DataGridViewTextBoxColumn GlassCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn GlassBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn EvaCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn EvaBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn TptCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn TptBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConBoxCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConBoxBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn LongFrameCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn LongFrameBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn GlassThickness;
        private System.Windows.Forms.DataGridViewTextBoxColumn PackingMode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShortFrameCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn shortFrameBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsCancelPacking;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesOrderNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesItemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsOnlyPacking;
        private System.Windows.Forms.TextBox txtWO1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ComboBox ddltxtAIFrameCode;
        private System.Windows.Forms.ComboBox ddltxtShortAIFrameCode;
        private System.Windows.Forms.ComboBox ddltxtConBoxCod;
        private System.Windows.Forms.ComboBox ddltxtTPTCode;
        private System.Windows.Forms.ComboBox ddltxtEVACode;
        private System.Windows.Forms.ComboBox ddltxtGlassCode;
        private System.Windows.Forms.ComboBox ddltxtCell;
    }
}