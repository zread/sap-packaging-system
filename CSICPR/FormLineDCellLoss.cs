﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormLineDCellLoss : Form
    {
        public FormLineDCellLoss()
        {
            InitializeComponent();
            List<string> returnList = new List<string>();
            SqlConnection conn = new SqlConnection("Data Source = ca01s0015; Initial Catalog = LabelPrintDB;User ID = reader; Password=12345");
            string sql = "select top 3 ProductionOrder FROM[LabelPrintDB].[dbo].[ProductionOrder_Mixing_Rule] where ProductionOrder like 'CD%' order by ID desc";
            try
            {
                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataReader sdr = sc.ExecuteReader();

                while (sdr.Read())
                {
                    returnList.Add(sdr[0].ToString());

                }
                foreach (string value in returnList)
                {
                    MO.Items.Add(value);


                }
                conn.Close();
            }

            catch (Exception e)
            {
                throw e;
            }
        }
        private static FormLineDCellLoss theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormLineDCellLoss();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                {
                    theSingleton.WindowState = FormWindowState.Maximized;
                }
            }
        }
        private void FormLineDCellLoss_Load(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            ClockD.Text = DateTime.Now.ToString("yyyy-MMM-dd HH:mm");
        }

        private void Submit_Click(object sender, EventArgs e)
        {
            Submit.Enabled = false;
            if (string.IsNullOrEmpty(MO.Text.ToString()) || string.IsNullOrEmpty(shift.Text.ToString()))
            {
                MessageBox.Show("MO and Shift can not be empty");
            }
            else
            {
                string sql = "insert into [CAPA01DB01].[dbo].[BrokenCells] values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}',getdate(),null)";
                sql = string.Format(sql, MO.Text.ToString(), shift.Text.ToString(), Convert.ToInt32(S1.Text), Convert.ToInt32(S2.Text), Convert.ToInt32(S3.Text), Convert.ToInt32(S4.Text), Convert.ToInt32(S5.Text), Convert.ToInt32(S6.Text), Convert.ToInt32(S7.Text), Convert.ToInt32(S8.Text), Convert.ToInt32(S9.Text), Convert.ToInt32(S10.Text), BoxNumTextbox.Text.ToString(), Convert.ToInt32(BoxId.Text), BoxNumtextBox2.Text.ToString(), Convert.ToInt32(BoxId2.Text), Convert.ToInt32(accident.Text), Convert.ToInt32(others.Text));
                Executenonequery(sql);
            }
            string sqlselect = "select ID, MO,Shift,s1 as SD1L,S2 as SD1R,S3 as SD2L,S4 as SD2R ,S5 as SD3L,S6 as SD3R,S7 as SD4L,S8 as SD4R ,S9 as SD5L,S10 as SD5R,Box1, Box1Qty as Incoming1,Box2,Box2Qty as Incoming2,Accident,Others,CreatedDate,ModifiedDate FROM [CAPA01DB01].[dbo].[BrokenCells] where MO = '" + MO.Text + "' and shift = '" + shift.Text + "' and CreatedDate >= GETDATE()-0.1";
            DataTable dt = RetrieveData(sqlselect);
            if (dt.Rows.Count >= 1)
            {
                MessageBox.Show("Broken Cells information go into database.");
            }
            else
            {
                MessageBox.Show("Please check value type of all the field.");
            }
            dataGridView1.DataSource = dt;
            Submit.Enabled = true;

        }
        private void Executenonequery(string sql)
        {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");

                conn.Open();
                SqlCommand Mycommand = new SqlCommand(sql, conn);
                Mycommand.ExecuteNonQuery();
                conn.Close();



            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private DataTable RetrieveData(string sql)
        {
            try
            {
                DataTable dt = new System.Data.DataTable();
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataAdapter sda = new SqlDataAdapter(sc);
                sda.Fill(dt);
                conn.Close();
                sda.Dispose();
                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void Review_Click(object sender, EventArgs e)
        {
            Review.Enabled = false;
            S1.Text = "0";
            S2.Text = "0";
            S3.Text = "0";
            S4.Text = "0";
            S5.Text = "0";
            S6.Text = "0";
            S7.Text = "0";
            S8.Text = "0";
            S9.Text = "0";
            S10.Text = "0";
            BoxNumTextbox.Text = "";
            BoxId.Text = "0";
            BoxNumtextBox2.Text = "";
            BoxId2.Text = "0";
            accident.Text = "0";
            others.Text = "0";


            string sqlselect = "select ID,MO,Shift,s1 as SD1L,S2 as SD1R,S3 as SD2L,S4 as SD2R ,S5 as SD3L,S6 as SD3R,S7 as SD4L,S8 as SD4R ,S9 as SD5L,S10 as SD5R,Box1, Box1Qty as Incoming1,Box2,Box2Qty as Incoming2,Accident,Others,CreatedDate,ModifiedDate FROM [CAPA01DB01].[dbo].[BrokenCells] where MO = '" + MO.Text + "' and shift = '" + shift.Text + "' and CreatedDate >= GETDATE()-1";
            DataTable dt = RetrieveData(sqlselect);
            if (dt.Rows.Count < 1)
            {
                MessageBox.Show("MO and Shift can not be Null. Otherwise, no data is found.");
            }
            else
            {
                dataGridView1.DataSource = dt;
            }
            Review.Enabled = true;
        }

        private void Modify_Click(object sender, EventArgs e)
        {
            Modify.Enabled = false;
            int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;

            DataGridViewRow selectedRow = dataGridView1.Rows[selectedrowindex];
            string sql = "Update  [CAPA01DB01].[dbo].[BrokenCells] set  shift = '" + selectedRow.Cells["shift"].Value.ToString() + "',Box1 = '" + selectedRow.Cells["box1"].Value.ToString() + "',Box2 = '" + selectedRow.Cells["Box2"].Value.ToString() + "',S1 = '" + selectedRow.Cells["SD1L"].Value + "',S2 = '" + selectedRow.Cells["SD1R"].Value + "',S3 = '" + selectedRow.Cells["SD2L"].Value + "',S4 = '" + selectedRow.Cells["SD2R"].Value + "',S5 = '" + selectedRow.Cells["SD3L"].Value + "',S6 = '" + selectedRow.Cells["SD3R"].Value + "',S7 = '" + selectedRow.Cells["SD4L"].Value + "',S8 = '" + selectedRow.Cells["SD4R"].Value + "',S9 = '" + selectedRow.Cells["SD5L"].Value + "',S10 = '" + selectedRow.Cells["SD5R"].Value + "',Box1Qty = '" + selectedRow.Cells["Incoming1"].Value + "',Box2Qty = '" + selectedRow.Cells["Incoming2"].Value + "',accident = '" + selectedRow.Cells["accident"].Value + "',others = '" + selectedRow.Cells["others"].Value + "', ModifiedDate = GETDATE()  where  ID = '" + selectedRow.Cells["ID"].Value + "'";
            Executenonequery(sql);
            MessageBox.Show("This row is Modified. Please review again.");
            Modify.Enabled = true;
        }
    }
}
