﻿using System;
using System.ComponentModel;
//using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Collections.Generic;


namespace CSICPR
{
    public partial class FormSAPJobNoLoad : Form
    {
        private static List<LanguageItemModel> LanMessList;//定义语言集
        #region 共有变量
        private static FormSAPJobNoLoad theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormSAPJobNoLoad();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        public FormSAPJobNoLoad()
        {
            InitializeComponent();
        }

        private void FormSAPJobNoLoad_Load(object sender, EventArgs e)
        {
            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion
            this.lstView.Columns.Add("提示信息", 1000, HorizontalAlignment.Left);
        }
 
        /// <summary>
        /// 提供数据复制功能
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (lstView.SelectedItems.Count > 0)
                {
                    if (lstView.SelectedItems[0].Text != "")
                        Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
                }
            }
        }

        private string wo = "";
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.txtWo.Text.Trim().Equals(""))
            {
                ToolsClass.Log("输入的货柜编码不能为空！", "ABNORMAL", lstView);
                this.txtWo.Focus();
                this.txtWo.SelectAll();
                return;
            }
            else
                wo = this.txtWo.Text.Trim();

            try
            {
                string msg = "";
                SapStorageService.SapMesInterfaceClient service = new SapStorageService.SapMesInterfaceClient();
                service.Endpoint.Address = new System.ServiceModel.EndpointAddress(FormCover.WebServiceAddress);
                msg = service.toPackDataSaptoMes("", wo);
                if (msg.Equals("0"))
                {
                    ToolsClass.Log("货柜编码:" + wo + " 不存在,请确认！", "ABNORMAL", lstView);
                    this.txtWo.Focus();
                    this.txtWo.SelectAll();
                    return;
                }
                else
                {
                    ToolsClass.Log("货柜编码:" + wo + " 下载成功", "NORMAL", lstView);
                    this.txtWo.Clear();
                    this.txtWo.Focus();
                    this.txtWo.SelectAll();
                }
            }
            catch (Exception ex)
            {
                ToolsClass.Log("货柜编码: " + wo + " 在下载时发生异常 " + ex.Message + "", "ABNORMAL", lstView);
                this.txtWo.Focus();
                this.txtWo.SelectAll();
            }
        }
        private void txtWo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                this.btnSave_Click(null,null);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            this.txtWo.Clear();
            if (lstView.Items.Count > 100)
                lstView.Items.Clear();
            this.txtWo.Focus();
        }
    }
}
