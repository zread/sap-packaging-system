﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace CSICPR
{
    public class ServerDateTimeDal
    {
        public static ServerDateTime GetServerDateTime()
        {
            const string sql = @"SELECT GETDATE()";
            var obj = SqlHelper.ExecuteScalar(FormCover.connectionBase.Trim(), CommandType.Text, sql);
            var dt = new ServerDateTime
                         {
                             DateTime = (DateTime)obj
                         };
            return dt;
        }
    }
}
