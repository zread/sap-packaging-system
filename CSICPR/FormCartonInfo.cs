﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormCartonInfo : Form
    {
        #region 私有变量
        private static List<LanguageItemModel> LanMessList;
        private string CartonNo = "";
        private string QueryType = "";
        public string Carton = "";
        private DateTime PackingDateFrom;
        private DateTime PackingDateTo;
        #endregion

        #region 私有方法
        private void ClearDataGridViewData()
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();
        }
        private void SetDataGridViewData(string type)
        {
            if (QueryType.Equals("Carton"))
            {
                if (Convert.ToString(this.txtCarton.Text.Trim()).Equals(""))
                {
                    this.txtCarton.Focus();
                    return;
                }
            }
            else if (QueryType.Equals("Counter"))
            {
                if (Convert.ToString(this.txtConter.Text.Trim()).Equals(""))
                {
                    this.txtConter.Focus();
                    return;
                }
            }
            else
            {
                PackingDateFrom = Convert.ToDateTime(this.dtpPackingDateFrom.Value);
                PackingDateTo = Convert.ToDateTime(this.dtpPackingDateTo.Value);
            }

            DataTable dt = ProductStorageDAL.GetCartonStorageInfo(QueryType, this.txtCarton.Text.Trim().ToString(), PackingDateFrom, PackingDateTo, this.txtConter.Text.Trim().ToString());
            if (dt != null && dt.Rows.Count > 0)
            {
                int RowNo = 0;
                int RowIndex = 1;
                foreach (DataRow row in dt.Rows)
                {
                    this.dataGridView1.Rows.Add();
                    this.dataGridView1.Rows[RowNo].Cells["RowIndex"].Value = RowIndex.ToString();
                    if (Convert.ToString(row["CartonStatus"]).Equals("0"))
                        this.dataGridView1.Rows[RowNo].Cells["CartonStatus"].Value = "已测试";
                    else if (Convert.ToString(row["CartonStatus"]).Equals("1"))
                        this.dataGridView1.Rows[RowNo].Cells["CartonStatus"].Value = "已包装";
                    else if (Convert.ToString(row["CartonStatus"]).Equals("2"))
                        this.dataGridView1.Rows[RowNo].Cells["CartonStatus"].Value = "已入库";
                    else
                        this.dataGridView1.Rows[RowNo].Cells["CartonStatus"].Value = Convert.ToString(row["CartonStatus"]);
                    this.dataGridView1.Rows[RowNo].Cells["CartonID"].Value = Convert.ToString(row["CartonID"]);
                    this.dataGridView1.Rows[RowNo].Cells["SNQTY"].Value = Convert.ToString(row["SNQTY"]);
                    this.dataGridView1.Rows[RowNo].Cells["Cust_BoxID"].Value = Convert.ToString(row["Cust_BoxID"]);
                    RowNo++;
                    RowIndex++;
                }
            }
            else
            {
                if (type.Equals(""))
                    MessageBox.Show("没有查到数据", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.txtCarton.Text = "";
        }
        #endregion

        #region 窗体事件
        public FormCartonInfo()
        {
            InitializeComponent();
        }

        public FormCartonInfo(string Carton)
        {
           
            InitializeComponent();
            CartonNo = Carton;
        }

        private void FormCartonInfoQuery_Load(object sender, EventArgs e)
        {
          
            this.Text = "托号选择";
            //查询类型
            this.ddlQueryType.Items.Insert(0, "包装日期");
            this.ddlQueryType.Items.Insert(1, "内部托号");
            this.ddlQueryType.Items.Insert(2, "货柜号");
            
            this.ddlQueryType.SelectedIndex = 1;
            this.PalPackingDate.Visible = false;
            this.PalCarton.Visible = true;
            this.txtCarton.Text = CartonNo;
            this.txtCarton.SelectAll();
            this.txtCarton.Focus();
            

            if (!CartonNo.Equals(""))
                SetDataGridViewData("First");


            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            LanguageHelper.GetCombomBox(this, this.ddlQueryType);
            # endregion
        }

        #endregion

        #region 窗体页面事件
        private void ddlQueryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlQueryType.SelectedIndex == 0)
            {
                this.PalPackingDate.Visible = true;
                this.PalCarton.Visible = false;
                this.PalConter.Visible = false;
                this.dtpPackingDateFrom.Focus();
                QueryType = "PackingDate";
            }
            else if (this.ddlQueryType.SelectedIndex == 1)
            {
                this.PalPackingDate.Visible = false;
                this.PalCarton.Visible = true;
                this.PalConter.Visible = false;
                this.txtCarton.Clear();
                this.txtCarton.Focus();
                QueryType = "Carton";
            }
            else
            {
                this.PalPackingDate.Visible = false;
                this.PalCarton.Visible = false;
                this.PalConter.Visible = true;
                this.txtConter.Clear();
                this.txtConter.Focus();
                QueryType = "Counter";
            }
            ClearDataGridViewData();
        }
        
        private void btnSave_Click(object sender, EventArgs e)
        {
            Carton = this.dataGridView1.CurrentRow.Cells["CartonID"].Value.ToString();
            this.Close();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            ClearDataGridViewData();
            SetDataGridViewData("");
        }
        #endregion

        private void txtConter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                btnQuery_Click(null,null);
        }

        private void txtCarton_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                btnQuery_Click(null,null);
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                Carton = this.dataGridView1.CurrentRow.Cells["CartonID"].Value.ToString();
                this.Close();
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Carton = this.dataGridView1.Rows[e.RowIndex].Cells["CartonID"].Value.ToString();
            this.Close();
        }
            
    }
}
