﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormChangeOver : Form
    {
        public FormChangeOver()
        {
            InitializeComponent();
            
        }
        private static FormChangeOver theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormChangeOver();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                {
                    theSingleton.WindowState = FormWindowState.Maximized;
                }
            }
        }
        private void FormChangeOver_Load(object sender, EventArgs e)
        {

        }
        private void Executenonequery(string sql)
        {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");

                conn.Open();
                SqlCommand Mycommand = new SqlCommand(sql, conn);
                Mycommand.ExecuteNonQuery();
                conn.Close();



            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private DataTable RetrieveData(string sql)
        {
            try
            {
                DataTable dt = new System.Data.DataTable();
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataAdapter sda = new SqlDataAdapter(sc);
                sda.Fill(dt);
                conn.Close();
                sda.Dispose();
                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void Line_Click(object sender, EventArgs e)
        {

        }

        private void LeftOverIn_Click(object sender, EventArgs e)
        {
            CellQty.Visible = true;
            string text = CartonNo.Text.ToString();
            string[] T = text.Split('\n');
            for (int i = 0; i < T.Length; i++)
            {
                T[i] = T[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                string sql = "insert into [CAPA01DB01].[dbo].[LeftOverBin] values('{0}','{1}','{2}','{3}','{4}',getdate(),null,null)";
                if (LeftOverMo.Text=="LINE A" && T[i] != "")
                {

                    sql = string.Format(sql, "CA0000000".ToString(), BatchNo.Text.ToString(), SkidNo.Text.ToString(),T[i], Convert.ToInt32(pcs.Text));
                    Executenonequery(sql);
                    
                }
                if (LeftOverMo.Text == "LINE B" && T[i] != "")
                {

                    sql = string.Format(sql, "CB0000000".ToString(), BatchNo.Text.ToString(), SkidNo.Text.ToString(), T[i], Convert.ToInt32(pcs.Text));
                    Executenonequery(sql);
                  
                }
                if (LeftOverMo.Text == "LINE D" && T[i] != "")
                {

                    sql = string.Format(sql, "CD0000000".ToString(), BatchNo.Text.ToString(), SkidNo.Text.ToString(), T[i], Convert.ToInt32(pcs.Text));
                    Executenonequery(sql);
                }
            }
            try
            {
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                string Mo = "SELECT Sum(Qty) as Qty FROM[CAPA01DB01].[dbo].[LeftOverBin] where SkidNO = '" + SkidNo.Text + "' group by SkidNo";

                conn.Open();
                SqlCommand sc = new SqlCommand(Mo, conn);
                SqlDataReader sdr = sc.ExecuteReader();

                if (sdr.Read())
                {
                    CellQty.Text = "Current Skid Cell Qty: " + sdr["Qty"].ToString() + " pcs";
                }
                else
                {
                    CellQty.Text = "Current Skid Cell Qty: 0 pcs ";
                }
                conn.Close();
            }

            catch (Exception e2)
            {
                throw e2;
            }
            string sqlselect = "select * from (select ROW_NUMBER()over(partition by CartonNo order by id) rn from [CAPA01DB01].[dbo].[LeftOverBin] where BatchNo = '" + BatchNo.Text + "' and SkidNo = '" + SkidNo.Text + "')tb1 where rn=2";
            DataTable dt = RetrieveData(sqlselect);
            if (dt.Rows.Count >= 1)
            {
                MessageBox.Show("Some Carton No. is Duplicate.");
                CartonNo.Clear();
                string sql = "select ID,BinNum,SkidNo,CartonNo,Qty,CreatedDate from [CAPA01DB01].[dbo].[LeftOverBin] where BatchNo = '" + BatchNo.Text + "' and SkidNo = '" + SkidNo.Text + "'";

                DataTable dt2 = RetrieveData(sql);
                dataGridView1.DataSource = dt2;
                HighlightDuplicate(dataGridView1);


            }
            else
            {
                MessageBox.Show("New Carton No. are Stocked In.");
                CartonNo.Clear();
                string sql = "select ID,BinNum,SkidNo,CartonNo,Qty,CreatedDate from [CAPA01DB01].[dbo].[LeftOverBin] where BatchNo = '" + BatchNo.Text + "' and SkidNo = '" + SkidNo.Text + "'";

                DataTable dt2 = RetrieveData(sql);
                dataGridView1.DataSource = dt2;
                HighlightDuplicate(dataGridView1);


            }
        }

        private void Review_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(LeftOverMo.Text.ToString()))
            {
                BatchNo.Clear();
                SkidNo.Clear();
                CellQty.Visible = true;
                if (LeftOverMo.Text == "LINE A")
                {
                    string sql = "select ID,BinNum,BatchNo,SkidNo,CartonNo,Qty,CreatedDate from [CAPA01DB01].[dbo].[LeftOverBin] where BinNum = 'CA0000000'";
                    DataTable dt = RetrieveData(sql);
                    dataGridView1.DataSource = dt;
                    HighlightDuplicate(dataGridView1);

                    string sql2 = "select BinNum,BatchNo,sum(Qty) as BatchQty from [CAPA01DB01].[dbo].[LeftOverBin] where BinNum = 'CA0000000' Group by BatchNo, BinNum";
                    DataTable dt2 = RetrieveData(sql2);
                    dataGridView2.DataSource = dt2;
                    try
                    {
                        SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                        string Mo = "SELECT Sum(Qty) as Qty FROM[CAPA01DB01].[dbo].[LeftOverBin] where BinNum = 'CA0000000' group by BinNum";

                        conn.Open();
                        SqlCommand sc = new SqlCommand(Mo, conn);
                        SqlDataReader sdr = sc.ExecuteReader();

                        if (sdr.Read())
                        {
                            CellQty.Text = "LeftOver Bin Cell Qty: " + sdr["Qty"].ToString() + " pcs";
                        }
                        else
                        {
                            CellQty.Text = "LeftOver Bin Cell Qty: 0 pcs ";
                        }
                        conn.Close();
                    }

                    catch (Exception e2)
                    {
                        throw e2;
                    }
                }
                if (LeftOverMo.Text == "LINE B")
                {
                    string sql = "select ID,BinNum,BatchNo,SkidNo,CartonNo,Qty,CreatedDate from [CAPA01DB01].[dbo].[LeftOverBin] where BinNum = 'CB0000000'";
                    DataTable dt = RetrieveData(sql);
                    dataGridView1.DataSource = dt;
                    HighlightDuplicate(dataGridView1);

                    string sql2 = "select BinNum,BatchNo,sum(Qty) as BatchQty from [CAPA01DB01].[dbo].[LeftOverBin] where BinNum = 'CB0000000' Group by BatchNo, BinNum";
                    DataTable dt2 = RetrieveData(sql2);
                    dataGridView2.DataSource = dt2;
                    try
                    {
                        SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                        string Mo = "SELECT Sum(Qty) as Qty FROM[CAPA01DB01].[dbo].[LeftOverBin] where BinNum = 'CB0000000' group by BinNum";

                        conn.Open();
                        SqlCommand sc = new SqlCommand(Mo, conn);
                        SqlDataReader sdr = sc.ExecuteReader();

                        if (sdr.Read())
                        {
                            CellQty.Text = "LeftOver Bin Cell Qty: " + sdr["Qty"].ToString() + " pcs";
                        }
                        else
                        {
                            CellQty.Text = "LeftOver Bin Cell Qty: 0 pcs ";
                        }
                        conn.Close();
                    }

                    catch (Exception e2)
                    {
                        throw e2;
                    }
                }
                if (LeftOverMo.Text == "LINE D")
                {
                    string sql = "select ID,BinNum,BatchNo,SkidNo,CartonNo,Qty,CreatedDate from [CAPA01DB01].[dbo].[LeftOverBin] where BinNum = 'CD0000000'";
                    DataTable dt = RetrieveData(sql);
                    dataGridView1.DataSource = dt;
                    HighlightDuplicate(dataGridView1);

                    string sql2 = "select BinNum,BatchNo,sum(Qty) as BatchQty from [CAPA01DB01].[dbo].[LeftOverBin] where BinNum = 'CD0000000' Group by BatchNo, BinNum";
                    DataTable dt2 = RetrieveData(sql2);
                    dataGridView2.DataSource = dt2;
                    try
                    {
                        SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                        string Mo = "SELECT Sum(Qty) as Qty FROM[CAPA01DB01].[dbo].[LeftOverBin] where BinNum = 'CD0000000' group by BinNum";

                        conn.Open();
                        SqlCommand sc = new SqlCommand(Mo, conn);
                        SqlDataReader sdr = sc.ExecuteReader();

                        if (sdr.Read())
                        {
                            CellQty.Text = "LeftOver Bin Cell Qty: " + sdr["Qty"].ToString() + " pcs";
                        }
                        else
                        {
                            CellQty.Text = "LeftOver Bin Cell Qty: 0 pcs ";
                        }
                        conn.Close();
                    }

                    catch (Exception e2)
                    {
                        throw e2;
                    }
                }

            }
        }
        private void HighlightDuplicate(DataGridView grv)
        {

            //use the currentRow to compare against
            for (int currentRow = 0; currentRow < grv.Rows.Count - 1; currentRow++)
            {
                DataGridViewRow rowToCompare = grv.Rows[currentRow];
                //specify otherRow as currentRow + 1
                for (int otherRow = currentRow + 1; otherRow < grv.Rows.Count; otherRow++)
                {
                    DataGridViewRow row = grv.Rows[otherRow];

                    bool duplicateRow = true;
                    //compare cell ENVA_APP_ID between the two rows
                    if (!rowToCompare.Cells["CartonNo"].Value.Equals(row.Cells["CartonNo"].Value))
                    {
                        duplicateRow = false;
                    }
                    

                    //highlight both the currentRow and otherRow if ENVA_APP_ID matches 
                    if (duplicateRow)
                    {
                        rowToCompare.DefaultCellStyle.BackColor = Color.Red;
                        rowToCompare.DefaultCellStyle.ForeColor = Color.Black;
                        row.DefaultCellStyle.BackColor = Color.Red;
                        row.DefaultCellStyle.ForeColor = Color.Black;
                    }
                }
            }
        }

        private void Modify_Click(object sender, EventArgs e)
        {
            int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dataGridView1.Rows[selectedrowindex];

            string sql = "Update  [CAPA01DB01].[dbo].[LeftOverBin] set  SkidNo = '" + selectedRow.Cells["SkidNo"].Value.ToString() + "',BatchNo = '" + selectedRow.Cells["BatchNo"].Value.ToString() + "',CartonNo = '" + selectedRow.Cells["CartonNo"].Value.ToString() + "' ,Qty = " + selectedRow.Cells["Qty"].Value + ",ModifiedDate = GETDATE() ,ModifiedBy = '" + FormCover.currUserName.ToString() + "' where  ID = '" + selectedRow.Cells["ID"].Value + "'";
            Executenonequery(sql);
            MessageBox.Show("This row is Modified.");
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            String Message = "Do you want to delete this information?";
            String Caption = "Delete";
            DialogResult result = MessageBox.Show(Message, Caption, MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dataGridView1.Rows[selectedrowindex];

                string sql = "Delete from  [CAPA01DB01].[dbo].[LeftOverBin]  where CartonNo = '" + selectedRow.Cells["CartonNo"].Value + "' and ID = '" + selectedRow.Cells["ID"].Value + "'";
                Executenonequery(sql);
                
            }
        }
        private void BatchNo_Keydown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SkidNo.Focus();
        }
        private void SkidNo_Keydown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                CartonNo.Focus();
        }
    }
}
