﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace CSICPR
{
    public partial class FormReWorkOrderInfo : Form   
    {
        private static List<LanguageItemModel> LanMessList;//定义语言集
        public string  Wo="";
         
        #region 私有方法
        #region
        private void ClearDataGridViewData()
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();
        }
        private void SetDataGridViewData()
        {
            if (this.txtWo.Text.Trim().Equals(""))
                return;

            DataTable dt = ProductStorageDAL.GetReworkWoIno(this.txtWo.Text.Trim());
            if (dt != null && dt.Rows.Count > 0)
            {
                int RowNo = 0;
                int RowIndex = 1;
                foreach (DataRow row in dt.Rows)
                {
                    this.dataGridView1.Rows.Add();
                    this.dataGridView1.Rows[RowNo].Cells["RowIndex"].Value = RowIndex.ToString();
                    this.dataGridView1.Rows[RowNo].Cells["WONO"].Value = Convert.ToString(row["TWO_NO"]);
                    this.dataGridView1.Rows[RowNo].Cells["Mitem"].Value = Convert.ToString(row["TWO_PRODUCT_NAME"]);
                    this.dataGridView1.Rows[RowNo].Cells["Order"].Value = Convert.ToString(row["TWO_ORDER_NO"]);
                    RowNo++;
                    RowIndex++;
                }
            }
            else
            {
                this.txtWo.SelectAll();
                MessageBox.Show("没有查到此工单所对应的信息");
                return;
            }
        }
        #endregion
        #endregion

        public FormReWorkOrderInfo()
        {
            InitializeComponent();
        }

        public FormReWorkOrderInfo(string WO)
        {
            InitializeComponent();
            Wo = WO;
            this.txtWo.Text = Wo;
        }

        #region 窗体页面事件
        private void btnQuery_Click(object sender, EventArgs e)
        {
            ClearDataGridViewData();
            SetDataGridViewData();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
        	if(dataGridView1.Rows.Count > 0)
        	Wo = this.dataGridView1.CurrentRow.Cells["WONO"].Value.ToString();
        	else 
        		return;
        	
            this.Close();        
        }
        #endregion

        private void txtWo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                btnQuery_Click(null,null);
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                Wo = this.dataGridView1.CurrentRow.Cells["WONO"].Value.ToString();
                this.Close();
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Wo = this.dataGridView1.Rows[e.RowIndex].Cells["WONO"].Value.ToString();
            this.Close();
        }

        private void FormReWorkOrderInfo_Load(object sender, EventArgs e)
        {
            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion
            btnQuery_Click(null,null);
        }
    }
}
