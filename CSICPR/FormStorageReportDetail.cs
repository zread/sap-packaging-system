﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormStorageReportDetail : Form
    {
        private DataTable dt = null;
        private int count = 0;

        public FormStorageReportDetail()
        {
            InitializeComponent();
        }

        public FormStorageReportDetail(DataTable dt_detail,int _count)
        {
            InitializeComponent();
            dt = dt_detail;
            count = _count;
        }

        private void FormStorageReportDetail_Load(object sender, EventArgs e)
        {
            this.Text = "共查询到 "+count+" 笔数据";
            if (dt != null && dt.Rows.Count > 0)
            {
                this.dataGridView1.DataSource = dt;
            }
        }

    }
}
