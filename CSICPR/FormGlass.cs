﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormGlass : Form
    {
        public FormGlass(string pa,string ma,string unit,string qty)
        {
            InitializeComponent();
            line.Text = pa;
            Material.Text = ma;
            SU.Text = (ma == "Glass")?"Box#":"SU#";
            pcs.Text = unit;
            Qty.Text = qty;

            List<string> returnList = new List<string>();
            SqlConnection conn = new SqlConnection("Data Source = ca01s0015; Initial Catalog = LabelPrintDB;User ID = reader; Password=12345");            
            string sql = "select top 4 ProductionOrder FROM[LabelPrintDB].[dbo].[ProductionOrder_Mixing_Rule]  where SUBSTRING(ProductionOrder, 2, 1) = '" + line.Text + "' order by ID desc";
            
            try
            {
                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataReader sdr = sc.ExecuteReader();

                while (sdr.Read())
                {
                    returnList.Add(sdr[0].ToString());

                }
                foreach (string value in returnList)
                {
                    MO.Items.Add(value);


                }
                conn.Close();
            }

            catch (Exception e)
            {
                throw e;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void FormGlass_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void Materialcode_keyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                Batchnum.Focus();
        }
        private void Batchnum_keyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SUNum.Focus();
        }
        private void sunum_keyDown(object sender, KeyEventArgs e)
        {
           
        }
        private int Executenonequery(string sql)
        {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");

                conn.Open();
                SqlCommand Mycommand = new SqlCommand(sql, conn);
                int a =(int)Mycommand.ExecuteNonQuery();
                conn.Close();
                return a;
                
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private DataTable RetrieveData(string sql)
        {
            try
            {
                DataTable dt = new System.Data.DataTable();
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataAdapter sda = new SqlDataAdapter(sc);
                sda.Fill(dt);
                conn.Close();
                sda.Dispose();
                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void Stockin_Click(object sender, EventArgs e)
        {
            Stockin.Enabled = false;
            if (string.IsNullOrEmpty(MO.Text.ToString()) || string.IsNullOrEmpty(Batchnum.Text.ToString())|| string.IsNullOrEmpty(Materialcode.Text.ToString()))
            {
                MessageBox.Show("MO,MaterialCode and Batch No. can not be empty");
            }
            else
            {
                string sql = "insert into [CAPA01DB01].[dbo].[RawMaterial] values('{0}','{1}','{2}','{3}','{4}',getdate(),null,null,null,'{5}')";
                sql = string.Format(sql, Material.Text.ToString(), Materialcode.Text.ToString(),Batchnum.Text.ToString(),SUNum.Text.ToString(), Convert.ToDouble(Qty.Text),MO.Text.ToString());
                Executenonequery(sql);
               
                string sqlselect = "SELECT  [ID],[RM],[MO],[MtrlCode],[BatchNum],[SU],[UnitQty],[CreatedDate],[ModifiedDate],[OutQty],[OutDate] FROM [CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM = '" + Material.Text + "' and  MtrlCode = '" + Materialcode.Text + "' and BatchNum = '"+Batchnum.Text+"' and SU = '"+SUNum.Text+"' and CreatedDate >= GETDATE()-0.1";
                DataTable dt = RetrieveData(sqlselect);
             
                    MessageBox.Show(Material.Text+" is stocked in");
                    Materialcode.Clear();
                    SUNum.Clear();
                    Batchnum.Clear();               
                dataGridView1.DataSource = dt;


            }
            Stockin.Enabled = true;
           
        }

        private void Stockout_Click(object sender, EventArgs e)
        {
            Stockout.Enabled = false;
            if (string.IsNullOrEmpty(MO.Text.ToString()) || string.IsNullOrEmpty(Batchnum.Text.ToString()) || string.IsNullOrEmpty(Materialcode.Text.ToString()))
            {
                MessageBox.Show("MO,MaterialCode and Batch No. can not be empty");
            }
            else
            {
                string sql = "Update [CAPA01DB01].[dbo].[RawMaterial] set outdate = getdate(),unitqty = unitqty-"+Convert.ToDouble(Qty.Text)+ ",outqty = isnull(outqty,0)+ " + Convert.ToDouble(Qty.Text) + " where  MO = '" + MO.Text + "' and RM = '" + Material.Text + "' and  MtrlCode = '" + Materialcode.Text + "' and BatchNum = '" + Batchnum.Text + "' and SU = '" + SUNum.Text + "' and isnull(outqty,0)+ " + Convert.ToDouble(Qty.Text) + "<=unitqty  ";
                
               int a= Executenonequery(sql);

                string sqlselect = "SELECT  [ID],[RM],[MO],[MtrlCode],[BatchNum],[SU],[UnitQty],[CreatedDate],[ModifiedDate],[OutQty],[OutDate] FROM [CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM = '" + Material.Text + "' and  MtrlCode = '" + Materialcode.Text + "' and BatchNum = '" + Batchnum.Text + "' and SU = '" + SUNum.Text + "' and outdate>=getdate()-0.01";
                DataTable dt = RetrieveData(sqlselect);
                if (a>0)
                {
                    MessageBox.Show(Material.Text + " is stocked out");
                    Materialcode.Clear();
                    SUNum.Clear();
                    Batchnum.Clear();
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show(Material.Text + " was not stocked in yet, or stock out quantity is over stock in quantity.");
                }                

            }
            Stockout.Enabled = true;
        }

        private void Modify_Click(object sender, EventArgs e)
        {
            Modify.Enabled = false;
            int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;

            DataGridViewRow selectedRow = dataGridView1.Rows[selectedrowindex];
            string sql = "Update  [CAPA01DB01].[dbo].[RawMaterial] set  mo = '" + selectedRow.Cells["mo"].Value.ToString() + "',mtrlcode = '" + selectedRow.Cells["mtrlcode"].Value.ToString() + "',batchnum = '" + selectedRow.Cells["batchnum"].Value.ToString() + "',SU = '" + selectedRow.Cells["SU"].Value.ToString() + "',unitqty = '" + selectedRow.Cells["unitqty"].Value + "',outqty = '" + selectedRow.Cells["outqty"].Value + "',ModifiedDate = GETDATE()  where  ID = '" + selectedRow.Cells["ID"].Value + "'";
            int a=Executenonequery(sql);
            string sqlselect = "SELECT  [ID],[RM],[MO],[MtrlCode],[BatchNum],[SU],[UnitQty],[CreatedDate],[ModifiedDate],[OutQty],[OutDate] FROM [CAPA01DB01].[dbo].[RawMaterial] where ID = '" + selectedRow.Cells["ID"].Value + "'";
            DataTable dt = RetrieveData(sqlselect);
            if (a>0)
            {
                MessageBox.Show("This row is Modified.");
                Materialcode.Clear();
                SUNum.Clear();
                Batchnum.Clear();
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("This row can not be modified.");
            }
            Modify.Enabled = true;
        }

        private void Review_Click(object sender, EventArgs e)
        {
            Review.Enabled = false;
            string sqlselect = "SELECT  [ID],[RM],[MO],[MtrlCode],[BatchNum],[SU],[UnitQty],[CreatedDate],[ModifiedDate],[OutQty],[OutDate] FROM [CAPA01DB01].[dbo].[RawMaterial] where RM='"+Material.Text+"' and mo='"+MO.Text+"'and (createddate>=getdate()-1 or outdate>=getdate()-1)";
            DataTable dt = RetrieveData(sqlselect);
            dataGridView1.DataSource = dt;
            if (Material.Text == "Glass")
            {
                string sql = "select sum(qty) as Qty from (select qty, OrderNo, CreatedOn, MaterialGroup, ROW_NUMBER()over(partition by orderNo order by createdon desc) rn from (select sum(cast(BatchQty as float)) as qty, OrderNo, CreatedOn, MaterialGroup from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where MaterialGroup = 1001 and OrderNo in (select orderno from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05 = '" + MO.Text + "') and OrderNo != '" + MO.Text + " '  group by createdon, OrderNo, MaterialGroup) tb1) tbfinal where rn = 1 group by MaterialGroup";
                SqlDataReader sdr= datareader(sql);

                while (sdr.Read())
                {
                    Mosize.Visible = true;
                    Mosize.Text = "Current Mo Size: " + sdr["Qty"].ToString() +" " +pcs.Text;
                }
                sdr.Close();

                string sql2 = "SELECT sum(isnull(UnitQty,0)) +sum(isnull(OutQty, 0)) as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='"+Material.Text+"' group by MO";
                SqlDataReader sdr2 = datareader2(sql2);
                if (sdr2.Read())
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: " + sdr2["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: 0 " + pcs.Text;
                }
                sdr2.Close();
                string sql3 = "SELECT sum(isnull(UnitQty,0))  as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr3 = datareader3(sql3);
                if (sdr3.Read())
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: " + sdr3["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: 0 "+ pcs.Text;
                }
                sdr3.Close();
            }
            if (Material.Text == "406 EVA")
            {
                string sql = "select sum(qty) as Qty from (select qty, OrderNo, CreatedOn, MaterialGroup, ROW_NUMBER()over(partition by orderNo order by createdon desc) rn from (select sum(cast(BatchQty as float)) as qty, OrderNo, CreatedOn, MaterialGroup from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where materialcode=11006159 and OrderNo in (select orderno from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05 = '" + MO.Text + "') and OrderNo != '" + MO.Text + " '  group by createdon, OrderNo, MaterialGroup) tb1) tbfinal where rn = 1 group by MaterialGroup";
                SqlDataReader sdr = datareader(sql);

                while (sdr.Read())
                {
                    Mosize.Visible = true;
                    Mosize.Text = "Current Mo Size: " + sdr["Qty"].ToString() + " " + pcs.Text;
                }
                sdr.Close();

                string sql2 = "SELECT sum(isnull(UnitQty,0)) +sum(isnull(OutQty, 0)) as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr2 = datareader2(sql2);
                if (sdr2.Read())
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: " + sdr2["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: 0 " + pcs.Text;
                }
                sdr2.Close();
                string sql3 = "SELECT sum(isnull(UnitQty,0))  as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr3 = datareader3(sql3);
                if (sdr3.Read())
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: " + sdr3["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: 0 " + pcs.Text;
                }
                sdr3.Close();
            }
            if (Material.Text == "806 EVA")
            {
                string sql = "select sum(qty) as Qty from (select qty, OrderNo, CreatedOn, MaterialGroup, ROW_NUMBER()over(partition by orderNo order by createdon desc) rn from (select sum(cast(BatchQty as float)) as qty, OrderNo, CreatedOn, MaterialGroup from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where MaterialCode = 11005260 and OrderNo in (select orderno from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05 = '" + MO.Text + "') and OrderNo != '" + MO.Text + " '  group by createdon, OrderNo, MaterialGroup) tb1) tbfinal where rn = 1 group by MaterialGroup";
                SqlDataReader sdr = datareader(sql);

                while (sdr.Read())
                {
                    Mosize.Visible = true;
                    Mosize.Text = "Current Mo Size: " + sdr["Qty"].ToString() + " " + pcs.Text;
                }
                sdr.Close();

                string sql2 = "SELECT sum(isnull(UnitQty,0)) +sum(isnull(OutQty, 0)) as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr2 = datareader2(sql2);
                if (sdr2.Read())
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: " + sdr2["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: 0 " + pcs.Text;
                }
                sdr2.Close();
                string sql3 = "SELECT sum(isnull(UnitQty,0))  as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr3 = datareader3(sql3);
                if (sdr3.Read())
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: " + sdr3["Qty"].ToString() +" "+ pcs.Text;
                }
                else
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: 0 " + pcs.Text;
                }
                sdr3.Close();
            }
            if (Material.Text == "Ribbon")
            {
                string sql = "select sum(qty) as Qty from (select qty, OrderNo, CreatedOn, MaterialGroup, ROW_NUMBER()over(partition by orderNo order by createdon desc) rn from (select sum(cast(BatchQty as float)) as qty, OrderNo, CreatedOn, MaterialGroup from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where  MaterialGroup = 1010 and OrderNo in (select orderno from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05 = '" + MO.Text + "') and OrderNo != '" + MO.Text + " '  group by createdon, OrderNo, MaterialGroup) tb1) tbfinal where rn = 1 group by MaterialGroup";
                SqlDataReader sdr = datareader(sql);

                while (sdr.Read())
                {
                    Mosize.Visible = true;
                    Mosize.Text = "Current Mo Size: " + sdr["Qty"].ToString() + " " + pcs.Text;
                }
                sdr.Close();

                string sql2 = "SELECT sum(isnull(UnitQty,0)) +sum(isnull(OutQty, 0)) as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr2 = datareader2(sql2);
                if (sdr2.Read())
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: " + sdr2["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: 0 " + pcs.Text;
                }
                sdr2.Close();
                string sql3 = "SELECT sum(isnull(UnitQty,0))  as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr3 = datareader3(sql3);
                if (sdr3.Read())
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: " + sdr3["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: 0 " + pcs.Text;
                }
                sdr3.Close();
            }
            if (Material.Text == "Backsheet")
            {
                string sql = "select sum(qty) as Qty from (select qty, OrderNo, CreatedOn, MaterialGroup, ROW_NUMBER()over(partition by orderNo order by createdon desc) rn from (select sum(cast(BatchQty as float)) as qty, OrderNo, CreatedOn, MaterialGroup from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where  MaterialGroup = 1003 and OrderNo in (select orderno from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05 = '" + MO.Text + "') and OrderNo != '" + MO.Text + " '  group by createdon, OrderNo, MaterialGroup) tb1) tbfinal where rn = 1 group by MaterialGroup";
                SqlDataReader sdr = datareader(sql);

                while (sdr.Read())
                {
                    Mosize.Visible = true;
                    Mosize.Text = "Current Mo Size: " + sdr["Qty"].ToString() + " " + pcs.Text;
                }
                sdr.Close();

                string sql2 = "SELECT sum(isnull(UnitQty,0)) +sum(isnull(OutQty, 0)) as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr2 = datareader2(sql2);
                if (sdr2.Read())
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: " + sdr2["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: 0 " + pcs.Text;
                }
                sdr2.Close();
                string sql3 = "SELECT sum(isnull(UnitQty,0))  as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr3 = datareader3(sql3);
                if (sdr3.Read())
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: " + sdr3["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: 0 " + pcs.Text;
                }
                sdr3.Close();
            }
            if (Material.Text == "J-Box")
            {
                string sql = "select sum(qty) as Qty from (select qty, OrderNo, CreatedOn, MaterialGroup, ROW_NUMBER()over(partition by orderNo order by createdon desc) rn from (select sum(cast(BatchQty as float)) as qty, OrderNo, CreatedOn, MaterialGroup from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where  MaterialGroup = 1004 and OrderNo in (select orderno from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05 = '" + MO.Text + "') and OrderNo != '" + MO.Text + " '  group by createdon, OrderNo, MaterialGroup) tb1) tbfinal where rn = 1 group by MaterialGroup";
                SqlDataReader sdr = datareader(sql);

                while (sdr.Read())
                {
                    Mosize.Visible = true;
                    Mosize.Text = "Current Mo Size: " + sdr["Qty"].ToString() + " " + pcs.Text;
                }
                sdr.Close();

                string sql2 = "SELECT sum(isnull(UnitQty,0)) +sum(isnull(OutQty, 0)) as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr2 = datareader2(sql2);
                if (sdr2.Read())
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: " + sdr2["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: 0 " + pcs.Text;
                }
                sdr2.Close();
                string sql3 = "SELECT sum(isnull(UnitQty,0))  as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr3 = datareader3(sql3);
                if (sdr3.Read())
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: " + sdr3["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: 0 " + pcs.Text;
                }
                sdr3.Close();
            }
            if (Material.Text == "CrossBar")
            {
                string sql = "select sum(qty) as Qty from (select qty, OrderNo, CreatedOn, MaterialGroup, ROW_NUMBER()over(partition by orderNo order by createdon desc) rn from (select sum(cast(BatchQty as float)) as qty, OrderNo, CreatedOn, MaterialGroup from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where  MaterialCode = 11003848 and OrderNo in (select orderno from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05 = '" + MO.Text + "') and OrderNo != '" + MO.Text + " '  group by createdon, OrderNo, MaterialGroup) tb1) tbfinal where rn = 1 group by MaterialGroup";
                SqlDataReader sdr = datareader(sql);

                while (sdr.Read())
                {
                    Mosize.Visible = true;
                    Mosize.Text = "Current Mo Size: " + sdr["Qty"].ToString() + " " + pcs.Text;
                }
                sdr.Close();

                string sql2 = "SELECT sum(isnull(UnitQty,0)) +sum(isnull(OutQty, 0)) as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr2 = datareader2(sql2);
                if (sdr2.Read())
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: " + sdr2["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: 0 " + pcs.Text;
                }
                sdr2.Close();
                string sql3 = "SELECT sum(isnull(UnitQty,0))  as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr3 = datareader3(sql3);
                if (sdr3.Read())
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: " + sdr3["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: 0 " + pcs.Text;
                }
                sdr3.Close();
            }
            if (Material.Text == "Frame(long)")
            {
                string sql = "select sum(qty) as Qty from (select qty, OrderNo, CreatedOn, MaterialGroup, ROW_NUMBER()over(partition by orderNo order by createdon desc) rn from (select sum(cast(BatchQty as float)) as qty, OrderNo, CreatedOn, MaterialGroup from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where  MaterialCode = 11000357 and OrderNo in (select orderno from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05 = '" + MO.Text + "') and OrderNo != '" + MO.Text + " '  group by createdon, OrderNo, MaterialGroup) tb1) tbfinal where rn = 1 group by MaterialGroup";
                SqlDataReader sdr = datareader(sql);

                while (sdr.Read())
                {
                    Mosize.Visible = true;
                    Mosize.Text = "Current Mo Size: " + sdr["Qty"].ToString() + " " + pcs.Text;
                }
                sdr.Close();

                string sql2 = "SELECT sum(isnull(UnitQty,0)) +sum(isnull(OutQty, 0)) as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr2 = datareader2(sql2);
                if (sdr2.Read())
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: " + sdr2["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: 0 " + pcs.Text;
                }
                sdr2.Close();
                string sql3 = "SELECT sum(isnull(UnitQty,0))  as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr3 = datareader3(sql3);
                if (sdr3.Read())
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: " + sdr3["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: 0 " + pcs.Text;
                }
                sdr3.Close();
            }
            if (Material.Text == "Frame(short)")
            {
                string sql = "select sum(qty) as Qty from (select qty, OrderNo, CreatedOn, MaterialGroup, ROW_NUMBER()over(partition by orderNo order by createdon desc) rn from (select sum(cast(BatchQty as float)) as qty, OrderNo, CreatedOn, MaterialGroup from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where  MaterialCode = 11000360 and OrderNo in (select orderno from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05 = '" + MO.Text + "') and OrderNo != '" + MO.Text + " '  group by createdon, OrderNo, MaterialGroup) tb1) tbfinal where rn = 1 group by MaterialGroup";
                SqlDataReader sdr = datareader(sql);

                while (sdr.Read())
                {
                    Mosize.Visible = true;
                    Mosize.Text = "Current Mo Size: " + sdr["Qty"].ToString() + " " + pcs.Text;
                }
                sdr.Close();

                string sql2 = "SELECT sum(isnull(UnitQty,0)) +sum(isnull(OutQty, 0)) as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr2 = datareader2(sql2);
                if (sdr2.Read())
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: " + sdr2["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: 0 " + pcs.Text;
                }
                sdr2.Close();
                string sql3 = "SELECT sum(isnull(UnitQty,0))  as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr3 = datareader3(sql3);
                if (sdr3.Read())
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: " + sdr3["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: 0 " + pcs.Text;
                }
                sdr3.Close();
            }
            if (Material.Text == "Sealnat MS930")
            {
                string sql = "select sum(qty) as Qty from (select qty, OrderNo, CreatedOn, MaterialGroup, ROW_NUMBER()over(partition by orderNo order by createdon desc) rn from (select sum(cast(BatchQty as float)) as qty, OrderNo, CreatedOn, MaterialGroup from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where  MaterialCode = 11002671 and OrderNo in (select orderno from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05 = '" + MO.Text + "') and OrderNo != '" + MO.Text + " '  group by createdon, OrderNo, MaterialGroup) tb1) tbfinal where rn = 1 group by MaterialGroup";
                SqlDataReader sdr = datareader(sql);

                while (sdr.Read())
                {
                    Mosize.Visible = true;
                    Mosize.Text = "Current Mo Size: " + sdr["Qty"].ToString() + " " + pcs.Text;
                }
                sdr.Close();

                string sql2 = "SELECT sum(isnull(UnitQty,0)) +sum(isnull(OutQty, 0)) as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr2 = datareader2(sql2);
                if (sdr2.Read())
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: " + sdr2["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: 0 " + pcs.Text;
                }
                sdr2.Close();
                string sql3 = "SELECT sum(isnull(UnitQty,0))  as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr3 = datareader3(sql3);
                if (sdr3.Read())
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: " + sdr3["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: 0 " + pcs.Text;
                }
                sdr3.Close();
            }
            if (Material.Text == "Sealnat 1521A")
            {
                string sql = "select sum(qty) as Qty from (select qty, OrderNo, CreatedOn, MaterialGroup, ROW_NUMBER()over(partition by orderNo order by createdon desc) rn from (select sum(cast(BatchQty as float)) as qty, OrderNo, CreatedOn, MaterialGroup from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where  MaterialCode = 11006207 and OrderNo in (select orderno from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05 = '" + MO.Text + "') and OrderNo != '" + MO.Text + " '  group by createdon, OrderNo, MaterialGroup) tb1) tbfinal where rn = 1 group by MaterialGroup";
                SqlDataReader sdr = datareader(sql);

                while (sdr.Read())
                {
                    Mosize.Visible = true;
                    Mosize.Text = "Current Mo Size: " + sdr["Qty"].ToString() + " " + pcs.Text;
                }
                sdr.Close();

                string sql2 = "SELECT sum(isnull(UnitQty,0)) +sum(isnull(OutQty, 0)) as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr2 = datareader2(sql2);
                if (sdr2.Read())
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: " + sdr2["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: 0 " + pcs.Text;
                }
                sdr2.Close();
                string sql3 = "SELECT sum(isnull(UnitQty,0))  as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr3 = datareader3(sql3);
                if (sdr3.Read())
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: " + sdr3["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: 0 " + pcs.Text;
                }
                sdr3.Close();
            }
            if (Material.Text == "Sealnat 1527")
            {
                string sql = "select sum(qty) as Qty from (select qty, OrderNo, CreatedOn, MaterialGroup, ROW_NUMBER()over(partition by orderNo order by createdon desc) rn from (select sum(cast(BatchQty as float)) as qty, OrderNo, CreatedOn, MaterialGroup from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where  MaterialCode = 11004020 and OrderNo in (select orderno from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05 = '" + MO.Text + "') and OrderNo != '" + MO.Text + " '  group by createdon, OrderNo, MaterialGroup) tb1) tbfinal where rn = 1 group by MaterialGroup";
                SqlDataReader sdr = datareader(sql);

                while (sdr.Read())
                {
                    Mosize.Visible = true;
                    Mosize.Text = "Current Mo Size: " + sdr["Qty"].ToString() + " " + pcs.Text;
                }
                sdr.Close();

                string sql2 = "SELECT sum(isnull(UnitQty,0)) +sum(isnull(OutQty, 0)) as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr2 = datareader2(sql2);
                if (sdr2.Read())
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: " + sdr2["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: 0 " + pcs.Text;
                }
                sdr2.Close();
                string sql3 = "SELECT sum(isnull(UnitQty,0))  as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr3 = datareader3(sql3);
                if (sdr3.Read())
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: " + sdr3["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: 0 " + pcs.Text;
                }
                sdr3.Close();
            }
            if (Material.Text == "Sealnat 1521B")
            {
                string sql = "select sum(qty) as Qty from (select qty, OrderNo, CreatedOn, MaterialGroup, ROW_NUMBER()over(partition by orderNo order by createdon desc) rn from (select sum(cast(BatchQty as float)) as qty, OrderNo, CreatedOn, MaterialGroup from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where  MaterialCode = 11006208 and OrderNo in (select orderno from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05 = '" + MO.Text + "') and OrderNo != '" + MO.Text + " '  group by createdon, OrderNo, MaterialGroup) tb1) tbfinal where rn = 1 group by MaterialGroup";
                SqlDataReader sdr = datareader(sql);

                while (sdr.Read())
                {
                    Mosize.Visible = true;
                    Mosize.Text = "Current Mo Size: " + sdr["Qty"].ToString() + " " + pcs.Text;
                }
                sdr.Close();

                string sql2 = "SELECT sum(isnull(UnitQty,0)) +sum(isnull(OutQty, 0)) as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr2 = datareader2(sql2);
                if (sdr2.Read())
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: " + sdr2["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: 0 " + pcs.Text;
                }
                sdr2.Close();
                string sql3 = "SELECT sum(isnull(UnitQty,0))  as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr3 = datareader3(sql3);
                if (sdr3.Read())
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: " + sdr3["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: 0 " + pcs.Text;
                }
                sdr3.Close();
            }
            if (Material.Text == "Sealnat MS 9371B")
            {
                string sql = "select sum(qty) as Qty from (select qty, OrderNo, CreatedOn, MaterialGroup, ROW_NUMBER()over(partition by orderNo order by createdon desc) rn from (select sum(cast(BatchQty as float)) as qty, OrderNo, CreatedOn, MaterialGroup from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_BOM] where  MaterialCode = 11002672 and OrderNo in (select orderno from[csimes_SapInterface].[dbo].[TSAP_WORKORDER_SALES_REQUEST] where Resv05 = '" + MO.Text + "') and OrderNo != '" + MO.Text + " '  group by createdon, OrderNo, MaterialGroup) tb1) tbfinal where rn = 1 group by MaterialGroup";
                SqlDataReader sdr = datareader(sql);

                while (sdr.Read())
                {
                    Mosize.Visible = true;
                    Mosize.Text = "Current Mo Size: " + sdr["Qty"].ToString() + " " + pcs.Text;
                }
                sdr.Close();

                string sql2 = "SELECT sum(isnull(UnitQty,0)) +sum(isnull(OutQty, 0)) as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr2 = datareader2(sql2);
                if (sdr2.Read())
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: " + sdr2["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    Recieved.Visible = true;
                    Recieved.Text = "Received From Warehouse: 0 " + pcs.Text;
                }
                sdr2.Close();
                string sql3 = "SELECT sum(isnull(UnitQty,0))  as Qty FROM[CAPA01DB01].[dbo].[RawMaterial] where MO = '" + MO.Text + "' and RM='" + Material.Text + "' group by MO";
                SqlDataReader sdr3 = datareader3(sql3);
                if (sdr3.Read())
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: " + sdr3["Qty"].ToString() + " " + pcs.Text;
                }
                else
                {
                    ProdWarehouse.Visible = true;
                    ProdWarehouse.Text = "Production Warehouse Stock: 0 " + pcs.Text;
                }
                sdr3.Close();
            }

            Review.Enabled = true;
        }
        private SqlDataReader datareader(string sql)
        {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source = ca01s0015; Initial Catalog = csimes_SapInterface; User ID = reader; Password=12345");
               

                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataReader sdr = sc.ExecuteReader();                                                            
                return sdr;
            }

            catch (Exception e)
            {
                throw e;
            }
        }
        private SqlDataReader datareader2(string sql)
        {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");                
                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataReader sdr = sc.ExecuteReader();
                return sdr;                
            }

            catch (Exception e)
            {
                throw e;
            }
        }
        private SqlDataReader datareader3(string sql)
        {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source = ca01a0047; Initial Catalog = CAPA01DB01; User ID = PowerDistribution; Password=Power123");
                conn.Open();
                SqlCommand sc = new SqlCommand(sql, conn);
                SqlDataReader sdr = sc.ExecuteReader();
                return sdr;
            }

            catch (Exception e)
            {
                throw e;
            }
        }
        
	
        
        private string getShift(DateTime dt)
        {
        	int[] S2 = {0,1,4,5,6,9,10};
        	string OneTwo = "";
        	string DN = "";
        	
        	
        	string date = dt.ToString("HH:mm:ss");
        	if(Convert.ToInt16(date.Substring(0,2)) >= 7 && Convert.ToInt16(date.Substring(0,2)) <19)
        		DN = "D";
        	else
        	{
        		DN = "N";
        		if (Convert.ToInt16(date.Substring(0,2)) < 7) {        			
        			dt = dt.AddDays(-1);
        		}        		
        	}
        	
        	var Days = (dt.Date- Convert.ToDateTime("2016-08-01")).TotalDays;
        	int Mod = Convert.ToInt16(Days) % 14;
        	if (S2.Contains(Mod)) 
        		OneTwo = "2";
        	else 
        		OneTwo = "1";
        	
        	return DN+OneTwo;
        	
        }
        
        private string getDate(DateTime dt)
        {
        	bool Finish = false;
        	while(!Finish)
        	{
        		if(getShift(dt) == CbShift.Text)
        			Finish = true;
        		else
        		{
        			dt = dt.AddHours(-12);
        		}
        	}
        	return dt.ToString("yyyy-MM-dd HH:mm:ss.000");
        }
		void WHRClick(object sender, EventArgs e)
		{
			WHR.Enabled = false;
			if (string.IsNullOrEmpty(Materialcode.Text) || string.IsNullOrEmpty(Batch.Text) || string.IsNullOrEmpty(SUNum.Text) ||string.IsNullOrEmpty(MO.Text.ToString()) || string.IsNullOrEmpty(CbShift.Text.ToString()))
            {
                MessageBox.Show("MO and Shift can not be empty");
            }
            else
            {
                string sql = "insert into [CAPA01DB01].[dbo].[QRWHR] values('{0}','{1}','{2}','{3}','{4}','{5}',null,'{6}','{7}',null)";
                if (Material.Text == "406 EVA" || Material.Text == "806 EVA")
                {
                    sql = string.Format(sql, Material.Text.ToString(), MO.Text.ToString(),Materialcode.Text,Batchnum.Text,SUNum.Text, CbShift.Text.ToString(), Convert.ToDouble(Qty.Text) * 180 / 60,getDate(DateTime.Now));
                    //Conversion.Text = "EVA: 60Kg = 180m²";
                    //Conversion.Visible = true;
                }
                if (Material.Text == "Backsheet")
                {
                    sql = string.Format(sql, Material.Text.ToString(), MO.Text.ToString(), Materialcode.Text,Batchnum.Text,SUNum.Text, CbShift.Text.ToString(), Convert.ToDouble(Qty.Text) * 400 / 173,getDate(DateTime.Now));
                    //Conversion.Text = "Backsheet: 173Kg = 400m²";
                    //Conversion.Visible = true;
                }
                else
                {
                    sql = string.Format(sql,  Material.Text.ToString(), MO.Text.ToString(), Materialcode.Text,Batchnum.Text,SUNum.Text, CbShift.Text.ToString(), Convert.ToDouble(Qty.Text),getDate(DateTime.Now));
                }
                Clipboard.SetText(sql);
                Executenonequery(sql);

                string sqlselect = "SELECT  [RM],[MO],[MaterialCode],[Batch],[SUNumber],[Shift],[QRQty],[WHRQty],[CreatedDate],[ModifiedDate] FROM [CAPA01DB01].[dbo].[QRWHR] where MO = '" + MO.Text + "' and RM = '" + Material.Text + "' and  shift = '" + CbShift.Text + "' and CreatedDate >= GETDATE()-1";
                DataTable dt = RetrieveData(sqlselect);
                MessageBox.Show(Material.Text + " Return to Warehouse");
                dataGridView1.DataSource = dt;
            }
            WHR.Enabled = true;
		}
		void QRClick(object sender, EventArgs e)
		{
			QR.Enabled = false;
            if (string.IsNullOrEmpty(Materialcode.Text) || string.IsNullOrEmpty(Batch.Text) || string.IsNullOrEmpty(SUNum.Text) || string.IsNullOrEmpty(MO.Text.ToString()) || string.IsNullOrEmpty(CbShift.Text.ToString()))
            {
                MessageBox.Show("MO and Shift can not be empty");
            }
            else if (Materialcode.Text.Length > 8) 
            {
            	MessageBox.Show("MaterialCode too long");
            }
            else
            {
                string sql = "insert into [CAPA01DB01].[dbo].[QRWHR] values('{0}','{1}','{2}','{3}','{4}','{5}','{6}',null,'{7}',null)";
                if (Material.Text == "406 EVA" || Material.Text == "806 EVA")
                {
                	sql = string.Format(sql, Material.Text.ToString(), MO.Text.ToString(),Materialcode.Text,Batchnum.Text,SUNum.Text, CbShift.Text.ToString(), Convert.ToDouble(Qty.Text) * 180 / 60,getDate(DateTime.Now));
                    //Conversion.Text = "EVA: 60Kg = 180m²";
                    //Conversion.Visible = true;
                }
                if (Material.Text == "Backsheet")
                {
                    sql = string.Format(sql, Material.Text.ToString(), MO.Text.ToString(), Materialcode.Text,Batchnum.Text,SUNum.Text, CbShift.Text.ToString(), Convert.ToDouble(Qty.Text) * 400 / 173,getDate(DateTime.Now));
                    //Conversion.Text = "Backsheet: 173Kg = 400m²";
                    //Conversion.Visible = true;
                }
                else
                {
                    sql = string.Format(sql,  Material.Text.ToString(), MO.Text.ToString(), Materialcode.Text,Batchnum.Text,SUNum.Text, CbShift.Text.ToString(), Convert.ToDouble(Qty.Text),getDate(DateTime.Now));
                }
                Executenonequery(sql);

                string sqlselect = "SELECT  [RM],[MO],[MaterialCode],[Batch],[SUNumber],[Shift],[QRQty],[WHRQty],[CreatedDate],[ModifiedDate] FROM [CAPA01DB01].[dbo].[QRWHR] where MO = '" + MO.Text + "' and RM = '" + Material.Text + "' and  shift = '" + CbShift.Text + "' and CreatedDate >= GETDATE()-1";
                DataTable dt = RetrieveData(sqlselect);
                MessageBox.Show(Material.Text + " is rejected by Quality");
                dataGridView1.DataSource = dt;
            }
            QR.Enabled = true;
		}
	
	
    }
}
