﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using LablePLibrary;


namespace CSICPR
{
    public partial class FormConfig : Form
    {
        private static List<LanguageItemModel> LanMessList;//定义语言集
        private string xmlFile = Application.StartupPath + "\\ParametersSetting.xml";
		private string labelpath = Application.StartupPath + "\\Excel";
        private static string ModuleLabelPath;//组件标签路径
        private static string CartonLabelPath;//箱号标签路径(A5)
        private static string CartonLabelParameter;//箱号参数名
        private static string CheckRule; //XML里 属性:是否检查工单; 值:组件工单检查,
        private static int CartonQty; //装箱组件数量
        private static int MixCount; //XML里 属性:是否检查混装;值:混装数量
        private static string IdealPower; //标称功率

        private static int PrintMode;
        private static int LabelFormat;
        
        private static bool mixChecked;
        private static bool CheckNoChecked;
        private string tempF1;//箱号中的车间代码
        private string tempF2;//常规箱号序列号
        private string tempF3;//箱号中的颜色代码
        private string tempF4; //箱号序列号最大值
        public bool isChange = false;

        public FormConfig(string ShowControl)
        {
            InitializeComponent();
            //CbLine.Enabled = false;
            ModuleLabelPath = "";
            CartonLabelPath = "";
            CartonLabelParameter = "";
            CheckRule = "------***";
            CartonQty = 0;
            IdealPower = "0";
            MixCount = 0;
            PrintMode = 0;
          
            if (System.IO.File.Exists(xmlFile))
            {
                ModuleLabelPath = ToolsClass.getConfig("ModuleLabelPath");
                CartonLabelPath = ToolsClass.getConfig("CartonLabelPath");
                CartonLabelParameter = ToolsClass.getConfig("CartonLabelParameter");
                CheckRule = ToolsClass.getConfig("CheckRule");
                CheckNoChecked = bool.Parse(ToolsClass.getConfig("CheckRule", true, "Checked"));
                CartonQty = int.Parse(ToolsClass.getConfig("CartonQty"));
                IdealPower = ToolsClass.getConfig("IdealPower");
                MixCount = int.Parse(ToolsClass.getConfig("MixCount"));
                mixChecked = bool.Parse(ToolsClass.getConfig("MixCount", true, "Checked"));

                tbCPTLabpath.Text = ModuleLabelPath;
                tbBoxLabpath.Text = CartonLabelPath;
                tbParBox.Text = CartonLabelParameter;
                txtCheckRule.Text = CheckRule;
                nudCartonQty.Value = CartonQty;

                nudMixCnt.Value = MixCount;
                cbMix.Checked = mixChecked;
                cbCheckNo.Checked = CheckNoChecked;
                this.txtCheckRule.Enabled = CheckNoChecked;
				
                CbLine.Text = ToolsClass.getConfig("Line");
                
                rbModel.Checked = bool.Parse(ToolsClass.getConfig("Model"));//常规标签
                rbTrueP.Checked = bool.Parse(ToolsClass.getConfig("RealPower"));//实测标签
                rbNomalP.Checked = bool.Parse(ToolsClass.getConfig("NominalPower"));//标称标签

                rbIndividual.Checked = bool.Parse(ToolsClass.getConfig("OnePrint"));//单个标签打印
                rbBatch.Checked = bool.Parse(ToolsClass.getConfig("BatchPrint"));//批量标签打印
                rbForbid.Checked = bool.Parse(ToolsClass.getConfig("NoPrint"));//不打印标签
                rbLargeLabelPrint.Checked = bool.Parse(ToolsClass.getConfig("LargeLabelPrint"));//A5标签打印																//modified by Shen Yu July 15, 2011
                CbPackType.Text = ToolsClass.getConfig("PType");
                CbCell.Text = ToolsClass.getConfig("CellVendor");
                CbGlassType.Text = ToolsClass.getConfig("GlassType");
                CbCertificate.Text = ToolsClass.getConfig("Certificate");                
                
                if (ToolsClass.sSite == ToolsClass.CAMO)
                {
                    tempF1 = tbF1BoxCode.Text = ToolsClass.getConfig("StationNo").Replace('\n', ' ').Trim();
                    cbCartonGrade.Text = ToolsClass.getConfig("ModuleClass");           
                    this.tbF2BoxCode.Enabled = false;
                    this.tbF3BoxCode.Enabled = false;
                    this.txtLabelCnt.Enabled = false;
                    this.nudH.Enabled = false;
                    this.nudV.Enabled = false;
                    this.nudH.Value = int.Parse(ToolsClass.getConfig("ExcelH"));
                    this.nudV.Value = int.Parse(ToolsClass.getConfig("ExcelV"));
                }
                else
                {
                    tempF1 = tbF1BoxCode.Text = ToolsClass.getConfig("FirstCode").Replace('\n', ' ').Trim();
                    tempF2 = tbF2BoxCode.Text = ToolsClass.getConfig("SerialCode", true, "initCnt").Trim();
                    tempF3 = tbF3BoxCode.Text = ToolsClass.getConfig("ColorCode").Replace('\n', ' ').Trim();
                    tempF4 = this.txtLabelCnt.Text = ToolsClass.getConfig("LabelCount").Trim();
                    cbCartonGrade.Text = ToolsClass.getConfig("ModuleClass");
                    this.nudH.Value = 1;
                    this.nudV.Value = 1;
                    this.lblStationNo.Text = "年份和车间:";
                    this.cbCartonGrade.Enabled = true;
                    this.nudH.Value = int.Parse(ToolsClass.getConfig("ExcelH"));
                    this.nudV.Value = int.Parse(ToolsClass.getConfig("ExcelV"));
                }

                rbHandPacking.Checked = bool.Parse(ToolsClass.getConfig("ManualMode"));
                rbAutoPacking.Checked = bool.Parse(ToolsClass.getConfig("AutoMode"));
                nudPrintNum.Value = int.Parse(ToolsClass.getConfig("PrintCount"));

                if (cbMix.Checked == false)
                    nudMixCnt.Value = 1;
                if (rbLargeLabelPrint.Checked)
                    tbCPTLabpath.Enabled = false;
                try
                {
                    txtSchNo.Text = ToolsClass.getConfig("SchSerialCode");
                    txtSupply.Text = ToolsClass.getConfig("Supplier");
                }
                catch
                {
                }
            }
            if (ShowControl.Equals("Y"))
            {
                this.groupBox4.Enabled = false;
                this.groupBox1.Enabled = false;
                //this.label6.Enabled = false;
                //this.nudCartonQty.Enabled = false;
                this.cbMix.Enabled = false;
                this.nudMixCnt.Enabled = false;
                this.cbCheckNo.Enabled = false;
                this.txtCheckRule.Enabled = false;
                this.label1.Enabled = false;

                this.rbModel.Enabled = false;
                this.rbTrueP.Enabled = false;
                this.rbNomalP.Enabled = false;
                this.label7.Enabled = false;
                this.tbBoxLabpath.Enabled = false;
                this.label5.Enabled = false;
                this.tbParBox.Enabled = false;

                //this.btTestPrint.Enabled = false;
            }
        }

        private void FormConfig_Load(object sender, EventArgs e)
        {
            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion
            tbParBox.Focus();
            isChange = false;

            DataTable dt = ProductStorageDAL.GetModuleClassInfo("ModuleClass");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach(DataRow row in dt.Rows)
                {
                    if (!this.cbCartonGrade.Items.Contains(Convert.ToString(row["MAPPING_KEY_01"]).Trim()))
                        this.cbCartonGrade.Items.Add(Convert.ToString(row["MAPPING_KEY_01"]).Trim());
                }
                try
                {
                    cbCartonGrade.Text = ToolsClass.getConfig("ModuleClass");
                    cbModuleClass.Checked = bool.Parse(ToolsClass.getConfig("CheckModuleClass"));
                }
                catch
                { }
            }
            else
            {
                MessageBox.Show("组件等级没有维护，请联系系统管理员");
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (!System.IO.File.Exists(xmlFile))
            {
                string sDataSet = "";
                ModuleLabelPath = tbCPTLabpath.Text;
                CartonLabelPath = tbBoxLabpath.Text;
                CartonLabelParameter = tbParBox.Text;
                CheckRule = txtCheckRule.Text;
                CartonQty = (int)nudCartonQty.Value;
                IdealPower = "230";
                MixCount = (int)nudMixCnt.Value;
                CheckNoChecked = cbCheckNo.Checked;
                mixChecked = cbMix.Checked;
                try
                {
                    long itemp = long.Parse((tbF2BoxCode.Text.Trim()));
                }
                catch
                {
                    MessageBox.Show("常规箱号[流水号]必须是整数", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                try
                {
                    long itemp = long.Parse((this.txtLabelCnt.Text.Trim()));
                }
                catch
                {
                    MessageBox.Show("常规箱号[产生数量]必须是整数", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                try
                {
                    long itemp = long.Parse((this.txtSchNo.Text.Trim()));
                }
                catch
                {
                    MessageBox.Show("旭格箱号的流水号必须是数字", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                sDataSet = CartonQty.ToString() + "|" + IdealPower.ToString() + "|" + MixCount.ToString() + "|" + ModuleLabelPath + "|" + CartonLabelPath + "|" + CartonLabelParameter + "|" + CheckRule + "|" + mixChecked.ToString() + "|" + CheckNoChecked.ToString() + "|";
                sDataSet = sDataSet + rbIndividual.Checked.ToString() + "|" + rbBatch.Checked.ToString() + "|" + rbForbid.Checked.ToString() + "|" + rbLargeLabelPrint.Checked.ToString() + "|" + rbModel.Checked.ToString() + "|" + rbTrueP.Checked.ToString() + "|" + rbNomalP.Checked.ToString() + "|";   	//modified by Shen Yu on July 15, 2011, added rbLargeLabelPrint
                sDataSet = sDataSet + tbF1BoxCode.Text.Trim() + "|" + tbF2BoxCode.Text.Trim() + "|" + tbF3BoxCode.Text.Trim() + "|";
                sDataSet = sDataSet + rbAutoPacking.Checked.ToString() + "|" + rbHandPacking.Checked.ToString() + "|" + nudPrintNum.Value.ToString() + "|";
                sDataSet = sDataSet + this.txtLabelCnt.Text + "|" + this.nudH.ToString() + "|" + this.nudV.ToString() + "|" + this.txtSupply.Text.Trim() + "|" + this.txtSchNo.Text.Trim();
                ToolsClass.setConfig(sDataSet);
            }
            else
                isSaveChange();
            this.Close();
        }

        private void btTestPrint_Click(object sender, EventArgs e)
        {
            if (tbCPTLabpath.TextLength == 0)
            {
                FormMain.showTipMSG("Setting Module Label File Path", this.tbCPTLabpath, "Setting Module Label", ToolTipIcon.Warning);
                return;
            }
            if (tbBoxLabpath.TextLength == 0)
            {
                FormMain.showTipMSG("Setting Carton Label File Path", this.tbBoxLabpath, "Setting Carton Label", ToolTipIcon.Warning);
                return;
            }
            if (tbParBox.TextLength == 0)
            {
                FormMain.showTipMSG("Setting Carton Label File Parameter", this.tbParBox, "Setting Carton Label", ToolTipIcon.Warning);
                return;
            }
            ArrayList lst = new ArrayList();
            string sSN = "";
            if (ToolsClass.Like(tbCPTLabpath.Text, "Sch*co*"))
            {
                sSN = "1234567890A" + ":" + "235.9" + ":" + "29.10" + ":" + "8.11" + ":" + "36.71" + ":" + "8.68";
            }
            else
            {
                sSN = "1234567890A" + ":" + (rbModel.Checked ? "CS6P-XMP" : "123") + ":" + "156*125" + ":" + "P" + ":" + "Blue";
            }
            lst.Add(sSN);
            string msg = LablePrint.printingLable(tbCPTLabpath.Text, lst, ToolsClass.Like(tbCPTLabpath.Text, "Sch*co*"), 1);
            if (msg.Length > 0)
            {
                MessageBox.Show("Module Label Print Fail:" + msg, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                msg = LablePrint.printingLable(this.tbBoxLabpath.Text, this.tbParBox.Text, "1234567890A", 1);
                if (msg.Length > 0)
                {
                    MessageBox.Show("Carton Label Print Fail:" + msg, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        #region"Control Event"
        private void tbCPTLabpath_Enter(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                tb.Text = openFileDialog1.FileName;
            if (tb.Name != "tbCPTLabpath")
                tbParBox.Focus();
        }

        private void cbMix_CheckedChanged(object sender, EventArgs e)
        {
            nudMixCnt.Enabled = cbMix.Checked;
            if (cbMix.Checked == false)
                nudMixCnt.Value = 1;
            isChange = true;
        }

        private void cbCheckNo_CheckedChanged(object sender, EventArgs e)
        {
            txtCheckRule.Enabled = cbCheckNo.Checked;
            isChange = true;
        }

        private void nudCartonQty_KeyPress(object sender, KeyPressEventArgs e)
        {
            isChange = true;
        }

        private void nudIdealPower_KeyPress(object sender, KeyPressEventArgs e)
        {
            isChange = true;
        }

        private void nudMixCnt_KeyPress(object sender, KeyPressEventArgs e)
        {
            isChange = true;
        }

        private void txtCheckRule_KeyPress(object sender, KeyPressEventArgs e)
        {
            isChange = true;
        }

        private void tbCPTLabpath_TextChanged(object sender, EventArgs e)
        {
            isChange = true;
        }

        private void tbBoxLabpath_TextChanged(object sender, EventArgs e)
        {
            isChange = true;
        }

        private void tbParBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            isChange = true;
        }

        private void rbIndividual_CheckedChanged(object sender, EventArgs e)
        {
            isChange = true;
            this.tbCPTLabpath.Enabled = !rbLargeLabelPrint.Checked;
            this.tbParBox.Enabled = !rbLargeLabelPrint.Checked;
        }

        private void rbBatch_CheckedChanged(object sender, EventArgs e)
        {
            isChange = true;
            this.tbCPTLabpath.Enabled = !rbLargeLabelPrint.Checked;
            this.tbParBox.Enabled = !rbLargeLabelPrint.Checked;
        }

        private void rbLargeLabelPrint_CheckedChanged(object sender, EventArgs e)
        {
            isChange = true;
            this.tbCPTLabpath.Enabled = !rbLargeLabelPrint.Checked;
            this.tbParBox.Enabled = !rbLargeLabelPrint.Checked;
            if (rbLargeLabelPrint.Checked)
            {
                this.openFileDialog1.Filter = "All File(*.*)|*.*";
                this.openFileDialog1.Title = "Select Label to Use";
            }
            else
            {
                this.openFileDialog1.Filter = "Label File(*.lab)|*.lab";
                this.openFileDialog1.Title = "Select Label File to Use";
            }
        }

        private void rbForbid_CheckedChanged(object sender, EventArgs e)
        {
            isChange = true;
            this.tbCPTLabpath.Enabled = !rbLargeLabelPrint.Checked;
            this.tbParBox.Enabled = !rbLargeLabelPrint.Checked;
        }

        private void rbModel_CheckedChanged(object sender, EventArgs e)
        {
            isChange = true;
        }

        private void rbTrueP_CheckedChanged(object sender, EventArgs e)
        {
            isChange = true;
        }

        private void rbNomalP_CheckedChanged(object sender, EventArgs e)
        {
            isChange = true;
        }

        private void cbCurrent_CheckedChanged(object sender, EventArgs e)
        {
            isChange = true;
        }

        private void tbF1BoxCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            isChange = true;
        }

        private void tbF2BoxCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            isChange = true;
        }

        private void tbF3BoxCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            isChange = true;
        }

        private void nudBoxNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            isChange = true;
        }

        private void nudIdealPower_ValueChanged(object sender, EventArgs e)
        {
            isChange = true;
        }

        private void nudCartonQty_ValueChanged(object sender, EventArgs e)
        {
            isChange = true;
        }

        private void nudMixCnt_ValueChanged(object sender, EventArgs e)
        {
            isChange = true;
        }

        private void rbHandPacking_CheckedChanged(object sender, EventArgs e)
        {
            isChange = true;
        }

        private void rbAutoPacking_CheckedChanged(object sender, EventArgs e)
        {
            isChange = true;
        }

        private void nudPrintNum_ValueChanged(object sender, EventArgs e)
        {
            isChange = true;
        }

        private void cbCartonGrade_SelectedIndexChanged(object sender, EventArgs e)
        {
            isChange = true;
        }

        private void txtLabelCnt_KeyPress(object sender, KeyPressEventArgs e)
        {
            isChange = true;
        }

        private void nudH_ValueChanged(object sender, EventArgs e)
        {
            isChange = true;
        }

        private void nudV_ValueChanged(object sender, EventArgs e)
        {
            isChange = true;
        }

        private void txtSchNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            isChange = true;
        }

        private void txtSupply_KeyPress(object sender, KeyPressEventArgs e)
        {
            isChange = true;
        }
        #endregion

        /// <summary>
        /// 如果config变更，保存新的配置到xml文件
        /// </summary>
        private void isSaveChange()
        {          
            if (isChange)
            {
                #region
                if (System.IO.File.Exists(xmlFile))
                {
                    if (MessageBox.Show("Do you want to save package config ?", "Prompting Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {
                            long itemp = long.Parse((tbF2BoxCode.Text.Trim()));
                            if (this.tbF2BoxCode.Text.Trim().Length != 5)
                            {
                                MessageBox.Show("常规箱号[流水号]长度不对,必须是5", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                this.tbF2BoxCode.Focus();
                                return;
                            }
                        }
                        catch
                        {
                            MessageBox.Show("常规箱号[流水号]必须是整数", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }

                        try
                        {
                            long itemp = long.Parse((this.txtLabelCnt.Text.Trim()));
                        }
                        catch
                        {
                            MessageBox.Show("常规箱号[产生数量]必须是整数", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        try
                        {
                            long itemp = long.Parse((this.txtSchNo.Text.Trim()));
                        }
                        catch
                        {
                            MessageBox.Show("旭格箱号的[流水号]必须是整数", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }

                        if (this.tbF1BoxCode.Text.Trim().Length != 4)
                        {
                            MessageBox.Show("年份和车间的长度不对，必须为4", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            this.tbF1BoxCode.Focus();
                            return;
                        }

                        if (this.tbF3BoxCode.Text.Trim().Length != 1)
                        {
                            MessageBox.Show("颜色长度不对，必须为1", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            this.tbF3BoxCode.Focus();
                            return;
                        }

                        ToolsClass.saveXMLNode("CartonQty", nudCartonQty.Value.ToString());
                        ToolsClass.saveXMLNode("MixCount", nudMixCnt.Value.ToString());
                        ToolsClass.saveXMLNode("MixCount", cbMix.Checked.ToString(), true, "Checked");
                        ToolsClass.saveXMLNode("CheckRule", cbCheckNo.Checked.ToString(), true, "Checked");
                        ToolsClass.saveXMLNode("CheckRule", txtCheckRule.Text.Trim());
                        ToolsClass.saveXMLNode("ModuleLabelPath", tbCPTLabpath.Text.Trim());
                        ToolsClass.saveXMLNode("CartonLabelPath", tbBoxLabpath.Text.Trim());
                        ToolsClass.saveXMLNode("CartonLabelParameter", tbParBox.Text.Trim());
                        ToolsClass.saveXMLNode("OnePrint", rbIndividual.Checked.ToString());
                        ToolsClass.saveXMLNode("BatchPrint", rbBatch.Checked.ToString());
                        ToolsClass.saveXMLNode("NoPrint", rbForbid.Checked.ToString());
                        ToolsClass.saveXMLNode("LargeLabelPrint", rbLargeLabelPrint.Checked.ToString());
                        ToolsClass.saveXMLNode("Model", rbModel.Checked.ToString());
                        ToolsClass.saveXMLNode("RealPower", rbTrueP.Checked.ToString());
                        ToolsClass.saveXMLNode("NominalPower", rbNomalP.Checked.ToString());
                        ToolsClass.saveXMLNode("PType",CbPackType.Text.ToString());
                        ToolsClass.saveXMLNode("CellVendor",CbCell.Text.ToString());
                        ToolsClass.saveXMLNode("Certificate",CbCertificate.Text.ToString());
                        ToolsClass.saveXMLNode("GlassType",CbGlassType.Text.ToString());
                        ToolsClass.saveXMLNode("Line",CbLine.Text.ToString());
                        if (ToolsClass.sSite == ToolsClass.CAMO)
                        {
                            ToolsClass.saveXMLNode("StationNo", tbF1BoxCode.Text.Trim());
                            if (!tempF1.Equals(tbF1BoxCode.Text.Trim()))
                                ToolsClass.saveXMLNode("SerialCode", "1");
                        }
                        else
                        {
                            if (!tempF1.Equals(tbF1BoxCode.Text.Trim()))
                                ToolsClass.saveXMLNode("FirstCode", tbF1BoxCode.Text.Trim());
                            if (!tempF2.Equals(tbF2BoxCode.Text.Trim()))
                            {
                                ToolsClass.saveXMLNode("SerialCode", tbF2BoxCode.Text.Trim());
                                ToolsClass.saveXMLNode("SerialCode", tbF2BoxCode.Text.Trim(), true, "initCnt");
                            }
                            if (!tempF3.Equals(tbF3BoxCode.Text.Trim()))
                                ToolsClass.saveXMLNode("ColorCode", tbF3BoxCode.Text.Replace('\n', ' ').Trim());
                            if (!tempF4.Equals(txtLabelCnt.Text.Trim()))
                            {
                                ToolsClass.saveXMLNode("LabelCount", this.txtLabelCnt.Text.Trim());
                            }
                            ToolsClass.saveXMLNode("ExcelH", nudH.Value.ToString());
                            ToolsClass.saveXMLNode("ExcelV", nudV.Value.ToString());
                        }

                        ToolsClass.saveXMLNode("AutoMode", rbAutoPacking.Checked.ToString());
                        ToolsClass.saveXMLNode("ManualMode", rbHandPacking.Checked.ToString());
                        ToolsClass.saveXMLNode("PrintCount", nudPrintNum.Value.ToString());

                        ToolsClass.saveXMLNode("ModuleClass", cbCartonGrade.Text.Trim());
                        ToolsClass.saveXMLNode("CheckModuleClass", cbModuleClass.Checked.ToString());
                        try
                        {
                            ToolsClass.saveXMLNode("SchSerialCode", txtSchNo.Text.Trim());
                            ToolsClass.saveXMLNode("Supplier", txtSupply.Text.Trim());
                        }
                        catch { }
                        isChange = false;
                    }
                }
                #endregion
            }        
        }

        private void cbModuleClass_CheckedChanged(object sender, EventArgs e)
        {
            isChange = true;
        }
        
        void CbPackTypeSelectedIndexChanged(object sender, EventArgs e)
        {
        	if(CbPackType.SelectedIndex == 0)
        		tbBoxLabpath.Text = labelpath + "\\Vertical.xls";
        	else
        		tbBoxLabpath.Text = labelpath +"\\Horizontal.xls";
        	isChange = true;
        }
        
       
        
        void CbCellSelectedIndexChanged(object sender, EventArgs e)
        {
        	isChange = true;
        }
        
      
        
        void CbVoltageSelectedIndexChanged(object sender, EventArgs e)
        {
        	isChange = true;
        }
        
        void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
        {
        	isChange = true;
        }
		void CbCertificateSelectedIndexChanged(object sender, EventArgs e)
		{
			isChange = true;
		}
		void Label21Click(object sender, EventArgs e)
		{
	
		}
    }
}
