﻿namespace CSICPR
{
    partial class FrmSapInventoryUploadResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.pnlTop = new System.Windows.Forms.Panel();
        	this.comboBoxWorkLine = new System.Windows.Forms.ComboBox();
        	this.lblWorkLine = new System.Windows.Forms.Label();
        	this.rbtDateRange = new System.Windows.Forms.RadioButton();
        	this.rbtJobNo = new System.Windows.Forms.RadioButton();
        	this.gbTime = new System.Windows.Forms.GroupBox();
        	this.dptTo = new System.Windows.Forms.DateTimePicker();
        	this.dptFrom = new System.Windows.Forms.DateTimePicker();
        	this.lblDateTo = new System.Windows.Forms.Label();
        	this.lblDateFrom = new System.Windows.Forms.Label();
        	this.gbJobNo = new System.Windows.Forms.GroupBox();
        	this.txtJobNo = new System.Windows.Forms.TextBox();
        	this.lblJobNo = new System.Windows.Forms.Label();
        	this.btnReset = new System.Windows.Forms.Button();
        	this.btnQuery = new System.Windows.Forms.Button();
        	this.dgvData = new System.Windows.Forms.DataGridView();
        	this.colCommandName = new System.Windows.Forms.DataGridViewLinkColumn();
        	this.colJobNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colWorkshop = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colCreateOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colModifiedOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colStatusStep1Cn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colTimeStep1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colStatusStep2Cn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colTimeStep2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colStatusStep3Cn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colTimeStep3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colStatusStep4Cn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colTimeStep4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colStatusStep5Cn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colTimeStep5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colErrorMessage = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.lstView = new System.Windows.Forms.ListView();
        	this.pnlTop.SuspendLayout();
        	this.gbTime.SuspendLayout();
        	this.gbJobNo.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
        	this.SuspendLayout();
        	// 
        	// pnlTop
        	// 
        	this.pnlTop.Controls.Add(this.comboBoxWorkLine);
        	this.pnlTop.Controls.Add(this.lblWorkLine);
        	this.pnlTop.Controls.Add(this.rbtDateRange);
        	this.pnlTop.Controls.Add(this.rbtJobNo);
        	this.pnlTop.Controls.Add(this.gbTime);
        	this.pnlTop.Controls.Add(this.gbJobNo);
        	this.pnlTop.Controls.Add(this.btnReset);
        	this.pnlTop.Controls.Add(this.btnQuery);
        	this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
        	this.pnlTop.Location = new System.Drawing.Point(3, 3);
        	this.pnlTop.Name = "pnlTop";
        	this.pnlTop.Size = new System.Drawing.Size(933, 137);
        	this.pnlTop.TabIndex = 0;
        	// 
        	// comboBoxWorkLine
        	// 
        	this.comboBoxWorkLine.FormattingEnabled = true;
        	this.comboBoxWorkLine.Location = new System.Drawing.Point(572, 48);
        	this.comboBoxWorkLine.Name = "comboBoxWorkLine";
        	this.comboBoxWorkLine.Size = new System.Drawing.Size(117, 21);
        	this.comboBoxWorkLine.TabIndex = 56;
        	// 
        	// lblWorkLine
        	// 
        	this.lblWorkLine.AutoSize = true;
        	this.lblWorkLine.Location = new System.Drawing.Point(570, 12);
        	this.lblWorkLine.Name = "lblWorkLine";
        	this.lblWorkLine.Size = new System.Drawing.Size(31, 13);
        	this.lblWorkLine.TabIndex = 55;
        	this.lblWorkLine.Tag = "WorkLine";
        	this.lblWorkLine.Text = "线别";
        	// 
        	// rbtDateRange
        	// 
        	this.rbtDateRange.AutoSize = true;
        	this.rbtDateRange.Location = new System.Drawing.Point(241, 12);
        	this.rbtDateRange.Name = "rbtDateRange";
        	this.rbtDateRange.Size = new System.Drawing.Size(145, 17);
        	this.rbtDateRange.TabIndex = 53;
        	this.rbtDateRange.Text = "根据入库上传时间查询";
        	this.rbtDateRange.UseVisualStyleBackColor = true;
        	this.rbtDateRange.CheckedChanged += new System.EventHandler(this.rbtDateRange_CheckedChanged);
        	// 
        	// rbtJobNo
        	// 
        	this.rbtJobNo.AutoSize = true;
        	this.rbtJobNo.Checked = true;
        	this.rbtJobNo.Enabled = false;
        	this.rbtJobNo.Location = new System.Drawing.Point(31, 16);
        	this.rbtJobNo.Name = "rbtJobNo";
        	this.rbtJobNo.Size = new System.Drawing.Size(121, 17);
        	this.rbtJobNo.TabIndex = 53;
        	this.rbtJobNo.TabStop = true;
        	this.rbtJobNo.Text = "根据出货柜号查询";
        	this.rbtJobNo.UseVisualStyleBackColor = true;
        	this.rbtJobNo.CheckedChanged += new System.EventHandler(this.rbtJobNo_CheckedChanged);
        	// 
        	// gbTime
        	// 
        	this.gbTime.Controls.Add(this.dptTo);
        	this.gbTime.Controls.Add(this.dptFrom);
        	this.gbTime.Controls.Add(this.lblDateTo);
        	this.gbTime.Controls.Add(this.lblDateFrom);
        	this.gbTime.Enabled = false;
        	this.gbTime.Location = new System.Drawing.Point(241, 36);
        	this.gbTime.Name = "gbTime";
        	this.gbTime.Size = new System.Drawing.Size(308, 88);
        	this.gbTime.TabIndex = 54;
        	this.gbTime.TabStop = false;
        	// 
        	// dptTo
        	// 
        	this.dptTo.CustomFormat = "yyyy-MM-dd HH:mm:ss";
        	this.dptTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
        	this.dptTo.Location = new System.Drawing.Point(89, 47);
        	this.dptTo.Name = "dptTo";
        	this.dptTo.Size = new System.Drawing.Size(200, 20);
        	this.dptTo.TabIndex = 7;
        	// 
        	// dptFrom
        	// 
        	this.dptFrom.CustomFormat = "yyyy-MM-dd HH:mm:ss";
        	this.dptFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
        	this.dptFrom.Location = new System.Drawing.Point(89, 16);
        	this.dptFrom.Name = "dptFrom";
        	this.dptFrom.Size = new System.Drawing.Size(200, 20);
        	this.dptFrom.TabIndex = 7;
        	this.dptFrom.Value = new System.DateTime(2015, 12, 22, 16, 24, 15, 0);
        	// 
        	// lblDateTo
        	// 
        	this.lblDateTo.AutoSize = true;
        	this.lblDateTo.Location = new System.Drawing.Point(15, 51);
        	this.lblDateTo.Name = "lblDateTo";
        	this.lblDateTo.Size = new System.Drawing.Size(67, 13);
        	this.lblDateTo.TabIndex = 6;
        	this.lblDateTo.Text = "上传时间到";
        	// 
        	// lblDateFrom
        	// 
        	this.lblDateFrom.AutoSize = true;
        	this.lblDateFrom.Location = new System.Drawing.Point(15, 21);
        	this.lblDateFrom.Name = "lblDateFrom";
        	this.lblDateFrom.Size = new System.Drawing.Size(67, 13);
        	this.lblDateFrom.TabIndex = 6;
        	this.lblDateFrom.Text = "上传时间从";
        	// 
        	// gbJobNo
        	// 
        	this.gbJobNo.Controls.Add(this.txtJobNo);
        	this.gbJobNo.Controls.Add(this.lblJobNo);
        	this.gbJobNo.Location = new System.Drawing.Point(15, 36);
        	this.gbJobNo.Name = "gbJobNo";
        	this.gbJobNo.Size = new System.Drawing.Size(219, 88);
        	this.gbJobNo.TabIndex = 54;
        	this.gbJobNo.TabStop = false;
        	// 
        	// txtJobNo
        	// 
        	this.txtJobNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.txtJobNo.Location = new System.Drawing.Point(16, 51);
        	this.txtJobNo.MaxLength = 24;
        	this.txtJobNo.Name = "txtJobNo";
        	this.txtJobNo.Size = new System.Drawing.Size(170, 20);
        	this.txtJobNo.TabIndex = 52;
        	// 
        	// lblJobNo
        	// 
        	this.lblJobNo.AutoSize = true;
        	this.lblJobNo.Location = new System.Drawing.Point(16, 21);
        	this.lblJobNo.Name = "lblJobNo";
        	this.lblJobNo.Size = new System.Drawing.Size(64, 13);
        	this.lblJobNo.TabIndex = 6;
        	this.lblJobNo.Text = "出 货 柜 号";
        	// 
        	// btnReset
        	// 
        	this.btnReset.Location = new System.Drawing.Point(674, 106);
        	this.btnReset.Name = "btnReset";
        	this.btnReset.Size = new System.Drawing.Size(68, 24);
        	this.btnReset.TabIndex = 53;
        	this.btnReset.Text = "重置";
        	this.btnReset.UseVisualStyleBackColor = true;
        	this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
        	// 
        	// btnQuery
        	// 
        	this.btnQuery.Location = new System.Drawing.Point(572, 106);
        	this.btnQuery.Name = "btnQuery";
        	this.btnQuery.Size = new System.Drawing.Size(68, 24);
        	this.btnQuery.TabIndex = 2;
        	this.btnQuery.Text = "查询";
        	this.btnQuery.UseVisualStyleBackColor = true;
        	this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
        	// 
        	// dgvData
        	// 
        	this.dgvData.AllowUserToAddRows = false;
        	this.dgvData.AllowUserToDeleteRows = false;
        	this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        	this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
        	        	        	this.colCommandName,
        	        	        	this.colJobNo,
        	        	        	this.colWorkshop,
        	        	        	this.colCreateOn,
        	        	        	this.colModifiedOn,
        	        	        	this.colStatusStep1Cn,
        	        	        	this.colTimeStep1,
        	        	        	this.colStatusStep2Cn,
        	        	        	this.colTimeStep2,
        	        	        	this.colStatusStep3Cn,
        	        	        	this.colTimeStep3,
        	        	        	this.colStatusStep4Cn,
        	        	        	this.colTimeStep4,
        	        	        	this.colStatusStep5Cn,
        	        	        	this.colTimeStep5,
        	        	        	this.colErrorMessage});
        	this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.dgvData.Location = new System.Drawing.Point(3, 140);
        	this.dgvData.Name = "dgvData";
        	this.dgvData.ReadOnly = true;
        	this.dgvData.RowTemplate.Height = 23;
        	this.dgvData.Size = new System.Drawing.Size(933, 308);
        	this.dgvData.TabIndex = 1;
        	this.dgvData.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellContentClick);
        	// 
        	// colCommandName
        	// 
        	this.colCommandName.DataPropertyName = "CommandName";
        	this.colCommandName.HeaderText = "操作";
        	this.colCommandName.Name = "colCommandName";
        	this.colCommandName.ReadOnly = true;
        	this.colCommandName.Text = "重试";
        	this.colCommandName.UseColumnTextForLinkValue = true;
        	this.colCommandName.Width = 60;
        	// 
        	// colJobNo
        	// 
        	this.colJobNo.DataPropertyName = "JobNo";
        	this.colJobNo.HeaderText = "出货柜号";
        	this.colJobNo.Name = "colJobNo";
        	this.colJobNo.ReadOnly = true;
        	this.colJobNo.Width = 150;
        	// 
        	// colWorkshop
        	// 
        	this.colWorkshop.DataPropertyName = "Workshop";
        	this.colWorkshop.HeaderText = "车间";
        	this.colWorkshop.Name = "colWorkshop";
        	this.colWorkshop.ReadOnly = true;
        	this.colWorkshop.Width = 150;
        	// 
        	// colCreateOn
        	// 
        	this.colCreateOn.DataPropertyName = "CreateOn";
        	this.colCreateOn.HeaderText = "创建时间";
        	this.colCreateOn.Name = "colCreateOn";
        	this.colCreateOn.ReadOnly = true;
        	this.colCreateOn.Width = 150;
        	// 
        	// colModifiedOn
        	// 
        	this.colModifiedOn.DataPropertyName = "ModifiedOn";
        	this.colModifiedOn.HeaderText = "最后修改时间";
        	this.colModifiedOn.Name = "colModifiedOn";
        	this.colModifiedOn.ReadOnly = true;
        	this.colModifiedOn.Width = 150;
        	// 
        	// colStatusStep1Cn
        	// 
        	this.colStatusStep1Cn.DataPropertyName = "StatusStep1Cn";
        	this.colStatusStep1Cn.HeaderText = "接收状态";
        	this.colStatusStep1Cn.Name = "colStatusStep1Cn";
        	this.colStatusStep1Cn.ReadOnly = true;
        	this.colStatusStep1Cn.Width = 150;
        	// 
        	// colTimeStep1
        	// 
        	this.colTimeStep1.DataPropertyName = "TimeStep1";
        	this.colTimeStep1.HeaderText = "接收时间";
        	this.colTimeStep1.Name = "colTimeStep1";
        	this.colTimeStep1.ReadOnly = true;
        	this.colTimeStep1.Width = 150;
        	// 
        	// colStatusStep2Cn
        	// 
        	this.colStatusStep2Cn.DataPropertyName = "StatusStep2Cn";
        	this.colStatusStep2Cn.HeaderText = "申请批次状态";
        	this.colStatusStep2Cn.Name = "colStatusStep2Cn";
        	this.colStatusStep2Cn.ReadOnly = true;
        	this.colStatusStep2Cn.Width = 150;
        	// 
        	// colTimeStep2
        	// 
        	this.colTimeStep2.DataPropertyName = "TimeStep2";
        	this.colTimeStep2.HeaderText = "申请批次时间";
        	this.colTimeStep2.Name = "colTimeStep2";
        	this.colTimeStep2.ReadOnly = true;
        	this.colTimeStep2.Width = 150;
        	// 
        	// colStatusStep3Cn
        	// 
        	this.colStatusStep3Cn.DataPropertyName = "StatusStep3Cn";
        	this.colStatusStep3Cn.HeaderText = "批次入库状态";
        	this.colStatusStep3Cn.Name = "colStatusStep3Cn";
        	this.colStatusStep3Cn.ReadOnly = true;
        	this.colStatusStep3Cn.Width = 150;
        	// 
        	// colTimeStep3
        	// 
        	this.colTimeStep3.DataPropertyName = "TimeStep3";
        	this.colTimeStep3.HeaderText = "批次入库时间";
        	this.colTimeStep3.Name = "colTimeStep3";
        	this.colTimeStep3.ReadOnly = true;
        	this.colTimeStep3.Width = 150;
        	// 
        	// colStatusStep4Cn
        	// 
        	this.colStatusStep4Cn.DataPropertyName = "StatusStep4Cn";
        	this.colStatusStep4Cn.HeaderText = "二级包装状态";
        	this.colStatusStep4Cn.Name = "colStatusStep4Cn";
        	this.colStatusStep4Cn.ReadOnly = true;
        	this.colStatusStep4Cn.Width = 150;
        	// 
        	// colTimeStep4
        	// 
        	this.colTimeStep4.DataPropertyName = "TimeStep4";
        	this.colTimeStep4.HeaderText = "二级包装时间";
        	this.colTimeStep4.Name = "colTimeStep4";
        	this.colTimeStep4.ReadOnly = true;
        	this.colTimeStep4.Width = 150;
        	// 
        	// colStatusStep5Cn
        	// 
        	this.colStatusStep5Cn.DataPropertyName = "StatusStep5Cn";
        	this.colStatusStep5Cn.HeaderText = "写入Tracking系统状态";
        	this.colStatusStep5Cn.Name = "colStatusStep5Cn";
        	this.colStatusStep5Cn.ReadOnly = true;
        	this.colStatusStep5Cn.Width = 150;
        	// 
        	// colTimeStep5
        	// 
        	this.colTimeStep5.DataPropertyName = "TimeStep5";
        	this.colTimeStep5.HeaderText = "写入Tracking系统时间";
        	this.colTimeStep5.Name = "colTimeStep5";
        	this.colTimeStep5.ReadOnly = true;
        	this.colTimeStep5.Width = 150;
        	// 
        	// colErrorMessage
        	// 
        	this.colErrorMessage.DataPropertyName = "ErrorMessage";
        	this.colErrorMessage.HeaderText = "异常信息";
        	this.colErrorMessage.Name = "colErrorMessage";
        	this.colErrorMessage.ReadOnly = true;
        	this.colErrorMessage.Width = 150;
        	// 
        	// lstView
        	// 
        	this.lstView.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.lstView.Dock = System.Windows.Forms.DockStyle.Bottom;
        	this.lstView.FullRowSelect = true;
        	this.lstView.Location = new System.Drawing.Point(3, 448);
        	this.lstView.MultiSelect = false;
        	this.lstView.Name = "lstView";
        	this.lstView.Size = new System.Drawing.Size(933, 88);
        	this.lstView.TabIndex = 3;
        	this.lstView.UseCompatibleStateImageBehavior = false;
        	this.lstView.View = System.Windows.Forms.View.Details;
        	this.lstView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstView_KeyDown);
        	// 
        	// FrmSapInventoryUploadResult
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(939, 539);
        	this.Controls.Add(this.dgvData);
        	this.Controls.Add(this.pnlTop);
        	this.Controls.Add(this.lstView);
        	this.Name = "FrmSapInventoryUploadResult";
        	this.Padding = new System.Windows.Forms.Padding(3);
        	this.ShowInTaskbar = false;
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "入库上传SAP结果查询";
        	this.Load += new System.EventHandler(this.FrmSapInventoryUploadResult_Load);
        	this.pnlTop.ResumeLayout(false);
        	this.pnlTop.PerformLayout();
        	this.gbTime.ResumeLayout(false);
        	this.gbTime.PerformLayout();
        	this.gbJobNo.ResumeLayout(false);
        	this.gbJobNo.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
        	this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.ListView lstView;
        private System.Windows.Forms.RadioButton rbtDateRange;
        private System.Windows.Forms.RadioButton rbtJobNo;
        private System.Windows.Forms.GroupBox gbTime;
        private System.Windows.Forms.Label lblDateTo;
        private System.Windows.Forms.Label lblDateFrom;
        private System.Windows.Forms.GroupBox gbJobNo;
        private System.Windows.Forms.TextBox txtJobNo;
        private System.Windows.Forms.Label lblJobNo;
        private System.Windows.Forms.DateTimePicker dptTo;
        private System.Windows.Forms.DateTimePicker dptFrom;
        private System.Windows.Forms.DataGridViewLinkColumn colCommandName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colJobNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colWorkshop;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCreateOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colModifiedOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatusStep1Cn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTimeStep1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatusStep2Cn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTimeStep2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatusStep3Cn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTimeStep3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatusStep4Cn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTimeStep4;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatusStep5Cn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTimeStep5;
        private System.Windows.Forms.DataGridViewTextBoxColumn colErrorMessage;
        private System.Windows.Forms.ComboBox comboBoxWorkLine;
        private System.Windows.Forms.Label lblWorkLine;

    }
}