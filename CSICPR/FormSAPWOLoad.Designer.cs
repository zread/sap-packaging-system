﻿namespace CSICPR
{
    partial class FormSAPWOLoad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.panel2 = new System.Windows.Forms.Panel();
        	this.PlWo = new System.Windows.Forms.Panel();
        	this.btnReset = new System.Windows.Forms.Button();
        	this.btnSave = new System.Windows.Forms.Button();
        	this.txtWo = new System.Windows.Forms.TextBox();
        	this.label1 = new System.Windows.Forms.Label();
        	this.splitContainer1 = new System.Windows.Forms.SplitContainer();
        	this.lstView = new System.Windows.Forms.ListView();
        	this.panel2.SuspendLayout();
        	this.PlWo.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
        	this.splitContainer1.Panel1.SuspendLayout();
        	this.splitContainer1.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// panel2
        	// 
        	this.panel2.Controls.Add(this.PlWo);
        	this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
        	this.panel2.Location = new System.Drawing.Point(3, 3);
        	this.panel2.Name = "panel2";
        	this.panel2.Size = new System.Drawing.Size(866, 68);
        	this.panel2.TabIndex = 0;
        	// 
        	// PlWo
        	// 
        	this.PlWo.Controls.Add(this.btnReset);
        	this.PlWo.Controls.Add(this.btnSave);
        	this.PlWo.Controls.Add(this.txtWo);
        	this.PlWo.Controls.Add(this.label1);
        	this.PlWo.Location = new System.Drawing.Point(17, 15);
        	this.PlWo.Name = "PlWo";
        	this.PlWo.Size = new System.Drawing.Size(720, 34);
        	this.PlWo.TabIndex = 49;
        	// 
        	// btnReset
        	// 
        	this.btnReset.Location = new System.Drawing.Point(348, 0);
        	this.btnReset.Name = "btnReset";
        	this.btnReset.Size = new System.Drawing.Size(75, 25);
        	this.btnReset.TabIndex = 55;
        	this.btnReset.Text = "重置";
        	this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
        	// 
        	// btnSave
        	// 
        	this.btnSave.Location = new System.Drawing.Point(267, 0);
        	this.btnSave.Name = "btnSave";
        	this.btnSave.Size = new System.Drawing.Size(75, 25);
        	this.btnSave.TabIndex = 54;
        	this.btnSave.Text = "下载";
        	this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        	// 
        	// txtWo
        	// 
        	this.txtWo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.txtWo.Location = new System.Drawing.Point(115, 3);
        	this.txtWo.MaxLength = 24;
        	this.txtWo.Name = "txtWo";
        	this.txtWo.Size = new System.Drawing.Size(146, 20);
        	this.txtWo.TabIndex = 52;
        	this.txtWo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWo_KeyPress);
        	// 
        	// label1
        	// 
        	this.label1.AutoSize = true;
        	this.label1.Location = new System.Drawing.Point(3, 5);
        	this.label1.Name = "label1";
        	this.label1.Size = new System.Drawing.Size(64, 13);
        	this.label1.TabIndex = 6;
        	this.label1.Text = "工 单 号 码";
        	// 
        	// splitContainer1
        	// 
        	this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
        	this.splitContainer1.IsSplitterFixed = true;
        	this.splitContainer1.Location = new System.Drawing.Point(3, 71);
        	this.splitContainer1.Name = "splitContainer1";
        	this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
        	// 
        	// splitContainer1.Panel1
        	// 
        	this.splitContainer1.Panel1.Controls.Add(this.lstView);
        	this.splitContainer1.Panel2Collapsed = true;
        	this.splitContainer1.Size = new System.Drawing.Size(866, 535);
        	this.splitContainer1.SplitterDistance = 394;
        	this.splitContainer1.SplitterWidth = 2;
        	this.splitContainer1.TabIndex = 1;
        	// 
        	// lstView
        	// 
        	this.lstView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.lstView.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.lstView.FullRowSelect = true;
        	this.lstView.Location = new System.Drawing.Point(0, 0);
        	this.lstView.MultiSelect = false;
        	this.lstView.Name = "lstView";
        	this.lstView.Size = new System.Drawing.Size(866, 535);
        	this.lstView.TabIndex = 2;
        	this.lstView.UseCompatibleStateImageBehavior = false;
        	this.lstView.View = System.Windows.Forms.View.Details;
        	this.lstView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstView_KeyDown);
        	// 
        	// FormSAPWOLoad
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(872, 609);
        	this.Controls.Add(this.splitContainer1);
        	this.Controls.Add(this.panel2);
        	this.Name = "FormSAPWOLoad";
        	this.Padding = new System.Windows.Forms.Padding(3);
        	this.ShowInTaskbar = false;
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "工单下载";
        	this.Load += new System.EventHandler(this.FormSAPLoad_Load);
        	this.panel2.ResumeLayout(false);
        	this.PlWo.ResumeLayout(false);
        	this.PlWo.PerformLayout();
        	this.splitContainer1.Panel1.ResumeLayout(false);
        	((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
        	this.splitContainer1.ResumeLayout(false);
        	this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel PlWo;
        private System.Windows.Forms.TextBox txtWo;
        private System.Windows.Forms.ListView lstView;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnReset;
    }
}