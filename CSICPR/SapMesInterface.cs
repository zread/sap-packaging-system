﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace CSICPR
{
    public class SapMesInterface
    {
        private static string sqlconnet = "";

        private static string _M01SQLConnetion = "";
        /// <summary>
        /// M01连接数据库字符串
        /// </summary>
        private static string M01SQLConnetion
        {
            get { return ToolsClass.getConfig("M01DB", false, "", "config.xml"); }
        }

        private static string _M02SQLConnetion = "";
        /// <summary>
        /// M02连接数据库字符串
        /// </summary>
        private static string M02SQLConnetion
        {
            get { return ToolsClass.getConfig("M02DB", false, "", "config.xml"); }
        }

        private static string _M03SQLConnetion = "";
        /// <summary>
        /// M02连接数据库字符串
        /// </summary>
        private static string M03SQLConnetion
        {
            get { return ToolsClass.getConfig("M03DB", false, "", "config.xml"); }
        }

        private static string _M07SQLConnetion = "";
        /// <summary>
        /// M02连接数据库字符串
        /// </summary>
        private static string M07SQLConnetion
        {
            get { return ToolsClass.getConfig("M07DB", false, "", "config.xml"); }
        }

        /// <summary>
        /// 更新包装数据库
        /// </summary>
        /// <param name="CartonList"></param>
        /// <returns></returns>
        public static bool UpdatePackingData(DataTable Carton, out string message)
        {
            try
            {
                var sqlconnet = "";
                message = "";
                if (Carton != null && Carton.Rows.Count > 0)
                {
                    //箱号必须是同一个车间的
                    DataRow row = Carton.Rows[0];
                    if (Convert.ToString(row["Resv02"]).Equals("1101") || Convert.ToString(row["Resv02"]).Equals("1106")) //M01
                        sqlconnet = M01SQLConnetion;
                    else if (Convert.ToString(row["Resv02"]).Equals("1102") || Convert.ToString(row["Resv02"]).Equals("1105") )//M02
                        sqlconnet = M02SQLConnetion;
                    else if (Convert.ToString(row["Resv02"]).Equals("1103"))//M03
                        sqlconnet = M03SQLConnetion;
                    else if (Convert.ToString(row["Resv02"]).Equals("1107"))//M7
                        sqlconnet = M07SQLConnetion;

                    using (SqlConnection SqlConnection = new SqlConnection(sqlconnet))
                    {
                        SqlConnection.Open();
                        using (SqlTransaction sqltst = SqlConnection.BeginTransaction())
                        {
                            try
                            {
                                #region    
                                foreach (DataRow rows in Carton.Rows)
                                {
                                    string status = "";
                                    string sql = @"UPDATE [Product] SET [SAPStorageDate] = getdate(),[SAPStorageOperator]='auto',[Flag]='{0}' where boxid='{1}' ";
                                    string sql1 = @"UPDATE [Box] SET [SAPStorageDate] = getdate(),[SAPStorageOperator]='auto',[Flag]='{0}' where boxid='{1}' ";
                                    if (Convert.ToString(rows["ProcessedResult"]).Trim().ToUpper().Equals("OK"))
                                        status = "3";
                                    else
                                    {
                                        //status = "1";
                                        continue;
                                    }
                                    sql = string.Format(sql, status, Convert.ToString(rows["CartonNo"]).Trim());
                                    if (SqlHelper.ExecuteNonQuery(SqlConnection, sqltst, CommandType.Text, sql, null) < 1)
                                    {
                                        message = "托号" + Convert.ToString(rows["CartonNo"]) + " 更新包装数据库失败";
                                        sqltst.Rollback();
                                        return false;
                                    }
                                    sql1 = string.Format(sql1, status, Convert.ToString(rows["CartonNo"]).Trim());
                                    if (SqlHelper.ExecuteNonQuery(SqlConnection, sqltst, CommandType.Text, sql1, null) < 1)
                                    {
                                        message = "托号" + Convert.ToString(rows["CartonNo"]) + " 更新包装数据库失败";
                                        sqltst.Rollback();
                                        return false;
                                    }
                                }
                                sqltst.Commit();
                                return true;
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                message = ex.Message;
                                sqltst.Rollback();
                                return false;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
        }
    }
}
