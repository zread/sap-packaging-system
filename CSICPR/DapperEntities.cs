﻿using System;

namespace CSICPR
{
    #region SysMapping
    //
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class SysMapping
    {
        /// <summary>
        /// 
        /// </summary>
        public String Sysid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String FunctionCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String MappingKey01 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String MappingKey02 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String MappingKey03 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String MappingKey04 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String MappingKey05 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String MappingKey06 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String MappingKey07 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String MappingKey08 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String MappingKey09 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String CreatedBy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String CreatedOn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ModifiedBy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ModifiedOn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Int32 Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv01 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv02 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv03 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv04 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv05 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv06 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv07 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv08 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv09 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv10 { get; set; }

        //构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        public SysMapping()
        {
            Sysid = string.Empty;
            FunctionCode = string.Empty;
            MappingKey01 = string.Empty;
            MappingKey02 = string.Empty;
            MappingKey03 = string.Empty;
            MappingKey04 = string.Empty;
            MappingKey05 = string.Empty;
            MappingKey06 = string.Empty;
            MappingKey07 = string.Empty;
            MappingKey08 = string.Empty;
            MappingKey09 = string.Empty;
            CreatedBy = string.Empty;
            CreatedOn = string.Empty;
            ModifiedBy = string.Empty;
            ModifiedOn = string.Empty;
            Resv01 = string.Empty;
            Resv02 = string.Empty;
            Resv03 = string.Empty;
            Resv04 = string.Empty;
            Resv05 = string.Empty;
            Resv06 = string.Empty;
            Resv07 = string.Empty;
            Resv08 = string.Empty;
            Resv09 = string.Empty;
            Resv10 = string.Empty;
        }
    }
    #endregion

    #region TsapReceiptUploadJobNo
    //
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class TsapReceiptUploadJobNo
    {
        /// <summary>
        /// 入库手动上传数据主键
        /// </summary>
        public String Sysid { get; set; }
        /// <summary>
        /// 上传时间
        /// </summary>
        public String CreatedOn { get; set; }
        /// <summary>
        /// 上传操作工
        /// </summary>
        public String CreatedBy { get; set; }
        /// <summary>
        /// 出货柜号
        /// </summary>
        public String JobNo { get; set; }
        /// <summary>
        /// 自定义柜号
        /// </summary>
        public String InnerJobNo { get; set; }
        /// <summary>
        /// 标记同一次上传的数据的Key
        /// </summary>
        public String GroupHistKey { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv01 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv02 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv03 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv04 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv05 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv06 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv07 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv08 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv09 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv10 { get; set; }

        //构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        public TsapReceiptUploadJobNo()
        {
            Sysid = string.Empty;
            CreatedOn = string.Empty;
            CreatedBy = string.Empty;
            JobNo = string.Empty;
            InnerJobNo = string.Empty;
            GroupHistKey = string.Empty;
            Resv01 = string.Empty;
            Resv02 = string.Empty;
            Resv03 = string.Empty;
            Resv04 = string.Empty;
            Resv05 = string.Empty;
            Resv06 = string.Empty;
            Resv07 = string.Empty;
            Resv08 = string.Empty;
            Resv09 = string.Empty;
            Resv10 = string.Empty;
        }
    }
    #endregion

    #region TsapReceiptUploadModule
    //
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class TsapReceiptUploadModule
    {
        /// <summary>
        /// 入库手动上传数据主键
        /// </summary>
        public String Sysid { get; set; }
        /// <summary>
        /// 上传时间
        /// </summary>
        public String CreatedOn { get; set; }
        /// <summary>
        /// 上传操作工
        /// </summary>
        public String CreatedBy { get; set; }
        /// <summary>
        /// 出货柜号
        /// </summary>
        public String JobNo { get; set; }
        /// <summary>
        /// 托号
        /// </summary>
        public String CartonNo { get; set; }
        /// <summary>
        /// 客户自定义托号
        /// </summary>
        public String CustomerCartonNo { get; set; }
        /// <summary>
        /// 自定义柜号
        /// </summary>
        public String InnerJobNo { get; set; }
        /// <summary>
        /// 打托时间
        /// </summary>
        public String FinishedOn { get; set; }
        /// <summary>
        /// 组件颜色
        /// </summary>
        public String ModuleColor { get; set; }
        /// <summary>
        /// 标记同一次上传的数据的Key
        /// </summary>
        public String GroupHistKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ModuleSn { get; set; }
        /// <summary>
        /// 生产订单号
        /// </summary>
        public String OrderNo { get; set; }
        /// <summary>
        /// 销售订单
        /// </summary>
        public String SalesOrderNo { get; set; }
        /// <summary>
        /// 销售订单行项目
        /// </summary>
        public String SalesItemNo { get; set; }
        /// <summary>
        /// 生产订单状态
        /// </summary>
        public String OrderStatus { get; set; }
        /// <summary>
        /// 产品物料代码
        /// </summary>
        public String ProductCode { get; set; }
        /// <summary>
        /// 单位 固定值：PC
        /// </summary>
        public String Unit { get; set; }
        /// <summary>
        /// 工厂
        /// </summary>
        public String Factory { get; set; }
        /// <summary>
        /// 车间
        /// </summary>
        public String Workshop { get; set; }
        /// <summary>
        /// 入库地点
        /// </summary>
        public String PackingLocation { get; set; }
        /// <summary>
        /// 包装方式：A横包装 B竖包装 C双件包 D单件包 E其它
        /// </summary>
        public String PackingMode { get; set; }
        /// <summary>
        /// 电池片转换效率
        /// </summary>
        public String CellEff { get; set; }
        /// <summary>
        /// 实测功率
        /// </summary>
        public String TestPower { get; set; }
        /// <summary>
        /// 标称功率
        /// </summary>
        public String StdPower { get; set; }
        /// <summary>
        /// 组件等级
        /// </summary>
        public String ModuleGrade { get; set; }
        /// <summary>
        /// 电流分档 传入实测电流值，SAP接收之后，如果有值，认为是 Y 表示是否做了电池分档
        /// </summary>
        public String ByIm { get; set; }
        /// <summary>
        /// 公差
        /// </summary>
        public String Tolerance { get; set; }
        /// <summary>
        /// 电池片物料号
        /// </summary>
        public String CellCode { get; set; }
        /// <summary>
        /// 电池片批次
        /// </summary>
        public String CellBatch { get; set; }
        /// <summary>
        /// 电池片网版本
        /// </summary>
        public String CellPrintMode { get; set; }
        /// <summary>
        /// 玻璃物料号
        /// </summary>
        public String GlassCode { get; set; }
        /// <summary>
        /// 玻璃批次
        /// </summary>
        public String GlassBatch { get; set; }
        /// <summary>
        /// EVA物料号
        /// </summary>
        public String EvaCode { get; set; }
        /// <summary>
        /// EVA批次
        /// </summary>
        public String EvaBatch { get; set; }
        /// <summary>
        /// 背板物料号
        /// </summary>
        public String TptCode { get; set; }
        /// <summary>
        /// 背板批次
        /// </summary>
        public String TptBatch { get; set; }
        /// <summary>
        /// 接线盒物料号
        /// </summary>
        public String ConboxCode { get; set; }
        /// <summary>
        /// 接线盒批次
        /// </summary>
        public String ConboxBatch { get; set; }
        /// <summary>
        /// 接线盒类型 目前为空，该节点可以不传
        /// </summary>
        public String ConboxType { get; set; }
        /// <summary>
        /// 长边框物料号
        /// </summary>
        public String LongFrameCode { get; set; }
        /// <summary>
        /// 长边框批次
        /// </summary>
        public String LongFrameBatch { get; set; }
        /// <summary>
        /// 短边框物料号
        /// </summary>
        public String ShortFrameCode { get; set; }
        /// <summary>
        /// 短边框批次
        /// </summary>
        public String ShortFrameBatch { get; set; }
        /// <summary>
        /// 玻璃厚度：3.2mm 4mm Other
        /// </summary>
        public String GlassThickness { get; set; }
        /// <summary>
        /// 是否重工，Y表示重工，N表示不重工
        /// </summary>
        public String IsRework { get; set; }
        /// <summary>
        /// 是否副产品
        /// </summary>
        public String IsByProduction { get; set; }
        /// <summary>
        /// 上传状态：包装系统使用 Ready/Finished/Canceled
        /// </summary>
        public String UploadStatus { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv01 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv02 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv03 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv04 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv05 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv06 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv07 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv08 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv09 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv10 { get; set; }

        public string CommandName { get; set; }

        //构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        public TsapReceiptUploadModule()
        {
            Sysid = string.Empty;
            CreatedOn = string.Empty;
            CreatedBy = string.Empty;
            JobNo = string.Empty;
            CartonNo = string.Empty;
            CustomerCartonNo = string.Empty;
            InnerJobNo = string.Empty;
            FinishedOn = string.Empty;
            ModuleColor = string.Empty;
            GroupHistKey = string.Empty;
            ModuleSn = string.Empty;
            OrderNo = string.Empty;
            SalesOrderNo = string.Empty;
            SalesItemNo = string.Empty;
            OrderStatus = string.Empty;
            ProductCode = string.Empty;
            Unit = string.Empty;
            Factory = string.Empty;
            Workshop = string.Empty;
            PackingLocation = string.Empty;
            PackingMode = string.Empty;
            CellEff = string.Empty;
            TestPower = string.Empty;
            StdPower = string.Empty;
            ModuleGrade = string.Empty;
            ByIm = string.Empty;
            Tolerance = string.Empty;
            CellCode = string.Empty;
            CellBatch = string.Empty;
            CellPrintMode = string.Empty;
            GlassCode = string.Empty;
            GlassBatch = string.Empty;
            EvaCode = string.Empty;
            EvaBatch = string.Empty;
            TptCode = string.Empty;
            TptBatch = string.Empty;
            ConboxCode = string.Empty;
            ConboxBatch = string.Empty;
            ConboxType = string.Empty;
            LongFrameCode = string.Empty;
            LongFrameBatch = string.Empty;
            ShortFrameCode = string.Empty;
            ShortFrameBatch = string.Empty;
            GlassThickness = string.Empty;
            IsRework = string.Empty;
            IsByProduction = string.Empty;
            UploadStatus = "Ready";
            Resv01 = string.Empty;
            Resv02 = string.Empty;
            Resv03 = string.Empty;
            Resv04 = string.Empty;
            Resv05 = string.Empty;
            Resv06 = string.Empty;
            Resv07 = string.Empty;
            Resv08 = string.Empty;
            Resv09 = string.Empty;
            Resv10 = string.Empty;
        }
    }
    #endregion

    #region esb entity

    #region T_BIZ_MODULE
    //T_BIZ_MODULE
    /// <summary>
    /// T_BIZ_MODULE
    /// </summary>
    [Serializable]
    public class BizModule
    {
        /// <summary>
        /// ID
        /// </summary>
        public Int64 ID { get; set; }
        /// <summary>
        /// 组件序列号
        /// </summary>
        public String ModuleSn { get; set; }
        /// <summary>
        /// 柜号
        /// </summary>
        public String JobNo { get; set; }
        /// <summary>
        /// 柜号
        /// </summary>
        public String InnerJobNo { get; set; }
        /// <summary>
        /// 托号
        /// </summary>
        public String CartonNo { get; set; }
        /// <summary>
        /// 托号
        /// </summary>
        public String CustomerCartonNo { get; set; }
        /// <summary>
        /// 产品物料号
        /// </summary>
        public String ProductCode { get; set; }
        /// <summary>
        /// 批次
        /// </summary>
        public String BatchNo { get; set; }
        /// <summary>
        /// 凭证号
        /// </summary>
        public String Voucher { get; set; }
        /// <summary>
        /// 生产订单号
        /// </summary>
        public String OrderNo { get; set; }
        /// <summary>
        /// 销售订单号
        /// </summary>
        public String SalesOrderNo { get; set; }
        /// <summary>
        /// 销售订单行号
        /// </summary>
        public String SalesItemNo { get; set; }
        /// <summary>
        /// 工厂
        /// </summary>
        public String Factory { get; set; }
        /// <summary>
        /// 车间
        /// </summary>
        public String Workshop { get; set; }
        /// <summary>
        /// 第一次入库时的GUID
        /// </summary>
        public String InvGuid { get; set; }
        /// <summary>
        /// 第一次入库时间
        /// </summary>
        public DateTime InvDate { get; set; }
        /// <summary>
        /// 最近入库时的GUID
        /// </summary>
        public String InvRecentGuid { get; set; }
        /// <summary>
        /// 最近入库时间
        /// </summary>
        public DateTime InvRecentDate { get; set; }
        /// <summary>
        /// 是否重工入库
        /// </summary>
        public String IsRework { get; set; }
        /// <summary>
        /// 是否副产品
        /// </summary>
        public String IsByProduction { get; set; }
        /// <summary>
        /// 报废
        /// </summary>
        public String IsScrap { get; set; }
        /// <summary>
        /// 是否过期/结转到下一年度
        /// </summary>
        public String IsExpire { get; set; }
        /// <summary>
        /// 标称功率
        /// </summary>
        public String StdPower { get; set; }
        /// <summary>
        /// 实测功率
        /// </summary>
        public String TestPower { get; set; }
        /// <summary>
        /// 入库地点
        /// </summary>
        public String PackingLocation { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        public String Unit { get; set; }
        /// <summary>
        /// INV, BATCH, SCRAP, REPACKAGE, HU_CHANGE
        /// </summary>
        public String ActionType { get; set; }
        /// <summary>
        /// T_BIZ_PACKAGE_INV.GUID; T_BIZ_MODULE_BATCH.GUID; T_BIZ_SCRAP.GUID; T_BIZ_REPACKAGE.GUID; T_BIZ_HU_CHANGE.GUID
        /// </summary>
        public String ActionId { get; set; }
        /// <summary>
        /// 时间
        /// </summary>
        public DateTime ActionDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ModuleType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Fromsys { get; set; }

        //构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        public BizModule()
        {
            ModuleSn = string.Empty;
            JobNo = string.Empty;
            InnerJobNo = string.Empty;
            CartonNo = string.Empty;
            CustomerCartonNo = string.Empty;
            ProductCode = string.Empty;
            BatchNo = string.Empty;
            Voucher = string.Empty;
            OrderNo = string.Empty;
            SalesOrderNo = string.Empty;
            SalesItemNo = string.Empty;
            Factory = string.Empty;
            Workshop = string.Empty;
            InvGuid = string.Empty;
            InvRecentGuid = string.Empty;
            IsRework = string.Empty;
            IsByProduction = string.Empty;
            IsScrap = string.Empty;
            IsExpire = string.Empty;
            StdPower = string.Empty;
            TestPower = string.Empty;
            PackingLocation = string.Empty;
            Unit = string.Empty;
            ActionType = string.Empty;
            ActionId = string.Empty;
            ModuleType = string.Empty;
            Fromsys = string.Empty;
        }
    }
    #endregion

    #region T_BIZ_MODULE_MATERIAL
    //T_BIZ_MODULE_MATERIAL
    /// <summary>
    /// T_BIZ_MODULE_MATERIAL
    /// </summary>
    [Serializable]
    public class BizModuleMaterial
    {
        /// <summary>
        /// 
        /// </summary>
        public Int64 ID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ModuleSn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Guid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String OrderNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ProductCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String CellEff { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String TestPower { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String StdPower { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ModuleGrade { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Byim { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String CellCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String CellBatch { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String GlassCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String GlassBatch { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String EvaCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String EvaBatch { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String TptCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String TptBatch { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ConboxCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ConboxBatch { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ConboxType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ShortFrameCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ShortFrameBatch { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String LongFrameCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String LongFrameBatch { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Tolerance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String PackingMode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ModuleColor { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String SourceType { get; set; }
        /// <summary>
        /// 时间
        /// </summary>
        public DateTime CreateOn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ModuleType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Fromsys { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String CellPrintMode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String BatchNo { get; set; }
        /// <summary>
        /// 入库时间
        /// </summary>
        public DateTime SapBatchDate { get; set; }

        //构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        public BizModuleMaterial()
        {
            ModuleSn = string.Empty;
            Guid = string.Empty;
            OrderNo = string.Empty;
            ProductCode = string.Empty;
            CellEff = string.Empty;
            TestPower = string.Empty;
            StdPower = string.Empty;
            ModuleGrade = string.Empty;
            Byim = string.Empty;
            CellCode = string.Empty;
            CellBatch = string.Empty;
            GlassCode = string.Empty;
            GlassBatch = string.Empty;
            EvaCode = string.Empty;
            EvaBatch = string.Empty;
            TptCode = string.Empty;
            TptBatch = string.Empty;
            ConboxCode = string.Empty;
            ConboxBatch = string.Empty;
            ConboxType = string.Empty;
            ShortFrameCode = string.Empty;
            ShortFrameBatch = string.Empty;
            LongFrameCode = string.Empty;
            LongFrameBatch = string.Empty;
            Tolerance = string.Empty;
            PackingMode = string.Empty;
            ModuleColor = string.Empty;
            SourceType = string.Empty;
            ModuleType = string.Empty;
            Fromsys = string.Empty;
            CellPrintMode = string.Empty;
            BatchNo = string.Empty;
        }
    }
    #endregion

    #endregion

    #region RegModuleRework
    //
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class RegModuleRework
    {
        /// <summary>
        /// 
        /// </summary>
        public String Sysid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ModuleSn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String WorkOrder { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String CreatedBy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String CreatedOn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv01 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv02 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv03 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv04 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv05 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv06 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv07 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv08 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv09 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv10 { get; set; }

        //构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        public RegModuleRework()
        {
            Sysid = string.Empty;
            ModuleSn = string.Empty;
            WorkOrder = string.Empty;
            CreatedBy = string.Empty;
            CreatedOn = string.Empty;
            Resv01 = string.Empty;
            Resv02 = string.Empty;
            Resv03 = string.Empty;
            Resv04 = string.Empty;
            Resv05 = string.Empty;
            Resv06 = string.Empty;
            Resv07 = string.Empty;
            Resv08 = string.Empty;
            Resv09 = string.Empty;
            Resv10 = string.Empty;
        }
    }
    #endregion
}