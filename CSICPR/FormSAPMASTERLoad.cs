﻿using System;
using System.ComponentModel;
//using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Collections.Generic;


namespace CSICPR
{
    public partial class FormSAPMASTERLoad : Form     
    {
        #region 共有变量
        private static List<LanguageItemModel> LanMessList;//定义语言集
        private static FormSAPMASTERLoad theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormSAPMASTERLoad();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        public  FormSAPMASTERLoad()
        {
            InitializeComponent();
        }

        private void FormSAPMASTERLoad_Load(object sender, EventArgs e)
        {
            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion

            this.lstView.Columns.Add(LanguageHelper.GetMessage(LanMessList, "Message1", "提示信息"), 1000, HorizontalAlignment.Left);
        }
        //物料编码
        private string Mcode = "";
        /// <summary>
        /// 提供数据复制功能
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (lstView.SelectedItems.Count > 0)
                {
                    if (lstView.SelectedItems[0].Text != "")
                        Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.txtMcode.Text.Trim().Equals(""))
            {
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message2", "输入的物料编码不能为空"), "ABNORMAL", lstView);
                this.txtMcode.Focus();
                return;
            }
            else
                Mcode = this.txtMcode.Text.Trim();

            try
            {
                string msg = "";
                SapStorageService.SapMesInterfaceClient service = new SapStorageService.SapMesInterfaceClient();
                service.Endpoint.Address = new System.ServiceModel.EndpointAddress(FormCover.WebServiceAddress);
                msg = service.materialSaptoMes("", Mcode);
                if (msg.Equals("0"))
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message3", "物料编码不存在,请确认！:") + Mcode, "ABNORMAL", lstView);
                    this.txtMcode.SelectAll();
                    return;
                }
                else
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message4", "物料编码下载成功:") + Mcode, "NORMAL", lstView);
                    this.txtMcode.Clear();
                    this.txtMcode.Focus();
                }
            }
            catch (Exception ex)
            {
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message5", "物料编码在下载时发生异常:") + Mcode + " ;" + ex.Message + "", "ABNORMAL", lstView);
                this.txtMcode.SelectAll();
            }

            //case "materialSaptoMes":
            //        //物料主数据接口
            //        //materialSaptoMes ( LastSycnDate As string ,  ItemCode As string ) As string
            //        msg = client.materialSaptoMes("", paramData);
            //        if (msg == "0")
            //            throw new Exception(string.Format("物料{0}不存在", paramData));
            //        break;
            //    case "orderSaptoMes":
            //        //工单主数据接口
            //        //orderSaptoMes ( LastSycnDate As string ,  OrderCode As string ) As string
            //        msg = client.orderSaptoMes("", paramData);
            //        if (msg == "0")
            //            throw new Exception(string.Format("工单{0}不存在", paramData));
            //        break;
            //    case "toStorageDataSaptoMes":
            //        //MES收料主数据接口
            //        //toStorageDataSaptoMes ( LastSycnDate As string ,  ItemCode As string ) As string
            //        msg = client.toStorageDataSaptoMes("", paramData, txtParam1.Text.Trim(), txtParam2.Text.Trim());
            //        if (msg == "0")
            //            throw new Exception("收料主数据不存在");
            //        break;
        }

        private void txtMcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                this.btnSave_Click(null,null);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            this.txtMcode.Clear();
            if (lstView.Items.Count > 100)
                lstView.Items.Clear();
            this.txtMcode.Focus();
        }

    }
}
