﻿using System;
using System.Windows.Forms;

using System.Diagnostics;
using System.IO;
using System.Collections.Generic;

namespace CSICPR
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。ExceptionDataProced
        /// </summary>
        [STAThread]
        static void Main()
        {
            ////自动更新部分
            //if (CheckAutoUpdate())
            //{
            //    MessageBox.Show("检测到有新版本，需要更新！", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    System.Diagnostics.Process.Start("AutoUpdate.exe");

            //    Application.Exit();
            //}

            //CheckAndDownloadMySqlConnector();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormMain());
        }

        /// <summary>
        /// 检查是否有新版本的程序发布出来，是否进行自动更新。
        /// </summary>
        private static bool CheckAutoUpdate()
        {

            //1:从数据库获取程序发布的路径，用户名，密码
            #region 从数据库获取程序发布的路径，用户名，密码
            List<string> lst = new List<string>();
            lst = AutoUpdateHelper.GetAutoUpdateInfo();

            if (lst.Count != 3)
            {
                MessageBox.Show("自动更新配置不正确，但不影响系统使用。请联系系统管理员！", "包装系统", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            string sharedPath = lst[0];
            string UserName = lst[1];
            string Password = lst[2];
            string SharedCSICPRFile = sharedPath + @"\CSICPR.exe";

            if (string.IsNullOrEmpty(sharedPath) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Password))
            {
                MessageBox.Show("自动更新配置不正确，但不影响系统使用。请联系系统管理员！", "包装系统", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            #endregion

            //2：检查远程访问权限，如果没权限访问共享路径，则用net use赋权限
            #region 必要时用net use赋权限
            if (!File.Exists(SharedCSICPRFile))
            {
                try
                {
                    AutoUpdateHelper.Delete(sharedPath);
                    AutoUpdateHelper.Connect(sharedPath, UserName, Password);
                }
                catch (Exception ex)
                {
                    //AutoUpdateHelper.Delete(sharedPath);
                    MessageBox.Show(ex.Message, "包装系统", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            #endregion

            //3：检查CSICPR.exe的版本是否有更新
            #region 检查CSICPR.exe的版本是否有更新
            if (File.Exists(SharedCSICPRFile))
            {
                FileVersionInfo fileVersionShared =
                    FileVersionInfo.GetVersionInfo(SharedCSICPRFile);

                FileVersionInfo fileVersionLocal =
                    FileVersionInfo.GetVersionInfo(Application.StartupPath + @"\CSICPR.exe");

                //判断版本的方法：字符串比较位数不固定的时候，不要使用大于小于，只要版本号与服务器不同，则更新
                if (fileVersionShared.FileVersion.CompareTo(fileVersionLocal.FileVersion.ToString()) != 0)
                {
                    return true;
                }
            }
            else
            {
                MessageBox.Show("没有找到自动更新的路径，但不影响系统使用。请联系系统管理员！", "包装系统", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            #endregion

            return false;

        }

        //确保本地存在MySql的连接dll
        /// <summary>
        /// 确保本地存在MySql的连接dll
        /// </summary>
        private static void CheckAndDownloadMySqlConnector()
        {
            try
            {
                const string fileName = "MySql.Data.dll";
                var localPath = Application.StartupPath + @"\" + fileName;
                //1、本地是否存在MySql.Data.dll
                if (File.Exists(localPath))
                    return;
                //2、如果不存在，从服务器下载
                //2.1、从数据库获取程序发布的路径，用户名，密码
                #region 从数据库获取程序发布的路径，用户名，密码
                var lst = AutoUpdateHelper.GetAutoUpdateInfo();
                if (lst.Count != 3)
                {
                    ToolsClass.Log("CheckAndDownloadMySqlConnector Failed::" + "自动更新配置不正确，但不影响系统使用。请联系系统管理员！");
                    return;
                }
                var sharedPath = lst[0];
                var userName = lst[1];
                var password = lst[2];
                var sharedFile = sharedPath + @"\CSICPR.exe";
                if (string.IsNullOrEmpty(sharedPath) || string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                {
                    ToolsClass.Log("CheckAndDownloadMySqlConnector Failed::" + "自动更新配置不正确，但不影响系统使用。请联系系统管理员！");
                    return;
                }
                #endregion
                //2.2、检查远程访问权限，如果没权限访问共享路径，则用net use赋权限
                #region 必要时用net use赋权限
                if (!File.Exists(sharedFile))
                {
                    try
                    {
                        AutoUpdateHelper.Delete(sharedPath);
                        AutoUpdateHelper.Connect(sharedPath, userName, password);
                    }
                    catch (Exception ex1)
                    {
                        ToolsClass.Log("CheckAndDownloadMySqlConnector Failed::" + ex1.Message);
                        return;
                    }
                }
                #endregion
                var remotePath = sharedPath + @"\" + fileName;
                //2.3、检查服务器是否存在MySql.Data.dll
                if (!File.Exists(remotePath))
                    return;
                //2.4、如果服务器存在文件，下载到本地
                File.Copy(remotePath, localPath, true);
            }
            catch (Exception ex)
            {
                ToolsClass.Log("CheckAndDownloadMySqlConnector Failed::" + ex.Message);
            }
        }
    }
}