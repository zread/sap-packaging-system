﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class frmMaterialTemplate : Form
    {
        private static List<LanguageItemModel> LanMessList;

        #region 私有变量

        private string WoType = ""; // 工单类型
        //private bool ComboxInitialized = false; //物料代码的下拉框是否初始化过

        private string currentWorkshop = "";

        #endregion

        private static frmMaterialTemplate theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new frmMaterialTemplate();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        public frmMaterialTemplate()
        {
            InitializeComponent();
        }

        //page_load事件，加载控件的初始值
        private void frmMaterialTemplate_Load(object sender, EventArgs e)
        {

            #  region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion
            
            //currentWorkshop
            switch (FormCover.CurrentFactory)
            {
                case "M09":
                    currentWorkshop = "M01";
                    break;
                case "M13":
                    currentWorkshop = "M07";
                    break;
                default:
                    currentWorkshop = FormCover.CurrentFactory;
                    break;
            }

            //默认带出当前的车间
            txtWorkShop.Text = currentWorkshop;

            //工单类型
            this.ddlWoType.Items.Insert(0, "");
            this.ddlWoType.Items.Insert(1, "Normal Production order");
            this.ddlWoType.Items.Insert(2, "Rework order");
            this.ddlWoType.SelectedIndex = 1;

            //电池分档
            this.ddlByIm.Items.Insert(0, "NA");
            //this.ddlByIm.Items.Insert(1, "是");
            //this.ddlByIm.Items.Insert(2, "否");
            this.ddlByIm.SelectedIndex = 0;
            this.ddlByIm.Enabled = false;

            //玻璃厚度
            this.ddlGlassLength.Items.Insert(0,"");
            this.ddlGlassLength.Items.Insert(1, "3.2mm");
            this.ddlGlassLength.Items.Insert(2, "4mm");
            this.ddlGlassLength.Items.Insert(3, "Other");
            this.ddlGlassLength.SelectedIndex = 0;

            //包装方式
            this.ddlPackingPattern.Items.Insert(0,"");
          	this.ddlPackingPattern.Items.Insert(1,LanguageHelper.GetMessage(LanMessList, "Message1", "横包装"));
            this.ddlPackingPattern.Items.Insert(2, LanguageHelper.GetMessage(LanMessList, "Message2", "竖包装"));
//            this.ddlPackingPattern.Items.Insert(3, LanguageHelper.GetMessage(LanMessList, "Message3", "双件装"));
//            this.ddlPackingPattern.Items.Insert(4,LanguageHelper.GetMessage(LanMessList, "Message4", "单件装") );
//            this.ddlPackingPattern.Items.Insert(5, LanguageHelper.GetMessage(LanMessList, "Message5", "其它"));
			this.ddlPackingPattern.SelectedIndex = 0;

            IntiDropDownList();


            /*取消等级 by he xing 2013 09 11
            //组件等级
            this.comboxModuleGrade.Items.Insert(0, "");
            this.comboxModuleGrade.Items.Insert(1, "A 等级");
            this.comboxModuleGrade.Items.Insert(2, "B 等级");
            this.comboxModuleGrade.Items.Insert(3, "废品");
            this.comboxModuleGrade.SelectedIndex = 0;
            */
            //gridview1设置选择格式
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.panelDisplay.Enabled = false;
            
            
        }

        //通过弹出窗口查询工单,并根据工单初始化下拉框的值
        #region 工单查询
        private void PicBoxWO_Click(object sender, EventArgs e)
        {
            //if (this.ddlWoType.SelectedIndex == 0)
            //{
            //    this.ddlWoType.Focus();
            //    return;
            //}

            var frm = new FormMaterialTemplateWo(this.txtWoOrder.Text.Trim().ToString(), FormCover.WOTypeCode.Trim());
            frm.ShowDialog();
            this.txtWoOrder.Text = frm.Wo;
            if (frm != null)
                frm.Dispose();
            SetWOData(false);
            InitialTolerance(this.txtWoOrder.Text.Trim(),"Add","");
        }


        private void IntiDropDownList()
        {
            this.comboxTolerance.Items.Clear();

            DataTable dt = ProductStorageDAL.GetValueFromInterface("SAP-MES-TOLERANCE",FormCover.CurrentFactory);
            if (dt != null && dt.Rows.Count > 1)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                this.comboxTolerance.Items.Insert(i, dt.Rows[i].ItemArray[0]);
                this.comboxTolerance.SelectedIndex = 0;
            }

        }

        private void SetWOData(bool flag)
        {   
            string workorder = this.txtWoOrder.Text.Trim();
            if (workorder.Equals(""))
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message6", "输入的工单为空，请确认！"));//, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.txtWoOrder.SelectAll();
                this.txtWoOrder.Focus();
                return;
            }

            //自动带出物料代码
            #region 自动带出物料代码
            if (workorder == "")
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message7", "请先选择工单!"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtWoOrder.Focus();
                return;
            }
            else
            {
                InitialMaterialCode(workorder);
                
            }
            #endregion

            #region 小组件工单如果没有发料，给一个默认值
            if (FormCover.WOTypeCode.Trim().ToUpper().Equals("ZP09"))
            {
                //电池片
                if (this.comboxCellCode.Items.Count < 1)
                {
                    DataTable dtCell = ProductStorageDAL.GetStorageMitemCodeInfo("CELL");
                    if (dtCell != null)
                    {
                        DataRow rowCell = dtCell.Rows[0];
                        this.comboxCellCode.Items.Add(Convert.ToString(rowCell["MAPPING_KEY_02"]));
                        this.comboxCellCode.SelectedIndex = 0;

    
                        if (this.comboxCellSAPBatch.Items.Count <= 1)
                        {
                            this.comboxCellSAPBatch.DataSource = null;
                            this.comboxCellSAPBatch.Items.Add(Convert.ToString(rowCell["MAPPING_KEY_03"]));
                            this.comboxCellSAPBatch.SelectedIndex = 0;
                        }
                    }
                }
                //玻璃
                if (this.comboxGlassCode.Items.Count < 1)
                {
                    DataTable dtGLASS = ProductStorageDAL.GetStorageMitemCodeInfo("GLASS");
                    if (dtGLASS != null)
                    {
                        DataRow rowGLASS = dtGLASS.Rows[0];
                        this.comboxGlassCode.Items.Add(Convert.ToString(rowGLASS["MAPPING_KEY_02"]));
                        this.comboxGlassCode.SelectedIndex = 0;

                        if (this.comboxGlassSAPBatch.Items.Count <= 1)
                        {
                            this.comboxGlassSAPBatch.DataSource = null;
                            this.comboxGlassSAPBatch.Items.Add(Convert.ToString(rowGLASS["MAPPING_KEY_03"]));
                            this.comboxGlassSAPBatch.SelectedIndex = 0;
                        }
                    }
                }
                // EVA
                if (this.comboxEVACode.Items.Count < 1)
                {
                    DataTable dtEVA = ProductStorageDAL.GetStorageMitemCodeInfo("EVA");
                    if (dtEVA != null)
                    {
                        DataRow rowEVA = dtEVA.Rows[0];
                        this.comboxEVACode.Items.Add(Convert.ToString(rowEVA["MAPPING_KEY_02"]));
                        this.comboxEVACode.SelectedIndex = 0;

                        if (this.comboxEVASAPBatch.Items.Count <= 1)
                        {
                            this.comboxEVASAPBatch.DataSource = null;
                            this.comboxEVASAPBatch.Items.Add(Convert.ToString(rowEVA["MAPPING_KEY_03"]));
                            this.comboxEVASAPBatch.SelectedIndex = 0;
                        }
                    }
                }
                // TPT
                if (this.comboxTPTCode.Items.Count < 1)
                {
                    DataTable dtTPT = ProductStorageDAL.GetStorageMitemCodeInfo("TPT");
                    if (dtTPT != null)
                    {
                        DataRow rowTPT = dtTPT.Rows[0];
                        this.comboxTPTCode.Items.Add(Convert.ToString(rowTPT["MAPPING_KEY_02"]));
                        this.comboxTPTCode.SelectedIndex = 0;

                        if (this.comboxTPTSAPBatch.Items.Count <= 1)
                        {
                            this.comboxTPTSAPBatch.DataSource = null;
                            this.comboxTPTSAPBatch.Items.Add(Convert.ToString(rowTPT["MAPPING_KEY_03"]));
                            this.comboxTPTSAPBatch.SelectedIndex = 0;
                        }
                    }
                }
                //长边框
                if (this.comboxLFrameCode.Items.Count < 1)
                {
                    DataTable dtAIFRAMELONG = ProductStorageDAL.GetStorageMitemCodeInfo("AIFRAME-LONG");
                    if (dtAIFRAMELONG != null)
                    {
                        DataRow rowAIFRAMELONG = dtAIFRAMELONG.Rows[0];
                        this.comboxLFrameCode.Items.Add(Convert.ToString(rowAIFRAMELONG["MAPPING_KEY_02"]));
                        this.comboxLFrameCode.SelectedIndex = 0;

                        if (this.comboxLframeSAPBatch.Items.Count <= 1)
                        {
                            this.comboxLframeSAPBatch.DataSource = null;
                            this.comboxLframeSAPBatch.Items.Add(Convert.ToString(rowAIFRAMELONG["MAPPING_KEY_03"]));
                            this.comboxLframeSAPBatch.SelectedIndex = 0;
                        }
                    }
                }
                //短边框
                if (this.comboxSFrameCode.Items.Count < 1)
                {
                    DataTable dtAIFRAMESHORT = ProductStorageDAL.GetStorageMitemCodeInfo("AIFRAME-SHORT");
                    if (dtAIFRAMESHORT != null)
                    {
                        DataRow rowAIFRAMESHORT = dtAIFRAMESHORT.Rows[0];
                        this.comboxSFrameCode.Items.Add(Convert.ToString(rowAIFRAMESHORT["MAPPING_KEY_02"]));
                        this.comboxSFrameCode.SelectedIndex = 0;

                        if (this.comboxSFrameSAPBatch.Items.Count <= 1)
                        {
                            this.comboxSFrameSAPBatch.DataSource = null;
                            this.comboxSFrameSAPBatch.Items.Add(Convert.ToString(rowAIFRAMESHORT["MAPPING_KEY_03"]));
                            this.comboxSFrameSAPBatch.SelectedIndex = 0;
                        }
                    }
                }
                //接线盒
                if (this.comboxConboxCode.Items.Count < 1)
                {
                    DataTable dtCONBOX = ProductStorageDAL.GetStorageMitemCodeInfo("CONBOX");
                    if (dtCONBOX != null)
                    {
                        DataRow rowCONBOX = dtCONBOX.Rows[0];
                        this.comboxConboxCode.Items.Add(Convert.ToString(rowCONBOX["MAPPING_KEY_02"]));
                        this.comboxConboxCode.SelectedIndex = 0;

                        if (this.comboxConboxSAPBatch.Items.Count <= 1)
                        {
                            this.comboxConboxSAPBatch.DataSource = null;
                            this.comboxConboxSAPBatch.Items.Add(Convert.ToString(rowCONBOX["MAPPING_KEY_03"]));
                            this.comboxConboxSAPBatch.SelectedIndex = 0;
                        }
                    }
                }
            }
            #endregion

            //DataTable wo = ProductStorageDAL.GetWoInfo(workorder, WoType);


            //if (wo != null && wo.Rows.Count > 0)
            //{
            //    //SetWoInfo(wo, WoType);
            //    InitialMaterialCode(workorder);
            //}
            //else
            //{
            //    if (flag)
            //    {
            //        MessageBox.Show("工单：" + workorder + " 没有从SAP下载或工单类型不一致,请确认!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        this.txtWoOrder.SelectAll();
            //        this.txtWoOrder.Focus();
            //        return;
            //    }
            //    else
            //    {
            //        MessageBox.Show("工单:" + workorder + "在SAP没有做物料转储,请确认!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        this.txtWoOrder.SelectAll();
            //        this.txtWoOrder.Focus();
            //        return;
            //    }
            //}

        }
        #endregion

        //Leave电池片转换效率文本框时判断输入格式是否正确
        private void ddlCellTransfer_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.ddlCellTransfer.Text.Trim()))
            {
                int aa = this.ddlCellTransfer.Text.Trim().ToString().Length;
                if (this.ddlCellTransfer.Text.Trim().ToString().Length != 6)
                {
                    MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message8", "电池片转换效率格式不对! 正确格式例如15.171"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.ddlCellTransfer.Focus();
                    return;
                }
                else
                {
                    string CellTransfer = this.ddlCellTransfer.Text.Trim().ToString();
                    string flag = "Y";
                    foreach (char c in CellTransfer)
                    {
                        if ((!char.IsNumber(c)))
                        {
                            char a = '.';
                            if (c.Equals(a))
                                continue;
                            else
                            {
                                flag = "N";
                                break;
                            }
                        }
                    }
                    if (flag.Equals("N"))
                    {
                        MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message8", "电池片转换效率格式不对! 正确格式例如15.171"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.ddlCellTransfer.Focus();
                        return;
                    }
                    else if (Convert.ToDouble(CellTransfer) > 40 || Convert.ToDouble(CellTransfer) <0)
                    {
                        MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message9", "电池片转换效率应该在0~40之间! 正确格式例如15.171"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.ddlCellTransfer.Focus();
                        return;
                    }
                }
            }
            else
            {
                if (!WoType.Equals("ZP11"))
                {
                    MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message10", "电池片转换效率不能为空"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.ddlCellTransfer.Focus();
                    return;
                }
            }
        }

        //查询物料模板，并显示在gridview中
        #region 查询物料模板，并显示在gridview中
        private void btnSelect_Click(object sender, EventArgs e)
        {
            //panelMaterialCode.Enabled = false; //不显示btnFindMaterialCode后的更改
            this.panelDisplay.Enabled = false;
            btnAdd.Enabled = true;
            btnUpdate.Enabled = true;
            this.btnCancel.Enabled = false;
            btnSave.Enabled = false;

            ClearDataGridViewData();
            ClearDisplayContent();
            SetDataGridViewData(this.txtTemplateNameSelect.Text.Trim().ToString());
        }

        /// 清空datagridview
        private void ClearDataGridViewData()
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();
        }

        private void SetDataGridViewData(string templateName)
        {
            DataTable dt = ProductStorageDAL.GetMaterialTemplateInfo(templateName);

            if (dt != null && dt.Rows.Count > 0)
            {
                int RowNo = 0;
                int RowIndex = 1;
                foreach (DataRow row in dt.Rows)
                {
                    this.dataGridView1.Rows.Add();
                    this.dataGridView1.Rows[RowNo].Cells["name"].Value = Convert.ToString(row[0]);
                    this.dataGridView1.Rows[RowNo].Cells["desc"].Value = Convert.ToString(row[1]);
                    this.dataGridView1.Rows[RowNo].Cells["sysID"].Value = Convert.ToString(row[2]);
                    RowNo++;
                    RowIndex++;
                }
            }
            else
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message11", "没有查询到数据"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        #endregion

        private void ClearDisplayContent()
        {

            this.txtTemplateName.Text = "";
            this.txtTemplateDesc.Text = "";
            this.txtWoOrder.Text = "";
            //this.txtWorkShop.Text = "";
            this.ddlByIm.Text = "";
            this.ddlCellTransfer.Text = "";
            this.ddlPackingPattern.Text = "";
            this.ddlGlassLength.Text = "";
            //this.comboxModuleGrade.Text = ""; by hexing 2013 09 11

            ClearCodeComboxs();
        }


        //双击gridview中的一行，自动带出此物料模板的信息。
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            //define variables
            #region  define variables
            string sysID = dataGridView1.SelectedRows[0].Cells[2].Value.ToString().Trim();
            string TemplateName = dataGridView1.SelectedRows[0].Cells[0].Value.ToString().Trim();
            string TemplateDesc = dataGridView1.SelectedRows[0].Cells[1].Value.ToString().Trim();

            string WorkOrder = string.Empty;
            string WorkShop = string.Empty;
            string ByIm = string.Empty;
            string CellTransfer = string.Empty;
            string PackingPattern = string.Empty;
            string GlassLength = string.Empty;
            string CellCode = string.Empty;
            string CellBatch = string.Empty;
            string CellPrintMode = string.Empty;
            string GlassCode = string.Empty;
            string GlassBatch = string.Empty;
            string EvaCode = string.Empty;
            string EvaBatch = string.Empty;
            string TptCode = string.Empty;
            string TptBatch = string.Empty;
            string ConBoxCode = string.Empty;
            string ConBoxBatch = string.Empty;
            string LongFrameCode = string.Empty;
            string LongFrameBatch = string.Empty;
            string ShortFrameCode = string.Empty;
            string ShortFrameBatch = string.Empty;
           // string ModuleGrade = string.Empty; by hexing 2013 09 11 
            string LastUpdateUser = string.Empty;
            string LastUpdateTime = string.Empty;
            string Tolerance = string.Empty;

            #endregion

            //get material template info by sysid
            #region get material template info by sysid

            DataTable dt = ProductStorageDAL.GetMaterialTemplateInfoBySySID(sysID);
            if (dt != null && dt.Rows.Count > 0)     
            {
                WorkOrder = dt.Rows[0][3].ToString();
                WorkShop = dt.Rows[0][4].ToString();
                ByIm = dt.Rows[0][5].ToString();
                CellTransfer = dt.Rows[0][6].ToString();
                PackingPattern = dt.Rows[0][7].ToString();
                GlassLength = dt.Rows[0][8].ToString();
                CellCode  = dt.Rows[0][9].ToString();
                CellBatch  = dt.Rows[0][10].ToString();
                CellPrintMode = dt.Rows[0][11].ToString();
                GlassCode = dt.Rows[0][12].ToString();
                GlassBatch = dt.Rows[0][13].ToString();
                EvaCode  = dt.Rows[0][14].ToString();
                EvaBatch = dt.Rows[0][15].ToString();
                TptCode = dt.Rows[0][16].ToString();
                TptBatch = dt.Rows[0][17].ToString();
                ConBoxCode = dt.Rows[0][18].ToString();
                ConBoxBatch = dt.Rows[0][19].ToString();
                LongFrameCode= dt.Rows[0][20].ToString();
                LongFrameBatch = dt.Rows[0][21].ToString();
                ShortFrameCode = dt.Rows[0][22].ToString();
                ShortFrameBatch = dt.Rows[0][23].ToString();
               // ModuleGrade = dt.Rows[0][24].ToString(); by hexing 2013 09 11 
                LastUpdateUser = dt.Rows[0][25].ToString();
                LastUpdateTime = dt.Rows[0][26].ToString();
                Tolerance = dt.Rows[0][27].ToString();
            }
            #endregion

            //initial material code, description by workorder.  
            //InitialMaterialCode(WorkOrder);

            //ClearDisplayContent
            ClearDisplayContent();

            //panelMaterialCode.Enabled = false; //不显示btnFindMaterialCode后的更改
            this.panelDisplay.Enabled = false;
            btnAdd.Enabled = true;
            btnUpdate.Enabled = true;
            this.btnCancel.Enabled = false;
            btnSave.Enabled = false;

            //display details on the page
            #region  display details on the page

            this.txtTemplateName.Text = TemplateName;
            this.txtTemplateDesc.Text = TemplateDesc;
            this.txtWoOrder.Text = WorkOrder;
            //this.txtWorkShop.Text = WorkShop;
            this.ddlByIm.Text = ByIm;
            this.ddlCellTransfer.Text = CellTransfer;
            this.ddlPackingPattern.Text = PackingPattern;
            this.ddlGlassLength.Text = GlassLength;
           // this.comboxModuleGrade.Text = ModuleGrade; by hexing 2013 09 11
            
            
            //给下拉框赋值
            this.comboxCellSAPBatch.Items.Insert(0, CellBatch);
            this.comboxCellSAPBatch.SelectedIndex = 0;
            this.comboxCellCodeDesc.Items.Insert(0, "");
            this.comboxCellCodeDesc.SelectedIndex = 0;
            this.comboxCellCode.Items.Insert(0, CellCode);
            this.comboxCellCode.SelectedIndex = 0;

            this.comboxGlassSAPBatch.Items.Insert(0, GlassBatch);
            this.comboxGlassSAPBatch.SelectedIndex = 0;
            this.comboxGlassCodeDesc.Items.Insert(0, "");
            this.comboxGlassCodeDesc.SelectedIndex = 0;
            this.comboxGlassCode.Items.Insert(0, GlassCode);
            this.comboxGlassCode.SelectedIndex = 0;


            this.comboxEVASAPBatch.Items.Insert(0, EvaBatch);
            this.comboxEVASAPBatch.SelectedIndex = 0;
            this.comboxEVACodeDesc.Items.Insert(0, "");
            this.comboxEVACodeDesc.SelectedIndex = 0;
            this.comboxEVACode.Items.Insert(0, EvaCode);
            this.comboxEVACode.SelectedIndex = 0;

            this.comboxTPTSAPBatch.Items.Insert(0, TptBatch);
            this.comboxTPTSAPBatch.SelectedIndex = 0;
            this.comboxTPTCodeDesc.Items.Insert(0, "");
            this.comboxTPTCodeDesc.SelectedIndex = 0;
            this.comboxTPTCode.Items.Insert(0, TptCode);
            this.comboxTPTCode.SelectedIndex = 0;


            this.comboxConboxSAPBatch.Items.Insert(0, ConBoxBatch);
            this.comboxConboxSAPBatch.SelectedIndex = 0;
            this.comboxConboxCodeDesc.Items.Insert(0, "");
            this.comboxConboxCodeDesc.SelectedIndex = 0;
            this.comboxConboxCode.Items.Insert(0, ConBoxCode);
            this.comboxConboxCode.SelectedIndex = 0;

            this.comboxLframeSAPBatch.Items.Insert(0, LongFrameBatch);
            this.comboxLframeSAPBatch.SelectedIndex = 0;
            this.comboxLFrameCodeDesc.Items.Insert(0, "");
            this.comboxLFrameCodeDesc.SelectedIndex = 0;
            this.comboxLFrameCode.Items.Insert(0, LongFrameCode);
            this.comboxLFrameCode.SelectedIndex = 0;

            this.comboxSFrameSAPBatch.Items.Insert(0, ShortFrameBatch);
            this.comboxSFrameSAPBatch.SelectedIndex = 0;
            this.comboxSFrameCodeDesc.Items.Insert(0, "");
            this.comboxSFrameCodeDesc.SelectedIndex = 0;
            this.comboxSFrameCode.Items.Insert(0, ShortFrameCode);
            this.comboxSFrameCode.SelectedIndex = 0;

            this.txtLastUpdateUser.Text = LastUpdateUser;
            this.txtLastUpdateTime.Text = LastUpdateTime;

            this.comboxTolerance.Items.Insert(0, Tolerance);
            this.comboxTolerance.SelectedIndex = 0;

            #endregion

            
            
        }


        //清空Combox的内容
        private void ClearCodeComboxs()
        {
            //CellCode and CellCodeDesc
            ClearCombox(comboxCellCode);
            ClearCombox(comboxCellCodeDesc);
            ClearCombox(comboxGlassCode);
            ClearCombox(comboxGlassCodeDesc);
            ClearCombox(comboxEVACode);
            ClearCombox(comboxEVACodeDesc);
            ClearCombox(comboxTPTCode);
            ClearCombox(comboxTPTCodeDesc);
            ClearCombox(comboxConboxCode);
            ClearCombox(comboxConboxCodeDesc);
            ClearCombox(comboxLFrameCode);
            ClearCombox(comboxLFrameCodeDesc);
            ClearCombox(comboxSFrameCode);
            ClearCombox(comboxSFrameCodeDesc);

            //SAPBatch
            ClearCombox(comboxCellSAPBatch);
            ClearCombox(comboxGlassSAPBatch);
            ClearCombox(comboxEVASAPBatch);
            ClearCombox(comboxTPTSAPBatch);
            ClearCombox(comboxConboxSAPBatch);
            ClearCombox(comboxLframeSAPBatch);
            ClearCombox(comboxSFrameSAPBatch);

            ClearCombox(comboxTolerance);
            
            
            //jacky
            ClearCombox(Cb_Market);
            ComboxBackToList(Cb_Market);
            ClearCombox(Cb_Lid);
            ComboxBackToList(Cb_Lid);
           	ClearCombox(Cb_CabelLength);
           	ComboxBackToList(Cb_CabelLength);
            ClearCombox(Cb_JboxType);
            ComboxBackToList(Cb_JboxType);
            
        }

        private void ClearCombox(ComboBox cb)
        { 
            cb.DataSource = null;
            cb.Items.Clear();
            cb.Text = "";
        }

        //利用设置combox的datasource为已知的datatable，动态初始化combox的列表
        private void SetComboxWithDatatable(ComboBox cb, DataTable dt, string valueMember, string displayMemeber)
        {
            
            cb.DataSource = dt;
            //Edit to take NA 20170530
            if (dt == null)
            {
                DataTable dttemp = new DataTable();
                dttemp.Columns.Add("MaterialCode");
                dttemp.Columns.Add("MaterialDescription");

                DataRow _onlyRow = dttemp.NewRow();

                switch (cb.Name)
                {
                    case "comboxGlassCode" :
                        _onlyRow[0] = "11008008";
                        _onlyRow[1] = "NA";
                        break;
                    case "comboxSFrameCode":
                        _onlyRow[0] = "11008009";
                        _onlyRow[1] = "NA";
                        break;
                    case "comboxLFrameCode":
                        _onlyRow[0] = "11008010";
                        _onlyRow[1] = "NA";
                        break;                    
                    case "comboxGlassCodeDesc" :
                        _onlyRow[0] = "Dummy Glass";
                        _onlyRow[1] = "NA";
                        break;                       
                    case "comboxSFrameCodeDesc":
                        _onlyRow[0] = "Dummy frame -1";
                        _onlyRow[1] = "NA";
                        break; 
                    case "comboxLFrameCodeDesc":
                        _onlyRow[0] = "Dummy frame -2";
                        _onlyRow[1] = "NA";
                        break;

                }


               
                dttemp.Rows.Add(_onlyRow);
                cb.DataSource = dttemp;
             
            }

            cb.DisplayMember = displayMemeber;            
            cb.ValueMember = valueMember;
            //cb.SelectedIndex = -1;

            //不知道为什么，有时候上面的语句在执行时，会很慢，再观察，如果不行需要改成下面的语句
            //if (dt != null && dt.Rows.Count>0) 
            //{
            //    foreach (DataRow row in dt.Rows)
            //    {
            //        if (valueMember.Equals("MaterialCode"))
            //        {
            //            if (!cb.Items.Contains(Convert.ToString(row["MaterialCode"]).Trim()))
            //                cb.Items.Add(Convert.ToString(row["MaterialCode"]).Trim());
            //        }
            //        else
            //        {
            //            if (!cb.Items.Contains(Convert.ToString(row["MaterialDescription"]).Trim()))
            //                cb.Items.Add(Convert.ToString(row["MaterialDescription"]).Trim());
            //        }

            //        cb.SelectedIndex = 0;
            //    }
            //}           
        }

        //根据工单设置下拉框中Tolerance

        private void InitialTolerance(string wrkorder, string type, string strTolerance)
        {
      
           DataSet ds = ProductStorageDAL.GetTolerancebyWorkOrder(wrkorder);
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0].ItemArray[0].ToString()))
                    {
                        SetComboxWithDatatable(comboxTolerance, ds.Tables[0], "Tolerance", "Tolerance");
                        comboxTolerance.Enabled = false;
                    }
                    else
                    {
                        IntiDropDownList();
                        if (type == "Update")
                        {
                            this.comboxTolerance.Text = strTolerance;
                        }

                    }
                }
                                else
            {
                IntiDropDownList();
            }
    
            }
        }

       

        //根据工单设置下拉框中的物料编码，物料描述
        private void InitialMaterialCode(string wrkorder)
        {
            //清空下拉框
            ClearCodeComboxs();
            ClearAllIndiCators();
            ddlCellTransfer.Text = "18.000";

            #region 根据dataset的值给下拉框赋值
            DataSet ds = ProductStorageDAL.GetMaterialCodebyWorkOrder(wrkorder);  
		                    
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                	if(ds.Tables[0].Rows.Count > 1)	
                		Color_Change(label4);             	
                	                	
                	SetComboxWithDatatable(comboxCellCode, ds.Tables[0], "MaterialCode", "MaterialCode");
                    SetComboxWithDatatable(comboxCellCodeDesc, ds.Tables[0], "MaterialDescription", "MaterialDescription");
                    InitialSAPBatch(txtWorkShop.Text.ToString(), wrkorder, "Cell",comboxCellCode.Text.ToString().Trim(), comboxCellSAPBatch);
                }
                else
                {
                    SetComboxWithDatatable(comboxCellCode, null, "MaterialCode", "MaterialCode");
                    SetComboxWithDatatable(comboxCellCodeDesc, null, "MaterialDescription", "MaterialDescription");
                    InitialSAPBatch(txtWorkShop.Text.ToString(), wrkorder, "Cell", comboxCellCode.Text.ToString().Trim(), comboxCellSAPBatch);
                }

                if (ds.Tables[1].Rows.Count > 0)
                {
                	if(ds.Tables[1].Rows.Count > 1)
                		Color_Change(label13);
                	
                	
                    SetComboxWithDatatable(comboxGlassCode, ds.Tables[1], "MaterialCode", "MaterialCode");
                    SetComboxWithDatatable(comboxGlassCodeDesc, ds.Tables[1], "MaterialDescription", "MaterialDescription");  
                    GetGlassThinkness();
                    InitialSAPBatch(txtWorkShop.Text.ToString(), wrkorder, "Glass",comboxGlassCode.Text.ToString().Trim(), comboxGlassSAPBatch);
                }
                else
                {
                    SetComboxWithDatatable(comboxGlassCode, null, "MaterialCode", "MaterialCode");
                    SetComboxWithDatatable(comboxGlassCodeDesc, null, "MaterialDescription", "MaterialDescription");
                    GetGlassThinkness();
                    InitialSAPBatch(txtWorkShop.Text.ToString(), wrkorder, "Glass", comboxGlassCode.Text.ToString().Trim(), comboxGlassSAPBatch);
                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                	if(ds.Tables[2].Rows.Count > 1)
                		Color_Change(label16);
                	
                	
                    SetComboxWithDatatable(comboxEVACode, ds.Tables[2], "MaterialCode", "MaterialCode");
                    SetComboxWithDatatable(comboxEVACodeDesc, ds.Tables[2], "MaterialDescription", "MaterialDescription");
                    InitialSAPBatch(txtWorkShop.Text.ToString(), wrkorder, "EVA",comboxEVACode.Text.ToString().Trim(), comboxEVASAPBatch);
                }
                else
                {
                    SetComboxWithDatatable(comboxEVACode, null, "MaterialCode", "MaterialCode");
                    SetComboxWithDatatable(comboxEVACodeDesc,null, "MaterialDescription", "MaterialDescription");
                    InitialSAPBatch(txtWorkShop.Text.ToString(), wrkorder, "EVA", comboxEVACode.Text.ToString().Trim(), comboxEVASAPBatch);
                }

                if (ds.Tables[3].Rows.Count > 0)
                {
                	if(ds.Tables[3].Rows.Count > 1)
                		Color_Change(label19);
                	
                	
                    SetComboxWithDatatable(comboxTPTCode, ds.Tables[3], "MaterialCode", "MaterialCode");
                    SetComboxWithDatatable(comboxTPTCodeDesc, ds.Tables[3], "MaterialDescription", "MaterialDescription");
                    InitialSAPBatch(txtWorkShop.Text.ToString(), wrkorder, "TPT",comboxTPTCode.Text.ToString().Trim(), comboxTPTSAPBatch);
                }
                else
                {
                    SetComboxWithDatatable(comboxTPTCode, null, "MaterialCode", "MaterialCode");
                    SetComboxWithDatatable(comboxTPTCodeDesc, null, "MaterialDescription", "MaterialDescription");
                    InitialSAPBatch(txtWorkShop.Text.ToString(), wrkorder, "TPT", comboxTPTCode.Text.ToString().Trim(), comboxTPTSAPBatch);
                }


                if (ds.Tables[4].Rows.Count > 0)
                {
                	if(ds.Tables[4].Rows.Count > 1)
                		Color_Change(label22);
                	
                	
                    SetComboxWithDatatable(comboxConboxCode, ds.Tables[4], "MaterialCode", "MaterialCode");
                    SetComboxWithDatatable(comboxConboxCodeDesc, ds.Tables[4], "MaterialDescription", "MaterialDescription");
                    InitialSAPBatch(txtWorkShop.Text.ToString(), wrkorder, "Conbox", comboxConboxCode.Text.ToString().Trim(), comboxConboxSAPBatch);
                }
                else
                {
                    SetComboxWithDatatable(comboxConboxCode, null, "MaterialCode", "MaterialCode");
                    SetComboxWithDatatable(comboxConboxCodeDesc, null, "MaterialDescription", "MaterialDescription");
                    InitialSAPBatch(txtWorkShop.Text.ToString(), wrkorder, "Conbox", comboxConboxCode.Text.ToString().Trim(), comboxConboxSAPBatch);
                }

                if (ds.Tables[5].Rows.Count > 0)
                {
                	if(ds.Tables[5].Rows.Count > 1)
                		Color_Change(label26);
                	
                	
                    SetComboxWithDatatable(comboxLFrameCode, ds.Tables[5], "MaterialCode", "MaterialCode");
                    SetComboxWithDatatable(comboxLFrameCodeDesc, ds.Tables[5], "MaterialDescription", "MaterialDescription");
                    InitialSAPBatch(txtWorkShop.Text.ToString(), wrkorder, "AIFrame",comboxLFrameCode.Text.ToString().Trim(), comboxLframeSAPBatch);
                }
                else
                {
                    SetComboxWithDatatable(comboxLFrameCode,null, "MaterialCode", "MaterialCode");
                    SetComboxWithDatatable(comboxLFrameCodeDesc, null, "MaterialDescription", "MaterialDescription");
                    InitialSAPBatch(txtWorkShop.Text.ToString(), wrkorder, "AIFrame", comboxLFrameCode.Text.ToString().Trim(), comboxLframeSAPBatch);

                }


                if (ds.Tables[6].Rows.Count > 0)
                {
                	if(ds.Tables[6].Rows.Count > 1)
                		Color_Change(label29);
                	
                	
                    SetComboxWithDatatable(comboxSFrameCode, ds.Tables[6], "MaterialCode", "MaterialCode");
                    SetComboxWithDatatable(comboxSFrameCodeDesc, ds.Tables[6], "MaterialDescription", "MaterialDescription");
                    InitialSAPBatch(txtWorkShop.Text.ToString(), wrkorder, "AIFrame",comboxSFrameCode.Text.ToString().Trim(), comboxSFrameSAPBatch);
                }
                else
                {
                    SetComboxWithDatatable(comboxSFrameCode, null, "MaterialCode", "MaterialCode");
                    SetComboxWithDatatable(comboxSFrameCodeDesc, null, "MaterialDescription", "MaterialDescription");
                    InitialSAPBatch(txtWorkShop.Text.ToString(), wrkorder, "AIFrame", comboxSFrameCode.Text.ToString().Trim(), comboxSFrameSAPBatch);

                }

                if (ds.Tables[8].Rows.Count > 0)
                {
                	SetComboxValue(Cb_Market,ds.Tables[8].Rows[0][0].ToString());
                	SetComboxValue(Cb_Lid,ds.Tables[8].Rows[0][1].ToString());
                	SetComboxValue(Cb_CabelLength,ds.Tables[8].Rows[0][2].ToString());
                	SetComboxValue(Cb_JboxType,ds.Tables[8].Rows[0][3].ToString());			             	
                	//jacky20160217
                }
              

            }
            #endregion
           
            ////通过click物料代码combox，初始化SAPBatch combox
            //comboxCellCode_Click(null, null);
            //comboxGlassCode_Click(null, null);
            //comboxEVACode_Click(null, null);
            //comboxTPTCode_Click(null, null);
            //comboxConboxCode_Click(null, null);
            //comboxLFrameCode_Click(null, null);
            //comboxSFrameCode_Click(null, null);

        }

        //根据MaterialCode，Workshop，Workorder，MaterialCategory，筛选出SAPBatch，并初始化相应的下拉框
        private void InitialSAPBatch(string WorkShop, string OrderNo, string MaterialCategory, string MaterialCode, ComboBox cb)
        {
            //查询时不初始化SAP批次，只有新增修改时初始化
            if (this.panelDisplay.Enabled == false)
            {
                return;
            }

            cb.DataSource = null;
            cb.Items.Clear();

            try
            {
                DataTable dt = ProductStorageDAL.getSAPBatchbyMaterialCode(WorkShop,OrderNo,MaterialCategory,MaterialCode);
                if (dt != null && dt.Rows.Count > 0)
                {
                    cb.DataSource = dt;
                    cb.DisplayMember = "BATCH";
                    cb.ValueMember = "BATCH";
                    //cb.ValueMember = "BATCH";
                    if(dt.Rows.Count > 1)
                    {
                    	switch (MaterialCategory) {
                    			case "Cell":
                    				label100.Visible = true;
                    				break;                    
                    			case "Glass":
                    				label101.Visible = true;
                    				break; 
                    			case "EVA":
                    				label102.Visible = true;
                    				break; 
                    			case "TPT":
									label103.Visible = true;
                    				break; 
                    			case "Conbox":
									label104.Visible = true;		
									break; 		
								case "AIFrame":
									label105.Visible = true;
									label106.Visible = true;	
									break;		
                    		
                    	} 
                    		
                    }
                }
                else 
                {
                    
                    dt = ProductStorageDAL.getSAPBatchbyMaterialCodeInBom(WorkShop, OrderNo, MaterialCategory, MaterialCode);

                    /* if (dt != null && dt.Rows.Count > 0)
                    {
                        cb.DataSource = dt;
                        cb.DisplayMember = "BATCH";
                        cb.ValueMember = "BATCH";
                    }
                    else
                    {
                        DataTable dttemp = new DataTable();
                        dttemp.Columns.Add("BATCH");
                        DataRow _onlyRow = dttemp.NewRow();

                        switch (MaterialCategory)
                        {
                        
                            case "Glass":
                                label101.Visible = true;
                                _onlyRow[0] = "Dummy Glass";
                                break;                           
                            case "AIFrame":
                                label105.Visible = true;
                                _onlyRow[0] = "Dummy frame -1";
                                label106.Visible = true;                               
                                break;

                        }

                      

                        dttemp.Rows.Add(_onlyRow);
                        cb.DataSource = dttemp;

                        cb.DisplayMember = "BATCH";
                        cb.ValueMember = "BATCH";
                    } */

                    DataTable dttemp = new DataTable();
                    dttemp.Columns.Add("BATCH");
                    DataRow _onlyRow = dttemp.NewRow();
                    _onlyRow[0] = "FLEXDUMMY";
                    dttemp.Rows.Add(_onlyRow);
                    cb.DataSource = dttemp;

                    cb.DisplayMember = "BATCH";
                    cb.ValueMember = "BATCH";

                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region 下拉框联动事件
        private void comboxCellCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboxCellCodeDesc.SelectedIndex > -1)
            {
                try
                {
                    this.comboxCellCodeDesc.SelectedIndex = this.comboxCellCode.SelectedIndex;
                }
                catch (ArgumentOutOfRangeException ex)
                {
                }
                InitialSAPBatch(currentWorkshop, txtWoOrder.Text.ToString().Trim(), "Cell",
                        comboxCellCode.Text.ToString().Trim(), comboxCellSAPBatch);
            }
        }

        private void comboxGlassCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboxGlassCodeDesc.SelectedIndex > -1)
            {
                try
                {
                    this.comboxGlassCodeDesc.SelectedIndex = this.comboxGlassCode.SelectedIndex;
                }
                catch (ArgumentOutOfRangeException ex)
                {
                }
                InitialSAPBatch(currentWorkshop, txtWoOrder.Text.ToString().Trim(), "Glass",
               comboxGlassCode.Text.ToString().Trim(), comboxGlassSAPBatch);
                
               // GetGlassThinkness();
                
            }
        }

        private void comboxEVACode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboxEVACodeDesc.SelectedIndex > -1)
            {
                try
                {
                    this.comboxEVACodeDesc.SelectedIndex = this.comboxEVACode.SelectedIndex;
                }
                catch (ArgumentOutOfRangeException ex)
                {
                }
                InitialSAPBatch(currentWorkshop, txtWoOrder.Text.ToString().Trim(), "EVA",
                        comboxEVACode.Text.ToString().Trim(), comboxEVASAPBatch);
            }
        }

        private void comboxTPTCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboxTPTCodeDesc.SelectedIndex > -1)
            {
                try
                {
                    this.comboxTPTCodeDesc.SelectedIndex = this.comboxTPTCode.SelectedIndex;
                }
                catch (ArgumentOutOfRangeException ex)
                {
                }
                InitialSAPBatch(currentWorkshop, txtWoOrder.Text.ToString().Trim(), "TPT",
                       comboxTPTCode.Text.ToString().Trim(), comboxTPTSAPBatch);
            }
        }

        private void comboxConboxCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboxConboxCodeDesc.SelectedIndex > -1)
            {
                try
                {
                    this.comboxConboxCodeDesc.SelectedIndex = this.comboxConboxCode.SelectedIndex;
                }
                catch (ArgumentOutOfRangeException ex)
                {
                }
                InitialSAPBatch(currentWorkshop, txtWoOrder.Text.ToString().Trim(), "Conbox",
                        comboxConboxCode.Text.ToString().Trim(), comboxConboxSAPBatch);
            }
        }

        private void comboxSFrameCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboxSFrameCodeDesc.SelectedIndex>-1)
            {
            try
            {
                this.comboxSFrameCodeDesc.SelectedIndex = this.comboxSFrameCode.SelectedIndex;
 
            }
            catch (ArgumentOutOfRangeException ex)
            {
            }
            InitialSAPBatch(currentWorkshop, txtWoOrder.Text.ToString().Trim(), "AIFRAME",
                   comboxSFrameCode.Text.ToString().Trim(), comboxSFrameSAPBatch);
            }
        }

        private void comboxLFrameCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboxLFrameCodeDesc.SelectedIndex>-1)
            {
            try
            {
                this.comboxLFrameCodeDesc.SelectedIndex = this.comboxLFrameCode.SelectedIndex;
            }
            catch (ArgumentOutOfRangeException ex)
            {
            }
            InitialSAPBatch(currentWorkshop, txtWoOrder.Text.ToString().Trim(), "AIFRAME",
                   comboxLFrameCode.Text.ToString().Trim(), comboxLframeSAPBatch);
            }
        }
        #endregion

       
        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.panelDisplay.Enabled = true;
            this.txtTemplateName.Enabled = true;
            this.btnUpdate.Enabled = false;
            this.btnCancel.Enabled = true;
            btnSave.Enabled = true;

            this.txtTemplateName.SelectAll();
            this.txtTemplateName.Focus();

            if (!string.IsNullOrEmpty(txtWoOrder.Text.Trim().ToString()))
            {
                  SetWOData(false);   
            }
            InitialTolerance(txtWoOrder.Text.Trim().ToString(), "Add", "");

        }

        //保存时，先更新，更新不成功则添加
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (panelDisplay.Enabled == false)
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message12", "没有新增和更新，不需要保存!"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            string type = string.Empty;

            if (btnAdd.Enabled == true && btnUpdate.Enabled == false)
            {
                type = "add";
            }
            else if (btnAdd.Enabled == false && btnUpdate.Enabled == true)
            {
                type = "update";
            }
            else
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message12", "没有新增和更新，不需要保存!"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            
            //判断页面的输入项不能为空
            if (!CheckBeforeSave()) return;
            //if (!CheckTemplateName()) return;
      

            #region 从页面取值
            string TemplateName = this.txtTemplateName.Text.Trim().ToUpper();
            string TemplateDesc = this.txtTemplateDesc.Text.Trim();
            string WorkOrder = this.txtWoOrder.Text.Trim();

            string WorkShop = currentWorkshop;
            string ByIm = this.ddlByIm.Text.Trim();
            switch (ByIm)
            {
                case "是":
                    ByIm = "Y";
                    break;
                case "否":
                    ByIm = "N";
                    break;
                default:
                    break;
            }
            string CellTransfer = this.ddlCellTransfer.Text.Trim();
            string PackingPattern = this.ddlPackingPattern.Text.Trim();
            switch (PackingPattern)
            {
                case "Horizontal":
                    PackingPattern = "A";
                    break;
                case "Vertical":
                    PackingPattern = "B";
                    break;
                case "双件装":
                    PackingPattern = "C";
                    break;
                case "单件装":
                    PackingPattern = "D";
                    break;
                case "其它":
                    PackingPattern = "E";
                    break;
                default:
                    break;
            }
            string GlassLength = this.ddlGlassLength.Text.Trim();
            string ModuleGrade ="";
            /*
            string ModuleGrade = this.comboxModuleGrade.Text.Trim();
            switch (ModuleGrade)
            {
                case "A 等级":
                    ModuleGrade = "A";
                    break;
                case "B 等级":
                    ModuleGrade = "B";
                    break;
                case "废品":
                    ModuleGrade = "F";
                    break;
                default:
                    break;
            }
             */
            string CellCode = this.comboxCellCode.Text.Trim();
            string CellBatch = this.comboxCellSAPBatch.Text.Trim();

            string GlassCode = this.comboxGlassCode.Text.Trim();
            string GlassBatch = this.comboxGlassSAPBatch.Text.Trim();

            string EvaCode = this.comboxEVACode.Text.Trim();
            string EvaBatch = this.comboxEVASAPBatch.Text.Trim();

            string TptCode = this.comboxTPTCode.Text.Trim();
            string TptBatch = this.comboxTPTSAPBatch.Text.Trim();

            string ConBoxCode = this.comboxConboxCode.Text.Trim();
            string ConBoxBatch = this.comboxConboxSAPBatch.Text.Trim();

            string LongFrameCode = this.comboxLFrameCode.Text.Trim();
            string LongFrameBatch = this.comboxLframeSAPBatch.Text.Trim();

            string ShortFrameCode = this.comboxSFrameCode.Text.Trim();
            string ShortFrameBatch = this.comboxSFrameSAPBatch.Text.Trim();
            string Tolerance = this.comboxTolerance.Text.Trim();
				
            string Market = this.Cb_Market.Text.Trim();
            string LID = this.Cb_Lid.Text.Trim();
            string CabelLength = this.Cb_CabelLength.Text.Trim();
            string JboxType = this.Cb_JboxType.Text.Trim();
            
            #endregion

            #region Update
            if (type == "update")
            {
                try
                {
                   int retval = ProductStorageDAL.UpdateMaterialTemplate(TemplateName, TemplateDesc, WorkOrder, WorkShop, ByIm,
                    CellTransfer, PackingPattern, GlassLength, CellCode, CellBatch, GlassCode, GlassBatch, EvaCode, EvaBatch,
                    TptCode, TptBatch, ConBoxCode, ConBoxBatch, LongFrameCode, LongFrameBatch, ShortFrameCode, ShortFrameBatch, ModuleGrade, Tolerance, Market, LID, CabelLength, JboxType);

                    if (retval == 1)
                    {
                        MessageBox.Show("Updated completed。", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        btnAdd.Enabled = true;
                        btnUpdate.Enabled = true;
                        //panelMaterialCode.Enabled = false; //不显示btnFindMaterialCode后的更改
                        panelDisplay.Enabled = false;
                        this.btnCancel.Enabled = false;

                        return;
                    }
                    else
                    {
                        MessageBox.Show("Update failed！", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString(), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            #endregion
 			//Edit by jacky 2016/02/03
            #region Add
            if (type == "add")
            {
                if (string.IsNullOrEmpty(txtTemplateName.Text.ToString().Trim()))
                {
                    MessageBox.Show("Template name cannot be empty！", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTemplateName.Focus();
                    return;
                }

                try
                {
                    //Jacky Add Rework Item
                    bool isRework = FormCover.HasPowerControl("Rework");
                    int retval = ProductStorageDAL.AddMaterialTemplate(TemplateName, TemplateDesc, WorkOrder, WorkShop, ByIm,
                         CellTransfer, PackingPattern, GlassLength, CellCode, CellBatch, GlassCode, GlassBatch, EvaCode, EvaBatch,
                         TptCode, TptBatch, ConBoxCode, ConBoxBatch, LongFrameCode, LongFrameBatch, ShortFrameCode, ShortFrameBatch, ModuleGrade, Tolerance, Market, LID, CabelLength,JboxType,isRework);
             

                    if (retval == 1)
                    {
                      	ProductStorageDAL.GetWoMaxCount(WorkOrder);
                    	MessageBox.Show("Update successful！", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        btnAdd.Enabled = true;
                        btnUpdate.Enabled = true;
                        //panelMaterialCode.Enabled = false; //不显示btnFindMaterialCode后的更改
                        panelDisplay.Enabled = false;
                        this.btnCancel.Enabled = false;
                       

                        return;
                    }
                    else
                    {
                        MessageBox.Show("Adding Template failed！", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    if(ex.Message.ToString().Contains(@"Violation of UNIQUE KEY constraint 'UK_T_PACK_MATERIAL'. Cannot insert duplicate key in object 'dbo.T_PACK_MATERIAL_TEMPLATE'."))
                    {
                        MessageBox.Show("Template with same name existed！", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        MessageBox.Show(ex.Message.ToString(), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
            #endregion

        }
        
        private bool CheckTemplateName()
        {
        	string tempname = txtTemplateName.Text.Trim().ToString();
        	if(!tempname.Substring(0,10).ToUpper().Equals(txtWoOrder.Text.Trim()))
        		
        	{
        		MessageBox.Show("Enter template name (Child MO + Sequence Number), description(Date + Comments) and production order! ");
        		return false;
        	}
        	return true;
        	
        }

        private bool CheckBeforeSave()
        {
            //重工工单不用检查
            if (ProductStorageDAL.GetWOType(this.txtWoOrder.Text.Trim()).ToUpper().Equals("ZP11"))
                return true;

            if (CheckControlTextIsEmpty(txtTemplateName)) return false;
            if (CheckControlTextIsEmpty(txtTemplateDesc)) return false;
            if (CheckControlTextIsEmpty(txtWoOrder)) return false;
            //if (CheckControlTextIsEmpty(comboxModuleGrade)) return false; by hexing 2013 09 11
            if (CheckControlTextIsEmpty(ddlGlassLength)) return false;
            if (CheckControlTextIsEmpty(ddlByIm)) return false;
            if (CheckControlTextIsEmpty(ddlCellTransfer)) return false;
            if (CheckControlTextIsEmpty(ddlPackingPattern)) return false;

            if (CheckControlTextIsEmpty(comboxCellCode)) return false;
            if (CheckControlTextIsEmpty(comboxGlassCode)) return false;
            if (CheckControlTextIsEmpty(comboxEVACode)) return false;
            if (CheckControlTextIsEmpty(comboxTPTCode)) return false;
            if (CheckControlTextIsEmpty(comboxConboxCode)) return false;

            //if (!FormCover.WOTypeCode.Trim().ToUpper().Equals("ZP09"))
            //{
                if (CheckControlTextIsEmpty(comboxLFrameCode)) return false;
                if (CheckControlTextIsEmpty(comboxSFrameCode)) return false;
                if (CheckControlTextIsEmpty(comboxLframeSAPBatch)) return false;
                if (CheckControlTextIsEmpty(comboxSFrameSAPBatch)) return false;
            //}
			
            //Jacky
            if (CheckControlTextIsEmpty(comboxCellSAPBatch)) return false;
            if (CheckControlTextIsEmpty(comboxGlassSAPBatch)) return false;
            if (CheckControlTextIsEmpty(comboxEVASAPBatch)) return false;
            if (CheckControlTextIsEmpty(comboxTPTSAPBatch)) return false;
            if (CheckControlTextIsEmpty(comboxConboxSAPBatch)) return false;
            if (CheckControlTextIsEmpty(comboxTolerance)) return false;
            if (CheckControlTextIsEmpty(Cb_Market)) return false;
            if (CheckControlTextIsEmpty(Cb_Lid)) return false;
            if (CheckControlTextIsEmpty(Cb_CabelLength)) return false;
            if (CheckControlTextIsEmpty(Cb_JboxType)) return false; 
            return true;

        }
        
        // 检查控件的text是否为空
        private bool CheckControlTextIsEmpty(Control c)
        {
            bool isEmpty = false;

            if (string.IsNullOrEmpty(c.Text.ToString().Trim()))
            {
                isEmpty = true;
                MessageBox.Show(c.Tag + "Cannot be empty！", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                c.Focus();
            }
            return isEmpty;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to delete selected template？", "Prompting Message", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                try
                {
                    string sqlStr = "DELETE FROM [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE] WHERE SYSID = '{0}'";
                    sqlStr = string.Format(sqlStr, dataGridView1.SelectedRows[0].Cells[2].Value.ToString().Trim());
                    int retval = ToolsClass.ExecuteNonQuery(sqlStr);
                    if (retval > 0)
                    {
                        
                        MessageBox.Show("Delete successful！", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
                        ClearDisplayContent();
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Delete failed！", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString(), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            }
        }

        private void btnFindMaterialCode_Click(object sender, EventArgs e)
        {
            string workorder = txtWoOrder.Text.ToString().Trim();
            if (workorder == "")
            {
                MessageBox.Show("Please select production order!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtWoOrder.Focus();
                return;
            }
            else
            {
                if (MessageBox.Show("updating following material infomation, are you sure to continue？", "Prompting Message", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    //panelMaterialCode.Enabled = true; //不显示btnFindMaterialCode后的更改
                    InitialMaterialCode(workorder);
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //更新模板时提示如下信息
            string templateName = txtTemplateName.Text.ToString().Trim();
            string strTolerance = this.comboxTolerance.Text.ToString().Trim();


            if (ProductStorageDAL.HasLinkedWithStoraged(templateName))
            {
                MessageBox.Show("Template has linked to module being stocked in, cannot modify!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (ProductStorageDAL.HasLinkedWithoutStoraged(templateName))
            {
                if (MessageBox.Show("Template has linked to module，do you want to modify？", "Prompting Message", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    return;
            }  

            this.panelDisplay.Enabled = true;
            this.txtTemplateName.Enabled = false;

            btnSave.Enabled = true;
            this.btnCancel.Enabled = true;

            btnAdd.Enabled = false;

            if (!string.IsNullOrEmpty(txtWoOrder.Text.Trim().ToString()))
            {
                SetWOData(false);
                string sqlPack = @"select PackingMode from [CentralizedDatabase].[dbo].[T_PACK_MATERIAL_TEMPLATE] where MaterialTemplateName = '{0}' ";
                sqlPack =  string.Format(sqlPack,txtTemplateName.Text.ToString());
                SqlDataReader rdp = ToolsClass.GetDataReader(sqlPack,FormCover.CenterDBConnString);
                if(rdp!= null && rdp.Read())
                {
                	switch (rdp.GetString(0).Trim())
                	{
                		case "A":
                			ddlPackingPattern.Text = "Horizontal";
                			break;
                		case "B": 
                			ddlPackingPattern.Text = "Vertical";
                			break;
                		default:
                			break;                				
                	}
                	
                }
           
                InitialTolerance(txtWoOrder.Text.Trim().ToString(), "Update", strTolerance);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnAdd.Enabled = true;
            btnUpdate.Enabled = true;
            //panelMaterialCode.Enabled = false; //不显示btnFindMaterialCode后的更改
            panelDisplay.Enabled = false;
            ClearDisplayContent();
            this.btnCancel.Enabled = false;
            btnSave.Enabled = false;
            //txtTemplateName.Enabled = true;
        }

        	#region Color Change	
		private void Color_Change(object sender)
		{
			((Control)sender).ForeColor = Color.Red;		
			
		}
			
		 void ClearAllIndiCators()
        {
        	Color_Back(label4);
        	Color_Back(label13);
        	Color_Back(label16);
            Color_Back(label19);
       		Color_Back(label22);
       		Color_Back(label26);
       		Color_Back(label29);
       		label100.Visible = false;
       		label101.Visible = false;
       		label102.Visible = false;
       		label103.Visible = false;
       		label104.Visible = false;
       		label105.Visible = false;
       		label106.Visible = false;      		
        	
        }
		private void Color_Back(object sender)
		{
			((Control)sender).ForeColor = Color.Black;
		}
		
	
		#endregion
		private void GetGlassThinkness()
		{
			  if (this.panelDisplay.Enabled == false)
            {
                return;
            }
            if (string.IsNullOrEmpty(comboxGlassCodeDesc.Text) || comboxGlassCodeDesc.Text.Length < 18)
            {
                ddlGlassLength.Text = "Other";
                return;
            }
			
				string length =  comboxGlassCodeDesc.Text.Trim().Substring(15,3);
               	switch (length) {
                	case "3.2":
                		ddlGlassLength.Text = "3.2mm";
                		break;                	
               		case "4.0":
               			ddlGlassLength.Text = "4mm";
               			break;
               		default:
               			ddlGlassLength.Text = "Other";
               			break;
               	}
			  
		}
		
		private void SetComboxValue(ComboBox cb, string Value)
		{
            if (string.IsNullOrEmpty(Value))
                cb.Items.Insert(0, "NA");
            else            
			    cb.Items.Insert(0, Value);
			cb.SelectedIndex = 0;
		}
       
        
        private void Bt_ModifyClick(object sender, EventArgs e)
        {
        	Cb_Market.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
        	Cb_Lid.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
        	//Cb_CabelLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
        	//Cb_JboxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
        }
        private void ComboxBackToList(ComboBox cb)
        {
        	cb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        }
        
     
    }
}
