﻿using System;
using System.ComponentModel;
//using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Collections.Generic;
using LablePLibrary;
using System.Text.RegularExpressions;

namespace CSICPR
{
    public partial class FormWoData : Form     
    {
        private static List<LanguageItemModel> LanMessList;//定义语言集
        #region 共有变量
        private static FormWoData theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormWoData();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        public FormWoData()
        {
            InitializeComponent();
        }
        

        private void txtWoOrder_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                CtrlClear();
                btnQuery_Click(null,null);
            }
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            if (TargetQty < 1)
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message1", "没有要修改的工单"));//,"信息提示",MessageBoxButtons.OK,MessageBoxIcon.Information);
                return;
            }

            this.txtWOScrapQty.Enabled = true;
            this.txtInputQty.Enabled = true;

            this.btnCancel.Enabled = true;
            this.btnSave.Enabled = true;

            this.btnQuery.Enabled = false;
            this.btnModify.Enabled = false;
        }

        private int ScrapQty = 0; //报废量    
        private int TargetQty = 0;//计划量
        private int StorageQty = 0;//已入库量
        private int InputQty = 0;//实际投产量
        private void CtrlClear()
        {
            this.txtWONumber.Text = "";
            this.txtWOTargetQty.Text = "";
            this.txtWoStorageQty.Text = "";

            this.txtWOScrapQty.Text = "";
            this.txtInputQty.Text = "";


            this.txtOldInputQty.Text = "";
            this.txtOldWOScrapQty.Text = "";
            this.txtWaitStorageQTT.Text = "";
                  
        }
        private void btnQuery_Click(object sender, EventArgs e)
        {
            CtrlClear();

            if (this.txtWoOrder.Text.Trim().Equals(""))
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message2", "工单号码不能为空!"));//, "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.txtWoOrder.Focus();
                return;
            }
            else
            {
                DataTable dt = ProductStorageDAL.GetWoDataIno(this.txtWoOrder.Text.Trim());
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    this.txtWONumber.Text = this.txtWoOrder.Text.Trim();
                    if (Convert.ToString(row["TWO_WO_QTY"]).Equals(""))
                        TargetQty = 0;
                    else
                        TargetQty = Convert.ToInt32(row["TWO_WO_QTY"]);
                    this.txtWOTargetQty.Text = Convert.ToString(TargetQty); //计划量

                    if (Convert.ToString(row["RESV07"]).Equals(""))//已入库量
                        StorageQty = 0;
                    else
                        StorageQty = Convert.ToInt32(row["RESV07"]);
                    this.txtWoStorageQty.Text = Convert.ToString(StorageQty);//已入库量

                    if (Convert.ToString(row["RESV08"]).Equals(""))//实际投产量
                        InputQty = 0;
                    else
                        InputQty = Convert.ToInt32(row["RESV08"]);
                    this.txtInputQty.Text = Convert.ToString(InputQty);//实际投产量
                    this.txtOldInputQty.Text = Convert.ToString(InputQty);


                    if (Convert.ToString(row["RESV09"]).Equals(""))//报废量
                        ScrapQty = 0;
                    else
                        ScrapQty = Convert.ToInt32(row["RESV09"]);
                    this.txtWOScrapQty.Text = Convert.ToString(ScrapQty);//报废量
                    this.txtOldWOScrapQty.Text = Convert.ToString(ScrapQty);

                    if (InputQty > 0)
                        this.txtWaitStorageQTT.Text = Convert.ToString(InputQty - ScrapQty - StorageQty);
                    else
                        this.txtWaitStorageQTT.Text = Convert.ToString(TargetQty - ScrapQty - StorageQty);

                    this.txtWoOrder.Text = "";
                }
                else
                {
                    MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message3", "没有找到此工单的信息!"));//, "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.txtWoOrder.Text = "";
                    this.txtWoOrder.Focus();
                    return;
                }
            }
        }

        private void FormWoData_Load(object sender, EventArgs e)
        {
         
            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion
            this.btnCancel.Enabled = false;
            this.btnSave.Enabled = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.txtInputQty.Enabled = false;
            this.txtWOScrapQty.Enabled = false;

            this.btnCancel.Enabled = false;
            this.btnSave.Enabled = false;

            this.btnQuery.Enabled = true;
            this.btnModify.Enabled = true;

            this.txtWOScrapQty.Text = Convert.ToString(ScrapQty);
            this.txtInputQty.Text = Convert.ToString(InputQty);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (StorageQty >= TargetQty)
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message4", "工单已入库数量已经大于或等于工单计划量"));//, "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (Convert.ToUInt32(this.txtInputQty.Text.Trim()) > TargetQty)
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message5", "工单实际投产量不能大于计划量"));//,"信息提示",MessageBoxButtons.OK,MessageBoxIcon.Information);
                return;
            }

            if (Convert.ToUInt32(this.txtWOScrapQty.Text.Trim()) > Convert.ToUInt32(this.txtInputQty.Text.Trim()))
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message6", "工单报废量不能大于实际投产量"));//, "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (MessageBox.Show(String.Format(LanguageHelper.GetMessage(LanMessList, "Message9", "确定要把投入量改为:{0},报废量改为{1}"), this.txtInputQty.Text.Trim(), this.txtWOScrapQty.Text.Trim()), LanguageHelper.GetMessage(LanMessList, "Message10", "信息提示"),
                MessageBoxButtons.OKCancel,MessageBoxIcon.Question) == DialogResult.OK)
            {
                string msg = "";
                if (ProductStorageDAL.UpdateWoDataIno(this.txtWONumber.Text.Trim(), Convert.ToInt32(this.txtInputQty.Text.Trim()),
                    Convert.ToInt32(this.txtWOScrapQty.Text.Trim()), out msg))
                {
                    CtrlClear();
                    this.txtInputQty.Enabled = false;
                    this.txtWOScrapQty.Enabled = false;
                    this.btnQuery.Enabled = true;
                    this.btnModify.Enabled = true;
                    this.btnCancel.Enabled = false;
                    this.btnSave.Enabled = false;
                    this.txtWoOrder.Focus();
                    MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message7", "更新成功"));
                }
                else
                    MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message8", "更新失败：原因 ") + msg + "");
            }
        }

        private void txtWOScrapQty_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        /// <summary>
        /// 这种写法会有问题的
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtInputQty_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8)
            //{
            //    e.Handled = true;
            //}
        }

        private void txtWOScrapQty_TextChanged(object sender, EventArgs e)
        {
            var reg = new Regex("^[0-9]*$");           
            var str = txtWOScrapQty.Text.Trim();            
            var sb = new StringBuilder();
            if (!reg.IsMatch(str))
            {
                for (int i = 0; i < str.Length; i++)
                {
                    if (reg.IsMatch(str[i].ToString()))
                    {
                        sb.Append(str[i].ToString());
                    }
                }
                txtWOScrapQty.Text = sb.ToString();
                txtWOScrapQty.SelectionStart = txtWOScrapQty.Text.Length; //定义输入焦点在最后一个字符
            }

            if (string.IsNullOrEmpty(this.txtInputQty.Text.Trim()))
                return;
            if (string.IsNullOrEmpty(this.txtWOScrapQty.Text.Trim()))
                return;
            if (string.IsNullOrEmpty(this.txtWoStorageQty.Text.Trim()))
                return;

            int waitqty = Convert.ToInt32(this.txtInputQty.Text.Trim()) -
                 Convert.ToInt32(this.txtWOScrapQty.Text.Trim()) -
                 Convert.ToInt32(this.txtWoStorageQty.Text.Trim());
            this.txtWaitStorageQTT.Text = Convert.ToString(waitqty);
        }

        private void txtInputQty_TextChanged(object sender, EventArgs e)
        {
            var reg = new Regex("^[0-9]*$");
            var str = txtInputQty.Text.Trim();
            var sb = new StringBuilder();
            if (!reg.IsMatch(str))
            {
                for (int i = 0; i < str.Length; i++)
                {
                    if (reg.IsMatch(str[i].ToString()))
                    {
                        sb.Append(str[i].ToString());
                    }
                }
                txtInputQty.Text = sb.ToString();
                txtInputQty.SelectionStart = txtInputQty.Text.Length; //定义输入焦点在最后一个字符
            }

            if (string.IsNullOrEmpty(this.txtInputQty.Text.Trim()))
                return;
            if (string.IsNullOrEmpty(this.txtWOScrapQty.Text.Trim()))
                return;
            if (string.IsNullOrEmpty(this.txtWoStorageQty.Text.Trim()))
                return;
            int waitqty = Convert.ToInt32(this.txtInputQty.Text.Trim()) -
                 Convert.ToInt32(this.txtWOScrapQty.Text.Trim()) -
                 Convert.ToInt32(this.txtWoStorageQty.Text.Trim());
            this.txtWaitStorageQTT.Text = Convert.ToString(waitqty);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            CtrlClear();
        }

        private void txtWOScrapQty_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtWOScrapQty.Text.Trim()))
                this.txtWOScrapQty.Text = "0";
        }

        private void txtInputQty_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtInputQty.Text.Trim()))
                this.txtInputQty.Text = "0";
        }

    }
}
