﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormAccountManager : Form
    {
        private static List<LanguageItemModel> LanMessList;
        private DataTable prodDateSource;
        private string sAccount = "",sPsw="",sNote="";
        private int DGSelectRowCnt=-1;
        private DataGridViewRow dgRow;

        private const int iAdminGroup = 9;//administrator group
        private const int iSuperGroup = 10;//supervisor group
        private const int iCommGroup = 11;//common user group
        private const int iMTCGroup = 12;//MTC group
        private const int iRWKGroup = 13;//Rework user group
 	    private const int iMCTLeader = 14; //MCT leader group
 	    //15 is reseved
 	    private const int iPackLeader = 16; // Packaging leader group

        public FormAccountManager()
        {
            InitializeComponent();
            this.prodDateSource = new DataTable();
            this.dgRow = new DataGridViewRow();
            //languagechange change = new languagechange("FormAccountManager", this);
            //languagechange.changeBTN(false);
            //languagechange.changefrmName(false);
            //languagechange.changeLBL(false);
            //languagechange.changeDataGrid(false);
        }

        private void FormAccountManager_Load(object sender, EventArgs e)
        {
            #  region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion
            
            string sqlStr = "";
            btnDel.Enabled = false;
            btnModify.Enabled = false;
            if (FormCover.isManager)
            {
                sqlStr = "select UserNM,UserPW,UserNote,Status,UserGroup from UserLogin";
                btnAdd.Enabled = true;
                btnDel.Enabled = true;
            }
            else 
            { 
                sqlStr = "select UserNM,UserPW,UserNote,Status,UserGroup from UserLogin where UserNM='"+FormCover.currUserName+"'";
                btnAdd.Enabled = false;
                btnDel.Enabled = false;
            }
            
            int rowNum = 0;
            ToolsClass.GetDataTable(sqlStr, this.prodDateSource);
            foreach (DataRow row in this.prodDateSource.Rows)
            { 
                rowNum = AccountList.Rows.Add();
                AccountList.Rows[rowNum].Cells[0].Value = row[0];
                AccountList.Rows[rowNum].Cells[1].Value = "******";//row[1];
                AccountList.Rows[rowNum].Cells[2].Value = row[2];
                //AccountList.Rows[rowNum].Cells[3].Value = row[3];
                int temp =int.Parse(row[4].ToString());
                if (temp == iAdminGroup) AccountList.Rows[rowNum].Cells[3].Value = "Administrator";
                if (temp == iSuperGroup) AccountList.Rows[rowNum].Cells[3].Value = "Supervisor";
                if (temp == iCommGroup) AccountList.Rows[rowNum].Cells[3].Value = "Common User";
                if (temp == iMTCGroup) AccountList.Rows[rowNum].Cells[3].Value = "Material Control";
                if (temp == iRWKGroup) AccountList.Rows[rowNum].Cells[3].Value = "Rework";
                if (temp == iMCTLeader) AccountList.Rows[rowNum].Cells[3].Value = "MCT Leader";
                if (temp == iPackLeader) AccountList.Rows[rowNum].Cells[3].Value = "Packaging Leader";
               
            }
            txtAccount.Focus();
            //FormCover.isManager = true;
            //MessageBox.Show("无可用包号码！请更新包号码池！", "提示信息", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //iGroup=1 admin;iGroup=2 supervisor;iGroup=3 common user
            int isChecked = -1, iGroup = -1, InterID = 0;
            string sqlStr="";
            sqlStr = "select * from Userlogin where UserNM='"+txtAccount.Text+"'";
            SqlDataReader rd = ToolsClass.GetDataReader(sqlStr); //int rd = ToolsClass.ExecuteNonQuery(sqlStr);
            if (rd.Read())
            {
                //MessageBox.Show("This Account Already Exist!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message6", "This Account Already Exist"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            sqlStr = "select max(InterID) from Userlogin";
            SqlDataReader read=ToolsClass.GetDataReader(sqlStr);
            
            if (read != null && read.Read())
            {
                  InterID = read.GetInt32(0);
            }
            InterID = InterID + 1;

            if (rBtnYes.Checked)
            {
                isChecked = 1;
                iGroup = iAdminGroup;
            }
            else if (rBtnOPT.Checked) { isChecked = 0; iGroup = iCommGroup; }
            else if (rBtnSuper.Checked) { isChecked = 1; iGroup = iSuperGroup; }
            else if (rBtnMCT.Checked) { isChecked = 0; iGroup = iMTCGroup; }
			else if (rBtnRWK.Checked) { isChecked = 0; iGroup = iRWKGroup; }
			else if (rBtnMCT.Checked) { isChecked = 0; iGroup = iMTCGroup; }
			else if (rBtnRWK.Checked) { isChecked = 0; iGroup = iRWKGroup; }
			else if (rBtnML.Checked) { isChecked = 0; iGroup = iMCTLeader; }
			else if (rBtnPL.Checked) { isChecked = 0; iGroup = iPackLeader; }

            if (txtAccount.Text.Length > 0)// & txtPsw.Text.Length>0 & isChecked.Length>0)
            {
                if (txtPsw.Text.Length > 0)
                {
                    if (txtPsw.Text.Trim() == txtCRM.Text.Trim())
                    {
                        if (isChecked > -1)
                        {
                            sqlStr = "insert into UserLogin(InterID,UserNM,UserPW,UserNote,Status,UserGroup) values(" + InterID + ",'" + txtAccount.Text + "','" + txtPsw.Text + "','" + txtUserName.Text + "'," + isChecked + ","+iGroup+")";
                            ToolsClass.ExecuteNonQuery(sqlStr);
                            int rowNum = AccountList.Rows.Add();
                            AccountList.Rows[rowNum].Cells[0].Value = txtAccount.Text;
                            AccountList.Rows[rowNum].Cells[1].Value = "******";// txtPsw.Text;
                            AccountList.Rows[rowNum].Cells[2].Value = txtUserName.Text;
                            //if (isChecked == 1) AccountList.Rows[rowNum].Cells[3].Value = "Yes";
                            //if (isChecked == 0) AccountList.Rows[rowNum].Cells[3].Value = "No";
                            if (rBtnOPT.Checked) AccountList.Rows[rowNum].Cells[3].Value = "Common User";
                            if (rBtnYes.Checked) AccountList.Rows[rowNum].Cells[3].Value = "Administrator";
                            if (rBtnSuper.Checked) AccountList.Rows[rowNum].Cells[3].Value = "Supervisor";
                            if (rBtnMCT.Checked) AccountList.Rows[rowNum].Cells[3].Value = "Material control";
							if (rBtnRWK.Checked) AccountList.Rows[rowNum].Cells[3].Value = "Rework";
							if (rBtnML.Checked) AccountList.Rows[rowNum].Cells[3].Value = "MCT Leader";
               				if (rBtnPL.Checked) AccountList.Rows[rowNum].Cells[3].Value = "Packaging Leader";

                            txtAccount.Text = "";
                            txtPsw.Text = "";
                            txtCRM.Text = "";
                            txtUserName.Text = "";
                            rBtnOPT.Checked = false;
                            rBtnYes.Checked = false;
                            rBtnSuper.Checked = false;
                            rBtnMCT.Checked = false;
                            rBtnRWK.Checked = false;
                            rBtnML.Checked = false;
                            rBtnPL.Checked = false;                            	
                            txtAccount.Enabled = true;
                            txtAccount.Focus();
                        }
                        else 
                        { 
                           // MessageBox.Show("Please Confirm UserGroup Option!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message7", ""), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            rBtnOPT.Focus();
                        }
                    }
                    else
                    {
                        //MessageBox.Show("Confirm Password not match Password!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message3", "Confirm Password not match Password!"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtPsw.Text = "";
                        txtCRM.Text = "";
                        txtPsw.Focus();
                    }
                }
                else 
                {
                    //MessageBox.Show("Password can not be empty!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning); txtPsw.Focus(); 
                    MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message4", "Password can not be empty!"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning); txtPsw.Focus(); 
                }
            }
            else
            { 
                //MessageBox.Show("Account can not be empty!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning); txtAccount.Focus(); 
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message4", "Password can not be empty!"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning); txtAccount.Focus(); 
            }

        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            int isChecked = -1, iGroup = -1;
            string sqlStr = "";

            if (rBtnYes.Checked)
            {
                isChecked = 1;
                iGroup = iAdminGroup;
            }
            else if (rBtnOPT.Checked)
            {
                isChecked = 0;
                iGroup = iCommGroup;
            }
            else if(rBtnSuper.Checked)
            {
                iGroup = iSuperGroup;
                isChecked = 1;
            }
            else if(rBtnMCT.Checked)
            {
                iGroup = iMTCGroup;
                isChecked = 0;
            }
            else if(rBtnRWK.Checked)
            {
                iGroup = iRWKGroup;
                isChecked = 0;
            }
            else if(rBtnML.Checked)
            {
            	iGroup = iMCTLeader;
            	isChecked = 0;
            }
            else if(rBtnPL.Checked)
            {
            	iGroup = iPackLeader;
            	isChecked = 0;
            }

            if (txtAccount.Text.Length > 0) 
            {
                if (txtPsw.Text.Length > 0)
                {
                    if (txtPsw.Text.Trim() == txtCRM.Text.Trim())
                    {
                        //if (isChecked > -1)
                        //{
                        if (!FormCover.isManager)
                            sqlStr = "Update Userlogin set UserPW='" + txtPsw.Text + "',UserNote='" + txtUserName.Text + "' where UserNM='" + this.sAccount + "'";
                        if (FormCover.isManager)
                            sqlStr = "Update Userlogin set UserPW='" + txtPsw.Text + "',UserNote='" + txtUserName.Text + "',Status=" + isChecked + ",UserGroup=" + iGroup + " where UserNM='" + this.sAccount + "'";
                                
                       // if (MessageBox.Show("Are you sure you want to change account info?", "Prompting Message", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning) == DialogResult.Yes)
                        if (MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message1", "Are you sure you want to change account info?"), "Prompting Message", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            int i = ToolsClass.ExecuteNonQuery(sqlStr);
                            if (i > 0)
                               // MessageBox.Show("Modify Account Success", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message2", "Modify Account Success"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            AccountList.Rows[this.DGSelectRowCnt].Cells[0].Value = txtAccount.Text;
                            AccountList.Rows[this.DGSelectRowCnt].Cells[1].Value = "******";//txtPsw.Text;
                            AccountList.Rows[this.DGSelectRowCnt].Cells[2].Value = txtUserName.Text;
                            if (rBtnOPT.Checked) AccountList.Rows[this.DGSelectRowCnt].Cells[3].Value = "Common User";
                            if (rBtnYes.Checked) AccountList.Rows[this.DGSelectRowCnt].Cells[3].Value = "Administrator";
                            if (rBtnSuper.Checked) AccountList.Rows[this.DGSelectRowCnt].Cells[3].Value = "Supervisor";
                            if (rBtnMCT.Checked) AccountList.Rows[this.DGSelectRowCnt].Cells[3].Value = "Material Control";
                            if (rBtnRWK.Checked) AccountList.Rows[this.DGSelectRowCnt].Cells[3].Value = "Rework";
                            if (rBtnML.Checked) AccountList.Rows[this.DGSelectRowCnt].Cells[3].Value = "MCT Leader";
                            if (rBtnPL.Checked) AccountList.Rows[this.DGSelectRowCnt].Cells[3].Value = "Packaging Leader";
                            btnModify.Enabled = false;
                            btnDel.Enabled = false;
                            if (FormCover.isManager)
                            btnAdd.Enabled = true;
                            txtAccount.Enabled = true;
                            txtAccount.Text = "";
                            txtPsw.Text = "";
                            txtCRM.Text = "";
                            txtUserName.Text = "";

                            rBtnYes.Enabled = true;
                            rBtnSuper.Enabled = true;
                            rBtnOPT.Enabled = true;

                            rBtnYes.Checked = false;
                            rBtnOPT.Checked = false;
                            rBtnSuper.Checked = false;
                            rBtnRWK.Checked = false;
                            rBtnML.Checked = false;
                            rBtnPL.Checked = false;
                        }
                        //}
                        //else { MessageBox.Show("Please Confirm Is Manager Option!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning); rBtnNo.Focus(); }
                    }
                    else
                    {
                        //MessageBox.Show("Confirm Password not match Password!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message3", "Confirm Password not match Password!"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtPsw.Text = "";
                        txtCRM.Text = "";
                        txtPsw.Focus();
                    }
                    
                }
                else
                { 
                   // MessageBox.Show("Password can not be empty!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning); txtPsw.Focus();
                    MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message4", "Password can not be empty!"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning); txtPsw.Focus(); 
                }
            }
            else 
            {
               // MessageBox.Show("Account can not be empty!", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning); txtAccount.Focus(); 
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message5", "Account can not be empty!"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning); txtAccount.Focus(); 
            }
            
        }

        private void AccountList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (this.sAccount.Equals("admin"))
            {
               // MessageBox.Show("can not delete admin", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message8", "can not delete admin"), "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            //if (MessageBox.Show("Are you sure you want to Delete Account：" + this.sAccount, "Prompting Message", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning) == DialogResult.Yes)
            if (MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message9", "Are you sure you want to Delete Account：") + this.sAccount, "Prompting Message", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                string sqlStr = "delete from UserLogin where UserNM='" + this.sAccount + "'";
                int i = ToolsClass.ExecuteNonQuery(sqlStr);
                if (i > 0) MessageBox.Show("Delete Account Success", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                AccountList.Rows.Remove(this.dgRow);
            }
            btnDel.Enabled = false;
            btnModify.Enabled = false;
        }

        private void AccountList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnModify.Enabled = true;
            txtAccount.Enabled = false;
            btnDel.Enabled = false;
            btnAdd.Enabled = false;

            if (!FormCover.isManager)
            {
                rBtnYes.Enabled = false;
                rBtnSuper.Enabled = false;
                rBtnOPT.Enabled = false;
                rBtnMCT.Enabled = false;
                rBtnRWK.Enabled = false;
                rBtnML.Enabled = false;
                rBtnPL.Enabled = false;
            }

            this.sAccount = AccountList.SelectedRows[0].Cells[0].Value.ToString().Trim();
            string sqlStr = "";
            sqlStr = "select UserPW,UserNote,Status,UserGroup from Userlogin where UserNM='" + this.sAccount + "'";
            SqlDataReader read = ToolsClass.GetDataReader(sqlStr);
            if (read != null && read.Read())
            {
                this.sPsw = read.GetString(0);
                this.sNote = read.GetString(1);
                //dChecked = read.GetInt32(2);
                if (read.GetBoolean(2) && read.GetInt32(3) == iAdminGroup)
                {
                    rBtnYes.Checked = true;
                }
                else if (read.GetBoolean(2) && read.GetInt32(3) == iSuperGroup)
                {
                    rBtnSuper.Checked = true;
                }
              	else if (read.GetBoolean(2) && read.GetInt32(3) == iMTCGroup)
                {
                    rBtnMCT.Checked = true;
                }
              	else if (read.GetBoolean(2) && read.GetInt32(3) == iRWKGroup)
                {
                    rBtnRWK.Checked = true;
                }
              	else if (read.GetBoolean(2) && read.GetInt32(3) == iMCTLeader)
                {
                    rBtnML.Checked = true;
                }
              	else if (read.GetBoolean(2) && read.GetInt32(3) == iPackLeader)
                {
                    rBtnPL.Checked = true;
                }
              	else { rBtnOPT.Checked = true; }
            }
            read.Close();
            read = null;

            this.DGSelectRowCnt = AccountList.CurrentRow.Index;
            txtAccount.Text = sAccount;
            txtPsw.Text = this.sPsw;
            txtCRM.Text = this.sPsw;
            txtUserName.Text = this.sNote;

            if (FormCover.isManager)
            {
                btnDel.Enabled = true;
                this.dgRow = AccountList.CurrentRow;
                //sAccount = AccountList.SelectedRows[0].Cells[0].Value.ToString().Trim();
            }
        }

    }

}
