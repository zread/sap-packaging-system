﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
namespace CSICPR
{
    public class PackinSystemg
    {
        private static string ReworkOrder = "";
        private static string Factory = "";
        private static List<string> CartonList = new List<string>();

        /// <summary>
        /// 集中数据库连接字符串
        /// </summary>
        private static string CenterDBSQLConnetion
        {
            get
            {
                return FormCover.CenterDBConnString;
            }
        }

        /// <summary>
        /// 集中数据库电脑名
        /// </summary>
        private static string CenterDBName
        {
            get
            {
                return FormCover.CenterDBName;
            }
        }

        /// <summary>
        /// 包装数据库服务器所在电脑名
        /// </summary>
        private static string DBName
        {
            get
            {
                return FormCover.PackingDBName;
            }
        }


        /// <summary>
        /// SAP接口数据库DB名字
        /// </summary>
        private static string SAPDBName
        {
            get
            {
                return FormCover.SAPDBName;
            }
        }
        /// <summary>
        /// 重工
        /// </summary>
        /// <param name="ReworkOrder">重工工单</param>
        /// <param name="PickingTo">来自哪个工厂</param>
        /// <param name="CartonNos">箱号</param>
        /// <param name="msg">出去参数</param>
        /// <returns></returns>
        public static bool PackinSystemgRework(string ReworkOrder, List<string> PickingFroms, List<string> CartonNos, out string msg)
        {
            try
            {
            	List<MODULEPACKINGTRANSACTION> _ListModulePacking = new List<MODULEPACKINGTRANSACTION>();
                List<CARTONPACKINGTRANSACTION> _ListCartonPacking = new List<CARTONPACKINGTRANSACTION>();
                #region
                msg = "";
                string CartonNoString = "";
                if (CartonNos.Count > 1)
                {
                    foreach (string carton in CartonNos)
                    {
                        CartonNoString += ",'" + carton + "'";
                    }
                    CartonNoString = CartonNoString.Substring(1, CartonNoString.Length - 1);
                    CartonNoString = "(" + CartonNoString + ")";
                }
                else if (CartonNos.Count == 1)
                {
                    foreach (string carton in CartonNos)
                    {
                        CartonNoString = "('" + carton + "')";
                    }
                }
                else
                {
                    msg = "托号为空";
                    return false;
                }
                //写入集中数据库重工数据
                string DayTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                string sqlCenterDB = @"INSERT INTO {0}.[dbo].[T_MODULE_REWORK]
                                       ([MODULE_SN],[BARCODE],[WORK_ORDER],[REWORK],[WORKSHOP],[MODULE_TYPE]
                                       ,[STD_POWER_LEVEL],[CARTON_NO],[CUST_CARTON_NO]
                                       ,[PMAX],[TEMP],[ISC],[VOC],[IMP],[VMP],[FF],[EFF],[TEST_DATE]
                                       ,[CREATED_ON],[CREATED_BY],[RESV1])
                              SELECT module.MODULE_SN,module.BARCODE,module.WORK_ORDER,'{1}',module.WORKSHOP,module.MODULE_TYPE,
                              carton.[STD_POWER_LEVEL],carton.RESV03,carton.CARTON_NO,
                              test.PMAX,test.TEMP,test.ISC,test.VOC,test.IMP,test.VMP,test.FF,test.EFF,test.TEST_DATE,
                              '{2}','{3}','{4}'
                              FROM 
                              {0}.[dbo].[T_MODULE_CARTON] carton with(nolock),
                              {0}.[dbo].[T_MODULE_PACKING_LIST] packinglist with(nolock),
                              {0}.[dbo].[T_MODULE] module with(nolock),
                              {0}.[dbo].[T_MODULE_TEST] test with(nolock)
                              where carton.SYSID = packinglist.MODULE_CARTON_SYSID
                              and packinglist.MODULE_SYSID= module.SYSID
                              and module.MODULE_SN = test.MODULE_SN
                              and carton.RESV03 in {5} ";

                sqlCenterDB = string.Format(sqlCenterDB, CenterDBName, ReworkOrder, DayTime, FormCover.currUserName, FormCover.CurrentFactory.Trim().ToUpper(), CartonNoString);

                //更新集中数据库
                string sqlCenter = @"UPDATE  {0}.[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] SET POST_ON=''
                                    WHERE MODULE_SN in (
                                    SELECT module.MODULE_SN
                                    FROM 
                                           {0}.[dbo].T_MODULE_CARTON carton with(nolock),
                                           {0}.[dbo].T_MODULE_PACKING_LIST list with(nolock),
                                           {0}.[dbo].T_MODULE module with(nolock),
                                           {0}.[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] template with(nolock)
                                    WHERE carton.SYSID = list.MODULE_CARTON_SYSID
                                            and list.MODULE_SYSID = module.SYSID 
                                            and carton.RESV03 IN {1}
                                            and module.MODULE_SN = template.MODULE_SN);
                                     DELETE
                                     FROM {0}.[dbo].T_MODULE_PACKING_LIST 
                                     FROM {0}.[dbo].T_MODULE_PACKING_LIST INNER JOIN {0}.[dbo].T_MODULE_CARTON MC ON MODULE_CARTON_SYSID=MC.SYSID
                                     WHERE MC.RESV03 IN {1};";
                sqlCenter = string.Format(sqlCenter, CenterDBName, CartonNoString);

                //删除集中数据库托号信息
                string DelCarton = @"DELETE FROM {0}.[dbo].T_MODULE_CARTON 
                                     WHERE RESV03 IN {1} ";
                DelCarton = string.Format(DelCarton, CenterDBName, CartonNoString);

                //更新接口数据库
                string sqlSap = @"UPDATE {0}.[dbo].[T_PICKING] SET ReworkFlag='1'
                                    WHERE OrderNo='{1}'
                                    AND CartonNo IN {2}
                                    AND PickingType IN ('261')";
                sqlSap = string.Format(sqlSap, SAPDBName, ReworkOrder, CartonNoString);

                #endregion

                #region 拼入库交易list
                DataTable dts = new DataTable();
                dts = ProductStorageDAL.ReworkStorageSucess(CartonNoString);
                if (dts.Rows.Count > 0 && dts != null)
                {
                    string job = dts.Rows[0]["JOBNO"].ToString();
                    string joinid = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");//交易ID
                    string posttime = DT.DateTime().LongDateTime;
                    foreach (var list in CartonNos)
                    {
                        DataRow[] rows = dts.Select("BOXID = '" + list + "'");
                        string cartonqty = "";
                        cartonqty = Convert.ToString(rows.Length);

                        #region
                        CARTONPACKINGTRANSACTION cartonpacking = new CARTONPACKINGTRANSACTION();
                        cartonpacking.SysID = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");
                        cartonpacking.Orgnization = "CS";
                        cartonpacking.Carton = list;
                        cartonpacking.Resv02 = Convert.ToString(rows[0]["CUSTBOXID"]);//新增加客户端号20140-3-24
                        cartonpacking.Resv01 = "CSAS";//新增厂别2014-03-24
                        cartonpacking.JobNo = job;
                        if (job.Equals(""))
                        {
                            cartonpacking.Action = "UnInvertory";
                        }
                        else
                        {
                            cartonpacking.Action = "UnLCL";
                        }
                        cartonpacking.ActionTxnID = joinid;
                        cartonpacking.ActionDate = posttime;
                        cartonpacking.ActionBy = FormCover.CurrUserName;
                        cartonpacking.ModuleQty = cartonqty;
                        cartonpacking.StdPowerLevel = Convert.ToString(rows[0]["StdPower"]);
                        cartonpacking.CreatedBy = "Packing_M" + FormCover.CurrentFactory.ToUpper().Trim().Substring(FormCover.CurrentFactory.ToUpper().Trim().Length - 2, 2);
                        _ListCartonPacking.Add(cartonpacking);
                        #endregion

                        if (rows.Length > 0)
                        {
                            for (int i = 0; i < rows.Length; i++)
                            {
                                #region
                                String modulesn = Convert.ToString(rows[i]["SN"]);
                                MODULEPACKINGTRANSACTION modulepacking = new MODULEPACKINGTRANSACTION();
                                modulepacking.SysID = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");
                                modulepacking.ModuleSn = modulesn;
                                modulepacking.CartonNoSysID = cartonpacking.SysID;
                                modulepacking.ProductNo = "";
                                modulepacking.ModuleType = Convert.ToString(rows[i]["ModelType"]);
                                modulepacking.ModuleStdPower = Convert.ToString(rows[i]["StdPower"]);
                                modulepacking.Pmax = Convert.ToString(rows[i]["Pmax"]);
                                modulepacking.ModuleStdPower = Convert.ToString(rows[i]["StdPower"]);
                                modulepacking.Workshop = FormCover.CurrentFactory.ToUpper().Trim();
                                modulepacking.Temp = Convert.ToString(rows[i]["TEMP"]);
                                modulepacking.VOC = Convert.ToString(rows[i]["VOC"]);
                                modulepacking.ISC = Convert.ToString(rows[i]["ISC"]);
                                modulepacking.IMP = Convert.ToString(rows[i]["IM"]);
                                modulepacking.VMP = Convert.ToString(rows[i]["VM"]);
                                modulepacking.FF = Convert.ToString(rows[i]["FF"]);
                                modulepacking.EFF = Convert.ToString(rows[i]["EFF"]);
                                modulepacking.TestDate = Convert.ToString(rows[i]["TESTTIME"]);
                                modulepacking.WorkOrder = Convert.ToString(rows[i]["WORKORDER"]);
                                modulepacking.CreatedBy = "Packing_M" + FormCover.CurrentFactory.ToUpper().Trim().Substring(FormCover.CurrentFactory.ToUpper().Trim().Length - 2, 2);
                                _ListModulePacking.Add(modulepacking);
                                #endregion

                            }
                        }
                    }
                }
                #endregion

                #region
                using (SqlConnection SqlConnection = new SqlConnection(CenterDBSQLConnetion))
                {
                    SqlConnection.Open();
                    using (SqlTransaction SqlTran = SqlConnection.BeginTransaction())
                    {
                        try
                        {
                            #region 更新集中数据库
                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTran, CommandType.Text, sqlCenterDB, null) < 1)
                            {
                                msg = "写入集中数据库数据失败:" + sqlCenterDB + "";
                                ToolsClass.Log(msg);
                                msg = "写入集中数据库数据失败";
                                SqlTran.Rollback();
                                return false;
                            }

                            if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTran, CommandType.Text, sqlCenter, null) < 1)
                            {
                                msg = "更新集中数据库数据失败:" + sqlCenter + "";
                                ToolsClass.Log(msg);
                                msg = "更新集中数据库数据失败";
                                SqlTran.Rollback();
                                return false;
                            }
                            #endregion

                            //如果包装和集中数据库，接口数据库做DB-LINK或者在一台服务器上
                            if (ProductStorageDAL.GetSysMapping("PackingSystemSameFlag", FormCover.CurrentFactory).Trim().ToUpper().Equals("Y"))
                            {
                                #region

                                #region 更新接口数据库
                                if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTran, CommandType.Text, sqlSap, null) < 1)
                                {
                                    msg = "更新接口据库数据失败:" + sqlSap + "";
                                    ToolsClass.Log(msg);
                                    msg = "更新接口据库数据失败";
                                    SqlTran.Rollback();
                                    return false;
                                }
                                #endregion
                                string currentFactoryName = FormCover.CurrentFactory.Trim().ToUpper();
                                if (currentFactoryName.Equals("M01") || currentFactoryName.Equals("M09"))
                                    currentFactoryName = "M01";
                                else if (currentFactoryName.Equals("M07") || currentFactoryName.Equals("M13"))
                                    currentFactoryName = "M07";

                                #region 更新包装数据库
                                for (int i = 0; i < CartonNos.Count; i++)
                                {
                                    string carton = CartonNos[i].Trim();

                                    string DBNAME = ProductStorageDAL.GetCartonFromCenterDB(carton);
                                    if (string.IsNullOrEmpty(DBNAME))
                                    {
                                        msg = "获取托号(" + carton + ")的原始入库车间错误";
                                        SqlTran.Rollback();
                                        return false;
                                    }

                                    if (DBNAME.Trim().Equals("M01") || DBNAME.Trim().Equals("M09"))
                                    {
                                        DBNAME = "M01";
                                    }
                                    else if (DBNAME.Trim().Equals("M07") || DBNAME.Trim().Equals("M13"))
                                    {
                                        DBNAME = "M07";
                                    }

                                    if (!DBNAME.Equals(currentFactoryName))
                                        continue;

                                    string sqlPacking = @"UPDATE {0}.[dbo].[Product] set BoxID='',[Cust_BoxID]='',[ReworkDate]= getdate() ,[Reworker]='{1}',Flag='4',ReWorkOrder='{2}',[Process]='T4'
                                     FROM {0}.[dbo].[Product] prd with(nolock),
                                          {0}.[dbo].[Box] box with(nolock) 
                                     where prd.BoxID = box.BoxID
                                     and box.BoxID = '{3}'
                                     and prd.Flag='3';
                                     update {0}.[dbo].Box set Flag = '4',IsUsed='L'
                                     where BoxID = '{3}' and Flag='3'";
                                    sqlPacking = string.Format(sqlPacking, DBName, FormCover.currUserName, ReworkOrder, CartonNos[i]);

                                    if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTran, CommandType.Text, sqlPacking, null) < 1)
                                    {
                                        msg = "更新包装数据库数据失败:原因托号（" + CartonNos[i] + "）没有在SAP做入库！";
                                        SqlTran.Rollback();
                                        return false;
                                    }
                                }
                                #endregion

                                if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTran, CommandType.Text, DelCarton, null) < 1)
                                {
                                    msg = "删除集中数据库托号信息失败";
                                    SqlTran.Rollback();
                                    return false;
                                }

                                if (!new CartonPackingTransactionDal().SaveStorage(SqlConnection, SqlTran, _ListCartonPacking, _ListModulePacking, out msg))
                                {
                                    // ToolsClass.Log("重工交易数据保存失败:" + msg + "", "ABNORMAL");
                                    SqlTran.Rollback();
                                    return false;
                                }

                                SqlTran.Commit();

                                #endregion
                            }
                            else
                            {
                                #region 更新接口数据库和包装数据库
                                using (SqlConnection SqlConnectionInterface = new SqlConnection(FormCover.InterfaceConnString))
                                {
                                    SqlConnectionInterface.Open();
                                    using (SqlTransaction SqlTranInterface = SqlConnectionInterface.BeginTransaction())
                                    {
                                        #region 更新接口数据库
                                        if (SqlHelper.ExecuteNonQuery(SqlConnectionInterface, SqlTranInterface, CommandType.Text, sqlSap, null) < 1)
                                        {
                                            msg = "更新接口据库数据失败:" + sqlSap + "";
                                            ToolsClass.Log(msg);
                                            msg = "更新接口据库数据失败";
                                            SqlTran.Rollback();
                                            SqlTranInterface.Rollback();
                                            return false;
                                        }
                                        #endregion

                                        using (SqlConnection SqlConnectionPacking = new SqlConnection(FormCover.connectionBase))
                                        {
                                            SqlConnectionPacking.Open();
                                            using (SqlTransaction SqlTranPacking = SqlConnectionPacking.BeginTransaction())
                                            {
                                                #region 更新包装数据库
                                                for (int i = 0; i < CartonNos.Count; i++)
                                                {
                                                    string carton = CartonNos[i].Trim();
                                                    string DBNAME = ProductStorageDAL.GetCartonFromCenterDB(carton);
                                                    if (string.IsNullOrEmpty(DBNAME))
                                                    {
                                                        msg = "获取托号(" + carton + ")的原始入库车间错误";
                                                        SqlTran.Rollback();
                                                        SqlTranInterface.Rollback();
                                                        return false;
                                                    }
                                                    if (!DBNAME.Equals(FormCover.CurrentFactory.Trim().ToUpper()))
                                                        continue;
                                                    else
                                                    {
                                                        if (DBNAME.Trim().Equals("M01") || DBNAME.Trim().Equals("M09"))
                                                        {
                                                            DBNAME = "M01";
                                                        }

                                                        if (DBNAME.Trim().Equals("M07") || DBNAME.Trim().Equals("M13"))
                                                        {
                                                            DBNAME = "M07";
                                                        }
                                                    }
                                                    string sqlPacking = @"UPDATE {0}.[dbo].[Product] set BoxID='',[Cust_BoxID]='',[ReworkDate]= getdate() ,[Reworker]='{1}',Flag='4',ReWorkOrder='{2}',[Process]='T4'
                                                                     FROM {0}.[dbo].[Product] prd with(nolock),
                                                                          {0}.[dbo].[Box] box with(nolock) 
                                                                     where prd.BoxID = box.BoxID
                                                                     and box.BoxID = '{3}'
                                                                     and prd.Flag='3';
                                                                     update {0}.[dbo].Box set Flag = '4',IsUsed='L'
                                                                     where BoxID = '{3}' and Flag='3'";
                                                    sqlPacking = string.Format(sqlPacking, DBName, FormCover.currUserName, ReworkOrder, CartonNos[i]);

                                                    if (SqlHelper.ExecuteNonQuery(SqlConnectionPacking, SqlTranPacking, CommandType.Text, sqlPacking, null) < 0)
                                                    {
                                                        msg = "更新包装数据库数据失败:" + sqlPacking + "";
                                                        ToolsClass.Log(msg);
                                                        msg = "更新包装数据库数据失败";
                                                        SqlTran.Rollback();
                                                        SqlTranInterface.Rollback();
                                                        SqlTranPacking.Rollback();
                                                        return false;
                                                    }
                                                }
                                                #endregion

                                                if (SqlHelper.ExecuteNonQuery(SqlConnection, SqlTran, CommandType.Text, DelCarton, null) < 1)
                                                {
                                                    msg = "删除集中数据库托号信息失败";
                                                    SqlTran.Rollback();
                                                    SqlTranInterface.Rollback();
                                                    SqlTranPacking.Rollback();
                                                    return false;
                                                }
                                                if (!new CartonPackingTransactionDal().SaveStorage(SqlConnection, SqlTran, _ListCartonPacking, _ListModulePacking, out msg))
                                                {
                                                    // ToolsClass.Log("重工交易数据保存失败:" + msg + "", "ABNORMAL");
                                                    SqlTran.Rollback();
                                                    return false;
                                                }
                                                SqlTran.Commit();
                                                SqlTranInterface.Commit();
                                                SqlTranPacking.Commit();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }


                        }
                        catch (Exception ex)
                        {
                            msg = "重工处理数据时发生异常:" + ex.Message + "";
                            SqlTran.Rollback();
                            return false;
                        }

                    }
                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                msg = "向接口数据库写数据发生异常：" + ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 重工
        /// </summary>
        /// <param name="orderNo">重工工单</param>
        /// <param name="moduleSns">组件序列号</param>
        /// <param name="msg">错误消息</param>
        /// <returns></returns>
        public static bool PackinSystemgRework(string orderNo, List<string> moduleSns, out string msg)
        {
            try
            {
                #region
                var moduleSnArray = "";
                if (moduleSns.Count > 0)
                {
                    moduleSnArray = moduleSns.Aggregate(moduleSnArray, (current, moduleSn) => current + ",'" + moduleSn + "'");
                    moduleSnArray = moduleSnArray.Substring(1, moduleSnArray.Length - 1);
                    moduleSnArray = "(" + moduleSnArray + ")";
                }
                else
                {
                    msg = "组件序列号为空";
                    return false;
                }
                //写入集中数据库重工数据
               
                var dayTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                var sqlCenterDb = @"INSERT INTO {0}.[dbo].[T_MODULE_REWORK]
                                       ([MODULE_SN],[BARCODE],[WORK_ORDER],[REWORK],[WORKSHOP],[MODULE_TYPE]
                                       ,[STD_POWER_LEVEL],[CARTON_NO],[CUST_CARTON_NO]
                                       ,[PMAX],[TEMP],[ISC],[VOC],[IMP],[VMP],[FF],[EFF],[TEST_DATE]
                                       ,[CREATED_ON],[CREATED_BY],[RESV1])
                              SELECT module.MODULE_SN,module.BARCODE,module.WORK_ORDER,'{1}',module.WORKSHOP,module.MODULE_TYPE,
                              carton.[STD_POWER_LEVEL],carton.RESV03,carton.CARTON_NO,
                              test.PMAX,test.TEMP,test.ISC,test.VOC,test.IMP,test.VMP,test.FF,test.EFF,test.TEST_DATE,
                              '{2}','{3}','{4}'
                              FROM 
                              {0}.[dbo].[T_MODULE_CARTON] carton with(nolock),
                              {0}.[dbo].[T_MODULE_PACKING_LIST] packinglist with(nolock),
                              {0}.[dbo].[T_MODULE] module with(nolock),
                              {0}.[dbo].[T_MODULE_TEST] test with(nolock)
                              where carton.SYSID = packinglist.MODULE_CARTON_SYSID
                              and packinglist.MODULE_SYSID= module.SYSID
                              and module.MODULE_SN = test.MODULE_SN
                              and module.MODULE_SN in {5} ";

                sqlCenterDb = string.Format(sqlCenterDb, CenterDBName, orderNo, dayTime, FormCover.currUserName,
                    FormCover.CurrentFactory.Trim().ToUpper(), moduleSnArray);

                //更新集中数据库
                var sqlCenter = @"UPDATE  {0}.[dbo].[T_PACK_MATERIAL_TEMPLATE_MODULE_LK] SET POST_ON=''
                                    WHERE MODULE_SN in {1};
                                     DELETE
                                     FROM {0}.[dbo].T_MODULE_PACKING_LIST 
                                     FROM {0}.[dbo].T_MODULE_PACKING_LIST 
                                            INNER JOIN {0}.[dbo].T_MODULE M ON 
                                        MODULE_SYSID=M.SYSID
                                     WHERE M.MODULE_SN in {1};";
                sqlCenter = string.Format(sqlCenter, CenterDBName, moduleSnArray);

                #endregion

                var listModulePacking = new List<MODULEPACKINGTRANSACTION>();
                var listCartonPacking = new List<CARTONPACKINGTRANSACTION>();
                #region 拼入库交易list
                var dts = ProductStorageDAL.ReworkStorageSucessByModuleSns(moduleSnArray);
                if (dts != null && dts.Rows != null && dts.Rows.Count > 0)
                {
                    var job = dts.Rows[0]["JOBNO"].ToString();
                    var joinid = Guid.NewGuid().ToString("N") + DateTime.Now.ToString("fff");//交易ID
                    var posttime = DT.DateTime().LongDateTime;
                    var cartonNos = new List<string>();
                    foreach (DataRow row in dts.Rows)
                    {
                        var cartonNo = row["BOXID"].ToString();
                        if (cartonNos.FindAll(p => p == cartonNo).Count > 0)
                            continue;
                        cartonNos.Add(cartonNo);
                    }
                    foreach (var cartonNo in cartonNos)
                    {
                        var rows = dts.Select("BOXID = '" + cartonNo + "'");
                        var cartonqty = Convert.ToString(rows.Length);

                        #region
                        var cartonpacking = new CARTONPACKINGTRANSACTION
                        {
                            SysID = Guid.NewGuid().ToString("N") + DateTime.Now.ToString("fff"),
                            Orgnization = "CS",
                            Carton = cartonNo,
                            Resv02 = Convert.ToString(rows[0]["CUSTBOXID"]), //新增加客户端号20140-3-24
                            Resv01 = "CSAS", //新增厂别2014-03-24
                            JobNo = job,
                            Action = job.Equals("") ? "UnInvertory" : "UnLCL",
                            ActionTxnID = joinid,
                            ActionDate = posttime,
                            ActionBy = FormCover.CurrUserName,
                            ModuleQty = cartonqty,
                            StdPowerLevel = Convert.ToString(rows[0]["StdPower"]),
                            CreatedBy =
                                "Packing_M" +
                                FormCover.CurrentFactory.ToUpper()
                                    .Trim()
                                    .Substring(FormCover.CurrentFactory.ToUpper().Trim().Length - 2, 2)
                        };
                        listCartonPacking.Add(cartonpacking);
                        #endregion

                        if (rows.Length <= 0)
                            continue;
                        foreach (var t in rows)
                        {
                            #region
                            var modulesn = Convert.ToString(t["SN"]);
                            var modulepacking = new MODULEPACKINGTRANSACTION
                            {
                                SysID = Guid.NewGuid().ToString("N") + DateTime.Now.ToString("fff"),
                                ModuleSn = modulesn,
                                CartonNoSysID = cartonpacking.SysID,
                                ProductNo = "",
                                ModuleType = Convert.ToString(t["ModelType"]),
                                ModuleStdPower = Convert.ToString(t["StdPower"]),
                                Pmax = Convert.ToString(t["Pmax"])
                            };
                            modulepacking.ModuleStdPower = Convert.ToString(t["StdPower"]);
                            modulepacking.Workshop = FormCover.CurrentFactory.ToUpper().Trim();
                            modulepacking.Temp = Convert.ToString(t["TEMP"]);
                            modulepacking.VOC = Convert.ToString(t["VOC"]);
                            modulepacking.ISC = Convert.ToString(t["ISC"]);
                            modulepacking.IMP = Convert.ToString(t["IM"]);
                            modulepacking.VMP = Convert.ToString(t["VM"]);
                            modulepacking.FF = Convert.ToString(t["FF"]);
                            modulepacking.EFF = Convert.ToString(t["EFF"]);
                            modulepacking.TestDate = Convert.ToString(t["TESTTIME"]);
                            modulepacking.WorkOrder = Convert.ToString(t["WORKORDER"]);
                            modulepacking.CreatedBy = "Packing_M" + FormCover.CurrentFactory.ToUpper().Trim().Substring(FormCover.CurrentFactory.ToUpper().Trim().Length - 2, 2);
                            listModulePacking.Add(modulepacking);
                            #endregion
                        }
                    }
                }
                #endregion

                #region
                using (var sqlConnection = new SqlConnection(CenterDBSQLConnetion))
                {
                    sqlConnection.Open();
                    using (var sqlConnectionPacking = new SqlConnection(FormCover.connectionBase))
                                {
                                    sqlConnectionPacking.Open();
                                    using (var sqlTran = sqlConnection.BeginTransaction())
                                    {
                                        try
                                        {
                                            #region 更新集中数据库
                                            if (SqlHelper.ExecuteNonQuery(sqlConnection, sqlTran, CommandType.Text, sqlCenterDb, null) < 1)
                                            {
                                            
                                            	msg = "写入集中数据库数据失败:" + sqlCenterDb + "";
                                                ToolsClass.Log(msg);
                                                msg = "写入集中数据库数据失败";
                                                sqlTran.Rollback();
                                                return false;
                                            }

                                            if (SqlHelper.ExecuteNonQuery(sqlConnection, sqlTran, CommandType.Text, sqlCenter, null) < 1)
                                            {
                                            	
                                            	msg = "更新集中数据库数据失败:" + sqlCenter + "";
                                                ToolsClass.Log(msg);
                                                msg = "更新集中数据库数据失败";
                                                sqlTran.Rollback();
                                                return false;
                                            }
                                            #endregion

                                            //如果包装和集中数据库，接口数据库做DB-LINK或者在一台服务器上
                                            if (ProductStorageDAL.GetSysMapping("PackingSystemSameFlag", FormCover.CurrentFactory).Trim().ToUpper().Equals("Y"))
                                            {
                                                #region
	
                                                var currentFactoryName = FormCover.CurrentFactory.Trim().ToUpper();
                                                if (currentFactoryName.Equals("M01") || currentFactoryName.Equals("M09"))
                                                    currentFactoryName = "M01";
                                                else if (currentFactoryName.Equals("M07") || currentFactoryName.Equals("M13"))
                                                    currentFactoryName = "M07";

                                                #region 更新包装数据库

                                                using (var sqlTranPacking = sqlConnectionPacking.BeginTransaction())
                                                {
                                                	
                                                    foreach (var moduleSn in moduleSns)
                                                    {
                                                        
                                                    	var sqlPacking = @"UPDATE {0}.[dbo].[Product] set BoxID='',[Cust_BoxID]='',[ReworkDate]= getdate() ,[Reworker]='{1}',Flag='4',ReWorkOrder='{2}',[Process]='T4'
                                     FROM {0}.[dbo].[Product] prd with(nolock) 
                                     where prd.SN = '{3}';";
                                                  
                                                        sqlPacking = string.Format(sqlPacking, DBName, FormCover.currUserName, orderNo, moduleSn);

                                                        SqlHelper.ExecuteNonQuery(sqlConnectionPacking, sqlTranPacking, CommandType.Text, sqlPacking, null);

                                                        DataAccess.InsertRegModuleRework(
                                                            new RegModuleRework
                                                            {
                                                                CreatedBy = FormCover.currUserName,
                                                                ModuleSn = moduleSn,
                                                                WorkOrder = orderNo,
                                                                Sysid = Guid.NewGuid().ToString("N")
                                                            }, sqlConnectionPacking, sqlTranPacking);
                                                    }
                                                #endregion

                                                    if (!new CartonPackingTransactionDal().SaveStorage(sqlConnection, sqlTran, listCartonPacking, listModulePacking, out msg))
                                                    {
                                                        // ToolsClass.Log("重工交易数据保存失败:" + msg + "", "ABNORMAL");
                                                        sqlTran.Rollback();
                                                        return false;
                                                    }
                                                    sqlTran.Commit();
                                                    sqlTranPacking.Commit();
                                                }
                                            }
                                                #endregion
                                            else
                                            {
                                                #region 更新接口数据库和包装数据库
                                                using (var sqlConnectionInterface = new SqlConnection(FormCover.InterfaceConnString))
                                                {
                                                    sqlConnectionInterface.Open();
                                                    using (var sqlTranInterface = sqlConnectionInterface.BeginTransaction())
                                                    {
                                                            using (var sqlTranPacking = sqlConnectionPacking.BeginTransaction())
                                                            {
                                                                #region 更新包装数据库
                                                               
                                                                foreach (var moduleSn in moduleSns)
                                                                {
                                                                    var sqlPacking = @"
UPDATE {0}.[dbo].[Product] set BoxID='',[Cust_BoxID]='',[ReworkDate]= getdate() ,[Reworker]='{1}',Flag='4',ReWorkOrder='{2}',[Process]='T4'
                                     FROM {0}.[dbo].[Product] prd with(nolock) 
                                     where prd.SN = '{3}';";
                                                                    sqlPacking = string.Format(sqlPacking, DBName, FormCover.currUserName, orderNo, moduleSn);

                                                                    SqlHelper.ExecuteNonQuery(sqlConnectionPacking, sqlTranPacking,
                                                                        CommandType.Text, sqlPacking, null);

                                                                    DataAccess.InsertRegModuleRework(
                                                                        new RegModuleRework
                                                                        {
                                                                            CreatedBy = FormCover.currUserName,
                                                                            ModuleSn = moduleSn,
                                                                            WorkOrder = orderNo,
                                                                            Sysid = Guid.NewGuid().ToString("N")
                                                                        }, sqlConnectionPacking, sqlTranPacking);
                                                                }
                                                                #endregion

                                                                if (!new CartonPackingTransactionDal().SaveStorage(sqlConnection, sqlTran, listCartonPacking, listModulePacking, out msg))
                                                                {
                                                                    // ToolsClass.Log("重工交易数据保存失败:" + msg + "", "ABNORMAL");
                                                                    sqlTran.Rollback();
                                                                    return false;
                                                                }
                                                                sqlTran.Commit();
                                                                sqlTranInterface.Commit();
                                                                sqlTranPacking.Commit();
                                                            }
                                                    }
                                                }
                                                #endregion
                                            }


                                        }
                                        catch (Exception ex)
                                        {
                                            msg = "重工处理数据时发生异常:" + ex.Message + "";
                                            sqlTran.Rollback();
                                            return false;
                                        }
                                    }
                    }
                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                msg = "向接口数据库写数据发生异常：" + ex.Message;
                return false;
            }
        }
    }
}
