﻿namespace CSICPR
{
    partial class FormCover
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.tbName = new System.Windows.Forms.TextBox();
        	this.label4 = new System.Windows.Forms.Label();
        	this.label2 = new System.Windows.Forms.Label();
        	this.tbPWD = new System.Windows.Forms.TextBox();
        	this.btLogin = new System.Windows.Forms.Button();
        	this.btQuit = new System.Windows.Forms.Button();
        	this.btSetUp = new System.Windows.Forms.Button();
        	this.label3 = new System.Windows.Forms.Label();
        	this.lblPlantCode = new System.Windows.Forms.Label();
        	this.ddlPlantCode = new System.Windows.Forms.ComboBox();
        	this.comboLangua = new System.Windows.Forms.ComboBox();
        	this.lblangua = new System.Windows.Forms.Label();
        	this.SuspendLayout();
        	// 
        	// tbName
        	// 
        	this.tbName.Location = new System.Drawing.Point(186, 67);
        	this.tbName.Name = "tbName";
        	this.tbName.Size = new System.Drawing.Size(108, 20);
        	this.tbName.TabIndex = 1;
        	// 
        	// label4
        	// 
        	this.label4.AutoSize = true;
        	this.label4.Location = new System.Drawing.Point(21, 96);
        	this.label4.Name = "label4";
        	this.label4.Size = new System.Drawing.Size(0, 13);
        	this.label4.TabIndex = 10;
        	// 
        	// label2
        	// 
        	this.label2.AutoSize = true;
        	this.label2.BackColor = System.Drawing.Color.White;
        	this.label2.ForeColor = System.Drawing.SystemColors.Desktop;
        	this.label2.Location = new System.Drawing.Point(92, 103);
        	this.label2.Name = "label2";
        	this.label2.Size = new System.Drawing.Size(34, 13);
        	this.label2.TabIndex = 8;
        	this.label2.Text = "密码:";
        	// 
        	// tbPWD
        	// 
        	this.tbPWD.Location = new System.Drawing.Point(186, 100);
        	this.tbPWD.Name = "tbPWD";
        	this.tbPWD.Size = new System.Drawing.Size(108, 20);
        	this.tbPWD.TabIndex = 2;
        	this.tbPWD.UseSystemPasswordChar = true;
        	// 
        	// btLogin
        	// 
        	this.btLogin.Location = new System.Drawing.Point(243, 213);
        	this.btLogin.Name = "btLogin";
        	this.btLogin.Size = new System.Drawing.Size(68, 30);
        	this.btLogin.TabIndex = 3;
        	this.btLogin.Text = "登录";
        	this.btLogin.UseVisualStyleBackColor = true;
        	this.btLogin.Click += new System.EventHandler(this.btLogin_Click);
        	// 
        	// btQuit
        	// 
        	this.btQuit.Location = new System.Drawing.Point(132, 213);
        	this.btQuit.Name = "btQuit";
        	this.btQuit.Size = new System.Drawing.Size(68, 30);
        	this.btQuit.TabIndex = 5;
        	this.btQuit.Text = "退出";
        	this.btQuit.UseVisualStyleBackColor = true;
        	this.btQuit.Click += new System.EventHandler(this.BtQuitClick);
        	// 
        	// btSetUp
        	// 
        	this.btSetUp.Location = new System.Drawing.Point(23, 213);
        	this.btSetUp.Name = "btSetUp";
        	this.btSetUp.Size = new System.Drawing.Size(68, 30);
        	this.btSetUp.TabIndex = 11;
        	this.btSetUp.Text = "设置";
        	this.btSetUp.UseVisualStyleBackColor = true;
        	// 
        	// label3
        	// 
        	this.label3.AutoSize = true;
        	this.label3.BackColor = System.Drawing.Color.White;
        	this.label3.ForeColor = System.Drawing.SystemColors.Desktop;
        	this.label3.Location = new System.Drawing.Point(92, 70);
        	this.label3.Name = "label3";
        	this.label3.Size = new System.Drawing.Size(34, 13);
        	this.label3.TabIndex = 7;
        	this.label3.Text = "账号:";
        	// 
        	// lblPlantCode
        	// 
        	this.lblPlantCode.AutoSize = true;
        	this.lblPlantCode.BackColor = System.Drawing.Color.White;
        	this.lblPlantCode.ForeColor = System.Drawing.SystemColors.Desktop;
        	this.lblPlantCode.Location = new System.Drawing.Point(92, 134);
        	this.lblPlantCode.Name = "lblPlantCode";
        	this.lblPlantCode.Size = new System.Drawing.Size(34, 13);
        	this.lblPlantCode.TabIndex = 12;
        	this.lblPlantCode.Text = "车间:";
        	// 
        	// ddlPlantCode
        	// 
        	this.ddlPlantCode.BackColor = System.Drawing.SystemColors.InactiveBorder;
        	this.ddlPlantCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.ddlPlantCode.FormattingEnabled = true;
        	this.ddlPlantCode.Location = new System.Drawing.Point(186, 134);
        	this.ddlPlantCode.Name = "ddlPlantCode";
        	this.ddlPlantCode.Size = new System.Drawing.Size(108, 21);
        	this.ddlPlantCode.TabIndex = 204;
        	// 
        	// comboLangua
        	// 
        	this.comboLangua.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.comboLangua.FormattingEnabled = true;
        	this.comboLangua.Items.AddRange(new object[] {
			"CH",
			"EN",
			"AI"});
        	this.comboLangua.Location = new System.Drawing.Point(186, 161);
        	this.comboLangua.Name = "comboLangua";
        	this.comboLangua.Size = new System.Drawing.Size(108, 21);
        	this.comboLangua.TabIndex = 206;
        	// 
        	// lblangua
        	// 
        	this.lblangua.AutoSize = true;
        	this.lblangua.BackColor = System.Drawing.Color.White;
        	this.lblangua.ForeColor = System.Drawing.SystemColors.Desktop;
        	this.lblangua.Location = new System.Drawing.Point(92, 164);
        	this.lblangua.Name = "lblangua";
        	this.lblangua.Size = new System.Drawing.Size(34, 13);
        	this.lblangua.TabIndex = 205;
        	this.lblangua.Text = "语言:";
        	// 
        	// FormCover
        	// 
        	this.AcceptButton = this.btLogin;
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.BackgroundImage = global::CSICPR.Properties.Resources.loginBG;
        	this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
        	this.ClientSize = new System.Drawing.Size(330, 261);
        	this.Controls.Add(this.comboLangua);
        	this.Controls.Add(this.lblangua);
        	this.Controls.Add(this.ddlPlantCode);
        	this.Controls.Add(this.lblPlantCode);
        	this.Controls.Add(this.btSetUp);
        	this.Controls.Add(this.label4);
        	this.Controls.Add(this.tbPWD);
        	this.Controls.Add(this.tbName);
        	this.Controls.Add(this.btQuit);
        	this.Controls.Add(this.btLogin);
        	this.Controls.Add(this.label3);
        	this.Controls.Add(this.label2);
        	this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
        	this.Name = "FormCover";
        	this.ShowInTaskbar = false;
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "User Logon";
        	this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCover_FormClosing);
        	this.Load += new System.EventHandler(this.FormCover_Load);
        	this.ResumeLayout(false);
        	this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbPWD;
        private System.Windows.Forms.Button btLogin;
        private System.Windows.Forms.Button btQuit;
        private System.Windows.Forms.Button btSetUp;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblPlantCode;
        private System.Windows.Forms.ComboBox ddlPlantCode;
        private System.Windows.Forms.ComboBox comboLangua;
        private System.Windows.Forms.Label lblangua;


    }
}