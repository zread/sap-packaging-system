﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSICPR
{
    public class ServerDateTime
    {
        public DateTime DateTime { get; set; }
        //yyyy-MM-dd HH:mm:ss.fff
        /// <summary>
        /// yyyy-MM-dd HH:mm:ss.fff
        /// </summary>
        public string LongDateTime
        {
            get
            {
                return DateTime.ToString("yyyy-MM-dd HH:mm:ss.fff");
            }
        }
        //yyyy-MM-dd HH:mm:ss
        /// <summary>
        /// yyyy-MM-dd HH:mm:ss
        /// </summary>
        public string LongDateTime2
        {
            get
            {
                return DateTime.ToString("yyyy-MM-dd HH:mm:ss");
            }
        }
        //yyyy-MM-dd
        /// <summary>
        /// yyyy-MM-dd
        /// </summary>
        public string LongDate
        {
            get
            {
                return DateTime.ToString("yyyy-MM-dd");
            }
        }
        //HH:mm:ss.fff
        /// <summary>
        /// HH:mm:ss.fff
        /// </summary>
        public string LongTime
        {
            get
            {
                return DateTime.ToString("HH:mm:ss.fff");
            }
        }
        //HH:mm:ss
        /// <summary>
        /// HH:mm:ss
        /// </summary>
        public string LongTime2
        {
            get
            {
                return DateTime.ToString("HH:mm:ss");
            }
        }
        //yyyyMMddHHmmssfff
        /// <summary>
        /// yyyyMMddHHmmssfff
        /// </summary>
        public string ShortDateTime
        {
            get
            {
                return DateTime.ToString("yyyyMMddHHmmssfff");
            }
        }
        //yyyyMMdd
        /// <summary>
        /// yyyyMMdd
        /// </summary>
        public string ShortDate
        {
            get
            {
                return DateTime.ToString("yyyyMMdd");
            }
        }
        //HHmmssfff
        /// <summary>
        /// HHmmssfff
        /// </summary>
        public string ShortTime
        {
            get
            {
                return DateTime.ToString("HHmmssfff");
            }
        }
        //yyyyMMddHHmmss
        /// <summary>
        /// yyyyMMddHHmmss
        /// </summary>
        public string ShortDateTime2
        {
            get
            {
                return DateTime.ToString("yyyyMMddHHmmss");
            }
        }
        //yyMMdd
        /// <summary>
        /// yyMMdd
        /// </summary>
        public string ShortDate2
        {
            get
            {
                return DateTime.ToString("yyMMdd");
            }
        }
        //HHmmss
        /// <summary>
        /// HHmmss
        /// </summary>
        public string ShortTime2
        {
            get
            {
                return DateTime.ToString("HHmmss");
            }
        }
        //5位日期号（年2位+月1位 10、11、12月使用字母A\B\C对应）
        /// <summary>
        /// 5位日期号（年2位+月1位 10、11、12月使用字母A\B\C对应）
        /// </summary>
        public string ShortDate3
        {
            get
            {
                var year = DateTime.ToString("yy");
                var month = string.Empty;
                switch (DateTime.Month)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                        month = DateTime.Month.ToString();
                        break;
                    case 10:
                        month = "A";
                        break;
                    case 11:
                        month = "B";
                        break;
                    case 12:
                        month = "C";
                        break;
                }
                var day = DateTime.ToString("dd");
                return year + month + day;
            }
        }
    }
}
