﻿namespace CSICPR
{
    partial class FrmRegReworkModule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRegReworkModule));
        	this.pnlTop = new System.Windows.Forms.Panel();
        	this.lblModuleCntValue = new System.Windows.Forms.Label();
        	this.lblModuleCnt = new System.Windows.Forms.Label();
        	this.btnReset = new System.Windows.Forms.Button();
        	this.btnSubmit = new System.Windows.Forms.Button();
        	this.btnQuery = new System.Windows.Forms.Button();
        	this.pnlType = new System.Windows.Forms.Panel();
        	this.cmbType = new System.Windows.Forms.ComboBox();
        	this.lblType = new System.Windows.Forms.Label();
        	this.pnlOrderNo = new System.Windows.Forms.Panel();
        	this.PicBoxWo = new System.Windows.Forms.PictureBox();
        	this.txtWo = new System.Windows.Forms.TextBox();
        	this.lblReworkOrderNo = new System.Windows.Forms.Label();
        	this.pnlModuleSns = new System.Windows.Forms.Panel();
        	this.lblModuleSns = new System.Windows.Forms.Label();
        	this.txtModuleSns = new System.Windows.Forms.TextBox();
        	this.pnlCartons = new System.Windows.Forms.Panel();
        	this.txtCartons = new System.Windows.Forms.TextBox();
        	this.lblCartonNo = new System.Windows.Forms.Label();
        	this.dgvData = new System.Windows.Forms.DataGridView();
        	this.lstView = new System.Windows.Forms.ListView();
        	this.colClearByModuleSn = new System.Windows.Forms.DataGridViewLinkColumn();
        	this.colClearByCartonNo = new System.Windows.Forms.DataGridViewLinkColumn();
        	this.colModuleSn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colJobNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.colCartonNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.pnlTop.SuspendLayout();
        	this.pnlType.SuspendLayout();
        	this.pnlOrderNo.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.PicBoxWo)).BeginInit();
        	this.pnlModuleSns.SuspendLayout();
        	this.pnlCartons.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
        	this.SuspendLayout();
        	// 
        	// pnlTop
        	// 
        	this.pnlTop.Controls.Add(this.lblModuleCntValue);
        	this.pnlTop.Controls.Add(this.lblModuleCnt);
        	this.pnlTop.Controls.Add(this.btnReset);
        	this.pnlTop.Controls.Add(this.btnSubmit);
        	this.pnlTop.Controls.Add(this.btnQuery);
        	this.pnlTop.Controls.Add(this.pnlType);
        	this.pnlTop.Controls.Add(this.pnlOrderNo);
        	this.pnlTop.Controls.Add(this.pnlModuleSns);
        	this.pnlTop.Controls.Add(this.pnlCartons);
        	this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
        	this.pnlTop.Location = new System.Drawing.Point(3, 3);
        	this.pnlTop.Name = "pnlTop";
        	this.pnlTop.Size = new System.Drawing.Size(933, 206);
        	this.pnlTop.TabIndex = 0;
        	// 
        	// lblModuleCntValue
        	// 
        	this.lblModuleCntValue.AutoSize = true;
        	this.lblModuleCntValue.ForeColor = System.Drawing.Color.Black;
        	this.lblModuleCntValue.Location = new System.Drawing.Point(674, 177);
        	this.lblModuleCntValue.Name = "lblModuleCntValue";
        	this.lblModuleCntValue.Size = new System.Drawing.Size(13, 13);
        	this.lblModuleCntValue.TabIndex = 56;
        	this.lblModuleCntValue.Text = "0";
        	// 
        	// lblModuleCnt
        	// 
        	this.lblModuleCnt.AutoSize = true;
        	this.lblModuleCnt.Location = new System.Drawing.Point(578, 177);
        	this.lblModuleCnt.Name = "lblModuleCnt";
        	this.lblModuleCnt.Size = new System.Drawing.Size(85, 13);
        	this.lblModuleCnt.TabIndex = 55;
        	this.lblModuleCnt.Text = "组件数量汇总：";
        	// 
        	// btnReset
        	// 
        	this.btnReset.Location = new System.Drawing.Point(482, 172);
        	this.btnReset.Name = "btnReset";
        	this.btnReset.Size = new System.Drawing.Size(68, 24);
        	this.btnReset.TabIndex = 53;
        	this.btnReset.Text = "重置";
        	this.btnReset.UseVisualStyleBackColor = true;
        	this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
        	// 
        	// btnSubmit
        	// 
        	this.btnSubmit.Location = new System.Drawing.Point(774, 172);
        	this.btnSubmit.Name = "btnSubmit";
        	this.btnSubmit.Size = new System.Drawing.Size(73, 24);
        	this.btnSubmit.TabIndex = 52;
        	this.btnSubmit.Text = "提  交";
        	this.btnSubmit.UseVisualStyleBackColor = true;
        	this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
        	// 
        	// btnQuery
        	// 
        	this.btnQuery.Location = new System.Drawing.Point(379, 172);
        	this.btnQuery.Name = "btnQuery";
        	this.btnQuery.Size = new System.Drawing.Size(97, 24);
        	this.btnQuery.TabIndex = 2;
        	this.btnQuery.Text = "获取组件数据";
        	this.btnQuery.UseVisualStyleBackColor = true;
        	this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
        	// 
        	// pnlType
        	// 
        	this.pnlType.Controls.Add(this.cmbType);
        	this.pnlType.Controls.Add(this.lblType);
        	this.pnlType.Location = new System.Drawing.Point(21, 46);
        	this.pnlType.Name = "pnlType";
        	this.pnlType.Size = new System.Drawing.Size(334, 34);
        	this.pnlType.TabIndex = 49;
        	// 
        	// cmbType
        	// 
        	this.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.cmbType.FormattingEnabled = true;
        	this.cmbType.Location = new System.Drawing.Point(143, 5);
        	this.cmbType.Name = "cmbType";
        	this.cmbType.Size = new System.Drawing.Size(176, 21);
        	this.cmbType.TabIndex = 7;
        	this.cmbType.SelectedIndexChanged += new System.EventHandler(this.cmbType_SelectedIndexChanged);
        	// 
        	// lblType
        	// 
        	this.lblType.AutoSize = true;
        	this.lblType.Location = new System.Drawing.Point(3, 9);
        	this.lblType.Name = "lblType";
        	this.lblType.Size = new System.Drawing.Size(64, 13);
        	this.lblType.TabIndex = 6;
        	this.lblType.Text = "注 册 方 式";
        	// 
        	// pnlOrderNo
        	// 
        	this.pnlOrderNo.Controls.Add(this.PicBoxWo);
        	this.pnlOrderNo.Controls.Add(this.txtWo);
        	this.pnlOrderNo.Controls.Add(this.lblReworkOrderNo);
        	this.pnlOrderNo.Location = new System.Drawing.Point(21, 11);
        	this.pnlOrderNo.Name = "pnlOrderNo";
        	this.pnlOrderNo.Size = new System.Drawing.Size(286, 34);
        	this.pnlOrderNo.TabIndex = 49;
        	// 
        	// PicBoxWo
        	// 
        	this.PicBoxWo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBoxWo.BackgroundImage")));
        	this.PicBoxWo.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicBoxWo.InitialImage")));
        	this.PicBoxWo.Location = new System.Drawing.Point(247, 6);
        	this.PicBoxWo.Name = "PicBoxWo";
        	this.PicBoxWo.Size = new System.Drawing.Size(27, 20);
        	this.PicBoxWo.TabIndex = 177;
        	this.PicBoxWo.TabStop = false;
        	this.PicBoxWo.Click += new System.EventHandler(this.PicBoxWo_Click);
        	// 
        	// txtWo
        	// 
        	this.txtWo.Location = new System.Drawing.Point(98, 5);
        	this.txtWo.Name = "txtWo";
        	this.txtWo.ReadOnly = true;
        	this.txtWo.Size = new System.Drawing.Size(148, 20);
        	this.txtWo.TabIndex = 176;
        	this.txtWo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtWo_KeyDown);
        	// 
        	// lblReworkOrderNo
        	// 
        	this.lblReworkOrderNo.AutoSize = true;
        	this.lblReworkOrderNo.Location = new System.Drawing.Point(9, 8);
        	this.lblReworkOrderNo.Name = "lblReworkOrderNo";
        	this.lblReworkOrderNo.Size = new System.Drawing.Size(67, 13);
        	this.lblReworkOrderNo.TabIndex = 6;
        	this.lblReworkOrderNo.Text = "重工工单号";
        	// 
        	// pnlModuleSns
        	// 
        	this.pnlModuleSns.Controls.Add(this.lblModuleSns);
        	this.pnlModuleSns.Controls.Add(this.txtModuleSns);
        	this.pnlModuleSns.Location = new System.Drawing.Point(21, 81);
        	this.pnlModuleSns.Name = "pnlModuleSns";
        	this.pnlModuleSns.Size = new System.Drawing.Size(334, 118);
        	this.pnlModuleSns.TabIndex = 49;
        	this.pnlModuleSns.Visible = false;
        	// 
        	// lblModuleSns
        	// 
        	this.lblModuleSns.AutoSize = true;
        	this.lblModuleSns.Location = new System.Drawing.Point(9, 8);
        	this.lblModuleSns.Name = "lblModuleSns";
        	this.lblModuleSns.Size = new System.Drawing.Size(67, 13);
        	this.lblModuleSns.TabIndex = 6;
        	this.lblModuleSns.Text = "组件序列号";
        	// 
        	// txtModuleSns
        	// 
        	this.txtModuleSns.AcceptsReturn = true;
        	this.txtModuleSns.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.txtModuleSns.ImeMode = System.Windows.Forms.ImeMode.NoControl;
        	this.txtModuleSns.Location = new System.Drawing.Point(143, 3);
        	this.txtModuleSns.MaxLength = 0;
        	this.txtModuleSns.Multiline = true;
        	this.txtModuleSns.Name = "txtModuleSns";
        	this.txtModuleSns.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        	this.txtModuleSns.Size = new System.Drawing.Size(168, 106);
        	this.txtModuleSns.TabIndex = 52;
        	// 
        	// pnlCartons
        	// 
        	this.pnlCartons.Controls.Add(this.txtCartons);
        	this.pnlCartons.Controls.Add(this.lblCartonNo);
        	this.pnlCartons.Location = new System.Drawing.Point(21, 81);
        	this.pnlCartons.Name = "pnlCartons";
        	this.pnlCartons.Size = new System.Drawing.Size(334, 118);
        	this.pnlCartons.TabIndex = 49;
        	// 
        	// txtCartons
        	// 
        	this.txtCartons.AcceptsReturn = true;
        	this.txtCartons.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.txtCartons.ImeMode = System.Windows.Forms.ImeMode.NoControl;
        	this.txtCartons.Location = new System.Drawing.Point(163, 3);
        	this.txtCartons.MaxLength = 0;
        	this.txtCartons.Multiline = true;
        	this.txtCartons.Name = "txtCartons";
        	this.txtCartons.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        	this.txtCartons.Size = new System.Drawing.Size(168, 106);
        	this.txtCartons.TabIndex = 52;
        	// 
        	// lblCartonNo
        	// 
        	this.lblCartonNo.AutoSize = true;
        	this.lblCartonNo.Location = new System.Drawing.Point(3, 8);
        	this.lblCartonNo.Name = "lblCartonNo";
        	this.lblCartonNo.Size = new System.Drawing.Size(64, 13);
        	this.lblCartonNo.TabIndex = 6;
        	this.lblCartonNo.Text = "内 部 托 号";
        	// 
        	// dgvData
        	// 
        	this.dgvData.AllowUserToAddRows = false;
        	this.dgvData.AllowUserToDeleteRows = false;
        	this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        	this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
        	        	        	this.colClearByModuleSn,
        	        	        	this.colClearByCartonNo,
        	        	        	this.colModuleSn,
        	        	        	this.colJobNo,
        	        	        	this.colCartonNo});
        	this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.dgvData.Location = new System.Drawing.Point(3, 209);
        	this.dgvData.Name = "dgvData";
        	this.dgvData.ReadOnly = true;
        	this.dgvData.RowTemplate.Height = 23;
        	this.dgvData.Size = new System.Drawing.Size(933, 341);
        	this.dgvData.TabIndex = 1;
        	this.dgvData.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellContentClick);
        	// 
        	// lstView
        	// 
        	this.lstView.BorderStyle = System.Windows.Forms.BorderStyle.None;
        	this.lstView.Dock = System.Windows.Forms.DockStyle.Bottom;
        	this.lstView.FullRowSelect = true;
        	this.lstView.Location = new System.Drawing.Point(3, 550);
        	this.lstView.MultiSelect = false;
        	this.lstView.Name = "lstView";
        	this.lstView.Size = new System.Drawing.Size(933, 80);
        	this.lstView.TabIndex = 3;
        	this.lstView.UseCompatibleStateImageBehavior = false;
        	this.lstView.View = System.Windows.Forms.View.Details;
        	this.lstView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstView_KeyDown);
        	// 
        	// colClearByModuleSn
        	// 
        	this.colClearByModuleSn.HeaderText = "";
        	this.colClearByModuleSn.Name = "colClearByModuleSn";
        	this.colClearByModuleSn.ReadOnly = true;
        	this.colClearByModuleSn.Text = "Remove Module";
        	this.colClearByModuleSn.UseColumnTextForLinkValue = true;
        	this.colClearByModuleSn.Width = 90;
        	// 
        	// colClearByCartonNo
        	// 
        	this.colClearByCartonNo.HeaderText = "";
        	this.colClearByCartonNo.Name = "colClearByCartonNo";
        	this.colClearByCartonNo.ReadOnly = true;
        	this.colClearByCartonNo.Text = "Remove Carton";
        	this.colClearByCartonNo.UseColumnTextForLinkValue = true;
        	this.colClearByCartonNo.Width = 90;
        	// 
        	// colModuleSn
        	// 
        	this.colModuleSn.DataPropertyName = "ModuleSn";
        	this.colModuleSn.HeaderText = "组件序列号";
        	this.colModuleSn.Name = "colModuleSn";
        	this.colModuleSn.ReadOnly = true;
        	this.colModuleSn.Width = 150;
        	// 
        	// colJobNo
        	// 
        	this.colJobNo.DataPropertyName = "JobNo";
        	this.colJobNo.HeaderText = "出货编号";
        	this.colJobNo.Name = "colJobNo";
        	this.colJobNo.ReadOnly = true;
        	this.colJobNo.Width = 150;
        	// 
        	// colCartonNo
        	// 
        	this.colCartonNo.DataPropertyName = "CartonNo";
        	this.colCartonNo.HeaderText = "内部托号";
        	this.colCartonNo.Name = "colCartonNo";
        	this.colCartonNo.ReadOnly = true;
        	this.colCartonNo.Width = 150;
        	// 
        	// FrmRegReworkModule
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(939, 633);
        	this.Controls.Add(this.dgvData);
        	this.Controls.Add(this.pnlTop);
        	this.Controls.Add(this.lstView);
        	this.Name = "FrmRegReworkModule";
        	this.Padding = new System.Windows.Forms.Padding(3);
        	this.ShowInTaskbar = false;
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "组件重工注册";
        	this.Load += new System.EventHandler(this.FrmRegReworkModule_Load);
        	this.pnlTop.ResumeLayout(false);
        	this.pnlTop.PerformLayout();
        	this.pnlType.ResumeLayout(false);
        	this.pnlType.PerformLayout();
        	this.pnlOrderNo.ResumeLayout(false);
        	this.pnlOrderNo.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.PicBoxWo)).EndInit();
        	this.pnlModuleSns.ResumeLayout(false);
        	this.pnlModuleSns.PerformLayout();
        	this.pnlCartons.ResumeLayout(false);
        	this.pnlCartons.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
        	this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Label lblCartonNo;
        private System.Windows.Forms.Panel pnlCartons;
        private System.Windows.Forms.TextBox txtCartons;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.ListView lstView;
        private System.Windows.Forms.Label lblModuleCntValue;
        private System.Windows.Forms.Label lblModuleCnt;
        private System.Windows.Forms.Panel pnlOrderNo;
        private System.Windows.Forms.Label lblReworkOrderNo;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.PictureBox PicBoxWo;
        private System.Windows.Forms.TextBox txtWo;
        private System.Windows.Forms.Panel pnlType;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Panel pnlModuleSns;
        private System.Windows.Forms.TextBox txtModuleSns;
        private System.Windows.Forms.Label lblModuleSns;
        private System.Windows.Forms.DataGridViewLinkColumn colClearByModuleSn;
        private System.Windows.Forms.DataGridViewLinkColumn colClearByCartonNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colModuleSn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colJobNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCartonNo;
    }
}