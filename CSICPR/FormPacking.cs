﻿using System;
//using System.IO;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using LablePLibrary;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;


namespace CSICPR
{
    public partial class FormPacking : Form
    {
        #region"private parameters"
        private DataTable prodDateSource;
        private string lastBarCode = "";
        private Dictionary<string, string> nominalPower = new Dictionary<string, string>();//存储标称功率 modify by Gengao 2012-09-05
        private string mixStr = "";
        private ArrayList lst;
        private string sqlIsSch, sqlGetArtNo, sqlGetPoint;
        private string sSN, sModel;
        private int nudCPTNum, nudMix, printNum;
        private string nudBoxPower = "";
        private string nudBoxPowerGrade = "";
        private bool cbCheckPower, cbCheckSN, rbAllPrint, rbOnePrint, rbNoPrint, rbLargeLabelPrint, rbTrueP, rbNomalP, rbModel, rbAutoPacking, rbHandPacking; //modified by shen yu on July 15, 2011, added rbLargeLabelPrint variable
        private string firstcode, secondcode, thirdcode;//自动生产箱号：firstcode-车间，secondcode-序列号，thirdcode-颜色
        private int labelCnt;
        private string tbCheckCode, sModulLabePath, sCartonLabelPath, sCartonPara;
        private string xmlFile = Application.StartupPath + "\\ParametersSetting.xml";//标签模板参数配置文件
        private ArrayList arrayCellColor;
        private string sPattern = "";
        private string sCellColor = "", sCellColorCode = "", sModuleSize = "", sCellType = "", ThisToBeColor = "";
        private string celltype = ""; //added by Shen Yu on July 15, 2011 for checking module type
        private string sModuleClass = "";//add by alex.dong 2011-07-22 for camo module grade
        private bool CheckModuleClass;//检查是否混组件包装.
        private DataTable DtModuleClass;//组件等级
        //private clsCSGenerateCarton GenerateCarton;
        private CSLargeLabel csLabel;
        private int iLabelH, iLabelV;
        private bool bIsConfig = false;
        //-------------end add
        private Dictionary<string, string> labelParameters = new Dictionary<string, string>();//存储标签参数名称
        private Dictionary<string, string> labelValue = new Dictionary<string, string>();//存储标签参数值
        private LargeLabel largeLabel;		//added by Shen Yu on July 18, 2011 
        private LargeLabelH smallLargeLabel;
        /// <summary>
        /// 内部托号
        /// </summary>
        private string sTempBoxID = "";
        /// <summary>
        /// 客户托号
        /// </summary>
        private string custBoxID = "";
        private string Work_Shop = "",Line = "";//add by alex.dong|2012-06-07|v2.0.6
        private string CurrentWorkShopDBconn = "";//add by alex.dong|2012-06-07|v2.0.6
        private bool IsCheckPattern = false;//True:检查Pattern,false:不检查patten

        private List<MODULE> _ListModule = new List<MODULE>();//CenterDB
        private List<MODULE_TEST> _ListModuleTest = new List<MODULE_TEST>();//CenterDB
        private string _CartonPower = "";//CenterDB 标称功率
        private string PackingType = "";
        private string POGroup = "";
        #endregion
        private static List<LanguageItemModel> LanMessList;
        private FormPacking()
        {
            InitializeComponent();
            #  region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion

            prodDateSource = new DataTable();

            if (tbHandPackingSQL.Text.Trim().Equals(""))
            {
                tbHandPackingSQL.Text = ToolsClass.getSQL(tbHandPackingSQL.Name);
            }

            loadParameters();
            if (this.printNum <= 0)
                this.printNum = 1;
            string[] nudBowPowerArrays = this.nudBoxPower.ToString().Split('_');
            if (nudBowPowerArrays.Length > 1)
            {
                this.tbNominalPowerGrade.Text = nudBowPowerArrays[1];
                this.tbNominalPower.Text = nudBowPowerArrays[0];
            }
            else
            {
                this.tbNominalPowerGrade.Text = "";
                this.tbNominalPower.Text = this.nudBoxPower.ToString();
            }
            //end modify by Gengao 2012-09-05

            arrayCellColor = new ArrayList();

            //v2.2|2011-08-05|add by alex.dong|for ChangShu Carton No
            if (ToolsClass.sSite != ToolsClass.CAMO)//2011-08-10|add by alex.dong|For distinguish Workshop
            {
                sTempBoxID = clsCSGenerateCarton.getBoxID(this.firstcode, this.secondcode, this.thirdcode, this.labelCnt);
                if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
                {
                    this.firstcode = "SCH-" + DateTime.Now.Year.ToString().Substring(2, 2) + "-";
                    this.thirdcode = ToolsClass.getConfig("Supplier") + "-";
                    this.secondcode = ToolsClass.getConfig("SchSerialCode");                    
                    this.labelCnt = 99999;
                    custBoxID = clsCSGenerateCarton.getBoxID(this.firstcode, this.secondcode, this.thirdcode, this.labelCnt, "SCH");
                }
                else
                {
                    custBoxID = sTempBoxID;
                    //custBoxID = clsCSGenerateCarton.getBoxID(this.firstcode, this.secondcode, this.thirdcode, this.labelCnt);
                }
                this.tbBoxCode.MaxLength = sTempBoxID.Length;
                this.tbCustBoxCode.MaxLength = custBoxID.Length;
                if (sTempBoxID.Equals(custBoxID))
                {
                    this.tbBoxCode.Text = sTempBoxID;
                    this.tbCustBoxCode.Text = sTempBoxID;
                }
                else
                {
                    this.tbBoxCode.Text = sTempBoxID;
                    this.tbCustBoxCode.Text = custBoxID;
                }
                if (this.tbBoxCode.Text.Trim().Equals(""))
                    ToolsClass.Log("Configured Carton number has been used up, Please reset Carton Number rules", "ABNORMAL", lstView);
            }
            //----------------------------end add v2.2
            Line = TbLine.Text.Trim();
            Work_Shop = ToolsClass.getConfig("WorkShop", false, "", "config.xml");//add by alex.dong|2012-06-07|v2.0.6        
            if (Work_Shop.ToUpper().Equals("M08"))
                btMixPackage.Visible = false;
            CurrentWorkShopDBconn = FormCover.connectionBase;
            labelParameters = ToolsClass.getLabelConfig();//Initial, Get Label Parameters
        }

        private void InitFormPacking()
        {
            prodDateSource = new DataTable();

            if (tbHandPackingSQL.Text.Trim().Equals(""))
            {
                tbHandPackingSQL.Text = ToolsClass.getSQL(tbHandPackingSQL.Name);
            }

            loadParameters();
            if (this.printNum <= 0)
                this.printNum = 1;
            string[] nudBowPowerArrays = this.nudBoxPower.ToString().Split('_');
            if (nudBowPowerArrays.Length > 1)
            {
                this.tbNominalPowerGrade.Text = nudBowPowerArrays[1];
                this.tbNominalPower.Text = nudBowPowerArrays[0];
            }
            else
            {
                this.tbNominalPowerGrade.Text = "";
                this.tbNominalPower.Text = this.nudBoxPower.ToString();
            }
            //end modify by Gengao 2012-09-05

            arrayCellColor = new ArrayList();

            //v2.2|2011-08-05|add by alex.dong|for ChangShu Carton No
            if (ToolsClass.sSite != ToolsClass.CAMO)//2011-08-10|add by alex.dong|For distinguish Workshop
            {
                sTempBoxID = clsCSGenerateCarton.getBoxID(this.firstcode, this.secondcode, this.thirdcode, this.labelCnt);
                if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
                {
                    this.firstcode = "SCH-" + DateTime.Now.Year.ToString().Substring(2, 2) + "-";
                    this.thirdcode = ToolsClass.getConfig("Supplier") + "-";
                    this.secondcode = ToolsClass.getConfig("SchSerialCode");
                    this.labelCnt = 99999;
                    custBoxID = clsCSGenerateCarton.getBoxID(this.firstcode, this.secondcode, this.thirdcode, this.labelCnt, "SCH");
                }
                else
                {
                    custBoxID = sTempBoxID;
                    //custBoxID = clsCSGenerateCarton.getBoxID(this.firstcode, this.secondcode, this.thirdcode, this.labelCnt);
                }
                this.tbBoxCode.MaxLength = sTempBoxID.Length;
                this.tbCustBoxCode.MaxLength = custBoxID.Length;
                if (sTempBoxID.Equals(custBoxID))
                {
                    this.tbBoxCode.Text = sTempBoxID;
                    this.tbCustBoxCode.Text = sTempBoxID;
                }
                else
                {
                    this.tbBoxCode.Text = sTempBoxID;
                    this.tbCustBoxCode.Text = custBoxID;
                }
                if (this.tbBoxCode.Text.Trim().Equals(""))
                   ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "维护的箱号范围已经使用，请重新维护!"), "ABNORMAL", lstView);
            }
            //----------------------------end add v2.2
            Work_Shop = ToolsClass.getConfig("WorkShop", false, "", "config.xml");//add by alex.dong|2012-06-07|v2.0.6
            if (Work_Shop.ToUpper().Equals("M08"))
                btMixPackage.Visible = false;
            CurrentWorkShopDBconn = FormCover.connectionBase;
            labelParameters = ToolsClass.getLabelConfig();//Initial, Get Label Parameters
        }

        #region"Event"
        private void textBox_Enter(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = System.Drawing.Color.Yellow;
        }
        private void textBox_Leave(object sender, EventArgs e)
        {
            ((Control)sender).BackColor = System.Drawing.SystemColors.Window;
        }
        private static FormPacking theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormPacking();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        private void pubRowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, FormMain.sbrush, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 5);
        }

        private void btColose_Click(object sender, EventArgs e)
        {
            this.rbModel = bool.Parse(ToolsClass.getConfig("Model"));
            this.rbTrueP = bool.Parse(ToolsClass.getConfig("RealPower"));

            if (FormCover.HasPower("btColose", btColose.Text))
                new FormBoxHandling("", this, true, this.rbModel, this.rbTrueP, false, btColose.Text).ShowDialog();
        }
        #endregion

        private void btPacking_Click(object sender, EventArgs e)
        {
            try
            {
  
            	string Certificate ="";
            	int sheetNo = 0;
            	Certificate = ToolsClass.getConfig("Certificate");
            	sheetNo = (Certificate == "Dual")?2:3;
            	
            	ToolsClass.saveXMLNode("SerialCode",tbBoxCode.Text.ToString().Substring(7,5));
                #region   
                if (dataGridView1.RowCount != this.nudCPTNum)
                {
                    btPacking.Enabled = true;
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message51", "包装数量和要打包数量不一致，请确认！"), "ABNORMAL", lstView);
                        return;
                }

                #region 检查箱号是否规则
                if (tbBoxCode.Text.Trim().Length != ProductStorageDAL.GetCartonNoLenth())
                {
                    ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message52", "内部箱号({0})长度不对,必须为{1}!"), this.tbBoxCode.Text.Trim(), ProductStorageDAL.GetCartonNoLenth()), "ABNORMAL", lstView);
                    return;
                }
                else
                {
                    #region 箱号必须是数字,用过后不能再被使用
                    string boxid = Convert.ToString(this.tbBoxCode.Text.Trim());
                    string flag = "Y";
                    foreach (char c in boxid)
                    {
                        if ((!char.IsNumber(c)))
                        {
                            flag = "N";
                            break;
                        }
                    }
                    if (flag.Equals("N"))
                    {
                        ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message53", "内部箱号({0})必须为数字!"), boxid), "ABNORMAL", lstView);
              			this.tbBarCode.SelectAll();
                        this.tbBoxCode.Focus();
                        return;
                    }
                    //箱号用过不允许再被使用
                    DataTable dt = ProductStorageDAL.GetCartonStatus(boxid);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        DataRow row = dt.Rows[0];
                        if (Convert.ToString(row["IsUsed"]).Equals("L"))
                        {
                            ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message54", "内部箱号({0})不能再被使用"), boxid), "ABNORMAL", lstView);
                             this.tbBarCode.SelectAll();
                            this.tbBoxCode.Focus();
                            return;
                        }
                    }
                    #endregion

                    //if (!Convert.ToString(this.tbBoxCode.Text.Trim().Substring(2, 2)).Equals(Convert.ToString(ToolsClass.sSite.Replace("M", ""))))
                    //{
                    //    ToolsClass.Log("内部箱号：" + tbBoxCode.Text.Trim() + " 的规则与本车间不符合!", "ABNORMAL", lstView);
                    //    return;
                    //}

                    if (!isPackage(tbBoxCode.Text.Trim(), true))
                    {
                         ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message55", "内部箱号已经被使用:")+ tbBoxCode.Text.Trim(), "ABNORMAL", lstView);
                         return;
                    }

                    if (ProductStorageDAL.GetCustCartonInfoFromCenterDB(this.tbCustBoxCode.Text.Trim()))
                    {
                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message56", "客户托号已经被使用:") + this.tbCustBoxCode.Text.Trim(), "ABNORMAL", lstView);
                       return;
                    }

                    if (!isPackage(tbCustBoxCode.Text.Trim(), true))
                    {
                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message57", "客户托号已经被内部托号使用:") + tbCustBoxCode.Text.Trim(), "ABNORMAL", lstView);
                         return;
                    }

                    if (dataGridView1.RowCount != this.nudCPTNum)
                    {
                       ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message2", "组件未满箱"), "ABNORMAL", lstView);
                        return;
                    }

                    #region 箱号添加车间后规则--不卡月份
                    //DataTable CartonRules = ProductStorageDAL.GetPackingSystemCartonNoRuleFlag();
                    //if (CartonRules != null && CartonRules.Rows.Count > 0)
                    //{
                    //    if (!Convert.ToString(this.tbBoxCode.Text.Trim().Substring(5, 2)).Equals(Convert.ToString(System.DateTime.Now.ToString("MM"))))
                    //    {
                    //        ToolsClass.Log("内部箱号：" + tbBoxCode.Text.Trim() + " 中的月份(" + this.tbBoxCode.Text.Trim().Substring(5, 2) + ")与本月份（" + System.DateTime.Now.ToString("MM") + "）不符!", "ABNORMAL", lstView);
                    //        return;
                    //    }
                    //}
                    #endregion
                }
                #endregion

                // 打印标签
                if (this.sModulLabePath.Length == 0)
                {
                    btPacking.Enabled = true;
                    return;
                }

                if (!sModuleClass.Equals("A") && (ToolsClass.sSite == ToolsClass.CAMO))
                {
                    if (!(MessageBox.Show("Are you sure you want to package this carton as Class " + sModuleClass + "?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
                        return;
                }

                //按车间排序
                dataGridView1.Sort(dataGridView1.Columns[2], ListSortDirection.Ascending);
                //构造SQL语句
                string sql = "", sql2 = "", model = "", conStr = "", sMuduleNo = "";
                model = dataGridView1[1, 0].Value.ToString();
                int location = model.IndexOf('-');
                if (location == -1)
                {
                    if (this.cbCheckPower)
                        model = model + "-" + this.nudMix + "MP";
                    else
                        model = model + "-" + dataGridView1[4, 0].Value.ToString() + "P";
                }
                else
                {
                    if (this.cbCheckPower)
                        model = model.Substring(0, location + 1) + this.nudMix + "MP";
                    else
                        model = model.Substring(0, location + 1) + dataGridView1[4, 0].Value.ToString() + "P";
                }
                #endregion

                #region
                //updatePackageData(sMuduleNo, model);//更新包装数据库

                //#region//CenterDB

                //if (dataGridView1.Rows.Count > 0 && _ListModule != null && _ListModule.Count > 0
                //    && _ListModuleTest != null && _ListModuleTest.Count > 0)
                //{
                //    #region
                //    try
                //    {
                //        foreach (MODULE_TEST test in _ListModuleTest)
                //        {
                //            new MODULE_TESTDAL().Add(test);
                //        }

                //        MODULE_CARTON moCarton = new MODULE_CARTON();
                //        moCarton.SYSID = Guid.NewGuid().ToString();
                //        if (Work_Shop.ToUpper().Equals("M08"))
                //            moCarton.SYSID = moCarton.SYSID + "-08";
                //        moCarton.STD_POWER_LEVEL = _CartonPower;
                //        moCarton.CARTON_NO = tbBoxCode.Text.Trim();
                //        moCarton.CREATED_NO = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                //        moCarton.CREATED_BY = FormCover.CurrUserName;
                //        moCarton.MODIFIED_BY = FormCover.CurrUserName;
                //        moCarton.MODIFIED_NO = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                //        new MODULE_CARTONDAL().Add(moCarton);

                //        StringBuilder log = new StringBuilder();
                //        foreach (MODULE mo in _ListModule)
                //        {
                //            if (mo.REMARK != "UnPacking")
                //            {
                //                new MODULEDAL().Add(mo);
                //            }

                //            MODULE_PACKING_LIST molk = new MODULE_PACKING_LIST();
                //            molk.MODULE_CARTON_SYSID = moCarton.SYSID;
                //            molk.MODULE_SYSID = mo.SYSID;
                //            new MODULE_PACKING_LISTDAL().Add(molk);

                //            log.AppendLine(string.Format("PackingList.CartonId:{0} ModuleSysId:{1}", moCarton.SYSID, mo.SYSID));
                //        }

                //        _ListModule = new List<MODULE>();
                //        _ListModuleTest = new List<MODULE_TEST>();

                //        ToolsClass.Log("PackingList\n" + log.ToString(), "PackingList", lstView);

                //    }
                //    catch (Exception exc)
                //    {
                //        ToolsClass.Log(exc.Message, "ABNORMAL", lstView);
                //    }
                //    #endregion
                //}
                //#endregion
                #endregion

                #region 保存,更新数据库
                if (dataGridView1.Rows.Count > 0 && _ListModule != null && _ListModule.Count > 0
                    && _ListModuleTest != null && _ListModuleTest.Count > 0)
                {
                    if (!_ListModule.Count.Equals(_ListModuleTest.Count))
                    {
                        MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message6", "测试数据和包装数据的数量不一致!"));
                       return;
                    }
                    #region
                    try
                    {
                        MODULE_CARTON moCarton = new MODULE_CARTON();
                        moCarton.SYSID = Guid.NewGuid().ToString();
                        if (Work_Shop.ToUpper().Equals("M08"))
                            moCarton.SYSID = moCarton.SYSID + "-08";

                        string[] NewCartonPower = _CartonPower.Split('_');
                        if (NewCartonPower.Length > 1)
                            moCarton.STD_POWER_LEVEL = NewCartonPower[0];
                        else
                            moCarton.STD_POWER_LEVEL = _CartonPower;
                        moCarton.CARTON_NO = tbCustBoxCode.Text.Trim(); //客户托号
                        moCarton.CREATED_NO = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                        moCarton.CREATED_BY = FormCover.CurrUserName;
                        moCarton.MODIFIED_BY = FormCover.CurrUserName;
                        moCarton.MODIFIED_NO = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                        moCarton.RESV03 = this.tbBoxCode.Text.Trim(); //内部托号
                        moCarton.RESV04 = FormCover.CurrentFactory.ToUpper().Trim();//车间

                        //重新统计每个组件的组件等级
                        foreach(MODULE vmodule in _ListModule)
                        {
                           string sn = vmodule.BARCODE.Trim();
                           foreach (DataGridViewRow row in this.dataGridView1.Rows)
                           {
                               if (sn == Convert.ToString(row.Cells["Column1"].Value).Trim())
                               {
                                   vmodule.Resv06 = Convert.ToString(row.Cells["Column12"].Value).Trim();
                                   break;
                               }
                           }
                        }

                        string msg = "";
                        if (!new Module_CartonDal().Packing(_ListModuleTest, _ListModule, moCarton, model, this.tbBoxCode.Text.Trim(),
                            sModuleClass, this.dataGridView1.RowCount, this.tbCustBoxCode.Text.Trim(), out msg))
                        {
                           ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message7", "打包失败:") + msg + "", "ABNORMAL", lstView);
                             return;
                            //updatePackageData(sMuduleNo, model);
                        }

                        _ListModule = new List<MODULE>();
                        _ListModuleTest = new List<MODULE_TEST>();

                       ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message9", "打包完成,开始打印标签"), "PackingList", lstView);

                    }
                    catch (Exception exc)
                    {
                       ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message10", "打包时发生异常:") + exc.Message, "ABNORMAL", lstView);
                           return;
                    }
                    #endregion
                }
                #endregion

                try
                {
                    #region"批量打印标签"	
                    if (this.rbAllPrint && this.sModulLabePath.Length > 0)
                    {
                        lst = new ArrayList();
                        for (int i = 0; i < dataGridView1.RowCount; i++)
                        {
                            printLabel(i);
                        }
                    }
                    #endregion

                    #region"注释CAMO列印大标签"
                    //	added by Shen Yu on July 18, 2011 for Large Label Printing
                    //////if (this.sCartonLabelPath.Length > 0 && this.rbLargeLabelPrint == true && (ToolsClass.sSite == ToolsClass.CAMO))//2011-08-10|add by alex.dong|For distinguish Workshop
                    //////{
                    //////    largeLabel = new LargeLabel();

                    //////    String tempsql = "select CONVERT(varchar(10), [ProcessDate], 101) from Process where SN='" + dataGridView1[0,0].Value.ToString() + "'";
                    //////    SqlDataReader read = ToolsClass.GetDataReader(tempsql);
                    //////    if(read.Read())
                    //////    {
                    //////        largeLabel.packingDate = read.GetString(0);
                    //////    }
                    //////    read.Close();
                    //////    read = null;

                    //////    largeLabel.cartonNumber = tbBoxCode.Text;
                    //////    largeLabel.colour = sCellColor;
                    //////    largeLabel.model = dataGridView1[1,0].Value.ToString();
                    //////    largeLabel.power = tbNominalPower.Text.Trim() + "W";//dataGridView1[4,0].Value.ToString()--modify by alex.dong
                    //////    largeLabel.cartonclass = sModuleClass;

                    //////    if(largeLabel.model[largeLabel.model.Length-1] == 'M')
                    //////    {
                    //////        largeLabel.cellType = "Mono";
                    //////    }
                    //////    if(largeLabel.model[largeLabel.model.Length-1] == 'P')
                    //////    {
                    //////        largeLabel.cellType = "Poly";
                    //////    }
                    //////    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                    //////    {
                    //////        largeLabel.arraylist.Add(dataGridView1[0,i].Value.ToString());
                    //////    }
                    //////    sql = LablePrint.LargeLabelPrint(this.sCartonLabelPath, largeLabel, 1);
                    //////    largeLabel = null;
                    //////    if (sql.Length > 0)
                    //////    {
                    //////        //MessageBox.Show("Packaging data is saved, Label Print Fail:" + sql, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //////        ToolsClass.Log("Packaging data is saved, Carton label print Failed:" + sql, "ABNORMAL", lstView);
                    //////        return;
                    //////    }
                    //////}
                    #endregion

                 #region"常熟列印大标签"
                  if (PackingType == "Horizontal")
          		  {
	            // Added by Shen Yu on July 18, 2011 for Large Label Printing
		            			
	            	            
	            
	            		if(this.sCartonLabelPath.Length > 0 && this.rbLargeLabelPrint == true)
		            {
		            		
	            			sql = @"select po from PO_Modification where InterID = (select MAX(InterID) from PO_Modification where sn = '"+dataGridView1.Rows[0].Cells[0].Value.ToString()+"')";
					        	string sequence = "";
					        	string Line = "";
					        	DataTable dtCheck = ToolsClass.GetDataTable(sql);
					        	if(dtCheck!=null && dtCheck.Rows.Count > 0)
					        	{
					        		sequence = dtCheck.Rows[0][0].ToString().Substring(6,3);
		        					Line = dtCheck.Rows[0][0].ToString().Substring(1,1);
					        	}
					        	else
					        	{
					        		sequence = dataGridView1.Rows[0].Cells[0].Value.ToString().Substring(7,3);

                                    string PONumber = GetPONumber(dataGridView1.Rows[0].Cells["Column1"].Value.ToString());
                                    Line = PONumber.Substring(1, 1);


                                    /*int lineNumber = Convert.ToInt32(dataGridView1.Rows[0].Cells[0].Value.ToString().Substring(5,2));
						        	if(lineNumber > 9 && lineNumber < 20 )
						        		Line = "A";
						        	else if(lineNumber > 19 && lineNumber < 30 )
						        		Line = "B";
						        	else if(lineNumber > 29 && lineNumber < 40 )
						        		Line = "C";
						        	else if(lineNumber > 39 && lineNumber < 50 )
					        			Line = "D";	*/
					        	}
					        	
					        	
					        	dtCheck.Rows.Clear();
//					        	SqlDataReader rd = ToolsClass.GetDataReader(sql);
//					        	if(rd!=null && rd.Read())
//					        	{
//					        		sequence = rd.GetString(0).Substring(6,3);
//		        					Line = rd.GetString(0).Substring(1,1);
//					        	}
//					        	else
//					        	{
//					        		sequence = dataGridView1.Rows[0].Cells[0].Value.ToString().Substring(7,3);
//					        	
//						        	int lineNumber = Convert.ToInt32(dataGridView1.Rows[0].Cells[0].Value.ToString().Substring(5,2));
//						        	if(lineNumber > 9 && lineNumber < 20 )
//						        		Line = "A";
//						        	else if(lineNumber > 19 && lineNumber < 30 )
//						        		Line = "B";
//						        	else if(lineNumber > 29 && lineNumber < 40 )
//						        		Line = "C";
//						        	else if(lineNumber > 39 && lineNumber < 50 )
//					        		Line = "D";	
//					        	}
	            			
	            				string lid = "";
	            				sql = @" Select top 1 [Resv01],[Resv02],[Resv03],[Resv04] from TSAP_WORKORDER_SALES_REQUEST where orderno like '{0}' order by CreatedOn desc";
	            				string CableLength = "", Connector =  "", BusBar ="", Market = "";
					        	string syr = DateTime.Now.Year.ToString().Substring(2,2);
					        	string sMth  = DateTime.Now.Month.ToString().PadLeft(2,'0');
	            				
	                    		sql = string.Format(sql,"C"+Line+ syr+"__"+ sequence);
	                    		
	                    		DataTable dtData = ToolsClass.GetDataTable1(sql,FormCover.InterfaceConnString);
	                    		if(dtData!=null && dtData.Rows.Count > 0)
	                    		{
	                    			Market = dtData.Rows[0][0].ToString();
			                    	lid = dtData.Rows[0][1].ToString();
			                    	CableLength =dtData.Rows[0][2].ToString();
			                    	Connector = dtData.Rows[0][3].ToString();
	                    		}
	                    		dtData.Rows.Clear();
	                    		if(string.IsNullOrEmpty(Market)||string.IsNullOrEmpty(lid)||string.IsNullOrEmpty(CableLength)||string.IsNullOrEmpty(Connector))
		                    	{
		                    		sql = @"select [Market],[Degradation],[Cable_Length],[Connector] from [ProductionOrder_Mixing_Rule] where [ProductionOrder] like '{0}'";
		                    		sql=string.Format(sql,"C"+Line+ syr+"__"+ sequence);
		                    		dtData = ToolsClass.GetDataTable(sql);
		                    		if(dtData!=null && dtData.Rows.Count > 0)
		                    		{
		                    			Market = dtData.Rows[0][0].ToString();
				                    	lid = dtData.Rows[0][1].ToString();
				                    	CableLength =dtData.Rows[0][2].ToString();
				                    	Connector = dtData.Rows[0][3].ToString();
		                    		}
		                    		dtData.Rows.Clear();
		                    	}
	                    		
	                    		
	                    		
	                    		
	                    		
//	                    		SqlDataReader reader = ToolsClass.GetDataReader(sql,FormCover.InterfaceConnString);	                    	
//		                    	while(reader.Read())
//		                    	{
//		                    		Market = reader.GetString(0);
//			                    	lid = reader.GetString(1);
//			                    	CableLength = reader.GetString(2);
//			                    	Connector = reader.GetString(3);
//			                    	
//		                    	}
//		                    	if(string.IsNullOrEmpty(Market)||string.IsNullOrEmpty(lid)||string.IsNullOrEmpty(CableLength)||string.IsNullOrEmpty(Connector))
//		                    	{
//		                    		sql = @"select [Market],[Degradation],[Cable_Length],[Connector] from [ProductionOrder_Mixing_Rule] where [ProductionOrder] like '{0}'";
//		                    		sql=string.Format(sql,"C"+Line+ syr+"__"+ sequence);
//		                    		reader  = ToolsClass.GetDataReader(sql);
//		                    		while(reader.Read())
//			                    	{
//				                    	Market = reader.GetString(0);
//				                    	lid = reader.GetString(1);
//				                    	CableLength = reader.GetString(2);
//				                    	Connector = reader.GetString(3);
//			                    	}
//		                    	}
		                    	
	            			
	            		smallLargeLabel = new LargeLabelH();
		            	smallLargeLabel.packingDate = DateTime.Now.ToString("yyyy-MMM-dd");
		            	smallLargeLabel.cartonNumber = tbCustBoxCode.Text;
		            	smallLargeLabel.colour = ThisToBeColor;
		            	smallLargeLabel.model = dataGridView1[1, 0].Value.ToString();
		            	smallLargeLabel.power = dataGridView1.Rows[0].Cells[4].Value.ToString();
		              	//smallLargeLabel.materialCode = "Jacky";// decision not made yet
		              	smallLargeLabel.LID = lid;
		              	
		              	smallLargeLabel.quantity = dataGridView1.RowCount.ToString();
		              	smallLargeLabel.grossweight = getWeight (dataGridView1[1, 0].Value.ToString(),"Gross", dataGridView1.RowCount);
		              	smallLargeLabel.netweight = getWeight (dataGridView1[1, 0].Value.ToString(), "Net", dataGridView1.RowCount);
		              	smallLargeLabel.size = getDimensions (dataGridView1[1, 0].Value.ToString(), dataGridView1.RowCount);
						//smallLargeLabel.CassCode = GetCassCode(tbMaterialCode.Text.Trim());
						#region ADDING J-box type, Cable Length, Connector type,  bus bar 
		                sql = @"select Busbar from ProductionOrder_Mixing_Rule where ProductionOrder like '{0}'";
		                sql = string.Format(sql,"C"+Line+ syr+"__"+ sequence);
		                dtData = ToolsClass.GetDataTable(sql);
		                if(dtData!=null && dtData.Rows.Count > 0)
		                	smallLargeLabel.BusBar =  dtData.Rows[0][0].ToString();
		                dtData.Rows.Clear();
//		                reader  = ToolsClass.GetDataReader(sql);
//		                    if(reader.Read() && reader!=null)
//		                    	smallLargeLabel.BusBar = reader.GetString(0);
		                smallLargeLabel.CableLength = CableLength;
		              	smallLargeLabel.ConnectorType = Connector;
		              	sql = @"select * from T_WORKORDER_BOM where [MaterialDescription] like '%gb%' and [MaterialGroup] = 1004 and [OrderNo] like '{0}'";
		              	sql = string.Format(sql,"C"+Line+ syr+"__"+ sequence);
		              	dtData = ToolsClass.GetDataTable(sql);
		              	smallLargeLabel.JboxType = (dtData!=null && dtData.Rows.Count > 0)?"CF1108-03gb":"CF1108-03";
//		              	reader  = ToolsClass.GetDataReader(sql);
//		              	smallLargeLabel.JboxType = (reader.Read() && reader!=null)?"CF1108-03gb":"CF1108-03";
		                #endregion
					
									
						//smallLargeLabel.loadID = loadID;	
			            if(smallLargeLabel.power != "Mixed")
			            {
			            	smallLargeLabel.power = smallLargeLabel.power + "W";
			            }
			            smallLargeLabel.cartonclass = dataGridView1.Rows[0].Cells["Column12"].Value.ToString();
		            	     
		            	if(smallLargeLabel.model[smallLargeLabel.model.Length-1] == 'M')
		            	{
		            		smallLargeLabel.cellType = "Mono";
		            	}
                        if (smallLargeLabel.model[smallLargeLabel.model.Length - 1] == 'S')
                        {
                            smallLargeLabel.cellType = "Perc";
                        }
                        if (smallLargeLabel.model[smallLargeLabel.model.Length-1] == 'P'||smallLargeLabel.model[smallLargeLabel.model.Length-1] == 'N')
		            	{
		            		smallLargeLabel.cellType = "Poly";
		            	}
		                for (int i = 0; i < dataGridView1.Rows.Count; i++)
		            	{
		            		smallLargeLabel.arraylist.Add(dataGridView1[0,i].Value.ToString());
		            	}	            
		            
			            csLabel = new CSLargeLabel("", this.sCartonLabelPath);
	                    csLabel.Write2ExcelH(this.iLabelH, iLabelV, smallLargeLabel,sheetNo);
		          	  }
            		}       
	
                  else if(PackingType == "Vertical")
                  {
	                    if (this.sCartonLabelPath.Length > 0 && this.rbLargeLabelPrint == true)
	                    {
	                    	
	                    	if (this.sCartonLabelPath.IndexOf(".xls") > 0)
	                        {
	                            largeLabel = new LargeLabel();
	                            largeLabel.CartonNumber = tbCustBoxCode.Text;
	                            largeLabel.Colour = ThisToBeColor;
	                            largeLabel.Model = dataGridView1[1, 0].Value.ToString();
	                            if(string.IsNullOrEmpty(Cell_Vendor.Text.ToString()))
	                            	largeLabel.CellVendor  = "";
	                            else
	                            	largeLabel.CellVendor = "Assembled in Canada with "+ Cell_Vendor.Text.ToString()+" cells";
	                            string CableLength = "", Connector =  "", BusBar ="", Market = "",lid = "";
	                          	string Line = "";
	                            string PONumber = GetPONumber(dataGridView1.Rows[0].Cells["Column1"].Value.ToString());  
								Line = PONumber.Substring(1,1);
	            				string PO = PONumber.Substring(6,3);
	            				                           
	                    		
	                          	sql = @"select System_Voltage from ProductionOrder_Mixing_Rule where ProductionOrder like '{0}' "; 
					        	
//					        	int lineNumber = Convert.ToInt32(dataGridView1.Rows[0].Cells["Column1"].Value.ToString().Substring(5,2));
//					        	if(lineNumber > 9 && lineNumber < 20 )
//					        		Line = "A";
//					        	else if(lineNumber > 19 && lineNumber < 30 )
//					        		Line = "B";
//					        	else if(lineNumber > 29 && lineNumber < 40 )
//					        		Line = "C";
//					        	else if(lineNumber > 39 && lineNumber < 50 )
//					        		Line = "D";
						                          	
	                          	
	                          	string syr = DateTime.Now.Year.ToString().Substring(2,2);
					        	string sMth  = DateTime.Now.Month.ToString().PadLeft(2,'0');
					        	
					        	//sql = string.Format(sql,"__"+ syr + sMth + sequence );
					        	sql = string.Format(sql,"C"+ Line + syr + "__" + PO );
					        	
					        	DataTable dtData = ToolsClass.GetDataTable(sql);
					        	if(dtData.Rows.Count > 0)					        		
					        		largeLabel.Voltage = dtData.Rows[0][0].ToString();
					        	dtData.Rows.Clear();
	                            sql = @" Select top 1 [Resv01],[Resv02],[Resv03],[Resv04] from TSAP_WORKORDER_SALES_REQUEST where orderno like '{0}' order by CreatedOn desc"; 
	                    		sql = string.Format(sql,"C"+Line+ syr+"__"+ PO);
	                    		
	                    		dtData = ToolsClass.GetDataTable1(sql,FormCover.InterfaceConnString);
	                    		if(dtData!=null && dtData.Rows.Count > 0)
	                    		{
	                    			Market = dtData.Rows[0][0].ToString();
			                    	lid = dtData.Rows[0][1].ToString();
			                    	CableLength =dtData.Rows[0][2].ToString();
			                    	Connector = dtData.Rows[0][3].ToString();
	                    		}
	                    		dtData.Rows.Clear();
	                    		if(string.IsNullOrEmpty(Market)||string.IsNullOrEmpty(lid)||string.IsNullOrEmpty(CableLength)||string.IsNullOrEmpty(Connector))
		                    	{
		                    		sql = @"select [Market],[Degradation],[Cable_Length],[Connector] from [ProductionOrder_Mixing_Rule] where [ProductionOrder] like '{0}'";
		                    		sql=string.Format(sql,"C"+Line+ syr+"__"+ PO);
		                    		dtData = ToolsClass.GetDataTable(sql);
		                    		if(dtData!=null && dtData.Rows.Count > 0)
		                    		{
		                    			Market = dtData.Rows[0][0].ToString();
				                    	lid = dtData.Rows[0][1].ToString();
				                    	CableLength =dtData.Rows[0][2].ToString();
				                    	Connector = dtData.Rows[0][3].ToString();
		                    		}
		                    		dtData.Rows.Clear();
		                    	}
	                    		
	                    		largeLabel.Market = Market;
	                    		largeLabel.LID = lid;
	                    		largeLabel.Cable = CableLength;
	                    		largeLabel.Connector = Connector;
	                    		
//	                    		SqlDataReader reader = ToolsClass.GetDataReader(sql,FormCover.InterfaceConnString);	                    	
//		                    	while(reader.Read())
//		                    	{
//			                    	largeLabel.Market = reader.GetString(0);
//			                    	largeLabel.LID = reader.GetString(1);
//			                    	largeLabel.Cable = reader.GetString(2);
//			                    	largeLabel.Connector = reader.GetString(3);
//		                    	}
//		                    	if(string.IsNullOrEmpty(largeLabel.Market)||string.IsNullOrEmpty(largeLabel.LID)||string.IsNullOrEmpty(largeLabel.Cable)||string.IsNullOrEmpty(largeLabel.Connector))
//		                    	{
//		                    		sql = @"select [Market],[Degradation],[Cable_Length],[Connector] from [ProductionOrder_Mixing_Rule] where [ProductionOrder] like '{0}'";
//		                    		string.Format(sql,"C"+Line+ syr+"__"+ PO);
//		                    		reader = ToolsClass.GetDataReader(sql);	
//		                    		while(reader.Read())
//			                    	{
//				                    	largeLabel.Market = reader.GetString(0);
//				                    	largeLabel.LID = reader.GetString(1);
//				                    	largeLabel.Cable = reader.GetString(2);
//				                    	largeLabel.Connector = reader.GetString(3);
//			                    	}
//		                    	}
		                    	
		                    	
		                    	sql = @"select BusBar, Frame_Color From ProductionOrder_Mixing_Rule where ProductionOrder like '{0}'";
		                    	sql = string.Format(sql,"C"+ Line + syr +"__"+ PO);
		                    	dtData = ToolsClass.GetDataTable(sql);
		                    	if(dtData!=null && dtData.Rows.Count > 0)
		                    	{
		                    		largeLabel.BusBar = dtData.Rows[0][0].ToString();
		                    		TbBusBar.Text = dtData.Rows[0][0].ToString();
		                    		largeLabel.FrameColor = dtData.Rows[0][1].ToString();
		                    	}
		                    	dtData.Rows.Clear();
//		                    	reader = ToolsClass.GetDataReader(sql);
//		                    	while(reader.Read())
//		                    	{
//		                    		largeLabel.BusBar = reader.GetString(0);
//		                    		TbBusBar.Text = reader.GetString(0);
//		                    		largeLabel.FrameColor = reader.GetString(1);
//		                    	}
//		                    
		                    	largeLabel.Class = TbClass.Text.ToString();
								
								largeLabel.Quantity = dataGridView1.RowCount;
//								if(ToolsClass.getConfig("GlassType") == "PN")
//								{
//									largeLabel.GrossWeight = "728Kg";
//		              				largeLabel.NetWeight = "655Kg";
//								}
//								else
//								{
//									largeLabel.GrossWeight = getWeight (dataGridView1[1, 0].Value.ToString(),"Gross", dataGridView1.RowCount);
//		              				largeLabel.NetWeight = getWeight (dataGridView1[1, 0].Value.ToString(), "Net", dataGridView1.RowCount);
//								}
//								
				              	largeLabel.GrossWeight = getWeight (dataGridView1[1, 0].Value.ToString(),"Gross", dataGridView1.RowCount);
				              	largeLabel.NetWeight = getWeight (dataGridView1[1, 0].Value.ToString(), "Net", dataGridView1.RowCount);
				              	largeLabel.Volumn = getDimensions (dataGridView1[1, 0].Value.ToString(), dataGridView1.RowCount);
	                            string[] powerArray = tbNominalPower.Text.Trim().Split('_');
	                            if (powerArray.Length > 1)
	                            {
	                                largeLabel.Power = powerArray[0].ToString();
	                                largeLabel.PowerGrade = powerArray[1];
	                            }
	                            else
	                            {
	                                largeLabel.Power = tbNominalPower.Text.Trim();
	                                largeLabel.PowerGrade = tbNominalPowerGrade.Text.ToString().TrimEnd();
	                            }
	
	                            largeLabel.Size = sModuleSize;
	                            largeLabel.Pattern = sPattern;
	
	                            if (largeLabel.Model[largeLabel.Model.Length - 1] == 'M')
	                            {
	                                largeLabel.CellType = "Mono";
	                                largeLabel.IsHavePattern = false;
	                            }
	                            if (largeLabel.Model[largeLabel.Model.Length - 1] == 'P' ||largeLabel.Model[largeLabel.Model.Length - 1] == 'N' )
	                            {
	                                largeLabel.CellType = "Poly";
	                                if (!string.IsNullOrEmpty(sPattern) && IsCheckPattern == true)
	                                    largeLabel.IsHavePattern = true;
	                            }
	
	                            #region 竖包装标签打印测试信心拼List数据
	                            List<List<string>> SerialNumberListTestData = new List<List<string>>();
	                            for (int i = 0; i < dataGridView1.Rows.Count; i++)
	                            {
	                                List<string> SerialNumberTestData = new List<string>();
	                                DataTable dt = ProductStorageDAL.GetFTData(dataGridView1[0, i].Value.ToString());
	                                if (dt != null && dt.Rows.Count > 0)
	                                {
	                                    DataRow row = dt.Rows[0];
	                                    SerialNumberTestData.Add(Convert.ToString(row["SN"]));//组件号                                   
	                                    SerialNumberTestData.Add(ProductStorageDAL.GetDecimalString(Convert.ToString(row["ISC"]), 2)); //工作电流                                      
	                                    SerialNumberTestData.Add(ProductStorageDAL.GetDecimalString(Convert.ToString(row["VOC"]), 2));//工作电压 
	                                    SerialNumberTestData.Add(ProductStorageDAL.GetDecimalString(Convert.ToString(row["Im"]), 2)); //开路 电流
	                                    SerialNumberTestData.Add(ProductStorageDAL.GetDecimalString(Convert.ToString(row["Vm"]), 2)); //开路 电流
	                                    SerialNumberTestData.Add(ProductStorageDAL.GetDecimalString(Convert.ToString(row["Pmax"]), 1));//实测功率 
	                                    SerialNumberTestData.Add(Convert.ToString(row["StdPower"]));//标称功率        
	                                }
	                                else
	                                {
	                                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message11", "列印竖包装标签时，此组件的测试数据不全:") + dataGridView1[0, i].Value.ToString(), "ABNORMAL", lstView);
                                   		break;
	                                }
	                                SerialNumberListTestData.Add(SerialNumberTestData);
	                                //largeLabel.Arraylist.Add(dataGridView1[0, i].Value.ToString());
	                            }
	 
	                            if (SerialNumberListTestData.Count != dataGridView1.Rows.Count)
	                                return;
	
	                            largeLabel.ListInfo = SerialNumberListTestData;
	
	                            #endregion
	
	                            largeLabel.IsShowPowerGrade = Convert.ToString(this.txtShowPowerGrade.Text.Trim());
	                           // csLabel = new CSLargeLabel("", this.sCartonLabelPath);
	                            csLabel = new CSLargeLabel("", this.sCartonLabelPath);
	                            csLabel.Write2Excel(this.iLabelH-2, iLabelV, largeLabel);
	                          
	                        }
	                        else
	                        {
	                           ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message12", "竖包装标签选择不正确，请选择【Excel】格式的标签文件！"), "ABNORMAL", lstView);
                           		 return;
	                        }
	                    }
                  }
                    #endregion

                    #region"打印包标签"
                    if (this.sCartonLabelPath.Length > 0 && this.rbLargeLabelPrint == false)
                    {
                        sql = LablePrint.printingLable(this.sCartonLabelPath, this.sCartonPara, this.tbCustBoxCode.Text, 2);
                        if (sql.Length > 0)
                        {
                            ToolsClass.Log("Packaging data is saved, Carton label print Failed:" + sql, "ABNORMAL", lstView);
                        }
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                   ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message58", "列印标签发生异常") + ex.Message + "", "ABNORMAL", lstView);
				}

                #region
               ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message14", "箱号打包完成:") + tbBoxCode.Text, "NORMAL", lstView);

//                if (!sModuleClass.Equals("A") && (ToolsClass.sSite == ToolsClass.CAMO))
//                {
//                    sModuleClass = "A";
//                    ToolsClass.saveXMLNode("ModuleClass", sModuleClass);
//                }
                loadParameters();
                if (ToolsClass.sSite != ToolsClass.CAMO)
                {
                    sTempBoxID = clsCSGenerateCarton.getBoxID(this.firstcode, this.secondcode, this.thirdcode, this.labelCnt);
                    if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
                    {
                        this.firstcode = "SCH-" + DateTime.Now.Year.ToString().Substring(2, 2) + "-";
                        this.thirdcode = ToolsClass.getConfig("Supplier") + "-";
                        this.secondcode = ToolsClass.getConfig("SchSerialCode");
                        this.labelCnt = 99999;
                        custBoxID = clsCSGenerateCarton.getBoxID(this.firstcode, this.secondcode, this.thirdcode, this.labelCnt, "SCH");
                    }
                    else
                    {
                        custBoxID = sTempBoxID;
                    }

                    if (sTempBoxID.Equals(custBoxID))
                    {
                        tbBoxCode.Text = sTempBoxID;
                        tbCustBoxCode.Text = sTempBoxID;
                    }
                    else
                    {
                        tbBoxCode.Text = sTempBoxID;
                        tbCustBoxCode.Text = custBoxID;
                    }
                }
                else
                {
                    tbBoxCode.Text = "";
                    tbCustBoxCode.Text = "";
                }

                arrayCellColor.Clear();
                sPattern = "";
                nominalPower.Clear();
                dataGridView1.Rows.Clear();
                TbClass.Clear();
                #endregion

                this.btPacking.Enabled = false;
                this.chbCarton.Checked = false;
                this.chbCustCarton.Checked = false;
                this.tbBoxCode.Enabled = false;
                this.tbCustBoxCode.Enabled = false;
            }
            catch (Exception ex)
            {
               ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message10", "打包时发生异常") + ex.Message + "!", "ABNORMAL", lstView);
                 return;
            }
        }

        private void FormPacking_Activated(object sender, EventArgs e)
        {
            if (tbBarCode.Enabled) tbBarCode.Focus();
        }

        private void btCancelSQL_Click(object sender, EventArgs e)
        {
            tbHandPackingSQL.Undo();
        }

        #region 包装验证
        private void tbBarCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                #region
                if (bIsConfig)
                {
                    loadParameters();
                    bIsConfig = false;
                }

                if (dataGridView1.Rows.Count == 0)
                {
                    arrayCellColor.Clear();
                    sPattern = "";
                    nominalPower.Clear();
                }

                if (e.KeyChar == '\r')
                {
                    Thread.Sleep(50);
				
                    string strTemp = tbBarCode.Text;
                    string[] sDataSet = strTemp.Split('\n');
                    for (int i = 0; i < sDataSet.Length; i++)
                    {
                        sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                           package(sDataSet[i], sender, e);
                       
                    }
//                    tbBarCode.Clear();
                    if (dataGridView1.Rows.Count > 0)
                        dataGridView1.FirstDisplayedScrollingRowIndex = dataGridView1.Rows.Count - 1;
                }
                #endregion
            }
            catch (Exception ex)
            {
               //ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message10", "打包时发生异常") + ex.Message + "!", "ABNORMAL", lstView);
                   return;
            }
        }

        /// <summary>
        /// 组件打包验证
        /// </summary>
        /// <param name="sSerialNumber">组件序列号</param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="isBatch"></param>
        private void package(string sSerialNumber, object sender, KeyPressEventArgs e, bool isBatch = true)
        {
            try
            {
                #region
                //是否已经打包完成
                
                if (dataGridView1.RowCount == this.nudCPTNum)
                {
                    btPacking.Enabled = true;
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message15", "已经达到箱包装数量,请包装"), "ABNORMAL", lstView);
                    tbBarCode.Clear();
                    return;
                }
//               		varifyworkordergroup
               		if(!verifyWorkOrderGroup(sSerialNumber))
               			return;
                	//Get Box Id
                	if (!verifyCellColorandGetBoxID(sSerialNumber))
                		return;
                	//VerifyifClass the same                	
                	sModuleClass = GetmoduleClass(sSerialNumber);
                	if(this.dataGridView1.Rows.Count < 1)
                	{
                		
                		if(sModuleClass == "A"||sModuleClass == "B")
                			TbClass.Text = sModuleClass;
                		else
                		{
                			 ToolsClass.Log("Module: "+ sSerialNumber + " is in class "+sModuleClass+" cannot be packed!", "ABNORMAL", lstView);
                			return;
                		}
                		
                	}
                	else
                	{
                		if(!sModuleClass.Equals(TbClass.Text.ToString()))
                		{
                			ToolsClass.Log("Module: "+ sSerialNumber + " is in class "+sModuleClass+" not in the same class with others!", "ABNORMAL", lstView);
                			return;
                		}
                	}

                #region 检查箱号是否符合规则
                if (this.dataGridView1.Rows.Count < 1)
                {
                	if (tbBoxCode.Text.Trim().Length != ProductStorageDAL.GetCartonNoLenth())
                    {
                       ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message59", "内部箱号({0})长度不对,必须为{1}"), this.tbBoxCode.Text.Trim(), ProductStorageDAL.GetCartonNoLenth()), "ABNORMAL", lstView);
                         return;
                    }
                    else
                    {
                        #region 内部箱号必须是数字,用过后不能再被使用
                        string boxid = Convert.ToString(this.tbBoxCode.Text.Trim());
                        string flag = "Y";
                        foreach (char c in boxid)
                        {
                            if ((!char.IsNumber(c)))
                            {
                                flag = "N";
                                break;
                            }
                        }
                        if (flag.Equals("N"))
                        {
                            ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message60", "内部箱号必须为数字:") + boxid, "ABNORMAL", lstView);
                            this.tbBarCode.SelectAll();
                            this.tbBoxCode.Focus();
                            return;
                        }
                        //箱号用过不允许再被使用
                        DataTable dt1 = ProductStorageDAL.GetCartonStatus(boxid);
                        if (dt1 != null && dt1.Rows.Count > 0)
                        {
                            DataRow row = dt1.Rows[0];
                            if (Convert.ToString(row["IsUsed"]).Equals("L"))
                            {
                               ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message61", "内部箱号为重工工单对应箱号,不能再被使用:"), "ABNORMAL", lstView);
                                this.tbBarCode.SelectAll();
                                this.tbBoxCode.Focus();
                                return;
                            }
                        }
                        #endregion

                        //if (!Convert.ToString(this.tbBoxCode.Text.Trim().Substring(2, 2)).Equals(Convert.ToString(ToolsClass.sSite.Replace("M", ""))))
                        //{
                        //    ToolsClass.Log("内部箱号：" + tbBoxCode.Text.Trim() + " 的规则与本车间不符合!", "ABNORMAL", lstView);
                        //    return;
                        //}

                        if(chbCarton.Checked)
                        {
                        	 if (!isPackage(tbBoxCode.Text.Trim(), true))
                        	{
                            	ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message55", "内部箱号已经被使用"), "ABNORMAL", lstView);
                           		return;
                        	}
                        }
                       

                        #region 箱号添加车间后规则--不卡月份
                        //DataTable CartonRules = ProductStorageDAL.GetPackingSystemCartonNoRuleFlag();
                        //if (CartonRules != null && CartonRules.Rows.Count > 0)
                        //{
                        //    if (!Convert.ToString(this.tbBoxCode.Text.Trim().Substring(5, 2)).Equals(Convert.ToString(System.DateTime.Now.ToString("MM"))))
                        //    {
                        //        ToolsClass.Log("内部箱号：" + tbBoxCode.Text.Trim() + " 中的月份(" + this.tbBoxCode.Text.Trim().Substring(5, 2) + ")与本月份（" + System.DateTime.Now.ToString("MM") + "）不符!", "ABNORMAL", lstView);
                        //        return;
                        //    }
                        //}
                        #endregion
                    }
                }
                #endregion
              
                if (!isPackage(sSerialNumber, false))
                {
                    return;
                }

                #region 检查组件是否存在
                DataTable dt = ProductStorageDAL.GetSNInfoBySn(sSerialNumber);
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    if (row["localflag"].ToString().Trim().Equals("1"))
                    {
                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message62", "此组件不是本车间的组件，如果打包，请先做测试:") + sSerialNumber, "ABNORMAL", lstView);
                        return;
                    }
                    else
                    {
                        if (row["flag"].ToString().Trim().Equals("1"))
                        {
                           ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message63", "此组件已经打包过:") + sSerialNumber, "ABNORMAL", lstView);
                            return;
                        }
                        else if (row["flag"].ToString().Trim().Equals("2"))
                        {
                            ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message64", "此组件已经入库:") + sSerialNumber, "ABNORMAL", lstView);
                            return;
                        }
                        else if (row["flag"].ToString().Trim().Equals("3"))
                        {
                            ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message65", "此组件已经SAP入库:") + sSerialNumber, "ABNORMAL", lstView);
                            return;
                        }
                        else if (row["flag"].ToString().Trim().Equals("5"))
                        {
                            ToolsClass.Log("Module:" + sSerialNumber + " has already merged", "ABNORMAL", lstView);
                            return;
                        }
                    }
                    //if (this.dataGridView1.Rows.Count >= 1)
                    //{
                    //    //重工工单的组件,不能混包
                    //    string Rework = Convert.ToString(this.txtRework.Text.Trim());
                    //    DataRow row = dt.Rows[0];
                    //    if (!Convert.ToString(row["ReWorkOrder"]).Trim().Equals(Rework))
                    //    {
                    //        ToolsClass.Log("此组件(" + sSerialNumber + ")为重工工单所对应的组件,不能混包!", "ABNORMAL", lstView);
                    //        return;
                    //    }
                    // }
                 }
                 else
                 {
                     ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message48", "组件号码不存在:") + sSerialNumber, "ABNORMAL", lstView);
                     return;
                 }
                #endregion

                #region 暂时不用
                //遏制检查
                //if (checkBox1.Checked)
                //{
                //    if (FormCurb.isIn(sSerialNumber))
                //    {
                //        ToolsClass.Log(sSerialNumber + " was prohibited,can not be packaged！", "ABNORMAL", lstView);
                //        tbBarCode.Clear();
                //        return;
                //    }
                //}
                #endregion

                /// <summary>
                //Jacky
                foreach (DataGridViewRow row in dataGridView1.Rows)
                	{
                    	if (row.Cells[0].Value != null && row.Cells[0].Value.Equals(sSerialNumber))
                   	 	{
                    	    ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message66", "这个组件：{0}在打包列表第{1}行"),sSerialNumber,(row.Index + 1)), "ABNORMAL", lstView);
                        	tbBarCode.Clear();//problem                   
                      	  	return;
                   	 	}
               		}
                
                if(!verifyModuleCanBePackage(sSerialNumber))
                	return;
                /// </summary>
                /// <param name="sender"></param>
                /// <param name="e"></param>
            

                //组件等级需要提前维护
//                if (this.sModuleClass.Trim().Equals(""))
//                {
//                    MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message67", "组件等级不能为空，请在包装参数配置里维护"));
//                    return;
//                }

                //检查是否打印组件标签
                if (this.sModulLabePath.Length == 0 && (this.rbAllPrint || this.rbOnePrint))
                {
                    tbBarCode.Text = "";
                    tbBarCode.Focus();
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message68","组件没有维护标签模板:")+ sSerialNumber, "ABNORMAL", lstView);
                    return;
                }
                //CTM judgement is or not 
                if (!DataAccess.IsCtmJudge(FormCover.PlanCode, FormCover.CurrentFactory.Trim(), FormCover.InterfaceConnString))
                {
                    if (!verifyCTM(sSerialNumber))
                        return;
                }

//                if (!verifyModuleCanBePackage(sSerialNumber))
//                    return;
			

                //打印SN组件标签
                if (this.rbOnePrint && this.sModulLabePath.Length > 0)
                {
                    printLabel(dataGridView1.Rows.Count - 1);
                }

                if (dataGridView1.RowCount == this.nudCPTNum)
                {
                    if (this.rbAutoPacking)
                    {
                        btPacking.Enabled = true;
                        btPacking_Click(sender, e);
                    }
                    else
                    {
                        btPacking.Enabled = true;
                        tbBarCode.Clear();
                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message15", "已经达到箱包装数量,请包装"), "ABNORMAL", lstView);
                        return;
                    }
                }            
                #endregion
            }
            catch (Exception ex)
            {
                //ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message10", "打包时发生异常:") + sSerialNumber, "ABNORMAL", lstView);
                //ToolsClass.Log( "Error with serial number: "+sSerialNumber, "ABNORMAL", lstView);                
                return;
            }
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (e.Row.Index < 0)
                return;

            string sn = this.dataGridView1[0, e.Row.Index].Value.ToString();
            for (int i = 0; i < _ListModule.Count; i++)
            {
                if (sn == _ListModule[i].MODULE_SN)
                {
                    _ListModule.RemoveAt(i);
                    break;
                }
            }

            for (int i = 0; i < _ListModuleTest.Count; i++)
            {
                if (sn == _ListModuleTest[i].MODULE_SN)
                {
                    _ListModuleTest.RemoveAt(i);
                    break;
                }
            }
        }
        #endregion

        #region"Control Event"
        private void btRePrint_Click(object sender, EventArgs e)
        {
            this.rbModel = bool.Parse(ToolsClass.getConfig("Model"));
            this.rbTrueP = bool.Parse(ToolsClass.getConfig("RealPower"));

            FormRePrint frp = new FormRePrint(this, true, this.rbModel, this.rbTrueP);
            frp.ShowDialog();
            frp.Dispose();
        }

        private void btTodayPackged_Click(object sender, EventArgs e)
        {
            this.rbModel = bool.Parse(ToolsClass.getConfig("Model"));
            this.rbTrueP = bool.Parse(ToolsClass.getConfig("RealPower"));

            if (FormCover.HasPower("btTodayPackged", btTodayPackged.Text))
                new FormBoxHandling("", this, true, this.rbModel, this.rbTrueP, true, btTodayPackged.Text).ShowDialog();
        }

        private void FormPacking_FormClosing(object sender, FormClosingEventArgs e)
        {
            nominalPower.Clear();
            arrayCellColor.Clear();
            sPattern = "";

            if (_ListModule.Count > 0)
                _ListModule.Clear();
            if (_ListModuleTest.Count > 0)
                _ListModuleTest.Clear();

            int j = 0;
            //if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
            //    j = int.Parse(ToolsClass.getConfig("SchSerialCode"));
            //else
            //    j = int.Parse(ToolsClass.getConfig("SerialCode"));

            if (!tbBoxCode.Text.Trim().Equals(""))
            {
                j = int.Parse(ToolsClass.getConfig("SerialCode"));

                string strsql = "update Box set IsUsed='N' where BoxID='" + tbBoxCode.Text.Trim() + "'";
                int iSql = ToolsClass.ExecuteNonQuery(strsql);
                j--;
                //if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
                //    ToolsClass.saveXMLNode("SchSerialCode", j.ToString());
                //else
                if (iSql >0)
                    ToolsClass.saveXMLNode("SerialCode", j.ToString());
            }
            if (!this.tbCustBoxCode.Text.Trim().Equals(""))
            {
                j = int.Parse(ToolsClass.getConfig("SchSerialCode"));

                string strsql = "update Box set IsUsed='N' where BoxID='" + tbCustBoxCode.Text.Trim() + "'";
                int iSql = ToolsClass.ExecuteNonQuery(strsql);
                j--;
                //if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
                if (iSql>0)
                    ToolsClass.saveXMLNode("SchSerialCode", j.ToString());
                //else
                //    ToolsClass.saveXMLNode("SerialCode", j.ToString());
            }
            ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message22", "包装画面已经关闭,  当前箱号:") + tbBoxCode.Text, "NORMAL", lstView);
        }

        private void btTodayNoPackge_Click(object sender, EventArgs e)
        {
            new FormNoPackge().ShowDialog();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                if (new FormCurb().ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    checkBox1.Checked = false;
            }
            tbBarCode.Focus();
        }

        private void btnBatchMode_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count == 0)
            {
            	if (FormCover.HasPower("btnBatchMode"))
            	{
                	var frm = new FormConfig("");
                	frm.ShowDialog();
            	
	                if (frm != null)
	                {
	                    frm.Dispose();
	                }             
	                bIsConfig = true;
            	}
            }
            else
            {
                 MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message22", "当前正在打包，请清除打包资料！"), "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
               // ToolsClass.Log("当前正在打包，请清除打包资料！", "ABNORMAL", lstView);
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message23", "当前正在打包，请清除打包资料!"), "ABNORMAL", lstView);
             }
            
             Cell_Vendor.Text = ToolsClass.getConfig("CellVendor");             
             TbLine.Text = ToolsClass.getConfig("Line");
            //InitFormPacking();
            #region Marked by alex.dong
            //////ArrayList Serialist = new ArrayList();
            //////KeyPressEventArgs ep = new KeyPressEventArgs('\r');
            //////OpenFileDialog open = new OpenFileDialog();
            //////open.Title = "Import Module Serial Number";
            //////open.Filter = "Notepad File(*.txt)|*.txt";
            //////if (open.ShowDialog() == DialogResult.OK)
            //////{
            //////    StreamReader sReader = new StreamReader(open.FileName);
            //////    string sLine = "";
            //////    while (sLine != null)
            //////    {
            //////        sLine = sReader.ReadLine();
            //////        if (sLine != null && sLine.Trim() !="")
            //////            Serialist.Add(sLine);
            //////    }
            //////    sReader.Close();
            //////}
            //////int i = Serialist.Count;
            //////foreach (string sSN in Serialist)
            //////{
            //////    tbBarCode.Text = sSN;
            //////    tbBarCode_KeyPress(sender, ep);
            //////}
            #endregion
        }

        private void nudIdealPower_ValueChanged(object sender, EventArgs e)
        {
        }

        private void tbNominalPower_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.tbNominalPower.Text.Trim()))
            {
                if (!string.IsNullOrEmpty(this.tbNominalPowerGrade.Text.TrimEnd()))
                {
                    ToolsClass.saveXMLNode("IdealPower", this.tbNominalPower.Text.Trim() + "_" + this.tbNominalPowerGrade.Text.TrimEnd());//Modify by Gengao 2012-09-05
                    this.nudBoxPower = this.tbNominalPower.Text.Trim() + "_" + this.tbNominalPowerGrade.Text.TrimEnd(); //Modify by Gengao 2012-09-05
                    ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message24", "上次包装的标称功率是:{0}标称功率档次是:{1}"), this.tbNominalPower.Text, this.tbNominalPowerGrade.Text.Trim()), "NORMAL", lstView);//Save Nominal Power
                }
                else
                {
                    ToolsClass.saveXMLNode("IdealPower", this.tbNominalPower.Text.Trim());
                    this.nudBoxPower = this.tbNominalPower.Text.Trim();
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message25", "上次包装的标称功率是") + this.tbNominalPower.Text, "NORMAL", lstView);//Save Nominal Power
                 }
            }
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            using (SolidBrush b = new SolidBrush(dataGridView1.RowHeadersDefaultCellStyle.ForeColor))
            {
                e.Graphics.DrawString((e.RowIndex + 1).ToString(System.Globalization.CultureInfo.CurrentCulture),
                        dataGridView1.DefaultCellStyle.Font, b, e.RowBounds.Location.X + 20, e.RowBounds.Location.Y + 4);
            }
        }

        private void FormPacking_Load(object sender, EventArgs e)
        {
            lstView.Columns.Add("Package Log", this.Width, HorizontalAlignment.Left);
            this.chbCarton.Checked = false;
            this.chbCustCarton.Checked = false;
            this.tbBoxCode.Enabled = false;
            this.tbCustBoxCode.Enabled = false;

            DtModuleClass = ProductStorageDAL.GetModuleClassInfo("ModuleClass");
            if (DtModuleClass != null && DtModuleClass.Rows.Count > 0)
            { }
            else
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message69", "组件等级没有维护，请联系系统管理员"));
                this.Close();
            }
        }

        private void FormPacking_Resize(object sender, EventArgs e)
        {
            if (lstView.Columns.Count > 0)
                lstView.Columns[0].Width = this.Width;
        }

        private void btMixPackage_Click(object sender, EventArgs e)
        {
            new FormNoPackge().ShowDialog();
        }
        #endregion

        #region"Method"
        /// <summary>
        /// 检查混装功率种类
        /// </summary>
        /// <param name="p">标称功率</param>
        /// <param name="sSerialNumber">组件序列号</param>
        /// <returns>true:不能混装，false:可以混装</returns>
        private bool checkPower(string p, string sSerialNumber = "") //Modify by Gengao 2012-09-05
        {
            try
            {
                if (nominalPower.ContainsKey(p))
                {
                    nominalPower[p] = nominalPower[p] + 1;
                }
                else
                {
                    if (this.cbCheckPower)
                    {
                        if (nominalPower.Count < this.nudMix)
                        {
                            nominalPower.Add(p, "1"); //Modify by Gengao 2012-09-05
                        }
                        else
                        {
                            ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message26", "{0} 组件标称功率是:{1}超过最大混装数量!"), sSerialNumber, p), "ABNORMAL", lstView);//Module Nominal Power is,exceeds mix limit
                             return false;
                        }
                    }
                    else
                    {
                        if (nominalPower.Count == 0 && this.nudBoxPower.ToString() == p)
                        {
                            nominalPower.Add(p, "1");
                        }
                        else
                        {
                            string[] arrays = p.Split('_');
                            if (arrays.Length > 1)
                                ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message27", "{0} 组件标称功率是:{1}标称功率档次为:{2},不允许混装"), sSerialNumber, arrays[0], arrays[1]), "ABNORMAL", lstView);
                            else
                                ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message28", "{0} 组件标称功率是:{1}标称功率档次为:不允许混装"), sSerialNumber, arrays[0]), "ABNORMAL", lstView);
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ToolsClass.Log(sSerialNumber + " error during checking power mixing rules:"+ex.Message+"", "ABNORMAL", lstView);
                return false;
            }
        }

        /// <summary>
        /// 检查工作号(工单)是否可以混装
        /// </summary>
        /// <param name="sn"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        private bool checkWorkCode(string sn, out string reason)
        {
            string tigWorkCode = "";
            reason = "";
            int location = this.tbCheckCode.LastIndexOf('-') + 1;
            reason = sn.Substring(0, location);
            if (this.tbCheckCode.EndsWith("*"))
            {
                if (dataGridView1.Rows.Count > 0)
                {
                    tigWorkCode = dataGridView1[0, 0].Value.ToString().Substring(0, location);
                }
                else
                {
                    tigWorkCode = reason;
                }
            }
            else
            {
                tigWorkCode = this.tbCheckCode.Substring(location);
            }
            if (tigWorkCode == reason)
            {
                reason = "";
            }
            else
            {
                // Removed because the MO are mixable, Zach
            	//if (MessageBox.Show(sn + LanguageHelper.GetMessage(LanMessList, "Message70", "组件工作号为：") + reason + (tbCheckCode.EndsWith("*") ? LanguageHelper.GetMessage(LanMessList, "Message71", "，与其它组件不同") : LanguageHelper.GetMessage(LanMessList, "Message72", "，与指定工作号不符，要继续吗？")), "prompt", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                //{
                //    return false;
                //}
            }
            return true;
        }

        /// <summary>
        /// 将组件号码和箱号在数据库中做Mapping关系
        /// </summary>
        /// <param name="sMuduleNo">组件序列号</param>
        /// <param name="model">版型</param>
        private void updatePackageData(string sMuduleNo, string model)
        {
            string conStr = "";
            object obj = dataGridView1[2, 0].Value;
            int start = 0;
            for (int f = 1; f < dataGridView1.RowCount; f++)
            {
                if (dataGridView1[2, f].Value == obj && f != dataGridView1.RowCount - 1)
                {
                    continue;
                }
                else
                {
                    if (obj.ToString() == "")
                        conStr = FormCover.connectionBase;
                    else
                        conStr = ToolsClass.getSQL(obj.ToString());
                    if ((f == dataGridView1.RowCount - 1) && obj == dataGridView1[2, f].Value)
                        f = dataGridView1.RowCount;
                    else
                        obj = dataGridView1[2, f].Value;
                }
                string sql = "", sql2 = "";
                using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(conStr))
                using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    SqlTransaction transaction = null;
                    try
                    {
                        if (conn.State == ConnectionState.Closed) conn.Open();
                        transaction = conn.BeginTransaction();
                        cmd.Transaction = transaction;
                        sql = "Update Box set IsUsed='Y',BoxType='" + model + "',ModelType='',Number=" + dataGridView1.RowCount + ",Mix='" + mixStr + "',PackOperator='" + FormCover.CurrUserWorkID + "' where BoxID='" + tbBoxCode.Text.Trim() + "';";
                        sql = sql + "if not exists (select BoxID from Box where boxid='" + tbBoxCode.Text.Trim() + "') insert into Box(BoxID,BoxType,ModelType,Number,IsUsed,PackOperator) values('" + tbBoxCode.Text.Trim() + "','" + model + "',''," + dataGridView1.RowCount + ",'Y','" + FormCover.CurrUserWorkID + "')";
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                        for (int t = start; t < f; t++)
                        {
                            sMuduleNo = dataGridView1[0, t].Value.ToString();
                            sql2 = "UPDATE [Product] SET [Process]='T5',[BoxID]='" + tbBoxCode.Text.Trim() + "',[Mix]='" + mixStr + "',[ModuleClass]='" + TbClass.Text.ToString() + "'" + ",[ProcessDateTime]=getdate()" + " WHERE [SN]='" + sMuduleNo + "' and [InterID] =(select max(InterID) from Product where SN='" + sMuduleNo + "')";
                            cmd.CommandText = sql2;
                            cmd.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        if (transaction != null)
                            transaction.Rollback();
                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message7", "打包失败:") + ex.Message + "", "ABNORMAL", lstView);
                        return;
                    }
                }
                start = f;
                if (f == dataGridView1.RowCount - 1)
                    f--;
            }

            btPacking.Enabled = false;
            mixStr = "";
        }

        /// <summary>
        /// 加载包装参数
        /// </summary>
        private void loadParameters()
        {
            try
            {
                this.sModulLabePath = ToolsClass.getConfig("ModuleLabelPath");
                this.sCartonLabelPath = ToolsClass.getConfig("CartonLabelPath");
                this.sCartonPara = ToolsClass.getConfig("CartonLabelParameter");

                this.nudCPTNum = int.Parse(ToolsClass.getConfig("CartonQty"));
                this.nudBoxPower = ToolsClass.getConfig("IdealPower");
                this.nudMix = int.Parse(ToolsClass.getConfig("MixCount"));

                this.cbCheckPower = bool.Parse(ToolsClass.getConfig("MixCount", true, "Checked"));
                this.cbCheckSN = bool.Parse(ToolsClass.getConfig("CheckRule", true, "Checked"));
                this.tbCheckCode = ToolsClass.getConfig("CheckRule");

                this.rbOnePrint = bool.Parse(ToolsClass.getConfig("OnePrint"));
                this.rbAllPrint = bool.Parse(ToolsClass.getConfig("BatchPrint"));
                this.rbNoPrint = bool.Parse(ToolsClass.getConfig("NoPrint"));
                this.rbLargeLabelPrint = bool.Parse(ToolsClass.getConfig("LargeLabelPrint"));

                this.rbModel = bool.Parse(ToolsClass.getConfig("Model"));
                this.rbTrueP = bool.Parse(ToolsClass.getConfig("RealPower"));
                this.rbNomalP = bool.Parse(ToolsClass.getConfig("NominalPower"));
               	this.PackingType = ToolsClass.getConfig("PType");
                if (ToolsClass.sSite == ToolsClass.CAMO)
                {
                    this.firstcode = ToolsClass.getConfig("StationNo").Replace('\n', ' ').Trim();
                    //this.sModuleClass = ToolsClass.getConfig("ModuleClass");
                    this.iLabelH = int.Parse(ToolsClass.getConfig("ExcelH"));
                    this.iLabelV = int.Parse(ToolsClass.getConfig("ExcelV"));
                }
                else
                {
                    this.firstcode = ToolsClass.getConfig("FirstCode").Replace('\n', ' ').Trim();
                    this.labelCnt = int.Parse(ToolsClass.getConfig("LabelCount").Replace('\n', ' ').Trim());
                    this.thirdcode = ToolsClass.getConfig("ColorCode").Replace('\n', ' ').Trim();
                    this.iLabelH = int.Parse(ToolsClass.getConfig("ExcelH"));
                    this.iLabelV = int.Parse(ToolsClass.getConfig("ExcelV"));
                    //this.sModuleClass = ToolsClass.getConfig("ModuleClass");
                }
                try
                {
                    this.CheckModuleClass = bool.Parse(ToolsClass.getConfig("CheckModuleClass"));
                }
                catch (Exception ex)
                {
                    this.CheckModuleClass = false;
                }

                this.secondcode = ToolsClass.getConfig("SerialCode");

                this.rbAutoPacking = bool.Parse(ToolsClass.getConfig("AutoMode"));
                this.rbHandPacking = bool.Parse(ToolsClass.getConfig("ManualMode"));
				
                this.printNum = int.Parse(ToolsClass.getConfig("PrintCount"));
                Cell_Vendor.Text = ToolsClass.getConfig("CellVendor");
                TbLine.Text = ToolsClass.getConfig("Line");
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message30", "包装参数加载成功!"), "NORMAL", lstView);
            }
            catch
            {
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message31", "包装参数加载失败!"), "ABNORMAL", lstView);
            }
        }

        /// <summary>
        /// 收集Label标签value，记录在ArrayList
        /// </summary>
        private void arrayLabelInfo(string sModelType, string sSerialNo = "")
        {
            if (sSerialNo != "")
                this.sSN = sSerialNo;
            if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
            {
                sqlGetPoint = "select Pmpp,Vmpp,Impp,VOC,ISC from point with(nolock) where InterID =(select ItemPointID from Scheme with(nolock) where InterID=(Select distinct SchemeInterID From ElecParaTest with(nolock) where FNumber='" + this.sSN + "' and InterID =(select max(InterID) from ElecParaTest with(nolock) where FNumber='" + this.sSN + "')))";
                //(select ItemPointID from Scheme where ItemMPID=(select InterID from Item where ItemValue='"+temp[1]+"') and ItemModelID=(select InterID from Item where ItemValue='" + temp[0] + "'))";
                sqlGetArtNo = "select NOValue as ART_No from ModelType with(nolock) where ModelTypeID=";
                sqlIsSch = "select Pmax as Pmpp,VOC as Voc,ISC as Isc,Vm as Vmpp,Im as Impp from ElecParaTest with(nolock) where FNumber='" + this.sSN + "' and InterID =(select max(InterID) from ElecParaTest with(nolock) where FNumber='" + this.sSN + "')";

                SqlDataReader read = ToolsClass.GetDataReader(sqlIsSch);
                string sArtNo = "";
                int iPmpp = 0, iImpp = 0, iVmpp = 0, iVOC = 0, iISC = 0;
                //sqlGetPoint = sqlGetPoint + "'" + sSN + "'))";
                SqlDataReader read1 = ToolsClass.GetDataReader(sqlGetPoint);
                if (read1.Read())
                {
                    iPmpp = read1.GetInt32(0);
                    iVmpp = read1.GetInt32(1);
                    iImpp = read1.GetInt32(2);
                    iVOC = read1.GetInt32(3);
                    iISC = read1.GetInt32(4);
                }

                sqlGetArtNo = sqlGetArtNo + "'" + sModelType + "'";
                SqlDataReader read2 = ToolsClass.GetDataReader(sqlGetArtNo);
                if (read2.Read())
                    sArtNo = read2.GetString(0);

                if (read.Read())
                {
                    this.sSN = sArtNo + this.sSN + ":" + ToolsClass.Round(read.GetDouble(0), iPmpp).ToString() + ":" + ToolsClass.Round(read.GetDouble(1), iVOC).ToString() + ":" + ToolsClass.Round(read.GetDouble(2), iISC).ToString() + ":" + ToolsClass.Round(read.GetDouble(3), iVmpp).ToString() + ":" + ToolsClass.Round(read.GetDouble(4), iImpp).ToString();
                }

                read.Close(); read = null;
                read1.Close(); read1 = null;
                read2.Close(); read2 = null;
            }
            else
            {
                sqlIsSch = "select CellSpec as Size,CellType as type,CellColor as Color from Product with(nolock) where SN='" + this.sSN + "' and InterID =(select max(InterID) from Product with(nolock) where SN='" + this.sSN + "')";
                SqlDataReader read = ToolsClass.GetDataReader(sqlIsSch);
                if (read.Read())
                {
                    this.sSN = this.sSN + ":" + this.sModel + ":" + read.GetString(0) + ":" + read.GetString(1) + ":" + read.GetString(2);
                }

                read.Close(); read = null;
            }
            lst.Add(this.sSN);
        }

        /// <summary>
        /// 收集Label标签value，记录在数据字典
        /// </summary>
        /// <param name="sModelType">组件版型</param>
        /// <param name="sSerialNo">组件序列号</param>
        private void dictLabelInfo(string sModelType, string sSerialNo = "")
        {
            if (sSerialNo != "")
                this.sSN = sSerialNo;
            if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
            {
                sqlGetPoint = "select Pmpp,Vmpp,Impp,VOC,ISC from point with(nolock) where InterID =(select ItemPointID from Scheme with(nolock) where InterID=(Select distinct SchemeInterID From ElecParaTest with(nolock) where FNumber='" + this.sSN + "' and InterID =(select max(InterID) from ElecParaTest with(nolock) where FNumber='" + this.sSN + "')))";
                sqlGetArtNo = "select NOValue as ART_No from ModelType where ModelTypeID=";
                sqlIsSch = "select Pmax as Pmpp,VOC as Voc,ISC as Isc,Vm as Vmpp,Im as Impp from ElecParaTest with(nolock) where FNumber='" + this.sSN + "' and InterID =(select max(InterID) from ElecParaTest with(nolock) where FNumber='" + this.sSN + "')";

                SqlDataReader read = ToolsClass.GetDataReader(sqlIsSch);
                string sArtNo = "";
                int iPmpp = 0, iImpp = 0, iVmpp = 0, iVOC = 0, iISC = 0;
                SqlDataReader read1 = ToolsClass.GetDataReader(sqlGetPoint);
                if (read1.Read())
                {
                    iPmpp = read1.GetInt32(0);
                    iVmpp = read1.GetInt32(1);
                    iImpp = read1.GetInt32(2);
                    iVOC = read1.GetInt32(3);
                    iISC = read1.GetInt32(4);
                }

                sqlGetArtNo = sqlGetArtNo + "'" + sModelType + "'";
                SqlDataReader read2 = ToolsClass.GetDataReader(sqlGetArtNo);
                if (read2.Read())
                    sArtNo = read2.GetString(0);

                if (read.Read())
                {
                    this.labelValue = new Dictionary<string, string>();
                    this.labelValue.Add(this.labelParameters["ModuleNo"], sArtNo + this.sSN);
                    this.labelValue.Add(this.labelParameters["Pmpp"], ToolsClass.Round(read.GetDouble(0), iPmpp).ToString());
                    this.labelValue.Add(this.labelParameters["Voc"], ToolsClass.Round(read.GetDouble(1), iVOC).ToString());
                    this.labelValue.Add(this.labelParameters["Isc"], ToolsClass.Round(read.GetDouble(2), iISC).ToString());
                    this.labelValue.Add(this.labelParameters["Vmpp"], ToolsClass.Round(read.GetDouble(3), iVmpp).ToString());
                    this.labelValue.Add(this.labelParameters["Impp"], ToolsClass.Round(read.GetDouble(4), iImpp).ToString());
                }

                read.Close(); read = null;
                read1.Close(); read1 = null;
                read2.Close(); read2 = null;
            }
            else
            {
                sqlIsSch = "select Size,type,Color,ElecParaTest.Pmax from(select CellSpec as Size,CellType as type,CellColor as Color,InterNewTempTableID from Product with(nolock) where SN='" + this.sSN + "' and InterID =(select max(InterID) from Product with(nolock) where SN='" + this.sSN + "'))as Prod inner join ElecParaTest on ElecParaTest.InterID=Prod.InterNewTempTableID";
                SqlDataReader read = ToolsClass.GetDataReader(sqlIsSch);
                if (read.Read())
                {
                    this.labelValue = new Dictionary<string, string>();
                    this.labelValue.Add(this.labelParameters["ModuleNo"], this.sSN);
                    this.labelValue.Add(this.labelParameters["Model"], this.sModel);
                    this.labelValue.Add(this.labelParameters["Size"], read.GetString(0));
                    this.labelValue.Add(this.labelParameters["ChipType"], read.GetString(1));
                    this.labelValue.Add(this.labelParameters["Color"], read.GetString(2));
                    this.labelValue.Add(this.labelParameters["Pmpp"], ToolsClass.Round(read.GetDouble(3), 1).ToString());
                }

                read.Close(); read = null;
            }
        }

        /// <summary>
        /// 验证CTM是否判等
        /// </summary>
        /// <param name="sSerialNumber"></param>
        /// <returns></returns>
        private bool verifyCTM(string sSerialNumber)
        {
            try
            {
                #region
                bool verify = true;
                string sqlCTM = "select CTMGrade from Product with(nolock) where SN='{0}' and InterID =(select max(InterID) from product with(nolock) where SN='{0}')";
                sqlCTM = string.Format(sqlCTM, sSerialNumber);
                SqlDataReader sdr = ToolsClass.GetDataReader(sqlCTM);
                if (sdr.Read())
                {
                    string sCTMGrade = "";
                    try
                    {
                        sCTMGrade = sdr.GetString(0);
                    }
                    catch
                    {
                        sCTMGrade = "";
                    }
                    if (sCTMGrade.ToUpper().Equals("NG"))
                    {
                        ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message32", "组件：{0} 的CTM判等NG，不能打包!!!"), sSerialNumber), "ABNORMAL", lstView);
                        verify = false;
                    }
                    if (sCTMGrade.ToUpper().Equals("W") || sCTMGrade.Equals(""))
                    {
                        ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message33", "组件：{0} 的CTM没有判等，不能打包!!!"), sSerialNumber), "ABNORMAL", lstView);
                        verify = false;
                    }
                }
                sdr.Close();
                sdr = null;
                return verify;
                #endregion
            }
            catch (Exception ex)
            {
                ToolsClass.Log("Module:(" + sSerialNumber + ")Error during CTM assessment", "ABNORMAL", lstView);
                return false;
            }
        }

        /// <summary>
        /// 重打印标签
        /// </summary>
        /// <param name="codeArr"></param>
        /// <param name="BoxCode"></param>
        /// <param name="sModelPowerSet"></param>
        /// <param name="sModelTypeSet"></param>
        /// <returns></returns>
        public string RePrint(string[,] codeArr = null, string BoxCode = "", string[] sModelPowerSet = null, string[] sModelTypeSet = null)
        {
            string rt = "";
            if (this.sModulLabePath.Length == 0)
            {
                return "Setting Module Label File Path Before Print Label";
            }
            if (codeArr != null && this.sModulLabePath.Length > 0 && sModelPowerSet != null && this.rbLargeLabelPrint == false)
            {
                //2011-05-24|add by alex.dong|for schuco----------------------------------------------
                lst = new ArrayList();
                for (int i = 0; i < codeArr.GetLength(0); i++)
                {
                    this.sSN = codeArr[i, 0];
                    string sModelType = sModelPowerSet[i];
                    dictLabelInfo(sModelType);

                    rt = LablePrint.printingLable(this.sModulLabePath, labelValue, this.printNum);
                    if (rt.Length > 0)
                    {
                        return rt;
                    }
                }
            }

            if (BoxCode.Length > 8 && this.sCartonLabelPath.Length > 0 && this.rbLargeLabelPrint == false)
            {
                rt = LablePrint.printingLable(this.sCartonLabelPath, this.sCartonPara, BoxCode, this.printNum);
            }
            return rt;
        }
		
        private bool verifyWorkOrderGroup(string sSerialNumber)
        {
        	
        	string PO = GetPONumber(sSerialNumber);
        	if (string.IsNullOrEmpty(PO)) {
        		return false;
        	}
        	string line	 = PO.Substring(1,1);
        	string sequence = PO.Substring(6,3);
        	
        	
        	
        	string sql = @"select PO_Group from ProductionOrder_Mixing_Rule where ProductionOrder like '{0}' ";
        	string syr = sSerialNumber.Substring(1,2);
        	sql = string.Format(sql,"C"+ line + syr + "__" + sequence );
        	DataTable dt =  ToolsClass.GetDataTable(sql);
        	string Group = "";
        	if (dt.Rows.Count > 0) {
        		Group = dt.Rows[0][0].ToString();
        	}	
        	
        	if(string.IsNullOrEmpty(Group))
        	{	
        		ToolsClass.Log("No Info Associated with this Production Order, Please maintain the Production Order Mixing Table!", "ABNORMAL", lstView);
        		return false;
        	} 
        	if(string.IsNullOrEmpty(Tb_WoG.Text.ToString()))
        	{
        		Tb_WoG.Text = Group;
        		return true;
        	}
        	if (!string.IsNullOrEmpty(Tb_WoG.Text.ToString())) {
        		if(!Tb_WoG.Text.ToString().Equals(Group))
        		{
        			ToolsClass.Log("serial number:" + sSerialNumber + " in Group: " + Group +" Cannot be mixed with group: "+ Tb_WoG.Text.ToString()+"!", "ABNORMAL", lstView);
        			return false;;
        		} 
				else
					return true;					
        	}
        	
        	return true;	
        	       	
			        		
        		
        }
        
        
        private string GetPONumber(string SN)
        {
        	if (string.IsNullOrEmpty(SN)) {
        		MessageBox.Show("Serial number: "+SN+" does not have a matched PO, please check!");
        		return "";
        	}
        	
        	string sql = @"select po from PO_Modification where InterID = (select MAX(InterID) from PO_Modification where sn = '"+SN+"')";
        	string sequence = "";
        	string Line = "";
        	
        	string syr = SN.Substring(1,2);
        	string sMonth = SN.Substring(3,2);
        	SqlDataReader rd = ToolsClass.GetDataReader(sql);
        	if(rd!=null && rd.Read())
        	{
        		sequence = rd.GetString(0).Substring(6,3);
		        Line = rd.GetString(0).Substring(1,1);
        	}
        	else
        	{
        		sequence = SN.Substring(7,3);
	        	int lineNumber = Convert.ToInt32(SN.Substring(5,2));
	        

	        
	        	//Changeover on 2016/10/01
	        	if((Convert.ToInt16(syr) == 16 && Convert.ToInt16(sMonth) > 9) || (Convert.ToInt16(syr) > 16 ))

                {
		        	switch (lineNumber) {
		        			case 40:
		        					Line = "A";
		        					break;
		        				case 41:
		        					Line = "A";
		        					break;
		        				case 42:
		        					Line = "B";
		        					break;
		        				case 43:
		        					Line = "B";
		        					break;
		        				case 44:
		        					Line = "C";
		        					break;
		        				case 45:
		        					return GetPONumber(getOriginalSn(SN));
		        					break;
		        	}
	        	}
	        	else
	        	{
		        	if(lineNumber > 9 && lineNumber < 20 )
		        		Line = "A";
		        	else if(lineNumber > 19 && lineNumber < 30 )
		        		Line = "B";
		        	else if(lineNumber > 29 && lineNumber < 40 )
		        		Line = "C";
		        	else if(lineNumber > 39 && lineNumber < 50 )
		        		Line = "D";
	        	}
        	}
        	
        	return "C"+Line+syr+sMonth+sequence;
        	
        	
        }
        
        private string getOriginalSn(string SN)
        {
        	string sql = @" select DuplicatedSN from SapDuplicationSN where NewSN = '{0}'";
	        		sql = string.Format(sql,SN);
	        		DataTable OriginalSn = ToolsClass.GetDataTable1(sql);
	        		if (OriginalSn != null && OriginalSn.Rows.Count > 0 ) {
	        			return OriginalSn.Rows[0][0].ToString();
	        		}
	        		return "";
        }
        /// <summary>
        /// 验证Cell Color是否一致.不一致，不能装箱
        /// </summary>
        /// <param name="sSerialNumber"></param>
        /// <returns></returns>
        private bool verifyCellColorandGetBoxID(string sSerialNumber)
        {
            try
            {
                #region
                bool verifyFlag = true;
                sCellColor = "";
                sModuleSize = "";
                string sqlCellColor = "select CellType,CellSpec,Pattern,CellColor from Product with(nolock) where SN='{0}' and InterID = (select max(InterID) FROM Product with(nolock) Where SN ='{0}')";
                SqlDataReader rdColor = ToolsClass.GetDataReader(string.Format(sqlCellColor, sSerialNumber));
                if (rdColor.Read())
                {
                    sCellColor = rdColor.GetString(0);
                    sModuleSize = rdColor.GetString(1);
                    ThisToBeColor = rdColor.GetString(3);
                    if (IsCheckPattern)
                    {
                        if (!rdColor.IsDBNull(2))
                        {
                            if (sPattern == "")
                            {
                                sPattern = rdColor.GetString(2);
                            }
                            else
                            {
                                if (!sPattern.Equals(rdColor.GetString(2).Trim()))
                                {
                                    ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message34", "组件：{0}的Pattern:{1} 与已经打包的组件Pattern:{2}不一致"), sSerialNumber, rdColor.GetString(2), sPattern), "ABNORMAL", lstView);
                                    return false;
                                }
                            }
                        }
                    }
					switch (Line) {
						case "A":
							firstcode = "40";
							break;
						case "B":
							firstcode = "42";
							break;
						case "C":
							firstcode = "44";
							break;
						case "D":
							firstcode = "43";
							break;						
						default:
							firstcode = "45";
							break;
					}
                    if (arrayCellColor.Count == 0)                    
                    {
                        sCellColorCode = "";
                        if (sCellColor.ToUpper().Equals("P")||sCellColor.ToUpper().Equals("PN"))		
                            sCellColorCode = "2";
                        if (sCellColor.ToUpper().Equals("M")|| sCellColor.ToUpper().Equals("MS"))		
                            sCellColorCode = "1";
                     

                        arrayCellColor.Add(sCellColor.ToUpper());                        
                        if (ToolsClass.sSite == ToolsClass.CAMO)
                        {
                        	//if(tbBoxCode.Text == "")
                        	if(dataGridView1.Rows.Count < 1)
                            tbBoxCode.Text = ToolsClass.getBoxID(this.firstcode, sCellColorCode);
                            tbCustBoxCode.Text = tbBoxCode.Text;
                            
                            ToolsClass.Log("Generate New Carton Number: " + tbBoxCode.Text, "NORMAL", lstView);
                        }
                    }
                }
                else
                {
                	ToolsClass.Log("No Flash data!", "ABNORMAL", lstView);
                	return false;
                        
                }
                rdColor.Close(); rdColor = null;
                if (arrayCellColor.Count > 0)
                {
                    if (!arrayCellColor.Contains(sCellColor.ToUpper()))
                    {
                        ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message35", "组件：{0} Cell Color:{1}与箱的 Cell Color不匹配:{2}不同颜色的组件不能混装"), sSerialNumber, sCellColor, arrayCellColor[0].ToString()), "ABNORMAL", lstView);
                        return false;
                    }
                }
                if (tbBoxCode.Text.Trim().Equals(""))
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message36", "箱号已经用完，请设定新的箱号规则"), "ABNORMAL", lstView);
                    return false;
                }
                return verifyFlag;
                #endregion
            }
            catch (Exception ex)
            {
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message73", "组件在验证颜色，型号是否一致时发生异常:") + sSerialNumber + ex.Message, "ABNORMAL", lstView);
                return false;
            }
        }

        /// <summary>
        /// 验证组件是否符合打包规则
        /// </summary>
        /// <param name="sSerialNumber"></param>
        /// <returns></returns>
        private bool verifyModuleCanBePackage(string sSerialNumber)
        {
            try
            {
                if (!isPackage(sSerialNumber, false))
                    return false;

                using (SqlDataReader reader = ToolsClass.GetDataReader(string.Format(tbHandPackingSQL.Text, sSerialNumber)))
                {
                    if (reader != null && reader.Read())
                    {
                        #region 组件处理状态
                        string boxCode = "";
                        if (reader.IsDBNull(4))
                            boxCode = "";
                        else
                            boxCode = reader.GetString(4).Trim();
                        if (boxCode != "T4")
                        {

                            //add by hexing 2013.11.18

                            if (boxCode == "T3")
                            {
                                DataTable dt = ProductStorageDAL.SNByReworkFromCenterDB(sSerialNumber);
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message74", "此组件为重工组件,漏打印名牌标签，请确认!:") + sSerialNumber, "ABNORMAL", lstView);
                                    tbBarCode.Clear();
                                    return false;
                                }
                                else
                                {
                                   #region dont care Process
                                   ToolsClass.ExecuteNonQuery(string.Format("update Product set Process = 'T4' where SN = '{0}' and Process = 'T3'",sSerialNumber));
                                   #endregion
                                	#region original Process Handling
//                                	ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message76", "此组件为在制组件,漏打印名牌标签，请确认!") + sSerialNumber, "ABNORMAL", lstView);
//                                    tbBarCode.Clear();
//                                    return false;
                                    #endregion
                                }
                            }
                            else
                            {


                                ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message75", "{0}处理状态为{1}不能打包"), sSerialNumber, boxCode), "ABNORMAL", lstView);
                                tbBarCode.Clear();
                                return false;
                            }
                        }
                        #endregion

                        #region 检查混装功率种数
                        string nPower = "0"; //标称功率
                        object pw = reader.GetDouble(3); //实测功率
                        nPower = ToolsClass.getIdealPower(reader.GetDouble(3), sSerialNumber.Trim());//标称功率（含档次）
                        _CartonPower = nPower.ToString();//标称功率 
                        if (nPower == "-1")
                        {
                            ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message38", "组件{0}: 功率{1} 超过功率设定范围. 这个组件不能打包."), sSerialNumber, reader.GetDouble(3).ToString()), "ABNORMAL", lstView);
                            tbBarCode.Clear();
                            return false;
                        }
                        else
                        {
                            if (this.nominalPower.Count == 0)
                            {
                                string[] array = nPower.ToString().Split('_');
                                if (array.Length > 1)
                                {
                                    this.tbNominalPowerGrade.Text = array[1].TrimEnd();
                                    this.tbNominalPower.Text = "";
                                    this.tbNominalPower.Text = array[0].ToString();
                                    if (!reader.IsDBNull(7))   
                                        this.txtRework.Text = Convert.ToString(reader.GetString(7).Trim());
                                    else
                                        this.txtRework.Text = "";
                                }
                                else
                                {
                                    this.tbNominalPowerGrade.Text = "";
                                    this.tbNominalPower.Text = "";
                                    this.tbNominalPower.Text = nPower.ToString();
                                    if (!reader.IsDBNull(7))
                                        this.txtRework.Text = Convert.ToString(reader.GetString(7).Trim());
                                    else
                                        this.txtRework.Text = "";
                                }

                                #region 获取标称功率档次是否显示
                                if (ToolsClass.getIsShowPowerGrade(reader.GetDouble(3), sSerialNumber.Trim()).Equals("ERROR"))
                                    return false;
                                else
                                    this.txtShowPowerGrade.Text = ToolsClass.getIsShowPowerGrade(reader.GetDouble(3), sSerialNumber.Trim());
                                #endregion

                                ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message39", "从第一个打包组件取得当前的包装箱标称功率为:{0} 标称功率的档次为：{1}"), this.tbNominalPower.Text.ToString(), this.tbNominalPowerGrade.Text), "NORMAL", lstView);
                            }
                        }
                        if (!checkPower(nPower, sSerialNumber))
                            return false;

                        #endregion

                        #region 检查组件型号，颜色
                        if (dataGridView1.DisplayedRowCount(true) == 0)
                        {
                            celltype = reader.GetString(1);
                        }
                        else
                        {
                            if (reader.GetString(1) != celltype)
                            {
                                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message40", "不同规格型号的组件不能混装!"), "ABNORMAL", lstView);
                                tbBarCode.Clear();
                                return false;
                            }
                        }

//                        if (!verifyCellColorandGetBoxID(sSerialNumber))
//                            return false; // jacky
                        #endregion
						// WorkOrder check jacky
                        #region workOrder Check
                        string workCode = "";
                        if (this.cbCheckSN)
                        {
                            if (!checkWorkCode(sSerialNumber, out workCode))
                            {
                                return false;
                            }
                        }
                        
//                        if(dataGridView1.Rows.Count > 0)
//                        {
//                        	if(string.IsNullOrEmpty(GetPoGroup(sSerialNumber,POGroup)))
//                        	{
//                        		ToolsClass.Log("Module:" + sSerialNumber + " Is Not Mixible with other Production Order" , "ABNORMAL", lstView);
//                        		return false;
//                        	}
//                        }
//                        else 
//                        {
//                        	POGroup = GetPoGroup(sSerialNumber);                        	
//                        	if (string.IsNullOrEmpty (POGroup))
//                        	{
//                        		ToolsClass.Log("Module:" + sSerialNumber + " PO:" + POGroup + " dose not exsit", "ABNORMAL", lstView);
//                        		return false;
//                        	}
//                	
//                       }
                        
                        #endregion

                        #region CenterDB
                        try
                        {
                            #region
                            string sqlQuery = @"
                                        WITH PRD AS
                                        (		
	                                        SELECT [SN],[ModelType],[Process],[BoxID],InterNewTempTableID,[Art_No] 
	                                        FROM [Product]  with(nolock)
	                                        Where [SN] ='{0}' 
		                                        AND [InterID] = (SELECT MAX(InterID) FROM [Product]  with(nolock) WHERE [SN] ='{0}')
                                        )
                                        SELECT *
                                        FROM PRD 
	                                        INNER JOIN [ElecParaTest] EPT ON
		                                        PRD.[InterNewTempTableID]=EPT.[InterID]";
                            sqlQuery = string.Format(sqlQuery, sSerialNumber);
                            DataSet ds = DbHelperSQL.Query(FormCover.connectionBase, sqlQuery, null);
                            if (ds != null && ds.Tables[0].Rows.Count > 0)
                            {
                                DataRow row = ds.Tables[0].Rows[0];
                                string module = row["SN"].ToString();
                                string workShop = ToolsClass.getConfig("WorkShop", false, "", "config.xml");
                                string isExist = new MODULEDAL().CheckRepeat(module, workShop);
                                if (isExist == "1")
                                {
                                    DataSet set = new MODULEDAL().GetInfoRepSN(module);
                                    if (set != null && set.Tables[0].Rows.Count > 0)
                                    {
                                        MessageBox.Show(string.Format(LanguageHelper.GetMessage(LanMessList, "Message41", "组件号{0}已存在{1}车间重复于箱号{2}"), module, set.Tables[0].Rows[0][0].ToString(), set.Tables[0].Rows[0][1].ToString()));
                                        //ToolsClass.Log("组件号" + module + "已存在" + set.Tables[0].Rows[0][0].ToString() + "车间重复于箱号" + set.Tables[0].Rows[0][1].ToString());
                                        ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message42", "组件号{0}已存在{1}车间重复于箱号{2}"), module, set.Tables[0].Rows[0][0].ToString(), set.Tables[0].Rows[0][1].ToString()));
                                    }
                                    else
                                    {
                                        MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message43", "组件号已存在或与其它车间重复:") + module);
                                       // ToolsClass.Log("组件号:" + module + " 已存在或与其它车间重复", "ABNORMAL", lstView);
                                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message44", "组件号已存在或与其它车间重复:") + module, "ABNORMAL", lstView);
                                    }

                                    return false;
                                }
                                if (isExist == "3")
                                {
                                     MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message45", "组件号已存在或与其它车间重复,请先拆包:") + module);
                                   // ToolsClass.Log("组件号:" + module + " 已存在或与其它车间重复", "ABNORMAL", lstView);
                                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message46", "组件号已存在或与其它车间重复,请先拆包:") + module, "ABNORMAL", lstView);
                                    return false;
                                }
                                if (isExist == "0" || isExist == "2")
                                {
                                    string currenttime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"); ;
                                    MODULE mo = new MODULE();
                                    mo.SYSID = Guid.NewGuid().ToString();
                                    if (Work_Shop.ToUpper().Equals("M08"))
                                        mo.SYSID = mo.SYSID + "-08";
                                    mo.MODULE_SN = module;
                                    mo.BARCODE = module;
                                    mo.WORKSHOP = workShop;
                                    mo.WORK_ORDER = "";// module.Substring(0, 9);
                                    mo.MODULE_TYPE = row["ModelType"].ToString();
                                    mo.CREATED_ON = currenttime; //可以理解为包装时间
                                    mo.CREATED_BY = FormCover.currUserName;
                                    mo.MODIFIED_BY = FormCover.currUserName;
                                    mo.MODIFIED_ON = currenttime;

                                    if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
                                       mo.RESV01 = row["Art_No"].ToString();

                                    string[] NewCartonPower = _CartonPower.Split('_');
                                    if (NewCartonPower.Length > 1)
                                    {
                                        mo.RESV02 = row["ModelType"].ToString().Insert(mo.MODULE_TYPE.IndexOf("-") + 1, NewCartonPower[0]);
                                        mo.RESV05 = NewCartonPower[0]; //标称功率
                                        mo.RESV04 = row["PMAX"].ToString();//实测功率
                                    }
                                    else
                                    {
                                        mo.RESV02 = row["ModelType"].ToString().Insert(mo.MODULE_TYPE.IndexOf("-") + 1, _CartonPower);
                                        mo.RESV05 = _CartonPower;//标称功率
                                        mo.RESV04 = row["PMAX"].ToString();//实测功率
                                    }

                                    mo.CustBoxID = this.tbCustBoxCode.Text.Trim();

                                    _ListModule.Add(mo);

                                    MODULE_TEST moTest = new MODULE_TEST();
                                    moTest.SYSID = Guid.NewGuid().ToString();
                                    if (Work_Shop.ToUpper().Equals("M08"))
                                        moTest.SYSID = moTest.SYSID + "-08";
                                    moTest.MODULE_SN = module;
                                    moTest.TEMP = row["Temp"].ToString();
                                    moTest.ISC = row["ISC"].ToString();
                                    moTest.VOC = row["VOC"].ToString();
                                    moTest.IMP = row["Im"].ToString();
                                    moTest.VMP = row["Vm"].ToString();
                                    moTest.FF = row["FF"].ToString();
                                    moTest.EFF = row["Eff"].ToString();
                                    moTest.PMAX = row["PMAX"].ToString();
                                    moTest.TEST_DATE = DateTime.Parse(row["TestDateTime"].ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff");
                                    moTest.CREATED_NO = currenttime;
                                    moTest.CREATED_BY = FormCover.currUserName;
                                    moTest.MODIFIED_ON = DateTime.Parse(row["CheckDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff");
                                    moTest.MODIFIED_BY = FormCover.currUserName;
                                    _ListModuleTest.Add(moTest);
                                }
                                //if (isExist == "2")
                                //{
                                //    MODULE_TEST moTest = new MODULE_TEST();
                                //    moTest.SYSID = Guid.NewGuid().ToString();
                                //    if (Work_Shop.ToUpper().Equals("M08"))
                                //        moTest.SYSID = moTest.SYSID + "-08";
                                //    moTest.MODULE_SN = module;
                                //    moTest.TEMP = row["Temp"].ToString();
                                //    moTest.ISC = row["ISC"].ToString();
                                //    moTest.VOC = row["VOC"].ToString();
                                //    moTest.IMP = row["Im"].ToString();
                                //    moTest.VMP = row["Vm"].ToString();
                                //    moTest.FF = row["FF"].ToString();
                                //    moTest.EFF = row["Eff"].ToString();
                                //    moTest.PMAX = row["PMAX"].ToString();
                                //    moTest.TEST_DATE = DateTime.Parse(row["TestDateTime"].ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff");
                                //    moTest.CREATED_NO = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                //    moTest.CREATED_BY = FormCover.currUserName;
                                //    moTest.MODIFIED_ON = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                //    moTest.MODIFIED_BY = FormCover.currUserName;
                                //    _ListModuleTest.Add(moTest);

                                //    string sqlQuerySNId = string.Format("SELECT SYSID FROM T_MODULE where MODULE_SN='{0}'", module);
                                //    object obj = DbHelperSQL.ExecuteScalar(sqlQuerySNId, null);
                                //    MODULE mo = new MODULE();
                                //    if (obj != null && obj != DBNull.Value)
                                //    {
                                //        mo.SYSID = obj.ToString();
                                //        mo.REMARK = "UnPacking";
                                //        mo.MODULE_SN = sSerialNumber;
                                //        _ListModule.Add(mo);
                                //    }
                                //    else
                                //    {
                                //        MessageBox.Show("组件未找到：" + sSerialNumber);
                                //    }
                                //}
                            }
                            #endregion
                        }
                        catch (Exception exc)
                        {
                            ToolsClass.Log(exc.Message, "ABNORMAL", lstView);
                        }
                        #endregion

                        #region 添加组件到list列表
                        string nGrade = "";
                        string[] nPowerArrays = nPower.Split('_');
                        if (nPowerArrays.Length > 1)
                        {
                            nPower = nPowerArrays[0];
                            nGrade = nPowerArrays[1].ToString().Trim();
                        }

                        this.dataGridView1.Rows.Add();
                        this.dataGridView1.Rows[this.dataGridView1.Rows.Count-1].Cells["Column1"].Value = reader.GetValue(0);
                        this.dataGridView1.Rows[this.dataGridView1.Rows.Count-1].Cells["Column2"].Value = reader.GetValue(1);
                        this.dataGridView1.Rows[this.dataGridView1.Rows.Count-1].Cells["Column3"].Value = reader.GetValue(2);
                        this.dataGridView1.Rows[this.dataGridView1.Rows.Count-1].Cells["Column4"].Value = reader.GetValue(3);
                        this.dataGridView1.Rows[this.dataGridView1.Rows.Count-1].Cells["Column5"].Value = nPower;
                        this.dataGridView1.Rows[this.dataGridView1.Rows.Count-1].Cells["Column8"].Value = nGrade;
                        this.dataGridView1.Rows[this.dataGridView1.Rows.Count-1].Cells["Column6"].Value = reader.GetValue(4);
                        this.dataGridView1.Rows[this.dataGridView1.Rows.Count-1].Cells["Column7"].Value = workCode;
                        this.dataGridView1.Rows[this.dataGridView1.Rows.Count-1].Cells["Column10"].Value = reader.GetValue(6);
                        this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column11"].Value = reader.GetValue(7);
                        this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column13"].Value = ProductStorageDAL.GetSNImByFromPackingDB(sSerialNumber);
                        DataGridViewComboBoxColumn cb;
                        cb = (DataGridViewComboBoxColumn)dataGridView1.Columns["Column12"];
                        cb.DataSource = DtModuleClass;
                        cb.DisplayMember = "MAPPING_KEY_01";
                        cb.ValueMember = "MAPPING_KEY_01";
                        this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Cells["Column12"].Value = sModuleClass;
                        this.dataGridView1.Columns["Column12"].ReadOnly = !this.CheckModuleClass;
 
                        //dataGridView1.Rows.Insert(dataGridView1.Rows.Count, new object[] { reader.GetValue(0), reader.GetValue(1), 
                        //    reader.GetValue(2), reader.GetValue(3), nPower, nGrade, reader.GetValue(4), workCode, reader.GetValue(6), reader.GetValue(7),sModuleClass});
                        ToolsClass.Log("Adding module:" + reader.GetValue(0).ToString() + " to list", "NORMAL", lstView);
                        #endregion
                    }
                    else
                    {
                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message48", "组件号码不存在:") + sSerialNumber, "ABNORMAL", lstView);
                        tbBarCode.Clear();
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ToolsClass.Log(ex.Message, "ABNORMAL", lstView);
                return false; 
            }
        }

        /// <summary>
        /// 打印单个组件的标签
        /// </summary>
        /// <param name="i"></param>
        private void printLabel(int i = 0)
        {
            string model = "";
            if (this.rbModel)
            {
                model = dataGridView1[1, i].Value.ToString();
                int location = model.IndexOf('-');
                if (location == -1)
                {
                    model = model + "-" + dataGridView1[4, i].Value.ToString();
                }
                else
                {
                    model = model.Insert(location + 1, dataGridView1[4, i].Value.ToString());
                }
            }
            else if (this.rbTrueP)
            {
                model = dataGridView1[3, i].Value.ToString();
            }
            else
            {
                model = dataGridView1[4, i].Value.ToString();
            }
            if (!ToolsClass.bChipType)
                model = model.Substring(0, model.Length - 1);
            this.sModel = model;

            this.sSN = dataGridView1[0, i].Value.ToString();

            string sModelType = "";
            if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
                sModelType = dataGridView1[1, i].Value.ToString().Replace(" ", "-").Insert(dataGridView1[1, i].Value.ToString().Replace(" ", "-").IndexOf('-') + 1, dataGridView1[4, i].Value.ToString());
            else
                sModelType = dataGridView1[1, i].Value.ToString().Insert(dataGridView1[1, i].Value.ToString().IndexOf('-') + 1, dataGridView1[4, i].Value.ToString());//dataGridView1[1, i].Value.ToString().Substring(0, dataGridView1[1, i].Value.ToString().IndexOf('-') + 1) + dataGridView1[4, i].Value.ToString();

            dictLabelInfo(sModelType);
            string returnMsg = LablePrint.printingLable(this.sModulLabePath, labelValue, this.printNum);
            if (returnMsg.Length > 0)
            {
                ToolsClass.Log("Packaging data is saved, Module label print Failed:" + returnMsg, "ABNORMAL", lstView);
                return;
            }
        }

        /// <summary>
        /// 验证组件是否已经打包或箱号是否被使用
        /// </summary>
        /// <param name="sSerialNumber"></param>
        /// <param name="IsBoxOrModule">true：Box，False：Module</param>
        /// <returns></returns>
        private bool isPackage(string sParameter, bool IsBoxOrModule = true)
        {
            try
            {
                #region
                string sql = "";
                if (IsBoxOrModule)
                    sql = "select BoxID from Product with(nolock) where BoxID='{0}'"; //Jacky
                else
                    sql = "select BoxID from Product with(nolock) where SN='{0}' and InterID = (select max(InterID) from Product with(nolock) where SN ='{0}')";

                string sBoxID = "";
                sql = string.Format(sql, sParameter);
                SqlDataReader sdr = ToolsClass.GetDataReader(sql);
                while (sdr != null && sdr.Read())
                {
                    if (IsBoxOrModule)
                    {
                        return false;
                    }
                    else
                    {
                        sBoxID = sdr.GetString(0);
                        if (!sBoxID.Trim().Equals(""))
                        {
                            ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message50", "{0}已经打包在:{1}"), sParameter, sBoxID), "ABNORMAL", lstView);
                            return false;
                        }
                    }
                }
                sdr.Close(); sdr = null;
                return true;
                #endregion
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
        private string GetmoduleClass(string SN)
        {
        	string sql = @"select [ModuleClass] from [QualityJudge] where [SN] = '"+SN + "' order by timestamp desc";
        	string Class = "";
        	SqlDataReader reader = ToolsClass.GetDataReader(sql,FormCover.ConnectionOldPackaging);
        	if(reader != null && reader.Read())
        		return reader.GetString(0);
        	return "A";
        	
        }
        private void tbBoxCode_TextChanged(object sender, EventArgs e)
        {
            //if (tbBoxCode.Text.Trim().Length >= 10 && !sTempBoxID.Equals(tbBoxCode.Text.Trim()))
            //{
            //    int j = 0;
            //    //if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
            //    //    j = int.Parse(ToolsClass.getConfig("SchSerialCode"));
            //    //else
            //        j = int.Parse(ToolsClass.getConfig("SerialCode"));
            //    int mm = j.ToString().Length;//流水码长度
            //    string strsql = "update Box set IsUsed='N' where BoxID='" + sTempBoxID + "'";
            //    int iSql = ToolsClass.ExecuteNonQuery(strsql);
            //    //j--;

            //    //if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
            //    //    ToolsClass.saveXMLNode("SchSerialCode", j.ToString());
            //    //else
            //    try
            //    {
            //        if ((iSql > 0) && (Convert.ToInt64(tbBarCode.Text.Trim().Substring(5, mm)) <= j))
            //        {
            //            j--;
            //            ToolsClass.saveXMLNode("SerialCode", j.ToString());
            //        }
            //    }
            //    catch
            //    { }
            //}
        }

        private void tbCustBoxCode_TextChanged(object sender, EventArgs e)
        {
            //if (tbCustBoxCode.Text.Trim().Length >= 15 && !custBoxID.Equals(tbCustBoxCode.Text.Trim()))
            //{
            //    int j = 0;
            //    //if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
            //        j = int.Parse(ToolsClass.getConfig("SchSerialCode"));
            //    //else
            //    //    j = int.Parse(ToolsClass.getConfig("SerialCode"));

            //    int mm = j.ToString().Length;//流水码长度
            //    string strsql = "update Box set IsUsed='N' where BoxID='" + custBoxID + "'";
            //    int iSql = ToolsClass.ExecuteNonQuery(strsql);
            //    //j--;

            //    //if (ToolsClass.Like(this.sModulLabePath, "Sch*co*"))
            //    try
            //    {
            //        if ((iSql > 0) && (Convert.ToInt64(tbBarCode.Text.Trim().Substring(5, mm)) <= j))
            //        {
            //            j--;
            //            ToolsClass.saveXMLNode("SchSerialCode", j.ToString());
            //        }
            //    }
            //    catch
            //    { }
            //    //else
            //    //    ToolsClass.saveXMLNode("SerialCode", j.ToString());
            //}
        }

        #region 不在使用的事件
        private void cbMergeBox_CheckedChanged(object sender, EventArgs e)
        {
            //if (cbMergeBox.Checked)
            //{
            //    lblWS.Enabled = true;
            //    lblWSFrom.Enabled = true;
            //    cmbWorkShop.Enabled = true;

            //    if(cmbWorkShop.Text.Length>0)
            //        FormCover.connectionBase = ToolsClass.getConfig(cmbWorkShop.Text, false, "", "config.xml");
            //}
            //else
            //{
            //    lblWS.Enabled = false;
            //    lblWSFrom.Enabled = false;
            //    cmbWorkShop.Text = "";
            //    cmbWorkShop.Enabled = false;
            //    FormCover.connectionBase = CurrentWorkShopDBconn;
            //}
        }

        private void cmbWorkShop_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cmbWorkShop.Text.Length > 0)
            //    FormCover.connectionBase = ToolsClass.getConfig(cmbWorkShop.Text, false, "", "config.xml");
        }
        #endregion
        private void cbPatternFlag_CheckedChanged(object sender, EventArgs e)
        {
            if (cbPatternFlag.Checked)
                IsCheckPattern = true;
            else
                IsCheckPattern = false;
        }

        /// <summary>
        /// 可以CTRL+C复制
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (lstView.SelectedItems.Count > 0)
                {
                     if (lstView.SelectedItems[0].Text != "")
                        Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
                }
            }
        }

        private void chbCarton_CheckedChanged(object sender, EventArgs e)
        {
            this.tbBoxCode.Enabled = this.chbCarton.Checked;
        }

        private void chbCustCarton_CheckedChanged(object sender, EventArgs e)
        {
            this.tbCustBoxCode.Enabled = this.chbCustCarton.Checked;
        }
        
         // Get the weight
        private string getWeight(string type, string grossOrNet, int quantity)
        {
        	//type = type.Substring(0,4);
        	double b = double.Parse(ToolsClass.getConfig(type + "Base" + grossOrNet + "Weight", false, "", "config.xml"));
        	//string us = ToolsClass.getConfig(type + "Unit" + grossOrNet + "Weight", false, "", "config.xml");
        	double u = double.Parse(ToolsClass.getConfig(type + "Unit" + grossOrNet + "Weight", false, "", "config.xml"));
        	return (b + u*quantity).ToString() + " kg";
        }
        
        
        //Get the dimensions
        private string getDimensions(string type, int quantity)
        {
        	//type = type.Substring(0,4);
        	int b = int.Parse(ToolsClass.getConfig(type + "Base" + "Height", false, "", "config.xml"));
        	int u = int.Parse(ToolsClass.getConfig(type + "Unit" + "Height", false, "", "config.xml"));
        	string length = ToolsClass.getConfig(type + "Length", false, "", "config.xml");
        	string width = ToolsClass.getConfig(type + "Width", false, "", "config.xml");
        	return length + "X" + width + "X" +(b + u*quantity).ToString() + " mm";
        }

        //JACKY PRODUCTION ORDER MIXING
        private string GetPoGroup (string SN, string Group = "")        	
        {
        	string ConnDB = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", FormCover.DBServer, "LabelPrintDB", FormCover.DBUser, FormCover.DBPWD);
        	string sql = "";
        	string line = SN.Substring(5,2);
        	switch(line)
        	{
        		case "41":
        			line = "A";
        			break;
        		case "42":
        			line = "B";
        			break;
        		case "43":
					line = "C";
					break;
				default:
					line = "";
					break;					
        	}
        	if(line.Equals(""))
        	{
        		MessageBox.Show("Error");
        		return null;
        	}
        	string Temp_Mo = "_"+ line + DateTime.Now.Year.ToString().Substring(2,2) + "____" + SN.Substring(7,3); // might need to change when PO changeds
        	
        	try
        	{
        		if(Group == "")
        		{
        			sql = @"select PO_Group from ProductionOrder_Mixing_Rule where ProductionOrder like '{0}'";
        			sql = string.Format(sql,Temp_Mo);        		
        		}
        		else
        		{
        			sql = @"select PO_Group from ProductionOrder_Mixing_Rule where ProductionOrder like '{0}' and PO_Group = '{1}'";
        			sql = string.Format(sql,Temp_Mo,Group);
   
        		}
        		 SqlDataReader sdr = ToolsClass.GetDataReader(sql,ConnDB);
        		if(sdr != null && sdr.Read())
        		{

        			return sdr.GetString(0);        			
	            }
        		else 
        			return null;      		 
        		
        		
        	}
        	catch (Exception e)
        	{
        		throw e;
        	}
        }
        
        private string getPOsequence( string serial, out string line)
        {
        	string sql = @"select po from PO_Modification where InterID = (select MAX(InterID) from PO_Modification where sn = '"+serial+"')";
        	string sequence = "";
        	line = "";
        	SqlDataReader rd = ToolsClass.GetDataReader(sql);
        	if(rd!=null && rd.Read())
        	{
        		sequence = rd.GetString(0).Substring(6,3);
        		line = rd.GetString(0).Substring(1,1);
        	}
        	else 
        	{
        		sequence = serial.Substring(7,3);
        		int lineNumber = Convert.ToInt32(serial.ToString().Substring(5,2));
				if(lineNumber > 9 && lineNumber < 20 )
					line = "A";
				else if(lineNumber > 19 && lineNumber < 30 )
					line = "B";
				else if(lineNumber > 29 && lineNumber < 40 )
					line = "C";
				else if(lineNumber > 39 && lineNumber < 50 )
					line = "D";
        	}
        	
        	return sequence;
        }
        
         
        void Button1Click(object sender, EventArgs e)
        {
        	
        		if(FormCover.HasPowerControl("BtReprintCartonLabel"))
        		panel7.Visible = true;
        }
        #region not in use
//        void BtP_PrintClick(object sender, EventArgs e)
//        {
//        
//        	    string Certificate ="";
//            	int sheetNo = 0;
//            	Certificate = ToolsClass.getConfig("Certificate");
//            	sheetNo = (Certificate == "Dual")?2:3;
//            	
//        	this.PackingType = ToolsClass.getConfig("PType");
//        	this.Cell_Vendor.Text = ToolsClass.getConfig("CellVendor");
//        	string sql = "";
//        	sql = @"Select distinct SN,CellColor,ModelType,StdPower,ModuleClass,CellSpec,Pattern From Product where BoxId = '{0}'";
//        	sql = string.Format(sql,TbCartonReprint.Text.ToString());
//        	DataTable DTSN = ToolsClass.GetDataTable(sql);
//        	if(DTSN.Rows.Count < 1)
//        	{
//        		MessageBox.Show("Carton Number does not exist, please confirm!");
//        		clearPanel7();
//        		return;
//        	}       	
//        	List<string> lsSN = new List<string>();
//        	foreach (DataRow row in DTSN.Rows) {
//        		lsSN.Add(row[0].ToString());
//        	}
//        	  #region"常熟列印大标签"
//                  if (PackingType == "Horizontal" )
//          		  {
//                  	if(lsSN.Count > 22)
//                  	{
//                  		MessageBox.Show("Please Verify the print QTY(max 22 pcs)!");
//                  		return;
//                  	}
//                  	// Added by Shen Yu on July 18, 2011 for Large Label Printing
//	            		if(this.sCartonLabelPath.Length > 0 && this.rbLargeLabelPrint == true)
//		            {
//		            			string Line = "";
//	            				string sequence = getPOsequence(lsSN[0],out Line);
//					        	
////					        	int lineNumber = Convert.ToInt32(lsSN[0].Substring(5,2));
////					        	if(lineNumber > 9 && lineNumber < 20 )
////					        		Line = "A";
////					        	else if(lineNumber > 19 && lineNumber < 30 )
////					        		Line = "B";
////					        	else if(lineNumber > 29 && lineNumber < 40 )
////					        		Line = "C";
////					        	else if(lineNumber > 39 && lineNumber < 50 )
////					        		Line = "D";	
////	            			
//	            			
//	            				string lid = "";
//	            				sql = @" Select top 1 [Resv01],[Resv02],[Resv03],[Resv04] from TSAP_WORKORDER_SALES_REQUEST where orderno like '{0}' order by CreatedOn desc";
//	                    		                       	
//					        	string syr = DateTime.Now.Year.ToString().Substring(2,2);
//					        	string sMth  = DateTime.Now.Month.ToString().PadLeft(2,'0');
//					        	string Market ="",CableLength ="",Connector = "";
//	                    		sql = string.Format(sql,"C"+Line+ syr+"__"+ sequence);
//	                    		SqlDataReader reader = ToolsClass.GetDataReader(sql,FormCover.InterfaceConnString);	                    	
//		                    	while(reader.Read())
//		                    	{
//		                    		Market = reader.GetString(0);
//			                    	lid = reader.GetString(1);
//			                    	CableLength = reader.GetString(2);
//			                    	Connector = reader.GetString(3);
//			                    	
//		                    	}
//		                    	if(string.IsNullOrEmpty(Market)||string.IsNullOrEmpty(lid)||string.IsNullOrEmpty(CableLength)||string.IsNullOrEmpty(Connector))
//		                    	{
//		                    		sql = @"select [Market],[Degradation],[Cable_Length],[Connector] from [ProductionOrder_Mixing_Rule] where [ProductionOrder] like '{0}'";
//		                    		sql = string.Format(sql,"C"+Line+ syr+"__"+ sequence);
//		                    		reader  = ToolsClass.GetDataReader(sql);
//		                    		while(reader.Read())
//			                    	{
//				                    	Market = reader.GetString(0);
//				                    	lid = reader.GetString(1);
//				                    	CableLength = reader.GetString(2);
//				                    	Connector = reader.GetString(3);
//			                    	}
//		                    	}
//	            			
//	            		smallLargeLabel = new LargeLabelH();
//		            	smallLargeLabel.packingDate = DateTime.Now.ToString("yyyy-MMM-dd");
//		            	smallLargeLabel.cartonNumber = TbCartonReprint.Text.ToString();
//		            	smallLargeLabel.colour = DTSN.Rows[0][1].ToString();
//		            	smallLargeLabel.model = DTSN.Rows[0][2].ToString();
//		            	smallLargeLabel.power = DTSN.Rows[0][3].ToString();
//		              	//smallLargeLabel.materialCode = "Jacky";// decision not made yet
//		              	smallLargeLabel.LID = lid;// 
//		              	smallLargeLabel.quantity = lsSN.Count.ToString();
//		              	smallLargeLabel.grossweight = getWeight (DTSN.Rows[0][2].ToString(),"Gross", lsSN.Count);
//		              	smallLargeLabel.netweight = getWeight (DTSN.Rows[0][2].ToString(), "Net", lsSN.Count);
//		              	smallLargeLabel.size = getDimensions (DTSN.Rows[0][2].ToString(), lsSN.Count);
//						//smallLargeLabel.CassCode = GetCassCode(tbMaterialCode.Text.Trim());
//						#region ADDING J-box type, Cable Length, Connector type,  bus bar 
//		                sql = @"select Busbar from ProductionOrder_Mixing_Rule where ProductionOrder like '{0}'";
//		                sql = string.Format(sql,"C"+Line+ syr+"__"+ sequence);
//		                reader  = ToolsClass.GetDataReader(sql);
//		                    if(reader.Read() && reader!=null)
//		                    	smallLargeLabel.BusBar = reader.GetString(0);
//		                smallLargeLabel.CableLength = CableLength;
//		              	smallLargeLabel.ConnectorType = Connector;
//		              	sql = @"select * from T_WORKORDER_BOM where [MaterialDescription] like '%gb%' and [MaterialGroup] = 1004 and [OrderNo] like '{0}'";
//		              	sql = string.Format(sql,"C"+Line+ syr+"__"+ sequence);
//		              	reader  = ToolsClass.GetDataReader(sql);
//		              	smallLargeLabel.JboxType = (reader.Read() && reader!=null)?"CF1108-03gb":"CF1108-03";
//		                #endregion
//					
//					
//									
//						//smallLargeLabel.loadID = loadID;	
//			            if(smallLargeLabel.power != "Mixed")
//			            {
//			            	smallLargeLabel.power = smallLargeLabel.power + "W";
//			            }
//			            smallLargeLabel.cartonclass = DTSN.Rows[0][4].ToString();
//		            	     
//		            	if(smallLargeLabel.model[smallLargeLabel.model.Length-1] == 'M')
//		            	{
//		            		smallLargeLabel.cellType = "Mono";
//		            	}
//		            	if(smallLargeLabel.model[smallLargeLabel.model.Length-1] == 'P' ||smallLargeLabel.model[smallLargeLabel.model.Length-1] == 'N' )
//		            	{
//		            		smallLargeLabel.cellType = "Poly";
//		            	}
//		                for (int i = 0; i < lsSN.Count; i++)
//		            	{
//		                	smallLargeLabel.arraylist.Add(lsSN[i]);
//		            	}	            
//		            
//			            csLabel = new CSLargeLabel("", this.sCartonLabelPath);
//	                    csLabel.Write2ExcelH(this.iLabelH, iLabelV, smallLargeLabel,sheetNo);
//		          	  }
//            		}       
//	
//                  else if(PackingType == "Vertical")
//                  {
//	                   	if(lsSN.Count > 26)
//                  	{
//                  		MessageBox.Show("Print Qty grater than 26, Please confirm!");
//                  		return;
//                  	}
//
//		                  	if (this.sCartonLabelPath.Length > 0 && this.rbLargeLabelPrint == true)
//	                    {
//	                    	
//	                    	if (this.sCartonLabelPath.IndexOf(".xls") > 0)
//	                        {
//	                            largeLabel = new LargeLabel();
//	                            largeLabel.CartonNumber = TbCartonReprint.Text;
//	                            largeLabel.Colour = DTSN.Rows[0][1].ToString();
//	                            largeLabel.Model = DTSN.Rows[0][2].ToString();
//	                            if(string.IsNullOrEmpty(Cell_Vendor.Text.ToString()))
//	                            	largeLabel.CellVendor  = "";
//	                            else
//	                            	largeLabel.CellVendor = "Assembled in Canada with "+ Cell_Vendor.Text.ToString()+" cells";
//	                            string Line = "";	                            
//	                            string sequence = getPOsequence(lsSN[0],out Line);
//	                    		
//	                          	sql = @"select System_Voltage from ProductionOrder_Mixing_Rule where ProductionOrder like '{0}' "; 
//					        	
////					        	int lineNumber = Convert.ToInt32(lsSN[0].Substring(5,2));
////					        	if(lineNumber > 9 && lineNumber < 20 )
////					        		Line = "A";
////					        	else if(lineNumber > 19 && lineNumber < 30 )
////					        		Line = "B";
////					        	else if(lineNumber > 29 && lineNumber < 40 )
////					        		Line = "C";
////					        	else if(lineNumber > 39 && lineNumber < 50 )
////					        		Line = "D";
//						                          	
//	                          	
//	                          	string syr = DateTime.Now.Year.ToString().Substring(2,2);
//					        	string sMth  = DateTime.Now.Month.ToString().PadLeft(2,'0');
//					        	
//					        	//sql = string.Format(sql,"__"+ syr + sMth + sequence );
//					        	sql = string.Format(sql,"C"+ Line + syr + "__" + sequence );
//					        	
//					        	DataTable dt1 = ToolsClass.GetDataTable(sql);
//					        	if(dt1.Rows.Count > 0)					        		
//					        		largeLabel.Voltage = dt1.Rows[0][0].ToString();
//					        	
//	                            sql = @" Select top 1 [Resv01],[Resv02],[Resv03],[Resv04] from TSAP_WORKORDER_SALES_REQUEST where orderno like '{0}' order by CreatedOn desc";
//	                           
//	                    		
//	                    		sql = string.Format(sql,"C"+Line+ syr+"__"+ sequence);
//	                    		SqlDataReader reader = ToolsClass.GetDataReader(sql,FormCover.InterfaceConnString);	                    	
//		                    	while(reader.Read())
//		                    	{
//			                    	largeLabel.Market = reader.GetString(0);
//			                    	largeLabel.LID = reader.GetString(1);
//			                    	largeLabel.Cable = reader.GetString(2);
//			                    	largeLabel.Connector = reader.GetString(3);
//		                    	}
//		                    	if(string.IsNullOrEmpty(largeLabel.Market)||string.IsNullOrEmpty(largeLabel.LID )||string.IsNullOrEmpty(largeLabel.Cable)||string.IsNullOrEmpty(largeLabel.Connector))
//		                    	{
//		                    		sql = @"select [Market],[Degradation],[Cable_Length],[Connector] from [ProductionOrder_Mixing_Rule] where [ProductionOrder] like '{0}'";
//		                    		sql = string.Format(sql,"C"+Line+ syr+"__"+ sequence);
//		                    		reader  = ToolsClass.GetDataReader(sql);
//		                    		while(reader.Read())
//			                    	{
//				                    	//largeLabel.Market = reader.GetString(0);
//				                    	largeLabel.LID  = reader.GetString(1);
//				                    	largeLabel.Cable = reader.GetString(2);
//				                    	largeLabel.Connector = reader.GetString(3);
//			                    	}
//		                    	}
//		                    	
//		                    	
//		                    	sql = @"select BusBar,Frame_Color From ProductionOrder_Mixing_Rule where ProductionOrder like '{0}'";
//		                    	sql = string.Format(sql,"C"+ Line + syr +"__"+ sequence);
//		                    	reader = ToolsClass.GetDataReader(sql);
//		                    	while(reader.Read())
//		                    	{
//		                    		largeLabel.BusBar = reader.GetString(0);
//		                    		TbBusBar.Text = reader.GetString(0);
//		                    		largeLabel.FrameColor = reader.GetString(1);
//		                    	}
//		                    
//		                    	largeLabel.Class = DTSN.Rows[0][4].ToString();
//								
//								largeLabel.Quantity = lsSN.Count;
//								if(ToolsClass.getConfig("GlassType") == "PN")
//								{
//									largeLabel.GrossWeight = "728";
//		              				largeLabel.NetWeight = "655";
//								}
//								else
//								{
//									largeLabel.GrossWeight = getWeight (DTSN.Rows[0][2].ToString(),"Gross", lsSN.Count);
//		              				largeLabel.NetWeight = getWeight (DTSN.Rows[0][2].ToString(), "Net", lsSN.Count);
//								}
//								
//				              	largeLabel.Volumn = getDimensions (DTSN.Rows[0][2].ToString(), lsSN.Count);
//	                            
//	                          
//	                            largeLabel.Power = DTSN.Rows[0][3].ToString();
//	                            largeLabel.PowerGrade = "";
//	                            
//	
//	                            largeLabel.Size = DTSN.Rows[0][5].ToString();
//	                            largeLabel.Pattern = DTSN.Rows[0][6].ToString();
//	
//	                            if (largeLabel.Model[largeLabel.Model.Length - 1] == 'M')
//	                            {
//	                                largeLabel.CellType = "Mono";
//	                                largeLabel.IsHavePattern = false;
//	                            }
//	                            if (largeLabel.Model[largeLabel.Model.Length - 1] == 'P' || largeLabel.Model[largeLabel.Model.Length - 1] == 'N' )
//	                            {
//	                                largeLabel.CellType = "Poly";
//	                                if (!string.IsNullOrEmpty(sPattern) && IsCheckPattern == true)
//	                                    largeLabel.IsHavePattern = true;
//	                            }
//	
//	                            #region 竖包装标签打印测试信心拼List数据
//	                            List<List<string>> SerialNumberListTestData = new List<List<string>>();
//	                            for (int i = 0; i < lsSN.Count; i++)
//	                            {
//	                                List<string> SerialNumberTestData = new List<string>();
//	                                DataTable dt = ProductStorageDAL.GetFTData(lsSN[i]);
//	                                if (dt != null && dt.Rows.Count > 0)
//	                                {
//	                                    DataRow row = dt.Rows[0];
//	                                    SerialNumberTestData.Add(Convert.ToString(row["SN"]));//组件号                                   
//	                                    SerialNumberTestData.Add(ProductStorageDAL.GetDecimalString(Convert.ToString(row["ISC"]), 2)); //工作电流                                      
//	                                    SerialNumberTestData.Add(ProductStorageDAL.GetDecimalString(Convert.ToString(row["VOC"]), 2));//工作电压 
//	                                    SerialNumberTestData.Add(ProductStorageDAL.GetDecimalString(Convert.ToString(row["Im"]), 2)); //开路 电流
//	                                    SerialNumberTestData.Add(ProductStorageDAL.GetDecimalString(Convert.ToString(row["Vm"]), 2)); //开路 电流
//	                                    SerialNumberTestData.Add(ProductStorageDAL.GetDecimalString(Convert.ToString(row["Pmax"]), 1));//实测功率 
//	                                    SerialNumberTestData.Add(Convert.ToString(row["StdPower"]));//标称功率        
//	                                }
//	                                else
//	                                {
//	                                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message11", "列印竖包装标签时，此组件的测试数据不全:") + dataGridView1[0, i].Value.ToString(), "ABNORMAL", lstView);
//                                   		break;
//	                                }
//	                                SerialNumberListTestData.Add(SerialNumberTestData);
//	                                //largeLabel.Arraylist.Add(dataGridView1[0, i].Value.ToString());
//	                            }
//	 
//	                            if (SerialNumberListTestData.Count != lsSN.Count)
//	                                return;
//	
//	                            largeLabel.ListInfo = SerialNumberListTestData;
//	
//	                            #endregion
//	
//	                            largeLabel.IsShowPowerGrade = Convert.ToString(this.txtShowPowerGrade.Text.Trim());
//	                           // csLabel = new CSLargeLabel("", this.sCartonLabelPath);
//	                            csLabel = new CSLargeLabel("", this.sCartonLabelPath);
//	                            csLabel.Write2Excel(this.iLabelH-2, iLabelV, largeLabel);
//	                          
//	                        }
//	                        else
//	                        {
//	                           ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message12", "竖包装标签选择不正确，请选择【Excel】格式的标签文件！"), "ABNORMAL", lstView);
//                           		 return;
//	                        }
//	                    }
//                  }
//                  #endregion
//        }
        #endregion
        void BtP_PrintClick(object sender, EventArgs e)
        {
        	string strTemp = TbCartonReprint.Text.ToString();
                    string[] sDataSet = strTemp.Split('\n');
                    for (int i = 0; i < sDataSet.Length; i++)
                    {
                        sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                           ReprintBatch(sDataSet[i]);
                    }
        }
        
        void ReprintBatch(string cartonno)
        {
        	    string Certificate ="";
            	int sheetNo = 0;
            	Certificate = ToolsClass.getConfig("Certificate");
            	sheetNo = (Certificate == "Dual")?2:3;
            	
        	this.PackingType = ToolsClass.getConfig("PType");
        	this.Cell_Vendor.Text = ToolsClass.getConfig("CellVendor");
        	string sql = "";
        	sql = @"Select distinct SN,CellColor,ModelType,StdPower,ModuleClass,CellSpec,Pattern From Product where BoxId = '{0}'";
        	sql = string.Format(sql,cartonno);
        	DataTable DTSN = ToolsClass.GetDataTable(sql);
        	if(DTSN.Rows.Count < 1)
        	{
        		MessageBox.Show("Carton Number does not exist, please confirm!");
        		clearPanel7();
        		return;
        	}       	
        	List<string> lsSN = new List<string>();
        	foreach (DataRow row in DTSN.Rows) {
        		lsSN.Add(row[0].ToString());
        	}
        	  #region"常熟列印大标签"
                  if (PackingType == "Horizontal" )
          		  {
                  	if(lsSN.Count > 22)
                  	{
                  		MessageBox.Show("Please Verify the print QTY(max 22 pcs)!");
                  		return;
                  	}
                  	// Added by Shen Yu on July 18, 2011 for Large Label Printing
	            		if(this.sCartonLabelPath.Length > 0 && this.rbLargeLabelPrint == true)
		            {
	            			string PO = GetPONumber(lsSN[0]);
	            			string line = PO.Substring(1,1);
	            			string sequence = PO.Substring(6,3);
	            				
	            				
					        	
//					        	int lineNumber = Convert.ToInt32(lsSN[0].Substring(5,2));
//					        	if(lineNumber > 9 && lineNumber < 20 )
//					        		Line = "A";
//					        	else if(lineNumber > 19 && lineNumber < 30 )
//					        		Line = "B";
//					        	else if(lineNumber > 29 && lineNumber < 40 )
//					        		Line = "C";
//					        	else if(lineNumber > 39 && lineNumber < 50 )
//					        		Line = "D";	
//	            			
	            			
	            				string lid = "";
	            				sql = @" Select top 1 [Resv01],[Resv02],[Resv03],[Resv04] from TSAP_WORKORDER_SALES_REQUEST where orderno like '{0}' order by CreatedOn desc";
	                    		                       	
	            				string syr = lsSN[0].Substring(1,2);
					        	//string sMth  = DateTime.Now.Month.ToString().PadLeft(2,'0');
					        	string Market ="",CableLength ="",Connector = "";
	                    		sql = string.Format(sql,"C"+line+ syr+"__"+ sequence);
	                    		SqlDataReader reader = ToolsClass.GetDataReader(sql,FormCover.InterfaceConnString);	                    	
		                    	while(reader.Read())
		                    	{
		                    		Market = reader.GetString(0);
			                    	lid = reader.GetString(1);
			                    	CableLength = reader.GetString(2);
			                    	Connector = reader.GetString(3);
			                    	
		                    	}
		                    	if(string.IsNullOrEmpty(Market)||string.IsNullOrEmpty(lid)||string.IsNullOrEmpty(CableLength)||string.IsNullOrEmpty(Connector))
		                    	{
		                    		sql = @"select [Market],[Degradation],[Cable_Length],[Connector] from [ProductionOrder_Mixing_Rule] where [ProductionOrder] like '{0}'";
		                    		sql = string.Format(sql,"C"+line+ syr+"__"+ sequence);
		                    		reader  = ToolsClass.GetDataReader(sql);
		                    		while(reader.Read())
			                    	{
				                    	Market = reader.GetString(0);
				                    	lid = reader.GetString(1);
				                    	CableLength = reader.GetString(2);
				                    	Connector = reader.GetString(3);
			                    	}
		                    	}
	            			
	            		smallLargeLabel = new LargeLabelH();
		            	smallLargeLabel.packingDate = DateTime.Now.ToString("yyyy-MMM-dd");
		            	smallLargeLabel.cartonNumber = cartonno;
		            	smallLargeLabel.colour = DTSN.Rows[0][1].ToString();
		            	smallLargeLabel.model = DTSN.Rows[0][2].ToString();
		            	smallLargeLabel.power = DTSN.Rows[0][3].ToString();
		              	//smallLargeLabel.materialCode = "Jacky";// decision not made yet
		              	smallLargeLabel.LID = lid;// 
		              	smallLargeLabel.quantity = lsSN.Count.ToString();
		              	smallLargeLabel.grossweight = getWeight (DTSN.Rows[0][2].ToString(),"Gross", lsSN.Count);
		              	smallLargeLabel.netweight = getWeight (DTSN.Rows[0][2].ToString(), "Net", lsSN.Count);
		              	smallLargeLabel.size = getDimensions (DTSN.Rows[0][2].ToString(), lsSN.Count);
						//smallLargeLabel.CassCode = GetCassCode(tbMaterialCode.Text.Trim());
						#region ADDING J-box type, Cable Length, Connector type,  bus bar 
		                sql = @"select Busbar from ProductionOrder_Mixing_Rule where ProductionOrder like '{0}'";
		                sql = string.Format(sql,"C"+line+ syr+"__"+ sequence);
		                reader  = ToolsClass.GetDataReader(sql);
		                    if(reader.Read() && reader!=null)
		                    	smallLargeLabel.BusBar = reader.GetString(0);
		                smallLargeLabel.CableLength = CableLength;
		              	smallLargeLabel.ConnectorType = Connector;
		              	sql = @"select * from T_WORKORDER_BOM where [MaterialDescription] like '%gb%' and [MaterialGroup] = 1004 and [OrderNo] like '{0}'";
		              	sql = string.Format(sql,"C"+line+ syr+"__"+ sequence);
		              	reader  = ToolsClass.GetDataReader(sql);
		              	smallLargeLabel.JboxType = (reader.Read() && reader!=null)?"CF1108-03gb":"CF1108-03";
		                #endregion
					
					
									
						//smallLargeLabel.loadID = loadID;	
			            if(smallLargeLabel.power != "Mixed")
			            {
			            	smallLargeLabel.power = smallLargeLabel.power + "W";
			            }
			            smallLargeLabel.cartonclass = DTSN.Rows[0][4].ToString();
		            	     
		            	if(smallLargeLabel.model[smallLargeLabel.model.Length-1] == 'M')
		            	{
		            		smallLargeLabel.cellType = "Mono";
		            	}
		            	if(smallLargeLabel.model[smallLargeLabel.model.Length-1] == 'P' ||smallLargeLabel.model[smallLargeLabel.model.Length-1] == 'N' )
		            	{
		            		smallLargeLabel.cellType = "Poly";
		            	}
		                for (int i = 0; i < lsSN.Count; i++)
		            	{
		                	smallLargeLabel.arraylist.Add(lsSN[i]);
		            	}	            
		            
			            csLabel = new CSLargeLabel("", this.sCartonLabelPath);
	                    csLabel.Write2ExcelH(this.iLabelH, iLabelV, smallLargeLabel,sheetNo);
		          	  }
            		}       
	
                  else if(PackingType == "Vertical")
                  {
	                   	if(lsSN.Count > 26)
                  	{
                  		MessageBox.Show("Print Qty grater than 26, Please confirm!");
                  		return;
                  	}

		                  	if (this.sCartonLabelPath.Length > 0 && this.rbLargeLabelPrint == true)
	                    {
	                    	
	                    	if (this.sCartonLabelPath.IndexOf(".xls") > 0)
	                        {
	                            largeLabel = new LargeLabel();
	                            largeLabel.CartonNumber = cartonno;
	                            largeLabel.Colour = DTSN.Rows[0][1].ToString();
	                            largeLabel.Model = DTSN.Rows[0][2].ToString();
	                            if(string.IsNullOrEmpty(Cell_Vendor.Text.ToString()))
	                            	largeLabel.CellVendor  = "";
	                            else
	                            	largeLabel.CellVendor = "Assembled in Canada with "+ Cell_Vendor.Text.ToString()+" cells";
	                            	
	                            string PO = GetPONumber(lsSN[0]);
			            		string line = PO.Substring(1,1);
			            		string sequence = PO.Substring(6,3);
	                           
	                    		
	                          	sql = @"select System_Voltage from ProductionOrder_Mixing_Rule where ProductionOrder like '{0}' "; 
					        	
//					     
						                          	
	                          	
	                          	string syr =lsSN[0].Substring(1,2);
					        	//string sMth  = DateTime.Now.Month.ToString().PadLeft(2,'0');
					        	
					        	//sql = string.Format(sql,"__"+ syr + sMth + sequence );
					        	sql = string.Format(sql,"C"+ line + syr + "__" + sequence );
					        	
					        	DataTable dt1 = ToolsClass.GetDataTable(sql);
					        	if(dt1.Rows.Count > 0)					        		
					        		largeLabel.Voltage = dt1.Rows[0][0].ToString();
					        	
	                            sql = @" Select top 1 [Resv01],[Resv02],[Resv03],[Resv04] from TSAP_WORKORDER_SALES_REQUEST where orderno like '{0}' order by CreatedOn desc";
	                           
	                    		
	                    		sql = string.Format(sql,"C"+line+ syr+"__"+ sequence);
	                    		SqlDataReader reader = ToolsClass.GetDataReader(sql,FormCover.InterfaceConnString);	                    	
		                    	while(reader.Read())
		                    	{
			                    	largeLabel.Market = reader.GetString(0);
			                    	largeLabel.LID = reader.GetString(1);
			                    	largeLabel.Cable = reader.GetString(2);
			                    	largeLabel.Connector = reader.GetString(3);
		                    	}
		                    	if(string.IsNullOrEmpty(largeLabel.Market)||string.IsNullOrEmpty(largeLabel.LID )||string.IsNullOrEmpty(largeLabel.Cable)||string.IsNullOrEmpty(largeLabel.Connector))
		                    	{
		                    		sql = @"select [Market],[Degradation],[Cable_Length],[Connector] from [ProductionOrder_Mixing_Rule] where [ProductionOrder] like '{0}'";
		                    		sql = string.Format(sql,"C"+line+ syr+"__"+ sequence);
		                    		reader  = ToolsClass.GetDataReader(sql);
		                    		while(reader.Read())
			                    	{
				                    	//largeLabel.Market = reader.GetString(0);
				                    	largeLabel.LID  = reader.GetString(1);
				                    	largeLabel.Cable = reader.GetString(2);
				                    	largeLabel.Connector = reader.GetString(3);
			                    	}
		                    	}
		                    	
		                    	
		                    	sql = @"select BusBar,Frame_Color From ProductionOrder_Mixing_Rule where ProductionOrder like '{0}'";
		                    	sql = string.Format(sql,"C"+ line + syr +"__"+ sequence);
		                    	reader = ToolsClass.GetDataReader(sql);
		                    	while(reader.Read())
		                    	{
		                    		largeLabel.BusBar = reader.GetString(0);
		                    		TbBusBar.Text = reader.GetString(0);
		                    		largeLabel.FrameColor = reader.GetString(1);
		                    	}
		                    
		                    	largeLabel.Class = DTSN.Rows[0][4].ToString();
								
								largeLabel.Quantity = lsSN.Count;
//								if(ToolsClass.getConfig("GlassType") == "PN")
//								{
//									largeLabel.GrossWeight = "728Kg";
//		              				largeLabel.NetWeight = "655Kg";
//								}
//								else
//								{
//									largeLabel.GrossWeight = getWeight (DTSN.Rows[0][2].ToString(),"Gross", lsSN.Count);
//		              				largeLabel.NetWeight = getWeight (DTSN.Rows[0][2].ToString(), "Net", lsSN.Count);
//								}
								
								largeLabel.GrossWeight = getWeight (DTSN.Rows[0][2].ToString(),"Gross", lsSN.Count);
		              			largeLabel.NetWeight = getWeight (DTSN.Rows[0][2].ToString(), "Net", lsSN.Count);
				              	largeLabel.Volumn = getDimensions (DTSN.Rows[0][2].ToString(), lsSN.Count);
	                            
	                          
	                            largeLabel.Power = DTSN.Rows[0][3].ToString();
	                            largeLabel.PowerGrade = "";
	                            
	
	                            largeLabel.Size = DTSN.Rows[0][5].ToString();
	                            largeLabel.Pattern = DTSN.Rows[0][6].ToString();
	
	                            if (largeLabel.Model[largeLabel.Model.Length - 1] == 'M')
	                            {
	                                largeLabel.CellType = "Mono";
	                                largeLabel.IsHavePattern = false;
	                            }
	                            if (largeLabel.Model[largeLabel.Model.Length - 1] == 'P' || largeLabel.Model[largeLabel.Model.Length - 1] == 'N' )
	                            {
	                                largeLabel.CellType = "Poly";
	                                if (!string.IsNullOrEmpty(sPattern) && IsCheckPattern == true)
	                                    largeLabel.IsHavePattern = true;
	                            }
	
	                            #region 竖包装标签打印测试信心拼List数据
	                            List<List<string>> SerialNumberListTestData = new List<List<string>>();
	                            for (int i = 0; i < lsSN.Count; i++)
	                            {
	                                List<string> SerialNumberTestData = new List<string>();
	                                DataTable dt = ProductStorageDAL.GetFTData(lsSN[i]);
	                                if (dt != null && dt.Rows.Count > 0)
	                                {
	                                    DataRow row = dt.Rows[0];
	                                    SerialNumberTestData.Add(Convert.ToString(row["SN"]));//组件号                                   
	                                    SerialNumberTestData.Add(ProductStorageDAL.GetDecimalString(Convert.ToString(row["ISC"]), 2)); //工作电流                                      
	                                    SerialNumberTestData.Add(ProductStorageDAL.GetDecimalString(Convert.ToString(row["VOC"]), 2));//工作电压 
	                                    SerialNumberTestData.Add(ProductStorageDAL.GetDecimalString(Convert.ToString(row["Im"]), 2)); //开路 电流
	                                    SerialNumberTestData.Add(ProductStorageDAL.GetDecimalString(Convert.ToString(row["Vm"]), 2)); //开路 电流
	                                    SerialNumberTestData.Add(ProductStorageDAL.GetDecimalString(Convert.ToString(row["Pmax"]), 1));//实测功率 
	                                    SerialNumberTestData.Add(Convert.ToString(row["StdPower"]));//标称功率        
	                                }
	                                else
	                                {
	                                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message11", "列印竖包装标签时，此组件的测试数据不全:") + dataGridView1[0, i].Value.ToString(), "ABNORMAL", lstView);
                                   		break;
	                                }
	                                SerialNumberListTestData.Add(SerialNumberTestData);
	                                //largeLabel.Arraylist.Add(dataGridView1[0, i].Value.ToString());
	                            }
	 
	                            if (SerialNumberListTestData.Count != lsSN.Count)
	                                return;
	
	                            largeLabel.ListInfo = SerialNumberListTestData;
	
	                            #endregion
	
	                            largeLabel.IsShowPowerGrade = Convert.ToString(this.txtShowPowerGrade.Text.Trim());
	                           // csLabel = new CSLargeLabel("", this.sCartonLabelPath);
	                            csLabel = new CSLargeLabel("", this.sCartonLabelPath);
	                            csLabel.Write2Excel(this.iLabelH-2, iLabelV, largeLabel);
	                          
	                        }
	                        else
	                        {
	                           ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message12", "竖包装标签选择不正确，请选择【Excel】格式的标签文件！"), "ABNORMAL", lstView);
                           		 return;
	                        }
	                    }
                  }
                  #endregion
        }
        
        
        void clearPanel7()
        {
        	TbCartonReprint.Text = "";
        }
        
        void Bt_ExitReprintClick(object sender, EventArgs e)
        {
        	clearPanel7();
        	panel7.Visible = false;
        }
		void Bt_cartonClick(object sender, EventArgs e)
		{
			if(!FormCover.HasPowerControl("Change Carton Number"))
			{
				return;
			}
			if(string.IsNullOrEmpty(tbBoxCode.Text.ToString()) ||string.IsNullOrEmpty(tbCustBoxCode.Text.ToString()))
				return;
			long i = 0;
			if(long.TryParse(tbBoxCode.Text, out i))
			{
				bool IsD = true;
				i= i + 1;
				while(IsD)
				{
					string sql = @" select * from Box where BoxID='"+i.ToString()+"' and (IsUsed<>'N'  OR number > 0) ";
					DataTable dt = ToolsClass.GetDataTable(sql);
					if(dt.Rows.Count < 1)
					{
						IsD = false;						
					}
					else
					{
						i = i + 1;
					}
				}
				
				tbBoxCode.Text = i.ToString();
				tbCustBoxCode.Text = i.ToString();
			}
			
		}
    }
}
