﻿namespace CSICPR
{
    partial class FormChangeOver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LeftOverMo = new System.Windows.Forms.ComboBox();
            this.Line = new System.Windows.Forms.Label();
            this.CartonNo = new System.Windows.Forms.TextBox();
            this.LeftOverIn = new System.Windows.Forms.Button();
            this.Review = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.pcs = new System.Windows.Forms.ComboBox();
            this.Qty = new System.Windows.Forms.Label();
            this.CellQty = new System.Windows.Forms.Label();
            this.BatchNo = new System.Windows.Forms.TextBox();
            this.Batch = new System.Windows.Forms.Label();
            this.Skid = new System.Windows.Forms.Label();
            this.SkidNo = new System.Windows.Forms.TextBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.Modify = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // LeftOverMo
            // 
            this.LeftOverMo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.LeftOverMo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LeftOverMo.FormattingEnabled = true;
            this.LeftOverMo.Items.AddRange(new object[] {
            "LINE A",
            "LINE B",
            "LINE D"});
            this.LeftOverMo.Location = new System.Drawing.Point(89, 12);
            this.LeftOverMo.Name = "LeftOverMo";
            this.LeftOverMo.Size = new System.Drawing.Size(154, 32);
            this.LeftOverMo.TabIndex = 0;
            // 
            // Line
            // 
            this.Line.AutoSize = true;
            this.Line.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Line.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Line.Location = new System.Drawing.Point(12, 16);
            this.Line.Name = "Line";
            this.Line.Size = new System.Drawing.Size(50, 24);
            this.Line.TabIndex = 1;
            this.Line.Text = "Line";
            this.Line.Click += new System.EventHandler(this.Line_Click);
            // 
            // CartonNo
            // 
            this.CartonNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CartonNo.Location = new System.Drawing.Point(12, 116);
            this.CartonNo.Multiline = true;
            this.CartonNo.Name = "CartonNo";
            this.CartonNo.Size = new System.Drawing.Size(231, 281);
            this.CartonNo.TabIndex = 3;
            // 
            // LeftOverIn
            // 
            this.LeftOverIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LeftOverIn.Location = new System.Drawing.Point(55, 442);
            this.LeftOverIn.Name = "LeftOverIn";
            this.LeftOverIn.Size = new System.Drawing.Size(133, 39);
            this.LeftOverIn.TabIndex = 5;
            this.LeftOverIn.Text = "LeftOver_In";
            this.LeftOverIn.UseVisualStyleBackColor = true;
            this.LeftOverIn.Click += new System.EventHandler(this.LeftOverIn_Click);
            // 
            // Review
            // 
            this.Review.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Review.Location = new System.Drawing.Point(345, 12);
            this.Review.Name = "Review";
            this.Review.Size = new System.Drawing.Size(133, 39);
            this.Review.TabIndex = 6;
            this.Review.Text = "Review";
            this.Review.UseVisualStyleBackColor = true;
            this.Review.Click += new System.EventHandler(this.Review_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(345, 60);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(764, 241);
            this.dataGridView1.TabIndex = 4;
            // 
            // pcs
            // 
            this.pcs.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pcs.FormattingEnabled = true;
            this.pcs.Items.AddRange(new object[] {
            "1600",
            "1500",
            "1400",
            "1300",
            "1200",
            "1100",
            "1000",
            "900",
            "800",
            "700",
            "600",
            "500",
            "400",
            "300",
            "200",
            "100"});
            this.pcs.Location = new System.Drawing.Point(98, 403);
            this.pcs.Name = "pcs";
            this.pcs.Size = new System.Drawing.Size(145, 32);
            this.pcs.TabIndex = 4;
            // 
            // Qty
            // 
            this.Qty.AutoSize = true;
            this.Qty.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Qty.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Qty.Location = new System.Drawing.Point(9, 408);
            this.Qty.Name = "Qty";
            this.Qty.Size = new System.Drawing.Size(86, 24);
            this.Qty.TabIndex = 1;
            this.Qty.Text = "Quantity";
            this.Qty.Click += new System.EventHandler(this.Line_Click);
            // 
            // CellQty
            // 
            this.CellQty.AutoSize = true;
            this.CellQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CellQty.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.CellQty.Location = new System.Drawing.Point(504, 20);
            this.CellQty.Name = "CellQty";
            this.CellQty.Size = new System.Drawing.Size(77, 24);
            this.CellQty.TabIndex = 1;
            this.CellQty.Text = "CellQty";
            this.CellQty.Visible = false;
            this.CellQty.Click += new System.EventHandler(this.Line_Click);
            // 
            // BatchNo
            // 
            this.BatchNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BatchNo.Location = new System.Drawing.Point(89, 49);
            this.BatchNo.Name = "BatchNo";
            this.BatchNo.Size = new System.Drawing.Size(154, 29);
            this.BatchNo.TabIndex = 1;
            this.BatchNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BatchNo_Keydown);
            // 
            // Batch
            // 
            this.Batch.AutoSize = true;
            this.Batch.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Batch.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Batch.Location = new System.Drawing.Point(12, 49);
            this.Batch.Name = "Batch";
            this.Batch.Size = new System.Drawing.Size(62, 24);
            this.Batch.TabIndex = 1;
            this.Batch.Text = "Batch";
            this.Batch.Click += new System.EventHandler(this.Line_Click);
            // 
            // Skid
            // 
            this.Skid.AutoSize = true;
            this.Skid.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Skid.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Skid.Location = new System.Drawing.Point(12, 82);
            this.Skid.Name = "Skid";
            this.Skid.Size = new System.Drawing.Size(70, 24);
            this.Skid.TabIndex = 1;
            this.Skid.Text = "S_Unit";
            this.Skid.Click += new System.EventHandler(this.Line_Click);
            // 
            // SkidNo
            // 
            this.SkidNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SkidNo.Location = new System.Drawing.Point(89, 83);
            this.SkidNo.Name = "SkidNo";
            this.SkidNo.Size = new System.Drawing.Size(154, 29);
            this.SkidNo.TabIndex = 2;
            this.SkidNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SkidNo_Keydown);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(546, 320);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(563, 161);
            this.dataGridView2.TabIndex = 7;
            // 
            // Modify
            // 
            this.Modify.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Modify.Location = new System.Drawing.Point(345, 339);
            this.Modify.Name = "Modify";
            this.Modify.Size = new System.Drawing.Size(133, 39);
            this.Modify.TabIndex = 8;
            this.Modify.Text = "Modify";
            this.Modify.UseVisualStyleBackColor = true;
            this.Modify.Click += new System.EventHandler(this.Modify_Click);
            // 
            // Delete
            // 
            this.Delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Delete.Location = new System.Drawing.Point(345, 442);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(133, 39);
            this.Delete.TabIndex = 9;
            this.Delete.Text = "Delete";
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // FormChangeOver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(1144, 579);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.Modify);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.SkidNo);
            this.Controls.Add(this.BatchNo);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.Review);
            this.Controls.Add(this.LeftOverIn);
            this.Controls.Add(this.CartonNo);
            this.Controls.Add(this.CellQty);
            this.Controls.Add(this.Qty);
            this.Controls.Add(this.Skid);
            this.Controls.Add(this.Batch);
            this.Controls.Add(this.Line);
            this.Controls.Add(this.pcs);
            this.Controls.Add(this.LeftOverMo);
            this.Name = "FormChangeOver";
            this.Text = "FormChangeOver";
            this.Load += new System.EventHandler(this.FormChangeOver_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox LeftOverMo;
        private System.Windows.Forms.Label Line;
        private System.Windows.Forms.TextBox CartonNo;
        private System.Windows.Forms.Button LeftOverIn;
        private System.Windows.Forms.Button Review;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox pcs;
        private System.Windows.Forms.Label Qty;
        private System.Windows.Forms.Label CellQty;
        private System.Windows.Forms.TextBox BatchNo;
        private System.Windows.Forms.Label Batch;
        private System.Windows.Forms.Label Skid;
        private System.Windows.Forms.TextBox SkidNo;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button Modify;
        private System.Windows.Forms.Button Delete;
    }
}