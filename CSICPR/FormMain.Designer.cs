﻿using System;

namespace CSICPR
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
        	System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
        	this.menuStrip1 = new System.Windows.Forms.MenuStrip();
        	this.menuHelp = new System.Windows.Forms.ToolStripMenuItem();
        	this.tstHelpAbput = new System.Windows.Forms.ToolStripMenuItem();
        	this.tsbHelpUsing = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
        	this.tsbWindowTile = new System.Windows.Forms.ToolStripMenuItem();
        	this.tsbWindowHzt = new System.Windows.Forms.ToolStripMenuItem();
        	this.tsbWindowVtcl = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
        	this.panel1 = new System.Windows.Forms.Panel();
        	this.toolStrip1 = new System.Windows.Forms.ToolStrip();
        	this.tssbSystem = new System.Windows.Forms.ToolStripSplitButton();
        	this.MenuItemBase = new System.Windows.Forms.ToolStripMenuItem();
        	this.accountManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.packageConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.LanguageChg = new System.Windows.Forms.ToolStripMenuItem();
        	this.tssbCTM = new System.Windows.Forms.ToolStripMenuItem();
        	this.二维码参数配置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
        	this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
        	this.SNPackingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.TotalPackingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
        	this.toolStripSplitButton2 = new System.Windows.Forms.ToolStripSplitButton();
        	this.MaterialTemplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.MaterialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.NormalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.ManyStorageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.ManyStorageSNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.ALLStorageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.SNStorageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItemCancel = new System.Windows.Forms.ToolStripMenuItem();
        	this.RevokeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItemUploadSap = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItemUpResult = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItemStorageQuery = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItemReworkRegister = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItemSapBatchDataQuery = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
        	this.toolStripSplitButton3 = new System.Windows.Forms.ToolStripSplitButton();
        	this.ToolStripMenuItemMaterial = new System.Windows.Forms.ToolStripMenuItem();
        	this.ToolStripMenuItemOrderno = new System.Windows.Forms.ToolStripMenuItem();
        	this.ToolStripMenuItemStorage = new System.Windows.Forms.ToolStripMenuItem();
        	this.ToolStripMenuItemConfirmReOrder = new System.Windows.Forms.ToolStripMenuItem();
        	this.ToolStripMenuItemPackingData = new System.Windows.Forms.ToolStripMenuItem();
        	this.ToolStripMenuItemMatainOrder = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
        	this.tssbChart = new System.Windows.Forms.ToolStripSplitButton();
        	this.MenuItemPackage = new System.Windows.Forms.ToolStripMenuItem();
        	this.packageReportByCartonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.packageReportByModuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.ToolStripMenuItemStorageDataUpQuery = new System.Windows.Forms.ToolStripMenuItem();
        	this.ToolStripMenuItemStorageResultQuery = new System.Windows.Forms.ToolStripMenuItem();
        	this.ToolStripMenuItemOrderQuery = new System.Windows.Forms.ToolStripMenuItem();
        	this.uploadStatusQueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
        	this.toolStripSplitButton4 = new System.Windows.Forms.ToolStripSplitButton();
        	this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem23 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem24 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem25 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem26 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem27 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem28 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem29 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem30 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem31 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem32 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem33 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem34 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem35 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem36 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem37 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem38 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem39 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem40 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem41 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem42 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem43 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem44 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem45 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem46 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripMenuItem47 = new System.Windows.Forms.ToolStripMenuItem();
        	this.MOChangeOverToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.cellLossToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineAToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineBToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineDToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        	this.productionWarehouseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.glassToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.eVAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineAToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineBToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
        	this.ribbonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineAToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineBToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
        	this.eVAToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineAToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineBToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
        	this.backSheetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineAToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineBToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
        	this.jBoxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineAToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineBToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
        	this.frameshortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineAToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineBToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
        	this.framelongToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineAToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineBToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
        	this.crossBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineAToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineBToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
        	this.sealnatMS930ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineAToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineBToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
        	this.sealnat1521AToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineAToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineBToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
        	this.sealnat1527ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineAToolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineBToolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
        	this.sealnat1521BToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineAToolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineBToolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
        	this.sealnatMS9371BToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineAToolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
        	this.lineBToolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
        	this.BarcodePrint = new System.Windows.Forms.ToolStripSplitButton();
        	this.SideBarcodePrint = new System.Windows.Forms.ToolStripMenuItem();
        	this.二维码商标打印ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
        	this.tssbExit = new System.Windows.Forms.ToolStripButton();
        	this.panel2 = new System.Windows.Forms.Panel();
        	this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
        	this.tsslCurUserName = new System.Windows.Forms.ToolStripStatusLabel();
        	this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
        	this.tsslCurWorkshop = new System.Windows.Forms.ToolStripStatusLabel();
        	this.statusStrip1 = new System.Windows.Forms.StatusStrip();
        	this.menuStrip1.SuspendLayout();
        	this.panel1.SuspendLayout();
        	this.toolStrip1.SuspendLayout();
        	this.panel2.SuspendLayout();
        	this.statusStrip1.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// menuStrip1
        	// 
        	this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.menuHelp});
        	this.menuStrip1.Location = new System.Drawing.Point(0, 0);
        	this.menuStrip1.Name = "menuStrip1";
        	this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
        	this.menuStrip1.Size = new System.Drawing.Size(86, 55);
        	this.menuStrip1.TabIndex = 8;
        	this.menuStrip1.ItemAdded += new System.Windows.Forms.ToolStripItemEventHandler(this.menuStrip1_ItemAdded);
        	// 
        	// menuHelp
        	// 
        	this.menuHelp.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
        	this.menuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.tstHelpAbput,
			this.tsbHelpUsing,
			this.toolStripSeparator10,
			this.tsbWindowTile,
			this.tsbWindowHzt,
			this.tsbWindowVtcl,
			this.toolStripSeparator11});
        	this.menuHelp.Image = global::CSICPR.Properties.Resources.book;
        	this.menuHelp.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.menuHelp.Name = "menuHelp";
        	this.menuHelp.Size = new System.Drawing.Size(44, 51);
        	this.menuHelp.Text = "Help";
        	this.menuHelp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	// 
        	// tstHelpAbput
        	// 
        	this.tstHelpAbput.Image = global::CSICPR.Properties.Resources.information;
        	this.tstHelpAbput.Name = "tstHelpAbput";
        	this.tstHelpAbput.Size = new System.Drawing.Size(179, 22);
        	this.tstHelpAbput.Text = "About";
        	// 
        	// tsbHelpUsing
        	// 
        	this.tsbHelpUsing.Image = global::CSICPR.Properties.Resources.lightbulb;
        	this.tsbHelpUsing.Name = "tsbHelpUsing";
        	this.tsbHelpUsing.Size = new System.Drawing.Size(179, 22);
        	this.tsbHelpUsing.Text = "Help";
        	// 
        	// toolStripSeparator10
        	// 
        	this.toolStripSeparator10.Name = "toolStripSeparator10";
        	this.toolStripSeparator10.Size = new System.Drawing.Size(176, 6);
        	// 
        	// tsbWindowTile
        	// 
        	this.tsbWindowTile.Name = "tsbWindowTile";
        	this.tsbWindowTile.Size = new System.Drawing.Size(179, 22);
        	this.tsbWindowTile.Text = "Tiling display";
        	this.tsbWindowTile.Click += new System.EventHandler(this.tsbWindowTile_Click);
        	// 
        	// tsbWindowHzt
        	// 
        	this.tsbWindowHzt.Name = "tsbWindowHzt";
        	this.tsbWindowHzt.Size = new System.Drawing.Size(179, 22);
        	this.tsbWindowHzt.Text = "Horizontally Display";
        	this.tsbWindowHzt.Click += new System.EventHandler(this.tsbWindowHzt_Click);
        	// 
        	// tsbWindowVtcl
        	// 
        	this.tsbWindowVtcl.Name = "tsbWindowVtcl";
        	this.tsbWindowVtcl.Size = new System.Drawing.Size(179, 22);
        	this.tsbWindowVtcl.Text = "Vertically Display";
        	this.tsbWindowVtcl.Click += new System.EventHandler(this.tsbWindowVtcl_Click);
        	// 
        	// toolStripSeparator11
        	// 
        	this.toolStripSeparator11.Name = "toolStripSeparator11";
        	this.toolStripSeparator11.Size = new System.Drawing.Size(176, 6);
        	// 
        	// panel1
        	// 
        	this.panel1.Controls.Add(this.toolStrip1);
        	this.panel1.Controls.Add(this.panel2);
        	this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
        	this.panel1.Location = new System.Drawing.Point(0, 0);
        	this.panel1.Name = "panel1";
        	this.panel1.Size = new System.Drawing.Size(784, 61);
        	this.panel1.TabIndex = 9;
        	// 
        	// toolStrip1
        	// 
        	this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
        	this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.tssbSystem,
			this.toolStripSeparator2,
			this.toolStripSplitButton1,
			this.toolStripSeparator3,
			this.toolStripSplitButton2,
			this.toolStripSeparator9,
			this.toolStripSplitButton3,
			this.toolStripSeparator8,
			this.tssbChart,
			this.toolStripSeparator6,
			this.toolStripSplitButton4,
			this.toolStripSeparator4,
			this.BarcodePrint,
			this.toolStripSeparator1,
			this.tssbExit});
        	this.toolStrip1.Location = new System.Drawing.Point(0, 0);
        	this.toolStrip1.Name = "toolStrip1";
        	this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
        	this.toolStrip1.Size = new System.Drawing.Size(698, 61);
        	this.toolStrip1.TabIndex = 4;
        	// 
        	// tssbSystem
        	// 
        	this.tssbSystem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.MenuItemBase,
			this.accountManagerToolStripMenuItem,
			this.packageConfigToolStripMenuItem,
			this.LanguageChg,
			this.tssbCTM,
			this.二维码参数配置ToolStripMenuItem});
        	this.tssbSystem.Image = global::CSICPR.Properties.Resources.box;
        	this.tssbSystem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.tssbSystem.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.tssbSystem.Name = "tssbSystem";
        	this.tssbSystem.Size = new System.Drawing.Size(71, 58);
        	this.tssbSystem.Text = "系统设置";
        	this.tssbSystem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.tssbSystem.ButtonClick += new System.EventHandler(this.tssbSystem_ButtonClick);
        	// 
        	// MenuItemBase
        	// 
        	this.MenuItemBase.Image = global::CSICPR.Properties.Resources.table_gear;
        	this.MenuItemBase.Name = "MenuItemBase";
        	this.MenuItemBase.Size = new System.Drawing.Size(187, 22);
        	this.MenuItemBase.Text = "数据库设定";
        	this.MenuItemBase.Visible = false;
        	this.MenuItemBase.Click += new System.EventHandler(this.MenuItemBase_Click);
        	// 
        	// accountManagerToolStripMenuItem
        	// 
        	this.accountManagerToolStripMenuItem.Image = global::CSICPR.Properties.Resources.table_gear;
        	this.accountManagerToolStripMenuItem.Name = "accountManagerToolStripMenuItem";
        	this.accountManagerToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
        	this.accountManagerToolStripMenuItem.Text = "Account Mangement";
        	this.accountManagerToolStripMenuItem.Click += new System.EventHandler(this.accountManagerToolStripMenuItem_Click);
        	// 
        	// packageConfigToolStripMenuItem
        	// 
        	this.packageConfigToolStripMenuItem.Image = global::CSICPR.Properties.Resources.table_gear;
        	this.packageConfigToolStripMenuItem.Name = "packageConfigToolStripMenuItem";
        	this.packageConfigToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
        	this.packageConfigToolStripMenuItem.Text = "包装参数配置";
        	this.packageConfigToolStripMenuItem.Visible = false;
        	this.packageConfigToolStripMenuItem.Click += new System.EventHandler(this.packageConfigToolStripMenuItem_Click);
        	// 
        	// LanguageChg
        	// 
        	this.LanguageChg.Image = global::CSICPR.Properties.Resources.table_gear;
        	this.LanguageChg.Name = "LanguageChg";
        	this.LanguageChg.Size = new System.Drawing.Size(187, 22);
        	this.LanguageChg.Text = "Change to Chinese";
        	this.LanguageChg.Visible = false;
        	this.LanguageChg.Click += new System.EventHandler(this.changeToChineseToolStripMenuItem_Click);
        	// 
        	// tssbCTM
        	// 
        	this.tssbCTM.Image = global::CSICPR.Properties.Resources.table_gear;
        	this.tssbCTM.Name = "tssbCTM";
        	this.tssbCTM.Size = new System.Drawing.Size(187, 22);
        	this.tssbCTM.Text = "CTM判等";
        	this.tssbCTM.Click += new System.EventHandler(this.tssbCTM_Click);
        	// 
        	// 二维码参数配置ToolStripMenuItem
        	// 
        	this.二维码参数配置ToolStripMenuItem.Image = global::CSICPR.Properties.Resources.table_gear;
        	this.二维码参数配置ToolStripMenuItem.Name = "二维码参数配置ToolStripMenuItem";
        	this.二维码参数配置ToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
        	this.二维码参数配置ToolStripMenuItem.Text = "二维码参数配置";
        	this.二维码参数配置ToolStripMenuItem.Visible = false;
        	this.二维码参数配置ToolStripMenuItem.Click += new System.EventHandler(this.二维码参数配置ToolStripMenuItem_Click);
        	// 
        	// toolStripSeparator2
        	// 
        	this.toolStripSeparator2.Name = "toolStripSeparator2";
        	this.toolStripSeparator2.Size = new System.Drawing.Size(6, 61);
        	// 
        	// toolStripSplitButton1
        	// 
        	this.toolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.SNPackingToolStripMenuItem,
			this.TotalPackingToolStripMenuItem});
        	this.toolStripSplitButton1.Image = global::CSICPR.Properties.Resources.down;
        	this.toolStripSplitButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.toolStripSplitButton1.Name = "toolStripSplitButton1";
        	this.toolStripSplitButton1.Size = new System.Drawing.Size(71, 58);
        	this.toolStripSplitButton1.Text = "打包管理";
        	this.toolStripSplitButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.toolStripSplitButton1.ButtonClick += new System.EventHandler(this.toolStripSplitButton1_ButtonClick);
        	// 
        	// SNPackingToolStripMenuItem
        	// 
        	this.SNPackingToolStripMenuItem.Image = global::CSICPR.Properties.Resources.door_in;
        	this.SNPackingToolStripMenuItem.Name = "SNPackingToolStripMenuItem";
        	this.SNPackingToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
        	this.SNPackingToolStripMenuItem.Text = "组件打包";
        	this.SNPackingToolStripMenuItem.Click += new System.EventHandler(this.SNPackingToolStripMenuItem_Click);
        	// 
        	// TotalPackingToolStripMenuItem
        	// 
        	this.TotalPackingToolStripMenuItem.Image = global::CSICPR.Properties.Resources.door_in;
        	this.TotalPackingToolStripMenuItem.Name = "TotalPackingToolStripMenuItem";
        	this.TotalPackingToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
        	this.TotalPackingToolStripMenuItem.Text = "并托打包";
        	this.TotalPackingToolStripMenuItem.Visible = false;
        	this.TotalPackingToolStripMenuItem.Click += new System.EventHandler(this.TotalPackingToolStripMenuItem_Click);
        	// 
        	// toolStripSeparator3
        	// 
        	this.toolStripSeparator3.Name = "toolStripSeparator3";
        	this.toolStripSeparator3.Size = new System.Drawing.Size(6, 61);
        	// 
        	// toolStripSplitButton2
        	// 
        	this.toolStripSplitButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.MaterialTemplateToolStripMenuItem,
			this.MaterialToolStripMenuItem,
			this.NormalToolStripMenuItem,
			this.ManyStorageToolStripMenuItem,
			this.ManyStorageSNToolStripMenuItem,
			this.ALLStorageToolStripMenuItem,
			this.SNStorageToolStripMenuItem,
			this.toolStripMenuItemCancel,
			this.RevokeToolStripMenuItem,
			this.toolStripMenuItemUploadSap,
			this.toolStripMenuItemUpResult,
			this.toolStripMenuItemStorageQuery,
			this.toolStripMenuItemReworkRegister,
			this.toolStripMenuItemSapBatchDataQuery});
        	this.toolStripSplitButton2.Image = global::CSICPR.Properties.Resources.CartonStorge;
        	this.toolStripSplitButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.toolStripSplitButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.toolStripSplitButton2.Name = "toolStripSplitButton2";
        	this.toolStripSplitButton2.Size = new System.Drawing.Size(71, 58);
        	this.toolStripSplitButton2.Text = "入库管理";
        	this.toolStripSplitButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.toolStripSplitButton2.ButtonClick += new System.EventHandler(this.toolStripSplitButton2_ButtonClick);
        	// 
        	// MaterialTemplateToolStripMenuItem
        	// 
        	this.MaterialTemplateToolStripMenuItem.Image = global::CSICPR.Properties.Resources.CancelStorge;
        	this.MaterialTemplateToolStripMenuItem.Name = "MaterialTemplateToolStripMenuItem";
        	this.MaterialTemplateToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
        	this.MaterialTemplateToolStripMenuItem.Text = "物料模板维护";
        	this.MaterialTemplateToolStripMenuItem.Click += new System.EventHandler(this.MaterialTemplateToolStripMenuItem_Click);
        	// 
        	// MaterialToolStripMenuItem
        	// 
        	this.MaterialToolStripMenuItem.Image = global::CSICPR.Properties.Resources.CancelStorge;
        	this.MaterialToolStripMenuItem.Name = "MaterialToolStripMenuItem";
        	this.MaterialToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
        	this.MaterialToolStripMenuItem.Text = "设定组件物料模板";
        	this.MaterialToolStripMenuItem.Click += new System.EventHandler(this.MaterialToolStripMenuItem_Click);
        	// 
        	// NormalToolStripMenuItem
        	// 
        	this.NormalToolStripMenuItem.Image = global::CSICPR.Properties.Resources.CancelStorge;
        	this.NormalToolStripMenuItem.Name = "NormalToolStripMenuItem";
        	this.NormalToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
        	this.NormalToolStripMenuItem.Text = "批量入库";
        	this.NormalToolStripMenuItem.Click += new System.EventHandler(this.NormalToolStripMenuItem_Click);
        	// 
        	// ManyStorageToolStripMenuItem
        	// 
        	this.ManyStorageToolStripMenuItem.Image = global::CSICPR.Properties.Resources.CancelStorge;
        	this.ManyStorageToolStripMenuItem.Name = "ManyStorageToolStripMenuItem";
        	this.ManyStorageToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
        	this.ManyStorageToolStripMenuItem.Text = "批量整托入库";
        	this.ManyStorageToolStripMenuItem.Visible = false;
        	this.ManyStorageToolStripMenuItem.Click += new System.EventHandler(this.ManyStorageToolStripMenuItem_Click);
        	// 
        	// ManyStorageSNToolStripMenuItem
        	// 
        	this.ManyStorageSNToolStripMenuItem.Image = global::CSICPR.Properties.Resources.CancelStorge;
        	this.ManyStorageSNToolStripMenuItem.Name = "ManyStorageSNToolStripMenuItem";
        	this.ManyStorageSNToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
        	this.ManyStorageSNToolStripMenuItem.Text = "批量零散件入库";
        	this.ManyStorageSNToolStripMenuItem.Visible = false;
        	this.ManyStorageSNToolStripMenuItem.Click += new System.EventHandler(this.ManyStorageSNToolStripMenuItem_Click);
        	// 
        	// ALLStorageToolStripMenuItem
        	// 
        	this.ALLStorageToolStripMenuItem.Image = global::CSICPR.Properties.Resources.CancelStorge;
        	this.ALLStorageToolStripMenuItem.Name = "ALLStorageToolStripMenuItem";
        	this.ALLStorageToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
        	this.ALLStorageToolStripMenuItem.Text = "整托入库";
        	this.ALLStorageToolStripMenuItem.Visible = false;
        	this.ALLStorageToolStripMenuItem.Click += new System.EventHandler(this.TotalStorageToolStripMenuItem_Click);
        	// 
        	// SNStorageToolStripMenuItem
        	// 
        	this.SNStorageToolStripMenuItem.Image = global::CSICPR.Properties.Resources.CancelStorge;
        	this.SNStorageToolStripMenuItem.Name = "SNStorageToolStripMenuItem";
        	this.SNStorageToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
        	this.SNStorageToolStripMenuItem.Text = "零散件入库";
        	this.SNStorageToolStripMenuItem.Visible = false;
        	this.SNStorageToolStripMenuItem.Click += new System.EventHandler(this.SNStorageToolStripMenuItem_Click);
        	// 
        	// toolStripMenuItemCancel
        	// 
        	this.toolStripMenuItemCancel.Image = global::CSICPR.Properties.Resources.CancelStorge;
        	this.toolStripMenuItemCancel.Name = "toolStripMenuItemCancel";
        	this.toolStripMenuItemCancel.Size = new System.Drawing.Size(191, 22);
        	this.toolStripMenuItemCancel.Text = "取消入库";
        	this.toolStripMenuItemCancel.Click += new System.EventHandler(this.toolStripMenuItem取消入库_Click);
        	// 
        	// RevokeToolStripMenuItem
        	// 
        	this.RevokeToolStripMenuItem.Image = global::CSICPR.Properties.Resources.CancelStorge;
        	this.RevokeToolStripMenuItem.Name = "RevokeToolStripMenuItem";
        	this.RevokeToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
        	this.RevokeToolStripMenuItem.Text = "撤销入库";
        	this.RevokeToolStripMenuItem.Visible = false;
        	this.RevokeToolStripMenuItem.Click += new System.EventHandler(this.CancelStorageToolStripMenuItem_Click);
        	// 
        	// toolStripMenuItemUploadSap
        	// 
        	this.toolStripMenuItemUploadSap.Image = global::CSICPR.Properties.Resources.CancelStorge;
        	this.toolStripMenuItemUploadSap.Name = "toolStripMenuItemUploadSap";
        	this.toolStripMenuItemUploadSap.Size = new System.Drawing.Size(191, 22);
        	this.toolStripMenuItemUploadSap.Text = "入库上传SAP";
        	this.toolStripMenuItemUploadSap.Click += new System.EventHandler(this.toolStripMenuItem入库上传SAP_Click);
        	// 
        	// toolStripMenuItemUpResult
        	// 
        	this.toolStripMenuItemUpResult.Image = global::CSICPR.Properties.Resources.CancelStorge;
        	this.toolStripMenuItemUpResult.Name = "toolStripMenuItemUpResult";
        	this.toolStripMenuItemUpResult.Size = new System.Drawing.Size(191, 22);
        	this.toolStripMenuItemUpResult.Text = "入库上传结果查询";
        	this.toolStripMenuItemUpResult.Visible = false;
        	this.toolStripMenuItemUpResult.Click += new System.EventHandler(this.toolStripMenuItem入库上传结果查询_Click);
        	// 
        	// toolStripMenuItemStorageQuery
        	// 
        	this.toolStripMenuItemStorageQuery.Image = global::CSICPR.Properties.Resources.CancelStorge;
        	this.toolStripMenuItemStorageQuery.Name = "toolStripMenuItemStorageQuery";
        	this.toolStripMenuItemStorageQuery.Size = new System.Drawing.Size(191, 22);
        	this.toolStripMenuItemStorageQuery.Text = "入库数据查询";
        	this.toolStripMenuItemStorageQuery.Visible = false;
        	this.toolStripMenuItemStorageQuery.Click += new System.EventHandler(this.toolStripMenuItem入库数据查询_Click);
        	// 
        	// toolStripMenuItemReworkRegister
        	// 
        	this.toolStripMenuItemReworkRegister.Image = global::CSICPR.Properties.Resources.CancelStorge;
        	this.toolStripMenuItemReworkRegister.Name = "toolStripMenuItemReworkRegister";
        	this.toolStripMenuItemReworkRegister.Size = new System.Drawing.Size(191, 22);
        	this.toolStripMenuItemReworkRegister.Text = "组件重工注册";
        	this.toolStripMenuItemReworkRegister.Click += new System.EventHandler(this.toolStripMenuItem组件重工注册_Click);
        	// 
        	// toolStripMenuItemSapBatchDataQuery
        	// 
        	this.toolStripMenuItemSapBatchDataQuery.Image = global::CSICPR.Properties.Resources.CancelStorge;
        	this.toolStripMenuItemSapBatchDataQuery.Name = "toolStripMenuItemSapBatchDataQuery";
        	this.toolStripMenuItemSapBatchDataQuery.Size = new System.Drawing.Size(191, 22);
        	this.toolStripMenuItemSapBatchDataQuery.Text = "SAP批次特性数据查询";
        	this.toolStripMenuItemSapBatchDataQuery.Click += new System.EventHandler(this.toolStripMenuItemSapBatchDataQuery_Click);
        	// 
        	// toolStripSeparator9
        	// 
        	this.toolStripSeparator9.Name = "toolStripSeparator9";
        	this.toolStripSeparator9.Size = new System.Drawing.Size(6, 61);
        	// 
        	// toolStripSplitButton3
        	// 
        	this.toolStripSplitButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.ToolStripMenuItemMaterial,
			this.ToolStripMenuItemOrderno,
			this.ToolStripMenuItemStorage,
			this.ToolStripMenuItemConfirmReOrder,
			this.ToolStripMenuItemPackingData,
			this.ToolStripMenuItemMatainOrder});
        	this.toolStripSplitButton3.Image = global::CSICPR.Properties.Resources.Lload;
        	this.toolStripSplitButton3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.toolStripSplitButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.toolStripSplitButton3.Name = "toolStripSplitButton3";
        	this.toolStripSplitButton3.Size = new System.Drawing.Size(71, 58);
        	this.toolStripSplitButton3.Text = "接口管理";
        	this.toolStripSplitButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.toolStripSplitButton3.ToolTipText = "接口管理";
        	this.toolStripSplitButton3.ButtonClick += new System.EventHandler(this.toolStripSplitButton3_ButtonClick);
        	// 
        	// ToolStripMenuItemMaterial
        	// 
        	this.ToolStripMenuItemMaterial.Image = global::CSICPR.Properties.Resources.account;
        	this.ToolStripMenuItemMaterial.Name = "ToolStripMenuItemMaterial";
        	this.ToolStripMenuItemMaterial.Size = new System.Drawing.Size(170, 22);
        	this.ToolStripMenuItemMaterial.Text = "物料主数据下载";
        	this.ToolStripMenuItemMaterial.Click += new System.EventHandler(this.SAPMaterialMaterToolStripMenuItem_Click);
        	// 
        	// ToolStripMenuItemOrderno
        	// 
        	this.ToolStripMenuItemOrderno.Image = global::CSICPR.Properties.Resources.account;
        	this.ToolStripMenuItemOrderno.Name = "ToolStripMenuItemOrderno";
        	this.ToolStripMenuItemOrderno.Size = new System.Drawing.Size(170, 22);
        	this.ToolStripMenuItemOrderno.Text = "工单下载";
        	this.ToolStripMenuItemOrderno.Click += new System.EventHandler(this.SAPWoLoadToolStripMenuItem_Click);
        	// 
        	// ToolStripMenuItemStorage
        	// 
        	this.ToolStripMenuItemStorage.Image = global::CSICPR.Properties.Resources.account;
        	this.ToolStripMenuItemStorage.Name = "ToolStripMenuItemStorage";
        	this.ToolStripMenuItemStorage.Size = new System.Drawing.Size(170, 22);
        	this.ToolStripMenuItemStorage.Text = "物料转储数据下载";
        	this.ToolStripMenuItemStorage.Click += new System.EventHandler(this.SAPMaterialTransferToolStripMenuItem_Click);
        	// 
        	// ToolStripMenuItemConfirmReOrder
        	// 
        	this.ToolStripMenuItemConfirmReOrder.Image = global::CSICPR.Properties.Resources.account;
        	this.ToolStripMenuItemConfirmReOrder.Name = "ToolStripMenuItemConfirmReOrder";
        	this.ToolStripMenuItemConfirmReOrder.Size = new System.Drawing.Size(170, 22);
        	this.ToolStripMenuItemConfirmReOrder.Text = "重工工单确认";
        	this.ToolStripMenuItemConfirmReOrder.Click += new System.EventHandler(this.SAPReworkLoadToolStripMenuItem_Click);
        	// 
        	// ToolStripMenuItemPackingData
        	// 
        	this.ToolStripMenuItemPackingData.Image = global::CSICPR.Properties.Resources.account;
        	this.ToolStripMenuItemPackingData.Name = "ToolStripMenuItemPackingData";
        	this.ToolStripMenuItemPackingData.Size = new System.Drawing.Size(170, 22);
        	this.ToolStripMenuItemPackingData.Text = "拼柜数据下载";
        	this.ToolStripMenuItemPackingData.Visible = false;
        	this.ToolStripMenuItemPackingData.Click += new System.EventHandler(this.JobNoToolStripMenuItem_Click);
        	// 
        	// ToolStripMenuItemMatainOrder
        	// 
        	this.ToolStripMenuItemMatainOrder.Image = global::CSICPR.Properties.Resources.account;
        	this.ToolStripMenuItemMatainOrder.Name = "ToolStripMenuItemMatainOrder";
        	this.ToolStripMenuItemMatainOrder.Size = new System.Drawing.Size(170, 22);
        	this.ToolStripMenuItemMatainOrder.Text = "工单数据维护";
        	this.ToolStripMenuItemMatainOrder.Click += new System.EventHandler(this.WODataToolStripMenuItem_Click);
        	// 
        	// toolStripSeparator8
        	// 
        	this.toolStripSeparator8.Name = "toolStripSeparator8";
        	this.toolStripSeparator8.Size = new System.Drawing.Size(6, 61);
        	// 
        	// tssbChart
        	// 
        	this.tssbChart.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.MenuItemPackage,
			this.packageReportByCartonToolStripMenuItem,
			this.packageReportByModuleToolStripMenuItem,
			this.ToolStripMenuItemStorageDataUpQuery,
			this.ToolStripMenuItemStorageResultQuery,
			this.ToolStripMenuItemOrderQuery,
			this.uploadStatusQueryToolStripMenuItem});
        	this.tssbChart.Image = global::CSICPR.Properties.Resources.chart;
        	this.tssbChart.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.tssbChart.Name = "tssbChart";
        	this.tssbChart.Size = new System.Drawing.Size(71, 58);
        	this.tssbChart.Text = "报表管理";
        	this.tssbChart.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.tssbChart.ToolTipText = "报表查询";
        	this.tssbChart.ButtonClick += new System.EventHandler(this.tssbChart_ButtonClick);
        	// 
        	// MenuItemPackage
        	// 
        	this.MenuItemPackage.Image = global::CSICPR.Properties.Resources.application_form;
        	this.MenuItemPackage.Name = "MenuItemPackage";
        	this.MenuItemPackage.Size = new System.Drawing.Size(216, 22);
        	this.MenuItemPackage.Text = "打包数据以日期查询";
        	this.MenuItemPackage.Click += new System.EventHandler(this.MenuItemPackage_Click);
        	// 
        	// packageReportByCartonToolStripMenuItem
        	// 
        	this.packageReportByCartonToolStripMenuItem.Image = global::CSICPR.Properties.Resources.application_form;
        	this.packageReportByCartonToolStripMenuItem.Name = "packageReportByCartonToolStripMenuItem";
        	this.packageReportByCartonToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
        	this.packageReportByCartonToolStripMenuItem.Text = "打包数据以托号查询";
        	this.packageReportByCartonToolStripMenuItem.Click += new System.EventHandler(this.packageReportByCartonToolStripMenuItem_Click);
        	// 
        	// packageReportByModuleToolStripMenuItem
        	// 
        	this.packageReportByModuleToolStripMenuItem.Image = global::CSICPR.Properties.Resources.application_form;
        	this.packageReportByModuleToolStripMenuItem.Name = "packageReportByModuleToolStripMenuItem";
        	this.packageReportByModuleToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
        	this.packageReportByModuleToolStripMenuItem.Text = "Package Report By Module";
        	this.packageReportByModuleToolStripMenuItem.Click += new System.EventHandler(this.packageReportByModuleToolStripMenuItem_Click);
        	// 
        	// ToolStripMenuItemStorageDataUpQuery
        	// 
        	this.ToolStripMenuItemStorageDataUpQuery.Image = global::CSICPR.Properties.Resources.application_form;
        	this.ToolStripMenuItemStorageDataUpQuery.Name = "ToolStripMenuItemStorageDataUpQuery";
        	this.ToolStripMenuItemStorageDataUpQuery.Size = new System.Drawing.Size(216, 22);
        	this.ToolStripMenuItemStorageDataUpQuery.Text = "入库数据上传查询";
        	this.ToolStripMenuItemStorageDataUpQuery.Click += new System.EventHandler(this.SAPStorageReportToolStripMenuItem_Click);
        	// 
        	// ToolStripMenuItemStorageResultQuery
        	// 
        	this.ToolStripMenuItemStorageResultQuery.Image = global::CSICPR.Properties.Resources.application_form;
        	this.ToolStripMenuItemStorageResultQuery.Name = "ToolStripMenuItemStorageResultQuery";
        	this.ToolStripMenuItemStorageResultQuery.Size = new System.Drawing.Size(216, 22);
        	this.ToolStripMenuItemStorageResultQuery.Text = "入库数据处理结果查询";
        	this.ToolStripMenuItemStorageResultQuery.Click += new System.EventHandler(this.SAPStorageDReportToolStripMenuItem_Click);
        	// 
        	// ToolStripMenuItemOrderQuery
        	// 
        	this.ToolStripMenuItemOrderQuery.Image = global::CSICPR.Properties.Resources.application_form;
        	this.ToolStripMenuItemOrderQuery.Name = "ToolStripMenuItemOrderQuery";
        	this.ToolStripMenuItemOrderQuery.Size = new System.Drawing.Size(216, 22);
        	this.ToolStripMenuItemOrderQuery.Text = "工单查询";
        	this.ToolStripMenuItemOrderQuery.Click += new System.EventHandler(this.SAPWOQueryToolStripMenuItem_Click);
        	// 
        	// uploadStatusQueryToolStripMenuItem
        	// 
        	this.uploadStatusQueryToolStripMenuItem.Image = global::CSICPR.Properties.Resources.application_form;
        	this.uploadStatusQueryToolStripMenuItem.Name = "uploadStatusQueryToolStripMenuItem";
        	this.uploadStatusQueryToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
        	this.uploadStatusQueryToolStripMenuItem.Text = "Upload Status Query";
        	this.uploadStatusQueryToolStripMenuItem.Click += new System.EventHandler(this.UploadStatusQueryToolStripMenuItemClick);
        	// 
        	// toolStripSeparator6
        	// 
        	this.toolStripSeparator6.Name = "toolStripSeparator6";
        	this.toolStripSeparator6.Size = new System.Drawing.Size(6, 61);
        	// 
        	// toolStripSplitButton4
        	// 
        	this.toolStripSplitButton4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem1,
			this.MOChangeOverToolStripMenuItem,
			this.cellLossToolStripMenuItem,
			this.productionWarehouseToolStripMenuItem});
        	this.toolStripSplitButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton4.Image")));
        	this.toolStripSplitButton4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.toolStripSplitButton4.Name = "toolStripSplitButton4";
        	this.toolStripSplitButton4.Size = new System.Drawing.Size(91, 58);
        	this.toolStripSplitButton4.Text = "Raw Material";
        	this.toolStripSplitButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.toolStripSplitButton4.ToolTipText = "Material";
        	this.toolStripSplitButton4.ButtonClick += new System.EventHandler(this.ToolStripSplitButton4ButtonClick);
        	// 
        	// toolStripMenuItem1
        	// 
        	this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem2,
			this.toolStripMenuItem6,
			this.toolStripMenuItem9,
			this.toolStripMenuItem12,
			this.toolStripMenuItem15,
			this.toolStripMenuItem18,
			this.toolStripMenuItem21,
			this.toolStripMenuItem24,
			this.toolStripMenuItem27,
			this.toolStripMenuItem30,
			this.toolStripMenuItem33,
			this.toolStripMenuItem36,
			this.toolStripMenuItem39,
			this.toolStripMenuItem42,
			this.toolStripMenuItem45});
        	this.toolStripMenuItem1.Name = "toolStripMenuItem1";
        	this.toolStripMenuItem1.Size = new System.Drawing.Size(195, 22);
        	this.toolStripMenuItem1.Text = "Production Warehouse";
        	// 
        	// toolStripMenuItem2
        	// 
        	this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem3,
			this.toolStripMenuItem4,
			this.toolStripMenuItem5});
        	this.toolStripMenuItem2.Name = "toolStripMenuItem2";
        	this.toolStripMenuItem2.Size = new System.Drawing.Size(166, 22);
        	this.toolStripMenuItem2.Text = "Cells";
        	this.toolStripMenuItem2.Click += new System.EventHandler(this.ToolStripMenuItem2Click);
        	// 
        	// toolStripMenuItem3
        	// 
        	this.toolStripMenuItem3.Name = "toolStripMenuItem3";
        	this.toolStripMenuItem3.Size = new System.Drawing.Size(152, 22);
        	this.toolStripMenuItem3.Text = "Line A";
        	this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
        	// 
        	// toolStripMenuItem4
        	// 
        	this.toolStripMenuItem4.Name = "toolStripMenuItem4";
        	this.toolStripMenuItem4.Size = new System.Drawing.Size(152, 22);
        	this.toolStripMenuItem4.Text = "Line B";
        	this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
        	// 
        	// toolStripMenuItem5
        	// 
        	this.toolStripMenuItem5.Name = "toolStripMenuItem5";
        	this.toolStripMenuItem5.Size = new System.Drawing.Size(152, 22);
        	this.toolStripMenuItem5.Text = "Line D";
        	this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
        	// 
        	// toolStripMenuItem6
        	// 
        	this.toolStripMenuItem6.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem7,
			this.toolStripMenuItem8});
        	this.toolStripMenuItem6.Name = "toolStripMenuItem6";
        	this.toolStripMenuItem6.Size = new System.Drawing.Size(166, 22);
        	this.toolStripMenuItem6.Text = "Glass";
        	// 
        	// toolStripMenuItem7
        	// 
        	this.toolStripMenuItem7.Name = "toolStripMenuItem7";
        	this.toolStripMenuItem7.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem7.Text = "Line A";
        	this.toolStripMenuItem7.Click += new System.EventHandler(this.toolStripMenuItem7_Click);
        	// 
        	// toolStripMenuItem8
        	// 
        	this.toolStripMenuItem8.Name = "toolStripMenuItem8";
        	this.toolStripMenuItem8.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem8.Text = "Line B";
        	this.toolStripMenuItem8.Click += new System.EventHandler(this.toolStripMenuItem8_Click);
        	// 
        	// toolStripMenuItem9
        	// 
        	this.toolStripMenuItem9.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem10,
			this.toolStripMenuItem11});
        	this.toolStripMenuItem9.Name = "toolStripMenuItem9";
        	this.toolStripMenuItem9.Size = new System.Drawing.Size(166, 22);
        	this.toolStripMenuItem9.Text = "406 EVA";
        	// 
        	// toolStripMenuItem10
        	// 
        	this.toolStripMenuItem10.Name = "toolStripMenuItem10";
        	this.toolStripMenuItem10.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem10.Text = "Line A";
        	this.toolStripMenuItem10.Click += new System.EventHandler(this.toolStripMenuItem10_Click);
        	// 
        	// toolStripMenuItem11
        	// 
        	this.toolStripMenuItem11.Name = "toolStripMenuItem11";
        	this.toolStripMenuItem11.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem11.Text = "Line B";
        	this.toolStripMenuItem11.Click += new System.EventHandler(this.toolStripMenuItem11_Click);
        	// 
        	// toolStripMenuItem12
        	// 
        	this.toolStripMenuItem12.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem13,
			this.toolStripMenuItem14});
        	this.toolStripMenuItem12.Name = "toolStripMenuItem12";
        	this.toolStripMenuItem12.Size = new System.Drawing.Size(166, 22);
        	this.toolStripMenuItem12.Text = "Ribbon";
        	// 
        	// toolStripMenuItem13
        	// 
        	this.toolStripMenuItem13.Name = "toolStripMenuItem13";
        	this.toolStripMenuItem13.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem13.Text = "Line A";
        	this.toolStripMenuItem13.Click += new System.EventHandler(this.toolStripMenuItem13_Click);
        	// 
        	// toolStripMenuItem14
        	// 
        	this.toolStripMenuItem14.Name = "toolStripMenuItem14";
        	this.toolStripMenuItem14.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem14.Text = "Line B";
        	this.toolStripMenuItem14.Click += new System.EventHandler(this.toolStripMenuItem14_Click);
        	// 
        	// toolStripMenuItem15
        	// 
        	this.toolStripMenuItem15.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem16,
			this.toolStripMenuItem17});
        	this.toolStripMenuItem15.Name = "toolStripMenuItem15";
        	this.toolStripMenuItem15.Size = new System.Drawing.Size(166, 22);
        	this.toolStripMenuItem15.Text = "806 EVA";
        	// 
        	// toolStripMenuItem16
        	// 
        	this.toolStripMenuItem16.Name = "toolStripMenuItem16";
        	this.toolStripMenuItem16.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem16.Text = "Line A";
        	this.toolStripMenuItem16.Click += new System.EventHandler(this.toolStripMenuItem16_Click);
        	// 
        	// toolStripMenuItem17
        	// 
        	this.toolStripMenuItem17.Name = "toolStripMenuItem17";
        	this.toolStripMenuItem17.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem17.Text = "Line B";
        	this.toolStripMenuItem17.Click += new System.EventHandler(this.toolStripMenuItem17_Click);
        	// 
        	// toolStripMenuItem18
        	// 
        	this.toolStripMenuItem18.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem19,
			this.toolStripMenuItem20});
        	this.toolStripMenuItem18.Name = "toolStripMenuItem18";
        	this.toolStripMenuItem18.Size = new System.Drawing.Size(166, 22);
        	this.toolStripMenuItem18.Text = "BackSheet";
        	// 
        	// toolStripMenuItem19
        	// 
        	this.toolStripMenuItem19.Name = "toolStripMenuItem19";
        	this.toolStripMenuItem19.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem19.Text = "Line A";
        	this.toolStripMenuItem19.Click += new System.EventHandler(this.toolStripMenuItem19_Click);
        	// 
        	// toolStripMenuItem20
        	// 
        	this.toolStripMenuItem20.Name = "toolStripMenuItem20";
        	this.toolStripMenuItem20.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem20.Text = "Line B";
        	this.toolStripMenuItem20.Click += new System.EventHandler(this.toolStripMenuItem20_Click);
        	// 
        	// toolStripMenuItem21
        	// 
        	this.toolStripMenuItem21.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem22,
			this.toolStripMenuItem23});
        	this.toolStripMenuItem21.Name = "toolStripMenuItem21";
        	this.toolStripMenuItem21.Size = new System.Drawing.Size(166, 22);
        	this.toolStripMenuItem21.Text = "J-Box";
        	// 
        	// toolStripMenuItem22
        	// 
        	this.toolStripMenuItem22.Name = "toolStripMenuItem22";
        	this.toolStripMenuItem22.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem22.Text = "Line A";
        	this.toolStripMenuItem22.Click += new System.EventHandler(this.toolStripMenuItem22_Click);
        	// 
        	// toolStripMenuItem23
        	// 
        	this.toolStripMenuItem23.Name = "toolStripMenuItem23";
        	this.toolStripMenuItem23.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem23.Text = "Line B";
        	this.toolStripMenuItem23.Click += new System.EventHandler(this.toolStripMenuItem23_Click);
        	// 
        	// toolStripMenuItem24
        	// 
        	this.toolStripMenuItem24.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem25,
			this.toolStripMenuItem26});
        	this.toolStripMenuItem24.Name = "toolStripMenuItem24";
        	this.toolStripMenuItem24.Size = new System.Drawing.Size(166, 22);
        	this.toolStripMenuItem24.Text = "Frame(short)";
        	// 
        	// toolStripMenuItem25
        	// 
        	this.toolStripMenuItem25.Name = "toolStripMenuItem25";
        	this.toolStripMenuItem25.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem25.Text = "Line A";
        	this.toolStripMenuItem25.Click += new System.EventHandler(this.toolStripMenuItem25_Click);
        	// 
        	// toolStripMenuItem26
        	// 
        	this.toolStripMenuItem26.Name = "toolStripMenuItem26";
        	this.toolStripMenuItem26.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem26.Text = "Line B";
        	this.toolStripMenuItem26.Click += new System.EventHandler(this.toolStripMenuItem26_Click);
        	// 
        	// toolStripMenuItem27
        	// 
        	this.toolStripMenuItem27.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem28,
			this.toolStripMenuItem29});
        	this.toolStripMenuItem27.Name = "toolStripMenuItem27";
        	this.toolStripMenuItem27.Size = new System.Drawing.Size(166, 22);
        	this.toolStripMenuItem27.Text = "Frame(long)";
        	// 
        	// toolStripMenuItem28
        	// 
        	this.toolStripMenuItem28.Name = "toolStripMenuItem28";
        	this.toolStripMenuItem28.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem28.Text = "Line A";
        	this.toolStripMenuItem28.Click += new System.EventHandler(this.toolStripMenuItem28_Click);
        	// 
        	// toolStripMenuItem29
        	// 
        	this.toolStripMenuItem29.Name = "toolStripMenuItem29";
        	this.toolStripMenuItem29.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem29.Text = "Line B";
        	this.toolStripMenuItem29.Click += new System.EventHandler(this.toolStripMenuItem29_Click);
        	// 
        	// toolStripMenuItem30
        	// 
        	this.toolStripMenuItem30.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem31,
			this.toolStripMenuItem32});
        	this.toolStripMenuItem30.Name = "toolStripMenuItem30";
        	this.toolStripMenuItem30.Size = new System.Drawing.Size(166, 22);
        	this.toolStripMenuItem30.Text = "CrossBar";
        	// 
        	// toolStripMenuItem31
        	// 
        	this.toolStripMenuItem31.Name = "toolStripMenuItem31";
        	this.toolStripMenuItem31.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem31.Text = "Line A";
        	this.toolStripMenuItem31.Click += new System.EventHandler(this.toolStripMenuItem31_Click);
        	// 
        	// toolStripMenuItem32
        	// 
        	this.toolStripMenuItem32.Name = "toolStripMenuItem32";
        	this.toolStripMenuItem32.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem32.Text = "Line B";
        	this.toolStripMenuItem32.Click += new System.EventHandler(this.toolStripMenuItem32_Click);
        	// 
        	// toolStripMenuItem33
        	// 
        	this.toolStripMenuItem33.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem34,
			this.toolStripMenuItem35});
        	this.toolStripMenuItem33.Name = "toolStripMenuItem33";
        	this.toolStripMenuItem33.Size = new System.Drawing.Size(166, 22);
        	this.toolStripMenuItem33.Text = "Sealnat MS 930";
        	// 
        	// toolStripMenuItem34
        	// 
        	this.toolStripMenuItem34.Name = "toolStripMenuItem34";
        	this.toolStripMenuItem34.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem34.Text = "Line A";
        	this.toolStripMenuItem34.Click += new System.EventHandler(this.toolStripMenuItem34_Click);
        	// 
        	// toolStripMenuItem35
        	// 
        	this.toolStripMenuItem35.Name = "toolStripMenuItem35";
        	this.toolStripMenuItem35.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem35.Text = "Line B";
        	this.toolStripMenuItem35.Click += new System.EventHandler(this.toolStripMenuItem35_Click);
        	// 
        	// toolStripMenuItem36
        	// 
        	this.toolStripMenuItem36.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem37,
			this.toolStripMenuItem38});
        	this.toolStripMenuItem36.Name = "toolStripMenuItem36";
        	this.toolStripMenuItem36.Size = new System.Drawing.Size(166, 22);
        	this.toolStripMenuItem36.Text = "Sealnat 1521A";
        	// 
        	// toolStripMenuItem37
        	// 
        	this.toolStripMenuItem37.Name = "toolStripMenuItem37";
        	this.toolStripMenuItem37.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem37.Text = "Line A";
        	this.toolStripMenuItem37.Click += new System.EventHandler(this.toolStripMenuItem37_Click);
        	// 
        	// toolStripMenuItem38
        	// 
        	this.toolStripMenuItem38.Name = "toolStripMenuItem38";
        	this.toolStripMenuItem38.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem38.Text = "Line B";
        	this.toolStripMenuItem38.Click += new System.EventHandler(this.toolStripMenuItem38_Click);
        	// 
        	// toolStripMenuItem39
        	// 
        	this.toolStripMenuItem39.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem40,
			this.toolStripMenuItem41});
        	this.toolStripMenuItem39.Name = "toolStripMenuItem39";
        	this.toolStripMenuItem39.Size = new System.Drawing.Size(166, 22);
        	this.toolStripMenuItem39.Text = "Sealnat 1527";
        	// 
        	// toolStripMenuItem40
        	// 
        	this.toolStripMenuItem40.Name = "toolStripMenuItem40";
        	this.toolStripMenuItem40.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem40.Text = "Line A";
        	this.toolStripMenuItem40.Click += new System.EventHandler(this.toolStripMenuItem40_Click);
        	// 
        	// toolStripMenuItem41
        	// 
        	this.toolStripMenuItem41.Name = "toolStripMenuItem41";
        	this.toolStripMenuItem41.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem41.Text = "Line B";
        	this.toolStripMenuItem41.Click += new System.EventHandler(this.toolStripMenuItem41_Click);
        	// 
        	// toolStripMenuItem42
        	// 
        	this.toolStripMenuItem42.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem43,
			this.toolStripMenuItem44});
        	this.toolStripMenuItem42.Name = "toolStripMenuItem42";
        	this.toolStripMenuItem42.Size = new System.Drawing.Size(166, 22);
        	this.toolStripMenuItem42.Text = "Sealnat 1521B";
        	// 
        	// toolStripMenuItem43
        	// 
        	this.toolStripMenuItem43.Name = "toolStripMenuItem43";
        	this.toolStripMenuItem43.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem43.Text = "Line A";
        	this.toolStripMenuItem43.Click += new System.EventHandler(this.toolStripMenuItem43_Click);
        	// 
        	// toolStripMenuItem44
        	// 
        	this.toolStripMenuItem44.Name = "toolStripMenuItem44";
        	this.toolStripMenuItem44.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem44.Text = "Line B";
        	this.toolStripMenuItem44.Click += new System.EventHandler(this.toolStripMenuItem44_Click);
        	// 
        	// toolStripMenuItem45
        	// 
        	this.toolStripMenuItem45.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem46,
			this.toolStripMenuItem47});
        	this.toolStripMenuItem45.Name = "toolStripMenuItem45";
        	this.toolStripMenuItem45.Size = new System.Drawing.Size(166, 22);
        	this.toolStripMenuItem45.Text = "Sealnat MS 9371B";
        	// 
        	// toolStripMenuItem46
        	// 
        	this.toolStripMenuItem46.Name = "toolStripMenuItem46";
        	this.toolStripMenuItem46.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem46.Text = "Line A";
        	this.toolStripMenuItem46.Click += new System.EventHandler(this.toolStripMenuItem46_Click);
        	// 
        	// toolStripMenuItem47
        	// 
        	this.toolStripMenuItem47.Name = "toolStripMenuItem47";
        	this.toolStripMenuItem47.Size = new System.Drawing.Size(107, 22);
        	this.toolStripMenuItem47.Text = "Line B";
        	this.toolStripMenuItem47.Click += new System.EventHandler(this.toolStripMenuItem47_Click);
        	// 
        	// MOChangeOverToolStripMenuItem
        	// 
        	this.MOChangeOverToolStripMenuItem.Name = "MOChangeOverToolStripMenuItem";
        	this.MOChangeOverToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
        	this.MOChangeOverToolStripMenuItem.Text = "MO ChangeOver";
        	this.MOChangeOverToolStripMenuItem.Click += new System.EventHandler(this.mOChangeOverToolStripMenuItem_Click);
        	// 
        	// cellLossToolStripMenuItem
        	// 
        	this.cellLossToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lineAToolStripMenuItem1,
			this.lineBToolStripMenuItem1,
			this.lineDToolStripMenuItem1});
        	this.cellLossToolStripMenuItem.Name = "cellLossToolStripMenuItem";
        	this.cellLossToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
        	this.cellLossToolStripMenuItem.Text = "Cell Loss";
        	// 
        	// lineAToolStripMenuItem1
        	// 
        	this.lineAToolStripMenuItem1.Name = "lineAToolStripMenuItem1";
        	this.lineAToolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
        	this.lineAToolStripMenuItem1.Text = "Line A";
        	this.lineAToolStripMenuItem1.Click += new System.EventHandler(this.lineAToolStripMenuItem1_Click);
        	// 
        	// lineBToolStripMenuItem1
        	// 
        	this.lineBToolStripMenuItem1.Name = "lineBToolStripMenuItem1";
        	this.lineBToolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
        	this.lineBToolStripMenuItem1.Text = "Line B";
        	this.lineBToolStripMenuItem1.Click += new System.EventHandler(this.lineBToolStripMenuItem1_Click);
        	// 
        	// lineDToolStripMenuItem1
        	// 
        	this.lineDToolStripMenuItem1.Name = "lineDToolStripMenuItem1";
        	this.lineDToolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
        	this.lineDToolStripMenuItem1.Text = "Line D";
        	this.lineDToolStripMenuItem1.Click += new System.EventHandler(this.lineDToolStripMenuItem1_Click);
        	// 
        	// productionWarehouseToolStripMenuItem
        	// 
        	this.productionWarehouseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.glassToolStripMenuItem,
			this.eVAToolStripMenuItem,
			this.ribbonToolStripMenuItem,
			this.eVAToolStripMenuItem1,
			this.backSheetToolStripMenuItem,
			this.jBoxToolStripMenuItem,
			this.frameshortToolStripMenuItem,
			this.framelongToolStripMenuItem,
			this.crossBarToolStripMenuItem,
			this.sealnatMS930ToolStripMenuItem,
			this.sealnat1521AToolStripMenuItem,
			this.sealnat1527ToolStripMenuItem,
			this.sealnat1521BToolStripMenuItem,
			this.sealnatMS9371BToolStripMenuItem});
        	this.productionWarehouseToolStripMenuItem.Name = "productionWarehouseToolStripMenuItem";
        	this.productionWarehouseToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
        	this.productionWarehouseToolStripMenuItem.Text = "RawMaterial Scrap";
        	// 
        	// glassToolStripMenuItem
        	// 
        	this.glassToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lineAToolStripMenuItem,
			this.lineBToolStripMenuItem});
        	this.glassToolStripMenuItem.Name = "glassToolStripMenuItem";
        	this.glassToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
        	this.glassToolStripMenuItem.Text = "Glass";
        	// 
        	// lineAToolStripMenuItem
        	// 
        	this.lineAToolStripMenuItem.Name = "lineAToolStripMenuItem";
        	this.lineAToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
        	this.lineAToolStripMenuItem.Text = "Line A";
        	this.lineAToolStripMenuItem.Click += new System.EventHandler(this.lineAToolStripMenuItem_Click_1);
        	// 
        	// lineBToolStripMenuItem
        	// 
        	this.lineBToolStripMenuItem.Name = "lineBToolStripMenuItem";
        	this.lineBToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
        	this.lineBToolStripMenuItem.Text = "Line B";
        	this.lineBToolStripMenuItem.Click += new System.EventHandler(this.lineBToolStripMenuItem_Click_1);
        	// 
        	// eVAToolStripMenuItem
        	// 
        	this.eVAToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lineAToolStripMenuItem2,
			this.lineBToolStripMenuItem2});
        	this.eVAToolStripMenuItem.Name = "eVAToolStripMenuItem";
        	this.eVAToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
        	this.eVAToolStripMenuItem.Text = "406 EVA";
        	// 
        	// lineAToolStripMenuItem2
        	// 
        	this.lineAToolStripMenuItem2.Name = "lineAToolStripMenuItem2";
        	this.lineAToolStripMenuItem2.Size = new System.Drawing.Size(107, 22);
        	this.lineAToolStripMenuItem2.Text = "Line A";
        	this.lineAToolStripMenuItem2.Click += new System.EventHandler(this.lineAToolStripMenuItem2_Click_1);
        	// 
        	// lineBToolStripMenuItem2
        	// 
        	this.lineBToolStripMenuItem2.Name = "lineBToolStripMenuItem2";
        	this.lineBToolStripMenuItem2.Size = new System.Drawing.Size(107, 22);
        	this.lineBToolStripMenuItem2.Text = "Line B";
        	this.lineBToolStripMenuItem2.Click += new System.EventHandler(this.lineBToolStripMenuItem2_Click_1);
        	// 
        	// ribbonToolStripMenuItem
        	// 
        	this.ribbonToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lineAToolStripMenuItem3,
			this.lineBToolStripMenuItem3});
        	this.ribbonToolStripMenuItem.Name = "ribbonToolStripMenuItem";
        	this.ribbonToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
        	this.ribbonToolStripMenuItem.Text = "Ribbon";
        	// 
        	// lineAToolStripMenuItem3
        	// 
        	this.lineAToolStripMenuItem3.Name = "lineAToolStripMenuItem3";
        	this.lineAToolStripMenuItem3.Size = new System.Drawing.Size(107, 22);
        	this.lineAToolStripMenuItem3.Text = "Line A";
        	this.lineAToolStripMenuItem3.Click += new System.EventHandler(this.lineAToolStripMenuItem3_Click_1);
        	// 
        	// lineBToolStripMenuItem3
        	// 
        	this.lineBToolStripMenuItem3.Name = "lineBToolStripMenuItem3";
        	this.lineBToolStripMenuItem3.Size = new System.Drawing.Size(107, 22);
        	this.lineBToolStripMenuItem3.Text = "Line B";
        	this.lineBToolStripMenuItem3.Click += new System.EventHandler(this.lineBToolStripMenuItem3_Click_1);
        	// 
        	// eVAToolStripMenuItem1
        	// 
        	this.eVAToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lineAToolStripMenuItem4,
			this.lineBToolStripMenuItem4});
        	this.eVAToolStripMenuItem1.Name = "eVAToolStripMenuItem1";
        	this.eVAToolStripMenuItem1.Size = new System.Drawing.Size(166, 22);
        	this.eVAToolStripMenuItem1.Text = "806 EVA";
        	// 
        	// lineAToolStripMenuItem4
        	// 
        	this.lineAToolStripMenuItem4.Name = "lineAToolStripMenuItem4";
        	this.lineAToolStripMenuItem4.Size = new System.Drawing.Size(107, 22);
        	this.lineAToolStripMenuItem4.Text = "Line A";
        	this.lineAToolStripMenuItem4.Click += new System.EventHandler(this.lineAToolStripMenuItem4_Click_1);
        	// 
        	// lineBToolStripMenuItem4
        	// 
        	this.lineBToolStripMenuItem4.Name = "lineBToolStripMenuItem4";
        	this.lineBToolStripMenuItem4.Size = new System.Drawing.Size(107, 22);
        	this.lineBToolStripMenuItem4.Text = "Line B";
        	this.lineBToolStripMenuItem4.Click += new System.EventHandler(this.lineBToolStripMenuItem4_Click_1);
        	// 
        	// backSheetToolStripMenuItem
        	// 
        	this.backSheetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lineAToolStripMenuItem5,
			this.lineBToolStripMenuItem5});
        	this.backSheetToolStripMenuItem.Name = "backSheetToolStripMenuItem";
        	this.backSheetToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
        	this.backSheetToolStripMenuItem.Text = "BackSheet";
        	// 
        	// lineAToolStripMenuItem5
        	// 
        	this.lineAToolStripMenuItem5.Name = "lineAToolStripMenuItem5";
        	this.lineAToolStripMenuItem5.Size = new System.Drawing.Size(107, 22);
        	this.lineAToolStripMenuItem5.Text = "Line A";
        	this.lineAToolStripMenuItem5.Click += new System.EventHandler(this.lineAToolStripMenuItem5_Click_1);
        	// 
        	// lineBToolStripMenuItem5
        	// 
        	this.lineBToolStripMenuItem5.Name = "lineBToolStripMenuItem5";
        	this.lineBToolStripMenuItem5.Size = new System.Drawing.Size(107, 22);
        	this.lineBToolStripMenuItem5.Text = "Line B";
        	this.lineBToolStripMenuItem5.Click += new System.EventHandler(this.lineBToolStripMenuItem5_Click_1);
        	// 
        	// jBoxToolStripMenuItem
        	// 
        	this.jBoxToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lineAToolStripMenuItem6,
			this.lineBToolStripMenuItem6});
        	this.jBoxToolStripMenuItem.Name = "jBoxToolStripMenuItem";
        	this.jBoxToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
        	this.jBoxToolStripMenuItem.Text = "J-Box";
        	// 
        	// lineAToolStripMenuItem6
        	// 
        	this.lineAToolStripMenuItem6.Name = "lineAToolStripMenuItem6";
        	this.lineAToolStripMenuItem6.Size = new System.Drawing.Size(107, 22);
        	this.lineAToolStripMenuItem6.Text = "Line A";
        	this.lineAToolStripMenuItem6.Click += new System.EventHandler(this.lineAToolStripMenuItem6_Click_1);
        	// 
        	// lineBToolStripMenuItem6
        	// 
        	this.lineBToolStripMenuItem6.Name = "lineBToolStripMenuItem6";
        	this.lineBToolStripMenuItem6.Size = new System.Drawing.Size(107, 22);
        	this.lineBToolStripMenuItem6.Text = "Line B";
        	this.lineBToolStripMenuItem6.Click += new System.EventHandler(this.lineBToolStripMenuItem6_Click_1);
        	// 
        	// frameshortToolStripMenuItem
        	// 
        	this.frameshortToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lineAToolStripMenuItem7,
			this.lineBToolStripMenuItem7});
        	this.frameshortToolStripMenuItem.Name = "frameshortToolStripMenuItem";
        	this.frameshortToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
        	this.frameshortToolStripMenuItem.Text = "Frame(short)";
        	// 
        	// lineAToolStripMenuItem7
        	// 
        	this.lineAToolStripMenuItem7.Name = "lineAToolStripMenuItem7";
        	this.lineAToolStripMenuItem7.Size = new System.Drawing.Size(107, 22);
        	this.lineAToolStripMenuItem7.Text = "Line A";
        	this.lineAToolStripMenuItem7.Click += new System.EventHandler(this.lineAToolStripMenuItem7_Click_1);
        	// 
        	// lineBToolStripMenuItem7
        	// 
        	this.lineBToolStripMenuItem7.Name = "lineBToolStripMenuItem7";
        	this.lineBToolStripMenuItem7.Size = new System.Drawing.Size(107, 22);
        	this.lineBToolStripMenuItem7.Text = "Line B";
        	this.lineBToolStripMenuItem7.Click += new System.EventHandler(this.lineBToolStripMenuItem7_Click_1);
        	// 
        	// framelongToolStripMenuItem
        	// 
        	this.framelongToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lineAToolStripMenuItem8,
			this.lineBToolStripMenuItem8});
        	this.framelongToolStripMenuItem.Name = "framelongToolStripMenuItem";
        	this.framelongToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
        	this.framelongToolStripMenuItem.Text = "Frame(long)";
        	// 
        	// lineAToolStripMenuItem8
        	// 
        	this.lineAToolStripMenuItem8.Name = "lineAToolStripMenuItem8";
        	this.lineAToolStripMenuItem8.Size = new System.Drawing.Size(107, 22);
        	this.lineAToolStripMenuItem8.Text = "Line A";
        	this.lineAToolStripMenuItem8.Click += new System.EventHandler(this.lineAToolStripMenuItem8_Click_1);
        	// 
        	// lineBToolStripMenuItem8
        	// 
        	this.lineBToolStripMenuItem8.Name = "lineBToolStripMenuItem8";
        	this.lineBToolStripMenuItem8.Size = new System.Drawing.Size(107, 22);
        	this.lineBToolStripMenuItem8.Text = "Line B";
        	this.lineBToolStripMenuItem8.Click += new System.EventHandler(this.lineBToolStripMenuItem8_Click_1);
        	// 
        	// crossBarToolStripMenuItem
        	// 
        	this.crossBarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lineAToolStripMenuItem9,
			this.lineBToolStripMenuItem9});
        	this.crossBarToolStripMenuItem.Name = "crossBarToolStripMenuItem";
        	this.crossBarToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
        	this.crossBarToolStripMenuItem.Text = "CrossBar";
        	// 
        	// lineAToolStripMenuItem9
        	// 
        	this.lineAToolStripMenuItem9.Name = "lineAToolStripMenuItem9";
        	this.lineAToolStripMenuItem9.Size = new System.Drawing.Size(107, 22);
        	this.lineAToolStripMenuItem9.Text = "Line A";
        	this.lineAToolStripMenuItem9.Click += new System.EventHandler(this.lineAToolStripMenuItem9_Click_1);
        	// 
        	// lineBToolStripMenuItem9
        	// 
        	this.lineBToolStripMenuItem9.Name = "lineBToolStripMenuItem9";
        	this.lineBToolStripMenuItem9.Size = new System.Drawing.Size(107, 22);
        	this.lineBToolStripMenuItem9.Text = "Line B";
        	this.lineBToolStripMenuItem9.Click += new System.EventHandler(this.lineBToolStripMenuItem9_Click_1);
        	// 
        	// sealnatMS930ToolStripMenuItem
        	// 
        	this.sealnatMS930ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lineAToolStripMenuItem10,
			this.lineBToolStripMenuItem10});
        	this.sealnatMS930ToolStripMenuItem.Name = "sealnatMS930ToolStripMenuItem";
        	this.sealnatMS930ToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
        	this.sealnatMS930ToolStripMenuItem.Text = "Sealnat MS 930";
        	// 
        	// lineAToolStripMenuItem10
        	// 
        	this.lineAToolStripMenuItem10.Name = "lineAToolStripMenuItem10";
        	this.lineAToolStripMenuItem10.Size = new System.Drawing.Size(107, 22);
        	this.lineAToolStripMenuItem10.Text = "Line A";
        	this.lineAToolStripMenuItem10.Click += new System.EventHandler(this.lineAToolStripMenuItem10_Click_1);
        	// 
        	// lineBToolStripMenuItem10
        	// 
        	this.lineBToolStripMenuItem10.Name = "lineBToolStripMenuItem10";
        	this.lineBToolStripMenuItem10.Size = new System.Drawing.Size(107, 22);
        	this.lineBToolStripMenuItem10.Text = "Line B";
        	this.lineBToolStripMenuItem10.Click += new System.EventHandler(this.lineBToolStripMenuItem10_Click_1);
        	// 
        	// sealnat1521AToolStripMenuItem
        	// 
        	this.sealnat1521AToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lineAToolStripMenuItem11,
			this.lineBToolStripMenuItem11});
        	this.sealnat1521AToolStripMenuItem.Name = "sealnat1521AToolStripMenuItem";
        	this.sealnat1521AToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
        	this.sealnat1521AToolStripMenuItem.Text = "Sealnat 1521A";
        	// 
        	// lineAToolStripMenuItem11
        	// 
        	this.lineAToolStripMenuItem11.Name = "lineAToolStripMenuItem11";
        	this.lineAToolStripMenuItem11.Size = new System.Drawing.Size(107, 22);
        	this.lineAToolStripMenuItem11.Text = "Line A";
        	this.lineAToolStripMenuItem11.Click += new System.EventHandler(this.lineAToolStripMenuItem11_Click_1);
        	// 
        	// lineBToolStripMenuItem11
        	// 
        	this.lineBToolStripMenuItem11.Name = "lineBToolStripMenuItem11";
        	this.lineBToolStripMenuItem11.Size = new System.Drawing.Size(107, 22);
        	this.lineBToolStripMenuItem11.Text = "Line B";
        	this.lineBToolStripMenuItem11.Click += new System.EventHandler(this.lineBToolStripMenuItem11_Click_1);
        	// 
        	// sealnat1527ToolStripMenuItem
        	// 
        	this.sealnat1527ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lineAToolStripMenuItem12,
			this.lineBToolStripMenuItem12});
        	this.sealnat1527ToolStripMenuItem.Name = "sealnat1527ToolStripMenuItem";
        	this.sealnat1527ToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
        	this.sealnat1527ToolStripMenuItem.Text = "Sealnat 1527";
        	// 
        	// lineAToolStripMenuItem12
        	// 
        	this.lineAToolStripMenuItem12.Name = "lineAToolStripMenuItem12";
        	this.lineAToolStripMenuItem12.Size = new System.Drawing.Size(107, 22);
        	this.lineAToolStripMenuItem12.Text = "Line A";
        	this.lineAToolStripMenuItem12.Click += new System.EventHandler(this.lineAToolStripMenuItem12_Click_1);
        	// 
        	// lineBToolStripMenuItem12
        	// 
        	this.lineBToolStripMenuItem12.Name = "lineBToolStripMenuItem12";
        	this.lineBToolStripMenuItem12.Size = new System.Drawing.Size(107, 22);
        	this.lineBToolStripMenuItem12.Text = "Line B";
        	this.lineBToolStripMenuItem12.Click += new System.EventHandler(this.lineBToolStripMenuItem12_Click_1);
        	// 
        	// sealnat1521BToolStripMenuItem
        	// 
        	this.sealnat1521BToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lineAToolStripMenuItem13,
			this.lineBToolStripMenuItem13});
        	this.sealnat1521BToolStripMenuItem.Name = "sealnat1521BToolStripMenuItem";
        	this.sealnat1521BToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
        	this.sealnat1521BToolStripMenuItem.Text = "Sealnat 1521B";
        	// 
        	// lineAToolStripMenuItem13
        	// 
        	this.lineAToolStripMenuItem13.Name = "lineAToolStripMenuItem13";
        	this.lineAToolStripMenuItem13.Size = new System.Drawing.Size(107, 22);
        	this.lineAToolStripMenuItem13.Text = "Line A";
        	this.lineAToolStripMenuItem13.Click += new System.EventHandler(this.lineAToolStripMenuItem13_Click_1);
        	// 
        	// lineBToolStripMenuItem13
        	// 
        	this.lineBToolStripMenuItem13.Name = "lineBToolStripMenuItem13";
        	this.lineBToolStripMenuItem13.Size = new System.Drawing.Size(107, 22);
        	this.lineBToolStripMenuItem13.Text = "Line B";
        	this.lineBToolStripMenuItem13.Click += new System.EventHandler(this.lineBToolStripMenuItem13_Click_1);
        	// 
        	// sealnatMS9371BToolStripMenuItem
        	// 
        	this.sealnatMS9371BToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.lineAToolStripMenuItem14,
			this.lineBToolStripMenuItem14});
        	this.sealnatMS9371BToolStripMenuItem.Name = "sealnatMS9371BToolStripMenuItem";
        	this.sealnatMS9371BToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
        	this.sealnatMS9371BToolStripMenuItem.Text = "Sealnat MS 9371B";
        	// 
        	// lineAToolStripMenuItem14
        	// 
        	this.lineAToolStripMenuItem14.Name = "lineAToolStripMenuItem14";
        	this.lineAToolStripMenuItem14.Size = new System.Drawing.Size(107, 22);
        	this.lineAToolStripMenuItem14.Text = "Line A";
        	this.lineAToolStripMenuItem14.Click += new System.EventHandler(this.lineAToolStripMenuItem14_Click_1);
        	// 
        	// lineBToolStripMenuItem14
        	// 
        	this.lineBToolStripMenuItem14.Name = "lineBToolStripMenuItem14";
        	this.lineBToolStripMenuItem14.Size = new System.Drawing.Size(107, 22);
        	this.lineBToolStripMenuItem14.Text = "Line B";
        	this.lineBToolStripMenuItem14.Click += new System.EventHandler(this.lineBToolStripMenuItem14_Click_1);
        	// 
        	// toolStripSeparator4
        	// 
        	this.toolStripSeparator4.Name = "toolStripSeparator4";
        	this.toolStripSeparator4.Size = new System.Drawing.Size(6, 61);
        	// 
        	// BarcodePrint
        	// 
        	this.BarcodePrint.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.SideBarcodePrint,
			this.二维码商标打印ToolStripMenuItem});
        	this.BarcodePrint.Image = global::CSICPR.Properties.Resources.Barcode;
        	this.BarcodePrint.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.BarcodePrint.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.BarcodePrint.Name = "BarcodePrint";
        	this.BarcodePrint.Size = new System.Drawing.Size(71, 58);
        	this.BarcodePrint.Text = "标签列印";
        	this.BarcodePrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.BarcodePrint.Visible = false;
        	this.BarcodePrint.ButtonClick += new System.EventHandler(this.BarcodePrint_ButtonClick);
        	// 
        	// SideBarcodePrint
        	// 
        	this.SideBarcodePrint.Image = global::CSICPR.Properties.Resources.information;
        	this.SideBarcodePrint.Name = "SideBarcodePrint";
        	this.SideBarcodePrint.Size = new System.Drawing.Size(158, 22);
        	this.SideBarcodePrint.Text = "侧边框条码列印";
        	this.SideBarcodePrint.Click += new System.EventHandler(this.SideBarcodePrint_Click);
        	// 
        	// 二维码商标打印ToolStripMenuItem
        	// 
        	this.二维码商标打印ToolStripMenuItem.Image = global::CSICPR.Properties.Resources.information;
        	this.二维码商标打印ToolStripMenuItem.Name = "二维码商标打印ToolStripMenuItem";
        	this.二维码商标打印ToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
        	this.二维码商标打印ToolStripMenuItem.Text = "二维码商标打印";
        	this.二维码商标打印ToolStripMenuItem.Click += new System.EventHandler(this.二维码商标打印ToolStripMenuItem_Click);
        	// 
        	// toolStripSeparator1
        	// 
        	this.toolStripSeparator1.Name = "toolStripSeparator1";
        	this.toolStripSeparator1.Size = new System.Drawing.Size(6, 61);
        	// 
        	// tssbExit
        	// 
        	this.tssbExit.Image = global::CSICPR.Properties.Resources._return;
        	this.tssbExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        	this.tssbExit.ImageTransparentColor = System.Drawing.Color.Magenta;
        	this.tssbExit.Name = "tssbExit";
        	this.tssbExit.Size = new System.Drawing.Size(36, 58);
        	this.tssbExit.Text = "注销";
        	this.tssbExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
        	this.tssbExit.Click += new System.EventHandler(this.tssbExit_Click);
        	// 
        	// panel2
        	// 
        	this.panel2.Controls.Add(this.menuStrip1);
        	this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
        	this.panel2.Location = new System.Drawing.Point(698, 0);
        	this.panel2.Name = "panel2";
        	this.panel2.Size = new System.Drawing.Size(86, 61);
        	this.panel2.TabIndex = 5;
        	// 
        	// toolStripStatusLabel1
        	// 
        	this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
        	this.toolStripStatusLabel1.Size = new System.Drawing.Size(57, 17);
        	this.toolStripStatusLabel1.Text = "Operator:";
        	// 
        	// tsslCurUserName
        	// 
        	this.tsslCurUserName.AutoSize = false;
        	this.tsslCurUserName.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
			| System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
			| System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
        	this.tsslCurUserName.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
        	this.tsslCurUserName.Name = "tsslCurUserName";
        	this.tsslCurUserName.Size = new System.Drawing.Size(100, 17);
        	this.tsslCurUserName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// toolStripStatusLabel2
        	// 
        	this.toolStripStatusLabel2.Margin = new System.Windows.Forms.Padding(10, 3, 0, 2);
        	this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
        	this.toolStripStatusLabel2.Size = new System.Drawing.Size(65, 17);
        	this.toolStripStatusLabel2.Text = "WorkShop:";
        	// 
        	// tsslCurWorkshop
        	// 
        	this.tsslCurWorkshop.AutoSize = false;
        	this.tsslCurWorkshop.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
			| System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
			| System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
        	this.tsslCurWorkshop.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
        	this.tsslCurWorkshop.Name = "tsslCurWorkshop";
        	this.tsslCurWorkshop.Size = new System.Drawing.Size(100, 17);
        	this.tsslCurWorkshop.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        	// 
        	// statusStrip1
        	// 
        	this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripStatusLabel1,
			this.tsslCurUserName,
			this.toolStripStatusLabel2,
			this.tsslCurWorkshop});
        	this.statusStrip1.Location = new System.Drawing.Point(0, 587);
        	this.statusStrip1.Name = "statusStrip1";
        	this.statusStrip1.Size = new System.Drawing.Size(784, 22);
        	this.statusStrip1.TabIndex = 6;
        	this.statusStrip1.Text = "statusStrip1";
        	// 
        	// FormMain
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(784, 609);
        	this.Controls.Add(this.panel1);
        	this.Controls.Add(this.statusStrip1);
        	this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        	this.IsMdiContainer = true;
        	this.MainMenuStrip = this.menuStrip1;
        	this.Name = "FormMain";
        	this.Text = "CSI Packing System";
        	this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        	this.Load += new System.EventHandler(this.FormMain_Load);
        	this.menuStrip1.ResumeLayout(false);
        	this.menuStrip1.PerformLayout();
        	this.panel1.ResumeLayout(false);
        	this.panel1.PerformLayout();
        	this.toolStrip1.ResumeLayout(false);
        	this.toolStrip1.PerformLayout();
        	this.panel2.ResumeLayout(false);
        	this.panel2.PerformLayout();
        	this.statusStrip1.ResumeLayout(false);
        	this.statusStrip1.PerformLayout();
        	this.ResumeLayout(false);
        	this.PerformLayout();

        }

       

        private System.Windows.Forms.ToolStripMenuItem uploadStatusQueryToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuHelp;
        private System.Windows.Forms.ToolStripMenuItem tstHelpAbput;
        private System.Windows.Forms.ToolStripMenuItem tsbHelpUsing;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem tsbWindowTile;
        private System.Windows.Forms.ToolStripMenuItem tsbWindowHzt;
        private System.Windows.Forms.ToolStripMenuItem tsbWindowVtcl;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSplitButton tssbSystem;
        private System.Windows.Forms.ToolStripMenuItem MenuItemBase;
        private System.Windows.Forms.ToolStripMenuItem accountManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSplitButton tssbChart;
        private System.Windows.Forms.ToolStripMenuItem MenuItemPackage;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel tsslCurUserName;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel tsslCurWorkshop;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem packageConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton tssbExit;
        private System.Windows.Forms.ToolStripMenuItem packageReportByCartonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem packageReportByModuleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LanguageChg;
        private System.Windows.Forms.ToolStripMenuItem tssbCTM;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.ToolStripMenuItem SNPackingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TotalPackingToolStripMenuItem;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton2;
        private System.Windows.Forms.ToolStripMenuItem ALLStorageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SNStorageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RevokeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton3;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemOrderno;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemConfirmReOrder;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemStorageDataUpQuery;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemStorageResultQuery;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemMaterial;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemStorage;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemOrderQuery;
        private System.Windows.Forms.ToolStripMenuItem NormalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MaterialTemplateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MaterialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ManyStorageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ManyStorageSNToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemPackingData;
        private System.Windows.Forms.ToolStripSplitButton BarcodePrint;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem SideBarcodePrint;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemMatainOrder;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemUploadSap;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemUpResult;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemStorageQuery;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemCancel;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemReworkRegister;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSapBatchDataQuery;
        private System.Windows.Forms.ToolStripMenuItem 二维码参数配置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 二维码商标打印ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton4;
        private System.Windows.Forms.ToolStripMenuItem productionWarehouseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MOChangeOverToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cellLossToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lineAToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem lineBToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem lineDToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem glassToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eVAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ribbonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eVAToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem backSheetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jBoxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem frameshortToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem framelongToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crossBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sealnatMS930ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sealnat1527ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sealnat1521BToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sealnatMS9371BToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem22;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem23;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem24;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem25;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem26;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem27;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem28;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem29;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem30;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem31;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem32;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem33;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem34;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem35;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem36;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem37;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem38;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem39;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem40;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem41;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem42;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem43;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem44;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem45;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem46;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem47;
        private System.Windows.Forms.ToolStripMenuItem lineAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lineBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sealnat1521AToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lineAToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem lineBToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem lineAToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem lineBToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem lineAToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem lineBToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem lineAToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem lineBToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem lineAToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem lineBToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem lineAToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem lineBToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem lineAToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem lineBToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem lineAToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem lineBToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem lineAToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem lineBToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem lineAToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem lineBToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem lineAToolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem lineBToolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem lineAToolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem lineBToolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem lineAToolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem lineBToolStripMenuItem14;
    }
}

