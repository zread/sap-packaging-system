﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Xml;
using System.Collections.Generic;
using System.Windows.Forms;

//add by Alex.dong 2011-05-14|for language exchange
using System.Collections;
using System.Runtime.InteropServices;
using System.Text;
using org.in2bits.MyXls;
using System.Net.Mail;

namespace CSICPR
{
    class ToolsClass
    {
        #region"Declare Parameters"
        //FormAnalyse Parameters
        public const int iDateReport = 1;//report by date
        public const int iCartonReport = 2;//report by carton
        public const int iModuleReport = 3;//report by module
        public const int iSerial_Length = 4;
        public const int iDate_Length = 3;
        /// <summary>
        /// 加拿大
        /// </summary>
        public const string CAMO = "CAMO";
        public const string ChangShu = "CS";
        /// <summary>
        /// 车间
        /// </summary>
        public static string sSite = "";
        private static string _PlanCode = "";

        public static string PlanCode
        {
            get { return ToolsClass._PlanCode; }
            set { ToolsClass._PlanCode = value; }
        }
        public const string ReportByCarton = "Report By Carton";
        public const string ReportByDate = "Report By Date";
        public const string ReportByModule = "Report By Module";
        private const string fileName = "Trace";
        private const string NormalMsg = "NORMAL";
        private const string AbnormalMsg = "ABNORMAL";
        public const string WorkShop = "C";//Canadian solar factory
        private static object lockGenerateBoxID;
        public static bool bChipType=true;
        public static string message = "";
        //public const bool NoAutoGenerateBoxID = true;
        #endregion

        #region"Log Function"
        //Record Package System running record
        public static void Log(string msg,string msgtype="", ListView lstV = null, string filePath = "", int fileSize = 0)
        {
            string sDayofWeek = System.DateTime.Now.DayOfWeek.ToString();
            string sFileName = "";

            if(filePath.Equals(""))
                filePath = Application.StartupPath + "\\PackageSystemLog" + "\\" + sDayofWeek;

            if (fileSize == 0)
                fileSize = 1024;

            if (lstV != null)
            {
                //lstV.Items.Add(System.DateTime.Now.ToString() +"   "+ msg);
                if (msgtype.Equals("NORMAL"))
                {
                    //lstV.Items[lstV.Items.Count - 1].ForeColor = System.Drawing.Color.Blue;
                    ListViewItem Item = new ListViewItem();
                    Item.SubItems.Clear();
                    Item.SubItems[0].Text = System.DateTime.Now.ToString() + "   " + msg;// i.ToString();
                    Item.SubItems[0].ForeColor = System.Drawing.Color.Blue;
                    lstV.Items.Add(Item);
                }
                else if (msgtype.Equals("ABNORMAL"))
                {
                    //lstV.Items[lstV.Items.Count - 1].ForeColor = System.Drawing.Color.Red;
                    ListViewItem Item = new ListViewItem();
                    Item.SubItems.Clear();
                    Item.SubItems[0].Text = System.DateTime.Now.ToString() + "   " + msg;// i.ToString();
                    Item.SubItems[0].ForeColor = System.Drawing.Color.Red;
                    lstV.Items.Add(Item);
                }
                lstV.Items[lstV.Items.Count - 1].EnsureVisible();
                if (lstV.Items.Count > 2000)
                    lstV.Items.Clear();
            }

            try
            {
                if (!Directory.Exists(filePath))
                    Directory.CreateDirectory(filePath);

                DirectoryInfo dinfo=new DirectoryInfo(filePath);
                sFileName = getFileName(dinfo, 2 * fileSize);

                filePath = filePath + "\\" + sFileName + ".log";
                if (!File.Exists(filePath))
                {
                    File.Create(filePath).Close();
                }
                
                StreamWriter sw = File.AppendText(filePath);
                sw.WriteLine(System.DateTime.Now);
                sw.WriteLine(msg);
                sw.WriteLine();
                sw.Flush();
                sw.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Write Log Fail\nException:"+ex.Message);
            }
        }

        public static string getFileName(DirectoryInfo path,int fileSize,int fileLife=30)
        {
            DirectoryInfo[] dirs = path.GetDirectories();
            FileInfo[] files = path.GetFiles();
            string tmp="";
            if (files.Length == 0)
            {
                tmp = fileName + "_" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss").Replace("-", "").Replace(" ", "").Replace(":", "").Trim();
                return tmp;
            }
            for (int i = 0; i < files.Length; i++)
            {
                deleteLogFile(files[i], fileLife);
                //if (files[i].Name.IndexOf("Trace") > -1)
                //{
                    //int iLength = files[i].Name.IndexOf('.') - files[i].Name.IndexOf('_') - 1;
                if (files[i].Length / 1024 < fileSize)//// 1024
                {
                    tmp = files[i].Name.Substring(0,files[i].Name.IndexOf('.'));
                    //tmp = int.Parse(files[i].Name.Substring(files[i].Name.IndexOf('_') + 1, iLength));
                    break;
                }
                else
                {
                    if (i == (files.Length - 1))
                        tmp = fileName + "_" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss").Replace("-", "").Replace(" ", "").Replace(":", "").Trim();
                        //tmp = int.Parse(files[i].Name.Substring(files[i].Name.IndexOf('_') + 1, iLength)) + 1;
                }
                //}
            }
            //if (tmp > 10000)
            //    tmp = 1;

            return tmp;
        }

        public static void deleteLogFile(FileInfo file,int fileLife)
        {
            try
            {
                int iDateDaff = DateDiff(DateInterval.Day, file.CreationTime, System.DateTime.Now);
                if (iDateDaff > fileLife)
                    file.Delete();
                    //File.Delete(file.DirectoryName);
            }
            catch
            {
            }
        }

        public enum DateInterval
        {
            Second, Minute, Hour, Day, Week, Month, Quarter, Year
        }
        public static int DateDiff(DateInterval Interval, System.DateTime StartDate, System.DateTime EndDate)
        {
            int lngDateDiffValue = 0;
            System.TimeSpan TS = new System.TimeSpan(EndDate.Ticks - StartDate.Ticks);
            switch (Interval)
            {
                case DateInterval.Second:
                    lngDateDiffValue = (int)TS.TotalSeconds;
                    break;
                case DateInterval.Minute:
                    lngDateDiffValue = (int)TS.TotalMinutes;
                    break;
                case DateInterval.Hour:
                    lngDateDiffValue = (int)TS.TotalHours;
                    break;
                case DateInterval.Day:
                    lngDateDiffValue = (int)TS.Days;
                    break;
                case DateInterval.Week:
                    lngDateDiffValue = (int)(TS.Days / 7);
                    break;
                case DateInterval.Month:
                    lngDateDiffValue = (int)(TS.Days / 30);
                    break;
                case DateInterval.Quarter:
                    lngDateDiffValue = (int)((TS.Days / 30) / 3);
                    break;
                case DateInterval.Year:
                    lngDateDiffValue = (int)(TS.Days / 365);
                    break;
            }
            return (lngDateDiffValue);
        }
        #endregion

        #region"Method Public"
        //2011-05-24|add by alex.dong|for Label string compare
        public static bool Like(string strText, string strPattern)
        {
            strPattern = strPattern.Replace("*", @"\w*");
            return System.Text.RegularExpressions.Regex.IsMatch(strText, strPattern);
        }
        //四舍五入
        public static string Round(double d, int i)
        {
            if (d >= 0)
            {
                d += 5 * Math.Pow(10, -(i + 1));
            }
            else
            {
                d += -5 * Math.Pow(10, -(i + 1));
            }
            string str = d.ToString();
            string[] strs = str.Split('.');
            string prestr = strs[0];
            string poststr = "";
            if (strs.Length == 1)
            {
                poststr = "0000";
                str = prestr + "." + poststr;
            }
            else
            {
                //if(strs[1].Length==i)
                    poststr = strs[1].PadRight(i,'0');
            }
            int idot = str.IndexOf('.');
            //string str = d.ToString();
            //string[] strs = str.Split('.');
            //int idot = str.IndexOf('.');
            //string prestr = strs[0];
            //string poststr = "";
            //if (strs.Length == 1)
            //    poststr = "0000";
            //else
            //    poststr = strs[1]; ;
            
            if (poststr.Length > i)
            {
                poststr = str.Substring(idot + 1, i);
            }
            string strd = prestr + "." + poststr;
            //d = Double.Parse(strd);
            return strd;
        }

        public static Dictionary<string, string> getLabelConfig()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("Model", getConfig("Model",false,"","LabelParameters.xml"));
            dict.Add("Color", getConfig("Color", false, "", "LabelParameters.xml"));
            dict.Add("ChipType", getConfig("ChipType", false, "", "LabelParameters.xml"));
            dict.Add("BoxID", getConfig("BoxID", false, "", "LabelParameters.xml"));
            dict.Add("Pmpp", getConfig("Pmpp", false, "", "LabelParameters.xml"));
            dict.Add("Isc", getConfig("Isc", false, "", "LabelParameters.xml"));
            dict.Add("Voc", getConfig("Voc", false, "", "LabelParameters.xml"));
            dict.Add("Impp", getConfig("Impp", false, "", "LabelParameters.xml"));
            dict.Add("Vmpp", getConfig("Vmpp", false, "", "LabelParameters.xml"));
            dict.Add("Size", getConfig("Size", false, "", "LabelParameters.xml"));
            dict.Add("ModuleNo", getConfig("ModuleNo", false, "", "LabelParameters.xml"));
            try
            {
                bChipType = bool.Parse(getConfig("Model", true, "ChipType", "LabelParameters.xml"));
            }
            catch
            {
                bChipType = true;
            }
            //dict.Add("Model", getConfig("Model", false, "", "LabelParameters.xml"));
            //dict.Add("Model", getConfig("Model", false, "", "LabelParameters.xml"));
            return dict;
        }
        //Check BoxID is overdue
        public static void CheckBoxIDisOverdue(int datediff=0)
        {
            string sqlstr ="";
            int i = 0 ;
            if (datediff == 0)
                datediff = 7;
            sqlstr = "update BoxCodeList set IsOverdue='Y' where datediff(DD,CreateDate,GetDate())>" + datediff + " and IsUsed<>'Y' and IsOverdue<>'Y'";
            
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(FormCover.connectionBase))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = sqlstr;
                cmd.CommandType = CommandType.Text;
                try
                {
                    if (conn.State == ConnectionState.Closed) 
                        conn.Open();
                    i = cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    conn.Close();
                }
                catch (Exception ex)
                {
                    //Log
                }
            }

        }

        #endregion

        #region"XML"
        //Initial/Save XML parameters
        public static void setConfig(string sData)//(int CartonQty, int IdealPower, int MixCount, int PrintMode,int LabelFormat,string ModuleLabelPath, string CartonLabelPath, string CartonLabelParameter, string CheckRule,string MixChecked,string CheckNOChecked)
        {
            string xmlFile = Application.StartupPath + "\\ParametersSetting.xml";
            string[] sDataSet = sData.Split('|');

            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlNode parentsNode = doc.CreateElement("Parameters");
            doc.AppendChild(parentsNode);

            subNode(doc, parentsNode, "CartonQty", sDataSet[0]);
            subNode(doc, parentsNode, "IdealPower", sDataSet[1]);
            subNode(doc, parentsNode, "MixCount", sDataSet[2], "Checked", sDataSet[7]);
            subNode(doc, parentsNode, "CheckRule", sDataSet[6], "Checked", sDataSet[8]);
            subNode(doc, parentsNode, "ModuleLabelPath", sDataSet[3]);
            subNode(doc, parentsNode, "CartonLabelPath", sDataSet[4]);
            subNode(doc, parentsNode, "CartonLabelParameter", sDataSet[5]);

            subNode(doc, parentsNode, "OnePrint", sDataSet[9]);
            subNode(doc, parentsNode, "BatchPrint", sDataSet[10]);
            subNode(doc, parentsNode, "NoPrint", sDataSet[11]);
            subNode(doc, parentsNode, "LargeLabelPrint", sDataSet[12]);		//modified by Shen Yu July 15, 2011
            subNode(doc, parentsNode, "Model", sDataSet[13]);
            subNode(doc, parentsNode, "RealPower", sDataSet[14]);
            subNode(doc, parentsNode, "NominalPower", sDataSet[15]);
            //subNode(doc, parentsNode, "IsUseCurrentCreateBoxID", sDataSet[15]);

            if (ToolsClass.sSite == ToolsClass.CAMO)
            {
                subNode(doc, parentsNode, "StationNo", sDataSet[16]);
                subNode(doc, parentsNode, "SerialCode", "1");		//modified by Shen Yu on July 15, 2011
                subNode(doc, parentsNode, "ColorCode", "C");		//modified by Shen Yu on July 15, 2011
            }
            else //if (ToolsClass.sSite == ToolsClass.ChangShu)
            {
                subNode(doc, parentsNode, "FirstCode", sDataSet[16]);
                //subNode(doc, parentsNode, "SerialCode", sDataSet[17]);
                subNode(doc, parentsNode, "SerialCode", sDataSet[17], "initCnt", sDataSet[17]);
                subNode(doc, parentsNode, "ColorCode", sDataSet[18]);
                subNode(doc, parentsNode, "LabelCount", sDataSet[22]);
            }
            subNode(doc, parentsNode, "AutoMode", sDataSet[19]);
            subNode(doc, parentsNode, "ManualMode", sDataSet[20]);
            subNode(doc, parentsNode, "PrintCount", sDataSet[21]);
            subNode(doc, parentsNode, "ExcelH", sDataSet[22]);
            subNode(doc, parentsNode, "ExcelV", sDataSet[23]);
            subNode(doc, parentsNode, "SystemDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            subNode(doc, parentsNode, "ModuleClass", "A");          //add by alex.dong 2011-07-22

            //add by alex.dong|2012-03-20
            subNode(doc, parentsNode, "Supplier", sDataSet[24]);
            subNode(doc, parentsNode, "SchSerialCode", sDataSet[25]);

            try
            {
                doc.Save(xmlFile);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// get packing parameters setting
        /// </summary>
        /// <param name="sNodeName"></param>
        /// <param name="isAttribute"></param>
        /// <param name="sAttributeName"></param>
        /// <param name="configFile"></param>
        /// <returns></returns>
        public static string getConfig(string sNodeName, bool isAttribute = false, string sAttributeName = "", string configFile = "")//(int CartonQty, int IdealPower, int MixCount, int PrintMode, string ModuleLabelPath, string CartonLabelPath, string CartonLabelParameter, string CheckRule)
        {
            string svalue = "";
            if (configFile == "" || configFile==null)
                configFile = "ParametersSetting.xml";
            string xmlFile = Application.StartupPath +"\\"+ configFile;
            if (!System.IO.File.Exists(xmlFile))
            {
                MessageBox.Show("Please Inform Manager to Config Package Parameters Before Packing", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return "";
            }
            try
            {
                XmlReader reader = XmlReader.Create(xmlFile);
                if (reader.ReadToFollowing(sNodeName))
                {
                    if(!isAttribute)
                        svalue = reader.ReadElementContentAsString();
                    else
                        svalue = reader.GetAttribute(sAttributeName);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (svalue != null)
                svalue = svalue.Replace('\r', ' ').Replace('\n', ' ').Trim();
            else
                svalue = "";
            return svalue;
        }
        public static XmlNode subNode(XmlDocument doc, XmlNode ParentNode, string sNodeName, string sValue, string sAttributeName = "",string sAttribute = "")
        {
            XmlNode subNode = doc.CreateElement(sNodeName);
            if (sAttribute != "")
            {
                XmlAttribute subAttribute = doc.CreateAttribute(sAttributeName);
                subAttribute.Value = sAttribute;
                subNode.Attributes.Append(subAttribute);
            }
            if (sValue != "")
                subNode.AppendChild(doc.CreateTextNode(sValue));
            ParentNode.AppendChild(subNode);
            return subNode;
        }
        public static void saveXMLNode(string snode, string value = "", bool isAttribute = false, string sAttributeName = "",string fileName="")
        {
            string xmlFile = "";
            string ParentNode = "";
            if (fileName == "" || fileName == null)
            {
                xmlFile = Application.StartupPath + "\\ParametersSetting.xml";
                ParentNode = "Parameters";
            }
            else
            {
                xmlFile = Application.StartupPath + "\\" + fileName;
                ParentNode = "appSQL";
            }

            #region 在xml文件里添加一个节点   
            if (snode.Trim().Equals("CheckModuleClass"))
            {
                XmlDocument newxmlDoc = new XmlDocument();
                newxmlDoc.Load(xmlFile);
                XmlNodeList newnodeList = newxmlDoc.SelectSingleNode(ParentNode).ChildNodes;
                string existsFlag = "N";

                foreach (XmlNode newxn in newnodeList)
                {
                    XmlElement xe = (XmlElement)newxn;
                    if (xe.Name == snode)
                    {
                        existsFlag = "Y";
                        break;
                    }
                }

                if (existsFlag.Equals("N"))
                {
                    XmlNode root = newxmlDoc.SelectSingleNode(ParentNode);
                    XmlElement node = newxmlDoc.CreateElement("CheckModuleClass");
                    root.AppendChild(node);
                    newxmlDoc.Save(xmlFile);
                }
            }
            #endregion

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xmlFile);
            XmlNodeList nodeList = xmlDoc.SelectSingleNode(ParentNode).ChildNodes;
            foreach (XmlNode xn in nodeList)
            {
                XmlElement xe = (XmlElement)xn;
                if (xe.Name == snode)
                {
                    if (isAttribute)
                    {
                        xe.SetAttribute(sAttributeName, value);
                        break;
                    }
                    else
                    {
                        xe.InnerText = value;
                        break;
                    }
                }
            }
            try
            {
                xmlDoc.Save(xmlFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Save Config File Fail: \r\r" + ex.Message, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 将指定文本框中的SQL语句保存到配置文件(默认为config.xml)中
        /// </summary>
        /// <param name="tb">文本框控件</param>
        /// <param name="xmlFile">配置文件(默认为config.xml)</param>
        public static void saveSQL(TextBox tb, string xmlFile = "config.xml")
        {
            //bool saved = false;
            xmlFile = Application.StartupPath + "\\" + xmlFile;
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);
            XmlNode node = doc.DocumentElement.FirstChild.SelectSingleNode("//" + tb.Name);
            if (node == null)
            {
                XmlElement elem = doc.CreateElement(tb.Name);
                XmlText text = doc.CreateTextNode(tb.Text);
                node = doc.DocumentElement.AppendChild(elem);
                node.AppendChild(text);
            }
            try
            {
                doc.Save(xmlFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Save Config File Fail: \r\r" + ex.Message, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //return saved;
        }
        /// <summary>
        /// 从指定的配置文件(默认为config.xml)中读取SQL语句
        /// </summary>
        /// <param name="xmlNode">要查询的节点</param>
        /// <param name="xmlFile">配置文件(默认为config.xml)</param>
        /// <returns>SQL语句文本</returns>
        public static string getSQL(string xmlNode, string xmlFile = "config.xml")
        {
            xmlFile = Application.StartupPath + "\\" + xmlFile;
            if (System.IO.File.Exists(xmlFile))
            {
                try
                {
                    using (XmlReader reader = XmlReader.Create(xmlFile))
                    {
                        if (reader.ReadToFollowing(xmlNode))
                        {

                            xmlNode = reader.ReadElementContentAsString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Read Config File Fail: \r\r" + ex.Message, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Config File is not exist", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return xmlNode;
        }

        public static string getSQL(string xmlNode)
        {
            if (string.IsNullOrEmpty(xmlNode))
                return "";

            string sql = "";

            //按照时间查询报表
            string tbCPTCSPackingQuerySQL = @"SELECT pd2.[BoxID],0,pd2.[SN],[ElecParaTest].[Pmax],[Box].[PackOperator],CONVERT(varchar(19),pd2.[ProcessDateTime],120),pd2.ModuleClass 
                                              FROM (SELECT [BoxID],[SN],[InterID],[InterNewTempTableID],[OperatorID],[ProcessDateTime]  FROM [Product] with(nolock) 
                                                    WHERE [BoxID] in (SELECT distinct [BoxID] 
                                                                      FROM [Product] with(nolock) WHERE len([BoxID]) &gt; 0 AND [ProcessDateTime]&gt;'{0}' 
                                                                      AND [ProcessDateTime]&lt;'{1}')) AS pd2 inner join [ElecParaTest] ON [ElecParaTest].[InterID]=pd2.[InterNewTempTableID] inner join Box on pd2.BoxID=Box.BoxID";

            string tbCPTPackingQuerySQL = @"SELECT pd2.[BoxID],0,pd2.[SN],[ElecParaTest].[Pmax],pd2.[ModelType],
                                            [ElecParaTest].[Voc], [ElecParaTest].[Isc], [ElecParaTest].[Vm], [ElecParaTest].[Im],
                                            CONVERT(varchar(10), [ElecParaTest].[EPTDate], 101),pd2.[CellColor],pd2.[CellType],
                                            pd2.[ModuleClass],CONVERT(varchar(10),Process.[ProcessDate],101),pd2.ModuleClass FROM 
                                            (SELECT [BoxID],[SN],[ModelType],[CellColor],[CellType],[ModuleClass] FROM [Product] with(nolock) 
                                            WHERE [BoxID] in (SELECT distinct [BoxID] FROM [Product] with(nolock) 
                                            WHERE len([BoxID]) > 0 AND [SN] in (SELECT distinct [SN] FROM [Process] with(nolock) WHERE [Process]='T5' 
                                            AND [ProcessDate]&gt;'{0}' AND [ProcessDate]&lt;'{1}'))) AS pd2 inner join [ElecParaTest] ON [ElecParaTest].[SN]=pd2.[SN]
                                            inner join [Process] ON [Process].[SN]=pd2.[SN] order by pd2.[BoxID],pd2.[SN],[EPTID] DESC";

            //按照箱号查询报表
            string tbCPTPackingQuerySQLCarton = @"SELECT pd2.[BoxID],pd2.Cust_BoxID,pd2.[SN],[ElecParaTest].[Pmax],[Box].[PackOperator],
CONVERT(varchar(19),pd2.[ProcessDateTime],120),[Box].JobNo,pd2.[Art_No],pd2.[StdPower],pd2.ModuleClass FROM (SELECT [BoxID],[Cust_BoxID],[SN],[InterID],[InterNewTempTableID],
[OperatorID],[ProcessDateTime],[Art_No],[StdPower],[ModuleClass] FROM [Product] with(nolock) 
WHERE [BoxID]='{0}') AS pd2 inner join [ElecParaTest] ON [ElecParaTest].[InterID]=pd2.[InterNewTempTableID] 
inner join Box on pd2.BoxID=Box.BoxID order by pd2.[BoxID],pd2.[SN] DESC";

            //按照组件查询报表
            string tbCPTPackingQuerySQLModule = @"SELECT pd2.[BoxID],0,pd2.[SN],[ElecParaTest].[Pmax],pd2.[ModelType], [ElecParaTest].[Voc], [ElecParaTest].[Isc], [ElecParaTest].[Vm], [ElecParaTest].[Im], CONVERT(varchar(10), [ElecParaTest].[EPTDate], 101),pd2.[CellColor],pd2.[CellType],pd2.[ModuleClass],CONVERT(varchar(10),Process.[ProcessDate],101),pd2.ModuleClass FROM (SELECT [BoxID],[SN],[ModelType],[CellColor],[CellType],[ModuleClass] FROM [Product] with(nolock) WHERE [BoxID]=(SELECT distinct [BoxID] FROM [Product] with(nolock) WHERE [SN]='{0}')) AS pd2 inner join [ElecParaTest] ON [ElecParaTest].[SN]=pd2.[SN] inner join [Process] ON [Process].[SN]=pd2.[SN] order by pd2.[BoxID],pd2.[SN],[EPTID] DESC";

            //检查组件是否存在
            string tbHandPackingSQL = @"SELECT top 1 pd1.[SN],[ModelType],'',[Pmax],pd1.[Process],pd1.[BoxID],pd1.[WorkOrder],pd1.[ReWorkOrder],pd1.[Flag],pd1.[localflag],pd1.ModuleClass
                                        FROM (SELECT Top 2 [SN],[ModelType],[Process],[BoxID],InterNewTempTableID,[WorkOrder],[ReWorkOrder],[Flag],[localflag],ModuleClass 
                                                FROM [Product] with(nolock) Where [SN] ='{0}'  and [InterID] in (select max(InterID) 
                                                                                    FROM [Product]  with(nolock) 
                                                                                    Where [SN] ='{0}')
                                              )AS pd1 inner join  [ElecParaTest] AS pd2  ON pd1.SN=pd2.[FNumber]";
            //替换组件
            string tbCPTHandingQuerySQL = @"SELECT pd1.[SN],[ModelType],[Pmax],[CellSpec],[CellType],[CellProvider],[CellColor],pd1.[Process],[BoxID],pd1.ModuleClass
                                              FROM (SELECT * FROM [Product]  with(nolock) 
                                              where [SN]='{0}' and [InterID] in (select max(InterID) FROM [Product]  with(nolock) Where [SN] ='{0}')) as pd1 inner join (SELECT Top 1 [FNumber],[Pmax],InterID FROM [ElecParaTest] with(nolock) Where [FNumber] ='{0}') Order By [InterID] DESC) as pd2 on pd1.[InterNewTempTableID]=pd2.[InterID]";
            //拆箱按照箱号查询组件
            string tbBoxHandingQuerySQL = @"SELECT pd1.[SN],[ModelType],[Pmax],[CellSpec],[CellType],[CellProvider],[CellColor],pd1.[Process],[BoxID],pd1.InterID,pd1.ModuleClass
                                              FROM (SELECT [SN],ModuleClass,[ModelType],[CellSpec],[CellType],[CellProvider],[CellColor],[Process],[BoxID],InterNewTempTableID,InterID 
                                                   FROM [Product] with(nolock) where [BoxID]='{0}') as pd1 inner join [ElecParaTest]  on pd1.[InterNewTempTableID]=[ElecParaTest].[InterID]  
                                            Order By pd1.[SN], pd1.[InterID] DESC ";

//            string tbBoxHandingQuerySQL = @"SELECT [SN],prd.[ModelType],[CellSpec],[CellType],[CellProvider],[CellColor],[Process],prd.[BoxID],InterNewTempTableID,InterID
//                                                                            FROM Product prd WITH(NOLOCK) ,
//                                                                                 box  box with(nolock) 
//                                                                            where prd.BoxID = box.BoxID 
//                                                                                  and box.Flag ='1'
//                                                                                  and box.BoxID='{0}'";

            string tbNoPackageQuerySQL = @"SELECT pd1.[SN],[ModelType],[Pmax],[CellSpec],[CellType],[CellProvider],[CellColor],pd1.[Process],[BoxID],pd1.ModuleClass 
                                           FROM (SELECT [SN],ModuleClass,[ModelType],[CellSpec],[CellType],[CellProvider],[CellColor],[Process],[BoxID] FROM [Product] with(nolock)  where len([BoxID]) &lt; 1 and [SN] in (SELECT distinct [SN] FROM [Process] with(nolock) WHERE [Process]='T4' AND  [ProcessDate]&gt;'{0}' AND [ProcessDate]&lt;'{1}')) as pd1 inner join [ElecParaTest]  on pd1.[SN]=[ElecParaTest].[SN] Order By pd1.[SN], [EPTID] DESC";

            string tbBoxReworkSQL = @"SELECT  [SN],prd.[ModelType],[Pmax],[CellSpec],[CellType],[CellProvider],[CellColor],[Process],prd.[BoxID],prd.ModuleClass
                                        FROM [Product] prd with(nolock),
                                             [dbo].Box box with(nolock) 
                                        WHERE prd.BoxID='{0}' 
                                              AND prd.Flag in ('5','1')
                                              and prd.BoxID = box.BoxID
                                              and box.Flag in ('5','1')";

            if (xmlNode == "tbCPTCSPackingQuerySQL")
                sql = tbCPTCSPackingQuerySQL;
            else if (xmlNode == "tbCPTPackingQuerySQL")
                sql = tbCPTPackingQuerySQL;
            else if (xmlNode == "tbCPTPackingQuerySQLCarton")
                sql = tbCPTPackingQuerySQLCarton;
            else if (xmlNode == "tbCPTPackingQuerySQLModule")
                sql = tbCPTPackingQuerySQLModule;
            else if (xmlNode == "tbHandPackingSQL")
                sql = tbHandPackingSQL;
            else if (xmlNode == "tbCPTHandingQuerySQL")
                sql = tbCPTHandingQuerySQL;
            else if (xmlNode == "tbBoxHandingQuerySQL")
                sql = tbBoxHandingQuerySQL;
            else if (xmlNode == "tbNoPackageQuerySQL")
                sql = tbNoPackageQuerySQL;
            else if (xmlNode == "tbBoxReworkSQL")
                sql = tbBoxReworkSQL;

           return sql;
        }

        #endregion

        #region "BoxID"
        public static string getBoxID(string sStationNo,string sCellColor,string sYear="",string sDayOfYear="")
        {
            string sboxid = "";
            string strsql="select * from Box where BoxID='{0}' and ((IsUsed<>'N' and  IsUsed<>'W') OR number > 0)";
            //string strsql="select * from Box where BoxID='{0}'";
            
            bool isUse = true;
         
            int iSerial = int.Parse(ToolsClass.getConfig("SerialCode"));
            try
            {
                DateTime dt = Convert.ToDateTime(getConfig("SystemDate"));
                if (DateDiff(DateInterval.Month, dt, DateTime.Now.Date) > 0)
                {
                    saveXMLNode("SystemDate", DateTime.Now.Date.ToString("yyyy-MM-dd HH:mm:ss"));
                    iSerial = 1;
                }
            }
            catch(Exception e)
            {
                MessageBox.Show("Get System Date Fail:"+e.Message, "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            lockGenerateBoxID = new object();
            if(sYear.Equals(""))
                sYear = DateTime.Now.Year.ToString().Substring(2,2);
            if (sDayOfYear.Equals(""))
                sDayOfYear = DateTime.Now.DayOfYear.ToString().PadLeft(iDate_Length, '0');
            string sMonth = DateTime.Now.Month.ToString().PadLeft(2,'0');

            while (isUse)
            {
                if (iSerial > 9999)
                {
                    MessageBox.Show("Carton Serial Number Has Run Out!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return "";
                }
                sboxid = sYear + sStationNo + sCellColor + sMonth + "0" + iSerial.ToString().PadLeft(iSerial_Length, '0');
                if (ExecuteQuery(string.Format(strsql, sboxid)) > 0)
                    isUse = true;
                else
                    isUse = false;
                iSerial++;
            }

            //saveXMLNode("SerialCode", iSerial.ToString());
            lock (lockGenerateBoxID)//Lock
            {
                saveBoxCodeList(sboxid);
                strsql = "update Box set IsUsed='W' where BoxID='" + sboxid + "'";
                ExecuteNonQuery(strsql);
            }
            return sboxid;
        }

        public static void saveBoxCodeList(string sBoxID)//(CheckedListBox clb)
        {
            int line = 0;
            string sql = "";
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(FormCover.connectionBase))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                sql="if not exists (select BoxID from Box where boxid='" + sBoxID + "') insert into Box(BoxID,Number,BoxType,ModelType) values('" + sBoxID + "',0,'','') ";
                //sql = sql + "else update Box set IsUsed='W' where boxid='" + sBoxID + "'";//add by alex.dong|2012-03-15
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                try
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    line = cmd.ExecuteNonQuery();
                    conn.Close();
                }
                catch (Exception ex)
                {
                    //Log--BoxID Repeat,do nothing
                    conn.Close();
                }
            }
        }
        #endregion

        #region"Sql Execute"
        /// <summary>
        /// According to the query statement, get the data to fill the data table
        /// </summary>
        /// <param name="conn">数据库连接</param>
        /// <param name="SqlStr">SQL语句</param>
        /// <returns>数据表（DataTable）</returns>
        public static DataTable GetDataTable(string SqlStr, DataTable ds = null)
        {
            if (ds == null)
                ds = new DataTable();
            else
                ds.Clear();
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(FormCover.connectionBase))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = SqlStr;
                cmd.CommandType = CommandType.Text;
                //cmd.Connection = conn;
                using (System.Data.SqlClient.SqlDataAdapter ada = new System.Data.SqlClient.SqlDataAdapter())
                {
                    ada.SelectCommand = cmd;
                    try
                    {
                        ada.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

            }

            return ds;
        }
        
        public static DataTable GetDataTable1(string SqlStr, string connection ="")
        {
        	string con = "";
        	DataTable ds = new DataTable();
        	if (string.IsNullOrEmpty( connection ) )
        		con = FormCover.connectionBase;
            else
            	con = connection;
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(con))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = SqlStr;
                cmd.CommandType = CommandType.Text;
                //cmd.Connection = conn;
                using (System.Data.SqlClient.SqlDataAdapter ada = new System.Data.SqlClient.SqlDataAdapter())
                {
                    ada.SelectCommand = cmd;
                    try
                    {
                        ada.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

            }

            return ds;
        }
        
        
        
        /// <summary>
        /// 取得数据读取器
        /// </summary>
        /// <param name="SqlStr">SQL语句</param>
        /// <returns>DataReader</returns>
        public static SqlDataReader GetDataReader(string SqlStr,string SqlConn="")
        {
            SqlDataReader dr = null;
            if (string.IsNullOrEmpty(SqlConn))
                SqlConn = FormCover.connectionBase;
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(SqlConn);
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {                  
                    try
                    {
                        cmd.CommandText = SqlStr;
                        cmd.CommandType = CommandType.Text;

                        if (conn.State == ConnectionState.Closed)
                            conn.Open();
                        dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        dr = null;
                    }
             }
            return dr;
        }
       
        public static bool replacementComponents(string box,string cpt1,string cpt2="" )
        {
            bool rep = false;
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(FormCover.connectionBase))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                SqlTransaction transaction = null;
                try
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    if (cpt2 != "")
                    {
                        cmd.CommandText = "SELECT [BoxID] FROM [Product] WHERE [SN]='" + cpt2 + "' and ProcessDateTime in (Select max(ProcessDateTime) from Product where SN='"+cpt2+"')";
                        object obj = cmd.ExecuteScalar();
                        if (obj.ToString().Trim().Length > 0)
                        {
                            if (MessageBox.Show(cpt2 + " packed in carton“" + obj.ToString() + "”,continue to replace?", "Prompting Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                            {
                                return false;
                            }
                        }
                    }
                    transaction = conn.BeginTransaction();
                    cmd.Transaction = transaction;
                    cmd.CommandText = string.Format("UPDATE [Product] SET [Process]='T4',[BoxID] = '' WHERE [SN]='{0}' AND [Process]='T5';", cpt1, cpt2);
                    int li = cmd.ExecuteNonQuery();
                    if (cpt2 != "")
                    {
                        cmd.CommandText = string.Format("INSERT INTO [Process] ([SN],[OperatorID],[ProcessDate],[Process],[Tester],[Pass],[UPReason]) VALUES ('{0}','{1}',getdate(),'T5','',1,'');UPDATE [Product] SET [Process]='T5',[BoxID] = '{2}' WHERE [SN]='{0}' and ProcessDateTime in (select max(ProcessDateTime) from Product where SN='{0}');", cpt2, FormCover.CurrUserWorkID, box);
                        li = cmd.ExecuteNonQuery();
                    }
                    transaction.Commit();
                    rep = true;
                    conn.Close();
                }
                catch (Exception ex)
                {
                    if (transaction != null) transaction.Rollback();
                    System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    conn.Close();
                }

            }
            return rep;
        }
        /// <summary>
        /// 执行SQL语句,返回受影响的行数
        /// </summary>
        /// <param name="sqlStr">SQL语句</param>
        /// <returns>受影响的行数</returns>
        public static int ExecuteNonQuery(string sqlStr,string SqlConn="")
        {
            int line = 0;
            if (string.IsNullOrEmpty(SqlConn))
                SqlConn = FormCover.connectionBase;
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(SqlConn))
            {
                using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = sqlStr;
                    cmd.CommandType = CommandType.Text;
                    try
                    {
                        if (conn.State == ConnectionState.Closed) conn.Open();
                        line = cmd.ExecuteNonQuery();

                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    return line;
                }
            }
        }

        public static int ExecuteScalarGetInterID(string sqlStr, string SqlConn = "")
        {
            int line = 0;
            if (string.IsNullOrEmpty(SqlConn))
                SqlConn = FormCover.connectionBase;
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(SqlConn))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = sqlStr;
                cmd.CommandType = CommandType.Text;
                try
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    line =int.Parse(cmd.ExecuteScalar().ToString());

                    conn.Close();
                }
                catch (Exception ex)
                {
                    conn.Close();
                    System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                return line;
            }
        }

        public static object ExecuteScalar(string sqlStr, string SqlConn = "")
        {
            object objScalar = new object();
            if (string.IsNullOrEmpty(SqlConn))
                SqlConn = FormCover.connectionBase;
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(SqlConn))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = sqlStr;
                cmd.CommandType = CommandType.Text;
                try
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    objScalar = cmd.ExecuteScalar();

                    conn.Close();
                }
                catch (Exception ex)
                {
                    conn.Close();
                    objScalar = null;
                }

                return objScalar;
            }
        }

        public static int ExecuteQuery(string sqlStr)
        {
            int line = 0;
            SqlDataReader dr = null;
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(FormCover.connectionBase))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = sqlStr;
                cmd.CommandType = CommandType.Text;
                try
                {
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    dr = cmd.ExecuteReader();
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            line++;
                        }
                    }
                    conn.Close();
                }
                catch (Exception ex)
                {
                    conn.Close();
                    System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                return line;
            }


        }
		//Email sending       
        public static void SendEmail(string body)
        {
           	
        	try
            {
        		string host = getConfig("smtphost",false,"","Config.xml");
        		string port =  getConfig("smtpport",false,"","Config.xml");
        		string MailFrom = getConfig("EmailFrom",false,"","Config.xml");
        		string MailTo = getConfig("EmailTo",false,"","Config.xml");
        		string subject = getConfig("Subject",false,"","Config.xml");
        		string userNM = getConfig("userNM",false,"","Config.xml");
        		string PsW = getConfig("PassWord",false,"","Config.xml");
        		string[] Receipts = MailTo.Split(';');
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(host);

                mail.From = new MailAddress(MailFrom);
                foreach (string r in Receipts)
                	mail.To.Add(r);
                mail.Subject = subject;
                mail.Body = body;
                SmtpServer.Port = Convert.ToInt16(port);   
               	SmtpServer.Credentials = new System.Net.NetworkCredential(userNM, PsW);          
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public static DataSet  GetDataTable(string sql,SqlDataAdapter adapter)
        {
            DataSet ds = new DataSet();

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(FormCover.connectionBase))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                //cmd.Connection = conn;
                using (System.Data.SqlClient.SqlDataAdapter ada = new System.Data.SqlClient.SqlDataAdapter())
                {
                    ada.SelectCommand = cmd;
                    try
                    {
                        ada.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

            }

            return ds;
        }
        #endregion

        #region"Nominal Power"
        /// <summary>
        /// 根据实际功率计算标称功率
        /// </summary>
        /// <param name="actualPoser">实际功率</param>
        /// <returns>标称功率（整数型） </returns>
        public static int getNominalPower(decimal actualPoser)
        {
            int nominalPower = (int)actualPoser / 10 * 10;
            if (actualPoser - nominalPower == 0)
                nominalPower -= 5;
            else if (actualPoser - nominalPower > 5)
                nominalPower += 5;
            return nominalPower;
        }
        public static int getNominalPower(double actualPoser)
        {
            int nominalPower = (int)actualPoser / 10 * 10;
            if (actualPoser - nominalPower == 0)
                nominalPower -= 5;
            else if (actualPoser - nominalPower > 5)
                nominalPower += 5;
            return nominalPower;
        }
        /// <summary>
        /// 获取标称功率档次是否显示
        /// </summary>
        /// <param name="actualPoser"></param>
        /// <param name="sModuleNo"></param>
        /// <param name="SchemeInterID"></param>
        /// <param name="sqlConn"></param>
        /// <returns></returns>
        public static string getIsShowPowerGrade(double actualPoser, string sModuleNo, int SchemeInterID = 0, string sqlConn = "")
        {
            string sqlGetPmaxID = "";
            if (SchemeInterID == 0) 
                sqlGetPmaxID = "select a.Pmax,a.MinValue,a.CtlValue,a.MaxValue,a.RESV1,a.RESV2 FROM SchemeEntry as a where a.PmaxID=(select ItemPmaxID from Scheme where InterID=(select distinct SchemeInterID from ElecParaTest where FNumber='" + sModuleNo + "' and InterID in (select max(InterID) from ElecParaTest where FNumber='" + sModuleNo + "')))";
            else
                sqlGetPmaxID = "select a.Pmax,a.MinValue,a.CtlValue,a.MaxValue,a.RESV1,a.RESV2 FROM SchemeEntry as a where a.PmaxID=(select ItemPmaxID from Scheme where InterID=" + SchemeInterID + ")";
            string strIsShowPowerGrade = "";
            SqlDataReader dr = null;
            try
            {
                dr = GetDataReader(sqlGetPmaxID, sqlConn);
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        if (!dr.IsDBNull(5))
                            strIsShowPowerGrade = dr.GetString(5);
                        else
                            strIsShowPowerGrade = "N";
                    }
                    dr.Close();
                    dr = null;
                }
                else
                    strIsShowPowerGrade = "ERROR";
            }
            catch (Exception ex)
            {
                dr.Close();
                dr = null;
                strIsShowPowerGrade = "ERROR";
                MessageBox.Show("获取标称功率档次是否显示发生异常 \nException:" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return strIsShowPowerGrade;
        }
        /// <summary>
        /// 获取标称功率
        /// </summary>
        /// <param name="actualPoser"></param>
        /// <param name="sModuleNo"></param>
        /// <param name="SchemeInterID"></param>
        /// <param name="sqlConn"></param>
        /// <returns></returns>
        public static string getIdealPower(double actualPoser, string sModuleNo, int SchemeInterID = 0, string sqlConn = "") //Modify by Gengao 2012-09-05
        {
            string idealPower = "-1"; //Modify by Gengao 2012-09-05
            string sqlGetPmaxID = "";
            if (SchemeInterID == 0) //Modify by Gengao 2012-09-05
                sqlGetPmaxID = "select a.Pmax,a.MinValue,a.CtlValue,a.MaxValue FROM SchemeEntry as a where a.PmaxID=(select ItemPmaxID from Scheme where InterID=(select distinct SchemeInterID from ElecParaTest where FNumber='" + sModuleNo + "' and InterID in (select max(InterID) from ElecParaTest where FNumber='" + sModuleNo + "')))";
            else
                sqlGetPmaxID = "select a.Pmax,a.MinValue,a.CtlValue,a.MaxValue FROM SchemeEntry as a where a.PmaxID=(select ItemPmaxID from Scheme where InterID=" + SchemeInterID + ")";
            	// taken off for canada edited by jacky 2016-01-19
            	//sqlGetPmaxID = "select a.Pmax,a.MinValue,a.CtlValue,a.MaxValue,a.RESV1
            string strPmax = "", strMinCtlValue = "", strMaxCtlValue = "", strCtlValue = "", strMinValue = "", strMaxValue = "";

            string strGrade = ""; // added by Gengao 2012-09-05

            SqlDataReader dr = null;
            try
            {
                dr = GetDataReader(sqlGetPmaxID, sqlConn);
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        strPmax = dr.GetString(0).Replace("W", "");
                        //strGrade = dr.GetString(4); edited by jacky 2016-01-19 
                        if (!dr.GetString(1).Trim().Equals(""))
                        {
                            strMinCtlValue = dr.GetString(1).Substring(dr.GetString(1).IndexOf(']') + 1, 2).Trim();
                            strMinValue = dr.GetString(1).Substring(dr.GetString(1).IndexOf(strMinCtlValue) + 2).Trim();
                        }
                        if (!dr.GetString(3).Trim().Equals(""))
                        {
                            strMaxCtlValue = dr.GetString(3).Substring(dr.GetString(3).IndexOf(']') + 1, 2).Trim();
                            strMaxValue = dr.GetString(3).Substring(dr.GetString(3).IndexOf(strMaxCtlValue) + 2).Trim();
                        }
                        strCtlValue = dr.GetString(2).Trim();
                        if (CheckIdealPower(actualPoser.ToString(), strMinCtlValue, strMaxCtlValue, strCtlValue, strMinValue, strMaxValue))
                        {
                            //Modify by Gengao 2012-09-05
                            if (strGrade.ToString().Trim().ToUpper() == "")
                                idealPower = strPmax;
                            else
                            	idealPower = strPmax + "_A";
                                //idealPower = strPmax + "_"+ strGrade; edited by jacky 2016-01-19
                            break;
                        }
                        strPmax = ""; strMinCtlValue = ""; strMaxCtlValue = ""; strCtlValue = ""; strMinValue = ""; strMaxValue = "";
                        strGrade = "";//added by Gengao 2012-09-05
                    }
                    dr.Close();
                    dr = null;
                }

                string sqlGetDowngrade = "select top 1 stdpower from ModuleDownGrade where sn = '{0}' order by interid desc";
                sqlGetDowngrade = string.Format(sqlGetDowngrade, sModuleNo);
                DataTable dt = GetDataTable(sqlGetDowngrade);
                if(dt != null && dt.Rows.Count > 0)
                {
                    idealPower = dt.Rows[0][0].ToString();
                }

            }
            catch (Exception ex)
            {
                dr.Close();
                dr = null;
                MessageBox.Show("Get Nominal Power Fail! \nException:"+ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return idealPower;
        }
        //Check Actual Power is in Ideal Power Scope
        public static bool CheckIdealPower(string strPmax, string strMinCtlValue, string strMaxCtlValue, string strCtlValue, string strMinValue, string strMaxValue)
        {
            bool idearPower = false;
            bool MinCheck = false, MaxCheck=false;

            if (!strMinValue.Equals(""))
            {
                if (strMinCtlValue.Equals("="))
                {
                    if (Convert.ToDouble(strPmax) == Convert.ToDouble(strMinValue))
                        MinCheck = true;
                    else
                        MinCheck = false;
                }
                else if (strMinCtlValue.Equals(">"))
                {
                    if (Convert.ToDouble(strPmax) > Convert.ToDouble(strMinValue))
                        MinCheck = true;
                    else
                        MinCheck = false;
                }
                else
                {
                    if (Convert.ToDouble(strPmax) >= Convert.ToDouble(strMinValue))
                        MinCheck = true;
                    else
                        MinCheck = false;
                }
            }
            else
                MinCheck = false;

            if (!strMaxValue.Equals(""))
            {
                if (strMaxCtlValue.Equals("="))
                {
                    if (Convert.ToDouble(strPmax) == Convert.ToDouble(strMaxValue))
                        MaxCheck = true;
                    else
                        MaxCheck = false;
                }
                else if (strMaxCtlValue.Equals("<"))
                {
                    if (Convert.ToDouble(strPmax) < Convert.ToDouble(strMaxValue))
                        MaxCheck = true;
                    else
                        MaxCheck = false;
                }
                else
                {
                    if (Convert.ToDouble(strPmax) <= Convert.ToDouble(strMaxValue))
                        MaxCheck = true;
                    else
                        MaxCheck = false;
                }
            }
            else
                MaxCheck = false;

            if (strCtlValue.Equals("And"))
            {
                if (MaxCheck && MinCheck)
                    idearPower = true;
            }
            else
            {
                if (MaxCheck || MinCheck)
                    idearPower=true;
            }
            return idearPower;
        }
        #endregion

        #region"Import Data"
        /// <summary>
        /// 将DataGridView内容保存为Notepad文件
        /// </summary>
        /// <param name="m_DataView">待保存的DataGridView</param>
        /// 
       //public static string FindUserGroup(string )
        public static void DataToNotepad(DataGridView m_DataView)
        {
            //定义表格内数据的行数和列数    
            int rowscount = m_DataView.Rows.Count;
            int colscount = m_DataView.Columns.Count;
            //////行数列数必须大于0    
            ////if (rowscount <= 0 || colscount <= 0)
            ////{
            ////    MessageBox.Show("No data can be saved", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            ////    return;
            ////}

            //////行数不可以大于65536    
            ////if (rowscount > 65536)
            ////{
            ////    if (MessageBox.Show("Records too much(can not exceed 65536 rows)，Excel maybe can not open! Continue?", "Prompting Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
            ////        return;
            ////}

            //////列数不可以大于255    
            ////if (colscount > 255)
            ////{
            ////    MessageBox.Show("Records too much(can not exceed 255 columns),can not save file！", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            ////    return;
            ////}
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Save Excel File";
            sfd.Filter = "Txt File(*.txt)|*.txt";
            //sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.DefaultExt = "txt";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string FileName = sfd.FileName;
                string strLine = "Serial No" + Convert.ToChar(9);
                StreamWriter objFileStream = new StreamWriter(FileName, false, System.Text.Encoding.Unicode);
                for (int i = 0; i < m_DataView.Columns.Count; i++)
                {
                    if (m_DataView.Columns[i].Visible == true && m_DataView.Columns[i].HeaderText != "")
                    {
                        strLine = strLine + m_DataView.Columns[i].HeaderText.ToString() + "\t";
                    }
                }
                objFileStream.WriteLine(strLine);

                for (int i = 0; i < m_DataView.Rows.Count; i++)
                {
                    strLine = (i + 1) + "\t";
                    for (int j = 0; j < m_DataView.Columns.Count; j++)
                    {
                        if (m_DataView.Columns[j].Visible == true && m_DataView.Columns[j].HeaderText != "")
                        {
                            if (m_DataView.Rows[i].Cells[j].Value == null || m_DataView.Rows[i].Cells[j].Value == DBNull.Value)
                                strLine = strLine + " " + "\t";
                            else
                            {
                                strLine = strLine + m_DataView.Rows[i].Cells[j].Value.ToString() + "\t";
                            }
                        }
                    }
                    objFileStream.WriteLine(strLine);
                }
                objFileStream.Close();

                if (MessageBox.Show("Save Text File success,Open File?", "Prompting Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    try
                    {
                        System.Diagnostics.Process sqlprocess = new System.Diagnostics.Process();
                        sqlprocess.StartInfo.FileName = FileName;
                        sqlprocess.StartInfo.Verb = "Open";
                        sqlprocess.Start();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }

        public static void DataToExcel(DataGridView m_DataView=null,bool flag=true,ListView lstView=null)
        {
            int rowscount = 0;
            int colscount = 0;
            if (m_DataView != null)
            {
                rowscount = m_DataView.Rows.Count;
                colscount = m_DataView.Columns.Count;
            }
            if (lstView != null)
            {
                rowscount = lstView.Items.Count;
                colscount = lstView.Columns.Count;
            }
            //行数列数必须大于0    
            if (rowscount <= 0 || colscount <= 0)
            {
                MessageBox.Show("No data can be saved", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //行数不可以大于65536    
            if (rowscount > 65536)
            {
                if (MessageBox.Show("Records too much(can not exceed 65536 rows)，Excel maybe can not open! Continue?", "Prompting Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
                    return;
            }

            //列数不可以大于255    
            if (colscount > 255)
            {
                MessageBox.Show("Records too much(can not exceed 255 columns),can not save file！", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Save Excel File";
            sfd.Filter = "Excel File(*.xls)|*.xls";
            //sfd.FilterIndex = 1;
            sfd.OverwritePrompt = true;
            sfd.DefaultExt = "xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string FileName = sfd.FileName;
                XlsDocument xls = new XlsDocument();//创建空xls文档
                xls.FileName = FileName;
                Worksheet sheet = xls.Workbook.Worksheets.AddNamed("Report"); //创建一个工作页为Report

                //设定列宽度--Carton Number、Class、Module Number
                ColumnInfo colInfo0 = new ColumnInfo(xls, sheet);
                colInfo0.ColumnIndexStart = 0;
                colInfo0.ColumnIndexEnd = 0;
                colInfo0.Width = 20 * 256;
                sheet.AddColumnInfo(colInfo0);

                ColumnInfo colInfo1 = new ColumnInfo(xls, sheet);
                colInfo1.ColumnIndexStart = 1;
                colInfo1.ColumnIndexEnd = 1;
                colInfo1.Width = 15 * 256;
                sheet.AddColumnInfo(colInfo1);

                ColumnInfo colInfo2 = new ColumnInfo(xls, sheet);
                colInfo2.ColumnIndexStart = 2;
                colInfo2.ColumnIndexEnd = 2;
                colInfo2.Width = 18 * 256;
                sheet.AddColumnInfo(colInfo2);

                ColumnInfo colInfo3 = new ColumnInfo(xls, sheet);
                colInfo3.ColumnIndexStart = 3;
                colInfo3.ColumnIndexEnd = 3;
                colInfo3.Width = 10 * 256;
                sheet.AddColumnInfo(colInfo3);

                ColumnInfo colInfo8 = new ColumnInfo(xls, sheet);
                colInfo8.ColumnIndexStart = 8;
                colInfo8.ColumnIndexEnd = 8;
                colInfo8.Width = 12 * 256;
                sheet.AddColumnInfo(colInfo8);
                //创建列
                Cells cells = sheet.Cells; //获得指定工作页列集合 

                int intRows = 0;
                int intColumns = 0;
                if (m_DataView != null)
                {
                    intRows = m_DataView.Rows.Count;
                    intColumns = m_DataView.Columns.Count;

                    for (int i = 0; i < intColumns; i++)
                    {
                        if (m_DataView.Columns[i].HeaderText.Replace(" ", "").Length > 0)
                        {
                            //创建列表头
                            Cell title = cells.Add(1, i + 1, m_DataView.Columns[i].HeaderText);
                            title.HorizontalAlignment = HorizontalAlignments.Centered;
                            title.VerticalAlignment = VerticalAlignments.Centered;
                        }
                    }
                    //bool JobNoIsEmpty = false;
                    for (int i = 0; i < intRows; i++)
                    {
                        for (int j = 0; j < intColumns; j++)
                        {
                            if (m_DataView.Rows[i].Cells[j].Value == null || m_DataView.Rows[i].Cells[j].Value == DBNull.Value)
                            {
                            }
                            else
                            {
                                //if (m_DataView.Rows[i].Cells[0].Value != null && m_DataView.Rows[i].Cells[1].Value != null)
                                //    JobNoIsEmpty = true;
                                ////if (!string.IsNullOrEmpty(m_DataView.Rows[i].Cells[0].Value) && string.IsNullOrEmpty(m_DataView.Rows[i].Cells[1].Value))
                                //if (m_DataView.Rows[i].Cells[0].Value != null && m_DataView.Rows[i].Cells[1].Value == null)
                                //    JobNoIsEmpty = false;

                                //if (JobNoIsEmpty)
                                //{
                                    Cell value = cells.Add(i + 2, j + 1, m_DataView.Rows[i].Cells[j].Value.ToString());
                                    if (m_DataView.Rows[i].Cells[j].Value.ToString().Length > 7)
                                        value.Format = StandardFormats.Text;
                                    else
                                        cells.Add(i + 2, j + 1, m_DataView.Rows[i].Cells[j].Value);//value.Format = StandardFormats.Decimal_2;
                                    value.HorizontalAlignment = HorizontalAlignments.Left;
                                    value.VerticalAlignment = VerticalAlignments.Centered;
                                //}
                            }
                        }
                    }
                }

                if (lstView != null)
                {
                    intColumns = lstView.Columns.Count;
                    for (int i = 0; i < intColumns; i++)
                    {
                        if (lstView.Columns[i].Text.Replace(" ", "").Length > 0)
                        {
                            //创建列表头
                            Cell title = cells.Add(1, i + 1, lstView.Columns[i].Text);
                            title.HorizontalAlignment = HorizontalAlignments.Centered;
                            title.VerticalAlignment = VerticalAlignments.Centered;
                        }
                    }
                    int m=0;
                    double dtmp=0.00;
                    foreach (ListViewItem lvi in lstView.Items)
                    {
                        m++;
                        for (int j = 0; j < intColumns; j++)
                        {
                            if (lvi.SubItems[j].Text == null || lvi.SubItems[j].Text == "")
                            {
                            }
                            else
                            {
                                if (j > 0 && j < intColumns - 6)
                                {
                                    try
                                    {
                                        dtmp = double.Parse(lvi.SubItems[j].Text);
                                    }
                                    catch
                                    {
                                        dtmp = 0.00;
                                    }
                                    Cell value = cells.Add(m + 1, j + 1, dtmp);

                                    value.Format = StandardFormats.General;
                                    value.HorizontalAlignment = HorizontalAlignments.Left;
                                    value.VerticalAlignment = VerticalAlignments.Centered;
                                }
                                else
                                {
                                    Cell value = cells.Add(m + 1, j + 1, lvi.SubItems[j].Text);
                                    if (lvi.SubItems[j].Text.Length > 7)
                                        value.Format = StandardFormats.Text;
                                    else
                                        cells.Add(m + 1, j + 1, lvi.SubItems[j].Text);
                                    value.HorizontalAlignment = HorizontalAlignments.Left;
                                    value.VerticalAlignment = VerticalAlignments.Centered;
                                }

                            }
                        }
                    }
                }

                try
                {
                    xls.Save(true);
                }
                catch(Exception e)
                {
                    MessageBox.Show(e.Message, "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

        }
        #endregion
    }
    #region"utility class"
    class Security
    {
        public Security()
        {

        }

        //默认密钥向量
        private static byte[] Keys = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
        /// <summary>
        /// DES加密字符串
        /// </summary>
        /// <param name="encryptString">待加密的字符串</param>
        /// <param name="encryptKey">加密密钥,要求为8位</param>
        /// <returns>加密成功返回加密后的字符串，失败返回源串</returns>
        public static string EncryptDES(string encryptString, string encryptKey)
        {
            try
            {
                byte[] rgbKey = System.Text.Encoding.UTF8.GetBytes(encryptKey.Substring(0, 8));
                byte[] rgbIV = Keys;
                byte[] inputByteArray = System.Text.Encoding.UTF8.GetBytes(encryptString);
                DESCryptoServiceProvider dCSP = new DESCryptoServiceProvider();
                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, dCSP.CreateEncryptor(rgbKey,rgbIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                return Convert.ToBase64String(mStream.ToArray());
            }
            catch
            {
                return encryptString;
            }
        }

        /// <summary>
        /// DES解密字符串
        /// </summary>
        /// <param name="decryptString">待解密的字符串</param>
        /// <param name="decryptKey">解密密钥,要求为8位,和加密密钥相同</param>
        /// <returns>解密成功返回解密后的字符串，失败返源串</returns>
        public static string DecryptDES(string decryptString, string decryptKey)
        {
            try
            {
                byte[] rgbKey = System.Text.Encoding.UTF8.GetBytes(decryptKey);
                byte[] rgbIV = Keys;
                byte[] inputByteArray = Convert.FromBase64String(decryptString);
                DESCryptoServiceProvider DCSP = new DESCryptoServiceProvider();
                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, DCSP.CreateDecryptor(rgbKey,rgbIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                return System.Text.Encoding.UTF8.GetString(mStream.ToArray());
            }
            catch
            {
                return decryptString;
            }
        }

    }

    //class for chinese and english exchange
    class languagechange
    {
        private static string frmname;
        private static Form frm;
        private static ArrayList list;
        private static string filepath = "D:\\Language.ini";//can be config
        private static int dtemp = 0;
        //private static Boolean flag = false;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileInt(string section, string key, int def, string filePath);

        public languagechange(string fname, Form form)
        {
            frmname = fname;
            frm = form;
            readConfigFile();
        }

        public void readConfigFile()
        {
            list = new ArrayList();
            int i = 0, len;
            string str = "";

            len = GetPrivateProfileInt(frmname, "TagCount", -1, filepath);
            for (i = 0; i <= len - 1; i++)
            {
                StringBuilder temp = new StringBuilder(500);
                int j = GetPrivateProfileString(frmname, i.ToString(), "", temp, 500, filepath);
                str = temp.ToString();
                list.Add(str);
            }
        }

        public static void writeConfigFile(Boolean flag)
        {
            WritePrivateProfileString("Setting", "Language", flag.ToString().ToLower(), filepath);
        }

        public static Boolean readSettingConfigFile()
        {
            Boolean flag = false;
            StringBuilder temp = new StringBuilder(500);
            GetPrivateProfileString("Setting", "Language", "", temp, 500, filepath);
            if (temp.ToString().ToUpper() == "ENG")
                flag = true;
            else
                flag = false;
            return flag;
        }

        public static void changefrmName(Boolean languageflag)
        {
            for (dtemp = 0; dtemp <= list.Count - 1; dtemp++)
            {
                if (list[dtemp].ToString().Contains("FRM"))
                {
                    string[] sSet = list[dtemp].ToString().Split(',');
                    if (languageflag)
                        frm.Text = sSet[2];
                    else
                        frm.Text = sSet[1];
                    return;
                }
            }
        }

        public static void changeBTN(Boolean languageflag)
        {
            foreach (Control col in frm.Controls)
            {
                 if (col.GetType() != typeof(Button)) continue;
                 for (dtemp = 0; dtemp <= list.Count - 1; dtemp++)
                 {
                     if (list[dtemp].ToString().Contains(((Button)col).Tag.ToString()))
                    {
                         string[] sSet=list[dtemp].ToString().Split(',');
                         if (languageflag)
                             col.Text = sSet[2];
                         else
                             col.Text = sSet[1];
                    }
                }
            }
        }

        public static void changeLBL(Boolean languageflag)
        {
            foreach (Control col in frm.Controls)
            {
                if (col.GetType() != typeof(Label)) continue;
                for (dtemp = 0; dtemp <= list.Count - 1; dtemp++)
                {
                    if (list[dtemp].ToString().Contains(((Label)col).Tag.ToString()))
                    {
                        string[] sSet = list[dtemp].ToString().Split(',');
                        if (languageflag)
                            col.Text = sSet[2];
                        else
                            col.Text = sSet[1];
                    }
                }
            }
        }

        public static void changeDataGrid(Boolean languageflag)
        {
            int i = 0;
            foreach (Control col in frm.Controls)
            {
                if (col.GetType() != typeof(DataGridView)) continue;
                for (dtemp = 0; dtemp <= list.Count - 1; dtemp++)
                {
                    for (i = 0; i < ((DataGridView)col).Columns.Count; i++)
                    {
                        if (list[dtemp].ToString().Contains(((DataGridView)col).Columns[i].Name.ToString()))
                        {
                            string[] sSet = list[dtemp].ToString().Split(',');
                            if (languageflag)
                                ((DataGridView)col).Columns[i].HeaderText = sSet[2];
                            else
                                ((DataGridView)col).Columns[i].HeaderText = sSet[1];
                        }
                    }
                }
            }
        }

        public static void changeToolStrip(Boolean languageflag)
        {
            foreach (Control col in frm.Controls)
            {
                if (col.GetType() != typeof(ToolStrip)) continue;
                for (int i = 0; i < ((ToolStrip)col).Items.Count; i++)
                {
                    for (dtemp = 0; dtemp <= list.Count - 1; dtemp++)
                    {
                        if (list[dtemp].ToString().Contains(((ToolStrip)col).Items[i].Tag.ToString()))
                        {
                            string[] sSet = list[dtemp].ToString().Split(',');
                            if (languageflag)
                                col.Text = sSet[2];
                            else
                                col.Text = sSet[1];
                        }
                    }
                }
            }
        }
        

        public static void changeStatusStrip(Boolean languageflag)
        {

        }

        public static void changeRadioBTN(Boolean languageflag)
        {

        }

        public static void changeCheckBox(Boolean languageflag)
        {

        }

        public static void changeGroupBox(Boolean languageflag)
        {

        }

        public static void changePanel(Boolean languageflag,Control ctl)
        {
            if (ctl.GetType() == typeof(Panel))
            {
                foreach (Control cl in ctl.Controls)
                {
                    if (cl.GetType() == typeof(TextBox))
                        cl.Text = "2222";
                    if (cl.GetType() == typeof(RadioButton))
                        cl.Text = "1111";
                    if (cl.GetType() == typeof(CheckBox))
                        cl.Text = "check";
                    if (cl.GetType() == typeof(CheckBox))
                        cl.Text = "check";
                    if (cl.GetType() == typeof(CheckBox))
                        cl.Text = "check";
                    if (cl.GetType() == typeof(CheckBox))
                        cl.Text = "check";
                    if (cl.GetType() == typeof(CheckBox))
                        cl.Text = "check";
                }
            }
        }
    }

    #endregion
}
