﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CSICPR.QRCode;

//using System.Threading;

namespace CSICPR
{
    public partial class FormMain : Form
    {
        #region 私有变量
        private static ToolTip toolTip1 = new ToolTip();
        public static SolidBrush sbrush = new SolidBrush(SystemColors.MenuText);
        public static Boolean languageflag = true;
        #endregion
        private static List<LanguageItemModel> LanMessList;//定义语言集
        #region 私有方法
        private void showCover(bool isLoading, bool isLock = false)
        {
            FormCover f2 = new FormCover(isLoading, isLock);
            f2.Owner = this;
            if (!isLoading) f2.Size = this.Size;
            f2.ShowDialog();
            this.tsslCurUserName.Text = FormCover.CurrUserName;
            this.tsslCurWorkshop.Text = FormCover.CurrentFactory;
           
            
            //ToolStripQualityInspection.Visible=f2.IsEN();//Jacky 2015/11/27
            toolStripSeparator6.Visible=f2.IsEN();//Jacky 2015/11/27
        }
        public static void showTipMSG(string msg, Control target, string title = "", ToolTipIcon icon = ToolTipIcon.None)
        {
            toolTip1.ToolTipTitle = title;
            toolTip1.ToolTipIcon = icon;
            toolTip1.SetToolTip(target, msg);
            toolTip1.Show(msg, target, 6000);
        }       
        public bool isOpen(string strFormName, bool isActive = false)
        {
            for (int i = 0; i < this.MdiChildren.Length; i++)
            {
                if (this.MdiChildren[i].Text.Equals(strFormName))
                {
                    if (isActive)
                    {
                        this.MdiChildren[i].Activate();
                        if (this.MdiChildren[i].WindowState == FormWindowState.Minimized)
                            this.MdiChildren[i].WindowState = FormWindowState.Maximized;
                    }
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region 
        private void tsbCPTPacking_Click(object sender, EventArgs e)
        {
            FormPacking.Instance(this);
        }
        private void menuStrip1_ItemAdded(object sender, ToolStripItemEventArgs e)
        {
            if (e.Item.Text.Length == 0 || e.Item.Text == " 还原(&R) " || e.Item.Text == " 最小化(&N) " || e.Item.Text == " Restore Down(&R) " || e.Item.Text == " MinimizeBox(&N) ")
            {
                e.Item.Visible = false;
            }
        }
        private void tssbExit_ButtonClick(object sender, EventArgs e)
        {
            Application.Restart();
        }
        #endregion

        #region 帮助
        private void tsbWindowTile_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.Cascade);
        }
        private void tsbWindowHzt_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }
        private void tsbWindowVtcl_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
        }
        #endregion

        #region 页面事件
        /// <summary>
        /// 窗体加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMain_Load(object sender, EventArgs e)
        {
            LanguageHelper.getNames(this);
            this.Text = "[Version: " + Application.ProductVersion + " ]";
            if (ToolsClass.sSite != ToolsClass.CAMO)
            {
                this.packageReportByModuleToolStripMenuItem.Visible = false;
            }

            if (!FormCover.ShowInputStorageFlag.Trim().Equals("Y"))
            {
                this.ALLStorageToolStripMenuItem.Visible = false;
                this.SNStorageToolStripMenuItem.Visible = false;
            }


            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        public FormMain()
        	
        {
            InitializeComponent();
            toolTip1.IsBalloon = true;
            showCover(true);
             if (DataAccess.IsCtmJudge(FormCover.PlanCode, FormCover.CurrentFactory.Trim(), FormCover.InterfaceConnString))
            	this.tssbCTM.Visible=false;
            this.menuStrip1.MdiWindowListItem = this.menuHelp;
        }
        #endregion

        #region 注销
        private void tssbExit_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
        #endregion

        #region 系统设置
        /// <summary>
        /// 数据库设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItemBase_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("MenuItemBase", sender.ToString()))
                new FormAccount().ShowDialog();
            
        }
        /// <summary>
        /// 账户管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void accountManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
        	//if (FormCover.HasPower("accountManagerToolStripMenuItem"))
        	if(FormCover.CurrUserName.Equals("MCT",StringComparison.InvariantCultureIgnoreCase) || FormCover.CurrUserName.Equals("Operator",StringComparison.InvariantCultureIgnoreCase))
        		return;
            new FormAccountManager().ShowDialog();
        }
        /// <summary>
        /// 包装参数设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void packageConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("packageConfigToolStripMenuItem", sender.ToString()))
            {
                if (isOpen("组件打包"))//isOpen("Package")
                {
                    MessageBox.Show("设定包装参数前，请先关闭[组件打包]画面", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);//Please Close Package Form Before Setting Parameters
                }
                else
                {
                    new FormConfig("").ShowDialog();
                }

            }
        }
        /// <summary>
        /// 语言更改设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void changeToChineseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LanguageChg.Text == "Change to Chinese")
            {
                LanguageChg.Text = "切换为英文";
                languageflag = true;//英文显示
            }
            else
            {
                LanguageChg.Text = "Change to Chinese";
                languageflag = false;//中文显示
            }
        }
        /// <summary>
        /// CTM判断
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tssbCTM_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("tssbCTM", sender.ToString()))
                new FormCTM().ShowDialog();
        }
        #endregion

        #region 打包管理
        private void SNPackingToolStripMenuItem_Click(object sender, EventArgs e)
        {
        	if (FormCover.HasPower("SNPackingToolStripMenuItem"))
        	FormPacking.Instance(this);
        }

        private void TotalPackingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormPackingTogether.Instance(this);
        }
        #endregion

        #region 入库管理
        private void TotalStorageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormStorageCarton.Instance(this);
        }

        private void SNStorageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormStorageSN.Instance(this);
        }

        private void CancelStorageToolStripMenuItem_Click(object sender, EventArgs e)
        {
       		if (FormCover.HasPower("RevokeToolStripMenuItem"))
            FormCancelStorge.Instance(this);
        }
        private void NormalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("NormalToolStripMenuItemm"))
        	FormNormalStorage.Instance(this);
        }

        private void MaterialTemplateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("MaterialTemplateToolStripMenuItem"))
        	frmMaterialTemplate.Instance(this);
        }

        private void MaterialToolStripMenuItem_Click(object sender, EventArgs e)
        {      
            if (FormCover.HasPower("MaterialToolStripMenuItem"))
        	frmMaterial.Instance(this);
        }

        private void ManyStorageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormNormalStorageCarton.Instance(this);
        }

        private void ManyStorageSNToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormNormalStorageSN.Instance(this);
        }
        #endregion

        #region 接口管理
        
        /// <summary>
        /// SAP物料主数据下载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SAPMaterialMaterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("ToolStripMenuItemMaterial"))
        	FormSAPMASTERLoad.Instance(this);
        }
        /// <summary>
        /// SAP生产订单数据下载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SAPWoLoadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("ToolStripMenuItemOrderno"))
        	FormSAPWOLoad.Instance(this);
        }
        
        /// <summary>
        /// SAP物料转储数据下载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SAPMaterialTransferToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("ToolStripMenuItemStorage"))
        	FormSAPTransferLoad.Instance(this);
        }
        /// <summary>
        /// SAP重工工单下载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SAPReworkLoadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormReWorkOrder.Instance(this);
        }
        private void JobNoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormSAPJobNoLoad.Instance(this);
        }

        private void WODataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("ToolStripMenuItemMatainOrder"))
            FormWoData.Instance(this);
        }
        #endregion

        #region 下拉框
        private void toolStripSplitButton3_ButtonClick(object sender, EventArgs e)
        {
            toolStripSplitButton3.ShowDropDown();
        }

        private void toolStripSplitButton2_ButtonClick(object sender, EventArgs e)
        {
            toolStripSplitButton2.ShowDropDown();
        }

        private void toolStripSplitButton1_ButtonClick(object sender, EventArgs e)
        {
            toolStripSplitButton1.ShowDropDown();
        }

        private void tssbChart_ButtonClick(object sender, EventArgs e)
        {
            tssbChart.ShowDropDown();
        }

        private void tssbSystem_ButtonClick(object sender, EventArgs e)
        {
            tssbSystem.ShowDropDown();
        }
        #endregion

        #region 报表管理
        private void SAPStorageReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormStorageReport.Instance(this);
        } 
        private void MenuItemPackage_Click(object sender, EventArgs e)
        {
            if (isOpen(ToolsClass.ReportByDate, true))
            {
            }
            else
            {
                if (ToolsClass.sSite == ToolsClass.CAMO)
                    new FormAnalyse(this, ToolsClass.iDateReport);
                else
                    new FormAnalyseCS(this, ToolsClass.iDateReport);
            }
        }

        private void packageReportByCartonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isOpen(ToolsClass.ReportByCarton, true))
            {
            }
            else
            {
                if (ToolsClass.sSite == ToolsClass.CAMO)//2011-08-10|add by alex.dong|For distinguish Workshop
                    new FormAnalyse(this, ToolsClass.iCartonReport);
                else
                    new FormAnalyseCS(this, ToolsClass.iCartonReport); //
            }
        }

        private void SAPStorageDReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormStorageDealReport.Instance(this);
        }

        private void SAPWOQueryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormWoQueryReport.Instance(this);
        }
        private void packageReportByModuleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isOpen(ToolsClass.ReportByModule, true))
            {
            }
            else
                new FormAnalyse(this, ToolsClass.iModuleReport);
        }
        #endregion                

        #region 标签列印
        private void BarcodePrint_ButtonClick(object sender, EventArgs e)
        {
            BarcodePrint.ShowDropDown();
        }
        private void SideBarcodePrint_Click(object sender, EventArgs e)
        {
            FormPrintSidebarcode.Instance(this);
        }
        #endregion

        private void toolStripMenuItem入库上传SAP_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripMenuItemUploadSap"))
        	FrmSapInventoryUploadManul.Instance(this);
        }

        private void toolStripMenuItem入库上传结果查询_Click(object sender, EventArgs e)
        {
            FrmSapInventoryUploadResult.Instance(this);
        }

        private void toolStripMenuItem入库数据查询_Click(object sender, EventArgs e)
        {
            FrmSapInventoryDataQuery.Instance(this);
        }

        private void toolStripMenuItem取消入库_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripMenuItemCancel"))
        	FrmCancelInventory.Instance(this);
        }

        private void toolStripMenuItem组件重工注册_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripMenuItemReworkRegister"))
        	FrmRegReworkModule.Instance(this);
            
        }

        private void toolStripMenuItemSapBatchDataQuery_Click(object sender, EventArgs e)
        {
            FrmSapBatchDataQuery.Instance(this);
        }

        private void 二维码参数配置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            QRCodeBase.Instance(this);
        }

        private void 二维码商标打印ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            QRCodePrint.Instance(this);
        }
        
      
        
        
        void ToolStripQualityInspectionClick(object sender, EventArgs e)//Jacky 2015/11/27
        {
        	 if (FormCover.HasPower("ToolStripQualityInspection"))
        	FormQualityInspection.Instance(this);
        }
        
        void UploadStatusQueryToolStripMenuItemClick(object sender, EventArgs e)
        {
        	FormQueryUploadStatus.Instance(this);
        }


        public static FormLineStagingArea theSingleton = null;

        
        private void mOChangeOverToolStripMenuItem_Click(object sender, EventArgs e)//Qing 2016/05/05
        {
            if (FormCover.HasPowerControl("FormLineModify"))
                FormChangeOver.Instance(this);
        }

       
        private void lineAToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
                FormLineACellLoss.Instance(this);
        }

        private void lineBToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
                FormLineBCellLoss.Instance(this);
        }

        private void lineDToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
                FormLineDCellLoss.Instance(this);
        }

        public static FormGlass Glass = null;
        

        public static FormScrap Scrap = null;

        private void lineAToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("A", "Glass", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("A", "Glass", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineBToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("B", "Glass", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("B", "Glass", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineAToolStripMenuItem2_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("A", "406 EVA", "KG", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("A", "406 EVA", "KG", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineBToolStripMenuItem2_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("B", "406 EVA", "KG", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("B", "406 EVA", "KG", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineAToolStripMenuItem3_Click_1(object sender, EventArgs e)
        {
            if (null == Scrap || Scrap.IsDisposed)
            {

                Scrap = new FormScrap("A", "Ribbon", "Kg", "0");
                Scrap.MdiParent = this;
                Scrap.WindowState = FormWindowState.Maximized;
                Scrap.Show();
            }
            else
            {
                Scrap.Close();
                Scrap = new FormScrap("A", "Ribbon", "Kg", "0");
                Scrap.MdiParent = this;
                Scrap.WindowState = FormWindowState.Maximized;
                Scrap.Show();
            }
        }

        private void lineBToolStripMenuItem3_Click_1(object sender, EventArgs e)
        {
            if (null == Scrap || Scrap.IsDisposed)
            {

                Scrap = new FormScrap("B", "Ribbon", "Kg", "0");
                Scrap.MdiParent = this;
                Scrap.WindowState = FormWindowState.Maximized;
                Scrap.Show();
            }
            else
            {
                Scrap.Close();
                Scrap = new FormScrap("B", "Ribbon", "Kg", "0");
                Scrap.MdiParent = this;
                Scrap.WindowState = FormWindowState.Maximized;
                Scrap.Show();
            }
        }

        private void lineAToolStripMenuItem4_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("A", "806 EVA", "KG", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("A", "806 EVA", "KG", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineBToolStripMenuItem4_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("B", "806 EVA", "KG", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("B", "806 EVA", "KG", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineAToolStripMenuItem5_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("A", "Backsheet", "KG", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("A", "Backsheet", "KG", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineBToolStripMenuItem5_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("B", "Backsheet", "KG", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("B", "Backsheet", "KG", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineAToolStripMenuItem6_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("A", "J-Box", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("A", "J-Box", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineBToolStripMenuItem6_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("B", "J-Box", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("B", "J-Box", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineAToolStripMenuItem7_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("A", "Frame(short)", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("A", "Frame(short)", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineBToolStripMenuItem7_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("B", "Frame(short)", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("B", "Frame(short)", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineAToolStripMenuItem8_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("A", "Frame(long)", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("A", "Frame(long)", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineBToolStripMenuItem8_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("B", "Frame(long)", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("B", "Frame(long)", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineAToolStripMenuItem9_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("A", "CrossBar", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("A", "CrossBar", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineBToolStripMenuItem9_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("B", "CrossBar", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("B", "CrossBar", "pcs", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineAToolStripMenuItem10_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("A", "Sealnat MS930", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("A", "Sealnat MS930", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineBToolStripMenuItem10_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("B", "Sealnat MS930", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("B", "Sealnat MS930", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineAToolStripMenuItem11_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("A", "Sealnat 1521A", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("A", "Sealnat 1521A", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineBToolStripMenuItem11_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("B", "Sealnat 1521A", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("B", "Sealnat 1521A", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineAToolStripMenuItem12_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("A", "Sealnat 1527", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("A", "Sealnat 1527", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineBToolStripMenuItem12_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("B", "Sealnat 1527", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("B", "Sealnat 1527", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineAToolStripMenuItem13_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("A", "Sealnat 1521B", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("A", "Sealnat 1521B", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineBToolStripMenuItem13_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("B", "Sealnat 1521B", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("B", "Sealnat 1521B", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineAToolStripMenuItem14_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("A", "Sealnat MS 9371B", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("A", "Sealnat MS 9371B", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void lineBToolStripMenuItem14_Click_1(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Scrap || Scrap.IsDisposed)
                {

                    Scrap = new FormScrap("B", "Sealnat MS 9371B", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }
                else
                {
                    Scrap.Close();
                    Scrap = new FormScrap("B", "Sealnat MS 9371B", "Kg", "0");
                    Scrap.MdiParent = this;
                    Scrap.WindowState = FormWindowState.Maximized;
                    Scrap.Show();
                }

            }
        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("A", "Glass", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("A", "Glass", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("B", "Glass", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("B", "Glass", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("A", "406 EVA", "M2", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("A", "406 EVA", "M2", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem11_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("B", "406 EVA", "M2", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("B", "406 EVA", "M2", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem13_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("A", "Ribbon", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("A", "Ribbon", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem14_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("B", "Ribbon", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("B", "Ribbon", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem16_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("A", "806 EVA", "M2", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("A", "806 EVA", "M2", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem17_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("B", "806 EVA", "M2", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("B", "806 EVA", "M2", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem19_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("A", "Backsheet", "M2", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("A", "Backsheet", "M2", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem20_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("B", "Backsheet", "M2", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("B", "Backsheet", "M2", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem22_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("A", "J-Box", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("A", "J-Box", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem23_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("B", "J-Box", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("B", "J-Box", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem25_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("A", "Frame(short)", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("A", "Frame(short)", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem26_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("B", "Frame(short)", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("B", "Frame(short)", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem28_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("A", "Frame(long)", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("A", "Frame(long)", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem29_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("B", "Frame(long)", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("B", "Frame(long)", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem31_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("A", "CrossBar", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("A", "CrossBar", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem32_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("B", "CrossBar", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("B", "CrossBar", "pcs", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem34_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("A", "Sealnat MS930", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("A", "Sealnat MS930", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem35_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("B", "Sealnat MS930", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("B", "Sealnat MS930", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem37_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("A", "Sealnat 1521A", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("A", "Sealnat 1521A", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem38_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("B", "Sealnat 1521A", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("B", "Sealnat 1521A", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem40_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("A", "Sealnat 1527", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("A", "Sealnat 1527", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem41_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("B", "Sealnat 1527", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("B", "Sealnat 1527", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem43_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("A", "Sealnat 1521B", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("A", "Sealnat 1521B", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem44_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("B", "Sealnat 1521B", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("B", "Sealnat 1521B", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem46_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("A", "Sealnat MS 9371B", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("A", "Sealnat MS 9371B", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem47_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == Glass || Glass.IsDisposed)
                {

                    Glass = new FormGlass("B", "Sealnat MS 9371B", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }
                else
                {
                    Glass.Close();
                    Glass = new FormGlass("B", "Sealnat MS 9371B", "Kg", "0");
                    Glass.MdiParent = this;
                    Glass.WindowState = FormWindowState.Maximized;
                    Glass.Show();
                }

            }
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == theSingleton || theSingleton.IsDisposed)
                {

                    theSingleton = new FormLineStagingArea("A");
                    theSingleton.MdiParent = this;
                    theSingleton.WindowState = FormWindowState.Maximized;
                    theSingleton.Show();
                }
                else
                {
                    theSingleton.Close();
                    theSingleton = new FormLineStagingArea("A");
                    theSingleton.MdiParent = this;
                    theSingleton.WindowState = FormWindowState.Maximized;
                    theSingleton.Show();
                }

            }
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {


                if (null == theSingleton || theSingleton.IsDisposed)
                {

                    theSingleton = new FormLineStagingArea("B");
                    theSingleton.MdiParent = this;
                    theSingleton.WindowState = FormWindowState.Maximized;
                    theSingleton.Show();
                }
                else
                {
                    theSingleton.Close();
                    theSingleton = new FormLineStagingArea("B");
                    theSingleton.MdiParent = this;
                    theSingleton.WindowState = FormWindowState.Maximized;
                    theSingleton.Show();
                }
            }
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            if (FormCover.HasPower("toolStripSplitButton4"))
            {
                if (null == theSingleton || theSingleton.IsDisposed)
                {

                    theSingleton = new FormLineStagingArea("D");
                    theSingleton.MdiParent = this;
                    theSingleton.WindowState = FormWindowState.Maximized;
                    theSingleton.Show();
                }
                else
                {
                    theSingleton.Close();
                    theSingleton = new FormLineStagingArea("D");
                    theSingleton.MdiParent = this;
                    theSingleton.WindowState = FormWindowState.Maximized;
                    theSingleton.Show();
                }

            }
        }
		void ToolStripSplitButton4ButtonClick(object sender, EventArgs e)
		{
	
		}
		void ToolStripMenuItem2Click(object sender, EventArgs e)
		{
	
		}
    }
}