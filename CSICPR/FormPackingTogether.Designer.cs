﻿namespace CSICPR
{
    partial class FormPackingTogether
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tbCustBoxCode = new System.Windows.Forms.TextBox();
            this.btnBatchMode = new System.Windows.Forms.Button();
            this.tbBoxCode = new System.Windows.Forms.TextBox();
            this.tbBarCode = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPatternFlag = new System.Windows.Forms.CheckBox();
            this.tbNominalPower = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.nudIdealPower = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRework = new System.Windows.Forms.TextBox();
            this.tbNominalPowerGrade = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chbCustCarton = new System.Windows.Forms.CheckBox();
            this.chbCarton = new System.Windows.Forms.CheckBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tbHandPackingSQL = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.btPacking = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btRePrint = new System.Windows.Forms.Button();
            this.btTodayNoPackge = new System.Windows.Forms.Button();
            this.btMixPackage = new System.Windows.Forms.Button();
            this.btTodayPackged = new System.Windows.Forms.Button();
            this.btColose = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lstView = new System.Windows.Forms.ListView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudIdealPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tbCustBoxCode);
            this.panel2.Controls.Add(this.btnBatchMode);
            this.panel2.Controls.Add(this.tbBoxCode);
            this.panel2.Controls.Add(this.tbBarCode);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.chbCustCarton);
            this.panel2.Controls.Add(this.chbCarton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1022, 69);
            this.panel2.TabIndex = 1;
            // 
            // tbCustBoxCode
            // 
            this.tbCustBoxCode.BackColor = System.Drawing.SystemColors.Window;
            this.tbCustBoxCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbCustBoxCode.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.tbCustBoxCode.Location = new System.Drawing.Point(359, 37);
            this.tbCustBoxCode.MaxLength = 16;
            this.tbCustBoxCode.Name = "tbCustBoxCode";
            this.tbCustBoxCode.Size = new System.Drawing.Size(166, 26);
            this.tbCustBoxCode.TabIndex = 7;
            this.tbCustBoxCode.TextChanged += new System.EventHandler(this.tbCustBoxCode_TextChanged);
            // 
            // btnBatchMode
            // 
            this.btnBatchMode.Location = new System.Drawing.Point(546, 17);
            this.btnBatchMode.Name = "btnBatchMode";
            this.btnBatchMode.Size = new System.Drawing.Size(85, 28);
            this.btnBatchMode.TabIndex = 6;
            this.btnBatchMode.Text = "包装参数配置";
            this.btnBatchMode.UseVisualStyleBackColor = true;
            this.btnBatchMode.Click += new System.EventHandler(this.btnBatchMode_Click);
            // 
            // tbBoxCode
            // 
            this.tbBoxCode.BackColor = System.Drawing.SystemColors.Window;
            this.tbBoxCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbBoxCode.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.tbBoxCode.Location = new System.Drawing.Point(359, 3);
            this.tbBoxCode.MaxLength = 16;
            this.tbBoxCode.Name = "tbBoxCode";
            this.tbBoxCode.Size = new System.Drawing.Size(166, 26);
            this.tbBoxCode.TabIndex = 1;
            this.tbBoxCode.TextChanged += new System.EventHandler(this.tbBoxCode_TextChanged);
            // 
            // tbBarCode
            // 
            this.tbBarCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbBarCode.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbBarCode.Location = new System.Drawing.Point(78, 3);
            this.tbBarCode.MaxLength = 5000;
            this.tbBarCode.Multiline = true;
            this.tbBarCode.Name = "tbBarCode";
            this.tbBarCode.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbBarCode.Size = new System.Drawing.Size(181, 63);
            this.tbBarCode.TabIndex = 0;
            this.tbBarCode.Enter += new System.EventHandler(this.textBox_Enter);
            this.tbBarCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBarCode_KeyPress);
            this.tbBarCode.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label1);
            this.panel6.Controls.Add(this.cbPatternFlag);
            this.panel6.Controls.Add(this.tbNominalPower);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.nudIdealPower);
            this.panel6.Controls.Add(this.label2);
            this.panel6.Controls.Add(this.txtRework);
            this.panel6.Controls.Add(this.tbNominalPowerGrade);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(637, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(385, 69);
            this.panel6.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(218, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 14);
            this.label1.TabIndex = 36;
            this.label1.Text = "标称功率档次:";
            this.label1.Visible = false;
            // 
            // cbPatternFlag
            // 
            this.cbPatternFlag.AutoSize = true;
            this.cbPatternFlag.Location = new System.Drawing.Point(12, 24);
            this.cbPatternFlag.Name = "cbPatternFlag";
            this.cbPatternFlag.Size = new System.Drawing.Size(66, 16);
            this.cbPatternFlag.TabIndex = 35;
            this.cbPatternFlag.Text = "Pattern";
            this.cbPatternFlag.UseVisualStyleBackColor = true;
            this.cbPatternFlag.Visible = false;
            this.cbPatternFlag.CheckedChanged += new System.EventHandler(this.cbPatternFlag_CheckedChanged);
            // 
            // tbNominalPower
            // 
            this.tbNominalPower.BackColor = System.Drawing.SystemColors.Window;
            this.tbNominalPower.Location = new System.Drawing.Point(146, 22);
            this.tbNominalPower.Name = "tbNominalPower";
            this.tbNominalPower.ReadOnly = true;
            this.tbNominalPower.Size = new System.Drawing.Size(40, 21);
            this.tbNominalPower.TabIndex = 34;
            this.tbNominalPower.Visible = false;
            this.tbNominalPower.TextChanged += new System.EventHandler(this.tbNominalPower_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(188, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 12);
            this.label4.TabIndex = 33;
            this.label4.Text = "瓦";
            this.label4.Visible = false;
            // 
            // nudIdealPower
            // 
            this.nudIdealPower.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.nudIdealPower.Location = new System.Drawing.Point(163, 22);
            this.nudIdealPower.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudIdealPower.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudIdealPower.Name = "nudIdealPower";
            this.nudIdealPower.ReadOnly = true;
            this.nudIdealPower.Size = new System.Drawing.Size(23, 21);
            this.nudIdealPower.TabIndex = 31;
            this.nudIdealPower.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudIdealPower.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.nudIdealPower.Visible = false;
            this.nudIdealPower.ValueChanged += new System.EventHandler(this.nudIdealPower_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(80, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 14);
            this.label2.TabIndex = 32;
            this.label2.Text = "标称功率:";
            this.label2.Visible = false;
            // 
            // txtRework
            // 
            this.txtRework.BackColor = System.Drawing.SystemColors.Window;
            this.txtRework.Location = new System.Drawing.Point(56, 22);
            this.txtRework.Name = "txtRework";
            this.txtRework.ReadOnly = true;
            this.txtRework.Size = new System.Drawing.Size(111, 21);
            this.txtRework.TabIndex = 38;
            this.txtRework.Visible = false;
            this.txtRework.WordWrap = false;
            // 
            // tbNominalPowerGrade
            // 
            this.tbNominalPowerGrade.BackColor = System.Drawing.SystemColors.Window;
            this.tbNominalPowerGrade.Location = new System.Drawing.Point(317, 22);
            this.tbNominalPowerGrade.Name = "tbNominalPowerGrade";
            this.tbNominalPowerGrade.ReadOnly = true;
            this.tbNominalPowerGrade.Size = new System.Drawing.Size(43, 21);
            this.tbNominalPowerGrade.TabIndex = 37;
            this.tbNominalPowerGrade.Visible = false;
            this.tbNominalPowerGrade.WordWrap = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(10, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 14);
            this.label6.TabIndex = 3;
            this.label6.Text = "组件号码:";
            // 
            // chbCustCarton
            // 
            this.chbCustCarton.AutoSize = true;
            this.chbCustCarton.Font = new System.Drawing.Font("宋体", 10.5F);
            this.chbCustCarton.Location = new System.Drawing.Point(274, 39);
            this.chbCustCarton.Name = "chbCustCarton";
            this.chbCustCarton.Size = new System.Drawing.Size(89, 18);
            this.chbCustCarton.TabIndex = 10;
            this.chbCustCarton.Text = "客户托号:";
            this.chbCustCarton.UseVisualStyleBackColor = true;
            this.chbCustCarton.CheckedChanged += new System.EventHandler(this.chbCustCarton_CheckedChanged);
            // 
            // chbCarton
            // 
            this.chbCarton.AutoSize = true;
            this.chbCarton.Font = new System.Drawing.Font("宋体", 10.5F);
            this.chbCarton.Location = new System.Drawing.Point(274, 9);
            this.chbCarton.Name = "chbCarton";
            this.chbCarton.Size = new System.Drawing.Size(89, 18);
            this.chbCarton.TabIndex = 9;
            this.chbCarton.Text = "内部托号:";
            this.chbCarton.UseVisualStyleBackColor = true;
            this.chbCarton.CheckedChanged += new System.EventHandler(this.chbCarton_CheckedChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonShadow;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeight = 30;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column13,
            this.Column8,
            this.Column6,
            this.Column7,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column9});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowHeadersWidth = 40;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1022, 403);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            this.dataGridView1.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridView1_UserDeletingRow);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tbHandPackingSQL);
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.btPacking);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 623);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1022, 50);
            this.panel1.TabIndex = 3;
            // 
            // tbHandPackingSQL
            // 
            this.tbHandPackingSQL.Location = new System.Drawing.Point(414, 33);
            this.tbHandPackingSQL.MaxLength = 800;
            this.tbHandPackingSQL.Multiline = true;
            this.tbHandPackingSQL.Name = "tbHandPackingSQL";
            this.tbHandPackingSQL.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbHandPackingSQL.Size = new System.Drawing.Size(10, 10);
            this.tbHandPackingSQL.TabIndex = 0;
            this.tbHandPackingSQL.Visible = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Enabled = false;
            this.checkBox1.Location = new System.Drawing.Point(167, 18);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(120, 16);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.Text = "启用组件遏制检测";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            this.checkBox1.Click += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // btPacking
            // 
            this.btPacking.Enabled = false;
            this.btPacking.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btPacking.Location = new System.Drawing.Point(23, 7);
            this.btPacking.Name = "btPacking";
            this.btPacking.Size = new System.Drawing.Size(82, 36);
            this.btPacking.TabIndex = 0;
            this.btPacking.Text = "打包";
            this.btPacking.UseVisualStyleBackColor = true;
            this.btPacking.Click += new System.EventHandler(this.btPacking_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btRePrint);
            this.panel3.Controls.Add(this.btTodayNoPackge);
            this.panel3.Controls.Add(this.btMixPackage);
            this.panel3.Controls.Add(this.btTodayPackged);
            this.panel3.Controls.Add(this.btColose);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(430, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(592, 50);
            this.panel3.TabIndex = 1;
            // 
            // btRePrint
            // 
            this.btRePrint.Location = new System.Drawing.Point(340, 13);
            this.btRePrint.Name = "btRePrint";
            this.btRePrint.Size = new System.Drawing.Size(75, 28);
            this.btRePrint.TabIndex = 4;
            this.btRePrint.Text = "重打标签";
            this.btRePrint.UseVisualStyleBackColor = true;
            this.btRePrint.Visible = false;
            this.btRePrint.Click += new System.EventHandler(this.btRePrint_Click);
            // 
            // btTodayNoPackge
            // 
            this.btTodayNoPackge.Enabled = false;
            this.btTodayNoPackge.Location = new System.Drawing.Point(3, 11);
            this.btTodayNoPackge.Name = "btTodayNoPackge";
            this.btTodayNoPackge.Size = new System.Drawing.Size(16, 23);
            this.btTodayNoPackge.TabIndex = 2;
            this.btTodayNoPackge.Text = "TodayUnpack";
            this.btTodayNoPackge.UseVisualStyleBackColor = true;
            this.btTodayNoPackge.Visible = false;
            // 
            // btMixPackage
            // 
            this.btMixPackage.Location = new System.Drawing.Point(52, 13);
            this.btMixPackage.Name = "btMixPackage";
            this.btMixPackage.Size = new System.Drawing.Size(106, 28);
            this.btMixPackage.TabIndex = 0;
            this.btMixPackage.Text = "零散件数据提取";
            this.btMixPackage.UseVisualStyleBackColor = true;
            this.btMixPackage.Visible = false;
            this.btMixPackage.Click += new System.EventHandler(this.btMixPackage_Click);
            // 
            // btTodayPackged
            // 
            this.btTodayPackged.Location = new System.Drawing.Point(198, 13);
            this.btTodayPackged.Name = "btTodayPackged";
            this.btTodayPackged.Size = new System.Drawing.Size(100, 28);
            this.btTodayPackged.TabIndex = 1;
            this.btTodayPackged.Text = "替换组件";
            this.btTodayPackged.UseVisualStyleBackColor = true;
            this.btTodayPackged.Visible = false;
            this.btTodayPackged.Click += new System.EventHandler(this.btTodayPackged_Click);
            // 
            // btColose
            // 
            this.btColose.Location = new System.Drawing.Point(464, 13);
            this.btColose.Name = "btColose";
            this.btColose.Size = new System.Drawing.Size(82, 28);
            this.btColose.TabIndex = 3;
            this.btColose.Tag = "";
            this.btColose.Text = "拆箱";
            this.btColose.UseVisualStyleBackColor = true;
            this.btColose.Click += new System.EventHandler(this.btColose_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lstView);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(3, 475);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1022, 148);
            this.panel4.TabIndex = 4;
            // 
            // lstView
            // 
            this.lstView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstView.FullRowSelect = true;
            this.lstView.Location = new System.Drawing.Point(0, 0);
            this.lstView.MultiSelect = false;
            this.lstView.Name = "lstView";
            this.lstView.Size = new System.Drawing.Size(1022, 148);
            this.lstView.TabIndex = 0;
            this.lstView.UseCompatibleStateImageBehavior = false;
            this.lstView.View = System.Windows.Forms.View.Details;
            this.lstView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstView_KeyDown);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.dataGridView1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 72);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1022, 403);
            this.panel5.TabIndex = 5;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "组件号码";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 120;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "规格型号";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 80;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "生产车间";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 60;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "实测功率";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 60;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "标称功率";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 60;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "IM";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Width = 42;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "标称功率档次";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "检测状态";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 60;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "包装工单";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "入库工单";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "重工工单";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.Column12.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Column12.HeaderText = "组件等级";
            this.Column12.Name = "Column12";
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column9.HeaderText = "";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // FormPackingTogether
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 676);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "FormPackingTogether";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.Text = "并托打包";
            this.Activated += new System.EventHandler(this.FormPacking_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPacking_FormClosing);
            this.Load += new System.EventHandler(this.FormPacking_Load);
            this.Resize += new System.EventHandler(this.FormPacking_Resize);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudIdealPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnBatchMode;
        private System.Windows.Forms.TextBox tbBoxCode;
        private System.Windows.Forms.TextBox tbBarCode;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button btPacking;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btRePrint;
        private System.Windows.Forms.Button btTodayNoPackge;
        private System.Windows.Forms.Button btMixPackage;
        private System.Windows.Forms.Button btTodayPackged;
        private System.Windows.Forms.Button btColose;
        private System.Windows.Forms.TextBox tbHandPackingSQL;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.NumericUpDown nudIdealPower;
        private System.Windows.Forms.ListView lstView;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox tbNominalPower;
        private System.Windows.Forms.CheckBox cbPatternFlag;
        private System.Windows.Forms.TextBox tbNominalPowerGrade;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtRework;
        private System.Windows.Forms.TextBox tbCustBoxCode;
        private System.Windows.Forms.CheckBox chbCustCarton;
        private System.Windows.Forms.CheckBox chbCarton;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;

    }
}