﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CSICPR
{
    public partial class FormBoxHandling : Form
    {
        private static List<LanguageItemModel> LanMessList;
        #region 私有变量
        private string snKick;

        private bool isModel, isTrueP, hasPar2;

        private FormPacking fp;

        private FormPackingTogether fp1;

        /// <summary>
        /// 是否拼托使用，Y是拼托
        /// </summary>
        private string ReworkFlag = "";

        #endregion

        #region 私有方法
        /// <summary>
        /// 绘制GRID后触发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pubRowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            e.Graphics.DrawString((e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, FormMain.sbrush, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 5);
        }

        /// <summary>
        /// 查询数据并填充Grid
        /// </summary>
        /// <param name="SqlStr"></param>
        /// <param name="SqlConn"></param>
        private void GetDataReader(string SqlStr, string SqlConn = "")
        {
            if (string.IsNullOrEmpty(SqlConn))
                SqlConn = FormCover.connectionBase;

            #region
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(SqlConn))
            {
                using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
                {
                    try
                    {
                        cmd.CommandText = SqlStr;
                        cmd.CommandType = CommandType.Text;

                        if (conn.State == ConnectionState.Closed)
                            conn.Open();

                        using (SqlDataReader read = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                        {
                            if (read != null)
                            {
                                object[] objArr = new object[read.FieldCount];
                                string sn = "", temp = "";
                                while (read.Read())
                                {
                                    temp = read.GetString(0);
                                    if (sn == temp)
                                        continue;
                                    else
                                        sn = temp;
                                    read.GetValues(objArr);
                                    dataGridView1.Rows.Add(objArr);
                                }
                            }
                            read.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            #endregion
        }

        /// <summary>
        /// 组件打包，删除集中数据库，并更新包装数据库数据
        /// </summary>
        /// <param name="cartonNo">箱号</param>
        public bool UnPacking(string cartonNo, out string msg)
        {
            try
            {
                msg = "";

                #region
                string sql_rework = @"UPDATE {0}.dbo.[T_MODULE_REWORK] SET FLAG='0' WHERE 
                                      MODULE_SN IN (SELECT A.MODULE_SN FROM {0}.dbo.[T_MODULE] A with(nolock) ,{0}.dbo.T_MODULE_CARTON B WITH(NOLOCK),{0}.dbo.T_MODULE_PACKING_LIST C WITH(NOLOCK)
                                      WHERE B.SYSID = C.MODULE_CARTON_SYSID AND A.SYSID = C.MODULE_SYSID 
                                      AND B.RESV03='{1}')";
                sql_rework = string.Format(sql_rework, FormCover.CenterDBName.Trim(), cartonNo.Trim());

                string sql_TestDat = @"DELETE FROM {0}.dbo.T_MODULE_TEST  
                                      WHERE MODULE_SN IN (SELECT A.MODULE_SN FROM {0}.dbo.[T_MODULE] A with(nolock) ,{0}.dbo.T_MODULE_CARTON B WITH(NOLOCK),{0}.dbo.T_MODULE_PACKING_LIST C WITH(NOLOCK)
                                      WHERE B.SYSID = C.MODULE_CARTON_SYSID AND A.SYSID = C.MODULE_SYSID 
                                      AND B.RESV03='{1}')";
                
               // string sql_TestDat = @"DELETE FROM {0}.dbo.T_MODULE_TEST  where MODULE_SN in (select distinct sn from LabelPrintDB.dbo.product where boxid = '{1}')";

                string sql_Module = @"DELETE FROM {0}.dbo.[T_MODULE] 
                                        WHERE  SYSID IN (SELECT C.MODULE_SYSID 
                                                          FROM {0}.dbo.T_MODULE_CARTON B WITH(NOLOCK),{0}.dbo.T_MODULE_PACKING_LIST C WITH(NOLOCK)
				                                          WHERE B.SYSID = C.MODULE_CARTON_SYSID 
                                                          AND B.RESV03='{1}')";

                string sql_PackingList = @"DELETE FROM {0}.dbo.T_MODULE_PACKING_LIST 
                                          WHERE MODULE_CARTON_SYSID  IN (SELECT SYSID FROM  {0}.dbo.T_MODULE_CARTON WITH(NOLOCK) WHERE RESV03='{1}')";

                string sql_Carton = @"DELETE FROM {0}.dbo.T_MODULE_CARTON WHERE RESV03='{1}'";

                string sql_Carton_count = @"SELECT TOP 100 * FROM {0}.dbo.T_MODULE_CARTON WITH(NOLOCK) WHERE RESV03='{1}'";

                sql_TestDat = string.Format(sql_TestDat, FormCover.CenterDBName.Trim(), cartonNo.Trim());
                sql_Module = string.Format(sql_Module, FormCover.CenterDBName.Trim(), cartonNo.Trim());
                sql_PackingList = string.Format(sql_PackingList, FormCover.CenterDBName.Trim(), cartonNo.Trim());
                sql_Carton = string.Format(sql_Carton, FormCover.CenterDBName.Trim(), cartonNo.Trim());
                sql_Carton_count = string.Format(sql_Carton_count, FormCover.CenterDBName.Trim(), cartonNo.Trim());
                #endregion

                #region
                using (SqlConnection connCenter = new SqlConnection(DbHelperSQL.sqlConn))//集中数据库 
                {
                    connCenter.Open();
                    if (ProductStorageDAL.GetSysMapping("PackingSystemSameFlag", FormCover.CurrentFactory).Trim().ToUpper().Equals("Y"))
                    {
                        #region
                        try
                        {
                            using (SqlTransaction transCenter = connCenter.BeginTransaction())
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Connection = connCenter;
                                    cmd.Transaction = transCenter;

                                    #region 更新包装数据库
                                    cmd.CommandText = string.Format(@"UPDATE {0}.dbo.Product Set Process='T4',BoxID='',[flag]='0',StdPower='',Pmax='',UnpackOperator='{1}',UnpackDate = getdate(),[Cust_BoxID]='' WHERE BoxID='{2}' and [flag]='1'", FormCover.PackingDBName.Trim(), FormCover.CurrUserWorkID, cartonNo);
                                    if (cmd.ExecuteNonQuery() < 1)
                                    {
                                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + cmd.CommandText, "ABNORMAL", null);
                                        msg = LanguageHelper.GetMessage(LanMessList, "Message30", "拆包失败:原因更新包装数据库数据失败:") + cmd.CommandText;
                                        transCenter.Rollback();
                                        return false;
                                    }
                                    cmd.CommandText = string.Format(@"Update {0}.dbo.Box set IsUsed='L',JobNo='',[Number]='0',[BoxType]='',[flag]='0', UnpackOperator='{1}',UnpackDate = getdate() where BoxID='{2}' and flag='1'", FormCover.PackingDBName.Trim(), FormCover.CurrUserWorkID, cartonNo);
                                    if (cmd.ExecuteNonQuery() < 1)
                                    {
                                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + cmd.CommandText, "ABNORMAL", null);
                                        msg = LanguageHelper.GetMessage(LanMessList, "Message30", "拆包失败:原因更新包装数据库数据失败:") + cmd.CommandText;
                                        transCenter.Rollback();
                                        return false;
                                    }
                                    #endregion

                                    #region 更新集中数据库
                                    #region
                                    try
                                    {
                                        DataSet ds = DbHelperSQL.Query(DbHelperSQL.sqlConn, sql_Carton_count, null);
                                        if (ds != null)
                                        {
                                            if (ds.Tables[0].Rows.Count > 1)
                                            {
                                                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + sql_Carton_count, "ABNORMAL", null);
                                                msg =  LanguageHelper.GetMessage(LanMessList, "Message31", "拆包失败:原因查询集中数据库数据失败:") + cmd.CommandText;
                                                transCenter.Rollback();
                                                return false;
                                            }
                                        }
                                        else
                                        {
                                            ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + sql_Carton_count, "ABNORMAL", null);
                                            //msg = "拆包失败:原因查询集中数据库数据失败:" + cmd.CommandText;
                                            msg = LanguageHelper.GetMessage(LanMessList, "Message31", "拆包失败:原因查询集中数据库数据失败:") + cmd.CommandText; transCenter.Rollback();
                                           transCenter.Rollback();
                                            return false;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + ex.Message, "ABNORMAL", null);
                                        transCenter.Rollback();
                                        return false;
                                    }
                                    #endregion

                                    cmd.CommandText = sql_rework;
                                    if (cmd.ExecuteNonQuery() < 0)
                                    {
                                         msg = LanguageHelper.GetMessage(LanMessList, "Message32", "拆包失败:更新集中数据库数据失败:") + cmd.CommandText; transCenter.Rollback();
                                        transCenter.Rollback();                                      
                                        return false;
                                    }

                                    cmd.CommandText = sql_TestDat;
                                    if (cmd.ExecuteNonQuery() < 1)
                                    {
                                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + sql_TestDat, "ABNORMAL", null);
                                        //msg = "拆包失败:更新集中数据库数据失败" + cmd.CommandText;
                                        msg = LanguageHelper.GetMessage(LanMessList, "Message32", "拆包失败:更新集中数据库数据失败:") + cmd.CommandText; transCenter.Rollback();
                                        transCenter.Rollback();
                                        return false;
                                    }

                                    cmd.CommandText = sql_Module;
                                    if (cmd.ExecuteNonQuery() < 1)
                                    {
                                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + sql_Module, "ABNORMAL", null);
                                       // msg = "拆包失败:更新集中数据库数据失败:" + cmd.CommandText;
                                        msg = LanguageHelper.GetMessage(LanMessList, "Message32", "拆包失败:更新集中数据库数据失败:") + cmd.CommandText; transCenter.Rollback();
                                        transCenter.Rollback();
                                        return false;
                                    }

                                    cmd.CommandText = sql_PackingList;
                                    if (cmd.ExecuteNonQuery() < 1)
                                    {
                                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + sql_PackingList, "ABNORMAL", null);
                                       // msg = "拆包失败:更新集中数据库数据失败:" + cmd.CommandText;
                                        msg = LanguageHelper.GetMessage(LanMessList, "Message32", "拆包失败:更新集中数据库数据失败:") + cmd.CommandText; transCenter.Rollback();
                                        transCenter.Rollback();
                                        return false;
                                    }

                                    cmd.CommandText = sql_Carton;
                                    if (cmd.ExecuteNonQuery() < 1)
                                    {
                                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + sql_Carton, "ABNORMAL", null);
                                        //msg = "拆包失败:更新集中数据库数据失败:" + cmd.CommandText;
                                        msg = LanguageHelper.GetMessage(LanMessList, "Message32", "拆包失败:更新集中数据库数据失败:") + cmd.CommandText; transCenter.Rollback();
                                        transCenter.Rollback();
                                        return false;
                                    }
                                    #endregion
                                }
                                transCenter.Commit();
                                return true;
                            }
                        }
                        catch (Exception ex)
                        {
                            msg = "拆包发生异常：" + ex.Message;
                            return false;
                        }
                        #endregion
                    }
                    else
                    {
                        #region
                        using (SqlTransaction transCenter = connCenter.BeginTransaction())
                        {
                            using (SqlConnection connPacking = new SqlConnection(FormCover.connectionBase))//包装数据库
                            {
                                connPacking.Open();
                                using (SqlTransaction transPacking = connPacking.BeginTransaction())
                                {
                                    try
                                    {
                                        #region 更新包装数据库
                                        using (SqlCommand cmd = new SqlCommand())
                                        {
                                            cmd.Connection = connPacking;
                                            cmd.Transaction = transPacking;
                                            cmd.CommandText = string.Format(@"UPDATE {0}.dbo.Product Set Process='T4',BoxID='',[flag]='0',StdPower='',Pmax='',UnpackOperator='{1}',UnpackDate = getdate(),[Cust_BoxID]='' WHERE BoxID='{2}' and [flag]='1'", FormCover.PackingDBName.Trim(), FormCover.CurrUserWorkID, cartonNo);
                                            if (cmd.ExecuteNonQuery() < 1)
                                            {
                                                  ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + cmd.CommandText, "ABNORMAL", null);
                                              //  msg = "拆包失败:原因更新包装数据库数据失败:" + cmd.CommandText;
                                                msg = LanguageHelper.GetMessage(LanMessList, "Message30", "拆包失败:原因更新包装数据库数据失败:") + cmd.CommandText;
                                                transPacking.Rollback();
                                                return false;
                                            }
                                            cmd.CommandText = string.Format(@"Update {0}.dbo.Box set IsUsed='L',JobNo='',[Number]='0',[BoxType]='',[flag]='0', UnpackOperator='{1}',UnpackDate = getdate() where BoxID='{2}' and flag='1'", FormCover.PackingDBName.Trim(), FormCover.CurrUserWorkID, cartonNo);
                                            if (cmd.ExecuteNonQuery() < 1)
                                            {
                                                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + cmd.CommandText, "ABNORMAL", null);
                                               // msg = "拆包失败:原因更新包装数据库数据失败:" + cmd.CommandText;
                                                msg = LanguageHelper.GetMessage(LanMessList, "Message30", "拆包失败:原因更新包装数据库数据失败:") + cmd.CommandText;
                                                transPacking.Rollback();
                                                return false;
                                            }
                                        }
                                        #endregion

                                        #region 更新集中数据库数据
                                        using (SqlCommand cmd = new SqlCommand())
                                        {
                                            cmd.Connection = connCenter;
                                            cmd.Transaction = transCenter;

                                            try
                                            {
                                                DataSet ds = DbHelperSQL.Query(DbHelperSQL.sqlConn, sql_Carton_count, null);
                                                if (ds != null)
                                                {
                                                    if (ds.Tables[0].Rows.Count > 1)
                                                    {
                                                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + sql_Carton_count, "ABNORMAL", null);
                                                       // msg = "拆包失败:原因更新集中数据库数据失败:" + sql_Carton_count;
                                                        msg = LanguageHelper.GetMessage(LanMessList, "Message32", "拆包失败:更新集中数据库数据失败:") + sql_Carton_count;
                                                        transPacking.Rollback();
                                                        transCenter.Rollback();
                                                        return false;
                                                    }
                                                }
                                                else
                                                {
                                                     ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + sql_Carton_count, "ABNORMAL", null);
                                                   // msg = "拆包失败:原因更新集中数据库数据失败:" + sql_Carton_count;
                                                    msg = LanguageHelper.GetMessage(LanMessList, "Message32", "拆包失败:更新集中数据库数据失败:") + sql_Carton_count;
                                                    transPacking.Rollback();
                                                    transCenter.Rollback();
                                                    return false;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + ex.Message, "ABNORMAL", null);
                                                //msg = "拆包失败:原因更新集中数据库数据失败:" + ex;
                                                msg = LanguageHelper.GetMessage(LanMessList, "Message32", "拆包失败:更新集中数据库数据失败:") + ex;
                                                transPacking.Rollback();
                                                transCenter.Rollback();
                                                return false;
                                            }

                                            cmd.CommandText = sql_rework;
                                            if (cmd.ExecuteNonQuery() < 0)
                                            {
                                                msg = LanguageHelper.GetMessage(LanMessList, "Message32", "拆包失败:更新集中数据库数据失败:") + cmd.CommandText;
                                                transCenter.Rollback();
                                                return false;
                                            }

                                            cmd.CommandText = sql_TestDat;
                                            if (cmd.ExecuteNonQuery() < 1)
                                            {
                                                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + sql_TestDat, "ABNORMAL", null);
                                                //msg = "拆包失败:原因更新集中数据库数据失败:" + sql_TestDat;
                                                msg = LanguageHelper.GetMessage(LanMessList, "Message32", "拆包失败:更新集中数据库数据失败:") + sql_TestDat;
                                                transPacking.Rollback();
                                                transCenter.Rollback();
                                                return false;
                                            }

                                            cmd.CommandText = sql_Module;
                                            if (cmd.ExecuteNonQuery() < 1)
                                            {
                                                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + sql_Module, "ABNORMAL", null);
                                                //msg = "拆包失败:原因更新集中数据库数据失败:" + sql_Module;
                                                msg = LanguageHelper.GetMessage(LanMessList, "Message32", "拆包失败:更新集中数据库数据失败:") + sql_Module;
                                                transPacking.Rollback();
                                                transCenter.Rollback();
                                                return false;
                                            }

                                            cmd.CommandText = sql_PackingList;
                                            if (cmd.ExecuteNonQuery() < 1)
                                            {
                                               ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + sql_PackingList, "ABNORMAL", null);
                                                //msg = "拆包失败:原因更新集中数据库数据失败:" + sql_PackingList;
                                                msg = LanguageHelper.GetMessage(LanMessList, "Message32", "拆包失败:更新集中数据库数据失败:") + sql_PackingList;
                                                transPacking.Rollback();
                                                transCenter.Rollback();
                                                return false;
                                            }

                                            cmd.CommandText = sql_Carton;
                                            if (cmd.ExecuteNonQuery() < 1)
                                            {
                                                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + sql_Carton, "ABNORMAL", null);
                                               // msg = "拆包失败:原因更新集中数据库数据失败:" + sql_Carton;
                                                msg = LanguageHelper.GetMessage(LanMessList, "Message32", "拆包失败:更新集中数据库数据失败:") + sql_Carton;
                                                transPacking.Rollback();
                                                transCenter.Rollback();
                                                return false;
                                            }
                                        }
                                        #endregion

                                        transPacking.Commit();
                                        transCenter.Commit();
                                        return true;
                                    }
                                    catch (Exception ex)
                                    {
                                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + ex.Message, "ABNORMAL", null);
                                        msg = ex.Message;
                                        transPacking.Rollback();
                                        transCenter.Rollback();
                                        return false;
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                msg = "打包发生异常：" + ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 拼托打包，删除集中数据库，并更新包装数据库数据
        /// </summary>
        /// <param name="cartonNo">箱号</param>
        public bool ReworkUnPacking(string cartonNo, out string msg)
        {
            try
            {
                msg = "";
                #region
                string sql_rework = @"UPDATE {0}.dbo.[T_MODULE_REWORK] SET FLAG='0' WHERE 
                                      MODULE_SN IN (SELECT A.MODULE_SN FROM {0}.dbo.[T_MODULE] A with(nolock) ,{0}.dbo.T_MODULE_CARTON B WITH(NOLOCK),{0}.dbo.T_MODULE_PACKING_LIST C WITH(NOLOCK)
                                      WHERE B.SYSID = C.MODULE_CARTON_SYSID AND A.SYSID = C.MODULE_SYSID 
                                      AND B.RESV03='{1}')";
                sql_rework = string.Format(sql_rework, FormCover.CenterDBName.Trim(), cartonNo.Trim());

                string sql_packing = @"UPDATE {0}.[dbo].[Product] SET Flag='6',BoxID='',UnpackDate= GETDATE(),UnpackOperator='{1}'
                                      WHERE BoxID='{2}'
                                      UPDATE {0}.[dbo].Box SET Flag='6',IsUsed='L',UnpackDate = GETDATE(),UnpackOperator='{1}'
                                      WHERE BoxID='{2}'";
                sql_packing = string.Format(sql_packing, FormCover.PackingDBName, FormCover.currUserName, cartonNo);

                string sqlCenter = @"DELETE
                                     FROM {0}.[dbo].T_MODULE_PACKING_LIST
                                     FROM {0}.[dbo].T_MODULE_PACKING_LIST INNER JOIN {0}.[dbo].T_MODULE_CARTON MC ON MODULE_CARTON_SYSID=MC.SYSID
                                     WHERE MC.RESV03 = '{1}';
                                     DELETE FROM {0}.[dbo].T_MODULE_CARTON
                                     WHERE RESV03 = '{1}' ";
                sqlCenter = string.Format(sqlCenter, FormCover.CenterDBName, cartonNo);
                #endregion

                #region
                using (SqlConnection connCenter = new SqlConnection(DbHelperSQL.sqlConn))//集中数据库 
                {
                    connCenter.Open();
                    if (ProductStorageDAL.GetSysMapping("PackingSystemSameFlag", FormCover.CurrentFactory).Trim().ToUpper().Equals("Y"))
                    {
                        #region
                        using (SqlTransaction transCenter = connCenter.BeginTransaction())
                        {
                            try
                            {
                                if (SqlHelper.ExecuteNonQuery(connCenter, transCenter, CommandType.Text, sql_packing, null) < 0)
                                {
                                    transCenter.Rollback();                                  
                                    msg = LanguageHelper.GetMessage(LanMessList, "Message36", "更新包装数据库失败：") + sql_packing + "";
                                    return false;
                                }

                                if (SqlHelper.ExecuteNonQuery(connCenter, transCenter, CommandType.Text, sql_rework, null) < 0)
                                {
                                    transCenter.Rollback();
                                    msg =  LanguageHelper.GetMessage(LanMessList, "Message33", "更新集中数据库失败（重工)") + sql_rework;
                                    return false;
                                }

                                if (SqlHelper.ExecuteNonQuery(connCenter, transCenter, CommandType.Text, sqlCenter, null) < 0)
                                {
                                    transCenter.Rollback();
                                    msg =LanguageHelper.GetMessage(LanMessList, "Message32", "拆包失败:更新集中数据库数据失败:")+ sqlCenter + "";
                                    return false;
                                }
                                transCenter.Commit();
                            }
                            catch (Exception ex)
                            {
                                msg = LanguageHelper.GetMessage(LanMessList, "Message34", "包装发生异常")+ ex.Message;
                                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + ex.Message, "ABNORMAL", null);
                                return false;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region
                        using (SqlTransaction transCenter = connCenter.BeginTransaction())
                        {
                            using (SqlConnection connPacking = new SqlConnection(FormCover.connectionBase))//包装数据库
                            {
                                connPacking.Open();
                                using (SqlTransaction transPacking = connPacking.BeginTransaction())
                                {
                                    try
                                    {
                                        if (SqlHelper.ExecuteNonQuery(connCenter, transCenter, CommandType.Text, sql_rework, null) < 0)
                                        {
                                            transCenter.Rollback();
                                            msg = LanguageHelper.GetMessage(LanMessList, "Message33", "更新集中数据库失败（重工)") + sql_rework;
                                            return false;
                                        }

                                        if (SqlHelper.ExecuteNonQuery(connCenter, transCenter, CommandType.Text, sqlCenter, null) < 1)
                                        {
                                            msg = LanguageHelper.GetMessage(LanMessList, "Message35", "更新集中数据库失败:") + sqlCenter + "";
                                            transCenter.Rollback();
                                            return false;
                                        }

                                        if (SqlHelper.ExecuteNonQuery(connPacking, transPacking, CommandType.Text, sql_packing, null) < 1)
                                        {
                                            msg = LanguageHelper.GetMessage(LanMessList, "Message36", "更新包装数据库失败:") + sql_packing + "";
                                            transCenter.Rollback();
                                            transPacking.Rollback();
                                            return false;
                                        }
                                        transCenter.Commit();
                                        transPacking.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                      	msg = LanguageHelper.GetMessage(LanMessList, "Message34", "包装发生异常") + ex.Message;
                                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败") + ex.Message, "ABNORMAL", null);
                                        return false;
                                    }
                                }
                            }
                        }
                        #endregion
                    }

                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                msg = LanguageHelper.GetMessage(LanMessList, "Message34", "包装发生异常") + ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 删除集中数据库，并更新包装数据库数据
        /// </summary>
        /// <param name="moduleSn">组件号</param>
        /// <param name="cartonNo">箱号</param>
        /// <param name="InterId">组件包装数据库内部ID</param>
        /// <returns></returns>
        public bool UnPacking(string moduleSn, Int64 InterId, string cartonNo)
        {
            try
            {
                #region
                string sql_TestDat = @"DELETE FROM T_MODULE_TEST WHERE MODULE_SN='{0}' ";

                string sql_Module = @"DELETE FROM T_MODULE WHERE MODULE_SN='{0}'";

                string sql_PackingList = @"DELETE FROM T_Module_Packing_LIST
                                         WHERE MODULE_SYSID IN
                                        (
	                                        SELECT T_Module.SYSID
	                                        FROM T_Module_Packing_LIST with(nolock)
		                                         INNER JOIN T_MODULE with(nolock) ON T_Module_Packing_LIST.MODULE_SYSID = T_MODULE.SYSID
	                                        WHERE T_MODULE.MODULE_SN ='{0}'
                                        )";

                string sql_Carton = @"DELETE FROM T_MODULE_CARTON WHERE CARTON_NO='{0}' ";

                string sql_Carton_Exist = @"SELECT T_Module_Packing_LIST.MODULE_SYSID
	                                            FROM T_Module_Packing_LIST with(nolock) INNER JOIN T_MODULE_CARTON with(nolock)
                                                    ON T_Module_Packing_LIST.MODULE_CARTON_SYSID=T_MODULE_CARTON.SYSID
	                                            WHERE T_MODULE_CARTON.CARTON_NO='{0}'";

                string sql_packing_product = @"UPDATE [Product] SET [BoxID]='',[Process]='T4',[CTMGrade]='W' WHERE [InterID]='{0}' AND [SN]='{1}'";

                sql_TestDat = string.Format(sql_TestDat, moduleSn);
                sql_Module = string.Format(sql_Module, moduleSn);
                sql_PackingList = string.Format(sql_PackingList, moduleSn);
                sql_Carton = string.Format(sql_Carton, cartonNo);
                sql_Carton_Exist = string.Format(sql_Carton_Exist, cartonNo);
                sql_packing_product = string.Format(sql_packing_product, InterId, moduleSn);
                #endregion

                #region
                using (SqlConnection connCenter = new SqlConnection(DbHelperSQL.sqlConn))//集中数据库
                {
                    connCenter.Open();

                    using (SqlTransaction transCenter = connCenter.BeginTransaction())
                    {
                        using (SqlConnection connPacking = new SqlConnection(FormCover.connectionBase))//包装数据库
                        {
                            connPacking.Open();

                            using (SqlTransaction transPacking = connPacking.BeginTransaction())
                            {
                                try
                                {
                                    #region 更新包装数据库
                                    using (SqlCommand cmd = new SqlCommand())
                                    {
                                        cmd.Connection = connPacking;
                                        cmd.Transaction = transPacking;

                                        cmd.CommandText = sql_packing_product;
                                        if (cmd.ExecuteNonQuery() < 1)
                                        {
                                            transPacking.Rollback();
                                            return false;
                                        }

                                        cmd.CommandText = string.Format(@"INSERT INTO [Process] ([SN],[OperatorID],[ProcessDate],[Process],[Tester],[Pass],[UPReason]) 
                                                                       VALUES ('{0}','{1}',getdate(),'T5','',1,'')", moduleSn, FormCover.CurrUserWorkID);
                                        if (cmd.ExecuteNonQuery() < 1)
                                        {
                                            transPacking.Rollback();
                                            return false;
                                        }
                                    }
                                    #endregion

                                    #region 更新集中数据库数据
                                    using (SqlCommand cmd = new SqlCommand())
                                    {
                                        cmd.Connection = connCenter;
                                        cmd.Transaction = transCenter;

                                        cmd.CommandText = sql_PackingList;
                                        if (cmd.ExecuteNonQuery() < 1)
                                        {
                                            transCenter.Rollback();
                                            return false;
                                        }

                                        cmd.CommandText = sql_TestDat;
                                        if (cmd.ExecuteNonQuery() < 1)
                                        {
                                            transCenter.Rollback();
                                            return false;
                                        }

                                        cmd.CommandText = sql_Module;
                                        if (cmd.ExecuteNonQuery() < 1)
                                        {
                                            transCenter.Rollback();
                                            return false;
                                        }


                                        cmd.CommandText = sql_Carton_Exist;
                                        object Carton_Exist = cmd.ExecuteScalar();
                                        if (Carton_Exist == null || string.IsNullOrEmpty(Carton_Exist.ToString()))
                                        {
                                            cmd.CommandText = sql_Carton;
                                            if (cmd.ExecuteNonQuery() < 1)
                                            {
                                                transCenter.Rollback();
                                                return false;
                                            }
                                        }
                                    }
                                    #endregion

                                    transPacking.Commit();
                                    transCenter.Commit();
                                    return true;
                                }
                                catch
                                {
                                    transPacking.Rollback();
                                    transCenter.Rollback();
                                    return false;
                                }
                            }
                        }
                    }
                }
                #endregion

            }
            catch
            {
                return false;
            }
        }
        #endregion


        //构造函数
        public FormBoxHandling(string sn = "", FormPacking fm = null, bool Par2 = false, bool md = false, bool tp = false, bool flag = false, string frmName = "")
        {
            InitializeComponent();
            this.snKick = sn;
            this.textBox1.Text = sn;

            this.hasPar2 = Par2;
            this.fp = fm;
            this.isModel = md;
            this.isTrueP = tp;

            if (flag)
            {
                btnUnpack.Enabled = false;
                btnUnpack.Visible = false;
                btnReplace.Enabled = true;
                btnReplace.Visible = true;
                btnUnpackRecord.Visible = false;
            }
            else
            {
                btnReplace.Enabled = false;
                btnReplace.Visible = false;
                btnUnpack.Enabled = true;
                btnUnpack.Visible = true;
            }
            this.Text = frmName;
            tbBoxHandingQuerySQL.Name = "tbBoxHandingQuerySQL";
            tbBoxHandingQuerySQL.Text = ToolsClass.getSQL(tbBoxHandingQuerySQL.Name);
        }

        //构造函数
        public FormBoxHandling(string reworkflag, string sn = "", FormPackingTogether fm = null, bool Par2 = false, bool md = false, bool tp = false, bool flag = false, string frmName = "")
        {
            InitializeComponent();
            this.snKick = sn;
            this.textBox1.Text = sn;

            this.hasPar2 = Par2;
            this.fp1 = fm;
            this.isModel = md;
            this.isTrueP = tp;

            ReworkFlag = reworkflag;

            if (flag)
            {
                btnUnpack.Enabled = false;
                btnUnpack.Visible = false;
                btnReplace.Enabled = true;
                btnReplace.Visible = true;
                btnUnpackRecord.Visible = false;
            }
            else
            {
                btnReplace.Enabled = false;
                btnReplace.Visible = false;
                btnUnpack.Enabled = true;
                btnUnpack.Visible = true;
            }
            this.Text = frmName;
            tbBoxHandingQuerySQL.Name = "tbBoxReworkSQL";
            tbBoxHandingQuerySQL.Text = ToolsClass.getSQL(tbBoxHandingQuerySQL.Name);
        }

        #region 页面事件
        /// <summary>
        /// 查询事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btQuery_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim().Length < 8)
            {
                FormMain.showTipMSG(LanguageHelper.GetMessage(LanMessList, "Message10", "输入的箱号长度不正确!"), this.textBox1, "Prompting Message", ToolTipIcon.Warning);
                this.textBox1.SelectAll();
                this.textBox1.Focus();
                return;
            }

            //清除当前显示内容
            if (this.dataGridView1.Rows.Count > 0)
                dataGridView1.Rows.Clear();

            DataTable dt = ProductStorageDAL.GetCancelCartonInfo(this.textBox1.Text.Trim().ToString());
            if (dt != null)
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message37", "此托已经入库或者没有打包!"));
                this.textBox1.SelectAll();
                this.textBox1.Focus();
                return;
            }

            if (!ReworkFlag.Equals("Y"))
            {
                DataTable dtm = ProductStorageDAL.GetDissemblyCartonInfo(this.textBox1.Text.Trim().ToString());
                if (dtm != null && dtm.Rows.Count > 1)
                {
                    MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message38", "此托是拼托打包"));
                    this.textBox1.SelectAll();
                    this.textBox1.Focus();
                    return;
                }
            }
            //显示数据
            GetDataReader(string.Format(tbBoxHandingQuerySQL.Text, textBox1.Text));

            if (this.dataGridView1.Rows.Count < 1)
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message39", "此箱号没有打包完成"));
                this.textBox1.SelectAll();
                this.textBox1.Focus();
                return;
            }
        }

        /// <summary>
        /// 拆箱事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUnpack_Click(object sender, EventArgs e)
        {
            string sboxid = textBox1.Text.Trim();
            //string sboxid = textBox1.Text.ToString();
            string msg = "";

            btQuery_Click(null, null);

            if (string.IsNullOrEmpty(sboxid))
            {
                FormMain.showTipMSG(LanguageHelper.GetMessage(LanMessList, "Message12", "箱号不能为空，请输入!"), this.textBox1, "Prompting Message", ToolTipIcon.Warning);
                this.textBox1.SelectAll();
                this.textBox1.Focus();
                return;
            }

            if (this.dataGridView1.Rows.Count > 0)
            {
                string cartonnum = this.dataGridView1.Rows[0].Cells[8].Value.ToString();
                if (string.IsNullOrEmpty(cartonnum))
                {
                   FormMain.showTipMSG(LanguageHelper.GetMessage(LanMessList, "Message13", "查询到的组件，箱号有为空的!"), this.label1, "Prompting Message", ToolTipIcon.Warning);
                }
                else
                {
                    if (!sboxid.Equals(cartonnum.Trim()))
                    {
                        FormMain.showTipMSG(LanguageHelper.GetMessage(LanMessList, "Message14", "查询的组件清单中有多个不同的箱号!"), this.label1, "Prompting Message", ToolTipIcon.Warning);
                        return;
                    }
                }
            }
            else
            {
                FormMain.showTipMSG(LanguageHelper.GetMessage(LanMessList, "Message16", "没有查询到此箱号对应的数据!"), this.label1, "Prompting Message", ToolTipIcon.Warning);
                this.textBox1.SelectAll();
                this.textBox1.Focus();
                return;
            }

            if (MessageBox.Show(string.Format(LanguageHelper.GetMessage(LanMessList, "Message17", "你要去拆箱吗，箱号:{0}？"), sboxid), "Prompting Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    //flag = Y 表示拼托打包，N表示组件打包
                    string flag="N";
                    DataTable dt = ProductStorageDAL.GetLocalFlag(sboxid);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        if (dt.Rows.Count == 2)
                            flag = "Y";
                        else if (dt.Rows.Count == 1)
                        {
                            DataRow row = dt.Rows[0];
                            if (Convert.ToString(row["LocalFlag"]).Trim().Equals("1"))
                                flag = "Y";
                        }
                    }
                    if (!ReworkFlag.Equals("Y"))
                    {
                        if (flag.Equals("Y"))
                        {
                            MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message40", "此托为拼托打包，请去拼托打包里拆箱") + sboxid);
                            return;
                        }

                        if (!UnPacking(sboxid, out msg))
                        {
                           MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message1", "拆包失败!") + msg);
                            return;
                        }
                        else
                        {
                            MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message19", "拆包成功!"));
                            if (this.dataGridView1.Rows.Count > 0)
                                dataGridView1.Rows.Clear();
                            this.textBox1.Text = "";
                            this.textBox1.Focus();
                        }
                    }
                    else //并托拆包
                    {
                        if (flag.Equals("N"))
                        {
                            MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message41", "此托为组件打包，请去组件打包里拆箱:") + sboxid);
                            return;
                        }

                        if (!ReworkUnPacking(sboxid, out msg))
                        {
                            MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message42", "拆包失败请重试！") + msg);
                            return;
                        }
                        else
                        {
                          	MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message19", "拆包成功!"));//, "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            if (this.dataGridView1.Rows.Count > 0)
                                dataGridView1.Rows.Clear();
                            this.textBox1.Text = "";
                            this.textBox1.Focus();
                        }
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message20", "拆包发生异常") + exc.Message);//, "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ToolsClass.Log(exc.Message, "ABNORMAL", null);
                }
            }
        }
        /// <summary>
        /// 拆包信息查询事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUnpackRecord_Click(object sender, EventArgs e)
        {
            new FormBoxUnpackRecord().ShowDialog();
        }

        /// <summary>
        /// 单击页面箱号，显示出sql
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label14_DoubleClick(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = !splitContainer1.Panel2Collapsed;
        }

        /// <summary>
        /// 保存SQL到XML
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btSaveSQL_Click(object sender, EventArgs e)
        {
            //保存组件装箱报表sql语句 tbCPTBoxSQL
            ToolsClass.saveSQL(tbBoxHandingQuerySQL);
            splitContainer1.Panel2Collapsed = true;
        }

        /// <summary>
        /// 取消sql保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btCancelSQL_Click(object sender, EventArgs e)
        {
            tbBoxHandingQuerySQL.Undo();
        }
        //更换组件
        private void btnReplace_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.textBox1.Text.Trim()))
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message21", "箱号不能为空"));//, "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.textBox1.Clear();
                this.textBox1.Focus();
                return;
            }
            if (this.dataGridView1.Rows.Count > 0)
            {
                #region
                FormReason fr = new FormReason();
                fr.request(LanguageHelper.GetMessage(LanMessList, "Message22", "请输入要替换的组件序列号"), LanguageHelper.GetMessage(LanMessList, "Message23", "替换组件"));
                if (!fr.exit)
                {
                    #region
                    if (fr.DialogResult == System.Windows.Forms.DialogResult.OK)
                    {
                        string oldsn = this.dataGridView1.CurrentRow.Cells[0].Value.ToString().Trim();
                        Int64 oldInterID = Convert.ToInt64(this.dataGridView1.CurrentRow.Cells[9].Value.ToString().Trim());
                        string carton = this.dataGridView1.CurrentRow.Cells[8].Value.ToString().Trim();

                        if (!string.IsNullOrEmpty(oldsn) && oldInterID > 1 && (!string.IsNullOrEmpty(carton)))
                        {
                            if (MessageBox.Show(string.Format(LanguageHelper.GetMessage(LanMessList, "Message24", "确定将箱子:{0}中的组件{1}替换为:{2}?"), textBox1.Text, oldsn, fr.reason), "Prompting Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                            {
                                #region
                                if (UnPacking(oldsn, oldInterID, carton))
                                {
                                    if (MessageBox.Show("information", LanguageHelper.GetMessage(LanMessList, "Message25", "更换成功,要重新打印标签吗"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    {
                                        if (new FormRePrint(fp, this.hasPar2, this.isModel, this.isTrueP, fr.reason.ToUpper()).ShowDialog() != System.Windows.Forms.DialogResult.OK)
                                            return;
                                    }
                                }
                                else
                                {
                                    MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message26", "更新失败!"));
                                    return;
                                }
                                #endregion
                            }
                        }
                    }
                    #endregion
                }
                fr.Dispose();
                #endregion
            }
            else
            {
                MessageBox.Show(LanguageHelper.GetMessage(LanMessList, "Message27", "没有要被更换的组件!"));
                this.textBox1.Clear();
                this.textBox1.Focus();
                return;
            }
        }
        #endregion

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                btQuery_Click(null, null);
            }
        }

        private void FormBoxHandling_Load(object sender, EventArgs e)
        {
            #  region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
