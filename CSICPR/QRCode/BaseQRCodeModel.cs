﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSICPR.QRCode
{
    [Serializable]
    public class BaseQRCodeModel
    {
        /// <summary>
        /// 
        /// </summary>
        public Int64 InterID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String SupplierCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String BSII { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Revision { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String BrandName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String SideBarcodeName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String CreateOn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String CreateBy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv01 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv02 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv03 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv04 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv05 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv06 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Resv07 { get; set; }
       
        //构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        public BaseQRCodeModel()
        {
            SupplierCode = string.Empty;
            BSII = string.Empty;
            Revision = string.Empty;
            BrandName = string.Empty;
            SideBarcodeName = string.Empty;
            CreateOn = string.Empty;
            CreateBy = string.Empty;
            Resv01 = string.Empty;
            Resv02 = string.Empty;
            Resv03 = string.Empty;
            Resv04 = string.Empty;
            Resv05 = string.Empty;
            Resv06 = string.Empty;
            Resv07 = string.Empty;
          
        }
    }
}
