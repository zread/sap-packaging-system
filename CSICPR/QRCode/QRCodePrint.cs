﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Seagull.BarTender.Print;
using System.IO;


namespace CSICPR.QRCode
{
    public partial class QRCodePrint : Form
    {
        public QRCodePrint()
        {
            InitializeComponent();
        }

        #region 共有变量
        private static QRCodePrint _theSingleton;
        public static void Instance(Form fm)
        {
            if (null == _theSingleton || _theSingleton.IsDisposed)
            {
                _theSingleton = new QRCodePrint
                {
                    MdiParent = fm,
                    WindowState = FormWindowState.Maximized
                };
                _theSingleton.Show();
            }
            else
            {
                _theSingleton.Activate();
                if (_theSingleton.WindowState == FormWindowState.Minimized)
                    _theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        #region 参数

        private string strMsg = "";//信息记录
        private string strQRCode = "";//二维码前三项信息
        private Engine _engine;//打印Engine对象
        private bool isPrint;//是否可以打印
        private string BrandTemplateName = "";//商标模板名称
        private string SiderBarcodeName = "";//侧边框模板名称
        private string SoftwareAddress;//软件安装地址
        private string PrintCount;//打印张数
        private string strModel;
        private string strPMax;
        private string strVmp;
        private string strImp;
        private string strVoc;
        private string strIsc;
        private string strBrandPrintName;
        private string strSidePrintName;

        #endregion

        #region 初始化
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void QRCodePrint_Load(object sender, EventArgs e)
        {
            loadParameters();
        }
        private void QRCodePrint_Shown(object sender, EventArgs e)
        {
            if (checkEngine())
            {
                isPrint = true;
                ToolsClass.Log("Bartender软件已启动，可进行打印", "NORMAL", lstView);
            }
            else
            {
                isPrint = false;
                ToolsClass.Log(strMsg, "ABNORMAL", lstView);
            }
        }
        #endregion

        #region Bartender软件检查
        /// <summary>
        /// Bartender软件检查
        /// </summary>
        /// <returns></returns>
        private bool checkEngine()
        {
            if (!File.Exists(SoftwareAddress))
            {
                strMsg = "Bartender软件未安装，请安装后进行打印";
                return false;
            }
            //打开引擎
            var myProcesses = System.Diagnostics.Process.GetProcessesByName("bartend");
            foreach (var myProcess in myProcesses)
            {
                myProcess.Kill();
            }
            _engine = new Engine(true);
            return true;
        }
        #endregion

        #region 文本框触发事件
        /// <summary>
        /// 文本框触发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSerialNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                this.btnSave_Click(null, null);
            //txtSerialNumber.SelectAll();
        }
        #endregion

        #region 打印核心事件
        /// <summary>
        /// 打印核心事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            if(!isPrint)
            {
                ToolsClass.Log("Bartender软件未安装，请安装后进行打印", "ABNORMAL", lstView);
                return;
            }
            if(!checkCanPrint())
            {
                ToolsClass.Log(strMsg, "ABNORMAL", lstView);
                return;
            }

            try
            {
                string strModuleSN = "";
                var Labellabelint = int.Parse(PrintCount);//打印的张数
                
                //商标打印
                if (checkBrand.Checked)
                {
                    var dictLabel = new Dictionary<string, string>();
                    var listBrandPrint = new List<Dictionary<string, string>>();
                    strModuleSN = strQRCode + txtSerialNumber.Text.Trim();
                    DirectInfo(ref  dictLabel, strModuleSN);
                    for (int i = 0; i < Labellabelint; i++)
                    {
                        listBrandPrint.Add(dictLabel);
                    }

                    if (Print(strModuleSN, BrandTemplateName, listBrandPrint, strBrandPrintName))
                    {
                        ToolsClass.Log(strMsg, "NORMAL", lstView);
                    }
                    else
                    {
                        ToolsClass.Log(strMsg, "ABNORMAL", lstView);
                    }


                }

                if (checkSideBarcode.Checked)
                {
                    var dictLabel = new Dictionary<string, string>();
                    var listSidePrint = new List<Dictionary<string, string>>();
                    strModuleSN = txtSerialNumber.Text.Trim();
                    DirectInfo(ref  dictLabel, strModuleSN);
                    for (int i = 0; i < Labellabelint; i++)
                    {
                        listSidePrint.Add(dictLabel);
                    }

                    if (Print(strModuleSN, SiderBarcodeName, listSidePrint, strSidePrintName))
                    {

                        ToolsClass.Log(strMsg, "NORMAL", lstView);
                    }
                    else
                    {
                        ToolsClass.Log(strMsg, "ABNORMAL", lstView);
                    }

                }

                txtSerialNumber.SelectAll();
                

            }
            catch (Exception ex)
            {
                ToolsClass.Log("打印异常："+ex.Message, "ABNORMAL", lstView);
                return;
            }
        }
        #endregion

        #region 调用打印
        private bool Print(string strModuleSN, string TemplateName, List<Dictionary<string, string>> listBrandPrint, string PrintName)
        {
            string msg = "";
            var BrandfileFullPath = Application.StartupPath + "\\Label" + "\\" + TemplateName;
            if (!BarTenderPrint.Print(_engine, TemplateName, BrandfileFullPath, listBrandPrint, SoftwareAddress, PrintName, out msg))
            {
                strMsg = msg;
                return false;
            }
            else
            {
                strMsg = "打印成功";
                return true;
            }
        }
        #endregion

        #region 打印前准备
        /// <summary>
        /// 打印前准备
        /// </summary>
        /// <returns></returns>
        private bool checkCanPrint()
        {
            if (!checkSideBarcode.Checked && !checkBrand.Checked)
            {
                strMsg = "请选择要打印的标签";
                return false;
            }

            if (txtSerialNumber.Text.Trim() == "")
            {
                strMsg = "条码不能为空!";
                return false;
            }


            var baseQRCodeModelNow = BaseQRCodeDAL.QueryBaseQRCodeModel("T", FormCover.InterfaceConnString);
            if (baseQRCodeModelNow == null)
            {
                strMsg = "未维护当前二维码规则，请维护后进行打印";
                return false;
            }
            strQRCode = baseQRCodeModelNow.SupplierCode + "-" + baseQRCodeModelNow.BSII + "-" + baseQRCodeModelNow.Revision + "-";
            BrandTemplateName = baseQRCodeModelNow.BrandName;
            SiderBarcodeName = baseQRCodeModelNow.SideBarcodeName;

            return true;
        }

        #endregion

        #region 商标数据
        /// <summary>
        /// 商标数据
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="ModuleSn"></param>
        private void DirectInfo(ref Dictionary<string, string> dict,string ModuleSn)
        {
            dict.Add("Model", strModel);
            dict.Add("Pmax", strPMax);
            dict.Add("Vmp", strVmp);
            dict.Add("Imp", strImp);
            dict.Add("Voc", strVoc);
            dict.Add("Isc", strIsc);
            dict.Add("SerialNum", ModuleSn);
            dict.Add("DateType", getDateType());
        }
        #endregion

        #region 侧边框数据
        /// <summary>
        /// 侧边框数据
        /// </summary>
        /// <param name="dict"></param>
        /// <param name="ModuleSn"></param>
        private void DirectInfoSide(ref Dictionary<string, string> dict, string ModuleSn)
        {
            dict.Add("ModuleSN", ModuleSn);
        }
        #endregion 

        #region 获取模板时间样式Q3-15
        private string getDateType()
        {
            string DateType = "";
            string strMonth = DateTime.Now.ToString("MM");
            
            switch (strMonth)
            {
                case "01":
                    DateType = "Q1";
                    break;
                case "02":
                    DateType = "Q1";
                    break;
                case "03":
                    DateType = "Q1";
                    break;
                case "04":
                    DateType = "Q2";
                    break;
                case "05":
                    DateType = "Q2";
                    break;
                case "06":
                    DateType = "Q2";
                    break;
                case "07":
                    DateType = "Q3";
                    break;
                case "08":
                    DateType = "Q3";
                    break;
                case "09":
                    DateType = "Q3";
                    break;
                case "10":
                    DateType = "Q4";
                    break;
                case "11":
                    DateType = "Q4";
                    break;
                case "12":
                    DateType = "Q4";
                    break;
            }
            DateType = DateType +"-"+ DateTime.Now.ToString("yy"); ;
            return DateType;
        }
        #endregion

        #region 加载二维码打印参数
        private void loadParameters()
        {
            try
            {
                this.SoftwareAddress = BaseQRCodeHelper.getConfig("SoftwareAddress");
                this.PrintCount = BaseQRCodeHelper.getConfig("PrintCount");
                this.strModel = BaseQRCodeHelper.getConfig("Model");
                this.strPMax = BaseQRCodeHelper.getConfig("PMax");
                this.strVmp = BaseQRCodeHelper.getConfig("Vmp");
                this.strImp = BaseQRCodeHelper.getConfig("Imp");
                this.strVoc = BaseQRCodeHelper.getConfig("Voc");
                this.strIsc = BaseQRCodeHelper.getConfig("Isc");
                this.strBrandPrintName = BaseQRCodeHelper.getConfig("BrandPrintName");
                this.strSidePrintName = BaseQRCodeHelper.getConfig("SidePrintName");
               
                ToolsClass.Log("二维码参数加载成功!", "NORMAL", lstView);
            }
            catch
            {
                ToolsClass.Log("二维码参数加载失败!", "ABNORMAL", lstView);
            }
        }
        #endregion

        #region 打印参数配置
        private void SetPrintConfig_Click(object sender, EventArgs e)
        {
            var frm = new QRCodeConfig();
            frm.ShowDialog();
            if (frm != null)
            {
                frm.Dispose();
            }
            //bIsConfig = true;
        }
        #endregion 

    }
}
