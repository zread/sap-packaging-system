﻿namespace CSICPR.QRCode
{
    partial class QRCodePrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkSideBarcode = new System.Windows.Forms.CheckBox();
            this.checkBrand = new System.Windows.Forms.CheckBox();
            this.txtSerialNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lstView = new System.Windows.Forms.ListView();
            this.SetPrintConfig = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.SetPrintConfig);
            this.panel1.Controls.Add(this.checkSideBarcode);
            this.panel1.Controls.Add(this.checkBrand);
            this.panel1.Controls.Add(this.txtSerialNumber);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(728, 62);
            this.panel1.TabIndex = 1;
            // 
            // checkSideBarcode
            // 
            this.checkSideBarcode.AutoSize = true;
            this.checkSideBarcode.Location = new System.Drawing.Point(493, 24);
            this.checkSideBarcode.Name = "checkSideBarcode";
            this.checkSideBarcode.Size = new System.Drawing.Size(108, 16);
            this.checkSideBarcode.TabIndex = 55;
            this.checkSideBarcode.Text = "打印侧边框条码";
            this.checkSideBarcode.UseVisualStyleBackColor = true;
            // 
            // checkBrand
            // 
            this.checkBrand.AutoSize = true;
            this.checkBrand.Location = new System.Drawing.Point(399, 25);
            this.checkBrand.Name = "checkBrand";
            this.checkBrand.Size = new System.Drawing.Size(72, 16);
            this.checkBrand.TabIndex = 54;
            this.checkBrand.Text = "打印商标";
            this.checkBrand.UseVisualStyleBackColor = true;
            // 
            // txtSerialNumber
            // 
            this.txtSerialNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSerialNumber.Location = new System.Drawing.Point(78, 20);
            this.txtSerialNumber.MaxLength = 24;
            this.txtSerialNumber.Name = "txtSerialNumber";
            this.txtSerialNumber.Size = new System.Drawing.Size(162, 21);
            this.txtSerialNumber.TabIndex = 53;
            this.txtSerialNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSerialNumber_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "组件条码:";
            // 
            // lstView
            // 
            this.lstView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstView.FullRowSelect = true;
            this.lstView.Location = new System.Drawing.Point(0, 62);
            this.lstView.MultiSelect = false;
            this.lstView.Name = "lstView";
            this.lstView.Size = new System.Drawing.Size(728, 339);
            this.lstView.TabIndex = 5;
            this.lstView.UseCompatibleStateImageBehavior = false;
            this.lstView.View = System.Windows.Forms.View.List;
            // 
            // SetPrintConfig
            // 
            this.SetPrintConfig.Location = new System.Drawing.Point(616, 17);
            this.SetPrintConfig.Name = "SetPrintConfig";
            this.SetPrintConfig.Size = new System.Drawing.Size(88, 23);
            this.SetPrintConfig.TabIndex = 56;
            this.SetPrintConfig.Text = "打印参数配置";
            this.SetPrintConfig.UseVisualStyleBackColor = true;
            this.SetPrintConfig.Click += new System.EventHandler(this.SetPrintConfig_Click);
            // 
            // QRCodePrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(728, 401);
            this.Controls.Add(this.lstView);
            this.Controls.Add(this.panel1);
            this.Name = "QRCodePrint";
            this.Text = "二维码打印";
            this.Load += new System.EventHandler(this.QRCodePrint_Load);
            this.Shown += new System.EventHandler(this.QRCodePrint_Shown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView lstView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSerialNumber;
        private System.Windows.Forms.CheckBox checkSideBarcode;
        private System.Windows.Forms.CheckBox checkBrand;
        private System.Windows.Forms.Button SetPrintConfig;
    }
}