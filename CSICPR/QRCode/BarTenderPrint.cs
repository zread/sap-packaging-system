﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Printing;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using Seagull.BarTender.Print;


namespace CSICPR.QRCode
{

    public class BarTenderPrint
    {


        [DllImport("winspool.drv")]
        private static extern bool SetDefaultPrinter(string name);


        public static bool Print(string templateFileOrgName, string templateFileFullName, List<Dictionary<string, string>> list, string bartenderExePath,
            out string msg)
        {
            return Print(templateFileOrgName, templateFileFullName, list, bartenderExePath, string.Empty, out msg);
        }

        /// <summary>
        /// 命令行方式打印
        /// </summary>
        /// <param name="templateFileOrgName">模板文件名</param>
        /// <param name="templateFileFullName">模板全名</param>
        /// <param name="list"></param>
        /// <param name="bartenderExePath">bartender.exe 全名</param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static bool Print(string templateFileOrgName, string templateFileFullName, List<Dictionary<string, string>> list, string bartenderExePath,
            string localPrinterName, out string msg)
        {
            try
            {
                if (list == null || list.Count <= 0)
                {
                    msg = "打印失败！获取参数数据失败！";
                    return false;
                }
                var index = templateFileFullName.LastIndexOf("\\");
                var path = templateFileFullName.Substring(0, index + 1);
                var paramsDataPath = path + templateFileOrgName.Substring(0, templateFileOrgName.Length - 4) + ".txt";
                var fileSt = new FileStream(paramsDataPath, FileMode.OpenOrCreate);
                var wr = new StreamWriter(fileSt, Encoding.Default);
                fileSt.SetLength(0);
                var sb1 = new StringBuilder();
                var titleDict = list[0];
                foreach (var item in titleDict)
                {
                    sb1.Append(item.Key);
                    sb1.Append(",");
                }
                var sb2 = new StringBuilder();
                foreach (var dict in list)
                {
                    foreach (var item in dict)
                    {
                        sb2.Append(item.Value);
                        sb2.Append(",");
                    }
                    sb2.AppendLine();
                }
                wr.WriteLine(sb1);
                wr.WriteLine(sb2);

                wr.Close();
                fileSt.Close();

                //实例一个process类
                var process = new Process();
                if (!File.Exists(bartenderExePath))
                {
                    msg = "打印失败！请安装Bartender软件！";
                    return false;
                }
                //设定程序名
                process.StartInfo.FileName = bartenderExePath;
                process.StartInfo.Arguments = "/F=\"" + templateFileFullName + "\"  /P /X";

                //关闭Shell的使用
                process.StartInfo.UseShellExecute = false;
                //重新定向标准输入，输入，错误输出
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                //设置cmd窗口不显示
                process.StartInfo.CreateNoWindow = true;

                //设置默认打印机
                if (!string.IsNullOrEmpty(localPrinterName))
                {
                    var currentDefaultPrinter = new PrintDocument().PrinterSettings.PrinterName;
                    if (!SetDefaultPrinter(localPrinterName))
                    {
                        throw new Exception(string.Format("将{0}设置为默认打印机失败，当前默认打印机为{1}", localPrinterName,
                                                          currentDefaultPrinter));
                    }
                }

                //开始
                process.Start();
                process.WaitForExit();

                process.Close();
            }
            catch (Exception ex)
            {
                msg = "BarTender打印异常！" + ex.Message;
                return false;
            }

            msg = "打印成功！";
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="engine">引擎</param>
        /// <param name="templateFileOrgName">模板名</param>
        /// <param name="templateFileFullName">模板全名</param>
        /// <param name="list">打印内容</param>
        /// <param name="bartenderExePath">bartender.exe全名</param>
        /// <param name="localPrinterName">打印机名称</param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static bool Print(Engine engine, string templateFileOrgName, string templateFileFullName, List<Dictionary<string, string>> list, string bartenderExePath,
            string localPrinterName, out string msg)
        {
            try
            {
                if (string.IsNullOrEmpty(localPrinterName))
                {
                    msg = "未找到打印机名称！参数localPrinterName为空";
                    return false;
                }
                if (list == null || list.Count <= 0)
                {
                    msg = "打印失败！获取参数数据失败！";
                    return false;
                }
                var index = templateFileFullName.LastIndexOf("\\");
                var path = templateFileFullName.Substring(0, index + 1);
                var paramsDataPath = path + templateFileOrgName.Substring(0, templateFileOrgName.Length - 4) + ".txt";
                var fileSt = new FileStream(paramsDataPath, FileMode.OpenOrCreate);
                var wr = new StreamWriter(fileSt, Encoding.Default);
                fileSt.SetLength(0);
                var sb1 = new StringBuilder();
                var titleDict = list[0];
                foreach (var item in titleDict)
                {
                    sb1.Append(item.Key);
                    sb1.Append(",");
                }
                var sb2 = new StringBuilder();
                foreach (var dict in list)
                {
                    foreach (var item in dict)
                    {
                        sb2.Append(item.Value);
                        sb2.Append(",");
                    }
                    sb2.AppendLine();
                }
                wr.WriteLine(sb1);
                wr.WriteLine(sb2);

                wr.Close();
                fileSt.Close();

                var labelFormatDocument = engine.Documents.Open(templateFileFullName, localPrinterName);
                labelFormatDocument.Print(Guid.NewGuid().ToString("N"), 15);
            }
            catch (Exception ex)
            {
                msg = "BarTender打印异常！" + ex.Message;
                return false;
            }

            msg = "打印成功！";
            return true;
        }


    }
}
