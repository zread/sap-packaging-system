﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CSICPR.QRCode
{
    public partial class QRCodeBase : Form
    {
        public QRCodeBase()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 提示信息
        /// </summary>
        private string strMsg = "";

        /// <summary>
        /// 要删除的ID
        /// </summary>
        List<Int64> interIDList = new List<Int64>();

        private void QRCodeBase_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            loadGrid();
            loadNowRule();
        }

        #region 共有变量
        private static  QRCodeBase _theSingleton;
        public static void Instance(Form fm)
        {
            if (null == _theSingleton || _theSingleton.IsDisposed)
            {
                _theSingleton = new QRCodeBase
                {
                    MdiParent = fm,
                    WindowState = FormWindowState.Maximized
                };
                _theSingleton.Show();
            }
            else
            {
                _theSingleton.Activate();
                if (_theSingleton.WindowState == FormWindowState.Minimized)
                    _theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        #region 初始化grid
        /// <summary>
        /// 初始化grid
        /// </summary>
        private void loadGrid()
        {
            //绑定grid
            var baseQRCodeModel = BaseQRCodeDAL.QueryBaseQRCodeModel(FormCover.InterfaceConnString);
            if (baseQRCodeModel.Count > 0)
            {
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = baseQRCodeModel;
            }
            
        }
        #endregion

        #region 初始化当前规则
        /// <summary>
        /// 初始化当前规则
        /// </summary>
        private void loadNowRule()
        {
            txtQRCode.Text = "";
            //绑定当前使用的规则
            var baseQRCodeModelNow = BaseQRCodeDAL.QueryBaseQRCodeModel("T", FormCover.InterfaceConnString);
            if (baseQRCodeModelNow != null)
            {               
                txtQRCode.Text = baseQRCodeModelNow.SupplierCode + "-" + baseQRCodeModelNow.BSII + "-" + baseQRCodeModelNow.Revision + "-00000000000000";
                lblMsg.Visible = false;
            }
            else
            {
                lblMsg.Visible = true;
            }
        }
        #endregion

        #region 检查文本框属性
        private bool CheckTXT()
        {
            if (txtSupplierCode.Text.Trim() == "" || txtSupplierCode.Text.Trim().Length!=4)
            {
                strMsg = "供应商代码不能为空且必须4位";
                return false;
            }
            if (txtBSII.Text.Trim() == "" || txtBSII.Text.Trim().Length != 11)
            {
                strMsg = "型号编号不能为空且必须11位";
                return false;
            }
            if (txtRevision.Text.Trim() == "" || txtRevision.Text.Trim().Length != 3)
            {
                strMsg = "版本号不能为空且必须3位";
                return false;
            }
            //if (!IsNumAndEnCh(txtSupplierCode.Text.Trim()))
            //{
            //    strMsg = "供应商代码存在异常字符";
            //    return false;
            //}
            //if (!IsNumAndEnCh(txtBSII.Text.Trim()))
            //{
            //    strMsg = "型号编号存在异常字符";
            //    return false;
            //}
            if (!IsNumAndEnCh(txtRevision.Text.Trim()))
            {
                strMsg = "版本号存在异常字符";
                return false;
            }
            if (txtBrand.Text.Trim() == "")
            {
                strMsg = "商标模板不能为空";
                return false;
            }
            if (txtBrand.Text.Trim().Substring(txtBrand.Text.Trim().Length - 4).ToUpper() == ".btw")
            {
                strMsg = "商标模板文件后缀名应为.btw";
                return false;
            }
            if (txtSidebarcode.Text.Trim() == "")
            {
                strMsg = "侧边框模板不能为空";
                return false;
            }
            if (txtSidebarcode.Text.Trim().Substring(txtSidebarcode.Text.Trim().Length - 4).ToUpper() == ".btw")
            {
                strMsg = "侧边框模板文件后缀名应为.btw";
                return false;
            }

            return true;
        }
        #endregion

        #region 清空文本框信息
        private void clearTXT()
        {
            txtSupplierCode.Text = "";
            txtBSII.Text = "";
            txtRevision.Text = "";
        }
        #endregion

        #region 判断输入的字符串是否只包含数字和英文字母
        /// <summary>  
        /// 判断输入的字符串是否只包含数字和英文字母  
        /// </summary>  
        /// <param name="input"></param>  
        /// <returns></returns>  
        public static bool IsNumAndEnCh(string input)
        {
            string pattern = @"^[A-Za-z0-9]+$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(input);
        }
        #endregion

        #region 显示log信息
        /// <summary>
        /// 显示信息
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="msgtype"></param>
        /// <param name="lstV"></param>
        public static void Log(string msg, string msgtype = "", ListView lstV = null)
        {
            if (lstV != null)
            {

                if (msgtype.Equals("NORMAL"))
                {

                    ListViewItem Item = new ListViewItem();
                    Item.SubItems.Clear();
                    Item.SubItems[0].Text = System.DateTime.Now.ToString() + "   " + msg;// i.ToString();
                    Item.SubItems[0].ForeColor = System.Drawing.Color.Blue;
                    lstV.Items.Add(Item);
                }
                else if (msgtype.Equals("ABNORMAL"))
                {

                    ListViewItem Item = new ListViewItem();
                    Item.SubItems.Clear();
                    Item.SubItems[0].Text = System.DateTime.Now.ToString() + "   " + msg;// i.ToString();
                    Item.SubItems[0].ForeColor = System.Drawing.Color.Red;
                    lstV.Items.Add(Item);
                }
                lstV.Items[lstV.Items.Count - 1].EnsureVisible();
                if (lstV.Items.Count > 8)
                    lstV.Items.Clear();
            }
        }
        #endregion

        #region 新增按钮事件
        private void btInsert_Click(object sender, EventArgs e)
        {
            if (!CheckTXT())
            {
                Log(strMsg, "ABNORMAL", lstView);
                return;
            }
            try
            {

                BaseQRCodeModel bsqr = new BaseQRCodeModel();
                bsqr.SupplierCode = txtSupplierCode.Text.Trim();
                bsqr.BSII = txtBSII.Text.Trim();
                bsqr.Revision = txtRevision.Text.Trim();
                bsqr.BrandName = txtBrand.Text.Trim();
                bsqr.SideBarcodeName = txtSidebarcode.Text.Trim();
                bsqr.CreateOn = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                bsqr.CreateBy = FormCover.CurrUserWorkID;
                BaseQRCodeDAL.InsertBaseQRCodeModel(bsqr, FormCover.InterfaceConnString);
                loadGrid();
                clearTXT();
                Log("新增成功", "NORMAL", lstView);
                return;
            }
            catch(Exception ex)
            {
                Log("操作失败"+ex.Message, "ABNORMAL", lstView);
                return;
            }
        }
        #endregion

        #region 删除按钮事件
        private void btDelete_Click(object sender, EventArgs e)
        {
         
            if (this.dataGridView1.Rows.Count < 1)
            {
                Log("没有要操作的数据!", "ABNORMAL", lstView);
                return;
            }

            if (interIDList.Count > 0)
                interIDList.Clear();

            int StorageCount = 0;
            foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
            {
                if (Convert.ToString(dgvRow.Cells[0].Value).ToUpper() == "TRUE")
                {
                    StorageCount = StorageCount + 1;
                    if (!interIDList.Contains(Convert.ToInt64(dgvRow.Cells[8].Value)))
                        interIDList.Add(Convert.ToInt64(dgvRow.Cells[8].Value));
                }
            }

            if (StorageCount == 0)
            {
                ToolsClass.Log("请选择要删除的数据!", "ABNORMAL", lstView);
                return;
            }

            try
            {

                foreach (Int64 interID in interIDList)
                {
                    BaseQRCodeModel bsqr = new BaseQRCodeModel();
                    bsqr.InterID = interID;
                    BaseQRCodeDAL.DeleteBaseQRCodeModel(bsqr, FormCover.InterfaceConnString);
                }
                loadGrid();
                loadNowRule();
                ToolsClass.Log("删除成功", "NORMAL", lstView);
            }
            catch(Exception ex)
            {
                ToolsClass.Log("删除失败"+ex.Message, "ABNORMAL", lstView);
                return;
            }

        }
        #endregion

        #region 设定当前规则按钮事件
        private void btSave_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.Rows.Count < 1)
            {
                Log("没有要操作的数据!", "ABNORMAL", lstView);
                return;
            }

            if (interIDList.Count > 0)
                interIDList.Clear();

            int StorageCount = 0;
            foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
            {
                if (Convert.ToString(dgvRow.Cells[0].Value).ToUpper() == "TRUE")
                {
                    StorageCount = StorageCount + 1;
                    if (!interIDList.Contains(Convert.ToInt64(dgvRow.Cells[8].Value)))
                        interIDList.Add(Convert.ToInt64(dgvRow.Cells[8].Value));
                }
            }

            if (StorageCount!=1)
            {
                Log("至多只能选择一笔数据", "ABNORMAL", lstView);
                return;
            }

            try
            {

                foreach (Int64 interID in interIDList)
                {
                    BaseQRCodeModel bsqr = new BaseQRCodeModel();
                    bsqr.InterID = interID;
                    BaseQRCodeDAL.UpdateBaseQRCodeModelByStatus(bsqr, FormCover.InterfaceConnString);
                    BaseQRCodeDAL.UpdateBaseQRCodeModelByID(bsqr, FormCover.InterfaceConnString);
                }
                loadNowRule();
                ToolsClass.Log("设定成功", "NORMAL", lstView);
            }
            catch (Exception ex)
            {
                ToolsClass.Log("设定失败" + ex.Message, "ABNORMAL", lstView);
                return;
            }
        }
        #endregion

        #region 全选按钮事件
        private void chbSelected_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
            {
                dgvRow.Cells[0].Value = this.chbSelected.Checked;
            }
        }
        #endregion
    }
}
