﻿namespace CSICPR
{
    partial class FormNormalStorageSN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNormalStorageSN));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CartonReset = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.GpbCartonQuery = new System.Windows.Forms.GroupBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.CartonQueryList = new System.Windows.Forms.Panel();
            this.txtCarton = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.CheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.RowIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ModuleSN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CartonNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NormalWo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WaiXieWo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReworkWo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesOrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SalesItemNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Factory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Workshop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PackingLocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustomerCartonNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TestPower = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StdPower = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ModuleGrade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FinishedOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CellEff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ByIm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CellCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CellBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CellPrintMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GlassCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GlassBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EvaCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EvaBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TptCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TptBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConBoxCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConBoxBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LongFrameCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LongFrameBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GlassThickness = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PackingMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShortFrameCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shortFrameBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsCancelPacking = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsOnlyPacking = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GpbWoQuery = new System.Windows.Forms.GroupBox();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.txtSalesItemNo = new System.Windows.Forms.TextBox();
            this.txtSalesOrderNo = new System.Windows.Forms.TextBox();
            this.PicBoxWO = new System.Windows.Forms.PictureBox();
            this.txtWo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ddlWoType = new System.Windows.Forms.ComboBox();
            this.txtWoOrder = new System.Windows.Forms.TextBox();
            this.LblWo = new System.Windows.Forms.Label();
            this.txtMitemCode = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.lblWoType = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPlanCode = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtWO1 = new System.Windows.Forms.TextBox();
            this.txtlocation = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFactory = new System.Windows.Forms.TextBox();
            this.Reset = new System.Windows.Forms.Button();
            this.Set = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SapSave = new System.Windows.Forms.Button();
            this.lstView = new System.Windows.Forms.ListView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.GpbCartonQuery.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.CartonQueryList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.GpbWoQuery.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxWO)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1020, 15);
            this.panel1.TabIndex = 3;
            // 
            // CartonReset
            // 
            this.CartonReset.Location = new System.Drawing.Point(297, 8);
            this.CartonReset.Name = "CartonReset";
            this.CartonReset.Size = new System.Drawing.Size(56, 22);
            this.CartonReset.TabIndex = 2;
            this.CartonReset.Tag = "button1";
            this.CartonReset.Text = "重置";
            this.CartonReset.UseVisualStyleBackColor = true;
            this.CartonReset.Click += new System.EventHandler(this.CartonReset_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.splitContainer1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 15);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1020, 536);
            this.panel3.TabIndex = 4;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.GpbCartonQuery);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.GpbWoQuery);
            this.splitContainer1.Size = new System.Drawing.Size(1020, 536);
            this.splitContainer1.SplitterDistance = 708;
            this.splitContainer1.TabIndex = 0;
            // 
            // GpbCartonQuery
            // 
            this.GpbCartonQuery.Controls.Add(this.splitContainer2);
            this.GpbCartonQuery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GpbCartonQuery.Location = new System.Drawing.Point(0, 0);
            this.GpbCartonQuery.Name = "GpbCartonQuery";
            this.GpbCartonQuery.Size = new System.Drawing.Size(708, 536);
            this.GpbCartonQuery.TabIndex = 2;
            this.GpbCartonQuery.TabStop = false;
            this.GpbCartonQuery.Text = "托号查询";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 17);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.pictureBox1);
            this.splitContainer2.Panel1.Controls.Add(this.CartonReset);
            this.splitContainer2.Panel1.Controls.Add(this.CartonQueryList);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dataGridView1);
            this.splitContainer2.Size = new System.Drawing.Size(702, 516);
            this.splitContainer2.SplitterDistance = 33;
            this.splitContainer2.TabIndex = 48;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(260, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(27, 18);
            this.pictureBox1.TabIndex = 223;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // CartonQueryList
            // 
            this.CartonQueryList.Controls.Add(this.txtCarton);
            this.CartonQueryList.Controls.Add(this.label2);
            this.CartonQueryList.Location = new System.Drawing.Point(30, 2);
            this.CartonQueryList.Name = "CartonQueryList";
            this.CartonQueryList.Size = new System.Drawing.Size(235, 32);
            this.CartonQueryList.TabIndex = 93;
            // 
            // txtCarton
            // 
            this.txtCarton.Location = new System.Drawing.Point(81, 6);
            this.txtCarton.Name = "txtCarton";
            this.txtCarton.Size = new System.Drawing.Size(148, 21);
            this.txtCarton.TabIndex = 94;
            this.txtCarton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCarton_KeyDown);
            this.txtCarton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCarton_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 12);
            this.label2.TabIndex = 94;
            this.label2.Text = "内 部 托 号";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.PeachPuff;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CheckBox,
            this.RowIndex,
            this.ModuleSN,
            this.CartonNo,
            this.OrderNo,
            this.NormalWo,
            this.WaiXieWo,
            this.ReworkWo,
            this.ProductCode,
            this.SalesOrderNo,
            this.SalesItemNo,
            this.Factory,
            this.Workshop,
            this.PackingLocation,
            this.CustomerCartonNo,
            this.TestPower,
            this.StdPower,
            this.ModuleGrade,
            this.FinishedOn,
            this.CellEff,
            this.ByIm,
            this.OrderStatus,
            this.CellCode,
            this.CellBatch,
            this.CellPrintMode,
            this.GlassCode,
            this.GlassBatch,
            this.EvaCode,
            this.EvaBatch,
            this.TptCode,
            this.TptBatch,
            this.ConBoxCode,
            this.ConBoxBatch,
            this.LongFrameCode,
            this.LongFrameBatch,
            this.GlassThickness,
            this.PackingMode,
            this.ShortFrameCode,
            this.shortFrameBatch,
            this.IsCancelPacking,
            this.IsOnlyPacking});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.InactiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(702, 479);
            this.dataGridView1.TabIndex = 47;
            // 
            // CheckBox
            // 
            this.CheckBox.HeaderText = "";
            this.CheckBox.Name = "CheckBox";
            this.CheckBox.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CheckBox.Width = 45;
            // 
            // RowIndex
            // 
            this.RowIndex.HeaderText = "序号";
            this.RowIndex.Name = "RowIndex";
            this.RowIndex.ReadOnly = true;
            this.RowIndex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RowIndex.Width = 45;
            // 
            // ModuleSN
            // 
            this.ModuleSN.HeaderText = "组件号";
            this.ModuleSN.Name = "ModuleSN";
            this.ModuleSN.ReadOnly = true;
            this.ModuleSN.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ModuleSN.Width = 110;
            // 
            // CartonNo
            // 
            this.CartonNo.HeaderText = "内部托号";
            this.CartonNo.Name = "CartonNo";
            this.CartonNo.ReadOnly = true;
            this.CartonNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // OrderNo
            // 
            this.OrderNo.HeaderText = "工单";
            this.OrderNo.Name = "OrderNo";
            this.OrderNo.ReadOnly = true;
            this.OrderNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // NormalWo
            // 
            this.NormalWo.HeaderText = "转常规工单";
            this.NormalWo.Name = "NormalWo";
            // 
            // WaiXieWo
            // 
            this.WaiXieWo.HeaderText = "外协后工单";
            this.WaiXieWo.Name = "WaiXieWo";
            // 
            // ReworkWo
            // 
            this.ReworkWo.HeaderText = "返工工单";
            this.ReworkWo.Name = "ReworkWo";
            // 
            // ProductCode
            // 
            this.ProductCode.HeaderText = "产品物料代码";
            this.ProductCode.Name = "ProductCode";
            this.ProductCode.ReadOnly = true;
            // 
            // SalesOrderNo
            // 
            this.SalesOrderNo.HeaderText = "销售订单";
            this.SalesOrderNo.Name = "SalesOrderNo";
            // 
            // SalesItemNo
            // 
            this.SalesItemNo.HeaderText = "销售订单行项目";
            this.SalesItemNo.Name = "SalesItemNo";
            // 
            // Factory
            // 
            this.Factory.HeaderText = "工厂";
            this.Factory.Name = "Factory";
            this.Factory.ReadOnly = true;
            // 
            // Workshop
            // 
            this.Workshop.HeaderText = "车间";
            this.Workshop.Name = "Workshop";
            this.Workshop.ReadOnly = true;
            // 
            // PackingLocation
            // 
            this.PackingLocation.HeaderText = "入库地点";
            this.PackingLocation.Name = "PackingLocation";
            this.PackingLocation.ReadOnly = true;
            // 
            // CustomerCartonNo
            // 
            this.CustomerCartonNo.HeaderText = "客户托号";
            this.CustomerCartonNo.Name = "CustomerCartonNo";
            // 
            // TestPower
            // 
            this.TestPower.HeaderText = "实测功率";
            this.TestPower.Name = "TestPower";
            this.TestPower.ReadOnly = true;
            this.TestPower.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // StdPower
            // 
            this.StdPower.HeaderText = "标称功率";
            this.StdPower.Name = "StdPower";
            this.StdPower.ReadOnly = true;
            this.StdPower.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ModuleGrade
            // 
            this.ModuleGrade.HeaderText = "组件等级";
            this.ModuleGrade.Name = "ModuleGrade";
            this.ModuleGrade.ReadOnly = true;
            this.ModuleGrade.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // FinishedOn
            // 
            this.FinishedOn.HeaderText = "完工日期";
            this.FinishedOn.Name = "FinishedOn";
            this.FinishedOn.ReadOnly = true;
            // 
            // CellEff
            // 
            this.CellEff.HeaderText = "电池片转换效率";
            this.CellEff.Name = "CellEff";
            this.CellEff.ReadOnly = true;
            // 
            // ByIm
            // 
            this.ByIm.HeaderText = "电池分档";
            this.ByIm.Name = "ByIm";
            // 
            // OrderStatus
            // 
            this.OrderStatus.HeaderText = "工单状态";
            this.OrderStatus.Name = "OrderStatus";
            // 
            // CellCode
            // 
            this.CellCode.HeaderText = "电池片物料号";
            this.CellCode.Name = "CellCode";
            // 
            // CellBatch
            // 
            this.CellBatch.HeaderText = "电池片批次";
            this.CellBatch.Name = "CellBatch";
            // 
            // CellPrintMode
            // 
            this.CellPrintMode.HeaderText = "网版";
            this.CellPrintMode.Name = "CellPrintMode";
            // 
            // GlassCode
            // 
            this.GlassCode.HeaderText = "玻璃物料号";
            this.GlassCode.Name = "GlassCode";
            // 
            // GlassBatch
            // 
            this.GlassBatch.HeaderText = "玻璃批次";
            this.GlassBatch.Name = "GlassBatch";
            // 
            // EvaCode
            // 
            this.EvaCode.HeaderText = "EVA物料号";
            this.EvaCode.Name = "EvaCode";
            // 
            // EvaBatch
            // 
            this.EvaBatch.HeaderText = "EVA批次";
            this.EvaBatch.Name = "EvaBatch";
            // 
            // TptCode
            // 
            this.TptCode.HeaderText = "背板物料号";
            this.TptCode.Name = "TptCode";
            // 
            // TptBatch
            // 
            this.TptBatch.HeaderText = "背板批次";
            this.TptBatch.Name = "TptBatch";
            // 
            // ConBoxCode
            // 
            this.ConBoxCode.HeaderText = "接线盒物料号";
            this.ConBoxCode.Name = "ConBoxCode";
            // 
            // ConBoxBatch
            // 
            this.ConBoxBatch.HeaderText = "接线盒批次";
            this.ConBoxBatch.Name = "ConBoxBatch";
            // 
            // LongFrameCode
            // 
            this.LongFrameCode.HeaderText = "长边框物料号";
            this.LongFrameCode.Name = "LongFrameCode";
            // 
            // LongFrameBatch
            // 
            this.LongFrameBatch.HeaderText = "长边框批次";
            this.LongFrameBatch.Name = "LongFrameBatch";
            // 
            // GlassThickness
            // 
            this.GlassThickness.HeaderText = "玻璃厚度";
            this.GlassThickness.Name = "GlassThickness";
            // 
            // PackingMode
            // 
            this.PackingMode.HeaderText = "包装方式";
            this.PackingMode.Name = "PackingMode";
            // 
            // ShortFrameCode
            // 
            this.ShortFrameCode.HeaderText = "短边框物料号";
            this.ShortFrameCode.Name = "ShortFrameCode";
            // 
            // shortFrameBatch
            // 
            this.shortFrameBatch.HeaderText = "短边框批次";
            this.shortFrameBatch.Name = "shortFrameBatch";
            // 
            // IsCancelPacking
            // 
            this.IsCancelPacking.HeaderText = "撤销入库";
            this.IsCancelPacking.Name = "IsCancelPacking";
            // 
            // IsOnlyPacking
            // 
            this.IsOnlyPacking.HeaderText = "是否拼托";
            this.IsOnlyPacking.Name = "IsOnlyPacking";
            // 
            // GpbWoQuery
            // 
            this.GpbWoQuery.Controls.Add(this.splitContainer3);
            this.GpbWoQuery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GpbWoQuery.Location = new System.Drawing.Point(0, 0);
            this.GpbWoQuery.Name = "GpbWoQuery";
            this.GpbWoQuery.Size = new System.Drawing.Size(308, 536);
            this.GpbWoQuery.TabIndex = 0;
            this.GpbWoQuery.TabStop = false;
            this.GpbWoQuery.Text = "工单查询";
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer3.Location = new System.Drawing.Point(3, 17);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.label6);
            this.splitContainer3.Panel1.Controls.Add(this.label5);
            this.splitContainer3.Panel1.Controls.Add(this.pictureBox2);
            this.splitContainer3.Panel1.Controls.Add(this.checkBox2);
            this.splitContainer3.Panel1.Controls.Add(this.txtSalesItemNo);
            this.splitContainer3.Panel1.Controls.Add(this.txtSalesOrderNo);
            this.splitContainer3.Panel1.Controls.Add(this.PicBoxWO);
            this.splitContainer3.Panel1.Controls.Add(this.txtWo);
            this.splitContainer3.Panel1.Controls.Add(this.label3);
            this.splitContainer3.Panel1.Controls.Add(this.ddlWoType);
            this.splitContainer3.Panel1.Controls.Add(this.txtWoOrder);
            this.splitContainer3.Panel1.Controls.Add(this.LblWo);
            this.splitContainer3.Panel1.Controls.Add(this.txtMitemCode);
            this.splitContainer3.Panel1.Controls.Add(this.label14);
            this.splitContainer3.Panel1.Controls.Add(this.lblWoType);
            this.splitContainer3.Panel1.Controls.Add(this.label10);
            this.splitContainer3.Panel1.Controls.Add(this.txtPlanCode);
            this.splitContainer3.Panel1.Controls.Add(this.label9);
            this.splitContainer3.Panel1.Controls.Add(this.txtWO1);
            this.splitContainer3.Panel1.Controls.Add(this.txtlocation);
            this.splitContainer3.Panel1.Controls.Add(this.label4);
            this.splitContainer3.Panel1.Controls.Add(this.txtFactory);
            this.splitContainer3.Panel1.Controls.Add(this.Reset);
            this.splitContainer3.Panel1.Controls.Add(this.Set);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer3.Size = new System.Drawing.Size(302, 516);
            this.splitContainer3.SplitterDistance = 453;
            this.splitContainer3.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 184);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 12);
            this.label6.TabIndex = 291;
            this.label6.Text = "销 售 订 单";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 223);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 290;
            this.label5.Text = "销售订单项目";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.InitialImage")));
            this.pictureBox2.Location = new System.Drawing.Point(264, 378);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(27, 18);
            this.pictureBox2.TabIndex = 289;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(17, 379);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(90, 16);
            this.checkBox2.TabIndex = 288;
            this.checkBox2.Text = "外协后 工单";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.Visible = false;
            // 
            // txtSalesItemNo
            // 
            this.txtSalesItemNo.BackColor = System.Drawing.SystemColors.Menu;
            this.txtSalesItemNo.Enabled = false;
            this.txtSalesItemNo.Location = new System.Drawing.Point(116, 223);
            this.txtSalesItemNo.Name = "txtSalesItemNo";
            this.txtSalesItemNo.Size = new System.Drawing.Size(150, 21);
            this.txtSalesItemNo.TabIndex = 286;
            // 
            // txtSalesOrderNo
            // 
            this.txtSalesOrderNo.BackColor = System.Drawing.SystemColors.Menu;
            this.txtSalesOrderNo.Enabled = false;
            this.txtSalesOrderNo.Location = new System.Drawing.Point(116, 181);
            this.txtSalesOrderNo.Name = "txtSalesOrderNo";
            this.txtSalesOrderNo.Size = new System.Drawing.Size(150, 21);
            this.txtSalesOrderNo.TabIndex = 285;
            // 
            // PicBoxWO
            // 
            this.PicBoxWO.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBoxWO.BackgroundImage")));
            this.PicBoxWO.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicBoxWO.InitialImage")));
            this.PicBoxWO.Location = new System.Drawing.Point(265, 56);
            this.PicBoxWO.Name = "PicBoxWO";
            this.PicBoxWO.Size = new System.Drawing.Size(27, 18);
            this.PicBoxWO.TabIndex = 284;
            this.PicBoxWO.TabStop = false;
            this.PicBoxWO.Click += new System.EventHandler(this.PicBoxWO_Click);
            // 
            // txtWo
            // 
            this.txtWo.BackColor = System.Drawing.SystemColors.Menu;
            this.txtWo.Enabled = false;
            this.txtWo.Location = new System.Drawing.Point(115, 96);
            this.txtWo.Name = "txtWo";
            this.txtWo.Size = new System.Drawing.Size(150, 21);
            this.txtWo.TabIndex = 279;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 12);
            this.label3.TabIndex = 278;
            this.label3.Text = "外协前 工单";
            // 
            // ddlWoType
            // 
            this.ddlWoType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlWoType.FormattingEnabled = true;
            this.ddlWoType.Location = new System.Drawing.Point(116, 15);
            this.ddlWoType.Name = "ddlWoType";
            this.ddlWoType.Size = new System.Drawing.Size(150, 20);
            this.ddlWoType.TabIndex = 276;
            this.ddlWoType.SelectedIndexChanged += new System.EventHandler(this.ddlWoType_SelectedIndexChanged);
            // 
            // txtWoOrder
            // 
            this.txtWoOrder.Location = new System.Drawing.Point(114, 55);
            this.txtWoOrder.Name = "txtWoOrder";
            this.txtWoOrder.Size = new System.Drawing.Size(150, 21);
            this.txtWoOrder.TabIndex = 275;
            this.txtWoOrder.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtWoOrder_KeyDown);
            this.txtWoOrder.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWoOrder_KeyPress);
            // 
            // LblWo
            // 
            this.LblWo.AutoSize = true;
            this.LblWo.Location = new System.Drawing.Point(29, 61);
            this.LblWo.Name = "LblWo";
            this.LblWo.Size = new System.Drawing.Size(71, 12);
            this.LblWo.TabIndex = 274;
            this.LblWo.Text = "工       单";
            // 
            // txtMitemCode
            // 
            this.txtMitemCode.BackColor = System.Drawing.SystemColors.Menu;
            this.txtMitemCode.Enabled = false;
            this.txtMitemCode.Location = new System.Drawing.Point(116, 140);
            this.txtMitemCode.Name = "txtMitemCode";
            this.txtMitemCode.Size = new System.Drawing.Size(150, 21);
            this.txtMitemCode.TabIndex = 273;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(22, 144);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 12);
            this.label14.TabIndex = 272;
            this.label14.Text = "产品物料编码";
            // 
            // lblWoType
            // 
            this.lblWoType.AutoSize = true;
            this.lblWoType.Location = new System.Drawing.Point(28, 17);
            this.lblWoType.Name = "lblWoType";
            this.lblWoType.Size = new System.Drawing.Size(71, 12);
            this.lblWoType.TabIndex = 270;
            this.lblWoType.Text = "工 单 类 型";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(25, 340);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 282;
            this.label10.Text = "工        厂";
            // 
            // txtPlanCode
            // 
            this.txtPlanCode.BackColor = System.Drawing.SystemColors.Menu;
            this.txtPlanCode.Enabled = false;
            this.txtPlanCode.Location = new System.Drawing.Point(116, 337);
            this.txtPlanCode.Name = "txtPlanCode";
            this.txtPlanCode.Size = new System.Drawing.Size(150, 21);
            this.txtPlanCode.TabIndex = 283;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 300);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 12);
            this.label9.TabIndex = 271;
            this.label9.Text = "入 库 地 点";
            // 
            // txtWO1
            // 
            this.txtWO1.Location = new System.Drawing.Point(113, 377);
            this.txtWO1.Name = "txtWO1";
            this.txtWO1.Size = new System.Drawing.Size(150, 21);
            this.txtWO1.TabIndex = 287;
            this.txtWO1.Visible = false;
            // 
            // txtlocation
            // 
            this.txtlocation.BackColor = System.Drawing.SystemColors.Menu;
            this.txtlocation.Enabled = false;
            this.txtlocation.Location = new System.Drawing.Point(116, 298);
            this.txtlocation.Name = "txtlocation";
            this.txtlocation.Size = new System.Drawing.Size(150, 21);
            this.txtlocation.TabIndex = 277;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 259);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 12);
            this.label4.TabIndex = 280;
            this.label4.Text = "车       间";
            // 
            // txtFactory
            // 
            this.txtFactory.BackColor = System.Drawing.SystemColors.Menu;
            this.txtFactory.Enabled = false;
            this.txtFactory.Location = new System.Drawing.Point(116, 256);
            this.txtFactory.Name = "txtFactory";
            this.txtFactory.Size = new System.Drawing.Size(150, 21);
            this.txtFactory.TabIndex = 281;
            // 
            // Reset
            // 
            this.Reset.Location = new System.Drawing.Point(191, 405);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(72, 24);
            this.Reset.TabIndex = 269;
            this.Reset.Tag = "button1";
            this.Reset.Text = "重置";
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // Set
            // 
            this.Set.Location = new System.Drawing.Point(106, 404);
            this.Set.Name = "Set";
            this.Set.Size = new System.Drawing.Size(72, 24);
            this.Set.TabIndex = 268;
            this.Set.Tag = "button1";
            this.Set.Text = "设置";
            this.Set.UseVisualStyleBackColor = true;
            this.Set.Click += new System.EventHandler(this.Set_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SapSave);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(302, 59);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // SapSave
            // 
            this.SapSave.Location = new System.Drawing.Point(178, 12);
            this.SapSave.Name = "SapSave";
            this.SapSave.Size = new System.Drawing.Size(83, 32);
            this.SapSave.TabIndex = 50;
            this.SapSave.Tag = "button1";
            this.SapSave.Text = "入库";
            this.SapSave.UseVisualStyleBackColor = true;
            this.SapSave.Click += new System.EventHandler(this.SapSave_Click);
            // 
            // lstView
            // 
            this.lstView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstView.FullRowSelect = true;
            this.lstView.Location = new System.Drawing.Point(0, 0);
            this.lstView.MultiSelect = false;
            this.lstView.Name = "lstView";
            this.lstView.Size = new System.Drawing.Size(1020, 85);
            this.lstView.TabIndex = 1;
            this.lstView.UseCompatibleStateImageBehavior = false;
            this.lstView.View = System.Windows.Forms.View.Details;
            this.lstView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstView_KeyDown);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lstView);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 551);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1020, 85);
            this.panel2.TabIndex = 5;
            // 
            // FormNormalStorageSN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 636);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "FormNormalStorageSN";
            this.Text = "批量零散件入库";
            this.Load += new System.EventHandler(this.SNStorage_Load);
            this.panel3.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.GpbCartonQuery.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.CartonQueryList.ResumeLayout(false);
            this.CartonQueryList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.GpbWoQuery.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBoxWO)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button CartonReset;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox GpbCartonQuery;
        private System.Windows.Forms.GroupBox GpbWoQuery;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.ListView lstView;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button SapSave;
        private System.Windows.Forms.Panel CartonQueryList;
        private System.Windows.Forms.TextBox txtCarton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.TextBox txtSalesItemNo;
        private System.Windows.Forms.TextBox txtSalesOrderNo;
        private System.Windows.Forms.PictureBox PicBoxWO;
        private System.Windows.Forms.TextBox txtWo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox ddlWoType;
        private System.Windows.Forms.TextBox txtWoOrder;
        private System.Windows.Forms.Label LblWo;
        private System.Windows.Forms.TextBox txtMitemCode;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblWoType;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPlanCode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtWO1;
        private System.Windows.Forms.TextBox txtlocation;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFactory;
        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.Button Set;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn ModuleSN;
        private System.Windows.Forms.DataGridViewTextBoxColumn CartonNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn NormalWo;
        private System.Windows.Forms.DataGridViewTextBoxColumn WaiXieWo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReworkWo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesOrderNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn SalesItemNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factory;
        private System.Windows.Forms.DataGridViewTextBoxColumn Workshop;
        private System.Windows.Forms.DataGridViewTextBoxColumn PackingLocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerCartonNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn TestPower;
        private System.Windows.Forms.DataGridViewTextBoxColumn StdPower;
        private System.Windows.Forms.DataGridViewTextBoxColumn ModuleGrade;
        private System.Windows.Forms.DataGridViewTextBoxColumn FinishedOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CellEff;
        private System.Windows.Forms.DataGridViewTextBoxColumn ByIm;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn CellCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn CellBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn CellPrintMode;
        private System.Windows.Forms.DataGridViewTextBoxColumn GlassCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn GlassBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn EvaCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn EvaBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn TptCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn TptBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConBoxCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConBoxBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn LongFrameCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn LongFrameBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn GlassThickness;
        private System.Windows.Forms.DataGridViewTextBoxColumn PackingMode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShortFrameCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn shortFrameBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsCancelPacking;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsOnlyPacking;
    }
}