﻿using System;
using System.ComponentModel;
//using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Collections.Generic;

namespace CSICPR
{
    public partial class FormSAPWOLoad : Form     
    {
        #region 共有变量
        private static List<LanguageItemModel> LanMessList;//定义语言集
        private static FormSAPWOLoad theSingleton = null;
        public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormSAPWOLoad();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if (theSingleton.WindowState == FormWindowState.Minimized)
                    theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        public  FormSAPWOLoad()
        {
            InitializeComponent();
        }

        private void FormSAPLoad_Load(object sender, EventArgs e)
        {
            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion
            this.lstView.Columns.Add(LanguageHelper.GetMessage(LanMessList, "Message1", "提示信息"), 1000, HorizontalAlignment.Left);
        }
 
        /// <summary>
        /// 提供数据复制功能
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (lstView.SelectedItems.Count > 0)
                {
                    if (lstView.SelectedItems[0].Text != "")
                        Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
                }
            }
        }

        private string wo = "";
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.txtWo.Text.Trim().Equals(""))
            {
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message2", "输入的工单号码不能为空！"), "ABNORMAL", lstView);
                this.txtWo.Focus();
                this.txtWo.SelectAll();
                return;
            }
            else
                wo = this.txtWo.Text.Trim();

            try
            {
                string msg = "";                
                SapStorageService.SapMesInterfaceClient service = new SapStorageService.SapMesInterfaceClient();
                service.Endpoint.Address = new System.ServiceModel.EndpointAddress(FormCover.WebServiceAddress);
                msg = service.orderSaptoMes("", wo);
                
                if (msg.Equals("0"))
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message3", "工单号码不存在,请确认:") + wo, "ABNORMAL", lstView);
                    this.txtWo.Focus();
                    this.txtWo.SelectAll();
                    return;
                }
                else
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message4", "工单号码下载成功:") + wo, "NORMAL", lstView);
                    //ProductStorageDAL.GetWoMaxCount(txtWo.Text.ToString()); 
                    this.txtWo.Clear();
                    this.txtWo.Focus();
                    this.txtWo.SelectAll();
                    
                   #region modification production yeild jacky
                    int InputQty = 0;
                    int ScrapQty = 0;
                   DataTable dt = ProductStorageDAL.GetWoDataIno(wo);
                   if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                     if (Convert.ToString(row["RESV08"]).Equals(""))//实际投产量
                     {
                     	if (Convert.ToString(row["TWO_WO_QTY"]).Equals(""))//Planned Production QTY
                       		InputQty = 0;
                   		else
                       		InputQty = Convert.ToInt32(row["TWO_WO_QTY"]);//Planned Production QTY                        
                     }
                    else
                        InputQty = Convert.ToInt32(row["RESV08"]);
                  
                   
					
                    if (Convert.ToString(row["RESV09"]).Equals(""))//Scrap
                        ScrapQty = 0;
                    else
                        ScrapQty = Convert.ToInt32(row["RESV09"]);//Scrap               
                }
                else
                {
                    MessageBox.Show("Procution Order is empty", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }                                 
                   
                string msg_Settingyeild = "";
                if (ProductStorageDAL.UpdateWoDataIno(wo, InputQty,ScrapQty, out msg_Settingyeild))
                {
                    MessageBox.Show("Successful");
                }
                else
                    MessageBox.Show("Failed in Modifying actual yeild： " + msg_Settingyeild + "");
                   #endregion
                }
            }
            catch (Exception ex)
            {
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message5", "工单号码在下载时发生异常: ") + wo + ex.Message + "", "ABNORMAL", lstView);
                this.txtWo.Focus();
                this.txtWo.SelectAll();
            }
        }
        private void txtWo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                this.btnSave_Click(null,null);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            this.txtWo.Clear();
            if (lstView.Items.Count > 100)
                lstView.Items.Clear();
            this.txtWo.Focus();
        }
    }
}
