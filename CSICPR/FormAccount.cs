﻿using System;
using System.Windows.Forms;

namespace CSICPR
{
    public partial class FormAccount : Form
    {
        string IsMix;
        public FormAccount(string isMix = "")
        {
            InitializeComponent();
            //languagechange change = new languagechange("FormAccount", this);
            //languagechange.changeBTN(false);
            //languagechange.changefrmName(false);
            //languagechange.changeLBL(false);

            IsMix = isMix;
            if (IsMix == "")
            {
                if (FormCover.DBName != null) this.tbDBName.Text = FormCover.DBName;
                if (FormCover.DBPWD != null) this.tbDBpwd.Text = FormCover.DBPWD;
                if (FormCover.DBServer != null) this.tbDBServer.Text = FormCover.DBServer;
                if (FormCover.DBUser != null) this.tbDBUser.Text = FormCover.DBUser;
            }
            if (FormCover.loginBackgroundImage!=null)
            {
                this.BackgroundImage = FormCover.loginBackgroundImage;
            }
        }

        private void textBox_Enter(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = System.Drawing.Color.Yellow;
        }
        private void textBox_Leave(object sender, EventArgs e)
        {
            ((TextBox)sender).BackColor = System.Drawing.SystemColors.Window;
            
        }
        private void bSave_Click(object sender, System.EventArgs e)
        {
            string sv = this.tbDBServer.Text.Trim();
            if (sv.Length == 0)
            {
                FormMain.showTipMSG("Please input DataBase Server Name or DataBase Server IP", this.tbDBServer, "Prompting Message", ToolTipIcon.Warning);
                return;
            }
            //if (IsMix.Length > 0)
            //{
            //    TextBox mix = new TextBox();
            //    mix.Name = IsMix;
            //    mix.Text = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", sv, tbDBName.Text.Trim(), tbDBUser.Text.Trim(), tbDBpwd.Text.Trim());
            //    MessageBox.Show("Save DataBase Config OK", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    ToolsClass.saveSQL(mix);
            //}
            //else
            //{
            //    if (FormCover.saveRegistryKeys(new string[,] { { "DBServer", sv }, { "DBName", this.tbDBName.Text.Trim() }, { "DBUser", this.tbDBUser.Text.Trim() }, { "DBPWD", this.tbDBpwd.Text.Trim() } }))
            //    {
            ToolsClass.saveXMLNode("DataSource", this.tbDBServer.Text.Trim(),false,"","config.xml");
            ToolsClass.saveXMLNode("UserID", this.tbDBUser.Text.Trim(), false, "", "config.xml");
            ToolsClass.saveXMLNode("Password", this.tbDBpwd.Text.Trim(), false, "", "config.xml");
            ToolsClass.saveXMLNode("DBName", this.tbDBName.Text.Trim(), false, "", "config.xml");

            FormCover.DBServer = sv;
            FormCover.DBName = this.tbDBName.Text.Trim();
            FormCover.DBPWD = this.tbDBpwd.Text.Trim();
            FormCover.DBUser = this.tbDBUser.Text.Trim();
            MessageBox.Show("Save DataBase Config OK", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            FormCover.connectionBase = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", FormCover.DBServer, FormCover.DBName, FormCover.DBUser, FormCover.DBPWD);
            //    }
            //}
            this.DialogResult = DialogResult.OK;
        }

        private void btTestConn_Click(object sender, EventArgs e)
        {
            string sv = this.tbDBServer.Text.Trim(); 
            if (this.tbDBServer.Text.Trim().Length == 0)
            {
                FormMain.showTipMSG("Please input DataBase Server Name or DataBase Server IP", this.tbDBServer, "Prompting Message", ToolTipIcon.Warning);
                return;
            }
            try
            {
                
                using (System.Data.SqlClient.SqlConnection conn =new System.Data.SqlClient.SqlConnection())
                {
                    conn.ConnectionString = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};Connection Timeout=1", sv, tbDBName.Text.Trim(), tbDBUser.Text.Trim(), tbDBpwd.Text.Trim());
                    conn.Open();
                    System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "if (Select name from syscolumns Where ID=OBJECT_ID('[Process]') AND name='Mix' ) is null " +
  "begin " +
  "ALTER TABLE [Process]  ADD Mix nvarchar(8) null; " +
  "ALTER TABLE [Box]  ADD Mix nvarchar(8) null; " +
  "ALTER TABLE [Product]  ADD Mix nvarchar(8) null;   end";
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    //return IsHasValue(conn, SqlStr);
                    MessageBox.Show("DataBase Connect Test OK, Please Click [Save] Button to Save Config", "Prompting Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    bSave.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                //if (ex.Message.Contains("posbase"))
                //{
                //    MessageBox.Show("请检测服务器，是否正确安装Pos Server，或者是否拥有访问权限！", "错误信息", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
                //else
                    MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
    }
}
