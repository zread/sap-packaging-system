﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Xml;
using CSICPR.Properties;



namespace CSICPR
{
    public partial class FrmSapInventoryUploadResult : Form
    {

        private static List<LanguageItemModel> LanMessList;//定义语言集
        public FrmSapInventoryUploadResult()
        {
            InitializeComponent();
        }

        #region 共有变量
        private static FrmSapInventoryUploadResult _theSingleton;
        public static void Instance(Form fm)
        {
            if (null == _theSingleton || _theSingleton.IsDisposed)
            {
                _theSingleton = new FrmSapInventoryUploadResult
                {
                    MdiParent = fm,
                    WindowState = FormWindowState.Maximized
                };
                _theSingleton.Show();
            }
            else
            {
                _theSingleton.Activate();
                if (_theSingleton.WindowState == FormWindowState.Minimized)
                    _theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion

        private string _esbAddress = string.Empty;

        private void FrmSapInventoryUploadResult_Load(object sender, EventArgs e)
        {

           
            lstView.Columns.Add("提示信息", 630, HorizontalAlignment.Left);

            dgvData.AutoGenerateColumns = false;

            _esbAddress = DataAccess.QueryEsbInterfaceAddress(FormCover.CurrentFactory, FormCover.InterfaceConnString);

            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion

            IntiDropDownList();

        }

        private void IntiDropDownList()
        {
            this.comboBoxWorkLine.Items.Clear();

            DataTable dt = ProductStorageDAL.GetValueFromInterface("SAP-MES-WORKLINE",FormCover.CurrentFactory);
            if (dt != null && dt.Rows.Count > 1)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                    this.comboBoxWorkLine.Items.Insert(i, dt.Rows[i].ItemArray[0]);
                this.comboBoxWorkLine.SelectedIndex = 0;
            }

        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            if (!rbtDateRange.Checked && !rbtJobNo.Checked)
            {
                ToolsClass.Log("请选择查询条件", "ABNORMAL", lstView);
                return;
            }

            if (string.IsNullOrEmpty(this.comboBoxWorkLine.Text.Trim()))
            {
                ToolsClass.Log("请选择生产线别", "ABNORMAL", lstView);
                return;
            }

            var url = _esbAddress;
            url = url + "QuerySAPInventoryStatus?workshop=" + this.comboBoxWorkLine.Text.Trim();
            if (rbtJobNo.Checked)
            {
                var jobNo = txtJobNo.Text.Trim();
                if (jobNo.Length <= 0)
                {
                    ToolsClass.Log("请输入出货柜号", "ABNORMAL", lstView);
                    return;
                }
               url += "&jobno=" + jobNo;
            
            }
            if (rbtDateRange.Checked)
            {
                if (dptFrom.Value > dptTo.Value)
                {
                    ToolsClass.Log("上传时间从不能大于上传时间到", "ABNORMAL", lstView);
                    return;
                }
                if (dptTo.Value.AddDays(-7) > dptFrom.Value)
                {
                    ToolsClass.Log("查询的时间范围不能大于7天", "ABNORMAL", lstView);
                    return;
                }
                var from = dptFrom.Value.ToString(dptFrom.CustomFormat);
                url += "&datefrom=" + from;
                var to = dptTo.Value.ToString(dptTo.CustomFormat);
                url += "&dateto=" + to;
            }
       
            var req = WebRequest.Create(url);
            ToolsClass.Log("btnQuery_Click::" + url);
            req.Method = "GET";
            try
            {
                //返回 HttpWebResponse
                var hwRes = req.GetResponse() as HttpWebResponse;
                string result;
                if (hwRes == null)
                {
                    dgvData.DataSource = null;
                    ToolsClass.Log("服务器反馈结果为空", "ABNORMAL", lstView);
                    return;
                }
                if (hwRes.StatusCode == HttpStatusCode.OK)
                {
                    //是否返回成功
                    var rStream = hwRes.GetResponseStream();
                    //流读取
                    var sr = new StreamReader(rStream, Encoding.UTF8);
                    result = sr.ReadToEnd();
                    sr.Close();
                    rStream.Close();
                }
                else
                {
                    result = "连接错误";
                }
                //关闭
                hwRes.Close();
                result = result.ToUpper();
                ToolsClass.Log("btnQuery_Click::" + result);
                result = result.Replace(@"<?XML VERSION=""1.0"" ENCODING=""UTF-8""?>", string.Empty);
                result = result.Replace(@"<SOAP:ENVELOPE XMLNS:SOAP=""HTTP://SCHEMAS.XMLSOAP.ORG/SOAP/ENVELOPE/"">", string.Empty);
                result = result.Replace(@"<SOAP:BODY>", @"<SOAPBODY>");
                result = result.Replace(@"</SOAP:BODY>", @"</SOAPBODY>");
                result = result.Replace(@"</SOAP:ENVELOPE>", string.Empty);
                var ds = ProductStorageDAL.ReadXmlToDataSet(result);
                if (ds == null || ds.Tables == null || ds.Tables.Count <= 0 ||
                    ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count <= 0)
                {
                    dgvData.DataSource = null;
                    ToolsClass.Log("服务器反馈结果与接口定义不符", "ABNORMAL", lstView);
                    return;
                }
                var responseStatusTable = ds.Tables["RESPONSESTATUS"];
                if (responseStatusTable == null)
                {
                    dgvData.DataSource = null;
                    ToolsClass.Log("服务器反馈结果与接口定义不符", "ABNORMAL", lstView);
                    return;
                }
                var statusCode = responseStatusTable.Rows[0][0].ToString();
                if (statusCode == "000")
                {
                    var responseContentTable = ds.Tables["ITEM"];
                    if (responseContentTable == null || responseContentTable.Rows == null || responseContentTable.Rows.Count <= 0)
                    {
                        dgvData.DataSource = null;
                        ToolsClass.Log("服务器反馈结果为空", "ABNORMAL", lstView);
                        return;
                    }
                    var sapInventoryUploadResults = new List<SapInventoryUploadResult>();
                    foreach (DataRow row in responseContentTable.Rows)
                    {
                        var sapInventoryUploadResult = new SapInventoryUploadResult
                        {
                            GroupHistKey = row["GUID"].ToString(),
                            JobNo = row["JOB_NO"].ToString(),
                            InnerJobNo = row["INNER_JOB_NO"].ToString(),
                            Workshop = row["WORKSHOP"].ToString(),
                            CreateOn = row["CREATE_ON"].ToString(),
                            ModifiedOn = row["MODIFIED_ON"].ToString(),
                            StatusStep1 = row["STATUS_STEP1"].ToString(),
                            TimeStep1 = row["TIME_STEP1"].ToString(),
                            StatusStep2 = row["STATUS_STEP2"].ToString(),
                            TimeStep2 = row["TIME_STEP2"].ToString(),
                            StatusStep3 = row["STATUS_STEP3"].ToString(),
                            TimeStep3 = row["TIME_STEP3"].ToString(),
                            StatusStep4 = row["STATUS_STEP4"].ToString(),
                            TimeStep4 = row["TIME_STEP4"].ToString(),
                            StatusStep5 = row["STATUS_STEP5"].ToString(),
                            TimeStep5 = row["TIME_STEP5"].ToString(),
                            ErrorMessage = row["ERROR_MESSAGE"].ToString()
                        };
                        sapInventoryUploadResults.Add(sapInventoryUploadResult);
                    }
                    dgvData.DataSource = sapInventoryUploadResults;
                    return;
                }
                if (statusCode == "001")
                {
                    dgvData.DataSource = null;
                    ToolsClass.Log("服务器反馈结果：参数异常", "ABNORMAL", lstView);
                    return;
                }
                if (statusCode == "002")
                {
                    dgvData.DataSource = null;
                    ToolsClass.Log("服务器反馈结果：系统异常", "ABNORMAL", lstView);
                    return;
                }
                dgvData.DataSource = null;
                ToolsClass.Log("服务器反馈结果：StatusCode异常", "ABNORMAL", lstView);
            }
            catch (WebException ex)
            {
                var responseFromServer = ex.Message + " ";
                if (ex.Response != null)
                {
                    using (var response = ex.Response)
                    {
                        var data = response.GetResponseStream();
                        using (var reader = new StreamReader(data))
                        {
                            responseFromServer += reader.ReadToEnd();
                        }
                    }
                }
                dgvData.DataSource = null;
                ToolsClass.Log(responseFromServer, "ABNORMAL", lstView);
                return;
            }
            catch (Exception ex)
            {
                dgvData.DataSource = null;
                ToolsClass.Log(ex.Message, "ABNORMAL", lstView);
                return;
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            dgvData.DataSource = null;
            txtJobNo.Text = string.Empty;
            rbtJobNo.Checked = true;
            rbtDateRange.Checked = false;
            comboBoxWorkLine.Text = "";

        }

        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (lstView.SelectedItems.Count > 0)
                {
                    if (lstView.SelectedItems[0].Text != "")
                        Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
                }
            }
        }

        private void dgvData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var columnIndex = e.ColumnIndex;
            //按钮所在列为第一列，列下标从0开始的  
            if (columnIndex != 0)
                return;

            var sapInventoryUploadResult = dgvData.Rows[e.RowIndex].DataBoundItem as SapInventoryUploadResult;
            if (sapInventoryUploadResult == null)
                return;
            if (sapInventoryUploadResult.IsFinished)
            {
                ToolsClass.Log("已上传成功，无需重试", "NORMAL", lstView);
                return;
            }
            if (!sapInventoryUploadResult.IsError)
            {
                ToolsClass.Log("处理中，无需重试", "NORMAL", lstView);
                return;
            }
            //http://xxx:xx/RetrySAPInventory?guid=?
            var url = _esbAddress + "RetrySAPInventory?guid=" + sapInventoryUploadResult.GroupHistKey;
            var req = WebRequest.Create(url);
            ToolsClass.Log("dgvData_CellContentClick::" + url);
            req.Method = "GET";
            try
            {
                //返回 HttpWebResponse
                var hwRes = req.GetResponse() as HttpWebResponse;
                string result;
                if (hwRes == null)
                {
                    ToolsClass.Log("服务器反馈结果为空", "ABNORMAL", lstView);
                    return;
                }
                if (hwRes.StatusCode == HttpStatusCode.OK)
                {
                    //是否返回成功
                    var rStream = hwRes.GetResponseStream();
                    //流读取
                    var sr = new StreamReader(rStream, Encoding.UTF8);
                    result = sr.ReadToEnd();
                    sr.Close();
                    rStream.Close();
                }
                else
                {
                    result = "连接错误";
                }
                //关闭
                hwRes.Close();
                result = result.ToUpper();
                ToolsClass.Log("dgvData_CellContentClick::" + result);
                result = result.Replace(@"<?XML VERSION=""1.0"" ENCODING=""UTF-8""?>", string.Empty);
                result = result.Replace(@"<SOAP:ENVELOPE XMLNS:SOAP=""HTTP://SCHEMAS.XMLSOAP.ORG/SOAP/ENVELOPE/"">", string.Empty);
                result = result.Replace(@"<SOAP:BODY>", string.Empty);
                result = result.Replace(@"</SOAP:BODY>", string.Empty);
                result = result.Replace(@"</SOAP:ENVELOPE>", string.Empty);
                var ds = ProductStorageDAL.ReadXmlToDataSet(result);
                if (ds == null || ds.Tables == null || ds.Tables.Count <= 0 ||
                    ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count <= 0)
                {
                    ToolsClass.Log("服务器反馈结果与接口定义不符", "ABNORMAL", lstView);
                    return;
                }
                var responseStatusTable = ds.Tables["RESPONSESTATUS"];
                if (responseStatusTable == null)
                {
                    ToolsClass.Log("服务器反馈结果与接口定义不符", "ABNORMAL", lstView);
                    return;
                }
                var statusCode = responseStatusTable.Rows[0][0].ToString();
                if (statusCode == "000")
                {
                    var tsapReceiptUploadJobNo = new TsapReceiptUploadJobNo
                    {
                        Sysid = Guid.NewGuid().ToString("N"),
                        CreatedOn = DT.DateTime().LongDateTime2,
                        CreatedBy = FormCover.CurrUserName,
                        JobNo = sapInventoryUploadResult.JobNo,
                        InnerJobNo = sapInventoryUploadResult.InnerJobNo,
                        GroupHistKey = sapInventoryUploadResult.GroupHistKey,
                        Resv01 = FormCover.CurrentFactory
                    };
                    if (!DataAccess.InsertTsapReceiptUploadJobNo(tsapReceiptUploadJobNo, FormCover.connectionBase))
                    {
                        ToolsClass.Log("重试结果保存失败", "ABNORMAL", lstView);
                        return;
                    }
                    ToolsClass.Log("操作成功", "NORMAL", lstView);
                    return;
                }
                switch (statusCode)
                {
                    case "001":
                        ToolsClass.Log("服务器反馈结果：参数异常", "ABNORMAL", lstView);
                        return;
                    case "002":
                        ToolsClass.Log("服务器反馈结果：系统异常", "ABNORMAL", lstView);
                        return;
                    case "003":
                        ToolsClass.Log("服务器反馈结果：正在处理中", "ABNORMAL", lstView);
                        return;
                    default:
                        ToolsClass.Log("服务器反馈结果StatusCode不正确，当前StatusCode=" + statusCode, "ABNORMAL", lstView);
                        return;
                }
            }
            catch (WebException ex)
            {
                var responseFromServer = ex.Message + " ";
                if (ex.Response != null)
                {
                    using (var response = ex.Response)
                    {
                        var data = response.GetResponseStream();
                        using (var reader = new StreamReader(data))
                        {
                            responseFromServer += reader.ReadToEnd();
                        }
                    }
                }
                ToolsClass.Log(responseFromServer, "ABNORMAL", lstView);
                return;
            }
            catch (Exception ex)
            {
                ToolsClass.Log(ex.Message, "ABNORMAL", lstView);
                return;
            }
        }

        private void rbtJobNo_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtJobNo.Checked)
            {
                gbJobNo.Enabled = true;
                txtJobNo.Text = string.Empty;
                txtJobNo.Focus();
            }
            else
            {
                gbJobNo.Enabled = false;
            }
        }

        private void rbtDateRange_CheckedChanged(object sender, EventArgs e)
        {
            gbTime.Enabled = rbtDateRange.Checked;
        }
    }

    public class SapInventoryUploadResult
    {
        public bool IsFinished { get { return IsSucess1 && IsSucess2 && IsSucess3 && IsSucess4 && IsSucess5; } }
        public bool IsError { get { return IsError1 || IsError2 || IsError3 || IsError4 || IsError5; } }
        public string CommandName { get { return IsFinished ? string.Empty : "重试"; } }
        public string GroupHistKey { get; set; }
        public string JobNo { get; set; }
        public string InnerJobNo { get; set; }
        public string Workshop { get; set; }
        public string CreateOn { get; set; }
        public string ModifiedOn { get; set; }
        public string StatusStep1 { get; set; }
        public bool IsSucess1 { get { return !string.IsNullOrEmpty(StatusStep1) && StatusStep1.Trim().ToLower() == "success"; } }
        public bool IsError1 { get { return !string.IsNullOrEmpty(StatusStep1) && StatusStep1.Trim().ToLower() == "error"; } }
        public string StatusStep1Cn { get { return IsSucess1 ? "成功" : IsError1 ? "失败" : "状态未知"; } }//无上一步
        public string TimeStep1 { get; set; }
        public string StatusStep2 { get; set; }
        public bool IsSucess2 { get { return !string.IsNullOrEmpty(StatusStep2) && StatusStep2.Trim().ToLower() == "success"; } }
        public bool IsError2 { get { return !string.IsNullOrEmpty(StatusStep2) && StatusStep2.Trim().ToLower() == "error"; } }
        public string StatusStep2Cn { get { return IsSucess2 ? "成功" : IsError2 ? "失败" : IsSucess1 ? "处理中" : "未开始"; } }
        public string TimeStep2 { get; set; }
        public string StatusStep3 { get; set; }
        public bool IsSucess3 { get { return !string.IsNullOrEmpty(StatusStep3) && StatusStep3.Trim().ToLower() == "success"; } }
        public bool IsError3 { get { return !string.IsNullOrEmpty(StatusStep3) && StatusStep3.Trim().ToLower() == "error"; } }
        public string StatusStep3Cn { get { return IsSucess3 ? "成功" : IsError3 ? "失败" : IsSucess2 ? "处理中" : "未开始"; } }
        public string TimeStep3 { get; set; }
        public string StatusStep4 { get; set; }
        public bool IsSucess4 { get { return !string.IsNullOrEmpty(StatusStep4) && StatusStep4.Trim().ToLower() == "success"; } }
        public bool IsError4 { get { return !string.IsNullOrEmpty(StatusStep4) && StatusStep4.Trim().ToLower() == "error"; } }
        public string StatusStep4Cn { get { return IsSucess4 ? "成功" : IsError4 ? "失败" : IsSucess3 ? "处理中" : "未开始"; } }
        public string TimeStep4 { get; set; }
        public string StatusStep5 { get; set; }
        public bool IsSucess5 { get { return !string.IsNullOrEmpty(StatusStep5) && StatusStep5.Trim().ToLower() == "success"; } }
        public bool IsError5 { get { return !string.IsNullOrEmpty(StatusStep5) && StatusStep5.Trim().ToLower() == "error"; } }
        public string StatusStep5Cn { get { return IsSucess5 ? "成功" : IsError5 ? "失败" : IsSucess4 ? "处理中" : "未开始"; } }
        public string TimeStep5 { get; set; }
        public string ErrorMessage { get; set; }

        public SapInventoryUploadResult()
        {
            GroupHistKey = string.Empty;
            JobNo = string.Empty;
            InnerJobNo = string.Empty;
            Workshop = string.Empty;
            CreateOn = string.Empty;
            ModifiedOn = string.Empty;
            StatusStep1 = string.Empty;
            TimeStep1 = string.Empty;
            StatusStep2 = string.Empty;
            TimeStep2 = string.Empty;
            StatusStep3 = string.Empty;
            TimeStep3 = string.Empty;
            StatusStep4 = string.Empty;
            TimeStep4 = string.Empty;
            StatusStep5 = string.Empty;
            TimeStep5 = string.Empty;
            ErrorMessage = string.Empty;
        }
    }
}