﻿namespace CSICPR
{
    partial class FormStorageDealReport    
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.RowIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CartonNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Result = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Factory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OperatorDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnQuery = new System.Windows.Forms.Button();
            this.PalCarton = new System.Windows.Forms.Panel();
            this.txtCarton = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ddlQueryType = new System.Windows.Forms.ComboBox();
            this.lblCartonQuery = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.PalDealDate = new System.Windows.Forms.Panel();
            this.lblPackingFrom = new System.Windows.Forms.Label();
            this.dtpStorageDateFrom = new System.Windows.Forms.DateTimePicker();
            this.lblPackingTo = new System.Windows.Forms.Label();
            this.dtpStorageDateTo = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.PalCarton.SuspendLayout();
            this.panel1.SuspendLayout();
            this.PalDealDate.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowDrop = true;
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle86.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle86;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle87.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle87.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle87.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle87.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle87.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle87.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle87.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle87;
            this.dataGridView1.ColumnHeadersHeight = 30;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RowIndex,
            this.CartonNo,
            this.Result,
            this.Factory,
            this.OperatorDate});
            dataGridViewCellStyle89.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle89.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle89.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle89.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle89.SelectionBackColor = System.Drawing.Color.PeachPuff;
            dataGridViewCellStyle89.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle89.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle89;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 92);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle90.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle90.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle90.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle90.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle90.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle90.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle90;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 16;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1004, 480);
            this.dataGridView1.TabIndex = 16;
            // 
            // RowIndex
            // 
            dataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.RowIndex.DefaultCellStyle = dataGridViewCellStyle88;
            this.RowIndex.HeaderText = "序号";
            this.RowIndex.Name = "RowIndex";
            this.RowIndex.ReadOnly = true;
            this.RowIndex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RowIndex.Width = 50;
            // 
            // CartonNo
            // 
            this.CartonNo.HeaderText = "内部托号";
            this.CartonNo.Name = "CartonNo";
            this.CartonNo.ReadOnly = true;
            // 
            // Result
            // 
            this.Result.HeaderText = "处理结果";
            this.Result.Name = "Result";
            this.Result.ReadOnly = true;
            // 
            // Factory
            // 
            this.Factory.HeaderText = "车间";
            this.Factory.Name = "Factory";
            // 
            // OperatorDate
            // 
            this.OperatorDate.HeaderText = "操作时间";
            this.OperatorDate.Name = "OperatorDate";
            this.OperatorDate.ReadOnly = true;
            this.OperatorDate.Width = 160;
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(482, 59);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(65, 22);
            this.btnQuery.TabIndex = 168;
            this.btnQuery.Tag = "button1";
            this.btnQuery.Text = "查询";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // PalCarton
            // 
            this.PalCarton.Controls.Add(this.txtCarton);
            this.PalCarton.Controls.Add(this.label2);
            this.PalCarton.Location = new System.Drawing.Point(29, 54);
            this.PalCarton.Name = "PalCarton";
            this.PalCarton.Size = new System.Drawing.Size(259, 32);
            this.PalCarton.TabIndex = 172;
            // 
            // txtCarton
            // 
            this.txtCarton.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtCarton.Location = new System.Drawing.Point(80, 3);
            this.txtCarton.Name = "txtCarton";
            this.txtCarton.Size = new System.Drawing.Size(148, 21);
            this.txtCarton.TabIndex = 94;
            this.txtCarton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCarton_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 12);
            this.label2.TabIndex = 94;
            this.label2.Text = "内 部 托 号";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ddlQueryType);
            this.panel1.Controls.Add(this.lblCartonQuery);
            this.panel1.Controls.Add(this.PalDealDate);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.btnQuery);
            this.panel1.Controls.Add(this.PalCarton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1004, 92);
            this.panel1.TabIndex = 174;
            // 
            // ddlQueryType
            // 
            this.ddlQueryType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlQueryType.FormattingEnabled = true;
            this.ddlQueryType.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ddlQueryType.Location = new System.Drawing.Point(93, 14);
            this.ddlQueryType.Name = "ddlQueryType";
            this.ddlQueryType.Size = new System.Drawing.Size(148, 20);
            this.ddlQueryType.TabIndex = 179;
            this.ddlQueryType.SelectedIndexChanged += new System.EventHandler(this.ddlQueryType_SelectedIndexChanged);
            // 
            // lblCartonQuery
            // 
            this.lblCartonQuery.AutoSize = true;
            this.lblCartonQuery.Location = new System.Drawing.Point(18, 17);
            this.lblCartonQuery.Name = "lblCartonQuery";
            this.lblCartonQuery.Size = new System.Drawing.Size(71, 12);
            this.lblCartonQuery.TabIndex = 180;
            this.lblCartonQuery.Text = "查 询 类 型";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(564, 59);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(65, 22);
            this.button2.TabIndex = 178;
            this.button2.Tag = "button1";
            this.button2.Text = "清空";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // PalDealDate
            // 
            this.PalDealDate.Controls.Add(this.lblPackingFrom);
            this.PalDealDate.Controls.Add(this.dtpStorageDateFrom);
            this.PalDealDate.Controls.Add(this.lblPackingTo);
            this.PalDealDate.Controls.Add(this.dtpStorageDateTo);
            this.PalDealDate.Location = new System.Drawing.Point(29, 54);
            this.PalDealDate.Name = "PalDealDate";
            this.PalDealDate.Size = new System.Drawing.Size(261, 36);
            this.PalDealDate.TabIndex = 181;
            // 
            // lblPackingFrom
            // 
            this.lblPackingFrom.AutoSize = true;
            this.lblPackingFrom.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPackingFrom.Location = new System.Drawing.Point(2, 6);
            this.lblPackingFrom.Name = "lblPackingFrom";
            this.lblPackingFrom.Size = new System.Drawing.Size(21, 14);
            this.lblPackingFrom.TabIndex = 0;
            this.lblPackingFrom.Text = "从";
            // 
            // dtpStorageDateFrom
            // 
            this.dtpStorageDateFrom.CustomFormat = "yyyy-MM-dd";
            this.dtpStorageDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStorageDateFrom.Location = new System.Drawing.Point(24, 3);
            this.dtpStorageDateFrom.Name = "dtpStorageDateFrom";
            this.dtpStorageDateFrom.Size = new System.Drawing.Size(100, 21);
            this.dtpStorageDateFrom.TabIndex = 5;
            // 
            // lblPackingTo
            // 
            this.lblPackingTo.AutoSize = true;
            this.lblPackingTo.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblPackingTo.Location = new System.Drawing.Point(133, 7);
            this.lblPackingTo.Name = "lblPackingTo";
            this.lblPackingTo.Size = new System.Drawing.Size(21, 14);
            this.lblPackingTo.TabIndex = 6;
            this.lblPackingTo.Text = "到";
            // 
            // dtpStorageDateTo
            // 
            this.dtpStorageDateTo.CustomFormat = "yyyy-MM-dd";
            this.dtpStorageDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStorageDateTo.Location = new System.Drawing.Point(156, 4);
            this.dtpStorageDateTo.Name = "dtpStorageDateTo";
            this.dtpStorageDateTo.Size = new System.Drawing.Size(104, 21);
            this.dtpStorageDateTo.TabIndex = 7;
            // 
            // FormStorageDealReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 572);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormStorageDealReport";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "FRM0002";
            this.Text = "入库数据处理结果查询";
            this.Load += new System.EventHandler(this.FormStorageDealReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.PalCarton.ResumeLayout(false);
            this.PalCarton.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.PalDealDate.ResumeLayout(false);
            this.PalDealDate.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Panel PalCarton;
        private System.Windows.Forms.TextBox txtCarton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox ddlQueryType;
        private System.Windows.Forms.Label lblCartonQuery;
        private System.Windows.Forms.Panel PalDealDate;
        private System.Windows.Forms.Label lblPackingFrom;
        private System.Windows.Forms.DateTimePicker dtpStorageDateFrom;
        private System.Windows.Forms.Label lblPackingTo;
        private System.Windows.Forms.DateTimePicker dtpStorageDateTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn CartonNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Result;
        private System.Windows.Forms.DataGridViewTextBoxColumn Factory;
        private System.Windows.Forms.DataGridViewTextBoxColumn OperatorDate;
    }
}