﻿namespace CSICPR
{
    partial class FormWoData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.splitContainer1 = new System.Windows.Forms.SplitContainer();
        	this.groupBox1 = new System.Windows.Forms.GroupBox();
        	this.label9 = new System.Windows.Forms.Label();
        	this.txtOldInputQty = new System.Windows.Forms.TextBox();
        	this.txtOldWOScrapQty = new System.Windows.Forms.TextBox();
        	this.label8 = new System.Windows.Forms.Label();
        	this.txtWaitStorageQTT = new System.Windows.Forms.TextBox();
        	this.label7 = new System.Windows.Forms.Label();
        	this.txtWoStorageQty = new System.Windows.Forms.TextBox();
        	this.label6 = new System.Windows.Forms.Label();
        	this.label2 = new System.Windows.Forms.Label();
        	this.txtWOScrapQty = new System.Windows.Forms.TextBox();
        	this.txtWONumber = new System.Windows.Forms.TextBox();
        	this.label5 = new System.Windows.Forms.Label();
        	this.label3 = new System.Windows.Forms.Label();
        	this.txtInputQty = new System.Windows.Forms.TextBox();
        	this.txtWOTargetQty = new System.Windows.Forms.TextBox();
        	this.label4 = new System.Windows.Forms.Label();
        	this.PlWo = new System.Windows.Forms.Panel();
        	this.btnReset = new System.Windows.Forms.Button();
        	this.btnCancel = new System.Windows.Forms.Button();
        	this.btnSave = new System.Windows.Forms.Button();
        	this.btnModify = new System.Windows.Forms.Button();
        	this.btnQuery = new System.Windows.Forms.Button();
        	this.txtWoOrder = new System.Windows.Forms.TextBox();
        	this.label1 = new System.Windows.Forms.Label();
        	this.panel2 = new System.Windows.Forms.Panel();
        	((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
        	this.splitContainer1.Panel1.SuspendLayout();
        	this.splitContainer1.SuspendLayout();
        	this.groupBox1.SuspendLayout();
        	this.PlWo.SuspendLayout();
        	this.panel2.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// splitContainer1
        	// 
        	this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
        	this.splitContainer1.IsSplitterFixed = true;
        	this.splitContainer1.Location = new System.Drawing.Point(3, 71);
        	this.splitContainer1.Name = "splitContainer1";
        	this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
        	// 
        	// splitContainer1.Panel1
        	// 
        	this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
        	this.splitContainer1.Panel2Collapsed = true;
        	this.splitContainer1.Size = new System.Drawing.Size(866, 535);
        	this.splitContainer1.SplitterDistance = 394;
        	this.splitContainer1.SplitterWidth = 2;
        	this.splitContainer1.TabIndex = 1;
        	// 
        	// groupBox1
        	// 
        	this.groupBox1.Controls.Add(this.label9);
        	this.groupBox1.Controls.Add(this.txtOldInputQty);
        	this.groupBox1.Controls.Add(this.txtOldWOScrapQty);
        	this.groupBox1.Controls.Add(this.label8);
        	this.groupBox1.Controls.Add(this.txtWaitStorageQTT);
        	this.groupBox1.Controls.Add(this.label7);
        	this.groupBox1.Controls.Add(this.txtWoStorageQty);
        	this.groupBox1.Controls.Add(this.label6);
        	this.groupBox1.Controls.Add(this.label2);
        	this.groupBox1.Controls.Add(this.txtWOScrapQty);
        	this.groupBox1.Controls.Add(this.txtWONumber);
        	this.groupBox1.Controls.Add(this.label5);
        	this.groupBox1.Controls.Add(this.label3);
        	this.groupBox1.Controls.Add(this.txtInputQty);
        	this.groupBox1.Controls.Add(this.txtWOTargetQty);
        	this.groupBox1.Controls.Add(this.label4);
        	this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.groupBox1.Location = new System.Drawing.Point(0, 0);
        	this.groupBox1.Name = "groupBox1";
        	this.groupBox1.Size = new System.Drawing.Size(866, 535);
        	this.groupBox1.TabIndex = 8;
        	this.groupBox1.TabStop = false;
        	// 
        	// label9
        	// 
        	this.label9.AutoSize = true;
        	this.label9.ForeColor = System.Drawing.Color.Blue;
        	this.label9.Location = new System.Drawing.Point(294, 167);
        	this.label9.Name = "label9";
        	this.label9.Size = new System.Drawing.Size(91, 13);
        	this.label9.TabIndex = 15;
        	this.label9.Text = "上次实际投产量";
        	// 
        	// txtOldInputQty
        	// 
        	this.txtOldInputQty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
        	this.txtOldInputQty.Enabled = false;
        	this.txtOldInputQty.ForeColor = System.Drawing.Color.Red;
        	this.txtOldInputQty.Location = new System.Drawing.Point(428, 161);
        	this.txtOldInputQty.Name = "txtOldInputQty";
        	this.txtOldInputQty.Size = new System.Drawing.Size(138, 20);
        	this.txtOldInputQty.TabIndex = 14;
        	// 
        	// txtOldWOScrapQty
        	// 
        	this.txtOldWOScrapQty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
        	this.txtOldWOScrapQty.Enabled = false;
        	this.txtOldWOScrapQty.ForeColor = System.Drawing.Color.Red;
        	this.txtOldWOScrapQty.Location = new System.Drawing.Point(131, 164);
        	this.txtOldWOScrapQty.Name = "txtOldWOScrapQty";
        	this.txtOldWOScrapQty.Size = new System.Drawing.Size(162, 20);
        	this.txtOldWOScrapQty.TabIndex = 13;
        	// 
        	// label8
        	// 
        	this.label8.AutoSize = true;
        	this.label8.ForeColor = System.Drawing.Color.Blue;
        	this.label8.Location = new System.Drawing.Point(22, 167);
        	this.label8.Name = "label8";
        	this.label8.Size = new System.Drawing.Size(67, 13);
        	this.label8.TabIndex = 12;
        	this.label8.Text = "上次报废量";
        	// 
        	// txtWaitStorageQTT
        	// 
        	this.txtWaitStorageQTT.Enabled = false;
        	this.txtWaitStorageQTT.Location = new System.Drawing.Point(699, 104);
        	this.txtWaitStorageQTT.Name = "txtWaitStorageQTT";
        	this.txtWaitStorageQTT.Size = new System.Drawing.Size(138, 20);
        	this.txtWaitStorageQTT.TabIndex = 11;
        	// 
        	// label7
        	// 
        	this.label7.AutoSize = true;
        	this.label7.Location = new System.Drawing.Point(572, 107);
        	this.label7.Name = "label7";
        	this.label7.Size = new System.Drawing.Size(91, 13);
        	this.label7.TabIndex = 10;
        	this.label7.Text = "工单待入库数量";
        	// 
        	// txtWoStorageQty
        	// 
        	this.txtWoStorageQty.Enabled = false;
        	this.txtWoStorageQty.Location = new System.Drawing.Point(700, 45);
        	this.txtWoStorageQty.Name = "txtWoStorageQty";
        	this.txtWoStorageQty.Size = new System.Drawing.Size(138, 20);
        	this.txtWoStorageQty.TabIndex = 9;
        	// 
        	// label6
        	// 
        	this.label6.AutoSize = true;
        	this.label6.Location = new System.Drawing.Point(572, 49);
        	this.label6.Name = "label6";
        	this.label6.Size = new System.Drawing.Size(79, 13);
        	this.label6.TabIndex = 8;
        	this.label6.Text = "工单已入库量";
        	// 
        	// label2
        	// 
        	this.label2.AutoSize = true;
        	this.label2.Location = new System.Drawing.Point(20, 52);
        	this.label2.Name = "label2";
        	this.label2.Size = new System.Drawing.Size(64, 13);
        	this.label2.TabIndex = 0;
        	this.label2.Text = "工 单 号 码";
        	// 
        	// txtWOScrapQty
        	// 
        	this.txtWOScrapQty.Enabled = false;
        	this.txtWOScrapQty.Location = new System.Drawing.Point(131, 109);
        	this.txtWOScrapQty.Name = "txtWOScrapQty";
        	this.txtWOScrapQty.Size = new System.Drawing.Size(162, 20);
        	this.txtWOScrapQty.TabIndex = 7;
        	this.txtWOScrapQty.TextChanged += new System.EventHandler(this.txtWOScrapQty_TextChanged);
        	this.txtWOScrapQty.Leave += new System.EventHandler(this.txtWOScrapQty_Leave);
        	// 
        	// txtWONumber
        	// 
        	this.txtWONumber.Enabled = false;
        	this.txtWONumber.Location = new System.Drawing.Point(131, 49);
        	this.txtWONumber.Name = "txtWONumber";
        	this.txtWONumber.Size = new System.Drawing.Size(162, 20);
        	this.txtWONumber.TabIndex = 1;
        	// 
        	// label5
        	// 
        	this.label5.AutoSize = true;
        	this.label5.Location = new System.Drawing.Point(22, 111);
        	this.label5.Name = "label5";
        	this.label5.Size = new System.Drawing.Size(67, 13);
        	this.label5.TabIndex = 6;
        	this.label5.Text = "工单报废量";
        	// 
        	// label3
        	// 
        	this.label3.AutoSize = true;
        	this.label3.Location = new System.Drawing.Point(295, 52);
        	this.label3.Name = "label3";
        	this.label3.Size = new System.Drawing.Size(79, 13);
        	this.label3.TabIndex = 2;
        	this.label3.Text = "工单  计 划 量";
        	// 
        	// txtInputQty
        	// 
        	this.txtInputQty.Enabled = false;
        	this.txtInputQty.Location = new System.Drawing.Point(428, 106);
        	this.txtInputQty.Name = "txtInputQty";
        	this.txtInputQty.Size = new System.Drawing.Size(138, 20);
        	this.txtInputQty.TabIndex = 5;
        	this.txtInputQty.TextChanged += new System.EventHandler(this.txtInputQty_TextChanged);
        	this.txtInputQty.Leave += new System.EventHandler(this.txtInputQty_Leave);
        	// 
        	// txtWOTargetQty
        	// 
        	this.txtWOTargetQty.Enabled = false;
        	this.txtWOTargetQty.Location = new System.Drawing.Point(428, 46);
        	this.txtWOTargetQty.Name = "txtWOTargetQty";
        	this.txtWOTargetQty.Size = new System.Drawing.Size(138, 20);
        	this.txtWOTargetQty.TabIndex = 3;
        	// 
        	// label4
        	// 
        	this.label4.AutoSize = true;
        	this.label4.Location = new System.Drawing.Point(294, 113);
        	this.label4.Name = "label4";
        	this.label4.Size = new System.Drawing.Size(91, 13);
        	this.label4.TabIndex = 4;
        	this.label4.Text = "工单实际投产量";
        	// 
        	// PlWo
        	// 
        	this.PlWo.Controls.Add(this.btnReset);
        	this.PlWo.Controls.Add(this.btnCancel);
        	this.PlWo.Controls.Add(this.btnSave);
        	this.PlWo.Controls.Add(this.btnModify);
        	this.PlWo.Controls.Add(this.btnQuery);
        	this.PlWo.Controls.Add(this.txtWoOrder);
        	this.PlWo.Controls.Add(this.label1);
        	this.PlWo.Location = new System.Drawing.Point(12, 30);
        	this.PlWo.Name = "PlWo";
        	this.PlWo.Size = new System.Drawing.Size(826, 34);
        	this.PlWo.TabIndex = 49;
        	// 
        	// btnReset
        	// 
        	this.btnReset.Location = new System.Drawing.Point(397, 1);
        	this.btnReset.Name = "btnReset";
        	this.btnReset.Size = new System.Drawing.Size(87, 25);
        	this.btnReset.TabIndex = 57;
        	this.btnReset.Text = "重置";
        	this.btnReset.UseVisualStyleBackColor = true;
        	this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
        	// 
        	// btnCancel
        	// 
        	this.btnCancel.Location = new System.Drawing.Point(609, 1);
        	this.btnCancel.Name = "btnCancel";
        	this.btnCancel.Size = new System.Drawing.Size(87, 25);
        	this.btnCancel.TabIndex = 56;
        	this.btnCancel.Text = "取消";
        	this.btnCancel.UseVisualStyleBackColor = true;
        	this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
        	// 
        	// btnSave
        	// 
        	this.btnSave.Location = new System.Drawing.Point(720, 1);
        	this.btnSave.Name = "btnSave";
        	this.btnSave.Size = new System.Drawing.Size(87, 25);
        	this.btnSave.TabIndex = 55;
        	this.btnSave.Text = "保存";
        	this.btnSave.UseVisualStyleBackColor = true;
        	this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
        	// 
        	// btnModify
        	// 
        	this.btnModify.Location = new System.Drawing.Point(501, 1);
        	this.btnModify.Name = "btnModify";
        	this.btnModify.Size = new System.Drawing.Size(87, 25);
        	this.btnModify.TabIndex = 54;
        	this.btnModify.Text = "修改";
        	this.btnModify.UseVisualStyleBackColor = true;
        	this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
        	// 
        	// btnQuery
        	// 
        	this.btnQuery.Location = new System.Drawing.Point(294, 1);
        	this.btnQuery.Name = "btnQuery";
        	this.btnQuery.Size = new System.Drawing.Size(87, 25);
        	this.btnQuery.TabIndex = 53;
        	this.btnQuery.Text = "查询";
        	this.btnQuery.UseVisualStyleBackColor = true;
        	this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
        	// 
        	// txtWoOrder
        	// 
        	this.txtWoOrder.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.txtWoOrder.Location = new System.Drawing.Point(125, 2);
        	this.txtWoOrder.MaxLength = 24;
        	this.txtWoOrder.Name = "txtWoOrder";
        	this.txtWoOrder.Size = new System.Drawing.Size(162, 20);
        	this.txtWoOrder.TabIndex = 52;
        	this.txtWoOrder.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWoOrder_KeyPress);
        	// 
        	// label1
        	// 
        	this.label1.AutoSize = true;
        	this.label1.Location = new System.Drawing.Point(10, 7);
        	this.label1.Name = "label1";
        	this.label1.Size = new System.Drawing.Size(64, 13);
        	this.label1.TabIndex = 6;
        	this.label1.Text = "工 单 号 码";
        	// 
        	// panel2
        	// 
        	this.panel2.Controls.Add(this.PlWo);
        	this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
        	this.panel2.Location = new System.Drawing.Point(3, 3);
        	this.panel2.Name = "panel2";
        	this.panel2.Size = new System.Drawing.Size(866, 68);
        	this.panel2.TabIndex = 0;
        	// 
        	// FormWoData
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(872, 609);
        	this.Controls.Add(this.splitContainer1);
        	this.Controls.Add(this.panel2);
        	this.Name = "FormWoData";
        	this.Padding = new System.Windows.Forms.Padding(3);
        	this.ShowInTaskbar = false;
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "工单数据维护";
        	this.Load += new System.EventHandler(this.FormWoData_Load);
        	this.splitContainer1.Panel1.ResumeLayout(false);
        	((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
        	this.splitContainer1.ResumeLayout(false);
        	this.groupBox1.ResumeLayout(false);
        	this.groupBox1.PerformLayout();
        	this.PlWo.ResumeLayout(false);
        	this.PlWo.PerformLayout();
        	this.panel2.ResumeLayout(false);
        	this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtOldInputQty;
        private System.Windows.Forms.TextBox txtOldWOScrapQty;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtWaitStorageQTT;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtWoStorageQty;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtWOScrapQty;
        private System.Windows.Forms.TextBox txtWONumber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtInputQty;
        private System.Windows.Forms.TextBox txtWOTargetQty;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel PlWo;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.TextBox txtWoOrder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
    }
}