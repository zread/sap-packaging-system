﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 4/13/2016
 * Time: 5:40 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace CSICPR
{
	/// <summary>
	/// Description of FormQueryUploadStatus.
	/// </summary>
	public partial class FormQueryUploadStatus : Form
	{
		public FormQueryUploadStatus()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		 private static FormQueryUploadStatus _theSingleton;
		 
		 public static void Instance(Form fm)
        {
            if (null == _theSingleton || _theSingleton.IsDisposed)
            {
                _theSingleton = new FormQueryUploadStatus
                {
                    MdiParent = fm,
                    WindowState = FormWindowState.Maximized
                };
                _theSingleton.Show();
            }
            else
            {
                _theSingleton.Activate();
                if (_theSingleton.WindowState == FormWindowState.Minimized)
                    _theSingleton.WindowState = FormWindowState.Maximized;
            }
        }
		 
		 
        
		
		void BtQueryClick(object sender, EventArgs e)
        {
			dataGridView1.Rows.Clear();
			List<string> T = DataAccess.QueryEsbInterfaceAddressUserIdPW(FormCover.CurrentFactory, FormCover.InterfaceConnString);
        	string host = T[0].Substring(7,12);
        	string user = T[1];
        	string password = T[2];
        	string dbcon = T[3];
			
			
			string strTemp = TbCarton.Text.ToString();
                string[] sDataSet = strTemp.Split('\n');
                 for (int i = 0; i < sDataSet.Length; i++)
                {
	                    sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
	                    if (sDataSet[i] != null && sDataSet[i] != "")
	                    {
	                    	 string sql = " Select status, process_flag from stockin where carton_no = '{0}' and status = 'S' order by Last_update_date desc limit 1";
	                    	 sql = string.Format(sql,sDataSet[i]);
	                    	 
	                    	 
					            MySqlConnection con = new MySqlConnection("host="+ host +";user= "+ user +";password="+password+";database="+dbcon+";");
					            MySqlCommand cmd = new MySqlCommand(sql, con);
					
					            con.Open();
					
					           MySqlDataReader  reader = cmd.ExecuteReader();
							
					           if(reader!= null &&reader.Read()) 
					           {
					           	dataGridView1.Rows.Insert(dataGridView1.Rows.Count, new object[] { sDataSet[i] ,reader.GetString(0), reader.GetString(1) });
								}
					           else
					           {
					           	sql = " Select status, process_flag from stockin where carton_no = '{0}'  order by Process_flag desc limit 1";
	                    		sql = string.Format(sql,sDataSet[i]);
					           }

	                    
	                    }
                }
        
		}
	}
}
