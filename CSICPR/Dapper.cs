﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using MySql.Data.MySqlClient;

namespace CSICPR
{
    public class Dapper
    {
        internal static bool Save<T>(T t, string sql, SqlConnection conn, SqlTransaction trans)
        {
            var result = conn.Execute(sql, t, trans, 30, CommandType.Text);

            return result > 0;
        }

        internal static bool Save<T>(T t, string sql, string connectionString)
        {
            int result;

            using (var conn = new SqlConnection(connectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                using (var trans = conn.BeginTransaction())
                {
                    result = conn.Execute(sql, t, trans, 30, CommandType.Text);
                    if (result <= 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }

                if (conn.State != ConnectionState.Closed)
                    conn.Close();
            }

            return result > 0;
        }

        internal static bool Save<T>(List<T> list, string sql, string connectionString)
        {
            var result = -1;

            using (var conn = new SqlConnection(connectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                using (var trans = conn.BeginTransaction())
                {
                    foreach (var item in list)
                    {
                        result = conn.Execute(sql, item, trans, 30, CommandType.Text);
                        if (result <= 0)
                            trans.Rollback();
                    }

                    trans.Commit();
                }

                if (conn.State != ConnectionState.Closed)
                    conn.Close();
            }

            return result > 0;
        }

        internal static T QuerySingle<T>(string sql, object param, string connectionString)
        {
            T t;

            using (var conn = new SqlConnection(connectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                t = conn.Query<T>(sql, param).SingleOrDefault();

                if (conn.State != ConnectionState.Closed)
                    conn.Close();
            }

            return t;
        }

        internal static List<T> Query<T>(string sql, object param, string connectionString)
        {
            List<T> list;

            using (var conn = new SqlConnection(connectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                list = conn.Query<T>(sql, param).ToList();

                if (conn.State != ConnectionState.Closed)
                    conn.Close();
            }

            return list;
        }

        internal static List<T> QueryInMySql<T>(string sql, object param, string connectionString)
        {
            List<T> list;

            using (var conn = new MySqlConnection(connectionString))           
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                list = conn.Query<T>(sql, param).ToList();

                if (conn.State != ConnectionState.Closed)
                    conn.Close();
            }

            return list;
        }
        internal static List<T> QueryInSql<T>(string sql, object param, string connectionString)
        {
            List<T> list;

            //using (var conn = new MySqlConnection(connectionString))
            using(SqlConnection conn = new SqlConnection(connectionString))
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();

                list = conn.Query<T>(sql, param).ToList();

                if (conn.State != ConnectionState.Closed)
                    conn.Close();
            }

            return list;
        }
    }
}