﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace CSICPR
{
    public partial class FormCartonInfoQuery : Form
    {
        #region 私有变量
        private static List<LanguageItemModel> LanMessList;//定义语言集
        private string CartonNo = "";
        private string QueryType = "";
        public DataTable CartonList = new DataTable();
        private DateTime PackingDateFrom;
        private DateTime PackingDateTo;
        private int StorageCount = 0;
        #endregion

        #region 私有方法
        private void ClearDataGridViewData()
        {
            if (this.dataGridView1.Rows.Count > 0)
                this.dataGridView1.Rows.Clear();
        }
        private void SetDataGridViewData(string type)
        {
            if (QueryType.Equals("Carton"))
            {
                if (Convert.ToString(this.txtCarton.Text.Trim()).Equals(""))
                {
                    this.txtCarton.Focus();
                    return;
                }
            }
            else if (QueryType.Equals("Counter"))
            {
                if (Convert.ToString(this.txtConter.Text.Trim()).Equals(""))
                {
                    this.txtConter.Focus();
                    return;
                }
            }
            else
            {
                PackingDateFrom = Convert.ToDateTime(this.dtpPackingDateFrom.Value);
                PackingDateTo = Convert.ToDateTime(this.dtpPackingDateTo.Value);
            }

            DataTable dt = ProductStorageDAL.GetCartonStorageInfo(QueryType, this.txtCarton.Text.Trim().ToString(), PackingDateFrom, PackingDateTo, this.txtConter.Text.Trim().ToString());
            if (dt != null && dt.Rows.Count > 0)
            {
                int RowNo = 0;
                int RowIndex = 1;
                foreach (DataRow row in dt.Rows)
                {
                    this.dataGridView1.Rows.Add();
                    this.dataGridView1.Rows[RowNo].Cells["Selected"].Value = false;
                    this.dataGridView1.Rows[RowNo].Cells["RowIndex"].Value = RowIndex.ToString();
                    if (Convert.ToString(row["CartonStatus"]).Equals("0"))
                        this.dataGridView1.Rows[RowNo].Cells["CartonStatus"].Value = "After Passon";
                    else if (Convert.ToString(row["CartonStatus"]).Equals("1"))
                        this.dataGridView1.Rows[RowNo].Cells["CartonStatus"].Value = "Packed";
                    else if (Convert.ToString(row["CartonStatus"]).Equals("2"))
                        this.dataGridView1.Rows[RowNo].Cells["CartonStatus"].Value = "Stocked IN";
                    else
                        this.dataGridView1.Rows[RowNo].Cells["CartonStatus"].Value = Convert.ToString(row["CartonStatus"]);
                    this.dataGridView1.Rows[RowNo].Cells["CartonID"].Value = Convert.ToString(row["CartonID"]);
                    this.dataGridView1.Rows[RowNo].Cells["SNQTY"].Value = Convert.ToString(row["SNQTY"]);
                    this.dataGridView1.Rows[RowNo].Cells["Cust_BoxID"].Value = Convert.ToString(row["Cust_BoxID"]);
                    RowNo++;
                    RowIndex++;
                }
            }
            else
            {
                if (type.Equals(""))
                {
                    MessageBox.Show("No record found", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (QueryType.Equals("Carton"))
                    {
                        this.txtCarton.SelectAll();
                        this.txtCarton.Focus();
                    }
                    else if (QueryType.Equals("Counter"))
                    {
                        this.txtConter.SelectAll();
                        this.txtConter.Focus();
                    }
                    else
                    {
                        this.dtpPackingDateFrom.Focus();
                    }
                }
            }

            if (QueryType.Equals("Carton"))
            {
                this.txtCarton.SelectAll();
                this.txtCarton.Focus();
            }
            else if (QueryType.Equals("Counter"))
            {
                this.txtConter.SelectAll();
                this.txtConter.Focus();
            }
            else
            {
                this.dtpPackingDateFrom.Focus();
            }
        }
        #endregion

        #region 窗体事件
        public FormCartonInfoQuery()
        {
            InitializeComponent();
        }

        public FormCartonInfoQuery(string Carton)
        {
            InitializeComponent();
            CartonNo = Carton;
        }

        private void FormCartonInfoQuery_Load(object sender, EventArgs e)
        {
          
            this.Text = "cartonno";
            //查询类型
//            this.ddlQueryType.Items.Insert(0, "包装日期");
			this.ddlQueryType.Items.Insert(0, "Packing Date");
            this.ddlQueryType.Items.Insert(1, "内部托号");
            this.ddlQueryType.Items.Insert(2, "货柜号");
            
            this.ddlQueryType.SelectedIndex = 1;
            this.PalPackingDate.Visible = false;
            this.PalCarton.Visible = true;
            this.txtCarton.Text = CartonNo;
            this.txtCarton.SelectAll();
            this.txtCarton.Focus();
            
            CartonList.Columns.Add("CartonID");
            CartonList.Columns.Add("CartonStatus");
            CartonList.Columns.Add("SNQTY");
            CartonList.Columns.Add("Cust_BoxID");

            if (!CartonNo.Equals(""))
                SetDataGridViewData("First");

            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            LanguageHelper.GetCombomBox(this, this.ddlQueryType);
            # endregion

        }

        private void FormCartonInfoQuery_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }
        #endregion

        #region 窗体页面事件
        private void ddlQueryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlQueryType.SelectedIndex == 0)
            {
                this.PalPackingDate.Visible = true;
                this.PalCarton.Visible = false;
                this.PalConter.Visible = false;
                this.dtpPackingDateFrom.Focus();
                QueryType = "PackingDate";
            }
            else if (this.ddlQueryType.SelectedIndex == 1)
            {
                this.PalPackingDate.Visible = false;
                this.PalCarton.Visible = true;
                this.PalConter.Visible = false;
                this.txtCarton.Clear();
                this.txtCarton.Focus();
                QueryType = "Carton";
            }
            else
            {
                this.PalPackingDate.Visible = false;
                this.PalCarton.Visible = false;
                this.PalConter.Visible = true;
                this.txtConter.Clear();
                this.txtConter.Focus();
                QueryType = "Counter";
            }
            ClearDataGridViewData();
        }
        
        private void chbSelected_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
            {
                dgvRow.Cells[0].Value = this.chbSelected.Checked;
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.Rows.Count < 1)
            {
                MessageBox.Show("No data selected, Please confirm!", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (CartonList.Rows.Count > 0)
                    CartonList.Rows.Clear();

                StorageCount = 0;
                foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
                {
                    if (dgvRow.Cells[0].Value.ToString().ToUpper() == "TRUE")
                    {
                        StorageCount = StorageCount + 1;
                        DataRow row = CartonList.NewRow();
                        row["CartonID"] = dgvRow.Cells["CartonID"].Value;
                        row["CartonStatus"] = dgvRow.Cells["CartonStatus"].Value;
                        row["SNQTY"] = dgvRow.Cells["SNQTY"].Value;
                        row["Cust_BoxID"] = dgvRow.Cells["Cust_BoxID"].Value;
                        CartonList.Rows.Add(row);
                    }
                }

                if (StorageCount == 0)
                {
                    //MessageBox.Show("没有选中数据,请确认!", "信息提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    MessageBox.Show("No data selected, Please confirm!", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (MessageBox.Show("Number of " + StorageCount + "Record selected, Please confirm？", "Carton selection", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                {
                    if (CartonList.Rows.Count > 0)
                        CartonList.Rows.Clear();
                    foreach (DataGridViewRow dgvRow in this.dataGridView1.Rows)
                    {
                        dgvRow.Cells[0].Value = false;
                    }
                    this.chbSelected.Checked = false;
                }
                else
                {
                    this.Close();
                }
            }
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            this.chbSelected.Checked = false;
            ClearDataGridViewData();
            SetDataGridViewData("");
        }
        #endregion

        private void txtConter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                btnQuery_Click(null,null);
        }

        private void txtCarton_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                btnQuery_Click(null,null);
        }
            
    }
}
