﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;

namespace CSICPR
{
    /// <summary>
    /// 常熟组件厂箱号生成
    /// V2.2
    /// </summary>
    class clsCSGenerateCarton
    {
        private static object lockGenerateBoxID;
        private string sFirstCode;
        private string sThirdCode;
        private string sSerialCode;
        private int iSerialCount;

        public clsCSGenerateCarton()
        {
        }

        public clsCSGenerateCarton(string sFirstCode, string sSerialCode, string sThirdCode, int iSerialCount)
        {
            this.sFirstCode = sFirstCode;
            this.sSerialCode = sSerialCode;
            this.sThirdCode = sThirdCode;
            this.iSerialCount = iSerialCount;
        }

        /// <summary>
        /// 依据箱号规则，生成箱号
        /// </summary>
        /// <param name="sFirstCode"></param>
        /// <param name="sSerialCode"></param>
        /// <param name="sThirdCode"></param>
        /// <param name="iSerialCount"></param>
        /// <returns></returns>
        public static string getBoxID(string sFirstCode, string sSerialCode, string sThirdCode, int iSerialCount,string schuco="")
        {
            string sboxid = "", strsql = "select * from Box where BoxID='{0}' and IsUsed<>'N'";
            bool isUse = true;
            string sInitSerial = ToolsClass.getConfig("SerialCode", true, "initCnt").Trim();
            int iSerialCode = int.Parse(sInitSerial);

            int iSerial = 0;
            if(schuco!=null && schuco.Equals("SCH"))
                iSerial = int.Parse(ToolsClass.getConfig("SchSerialCode"));
            else
                iSerial = int.Parse(ToolsClass.getConfig("SerialCode"));
          
            lockGenerateBoxID = new object();
            if (iSerialCount == 0)
            {
                iSerialCount = int.Parse("9".PadLeft(sInitSerial.Length, '9')) - iSerialCode;
            }
            while (isUse)
            {
                if (iSerial - iSerialCode > iSerialCount)
                {
                    MessageBox.Show("箱号已经使用完，请维护新的箱号!", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return "";
                }

                DataTable dt = ProductStorageDAL.GetPackingSystemCartonNoRuleFlag();
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (schuco != null && schuco.Equals("SCH"))
                        sboxid = sFirstCode + sThirdCode + ProductStorageDAL.GetServiceDate().ToString("MM") + iSerial.ToString().PadLeft(5, '0');
                    else
                        sboxid = sFirstCode + sThirdCode + ProductStorageDAL.GetServiceDate().ToString("MM") + iSerial.ToString().PadLeft(5, '0');
                }
                else
                {
                    if (schuco != null && schuco.Equals("SCH"))
                        sboxid = sFirstCode + sThirdCode  + iSerial.ToString().PadLeft(5, '0');
                    else
                        sboxid = sFirstCode + sThirdCode + iSerial.ToString().PadLeft(5, '0');
                }

                if (ToolsClass.ExecuteQuery(string.Format(strsql, sboxid)) > 0)
                    isUse = true;
                else
                    isUse = false;
                iSerial++;
            }
            if (schuco != null && schuco.Equals("SCH"))
                ToolsClass.saveXMLNode("SchSerialCode", iSerial.ToString().PadLeft(sInitSerial.Length, '0'));
            else
                ToolsClass.saveXMLNode("SerialCode", iSerial.ToString().PadLeft(sInitSerial.Length, '0'));

            lock (lockGenerateBoxID)//Lock
            {
                ToolsClass.saveBoxCodeList(sboxid);
                strsql = "update Box set IsUsed='W' where BoxID='" + sboxid + "'";
                ToolsClass.ExecuteNonQuery(strsql);
            }
            return sboxid;
        }
    }
}
