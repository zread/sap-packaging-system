﻿namespace CSICPR
{
    partial class FormConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CbGlassType = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.CbCertificate = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.CbLine = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.CbCell = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.CbPackType = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.rbNomalP = new System.Windows.Forms.RadioButton();
            this.rbTrueP = new System.Windows.Forms.RadioButton();
            this.rbModel = new System.Windows.Forms.RadioButton();
            this.btTestPrint = new System.Windows.Forms.Button();
            this.tbParBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbCPTLabpath = new System.Windows.Forms.TextBox();
            this.tbBoxLabpath = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbModuleClass = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtLabelCnt = new System.Windows.Forms.TextBox();
            this.tbF2BoxCode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbF1BoxCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbF3BoxCode = new System.Windows.Forms.TextBox();
            this.cbCartonGrade = new System.Windows.Forms.ComboBox();
            this.lblStationNo = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtSupply = new System.Windows.Forms.TextBox();
            this.txtSchNo = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbHandPacking = new System.Windows.Forms.RadioButton();
            this.rbAutoPacking = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.nudV = new System.Windows.Forms.NumericUpDown();
            this.nudH = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.rbLargeLabelPrint = new System.Windows.Forms.RadioButton();
            this.label16 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nudPrintNum = new System.Windows.Forms.NumericUpDown();
            this.rbIndividual = new System.Windows.Forms.RadioButton();
            this.rbBatch = new System.Windows.Forms.RadioButton();
            this.rbForbid = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCheckRule = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.nudCartonQty = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.cbMix = new System.Windows.Forms.CheckBox();
            this.nudMixCnt = new System.Windows.Forms.NumericUpDown();
            this.cbCheckNo = new System.Windows.Forms.CheckBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrintNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCartonQty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMixCnt)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CbGlassType);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.CbCertificate);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.CbLine);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.CbCell);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.CbPackType);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.rbNomalP);
            this.groupBox3.Controls.Add(this.rbTrueP);
            this.groupBox3.Controls.Add(this.rbModel);
            this.groupBox3.Controls.Add(this.btTestPrint);
            this.groupBox3.Controls.Add(this.tbParBox);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.tbCPTLabpath);
            this.groupBox3.Controls.Add(this.tbBoxLabpath);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Location = new System.Drawing.Point(11, 307);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(368, 240);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "标签设定";
            // 
            // CbGlassType
            // 
            this.CbGlassType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbGlassType.FormattingEnabled = true;
            this.CbGlassType.Items.AddRange(new object[] {
            "Normal",
            "PN"});
            this.CbGlassType.Location = new System.Drawing.Point(111, 184);
            this.CbGlassType.Name = "CbGlassType";
            this.CbGlassType.Size = new System.Drawing.Size(86, 21);
            this.CbGlassType.TabIndex = 74;
            this.CbGlassType.Visible = false;
            this.CbGlassType.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(19, 182);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(100, 23);
            this.label21.TabIndex = 75;
            this.label21.Text = "GlassType:";
            this.label21.Visible = false;
            this.label21.Click += new System.EventHandler(this.Label21Click);
            // 
            // CbCertificate
            // 
            this.CbCertificate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbCertificate.FormattingEnabled = true;
            this.CbCertificate.Items.AddRange(new object[] {
            "Dual",
            "CSA"});
            this.CbCertificate.Location = new System.Drawing.Point(272, 121);
            this.CbCertificate.Name = "CbCertificate";
            this.CbCertificate.Size = new System.Drawing.Size(86, 21);
            this.CbCertificate.TabIndex = 72;
            this.CbCertificate.SelectedIndexChanged += new System.EventHandler(this.CbCertificateSelectedIndexChanged);
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(201, 124);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(100, 23);
            this.label19.TabIndex = 73;
            this.label19.Text = "Certificate";
            // 
            // CbLine
            // 
            this.CbLine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbLine.FormattingEnabled = true;
            this.CbLine.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "Rework"});
            this.CbLine.Location = new System.Drawing.Point(272, 152);
            this.CbLine.Name = "CbLine";
            this.CbLine.Size = new System.Drawing.Size(86, 21);
            this.CbLine.TabIndex = 70;
            this.CbLine.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(201, 155);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(100, 23);
            this.label20.TabIndex = 71;
            this.label20.Text = "Line:";
            // 
            // CbCell
            // 
            this.CbCell.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbCell.FormattingEnabled = true;
            this.CbCell.Items.AddRange(new object[] {
            "",
            "Taiwanese",
            "Chinese",
            "Thai",
            "Indian",
            "Korean"});
            this.CbCell.Location = new System.Drawing.Point(109, 121);
            this.CbCell.Name = "CbCell";
            this.CbCell.Size = new System.Drawing.Size(86, 21);
            this.CbCell.TabIndex = 28;
            this.CbCell.SelectedIndexChanged += new System.EventHandler(this.CbCellSelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(16, 151);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 13);
            this.label17.TabIndex = 36;
            this.label17.Text = "Packaging Type:";
            // 
            // CbPackType
            // 
            this.CbPackType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbPackType.FormattingEnabled = true;
            this.CbPackType.Items.AddRange(new object[] {
            "Vertical",
            "Horizontal"});
            this.CbPackType.Location = new System.Drawing.Point(109, 151);
            this.CbPackType.Name = "CbPackType";
            this.CbPackType.Size = new System.Drawing.Size(86, 21);
            this.CbPackType.TabIndex = 35;
            this.CbPackType.SelectedIndexChanged += new System.EventHandler(this.CbPackTypeSelectedIndexChanged);
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(19, 124);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(100, 23);
            this.label18.TabIndex = 67;
            this.label18.Text = "Cell Vendor";
            // 
            // rbNomalP
            // 
            this.rbNomalP.AutoSize = true;
            this.rbNomalP.Location = new System.Drawing.Point(193, 44);
            this.rbNomalP.Name = "rbNomalP";
            this.rbNomalP.Size = new System.Drawing.Size(73, 17);
            this.rbNomalP.TabIndex = 12;
            this.rbNomalP.TabStop = true;
            this.rbNomalP.Text = "标称功率";
            this.rbNomalP.UseVisualStyleBackColor = true;
            this.rbNomalP.CheckedChanged += new System.EventHandler(this.rbNomalP_CheckedChanged);
            // 
            // rbTrueP
            // 
            this.rbTrueP.AutoSize = true;
            this.rbTrueP.Location = new System.Drawing.Point(92, 44);
            this.rbTrueP.Name = "rbTrueP";
            this.rbTrueP.Size = new System.Drawing.Size(73, 17);
            this.rbTrueP.TabIndex = 11;
            this.rbTrueP.TabStop = true;
            this.rbTrueP.Text = "实测功率";
            this.rbTrueP.UseVisualStyleBackColor = true;
            this.rbTrueP.CheckedChanged += new System.EventHandler(this.rbTrueP_CheckedChanged);
            // 
            // rbModel
            // 
            this.rbModel.AutoSize = true;
            this.rbModel.Checked = true;
            this.rbModel.Location = new System.Drawing.Point(20, 44);
            this.rbModel.Name = "rbModel";
            this.rbModel.Size = new System.Drawing.Size(49, 17);
            this.rbModel.TabIndex = 10;
            this.rbModel.TabStop = true;
            this.rbModel.Text = "规格";
            this.rbModel.UseVisualStyleBackColor = true;
            this.rbModel.CheckedChanged += new System.EventHandler(this.rbModel_CheckedChanged);
            // 
            // btTestPrint
            // 
            this.btTestPrint.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btTestPrint.Location = new System.Drawing.Point(3, 211);
            this.btTestPrint.Name = "btTestPrint";
            this.btTestPrint.Size = new System.Drawing.Size(362, 26);
            this.btTestPrint.TabIndex = 15;
            this.btTestPrint.Text = "测试标签打印";
            this.btTestPrint.UseVisualStyleBackColor = true;
            this.btTestPrint.Click += new System.EventHandler(this.btTestPrint_Click);
            // 
            // tbParBox
            // 
            this.tbParBox.Location = new System.Drawing.Point(75, 95);
            this.tbParBox.Name = "tbParBox";
            this.tbParBox.Size = new System.Drawing.Size(232, 20);
            this.tbParBox.TabIndex = 9;
            this.tbParBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbParBox_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(21, 99);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "包号变量:";
            // 
            // tbCPTLabpath
            // 
            this.tbCPTLabpath.Location = new System.Drawing.Point(77, 22);
            this.tbCPTLabpath.Name = "tbCPTLabpath";
            this.tbCPTLabpath.Size = new System.Drawing.Size(232, 20);
            this.tbCPTLabpath.TabIndex = 60;
            this.tbCPTLabpath.TextChanged += new System.EventHandler(this.tbCPTLabpath_TextChanged);
            this.tbCPTLabpath.Enter += new System.EventHandler(this.tbCPTLabpath_Enter);
            // 
            // tbBoxLabpath
            // 
            this.tbBoxLabpath.Location = new System.Drawing.Point(77, 66);
            this.tbBoxLabpath.Name = "tbBoxLabpath";
            this.tbBoxLabpath.Size = new System.Drawing.Size(232, 20);
            this.tbBoxLabpath.TabIndex = 66;
            this.tbBoxLabpath.TextChanged += new System.EventHandler(this.tbBoxLabpath_TextChanged);
            this.tbBoxLabpath.Enter += new System.EventHandler(this.tbCPTLabpath_Enter);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.Location = new System.Drawing.Point(18, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 12);
            this.label12.TabIndex = 2;
            this.label12.Text = "组件标签:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("SimSun", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(18, 72);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 12);
            this.label7.TabIndex = 1;
            this.label7.Text = "包号标签:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbModuleClass);
            this.groupBox2.Controls.Add(this.tabControl1);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtCheckRule);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.nudCartonQty);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cbMix);
            this.groupBox2.Controls.Add(this.nudMixCnt);
            this.groupBox2.Controls.Add(this.cbCheckNo);
            this.groupBox2.Location = new System.Drawing.Point(12, 8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(367, 299);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "基础参数设定";
            // 
            // cbModuleClass
            // 
            this.cbModuleClass.AutoSize = true;
            this.cbModuleClass.Location = new System.Drawing.Point(14, 272);
            this.cbModuleClass.Name = "cbModuleClass";
            this.cbModuleClass.Size = new System.Drawing.Size(110, 17);
            this.cbModuleClass.TabIndex = 34;
            this.cbModuleClass.Text = "可混装组件等级";
            this.cbModuleClass.UseVisualStyleBackColor = true;
            this.cbModuleClass.CheckedChanged += new System.EventHandler(this.cbModuleClass_CheckedChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(167, 105);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(199, 163);
            this.tabControl1.TabIndex = 30;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtLabelCnt);
            this.tabPage1.Controls.Add(this.tbF2BoxCode);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.tbF1BoxCode);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.tbF3BoxCode);
            this.tabPage1.Controls.Add(this.cbCartonGrade);
            this.tabPage1.Controls.Add(this.lblStationNo);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(191, 137);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "托号编码规则";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtLabelCnt
            // 
            this.txtLabelCnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLabelCnt.Location = new System.Drawing.Point(88, 85);
            this.txtLabelCnt.MaxLength = 5;
            this.txtLabelCnt.Name = "txtLabelCnt";
            this.txtLabelCnt.Size = new System.Drawing.Size(86, 20);
            this.txtLabelCnt.TabIndex = 27;
            this.txtLabelCnt.Text = "1";
            this.txtLabelCnt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLabelCnt_KeyPress);
            // 
            // tbF2BoxCode
            // 
            this.tbF2BoxCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbF2BoxCode.Location = new System.Drawing.Point(88, 29);
            this.tbF2BoxCode.MaxLength = 8;
            this.tbF2BoxCode.Name = "tbF2BoxCode";
            this.tbF2BoxCode.Size = new System.Drawing.Size(86, 20);
            this.tbF2BoxCode.TabIndex = 20;
            this.tbF2BoxCode.Text = "00001";
            this.tbF2BoxCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbF2BoxCode_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "产生数量:";
            // 
            // tbF1BoxCode
            // 
            this.tbF1BoxCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbF1BoxCode.Location = new System.Drawing.Point(88, 2);
            this.tbF1BoxCode.MaxLength = 8;
            this.tbF1BoxCode.Name = "tbF1BoxCode";
            this.tbF1BoxCode.Size = new System.Drawing.Size(86, 20);
            this.tbF1BoxCode.TabIndex = 19;
            this.tbF1BoxCode.Text = "1501";
            this.tbF1BoxCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbF1BoxCode_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "箱号等级:";
            // 
            // tbF3BoxCode
            // 
            this.tbF3BoxCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbF3BoxCode.Location = new System.Drawing.Point(88, 56);
            this.tbF3BoxCode.MaxLength = 3;
            this.tbF3BoxCode.Name = "tbF3BoxCode";
            this.tbF3BoxCode.Size = new System.Drawing.Size(86, 20);
            this.tbF3BoxCode.TabIndex = 21;
            this.tbF3BoxCode.Text = "2";
            this.tbF3BoxCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbF3BoxCode_KeyPress);
            // 
            // cbCartonGrade
            // 
            this.cbCartonGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCartonGrade.FormattingEnabled = true;
            this.cbCartonGrade.Location = new System.Drawing.Point(88, 112);
            this.cbCartonGrade.Name = "cbCartonGrade";
            this.cbCartonGrade.Size = new System.Drawing.Size(86, 21);
            this.cbCartonGrade.TabIndex = 25;
            this.cbCartonGrade.SelectedIndexChanged += new System.EventHandler(this.cbCartonGrade_SelectedIndexChanged);
            // 
            // lblStationNo
            // 
            this.lblStationNo.AutoSize = true;
            this.lblStationNo.Location = new System.Drawing.Point(13, 5);
            this.lblStationNo.Name = "lblStationNo";
            this.lblStationNo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblStationNo.Size = new System.Drawing.Size(70, 13);
            this.lblStationNo.TabIndex = 22;
            this.lblStationNo.Text = "年份和车间:";
            this.lblStationNo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(24, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "颜色代码:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(35, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "流水号:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtSupply);
            this.tabPage2.Controls.Add(this.txtSchNo);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(191, 137);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "旭格托号规则";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtSupply
            // 
            this.txtSupply.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSupply.Location = new System.Drawing.Point(89, 41);
            this.txtSupply.MaxLength = 5;
            this.txtSupply.Name = "txtSupply";
            this.txtSupply.Size = new System.Drawing.Size(86, 20);
            this.txtSupply.TabIndex = 31;
            this.txtSupply.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSupply_KeyPress);
            // 
            // txtSchNo
            // 
            this.txtSchNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSchNo.Location = new System.Drawing.Point(89, 12);
            this.txtSchNo.MaxLength = 8;
            this.txtSchNo.Name = "txtSchNo";
            this.txtSchNo.Size = new System.Drawing.Size(86, 20);
            this.txtSchNo.TabIndex = 28;
            this.txtSchNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSchNo_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(21, 49);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 13);
            this.label13.TabIndex = 30;
            this.label13.Text = "供应商代码:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(21, 15);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = "旭格流水号:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbHandPacking);
            this.groupBox1.Controls.Add(this.rbAutoPacking);
            this.groupBox1.Location = new System.Drawing.Point(170, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(191, 60);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "处理模式";
            // 
            // rbHandPacking
            // 
            this.rbHandPacking.AutoSize = true;
            this.rbHandPacking.Checked = true;
            this.rbHandPacking.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rbHandPacking.Location = new System.Drawing.Point(12, 22);
            this.rbHandPacking.Name = "rbHandPacking";
            this.rbHandPacking.Size = new System.Drawing.Size(81, 18);
            this.rbHandPacking.TabIndex = 31;
            this.rbHandPacking.TabStop = true;
            this.rbHandPacking.Text = "手动处理";
            this.rbHandPacking.UseVisualStyleBackColor = true;
            this.rbHandPacking.CheckedChanged += new System.EventHandler(this.rbHandPacking_CheckedChanged);
            // 
            // rbAutoPacking
            // 
            this.rbAutoPacking.AutoSize = true;
            this.rbAutoPacking.Font = new System.Drawing.Font("SimSun", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rbAutoPacking.Location = new System.Drawing.Point(106, 22);
            this.rbAutoPacking.Name = "rbAutoPacking";
            this.rbAutoPacking.Size = new System.Drawing.Size(81, 18);
            this.rbAutoPacking.TabIndex = 32;
            this.rbAutoPacking.Text = "自动处理";
            this.rbAutoPacking.UseVisualStyleBackColor = true;
            this.rbAutoPacking.CheckedChanged += new System.EventHandler(this.rbAutoPacking_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.nudV);
            this.groupBox4.Controls.Add(this.nudH);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.rbLargeLabelPrint);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.nudPrintNum);
            this.groupBox4.Controls.Add(this.rbIndividual);
            this.groupBox4.Controls.Add(this.rbBatch);
            this.groupBox4.Controls.Add(this.rbForbid);
            this.groupBox4.Location = new System.Drawing.Point(12, 105);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(149, 158);
            this.groupBox4.TabIndex = 28;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "打印模式";
            // 
            // nudV
            // 
            this.nudV.Location = new System.Drawing.Point(98, 103);
            this.nudV.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.nudV.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudV.Name = "nudV";
            this.nudV.Size = new System.Drawing.Size(33, 20);
            this.nudV.TabIndex = 30;
            this.nudV.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudV.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudV.ValueChanged += new System.EventHandler(this.nudV_ValueChanged);
            // 
            // nudH
            // 
            this.nudH.Location = new System.Drawing.Point(35, 103);
            this.nudH.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.nudH.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudH.Name = "nudH";
            this.nudH.Size = new System.Drawing.Size(33, 20);
            this.nudH.TabIndex = 29;
            this.nudH.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudH.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudH.ValueChanged += new System.EventHandler(this.nudH_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(73, 107);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "列：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "行：";
            // 
            // rbLargeLabelPrint
            // 
            this.rbLargeLabelPrint.Location = new System.Drawing.Point(7, 82);
            this.rbLargeLabelPrint.Name = "rbLargeLabelPrint";
            this.rbLargeLabelPrint.Size = new System.Drawing.Size(115, 24);
            this.rbLargeLabelPrint.TabIndex = 20;
            this.rbLargeLabelPrint.TabStop = true;
            this.rbLargeLabelPrint.Text = "打印竖包装标签";
            this.rbLargeLabelPrint.UseVisualStyleBackColor = true;
            this.rbLargeLabelPrint.CheckedChanged += new System.EventHandler(this.rbLargeLabelPrint_CheckedChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(96, 41);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(24, 13);
            this.label16.TabIndex = 19;
            this.label16.Text = "pcs";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Print";
            // 
            // nudPrintNum
            // 
            this.nudPrintNum.Location = new System.Drawing.Point(60, 37);
            this.nudPrintNum.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudPrintNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudPrintNum.Name = "nudPrintNum";
            this.nudPrintNum.Size = new System.Drawing.Size(33, 20);
            this.nudPrintNum.TabIndex = 12;
            this.nudPrintNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudPrintNum.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudPrintNum.ValueChanged += new System.EventHandler(this.nudPrintNum_ValueChanged);
            // 
            // rbIndividual
            // 
            this.rbIndividual.AutoSize = true;
            this.rbIndividual.Checked = true;
            this.rbIndividual.Location = new System.Drawing.Point(6, 21);
            this.rbIndividual.Name = "rbIndividual";
            this.rbIndividual.Size = new System.Drawing.Size(97, 17);
            this.rbIndividual.TabIndex = 6;
            this.rbIndividual.TabStop = true;
            this.rbIndividual.Text = "逐个打印标签";
            this.rbIndividual.UseVisualStyleBackColor = true;
            this.rbIndividual.CheckedChanged += new System.EventHandler(this.rbIndividual_CheckedChanged);
            // 
            // rbBatch
            // 
            this.rbBatch.AutoSize = true;
            this.rbBatch.Location = new System.Drawing.Point(7, 60);
            this.rbBatch.Name = "rbBatch";
            this.rbBatch.Size = new System.Drawing.Size(97, 17);
            this.rbBatch.TabIndex = 7;
            this.rbBatch.Text = "批量打印标签";
            this.rbBatch.UseVisualStyleBackColor = true;
            this.rbBatch.CheckedChanged += new System.EventHandler(this.rbBatch_CheckedChanged);
            // 
            // rbForbid
            // 
            this.rbForbid.AutoSize = true;
            this.rbForbid.Location = new System.Drawing.Point(6, 132);
            this.rbForbid.Name = "rbForbid";
            this.rbForbid.Size = new System.Drawing.Size(85, 17);
            this.rbForbid.TabIndex = 8;
            this.rbForbid.Text = "不打印标签";
            this.rbForbid.UseVisualStyleBackColor = true;
            this.rbForbid.CheckedChanged += new System.EventHandler(this.rbForbid_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(120, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "功率";
            // 
            // txtCheckRule
            // 
            this.txtCheckRule.Location = new System.Drawing.Point(91, 79);
            this.txtCheckRule.MaxLength = 18;
            this.txtCheckRule.Name = "txtCheckRule";
            this.txtCheckRule.Size = new System.Drawing.Size(268, 20);
            this.txtCheckRule.TabIndex = 5;
            this.txtCheckRule.Text = "------***";
            this.txtCheckRule.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCheckRule_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1157, 884);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Module";
            // 
            // nudCartonQty
            // 
            this.nudCartonQty.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudCartonQty.Location = new System.Drawing.Point(80, 23);
            this.nudCartonQty.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCartonQty.Name = "nudCartonQty";
            this.nudCartonQty.Size = new System.Drawing.Size(40, 20);
            this.nudCartonQty.TabIndex = 0;
            this.nudCartonQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudCartonQty.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nudCartonQty.ValueChanged += new System.EventHandler(this.nudCartonQty_ValueChanged);
            this.nudCartonQty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nudCartonQty_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "每箱数量:";
            // 
            // cbMix
            // 
            this.cbMix.AutoSize = true;
            this.cbMix.Location = new System.Drawing.Point(13, 55);
            this.cbMix.Name = "cbMix";
            this.cbMix.Size = new System.Drawing.Size(62, 17);
            this.cbMix.TabIndex = 2;
            this.cbMix.Text = "可混装";
            this.cbMix.UseVisualStyleBackColor = true;
            this.cbMix.CheckedChanged += new System.EventHandler(this.cbMix_CheckedChanged);
            // 
            // nudMixCnt
            // 
            this.nudMixCnt.Enabled = false;
            this.nudMixCnt.Location = new System.Drawing.Point(80, 52);
            this.nudMixCnt.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.nudMixCnt.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMixCnt.Name = "nudMixCnt";
            this.nudMixCnt.Size = new System.Drawing.Size(40, 20);
            this.nudMixCnt.TabIndex = 3;
            this.nudMixCnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudMixCnt.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudMixCnt.ValueChanged += new System.EventHandler(this.nudMixCnt_ValueChanged);
            this.nudMixCnt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nudMixCnt_KeyPress);
            // 
            // cbCheckNo
            // 
            this.cbCheckNo.AutoSize = true;
            this.cbCheckNo.Location = new System.Drawing.Point(13, 81);
            this.cbCheckNo.Name = "cbCheckNo";
            this.cbCheckNo.Size = new System.Drawing.Size(86, 17);
            this.cbCheckNo.TabIndex = 4;
            this.cbCheckNo.Text = "检查工作号";
            this.cbCheckNo.UseVisualStyleBackColor = true;
            this.cbCheckNo.CheckedChanged += new System.EventHandler(this.cbCheckNo_CheckedChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(143, 553);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 25);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "返回";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Label File(*.*)|*.*";
            this.openFileDialog1.Title = "Select Label File to Use";
            // 
            // FormConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 590);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormConfig";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Package Configuration";
            this.Load += new System.EventHandler(this.FormConfig_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrintNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCartonQty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMixCnt)).EndInit();
            this.ResumeLayout(false);

        }
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox CbLine;
        private System.Windows.Forms.ComboBox CbCell;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox CbPackType;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.RadioButton rbLargeLabelPrint;

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbNomalP;
        private System.Windows.Forms.RadioButton rbTrueP;
        private System.Windows.Forms.RadioButton rbModel;
        private System.Windows.Forms.Button btTestPrint;
        private System.Windows.Forms.TextBox tbParBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tbCPTLabpath;
        private System.Windows.Forms.TextBox tbBoxLabpath;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbForbid;
        private System.Windows.Forms.RadioButton rbBatch;
        private System.Windows.Forms.TextBox txtCheckRule;
        private System.Windows.Forms.RadioButton rbIndividual;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudCartonQty;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox cbMix;
        private System.Windows.Forms.NumericUpDown nudMixCnt;
        private System.Windows.Forms.CheckBox cbCheckNo;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbF3BoxCode;
        private System.Windows.Forms.TextBox tbF2BoxCode;
        private System.Windows.Forms.TextBox tbF1BoxCode;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblStationNo;
        private System.Windows.Forms.RadioButton rbHandPacking;
        private System.Windows.Forms.RadioButton rbAutoPacking;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudPrintNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbCartonGrade;
        private System.Windows.Forms.TextBox txtLabelCnt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nudV;
        private System.Windows.Forms.NumericUpDown nudH;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtSupply;
        private System.Windows.Forms.TextBox txtSchNo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox cbModuleClass;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox CbCertificate;
        private System.Windows.Forms.ComboBox CbGlassType;
        private System.Windows.Forms.Label label21;
    }
}