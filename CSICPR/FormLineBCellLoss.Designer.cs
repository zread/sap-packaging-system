﻿namespace CSICPR
{
    partial class FormLineBCellLoss
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LineB = new System.Windows.Forms.Label();
            this.ClockB = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.S4 = new System.Windows.Forms.TextBox();
            this.Review = new System.Windows.Forms.Button();
            this.Modify = new System.Windows.Forms.Button();
            this.BoxNumTextbox = new System.Windows.Forms.TextBox();
            this.BoxId = new System.Windows.Forms.TextBox();
            this.MOLabel = new System.Windows.Forms.Label();
            this.S1 = new System.Windows.Forms.TextBox();
            this.Submit = new System.Windows.Forms.Button();
            this.BoxNumtextBox2 = new System.Windows.Forms.TextBox();
            this.MO = new System.Windows.Forms.ComboBox();
            this.XN4 = new System.Windows.Forms.Label();
            this.BoxId2 = new System.Windows.Forms.TextBox();
            this.Seperate = new System.Windows.Forms.Panel();
            this.FromStringer = new System.Windows.Forms.Label();
            this.pcs3 = new System.Windows.Forms.Label();
            this.accident = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.FromIncoming = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pcs4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.XN3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.XN1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.S2 = new System.Windows.Forms.TextBox();
            this.BoxNum = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pcs1 = new System.Windows.Forms.Label();
            this.Qty = new System.Windows.Forms.Label();
            this.S3 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pcs2 = new System.Windows.Forms.Label();
            this.XN2 = new System.Windows.Forms.Label();
            this.shift = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.others = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.Seperate.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // LineB
            // 
            this.LineB.AutoSize = true;
            this.LineB.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LineB.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.LineB.Location = new System.Drawing.Point(410, 29);
            this.LineB.Name = "LineB";
            this.LineB.Size = new System.Drawing.Size(96, 31);
            this.LineB.TabIndex = 1;
            this.LineB.Text = "Line B";
            // 
            // ClockB
            // 
            this.ClockB.AutoSize = true;
            this.ClockB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClockB.ForeColor = System.Drawing.SystemColors.Control;
            this.ClockB.Location = new System.Drawing.Point(675, 35);
            this.ClockB.Name = "ClockB";
            this.ClockB.Size = new System.Drawing.Size(76, 25);
            this.ClockB.TabIndex = 3;
            this.ClockB.Text = "label1";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(30, 465);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(867, 287);
            this.dataGridView1.TabIndex = 52;
            // 
            // S4
            // 
            this.S4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.S4.Location = new System.Drawing.Point(75, 296);
            this.S4.Name = "S4";
            this.S4.Size = new System.Drawing.Size(120, 22);
            this.S4.TabIndex = 36;
            this.S4.Text = "0";
            // 
            // Review
            // 
            this.Review.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.Review.Location = new System.Drawing.Point(680, 85);
            this.Review.Name = "Review";
            this.Review.Size = new System.Drawing.Size(116, 33);
            this.Review.TabIndex = 51;
            this.Review.Text = "Review";
            this.Review.UseVisualStyleBackColor = true;
            this.Review.Click += new System.EventHandler(this.Review_Click);
            // 
            // Modify
            // 
            this.Modify.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.Modify.Location = new System.Drawing.Point(355, 410);
            this.Modify.Name = "Modify";
            this.Modify.Size = new System.Drawing.Size(116, 33);
            this.Modify.TabIndex = 50;
            this.Modify.Text = "Modify";
            this.Modify.UseVisualStyleBackColor = true;
            this.Modify.Click += new System.EventHandler(this.Modify_Click);
            // 
            // BoxNumTextbox
            // 
            this.BoxNumTextbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BoxNumTextbox.Location = new System.Drawing.Point(453, 185);
            this.BoxNumTextbox.Name = "BoxNumTextbox";
            this.BoxNumTextbox.Size = new System.Drawing.Size(120, 22);
            this.BoxNumTextbox.TabIndex = 37;
            // 
            // BoxId
            // 
            this.BoxId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BoxId.Location = new System.Drawing.Point(402, 222);
            this.BoxId.Name = "BoxId";
            this.BoxId.Size = new System.Drawing.Size(120, 22);
            this.BoxId.TabIndex = 44;
            this.BoxId.Text = "0";
            // 
            // MOLabel
            // 
            this.MOLabel.AutoSize = true;
            this.MOLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MOLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.MOLabel.Location = new System.Drawing.Point(24, 87);
            this.MOLabel.Name = "MOLabel";
            this.MOLabel.Size = new System.Drawing.Size(75, 31);
            this.MOLabel.TabIndex = 12;
            this.MOLabel.Text = "MO#";
            // 
            // S1
            // 
            this.S1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.S1.Location = new System.Drawing.Point(75, 182);
            this.S1.Name = "S1";
            this.S1.Size = new System.Drawing.Size(120, 22);
            this.S1.TabIndex = 42;
            this.S1.Text = "0";
            // 
            // Submit
            // 
            this.Submit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.Submit.Location = new System.Drawing.Point(75, 410);
            this.Submit.Name = "Submit";
            this.Submit.Size = new System.Drawing.Size(116, 33);
            this.Submit.TabIndex = 49;
            this.Submit.Text = "Submit";
            this.Submit.UseVisualStyleBackColor = true;
            this.Submit.Click += new System.EventHandler(this.Submit_Click);
            // 
            // BoxNumtextBox2
            // 
            this.BoxNumtextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BoxNumtextBox2.Location = new System.Drawing.Point(453, 305);
            this.BoxNumtextBox2.Name = "BoxNumtextBox2";
            this.BoxNumtextBox2.Size = new System.Drawing.Size(120, 22);
            this.BoxNumtextBox2.TabIndex = 39;
            // 
            // MO
            // 
            this.MO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MO.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MO.FormattingEnabled = true;
            this.MO.Location = new System.Drawing.Point(100, 87);
            this.MO.Name = "MO";
            this.MO.Size = new System.Drawing.Size(147, 33);
            this.MO.TabIndex = 13;
            // 
            // XN4
            // 
            this.XN4.AutoSize = true;
            this.XN4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XN4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.XN4.Location = new System.Drawing.Point(27, 298);
            this.XN4.Name = "XN4";
            this.XN4.Size = new System.Drawing.Size(33, 16);
            this.XN4.TabIndex = 33;
            this.XN4.Text = "XN4";
            // 
            // BoxId2
            // 
            this.BoxId2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BoxId2.Location = new System.Drawing.Point(402, 342);
            this.BoxId2.Name = "BoxId2";
            this.BoxId2.Size = new System.Drawing.Size(120, 22);
            this.BoxId2.TabIndex = 38;
            this.BoxId2.Text = "0";
            // 
            // Seperate
            // 
            this.Seperate.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Seperate.Controls.Add(this.FromStringer);
            this.Seperate.Location = new System.Drawing.Point(30, 133);
            this.Seperate.Name = "Seperate";
            this.Seperate.Size = new System.Drawing.Size(217, 33);
            this.Seperate.TabIndex = 16;
            // 
            // FromStringer
            // 
            this.FromStringer.AutoSize = true;
            this.FromStringer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FromStringer.Location = new System.Drawing.Point(9, 7);
            this.FromStringer.Name = "FromStringer";
            this.FromStringer.Size = new System.Drawing.Size(189, 20);
            this.FromStringer.TabIndex = 0;
            this.FromStringer.Text = "Crack Cells From Stringer";
            // 
            // pcs3
            // 
            this.pcs3.AutoSize = true;
            this.pcs3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.pcs3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pcs3.Location = new System.Drawing.Point(212, 261);
            this.pcs3.Name = "pcs3";
            this.pcs3.Size = new System.Drawing.Size(35, 16);
            this.pcs3.TabIndex = 32;
            this.pcs3.Text = "PCS";
            // 
            // accident
            // 
            this.accident.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accident.Location = new System.Drawing.Point(680, 223);
            this.accident.Name = "accident";
            this.accident.Size = new System.Drawing.Size(120, 22);
            this.accident.TabIndex = 48;
            this.accident.Text = "0";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.FromIncoming);
            this.panel1.Location = new System.Drawing.Point(355, 133);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(217, 33);
            this.panel1.TabIndex = 14;
            // 
            // FromIncoming
            // 
            this.FromIncoming.AutoSize = true;
            this.FromIncoming.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FromIncoming.Location = new System.Drawing.Point(9, 7);
            this.FromIncoming.Name = "FromIncoming";
            this.FromIncoming.Size = new System.Drawing.Size(198, 20);
            this.FromIncoming.TabIndex = 0;
            this.FromIncoming.Text = "Crack Cells From Incoming";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(538, 345);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 16);
            this.label4.TabIndex = 34;
            this.label4.Text = "PCS";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(816, 226);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 16);
            this.label6.TabIndex = 46;
            this.label6.Text = "PCS";
            // 
            // pcs4
            // 
            this.pcs4.AutoSize = true;
            this.pcs4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.pcs4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pcs4.Location = new System.Drawing.Point(212, 300);
            this.pcs4.Name = "pcs4";
            this.pcs4.Size = new System.Drawing.Size(35, 16);
            this.pcs4.TabIndex = 29;
            this.pcs4.Text = "PCS";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(680, 133);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(217, 33);
            this.panel2.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(195, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Crack Cells From Accident";
            // 
            // XN3
            // 
            this.XN3.AutoSize = true;
            this.XN3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XN3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.XN3.Location = new System.Drawing.Point(27, 260);
            this.XN3.Name = "XN3";
            this.XN3.Size = new System.Drawing.Size(33, 16);
            this.XN3.TabIndex = 17;
            this.XN3.Text = "XN3";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(677, 191);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 16);
            this.label7.TabIndex = 47;
            this.label7.Text = "Total Quantity";
            // 
            // XN1
            // 
            this.XN1.AutoSize = true;
            this.XN1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XN1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.XN1.Location = new System.Drawing.Point(27, 183);
            this.XN1.Name = "XN1";
            this.XN1.Size = new System.Drawing.Size(33, 16);
            this.XN1.TabIndex = 19;
            this.XN1.Text = "XN1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(352, 345);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 16);
            this.label2.TabIndex = 20;
            this.label2.Text = "Qty";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(355, 271);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(218, 10);
            this.progressBar1.TabIndex = 45;
            // 
            // S2
            // 
            this.S2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.S2.Location = new System.Drawing.Point(75, 220);
            this.S2.Name = "S2";
            this.S2.Size = new System.Drawing.Size(120, 22);
            this.S2.TabIndex = 41;
            this.S2.Text = "0";
            // 
            // BoxNum
            // 
            this.BoxNum.AutoSize = true;
            this.BoxNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BoxNum.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BoxNum.Location = new System.Drawing.Point(352, 188);
            this.BoxNum.Name = "BoxNum";
            this.BoxNum.Size = new System.Drawing.Size(89, 16);
            this.BoxNum.TabIndex = 21;
            this.BoxNum.Text = "Box Number1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(538, 225);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 16);
            this.label3.TabIndex = 22;
            this.label3.Text = "PCS";
            // 
            // pcs1
            // 
            this.pcs1.AutoSize = true;
            this.pcs1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.pcs1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pcs1.Location = new System.Drawing.Point(212, 183);
            this.pcs1.Name = "pcs1";
            this.pcs1.Size = new System.Drawing.Size(35, 16);
            this.pcs1.TabIndex = 24;
            this.pcs1.Text = "PCS";
            // 
            // Qty
            // 
            this.Qty.AutoSize = true;
            this.Qty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Qty.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Qty.Location = new System.Drawing.Point(352, 225);
            this.Qty.Name = "Qty";
            this.Qty.Size = new System.Drawing.Size(28, 16);
            this.Qty.TabIndex = 25;
            this.Qty.Text = "Qty";
            // 
            // S3
            // 
            this.S3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.S3.Location = new System.Drawing.Point(75, 258);
            this.S3.Name = "S3";
            this.S3.Size = new System.Drawing.Size(120, 22);
            this.S3.TabIndex = 35;
            this.S3.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(352, 308);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 16);
            this.label1.TabIndex = 26;
            this.label1.Text = "Box Number2";
            // 
            // pcs2
            // 
            this.pcs2.AutoSize = true;
            this.pcs2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.pcs2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pcs2.Location = new System.Drawing.Point(212, 222);
            this.pcs2.Name = "pcs2";
            this.pcs2.Size = new System.Drawing.Size(35, 16);
            this.pcs2.TabIndex = 27;
            this.pcs2.Text = "PCS";
            // 
            // XN2
            // 
            this.XN2.AutoSize = true;
            this.XN2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XN2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.XN2.Location = new System.Drawing.Point(27, 221);
            this.XN2.Name = "XN2";
            this.XN2.Size = new System.Drawing.Size(33, 16);
            this.XN2.TabIndex = 28;
            this.XN2.Text = "XN2";
            // 
            // shift
            // 
            this.shift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.shift.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.shift.FormattingEnabled = true;
            this.shift.Items.AddRange(new object[] {
            "D1",
            "N1",
            "D2",
            "N2"});
            this.shift.Location = new System.Drawing.Point(440, 85);
            this.shift.Name = "shift";
            this.shift.Size = new System.Drawing.Size(133, 33);
            this.shift.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.Control;
            this.label8.Location = new System.Drawing.Point(350, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 31);
            this.label8.TabIndex = 12;
            this.label8.Text = "Shift";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(677, 338);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 16);
            this.label9.TabIndex = 47;
            this.label9.Text = "Total Quantity";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel3.Controls.Add(this.label10);
            this.panel3.Location = new System.Drawing.Point(680, 280);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(217, 33);
            this.panel3.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 7);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(181, 20);
            this.label10.TabIndex = 0;
            this.label10.Text = "Crack Cells From Others";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label11.Location = new System.Drawing.Point(816, 373);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 16);
            this.label11.TabIndex = 46;
            this.label11.Text = "PCS";
            // 
            // others
            // 
            this.others.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.others.Location = new System.Drawing.Point(680, 370);
            this.others.Name = "others";
            this.others.Size = new System.Drawing.Size(120, 22);
            this.others.TabIndex = 48;
            this.others.Text = "0";
            // 
            // FormLineBCellLoss
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(971, 738);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.S4);
            this.Controls.Add(this.Review);
            this.Controls.Add(this.Modify);
            this.Controls.Add(this.BoxNumTextbox);
            this.Controls.Add(this.BoxId);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.MOLabel);
            this.Controls.Add(this.S1);
            this.Controls.Add(this.Submit);
            this.Controls.Add(this.BoxNumtextBox2);
            this.Controls.Add(this.shift);
            this.Controls.Add(this.MO);
            this.Controls.Add(this.XN4);
            this.Controls.Add(this.BoxId2);
            this.Controls.Add(this.Seperate);
            this.Controls.Add(this.pcs3);
            this.Controls.Add(this.others);
            this.Controls.Add(this.accident);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pcs4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.XN3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.XN1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.S2);
            this.Controls.Add(this.BoxNum);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pcs1);
            this.Controls.Add(this.Qty);
            this.Controls.Add(this.S3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pcs2);
            this.Controls.Add(this.XN2);
            this.Controls.Add(this.ClockB);
            this.Controls.Add(this.LineB);
            this.Name = "FormLineBCellLoss";
            this.Text = "FormLineBCellLoss";
            this.Load += new System.EventHandler(this.FormLineBCellLoss_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.Seperate.ResumeLayout(false);
            this.Seperate.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LineB;
        private System.Windows.Forms.Label ClockB;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox S4;
        private System.Windows.Forms.Button Review;
        private System.Windows.Forms.Button Modify;
        private System.Windows.Forms.TextBox BoxNumTextbox;
        private System.Windows.Forms.TextBox BoxId;
        private System.Windows.Forms.Label MOLabel;
        private System.Windows.Forms.TextBox S1;
        private System.Windows.Forms.Button Submit;
        private System.Windows.Forms.TextBox BoxNumtextBox2;
        private System.Windows.Forms.ComboBox MO;
        private System.Windows.Forms.Label XN4;
        private System.Windows.Forms.TextBox BoxId2;
        private System.Windows.Forms.Panel Seperate;
        private System.Windows.Forms.Label FromStringer;
        private System.Windows.Forms.Label pcs3;
        private System.Windows.Forms.TextBox accident;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label FromIncoming;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label pcs4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label XN3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label XN1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox S2;
        private System.Windows.Forms.Label BoxNum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label pcs1;
        private System.Windows.Forms.Label Qty;
        private System.Windows.Forms.TextBox S3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label pcs2;
        private System.Windows.Forms.Label XN2;
        private System.Windows.Forms.ComboBox shift;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox others;
    }
}