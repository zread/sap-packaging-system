﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
    public partial class FormDeleteDataFromCDB : Form
    {
        private string CenterDB = "";

        public FormDeleteDataFromCDB()
        {
            InitializeComponent();
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            string delSql = "delete from T_MODULE_TEST where MODULE_SN='{0}'";
            string sn = tbSN.Text.Trim();
            if (string.IsNullOrEmpty(sn))
                return;
            delSql = string.Format(delSql, sn);
            if (!CheckModuleExist(sn))
            {
                if (ToolsClass.ExecuteNonQuery(delSql, CenterDB) > 0)
                    lstLog.Items.Add("组件:" + sn + " 删除成功");
                else
                    lstLog.Items.Add("无此组件:" + sn + " 测试数据");
            }
            else
            {
                lstLog.Items.Add("此组件："+sn+" 在包装数据中，无法删除");
            }
            tbSN.Text = "";
        }

        private bool CheckModuleExist(string Module_SN)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * from T_MODULE");
            strSql.Append(" where MODULE_SN=@MODULE_SN ");
            SqlParameter[] parameters = {
                    new SqlParameter("@MODULE_SN", SqlDbType.NVarChar,50)};
            parameters[0].Value = Module_SN;

            bool snIsHave = DbHelperSQL.Exists(strSql.ToString(), parameters);
            return snIsHave;
        }

        private void FormDeleteDataFromCDB_Load(object sender, EventArgs e)
        {
            lstLog.Items.Clear();
            CenterDB = ToolsClass.getConfig("CenterDB", false, "", "config.xml");
            DbHelperSQL.sqlConn = CenterDB;
        }
    }
}
