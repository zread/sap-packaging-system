﻿/*
 * Created by SharpDevelop.
 * Date: 2015/11/27
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CSICPR
{
	/// <summary>
	/// Description of FormQualityCheck.
	/// </summary>
	public partial class FormQualityInspection : Form
	{
		public FormQualityInspection()
		{
			InitializeComponent();
		}
		
		//--------------This is to ensure only one instance of this form is loaded
		private static FormQualityInspection theSingleton = null;
		public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormQualityInspection();
            	theSingleton.MdiParent = fm;                
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if(theSingleton.WindowState == FormWindowState.Minimized)
                {
                	theSingleton.WindowState = FormWindowState.Maximized;
                }
            }
        }
		
		void Button1Click(object sender, EventArgs e)
		{
			string sn = textBox1.Text.ToString().Trim();
			string moduleClass = "";
			
			if(sn.Length != 13 && sn.Length != 14)
			{
				MessageBox.Show("Serial number "+sn+" does not match the correct length!");
				return;
			}
					
			if(comboBox1.SelectedItem == null)
			{
				MessageBox.Show("Class field is empty. Please select the class.");
				return;
			}
			else
			{
				moduleClass = comboBox1.SelectedItem.ToString().Trim();
			}
			
			if(moduleClass == "A1" && comboBox2.SelectedItem == null)
			{
				MessageBox.Show("Please select defect code for class A1.");
				return;
			}
			
            if(!isPacked(textBox1.Text.ToString().Trim()))//add by shen.yu
            {
            	MessageBox.Show("Module "+sn+" has been packaged, its class cannot be changed!");
                return;
            }
            
            string defectcode = "";
            if(comboBox2.Text != "")
            {
            	defectcode = comboBox2.Text.Substring(0,5).Trim();
            }
            
            string sql = "insert into QualityJudge (SN, ModuleClass, timestamp, operator, defectcode, remark) values ('"+sn+"', '"+moduleClass+"', getdate(), '"+FormCover.CurrUserWorkID+"', '"+defectcode+"', '"+textBox2.Text+"')";
            ToolsClass.ExecuteNonQuery(sql,FormCover.ConnectionOldPackaging);
            
            MessageBox.Show("Module "+sn+" has been graded as class "+moduleClass);
            
            textBox1.Clear();
            comboBox1.SelectedItem = null;
            comboBox2.SelectedItem = null;
            textBox2.Clear();

		}
		
		void Button2MouseClick(object sender, MouseEventArgs e)
		{
			string sn = textBox1.Text.ToString().Trim();
			string moduleClass = "";
			
			if(sn.Length != 13 && sn.Length != 14)
			{
				MessageBox.Show("Serial number "+sn+" does not match the correct length!");
				return;
			}
			
			string sql = "select moduleclass from qualityjudge where id = (select max(ID) from qualityjudge where sn = '"+sn+"')";
			SqlDataReader sdr = ToolsClass.GetDataReader(sql);
			if(sdr != null && sdr.Read())
			{
				moduleClass = sdr.GetString(0);
			}
			else
			{
				moduleClass = "A";
			}
			sdr.Close();
            sdr = null;
            
            MessageBox.Show("Module "+sn+" is class "+moduleClass);
		}
		
		private bool isPacked(string sSerialNumber)
        {
            string sql = "select BoxID from Product where SN='{0}' and len(boxid)>0";
            sql = string.Format(sql, sSerialNumber);
            SqlDataReader sdr = ToolsClass.GetDataReader(sql);
            if(sdr != null && sdr.Read())
            {
            	return false;
            }
            sdr.Close();
            sdr = null;
            return true;
        }
		
		void TextBox1Enter(object sender, EventArgs e)
		{
			((Control)sender).BackColor = System.Drawing.Color.Yellow;
		}
		
		void TextBox1Leave(object sender, EventArgs e)
		{
			((Control)sender).BackColor = System.Drawing.SystemColors.Window;
		}
		
		void Button3Click(object sender, EventArgs e)
		{
			if(comboBox1.SelectedItem == null)
			{
				MessageBox.Show("Class field is empty. Please select the class.");
				return;
			}
			if(comboBox1.SelectedItem.ToString() == "A1" && comboBox2.SelectedItem == null)
			{
				MessageBox.Show("Please select defect code for class A1.");
				return;
			}
			
            string strTemp = textBox3.Text;
            string[] sDataSet = strTemp.Split('\n');
            for (int i = 0; i < sDataSet.Length; i++)
            {
                sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                if (sDataSet[i] != null && sDataSet[i] != "")
                    batchGrade(sDataSet[i]);
            }
            textBox3.Clear();
            comboBox1.SelectedItem = null;
            comboBox2.SelectedItem = null;
            textBox2.Clear();
		}
		
		void batchGrade(string sn)			
		{
			string moduleClass = comboBox1.SelectedItem.ToString().Trim();;
			string defectcode = "";
			if(comboBox2.SelectedItem != null)
			{
				defectcode = comboBox2.Text.Substring(0,6).Trim();
			}
			
			if(sn.Length != 13 && sn.Length != 14)
			{
				MessageBox.Show("Serial number "+sn+" does not match the correct length!");
				return;
			}			
			
            if (!isPacked(sn))
            {
            	MessageBox.Show("Module "+sn+" has been packaged, its class cannot be changed!");
                return;
            }
                  
            string sql = "insert into QualityJudge (SN, ModuleClass, timestamp, operator, defectcode, remark) values ('"+sn+"', '"+moduleClass+"', getdate(), '"+FormCover.CurrUserWorkID+"', '"+defectcode+"', '"+textBox2.Text+"')";
            ToolsClass.ExecuteNonQuery(sql,FormCover.ConnectionOldPackaging);
		}
		
		void Button2Click(object sender, EventArgs e)
		{
			
		}
	}
}
