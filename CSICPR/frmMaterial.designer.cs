﻿namespace CSICPR
{
    partial class frmMaterial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMaterial));
        	this.panel2 = new System.Windows.Forms.Panel();
        	this.Lb_Carton = new System.Windows.Forms.Label();
        	this.Gen_SN = new System.Windows.Forms.Button();
        	this.CartonNo = new System.Windows.Forms.TextBox();
        	this.panel1 = new System.Windows.Forms.Panel();
        	this.radioButtonNothing = new System.Windows.Forms.RadioButton();
        	this.radioButtonGreen = new System.Windows.Forms.RadioButton();
        	this.radioButtonWhite = new System.Windows.Forms.RadioButton();
        	this.dataGridView1 = new System.Windows.Forms.DataGridView();
        	this.Selected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
        	this.SN = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.PostedOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.MaterialTemplateName = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.LastUpdateUser = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.LastUpdateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.MaterialTemplateID = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.rowcolor = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.PicBoxWO = new System.Windows.Forms.PictureBox();
        	this.ddlWoType = new System.Windows.Forms.ComboBox();
        	this.txtWoOrder = new System.Windows.Forms.TextBox();
        	this.btnUnbind = new System.Windows.Forms.Button();
        	this.PanelSelect = new System.Windows.Forms.Panel();
        	this.CbLinenumber = new System.Windows.Forms.ComboBox();
        	this.btnDefine = new System.Windows.Forms.Button();
        	this.dataGridView2 = new System.Windows.Forms.DataGridView();
        	this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.sysID = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.QtyAvailable = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.btnSelect = new System.Windows.Forms.Button();
        	this.Label101 = new System.Windows.Forms.Label();
        	this.txtTemplateNameSelect = new System.Windows.Forms.TextBox();
        	this.btnUpdateBind = new System.Windows.Forms.Button();
        	this.btnBind = new System.Windows.Forms.Button();
        	this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        	this.BtLink = new System.Windows.Forms.Button();
        	this.panel2.SuspendLayout();
        	this.panel1.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
        	((System.ComponentModel.ISupportInitialize)(this.PicBoxWO)).BeginInit();
        	this.PanelSelect.SuspendLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
        	this.SuspendLayout();
        	// 
        	// panel2
        	// 
        	this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.panel2.Controls.Add(this.BtLink);
        	this.panel2.Controls.Add(this.Lb_Carton);
        	this.panel2.Controls.Add(this.Gen_SN);
        	this.panel2.Controls.Add(this.CartonNo);
        	this.panel2.Controls.Add(this.panel1);
        	this.panel2.Controls.Add(this.dataGridView1);
        	this.panel2.Controls.Add(this.PicBoxWO);
        	this.panel2.Controls.Add(this.ddlWoType);
        	this.panel2.Controls.Add(this.txtWoOrder);
        	this.panel2.Location = new System.Drawing.Point(12, 20);
        	this.panel2.Name = "panel2";
        	this.panel2.Size = new System.Drawing.Size(542, 537);
        	this.panel2.TabIndex = 184;
        	// 
        	// Lb_Carton
        	// 
        	this.Lb_Carton.Location = new System.Drawing.Point(3, 5);
        	this.Lb_Carton.Name = "Lb_Carton";
        	this.Lb_Carton.Size = new System.Drawing.Size(59, 20);
        	this.Lb_Carton.TabIndex = 200;
        	this.Lb_Carton.Text = "Carton No";
        	// 
        	// Gen_SN
        	// 
        	this.Gen_SN.AutoEllipsis = true;
        	this.Gen_SN.Location = new System.Drawing.Point(340, 97);
        	this.Gen_SN.Name = "Gen_SN";
        	this.Gen_SN.Size = new System.Drawing.Size(68, 42);
        	this.Gen_SN.TabIndex = 199;
        	this.Gen_SN.Text = "GenSN";
        	this.Gen_SN.UseVisualStyleBackColor = true;
        	this.Gen_SN.Click += new System.EventHandler(this.Gen_SNClick);
        	// 
        	// CartonNo
        	// 
        	this.CartonNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
        	this.CartonNo.Font = new System.Drawing.Font("SimSun", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
        	this.CartonNo.Location = new System.Drawing.Point(3, 33);
        	this.CartonNo.MaxLength = 5000;
        	this.CartonNo.Multiline = true;
        	this.CartonNo.Name = "CartonNo";
        	this.CartonNo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
        	this.CartonNo.Size = new System.Drawing.Size(284, 103);
        	this.CartonNo.TabIndex = 198;
        	// 
        	// panel1
        	// 
        	this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
        	this.panel1.Controls.Add(this.radioButtonNothing);
        	this.panel1.Controls.Add(this.radioButtonGreen);
        	this.panel1.Controls.Add(this.radioButtonWhite);
        	this.panel1.Location = new System.Drawing.Point(340, 5);
        	this.panel1.Name = "panel1";
        	this.panel1.Size = new System.Drawing.Size(197, 86);
        	this.panel1.TabIndex = 196;
        	// 
        	// radioButtonNothing
        	// 
        	this.radioButtonNothing.AutoSize = true;
        	this.radioButtonNothing.Location = new System.Drawing.Point(3, 7);
        	this.radioButtonNothing.Name = "radioButtonNothing";
        	this.radioButtonNothing.Size = new System.Drawing.Size(73, 17);
        	this.radioButtonNothing.TabIndex = 2;
        	this.radioButtonNothing.Text = "全部不选";
        	this.radioButtonNothing.UseVisualStyleBackColor = true;
        	this.radioButtonNothing.Click += new System.EventHandler(this.radioButtonNothing_Click);
        	// 
        	// radioButtonGreen
        	// 
        	this.radioButtonGreen.AutoSize = true;
        	this.radioButtonGreen.BackColor = System.Drawing.Color.LimeGreen;
        	this.radioButtonGreen.Location = new System.Drawing.Point(3, 53);
        	this.radioButtonGreen.Name = "radioButtonGreen";
        	this.radioButtonGreen.Size = new System.Drawing.Size(183, 17);
        	this.radioButtonGreen.TabIndex = 1;
        	this.radioButtonGreen.Text = "选绿色(已设定模板，尚未入库)";
        	this.radioButtonGreen.UseVisualStyleBackColor = false;
        	this.radioButtonGreen.Click += new System.EventHandler(this.radioButtonGreen_Click);
        	// 
        	// radioButtonWhite
        	// 
        	this.radioButtonWhite.AutoSize = true;
        	this.radioButtonWhite.BackColor = System.Drawing.SystemColors.InactiveCaption;
        	this.radioButtonWhite.Location = new System.Drawing.Point(3, 30);
        	this.radioButtonWhite.Name = "radioButtonWhite";
        	this.radioButtonWhite.Size = new System.Drawing.Size(127, 17);
        	this.radioButtonWhite.TabIndex = 0;
        	this.radioButtonWhite.Text = "选白色(未设定模板)";
        	this.radioButtonWhite.UseVisualStyleBackColor = false;
        	this.radioButtonWhite.CheckedChanged += new System.EventHandler(this.radioButtonWhite_Click);
        	// 
        	// dataGridView1
        	// 
        	this.dataGridView1.AllowUserToAddRows = false;
        	this.dataGridView1.AllowUserToDeleteRows = false;
        	this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
        	this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        	this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.Selected,
			this.SN,
			this.PostedOn,
			this.MaterialTemplateName,
			this.LastUpdateUser,
			this.LastUpdateTime,
			this.MaterialTemplateID,
			this.rowcolor});
        	this.dataGridView1.Location = new System.Drawing.Point(0, 145);
        	this.dataGridView1.Name = "dataGridView1";
        	this.dataGridView1.RowHeadersWidth = 20;
        	this.dataGridView1.Size = new System.Drawing.Size(538, 391);
        	this.dataGridView1.TabIndex = 195;
        	this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
        	// 
        	// Selected
        	// 
        	this.Selected.HeaderText = "选择";
        	this.Selected.Name = "Selected";
        	this.Selected.Width = 30;
        	// 
        	// SN
        	// 
        	this.SN.HeaderText = "组件号";
        	this.SN.Name = "SN";
        	this.SN.ReadOnly = true;
        	// 
        	// PostedOn
        	// 
        	this.PostedOn.HeaderText = "已入库";
        	this.PostedOn.Name = "PostedOn";
        	this.PostedOn.ReadOnly = true;
        	// 
        	// MaterialTemplateName
        	// 
        	this.MaterialTemplateName.HeaderText = "物料模板名称";
        	this.MaterialTemplateName.Name = "MaterialTemplateName";
        	this.MaterialTemplateName.ReadOnly = true;
        	// 
        	// LastUpdateUser
        	// 
        	this.LastUpdateUser.HeaderText = "最后更新人员";
        	this.LastUpdateUser.Name = "LastUpdateUser";
        	this.LastUpdateUser.ReadOnly = true;
        	// 
        	// LastUpdateTime
        	// 
        	this.LastUpdateTime.HeaderText = "最后更新时间";
        	this.LastUpdateTime.Name = "LastUpdateTime";
        	this.LastUpdateTime.ReadOnly = true;
        	// 
        	// MaterialTemplateID
        	// 
        	this.MaterialTemplateID.HeaderText = "物料模板ID";
        	this.MaterialTemplateID.Name = "MaterialTemplateID";
        	this.MaterialTemplateID.ReadOnly = true;
        	this.MaterialTemplateID.Visible = false;
        	// 
        	// rowcolor
        	// 
        	this.rowcolor.HeaderText = "颜色";
        	this.rowcolor.Name = "rowcolor";
        	this.rowcolor.ReadOnly = true;
        	this.rowcolor.Visible = false;
        	// 
        	// PicBoxWO
        	// 
        	this.PicBoxWO.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PicBoxWO.BackgroundImage")));
        	this.PicBoxWO.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicBoxWO.InitialImage")));
        	this.PicBoxWO.Location = new System.Drawing.Point(211, 4);
        	this.PicBoxWO.Name = "PicBoxWO";
        	this.PicBoxWO.Size = new System.Drawing.Size(27, 20);
        	this.PicBoxWO.TabIndex = 189;
        	this.PicBoxWO.TabStop = false;
        	this.PicBoxWO.Visible = false;
        	this.PicBoxWO.Click += new System.EventHandler(this.PicBoxWO_Click);
        	// 
        	// ddlWoType
        	// 
        	this.ddlWoType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.ddlWoType.FormattingEnabled = true;
        	this.ddlWoType.Location = new System.Drawing.Point(190, 2);
        	this.ddlWoType.Name = "ddlWoType";
        	this.ddlWoType.Size = new System.Drawing.Size(101, 21);
        	this.ddlWoType.TabIndex = 188;
        	this.ddlWoType.Visible = false;
        	// 
        	// txtWoOrder
        	// 
        	this.txtWoOrder.Location = new System.Drawing.Point(83, 2);
        	this.txtWoOrder.Name = "txtWoOrder";
        	this.txtWoOrder.Size = new System.Drawing.Size(101, 20);
        	this.txtWoOrder.TabIndex = 187;
        	this.txtWoOrder.Visible = false;
        	this.txtWoOrder.TextChanged += new System.EventHandler(this.TxtWoOrderTextChanged);
        	this.txtWoOrder.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtwoOrder_KeyPress);
        	// 
        	// btnUnbind
        	// 
        	this.btnUnbind.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.btnUnbind.Location = new System.Drawing.Point(561, 330);
        	this.btnUnbind.Name = "btnUnbind";
        	this.btnUnbind.Size = new System.Drawing.Size(96, 40);
        	this.btnUnbind.TabIndex = 6;
        	this.btnUnbind.Text = "删除模板";
        	this.btnUnbind.UseVisualStyleBackColor = true;
        	this.btnUnbind.Click += new System.EventHandler(this.btnUnbind_Click);
        	// 
        	// PanelSelect
        	// 
        	this.PanelSelect.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        	this.PanelSelect.Controls.Add(this.CbLinenumber);
        	this.PanelSelect.Controls.Add(this.btnDefine);
        	this.PanelSelect.Controls.Add(this.dataGridView2);
        	this.PanelSelect.Controls.Add(this.btnSelect);
        	this.PanelSelect.Controls.Add(this.Label101);
        	this.PanelSelect.Controls.Add(this.txtTemplateNameSelect);
        	this.PanelSelect.Location = new System.Drawing.Point(663, 12);
        	this.PanelSelect.Name = "PanelSelect";
        	this.PanelSelect.Size = new System.Drawing.Size(363, 537);
        	this.PanelSelect.TabIndex = 189;
        	this.PanelSelect.Tag = "";
        	// 
        	// CbLinenumber
        	// 
        	this.CbLinenumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        	this.CbLinenumber.FormattingEnabled = true;
        	this.CbLinenumber.Items.AddRange(new object[] {
			"",
			"A",
			"B",
			"C",
			"D"});
        	this.CbLinenumber.Location = new System.Drawing.Point(58, 7);
        	this.CbLinenumber.Name = "CbLinenumber";
        	this.CbLinenumber.Size = new System.Drawing.Size(101, 21);
        	this.CbLinenumber.TabIndex = 201;
        	// 
        	// btnDefine
        	// 
        	this.btnDefine.Location = new System.Drawing.Point(217, 34);
        	this.btnDefine.Name = "btnDefine";
        	this.btnDefine.Size = new System.Drawing.Size(141, 23);
        	this.btnDefine.TabIndex = 5;
        	this.btnDefine.Text = "定义或查看模板";
        	this.btnDefine.UseVisualStyleBackColor = true;
        	this.btnDefine.Click += new System.EventHandler(this.btnDefine_Click);
        	// 
        	// dataGridView2
        	// 
        	this.dataGridView2.AllowUserToAddRows = false;
        	this.dataGridView2.AllowUserToDeleteRows = false;
        	this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
        	this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        	this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.name,
			this.desc,
			this.sysID,
			this.QtyAvailable});
        	this.dataGridView2.Location = new System.Drawing.Point(0, 68);
        	this.dataGridView2.MultiSelect = false;
        	this.dataGridView2.Name = "dataGridView2";
        	this.dataGridView2.ReadOnly = true;
        	this.dataGridView2.RowHeadersWidth = 20;
        	this.dataGridView2.Size = new System.Drawing.Size(355, 468);
        	this.dataGridView2.TabIndex = 3;
        	// 
        	// name
        	// 
        	this.name.HeaderText = "物料模板名称";
        	this.name.Name = "name";
        	this.name.ReadOnly = true;
        	// 
        	// desc
        	// 
        	this.desc.HeaderText = "说明";
        	this.desc.Name = "desc";
        	this.desc.ReadOnly = true;
        	this.desc.Width = 150;
        	// 
        	// sysID
        	// 
        	this.sysID.HeaderText = "sysID";
        	this.sysID.Name = "sysID";
        	this.sysID.ReadOnly = true;
        	this.sysID.Visible = false;
        	this.sysID.Width = 30;
        	// 
        	// QtyAvailable
        	// 
        	this.QtyAvailable.HeaderText = "QtyAvailable";
        	this.QtyAvailable.Name = "QtyAvailable";
        	this.QtyAvailable.ReadOnly = true;
        	// 
        	// btnSelect
        	// 
        	this.btnSelect.Location = new System.Drawing.Point(274, 7);
        	this.btnSelect.Name = "btnSelect";
        	this.btnSelect.Size = new System.Drawing.Size(72, 23);
        	this.btnSelect.TabIndex = 2;
        	this.btnSelect.Text = "查询";
        	this.btnSelect.UseVisualStyleBackColor = true;
        	this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
        	// 
        	// Label101
        	// 
        	this.Label101.AutoSize = true;
        	this.Label101.Location = new System.Drawing.Point(0, 13);
        	this.Label101.Name = "Label101";
        	this.Label101.Size = new System.Drawing.Size(33, 13);
        	this.Label101.TabIndex = 1;
        	this.Label101.Text = "Line :";
        	// 
        	// txtTemplateNameSelect
        	// 
        	this.txtTemplateNameSelect.Location = new System.Drawing.Point(97, 9);
        	this.txtTemplateNameSelect.Name = "txtTemplateNameSelect";
        	this.txtTemplateNameSelect.Size = new System.Drawing.Size(153, 20);
        	this.txtTemplateNameSelect.TabIndex = 0;
        	this.txtTemplateNameSelect.Visible = false;
        	// 
        	// btnUpdateBind
        	// 
        	this.btnUpdateBind.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.btnUpdateBind.Location = new System.Drawing.Point(561, 262);
        	this.btnUpdateBind.Name = "btnUpdateBind";
        	this.btnUpdateBind.Size = new System.Drawing.Size(96, 40);
        	this.btnUpdateBind.TabIndex = 190;
        	this.btnUpdateBind.Text = "更新模板";
        	this.btnUpdateBind.UseVisualStyleBackColor = true;
        	this.btnUpdateBind.Click += new System.EventHandler(this.btnUpdateBind_Click);
        	// 
        	// btnBind
        	// 
        	this.btnBind.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        	this.btnBind.Location = new System.Drawing.Point(561, 194);
        	this.btnBind.Name = "btnBind";
        	this.btnBind.Size = new System.Drawing.Size(96, 40);
        	this.btnBind.TabIndex = 4;
        	this.btnBind.Text = "设定模板";
        	this.btnBind.UseVisualStyleBackColor = true;
        	this.btnBind.Click += new System.EventHandler(this.btnBind_Click);
        	// 
        	// dataGridViewTextBoxColumn1
        	// 
        	this.dataGridViewTextBoxColumn1.HeaderText = "SN";
        	this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
        	this.dataGridViewTextBoxColumn1.ReadOnly = true;
        	// 
        	// BtLink
        	// 
        	this.BtLink.AutoEllipsis = true;
        	this.BtLink.Location = new System.Drawing.Point(431, 97);
        	this.BtLink.Name = "BtLink";
        	this.BtLink.Size = new System.Drawing.Size(68, 42);
        	this.BtLink.TabIndex = 201;
        	this.BtLink.Text = "AutoLink";
        	this.BtLink.UseVisualStyleBackColor = true;
        	this.BtLink.Click += new System.EventHandler(this.BtLinkClick);
        	// 
        	// frmMaterial
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(1352, 720);
        	this.Controls.Add(this.btnUpdateBind);
        	this.Controls.Add(this.btnUnbind);
        	this.Controls.Add(this.PanelSelect);
        	this.Controls.Add(this.panel2);
        	this.Controls.Add(this.btnBind);
        	this.Name = "frmMaterial";
        	this.Text = "设定组件物料模板";
        	this.Load += new System.EventHandler(this.frmMaterial_Load);
        	this.panel2.ResumeLayout(false);
        	this.panel2.PerformLayout();
        	this.panel1.ResumeLayout(false);
        	this.panel1.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
        	((System.ComponentModel.ISupportInitialize)(this.PicBoxWO)).EndInit();
        	this.PanelSelect.ResumeLayout(false);
        	this.PanelSelect.PerformLayout();
        	((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
        	this.ResumeLayout(false);

        }
        private System.Windows.Forms.ComboBox CbLinenumber;
        private System.Windows.Forms.Label Lb_Carton;
        private System.Windows.Forms.DataGridViewTextBoxColumn QtyAvailable;
        private System.Windows.Forms.Button Gen_SN;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.TextBox CartonNo;

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.PictureBox PicBoxWO;
        private System.Windows.Forms.ComboBox ddlWoType;
        private System.Windows.Forms.TextBox txtWoOrder;
        private System.Windows.Forms.Panel PanelSelect;
        private System.Windows.Forms.Button btnBind;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Label Label101;
        private System.Windows.Forms.TextBox txtTemplateNameSelect;
        private System.Windows.Forms.Button btnDefine;
        private System.Windows.Forms.Button btnUnbind;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButtonGreen;
        private System.Windows.Forms.RadioButton radioButtonWhite;
        private System.Windows.Forms.Button btnUpdateBind;
        private System.Windows.Forms.RadioButton radioButtonNothing;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn sysID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Selected;
        private System.Windows.Forms.DataGridViewTextBoxColumn SN;
        private System.Windows.Forms.DataGridViewTextBoxColumn PostedOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaterialTemplateName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastUpdateUser;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastUpdateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaterialTemplateID;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowcolor;
        private System.Windows.Forms.Button BtLink;
    }
}