﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Xml;
using CSICPR.Properties;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.SqlClient;


namespace CSICPR
{
    public partial class FrmSapInventoryUploadManul : Form
    {
        public FrmSapInventoryUploadManul()
        {
            InitializeComponent();
             	string[] teco = GetTECOdata();
        	if(teco[0].ToString().Equals("True"))
            	this.CbTECO.Checked = true;
            else
            	this.CbTECO.Checked = false; 
            
            if (teco[4].Equals("True")) 
            	this.CbAuto.Checked = true;
            else
            	this.CbAuto.Checked = false; 
            if(!FormCover.HasPowerControl("TECO"))
            {
            	CbTECO.Enabled = false;
            	CbAuto.Enabled = false;
            }
            if(!FormCover.HasPowerControl("BtRetry"))
            	BtRetry.Visible = false;
            
        }
        private static List<LanguageItemModel> LanMessList;//定义语言集
        #region 共有变量
        private static FrmSapInventoryUploadManul _theSingleton;
        public static void Instance(Form fm)
        {
            if (null == _theSingleton || _theSingleton.IsDisposed)
            {
                _theSingleton = new FrmSapInventoryUploadManul
                {
                    MdiParent = fm,
                    WindowState = FormWindowState.Maximized
                };
                _theSingleton.Show();
            }
            else
            {
                _theSingleton.Activate();
                if (_theSingleton.WindowState == FormWindowState.Minimized)
                    _theSingleton.WindowState = FormWindowState.Maximized;
            }
           
              
        }
        #endregion

        private List<TsapReceiptUploadModule> _tsapReceiptUploadModules = new List<TsapReceiptUploadModule>();

        private void FrmSapInventoryUploadManul_Load(object sender, EventArgs e)
        {
            
            # region 多语言
            LanMessList = LanguageHelper.getLanguageItem(this.Name, "MESSAGE");
            LanguageHelper.getNames(this);
            # endregion
			lstView.Columns.Add(LanguageHelper.GetMessage(LanMessList, "Message1","提示信息"), 630, HorizontalAlignment.Left);            
            dgvData.AutoGenerateColumns = false;

        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
        	if(CbTECO.Checked)
        	{
        		ToolsClass.Log("No Finish Goods Receive Allowed, please contact Supervisor!", "ABNORMAL", lstView);
        		return;
        	}
        	if (_tsapReceiptUploadModules == null)
                _tsapReceiptUploadModules = new List<TsapReceiptUploadModule>();
            lblModuleCntValue.Text = _tsapReceiptUploadModules.Select(p => p.ModuleSn).Distinct().Count().ToString();
            lblTestPowerSumValue.Text = _tsapReceiptUploadModules.Select(p => decimal.Parse(p.TestPower)).Sum().ToString();
            var tempCartonNos = txtCartons.Lines;
            if (tempCartonNos.Length <= 0)
            {
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message2","请输入托号"), "ABNORMAL", lstView);
               	txtCartons.Clear();
                txtCartons.Focus();
                return;
            }
            var cartonNos = new List<string>();
            for (var i = 0; i < tempCartonNos.Length; i++)
            {
                var cartonNo = tempCartonNos[i];
                if (string.IsNullOrEmpty(cartonNo.Trim()))
                    continue;
                cartonNos.Add(cartonNo);
            }
            foreach (var cartonNo in cartonNos)
            {
                string msg;
                #region Testing Esb Query Jacky 2016.05.09 

                	if(IsEsbedSapCheck(cartonNo,out msg))
                	{
                		ToolsClass.Log(msg, "ABNORMAL", lstView);
                		return;
                	}
                #endregion
                
                if (!DataAccess.CanUploadToSap(cartonNo, FormCover.connectionBase, out msg))
                {
                    ToolsClass.Log(msg, "ABNORMAL", lstView);
                    txtCartons.Clear();
                    txtCartons.Focus();
                    return;
                }
                if (!CheckRepeat(cartonNo))
                    return;
                var tsapReceiptUploadModules = DataAccess.QueryTsapReceiptUploadModuleByCartonNo(cartonNo,
                    FormCover.connectionBase);
                if (tsapReceiptUploadModules != null && tsapReceiptUploadModules.Count > 0)
                {
                    tsapReceiptUploadModules.ForEach(p => p.CommandName = LanguageHelper.GetMessage(LanMessList, "Message3", "移除"));
                    _tsapReceiptUploadModules.AddRange(tsapReceiptUploadModules);
                    dgvData.DataSource = null;
                    dgvData.DataSource = _tsapReceiptUploadModules;
                    lblModuleCntValue.Text = _tsapReceiptUploadModules.Select(p => p.ModuleSn).Distinct().Count().ToString();
                    lblTestPowerSumValue.Text = _tsapReceiptUploadModules.Select(p => decimal.Parse(p.TestPower)).Sum().ToString();
                }
            }

            txtCartons.Clear();
            txtCartons.Focus();
        }
		
        private bool IsEsbedSapCheck(string Carton,out string message)
        {
        	     
        	List<string> T = DataAccess.QueryEsbInterfaceAddressUserIdPW(FormCover.CurrentFactory, FormCover.InterfaceConnString);
        	string host = T[0].Substring(7,12);
        	string user = T[1];
        	string password = T[2];
        	string dbcon = T[3];
        	string sql = " Select * from stockin where Carton_no = '{0}' and  process_flag = '3' and status = 'S'";
	               sql = string.Format(sql,Carton);
			MySqlConnection con = new MySqlConnection("host="+ host +";user= "+ user +";password="+password+";database="+dbcon+";");
			MySqlCommand cmd = new MySqlCommand(sql, con);
			con.Open();
        	try
        	{
					MySqlDataReader  reader = cmd.ExecuteReader();
					if(reader!= null && reader.Read())
					{
						message = "Carton No.: "+Carton+ " is already uploaded to SAP!";
						return true;
					}
					message = "";
					return false;  
        	}
        	catch(MySql.Data.MySqlClient.MySqlException ex)
        	{
        		message = ex.Message;
        		return true;
        		
        	} 
			finally
			{
				con.Close();
				con.Dispose();
			}
        
		}
         private bool IsHalfEsbSapCheck(string Carton,out string message)
        {
        	         
        	
        	List<string> T = DataAccess.QueryEsbInterfaceAddressUserIdPW(FormCover.CurrentFactory, FormCover.InterfaceConnString);
        	string host = T[0].Substring(7,12);
        	string user = T[1];
        	string password = T[2];
        	string dbcon = T[3];
        	bool ret = false;
        	string sql = "";
					#region leave the carton at 2nd call
						sql = " Select * from stockin where Carton_no = '{0}' and  process_flag = '3' and status = 'F'";
						sql = string.Format(sql,Carton);
						MySqlConnection con1 = new MySqlConnection("host="+ host +";user= "+ user +";password="+password+";database="+dbcon+";");					
						MySqlCommand cmd1 = new MySqlCommand(sql, con1);
						con1.Open();
						cmd1 = new MySqlCommand(sql, con1);
						
					try 
					{
						MySqlDataReader  reader  = cmd1.ExecuteReader();
						if(reader!= null && reader.Read())
						{
							message = "Carton No.: "+Carton+ " is waiting for pack in SAP, re-submit not required!";
							ToolsClass.SendEmail(message);
							ret = true;							
						}
						message = "";
						reader.Close();
						return ret;
					} 
					catch (MySql.Data.MySqlClient.MySqlException ex) {
						message = ex.Message;
        				return true;
						
					}
					finally
					{
						con1.Close();
						con1.Dispose();
					}
						
					#endregion
					               
        
		}
        
        private bool CheckRepeat(string cartonNo)
        {
            foreach (DataGridViewRow dataRow in dgvData.Rows)
            {
                var tsapReceiptUploadModule = dataRow.DataBoundItem as TsapReceiptUploadModule;
                if (tsapReceiptUploadModule == null)
                    continue;
                if (tsapReceiptUploadModule.CartonNo.Equals(cartonNo))
                {
                    ToolsClass.Log(string.Format(LanguageHelper.GetMessage(LanMessList, "Message4", "托号：{0}已在列表第 {1}行"), cartonNo, (dataRow.Index + 1)), "ABNORMAL", lstView);
                    return false;
                }
                
            }
            return true;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            _tsapReceiptUploadModules = new List<TsapReceiptUploadModule>();
            lblModuleCntValue.Text = _tsapReceiptUploadModules.Select(p => p.ModuleSn).Distinct().Count().ToString();
            lblTestPowerSumValue.Text = _tsapReceiptUploadModules.Select(p => decimal.Parse(p.TestPower)).Sum().ToString();
            dgvData.DataSource = null;
            //dgvData.DataSource = _tsapReceiptUploadModules;
			//dgvData.Refresh();
            txtCartons.Text = string.Empty;
            txtJobNo.Text = string.Empty;
            txtCartons.SelectAll();
        }

        private void lstView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                if (lstView.SelectedItems.Count > 0)
                {
                    if (lstView.SelectedItems[0].Text != "")
                        Clipboard.SetDataObject(lstView.SelectedItems[0].Text);
                }
            }
        }

        private void dgvData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var columnIndex = e.ColumnIndex;
            //按钮所在列为第一列，列下标从0开始的  
            if (columnIndex != 0)
                return;

            var tsapReceiptUploadModule = dgvData.Rows[e.RowIndex].DataBoundItem as TsapReceiptUploadModule;
            if (tsapReceiptUploadModule == null)
                return;
            var msg = string.Format(LanguageHelper.GetMessage(LanMessList, "Message5", "确定要移除内部托号{0}的数据吗？"), tsapReceiptUploadModule.CartonNo);
            if (DialogResult.No == MessageBox.Show(msg, Resources.FrmSapInventoryUploadManul_dgvData_CellContentClick_Info, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                return;
            var findAll = _tsapReceiptUploadModules.FindAll(p => p.CartonNo == tsapReceiptUploadModule.CartonNo);
            if (findAll.Count <= 0)
                return;
            findAll.ForEach(p => _tsapReceiptUploadModules.Remove(p));
            dgvData.DataSource = null;
            dgvData.DataSource = _tsapReceiptUploadModules.Count <= 0 ? null : _tsapReceiptUploadModules;
            if (_tsapReceiptUploadModules == null)
                _tsapReceiptUploadModules = new List<TsapReceiptUploadModule>();
            lblModuleCntValue.Text = _tsapReceiptUploadModules.Select(p => p.ModuleSn).Distinct().Count().ToString();
            lblTestPowerSumValue.Text = _tsapReceiptUploadModules.Select(p => decimal.Parse(p.TestPower)).Sum().ToString();
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if (_tsapReceiptUploadModules == null || _tsapReceiptUploadModules.Count <= 0)
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message6","未找到待上传数据"), "ABNORMAL", lstView);
                    return;
                }
               
                var cartonNos = _tsapReceiptUploadModules.Select(p => p.CartonNo).Distinct().ToList();
                foreach (var cartonNo in cartonNos)
                {
                    string msg;
                   
                    if (IsHalfEsbSapCheck(cartonNo,out msg)) {
                		ToolsClass.Log(msg, "ABNORMAL", lstView);
                		return;
               		 }
                    if (!DataAccess.CanUploadToSap(cartonNo, FormCover.connectionBase, out msg))
                    {
                        ToolsClass.Log(msg, "ABNORMAL", lstView);
                        return;
                    }
                }
                var jobNo = txtJobNo.Text.Trim();
                if (string.IsNullOrEmpty(jobNo) || jobNo.Trim().Length <= 0)
                {
                    //if (DialogResult.No ==
                    //    MessageBox.Show(Resources.FrmSapInventoryUploadManul_btnUpload_Click_NotInputJobNoConfirm,
                    //        Resources.FrmSapInventoryUploadManul_dgvData_CellContentClick_Info, MessageBoxButtons.YesNo,
                    //        MessageBoxIcon.Question))
                    //    return;
                    jobNo = "NULL";
                }
                else
                {
                    if (DialogResult.No ==
                        MessageBox.Show(Resources.FrmSapInventoryUploadManul_btnUpload_Click_CheckJobNoFullConfirm,
                            Resources.FrmSapInventoryUploadManul_dgvData_CellContentClick_Info, MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question))
                        return;
//                    if (IsUploadedToSap(jobNo))
//                    {
//                        ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message7","柜号已上传至SAP:") + jobNo + "", "ABNORMAL", lstView);
//                        return;
//                    }
                }
                //var dateTime = DT.DateTime().LongDateTime2;//Jacky
                var dateTime = DT.DateTime().ShortDate;                
                var groupHistKey = FormCover.CurrentFactory + Guid.NewGuid().ToString("N");
                _tsapReceiptUploadModules.ForEach(p =>
                {
                    p.CreatedOn = dateTime;
                    p.CreatedBy = FormCover.CurrUserName;
                    p.JobNo = jobNo;
                    p.GroupHistKey = groupHistKey;
                    p.UploadStatus = "Finished";
                });
                var tsapReceiptUploadJobNo = new TsapReceiptUploadJobNo
                {
                    Sysid = Guid.NewGuid().ToString("N"),
                    CreatedOn = dateTime,
                    CreatedBy = FormCover.CurrUserName,
                    JobNo = jobNo,
                    InnerJobNo = string.Empty,
                    GroupHistKey = groupHistKey,
                    Resv01 = FormCover.CurrentFactory
                };
                if (!UploadToEsb(_tsapReceiptUploadModules))
                    return;
                var actionTxnId = Guid.NewGuid().ToString("N") + DateTime.Now.ToString("fff");
                //var actionDate = DT.DateTime().LongDateTime; //jacky
              	var actionDate = DT.DateTime().ShortDate;
               
                if (!ProductStorageDAL.SaveUploadEsbResult(tsapReceiptUploadJobNo, _tsapReceiptUploadModules, actionTxnId, actionDate))
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message8","保存上传结果失败"), "ABNORMAL", lstView);
                    return;
                }
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message9","上传成功"), "NORMAL", lstView);
                btnReset_Click(null, null);
            }
            catch (Exception ex)
            {
                ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message10", "上传出错：") + ex.Message, "ABNORMAL", lstView);
            }
        }

        private bool IsUploadedToSap(string jobNo)
        {
            //http://xxx:xx/QuerySAPInventoryStatus?workshop=?&jobno=?&innerjobno=?&datefrom=?&dateto=?
            var url = DataAccess.QueryEsbInterfaceAddress(FormCover.CurrentFactory, FormCover.InterfaceConnString) +
                      "QuerySAPInventoryStatus?workshop=" + FormCover.CurrentFactory +
                      "&jobno=" + jobNo;
            var req = WebRequest.Create(url);
            req.Timeout = 15000;
            ToolsClass.Log("IsUploadedToSap::" + url);
            req.Method = "GET";
            try
            {
                //返回 HttpWebResponse
                var hwRes = req.GetResponse() as HttpWebResponse;
                string result;
                if (hwRes == null)
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message11", "服务器反馈结果为空"), "ABNORMAL", lstView);
                    return false;
                }
                if (hwRes.StatusCode == HttpStatusCode.OK)
                {
                    //是否返回成功
                    var rStream = hwRes.GetResponseStream();
                    //流读取
                    var sr = new StreamReader(rStream, Encoding.UTF8);
                    result = sr.ReadToEnd();
                    sr.Close();
                    rStream.Close();
                }
                else
                {
                    result = LanguageHelper.GetMessage(LanMessList, "Message17", "连接错误");
                }
                //关闭
                hwRes.Close();
                result = result.ToUpper();
                ToolsClass.Log("IsUploadedToSap::" + result);
                result = result.Replace(@"<?XML VERSION=""1.0"" ENCODING=""UTF-8""?>", string.Empty);
                result = result.Replace(@"<SOAP:ENVELOPE XMLNS:SOAP=""HTTP://SCHEMAS.XMLSOAP.ORG/SOAP/ENVELOPE/"">", string.Empty);
                result = result.Replace(@"<SOAP:BODY>", @"<SOAPBODY>");
                result = result.Replace(@"</SOAP:BODY>", @"</SOAPBODY>");
                result = result.Replace(@"</SOAP:ENVELOPE>", string.Empty);
                var ds = ProductStorageDAL.ReadXmlToDataSet(result);
                if (ds == null || ds.Tables == null || ds.Tables.Count <= 0 ||
                    ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count <= 0)
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message13","服务器反馈结果与接口定义不符"), "ABNORMAL", lstView);
                    return false;
                }
                var responseStatusTable = ds.Tables["RESPONSESTATUS"];
                if (responseStatusTable == null)
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message13","服务器反馈结果与接口定义不符"), "ABNORMAL", lstView);
                    return false;
                }
                var statusCode = responseStatusTable.Rows[0][0].ToString();
                var statusMessage = responseStatusTable.Rows[0][1].ToString();
                if (statusCode == "000")
                {
                    return ds.Tables["ITEM"] != null;
                }
                else
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message14", "查询柜号") + jobNo + LanguageHelper.GetMessage(LanMessList, "Message15", "历史上传信息出错::") + statusMessage, "ABNORMAL", lstView);
                    return false;
                }
            }
            catch (WebException ex)
            {
                var responseFromServer = ex.Message + " ";
                if (ex.Response != null)
                {
                    using (var response = ex.Response)
                    {
                        var data = response.GetResponseStream();
                        using (var reader = new StreamReader(data))
                        {
                            responseFromServer += reader.ReadToEnd();
                        }
                    }
                }
                ToolsClass.Log(responseFromServer, "ABNORMAL", lstView);
                return false;
            }
            catch (Exception ex)
            {
                ToolsClass.Log(ex.Message, "ABNORMAL", lstView);
                return false;
            }
        }

        private bool UploadToEsb(List<TsapReceiptUploadModule> tsapReceiptUploadModules)
        {
            //var url = DataAccess.QueryEsbInterfaceAddress(FormCover.CurrentFactory, FormCover.InterfaceConnString) +"SAPInventory"; 
           	
            var url = DataAccess.QueryEsbInterfaceAddress(FormCover.CurrentFactory, FormCover.InterfaceConnString) +"CanadianSolar/QuerySAPInventoryStatus";
			

            //var req =WebRequest.Create(url);
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url); 
            req.KeepAlive = false;
            req.Timeout = 120000;
            req.ServicePoint.ConnectionLeaseTimeout = 120000;
    		req.ServicePoint.MaxIdleTime = 120000;
			
            ToolsClass.Log("UploadToEsb::" + url);
            
            req.Method = "POST";
            var soap = BuildSoapXml(tsapReceiptUploadModules);

            req.ContentType = "text/xml;charset=UTF-8";

           
            ToolsClass.Log("UploadToEsb::" + soap);
            //字符转字节
            var bytes = Encoding.UTF8.GetBytes(soap);           

            var writer = req.GetRequestStream();
            writer.Write(bytes, 0, bytes.Length);
            writer.Flush();
            writer.Close();
            try
            {
                //返回 HttpWebResponse
               // var hwRes = req.GetResponse() as HttpWebResponse;
                HttpWebResponse hwRes = req.GetResponse() as HttpWebResponse;
                string result;
                if (hwRes == null)
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message16", "服务器反馈结果为空"), "ABNORMAL", lstView);
                    return false;
                }
                if (hwRes.StatusCode == HttpStatusCode.OK)
                {
                    //是否返回成功
                    var rStream = hwRes.GetResponseStream();
                    //流读取
                    var sr = new StreamReader(rStream, Encoding.UTF8);
                    result = sr.ReadToEnd();
                    sr.Close();
                    rStream.Close();
                }
                else
                {
                    result = LanguageHelper.GetMessage(LanMessList, "Message17", "连接错误");
                }
                //关闭
                hwRes.Close();
                result = result.ToUpper();
                ToolsClass.Log("UploadToEsb::" + result);
                result = result.Replace(@"<?XML VERSION=""1.0"" ENCODING=""UTF-8""?>", string.Empty);
                result = result.Replace(@"<SOAP:ENVELOPE XMLNS:SOAP=""HTTP://SCHEMAS.XMLSOAP.ORG/SOAP/ENVELOPE/"">", string.Empty);
                result = result.Replace(@"<ENTERWAREHOUSERESPONSE XMLNS=""HTTP://ESB.CANADIANSOLAR.COM"">",@"<ENTERWAREHOUSERESPONSE>");
                result = result.Replace(@"<SOAP:BODY>", string.Empty);
                result = result.Replace(@"</SOAP:BODY>", string.Empty);
                result = result.Replace(@"</SOAP:ENVELOPE>", string.Empty);
                //yuhua
                result = result.Replace(@"<RETURN>", string.Empty);
				result = result.Replace(@"</RETURN>", string.Empty);

                #region testing
//		                using (System.IO.StreamWriter file = 
//		            new System.IO.StreamWriter(@"C:\Users\Jacky.li\Documents\Jacky\WriteLineschina.txt"))
//		        {
//		           
//                	file.WriteLine(result.ToString());
//		                
//		            
//		        }
                #endregion
                
                
                var ds = ProductStorageDAL.ReadXmlToDataSet(result);
                if (ds == null || ds.Tables == null || ds.Tables.Count <= 0 ||
                    ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count <= 0)
                {
                    ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message13", "服务器反馈结果与接口定义不符"), "ABNORMAL", lstView);
                    return false;
                }
                var row = ds.Tables[0].Rows[0];
                var isSuccess = row[0].ToString().ToUpper();
                var message = row[1].ToString();
                if (isSuccess != "true".ToUpper())
                {
					//By pass carton exist error in SAP.Jacky 2016.05.10
                	if(message.Substring(message.IndexOf("在SAP")+4,6).Equals("系统中已存在"))
						return true;
                	if(message.IndexOf("电池片晶面不能为空")!=-1)
                	{
                		ToolsClass.Log("Cell pattern cannot be empty, Materialcode/Batch no. "+message.Substring(message.IndexOf("电池片晶面不能为空")+17,29),"ABNORMAL", lstView);
                	}
                	else					             		
                	ToolsClass.Log(LanguageHelper.GetMessage(LanMessList, "Message18", "上传失败::") + message, "ABNORMAL", lstView);
                    return false;
                }
                return true;
            }
            catch (WebException ex)
            {
                var responseFromServer = ex.Message + " ";
                if (ex.Response != null)
                {
                    using (var response = ex.Response)
                    {
                        var data = response.GetResponseStream();
                        using (var reader = new StreamReader(data))
                        {
                            responseFromServer += reader.ReadToEnd();
                        }
                    }
                }
                ToolsClass.Log(responseFromServer, "ABNORMAL", lstView);
                return false;
            }
            catch (Exception ex)
            {
                ToolsClass.Log(ex.Message, "ABNORMAL", lstView);
                return false;
            }
            finally
            {
            	req.Abort();
            }
        }

        private string BuildSoapXml(List<TsapReceiptUploadModule> tsapReceiptUploadModules)
        {
            var cartons =
                tsapReceiptUploadModules.Select(
                    p => new { p.CartonNo, p.CustomerCartonNo, p.InnerJobNo, p.JobNo, p.ModuleColor }).
                    Distinct().ToList();

            
           // Encoding Utf8 = new UTF8Encoding(false);     //Initializes a new instance of the UTF8Encoding class. A parameter specifies whether to provide a Unicode byte order mark.  
		         
            
            var st = new MemoryStream();
            //初始化一个XmlTextWriter,编码方式为Encoding.UTF8
            
            var tr = new XmlTextWriter(st, Encoding.UTF8);
            tr.WriteStartDocument();
			
            tr.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");//soap:Envelope
           //tr.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/wsdl");//soap:Envelope
            tr.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");

            tr.WriteStartElement("Header", "http://schemas.xmlsoap.org/soap/envelope/");//soap:Header
            tr.WriteStartElement(null, "RequestHeader", "http://esb.canadiansolar.com");//RequestHeader ***changed this from NULL
            tr.WriteElementString("AppCode", "PackingSystem_" + FormCover.CurrentFactory);
            tr.WriteElementString("Version", "1");
            tr.WriteElementString("ClientTimestamp", tsapReceiptUploadModules[0].CreatedOn);
            tr.WriteElementString("GUID", tsapReceiptUploadModules[0].GroupHistKey);
            tr.WriteEndElement();//RequestHeader
            tr.WriteEndElement();//soap:Header

            tr.WriteStartElement("Body", "http://schemas.xmlsoap.org/soap/envelope/");//soap:Body
            tr.WriteStartElement(null, "EnterWarehouse", "http://esb.canadiansolar.com");//EnterWarehouse ***changed this from NULL
            tr.WriteStartElement(null, "PackingData", null);//PackingData
            //循环Carton
            for (var i = 0; i < cartons.Count; i++)
            {
                var carton = cartons[i];
                var findByCartonNo =
                    tsapReceiptUploadModules.FindAll(p => p.CartonNo == carton.CartonNo)
                        .OrderBy(p => p.FinishedOn)
                        .Take(1).FirstOrDefault();
                tr.WriteStartElement(null, "Carton", null); //Carton
                tr.WriteElementString("CartonNo", carton.CartonNo);
                tr.WriteElementString("CustomerCartonNo", carton.CustomerCartonNo);
                tr.WriteElementString("InnerJobNo", carton.InnerJobNo);
                tr.WriteElementString("JobNo", carton.JobNo);
                //tr.WriteElementString("FinishedOn", findByCartonNo.FinishedOn);
                tr.WriteElementString("FinishedOn", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));//jacky
                //tr.WriteElementString("FinishedOn", DTpick.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                
                tr.WriteElementString("ModuleColor", carton.ModuleColor);
                tr.WriteStartElement(null, "Modules", null); //Modules
                //循环Module
                var modules = tsapReceiptUploadModules.FindAll(p => p.CartonNo == carton.CartonNo);
                foreach (var module in modules)
                {
                    tr.WriteStartElement(null, "Module", null); //Module
                    tr.WriteElementString("ModuleSN", module.ModuleSn);
                    tr.WriteElementString("OrderNo", module.OrderNo);
                    tr.WriteElementString("SalesOrderNo", module.SalesOrderNo);
                    tr.WriteElementString("SalesItemNo", module.SalesItemNo);
                    tr.WriteElementString("OrderStatus", module.OrderStatus);
                    tr.WriteElementString("ProductCode", module.ProductCode);
                    tr.WriteElementString("Unit", module.Unit);
                    tr.WriteElementString("Factory", module.Factory);
                    tr.WriteElementString("Workshop", module.Workshop);
                    tr.WriteElementString("PackingLocation", module.PackingLocation);
                    tr.WriteElementString("PackingMode", module.PackingMode);
                    tr.WriteElementString("CellEff", module.CellEff);
                    tr.WriteElementString("TestPower", module.TestPower);
                    tr.WriteElementString("StdPower", module.StdPower);
                    tr.WriteElementString("ModuleGrade", module.ModuleGrade);
                    tr.WriteElementString("ByIm", module.ByIm);
                    tr.WriteElementString("Tolerance", module.Tolerance);
                    tr.WriteElementString("CellCode", module.CellCode);
                    tr.WriteElementString("CellBatch", module.CellBatch);
                    tr.WriteElementString("CellPrintMode", module.CellPrintMode);
                    tr.WriteElementString("GlassCode", module.GlassCode);
                    tr.WriteElementString("GlassBatch", module.GlassBatch);
                    tr.WriteElementString("EvaCode", module.EvaCode);
                    tr.WriteElementString("EvaBatch", module.EvaBatch);
                    tr.WriteElementString("TptCode", module.TptCode);
                    tr.WriteElementString("TptBatch", module.TptBatch);
                    tr.WriteElementString("ConBoxCode", module.ConboxCode);
                    tr.WriteElementString("ConBoxBatch", module.ConboxBatch);
                    tr.WriteElementString("ConBoxType", module.ConboxType);
                    tr.WriteElementString("LongFrameCode", module.LongFrameCode);
                    tr.WriteElementString("LongFrameBatch", module.LongFrameBatch);
                    tr.WriteElementString("ShortFrameCode", module.ShortFrameCode);
                    tr.WriteElementString("ShortFrameBatch", module.ShortFrameBatch);
                    tr.WriteElementString("GlassThickness", module.GlassThickness);
                    tr.WriteElementString("IsRework", module.IsRework);
                    tr.WriteElementString("IsByProduction", module.IsByProduction);
                    tr.WriteElementString("StdPowerLevel", module.Resv01);
                    tr.WriteElementString("Market", module.Resv02);
                    tr.WriteElementString("LID", module.Resv03); //jakcy20160203

                    tr.WriteEndElement(); //Module
                }
                tr.WriteEndElement(); //Modules
                tr.WriteEndElement(); //Carton 
            }

            tr.WriteEndElement();//PackingData
            tr.WriteEndElement();//EnterWarehouse
            tr.WriteEndElement();//soap:Body

            tr.WriteEndElement();//soap:Envelope

            tr.WriteEndDocument();

            tr.Flush();

            var bytes = st.ToArray();
            st.Close();
            return Encoding.UTF8.GetString(bytes);
        }
		void CbTECOCheckedChanged(object sender, EventArgs e)
		{
			if(!FormCover.HasPowerControl("TECO"))
				return;			
			string[] Dtteco = GetTECOdata();
			string sql = @" update TECO set ISTECO = '{0}', ModifiedBy = '{1}', LastModifiedBy = '{2}', TimeModified = GETDATE() ";
			if(CbTECO.Checked)
				sql = string.Format(sql,"True",FormCover.CurrUserName,Dtteco[1]);
			else
				sql = string.Format(sql,"False",FormCover.CurrUserName,Dtteco[1]);
			ToolsClass.ExecuteNonQuery(sql);
		}
		void CbAutoCheckedChanged(object sender, EventArgs e)
		{
			if(!FormCover.HasPowerControl("TECO"))
				return;			
			string[] Dtteco = GetTECOdata();
			string sql = @" update TECO set  AutoUpload = '{0}', ModifiedBy = '{1}', LastModifiedBy = '{2}', TimeModified = GETDATE() ";
			if(CbAuto.Checked)
				sql = string.Format(sql,"True",FormCover.CurrUserName,Dtteco[1]);
			else
				sql = string.Format(sql,"False",FormCover.CurrUserName,Dtteco[1]);
			ToolsClass.ExecuteNonQuery(sql);
		}
		
		
		public static string[] GetTECOdata()
		{
			string sql = @"Select * from TECO";
			string[] Ret = new string[5];
			
			DataTable dtCheck = ToolsClass.GetDataTable(sql);
			if(dtCheck!= null && dtCheck.Rows.Count > 0)
			{
				for(int i = 0; i < 5 ; i ++)
					Ret[i] = dtCheck.Rows[0][i].ToString();
			}
			
//			SqlDataReader rd = ToolsClass.GetDataReader(sql);		
//			while(rd.Read())
//			{
//				for(int i = 0; i < 3 ; i ++)
//					Ret[i] = rd.GetString(i);
//			}
			return Ret;			
		}
		void BtRetryClick(object sender, EventArgs e)
		{
			string sql = @"update SapUploadStatus set UploadStatus = 0 where Carton_no = '{0}' and [UploadStatus] > 1";
			string strTemp = txtCartons.Text;
			int count = 0;
                    string[] sDataSet = strTemp.Split('\n');
                    for (int i = 0; i < sDataSet.Length; i++)
                    {
                        sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                        if(string.IsNullOrEmpty(sDataSet[i]))
                        	continue;
                        string sqlupdate = string.Format(sql,sDataSet[i]);
                        count = count + ToolsClass.ExecuteNonQuery(sqlupdate);
                    }
                    MessageBox.Show((count > 0)?"Cartons Retried":"Retry failed");  
		}
	
    }
}