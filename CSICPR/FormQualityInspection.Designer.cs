﻿/*
 * Created by SharpDevelop.
 * Date: 2015/11/27
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace CSICPR
{
	partial class FormQualityInspection
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>adimin
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.button2 = new System.Windows.Forms.Button();
			this.comboBox2 = new System.Windows.Forms.ComboBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.button3 = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(140, 24);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(176, 20);
			this.textBox1.TabIndex = 0;
			this.textBox1.Enter += new System.EventHandler(this.TextBox1Enter);
			this.textBox1.Leave += new System.EventHandler(this.TextBox1Leave);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(227, 270);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(89, 34);
			this.button1.TabIndex = 1;
			this.button1.Text = "Grade";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// comboBox1
			// 
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Items.AddRange(new object[] {
									"A",
									"A-",
									"B",
									"C",
									"F"});
			this.comboBox1.Location = new System.Drawing.Point(140, 70);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(63, 21);
			this.comboBox1.TabIndex = 2;
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(227, 215);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(89, 35);
			this.button2.TabIndex = 3;
			this.button2.Text = "Check";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.Button2Click);
			this.button2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Button2MouseClick);
			// 
			// comboBox2
			// 
			this.comboBox2.FormattingEnabled = true;
			this.comboBox2.Items.AddRange(new object[] {
									"00A001 - Color Variation",
									"00A002 - Surface Chips",
									"00A003 - Hole in Cell",
									"00A004 - Cracked Through Cell",
									"00A005 - V Type Breach",
									"00A006 - Cracked Corner",
									"00A007 - Cell Edge Chips",
									"00A008 - Broken Gridlines",
									"00A009 - Microcrack",
									"00A010 - Cell Short-Circuit",
									"00A011 - Broken Cell",
									"00A012 - Black Spot",
									"00A013 - Others",
									"00B001 - Cell-to-Cell Distance",
									"00B002 - Gap oversize",
									"00B003 - Cell Misalignment",
									"00B004 - Twisted Ribbon",
									"00B005 - Ribbon Misalignment",
									"00B006 - Cell-to-Edge (End of inner Frame or Glass)",
									"00B008 - Gap between Insulation Strip and edge of Cell",
									"00B009 - Busbar Showing",
									"00B010 - Unsoldered Ribbon",
									"00B011 - Gap undersize",
									"00B012 - Cell touching",
									"00B013 - Uncut Ribbons",
									"00C002 - Glass Bubbles",
									"00C003 - Glass scratch",
									"00C004 - Glass coating scratch",
									"00C006 - Broken Glass",
									"00C007 - Glass Edge chips",
									"00D001 - Flux",
									"00D002 - Organic",
									"00D003 - Inorganic connecting Cell to Cell or Busbar",
									"00D004 - Inorganic not connecting Cell to Cell or Busbar",
									"00D005 - Incoming Cell Contamination",
									"00D006 - Incoming Busbar Contamination",
									"00D007 - Ribbon Contamination",
									"00D008 - Hair",
									"00D009 - Fiber",
									"00D010 - Dust",
									"00D011 - Paper",
									"00D012 - Bugs",
									"00D013 - Solder splatter",
									"00D014 - Ribbon cutouts",
									"00D015 - Contam-Cell chips",
									"00D016 - Tape",
									"00D017 - Contam-glass chips",
									"00D018 - Screw",
									"00D019 - Extra S/N label",
									"00D020 - bussing template",
									"00E001 - Bubbles on Cells",
									"00E002 - Bubbles on strip",
									"00E003 - Bubbles around barcode",
									"00E004 - Bubbles under frame",
									"00E005 - Bubbles between cells",
									"00E006 - Bubbles between busbar-cell",
									"00E007 - Other bubbles",
									"00E008 - Bubbles on foreign object",
									"00F001 - Skewed Jbox",
									"00F002 - Damaged Diodes",
									"00F003 - Incorrect position of diodes",
									"00F004 - Lack of Silicone",
									"00F005 - Excessive Silicone",
									"00F006 - Broken connector",
									"00F007 - Wrong Jbox",
									"00F008 - Jbox gasket",
									"00F009 - Jbox Lid tabs",
									"00F010 - Loose busbar slot",
									"00F011 - Tight busbar slot",
									"00G001 - Dark Lines",
									"00G002 - Scuff marks or Dents",
									"00G003 - Scratches",
									"00G004 - Bumps",
									"00G005 - Uneven Color",
									"00G006 - Unflushed Screws",
									"00G007 - Unflushed Frame Joints",
									"00G008 - Wrong number of holes",
									"00G009 - Defective Frame Countersink",
									"00G010 - Undersized frame",
									"00G011 - Oversized frame",
									"OOG012 - Missing Holes",
									"00G013 - Warped Frame",
									"00G014 - Wrong frame",
									"00G015 - Wrong Hole position",
									"00H002 - Excessive silicone",
									"00H003 - Insufficient silicone",
									"00H004 - Silicone voids",
									"00H006 - Wrong silicone type",
									"00I001 - Scratches",
									"00I002 - Punctures",
									"00I003 - Protrusion",
									"00I004 - Bubbles",
									"00I005 - Dents",
									"00I006 - Discolored backsheet",
									"00I007 - Tape on Backsheet",
									"00I008 - Wrong orientation",
									"00I009 - Wrong Backsheet",
									"00I010 - Double slit",
									"00I011 - Partially punched slit",
									"00I012 - Oversized slit",
									"00J001 - Missing Serial Number",
									"00J002 - Incorrect Label position",
									"00J003 - Error in Label content",
									"00J004 - Wrong end cap label",
									"00J005 - Misprint",
									"00J006 - Duplicate",
									"00J007 - Wrong module label",
									"00J008 - Wrong power label",
									"00K001 - manual handling",
									"00K002 - machine damage",
									"00K003 - transportation damage",
									"00K004 - underbaked",
									"00K005 - overbaked",
									"00K006 - power outage",
									"00K007 - process interruption",
									"00K008 - glass upside-down",
									"00K009 - unqualified BOM",
									"00K010 - electrical safety issue",
									"00L001 - manual handling",
									"00L002 - machine damage",
									"00L003 - transportation damage",
									"00L004 - underbaked",
									"00L005 - overbaked",
									"00L006 - power outage",
									"00L007 - process interruption",
									"00L008 - glass upside-down",
									"00L009 - unqualified BOM",
									"00L010 - low power - internal",
									"00L011 - electrical safety issue",
									"00M001 - Reliability",
									"00M002 - Other Destructive test",
									"00M003 - Certification"});
			this.comboBox2.Location = new System.Drawing.Point(140, 117);
			this.comboBox2.Name = "comboBox2";
			this.comboBox2.Size = new System.Drawing.Size(321, 21);
			this.comboBox2.TabIndex = 4;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(140, 169);
			this.textBox2.MaxLength = 40;
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(321, 20);
			this.textBox2.TabIndex = 5;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(17, 21);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(45, 23);
			this.label1.TabIndex = 6;
			this.label1.Text = "SN";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(17, 73);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(45, 23);
			this.label2.TabIndex = 7;
			this.label2.Text = "Class";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(17, 120);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(75, 23);
			this.label3.TabIndex = 8;
			this.label3.Text = "Defect Code";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(17, 172);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(75, 23);
			this.label4.TabIndex = 9;
			this.label4.Text = "Remark";
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(545, 24);
			this.textBox3.Multiline = true;
			this.textBox3.Name = "textBox3";
			this.textBox3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBox3.Size = new System.Drawing.Size(199, 266);
			this.textBox3.TabIndex = 10;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(479, 27);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(54, 17);
			this.label5.TabIndex = 11;
			this.label5.Text = "SN Batch";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(592, 327);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(89, 35);
			this.button3.TabIndex = 12;
			this.button3.Text = "Batch Grade";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.Button3Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.button3);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.textBox3);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.textBox2);
			this.panel1.Controls.Add(this.comboBox2);
			this.panel1.Controls.Add(this.button2);
			this.panel1.Controls.Add(this.comboBox1);
			this.panel1.Controls.Add(this.button1);
			this.panel1.Controls.Add(this.textBox1);
			this.panel1.Location = new System.Drawing.Point(12, 12);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(796, 444);
			this.panel1.TabIndex = 13;
			// 
			// FormQualityInspection
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveBorder;
			this.ClientSize = new System.Drawing.Size(805, 538);
			this.Controls.Add(this.panel1);
			this.Name = "FormQualityInspection";
			this.Text = "FormQualityInspection";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.ComboBox comboBox2;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBox1;
	}
}
