﻿/*
 * Created by SharpDevelop.
 * User: shen.yu
 * Date: 18/07/2011
 * Time: 11:34 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections;
using System.Collections.Generic;

namespace LablePLibrary
{
    /// <summary>
    /// Description of Class2.
    /// </summary>
    public class LargeLabel
    {
        public LargeLabel()
        {

        }
        
        private string _cartonNumber = "";
        /// <summary>
        /// 客户托号
        /// </summary>
        public string CartonNumber
        {
            get { return _cartonNumber; }
            set { _cartonNumber = value; }
        }

        private ArrayList _arraylist = new ArrayList();
        /// <summary>
        /// 存储组件序列号
        /// </summary>
        public ArrayList Arraylist
        {
            get { return _arraylist; }
            set { _arraylist = value; }
        }

        private string _packingDate = "";
        /// <summary>
        /// 包装时间
        /// </summary>
        public string PackingDate
        {
            get { return _packingDate; }
            set { _packingDate = value; }
        }

        private string _model = "";
        /// <summary>
        /// 组件版型
        /// </summary>
        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }

        private string _size = "";
        /// <summary>
        /// 电池片大小
        /// </summary>
        public string Size
        {
            get { return _size; }
            set { _size = value; }
        }

        private string _cellType = "";
        /// <summary>
        /// 电池片晶体类型
        /// </summary>
        public string CellType
        {
            get { return _cellType; }
            set { _cellType = value; }
        }

        private string _colour = "";
        /// <summary>
        /// 电池片颜色
        /// </summary>
        public string Colour
        {
            get { return _colour; }
            set { _colour = value; }
        }

        private string _power = "";
        /// <summary>
        /// 组件标称功率
        /// </summary>
        public string Power
        {
            get { return _power; }
            set { _power = value; }
        }

        private string _cartonclass = "";
        /// <summary>
        /// 组件等级
        /// </summary>
        public string Cartonclass
        {
            get { return _cartonclass; }
            set { _cartonclass = value; }
        }

        private string _pattern = "";
        /// <summary>
        /// 组件闸线类型
        /// </summary>
        public string Pattern
        {
            get { return _pattern; }
            set { _pattern = value; }
        }

        private bool _IsHavePattern = false;
        /// <summary>
        /// 是否填充组件闸线类型
        /// </summary>
        public bool IsHavePattern
        {
            get { return _IsHavePattern; }
            set { _IsHavePattern = value; }
        }
        private string _powerGrade = "";
        /// <summary>
        /// 功率档次
        /// </summary>
        public string PowerGrade
        {
            get { return _powerGrade; }
            set { _powerGrade = value; }
        }

        private string _IsShowPowerGrade = "";
        /// <summary>
        /// 是否显示功率档次
        /// </summary>
        public string IsShowPowerGrade
        {
            get { return _IsShowPowerGrade; }
            set { _IsShowPowerGrade = value; }
        }

        private string _Isc = "";
        /// <summary>
        /// 工作电流
        /// </summary>
        public string Isc
        {
            get { return _Isc; }
            set { _Isc = value; }
        }

        private string _Voc = "";
        /// <summary>
        /// 工作电压
        /// </summary>
        public string Voc
        {
            get { return _Voc; }
            set { _Voc = value; }
        }

        private string _Imp = "";
        /// <summary>
        /// 短路电流
        /// </summary>
        public string Imp
        {
            get { return _Imp; }
            set { _Imp = value; }
        }

        private string _Vmp = "";
        /// <summary>
        /// 开路电压
        /// </summary>
        public string Vmp
        {
            get { return _Vmp; }
            set { _Vmp = value; }
        }

        private string _Pmax = "";
        /// <summary>
        /// 实测功率
        /// </summary>
        public string Pmax
        {
            get { return _Pmax; }
            set { _Pmax = value; }
        }
        
		private string _Cable = "";
        public string Cable
        {
            get { return _Cable; }
            set { _Cable = value; }
        }
        
        private string _Market = "";
        public string Market
        {
            get { return _Market; }
            set { _Market = value; }
        }
        
        private string _BusBar = "";
        public string BusBar
        {
            get { return _BusBar; }
            set { _BusBar = value; }
        }
         private string _Volumn = "";
        public string Volumn
        {
            get { return _Volumn; }
            set { _Volumn = value; }
        }
         private int _Quantity = 0;
        public int Quantity
        {
        	get{ return _Quantity; }
        	set{ _Quantity = value; }
        }
         
        private string _NetWeight = "";
        public string NetWeight
        {
        	get{ return _NetWeight; }
        	set{ _NetWeight = value; }
        }
        
        private string _GrossWeight = "";
        public string GrossWeight
        {
        	get{ return _GrossWeight; }
        	set{ _GrossWeight = value; }
        }
        private string _CellVendor = "";
       	
        public string CellVendor
        {
        	get{ return _CellVendor; }
        	set{ _CellVendor = value; }
        }
        private string _LID = "";
        public string LID
        {
        	get{ return _LID; }
        	set{ _LID = value; }
        }
        private string _Connector = "";
        public string Connector
        {
        	get{ return _Connector; }
        	set{ _Connector = value; }
        }
         
        private string _Class = "A";
        public string Class
        {
        	get{ return _Class; }
        	set{ _Class = value; }
        }
  		private string _Voltage = "1000V";
        public string Voltage
        {
        	get{ return _Voltage; }
        	set{ _Voltage = value; }
        } 
		private string _FrameColor = "";
        public string FrameColor
        {
        	get{ return _FrameColor; }
        	set{ _FrameColor = value; }
        }           
         
        private List<List<string>> _listInfo = null;
        /// <summary>
        /// 储存组件的测试信息
        /// </summary>
        public List<List<string>> ListInfo
        {
            get { return _listInfo; }
            set { _listInfo = value; }
        }

    }
}
