﻿using System;
using System.Collections.Generic;
using System.Text;
using LabelManager2;
using System.Collections;
using System.Diagnostics;

namespace LablePLibrary
{
    public class LablePrint
    {
        public static string printingLable(string labFilePath, string Var0, string Value0, int pnum = 2, string Var1 = "", string Value1 = "", string printerName = "")
        {
            LabelManager2.ApplicationClass lbl = new LabelManager2.ApplicationClass();
            int VarCnt = 0,i;

            //////LabelManager2.ApplicationClass lbl = new LabelManager2.ApplicationClass();
            try
            {
                if (lbl.Documents.Count != 0)
                {
                    lbl.Documents.CloseAll();
                }
                //////lbl.Documents.Open(labFilePath, false);// 调用设计好的label文件
                lbl.Documents.Open(labFilePath);// 调用设计好的label文件

                LabelManager2.Document doc = lbl.ActiveDocument;
                if (printerName != "") doc.Printer.SwitchTo(printerName);// ShowSetup();// DriverName = textBox4.Text;

                //--------2011-05-18|add by alex.dong|for multiple Vars Print
               	VarCnt = lbl.ActiveDocument.Variables.Count;
                for (i = 1; i <= VarCnt; i++)
                {
                    if (lbl.ActiveDocument.Variables.Item(i).Name == Var0)
                        lbl.ActiveDocument.Variables.Item(i).Value = Value0;
                    if (lbl.ActiveDocument.Variables.Item(i).Name == Var1)
                        if (Var1 != "")
                            lbl.ActiveDocument.Variables.Item(i).Value = Value1;
                }
                //---------------end add

                //if (doc.Variables.FormVariables.Count > 1)
                //{
                //    doc.Variables.FreeVariables.Item(Var0).Value = Value0; //给参数传值
                //    //doc.Variables.FormVariables.Item(Var0).Value = Value0; //给参数传值
                //    if (Var1 != "")
                //        doc.Variables.FreeVariables.Item(Var1).Value = Value1; //给参数传值   
                //}
                //else if (doc.Variables.FormVariables.Count == 1)
                //{
                //    doc.Variables.FormVariables.Item(Var0).Value = Value0;
                //}
                ////////doc.Variables.FreeVariables.Item(Var0).Value = Value0; //给参数传值
                //////////doc.Variables.FormVariables.Item(Var0).Value = Value0; //给参数传值
                ////////if (Var1 != "")
                ////////    doc.Variables.FreeVariables.Item(Var1).Value = Value1; //给参数传值     
                //打印
                doc.PrintLabel(1, 1, 1, pnum); //打印数量
                doc.PrintDocument(0);

                lbl.Documents.CloseAll();
                lbl.Quit();

                //----------------------------2011-007-14|add by alex.dong|kill lppa.exe after use
                Process[] killbyname = Process.GetProcessesByName("lppa");
                for (int j = 0; j < killbyname.Length; j++)
                {
                    killbyname[j].Kill();
                }
                //----------------------------end add
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            
            return "";

        }
        public static string printingLable(string labFilePath, string Var0, string[,] Values, int pnum = 2, string Var1 = "", string printerName = "")
        {
            LabelManager2.ApplicationClass lbl = new ApplicationClass();
            try
            {
                lbl.Documents.Open(labFilePath, false);// 调用设计好的label文件
                Document doc = lbl.ActiveDocument;
                if (printerName != "") doc.Printer.SwitchTo(printerName);// ShowSetup();// DriverName = textBox4.Text;
                //打印
                int VarCnt = lbl.ActiveDocument.Variables.Count;
                int j = 0;
                for (int i = 0; i < Values.GetLength(0);i++ )
                {
                    //doc.Variables.FormVariables.Item(Var0).Value = Values[i, 0]; //给参数传值
                    ////lbl.ActiveDocument.Variables.Item(Var0).Value = Values[i, 0];
                    ////if (Var1 != "") lbl.ActiveDocument.Variables.Item(Var1).Value = Values[i, 1]; //给参数传值     
                    for (j = 1; j <= VarCnt; j++)
                    {
                        if (lbl.ActiveDocument.Variables.Item(j).Name == Var0)
                            lbl.ActiveDocument.Variables.Item(j).Value = Values[i, 0];
                        if (Var1 != "")
                        {
                            if (lbl.ActiveDocument.Variables.Item(j).Name == Var1)
                                lbl.ActiveDocument.Variables.Item(j).Value = Values[i, 1];
                        }
                    }
                    doc.PrintLabel(1, 1, 1, pnum); //打印数量
                    doc.PrintDocument(0);
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                lbl.Quit();                                         //退出
            }
            return "";
        }

        public static string printingLable(string labFilePath, ArrayList lst, bool isSch, int pnum = 2, string printerName = "")
        {
            string[] sDataSet;
            LabelManager2.ApplicationClass lbl = new ApplicationClass();
            try
            {
                lbl.Documents.Open(labFilePath, false);// 调用设计好的label文件
                Document doc = lbl.ActiveDocument;
                if (printerName != "") doc.Printer.SwitchTo(printerName);// ShowSetup();// DriverName = textBox4.Text;
                //打印
                int VarCnt = lbl.ActiveDocument.Variables.Count;
                int j = 0;
                for (int i = 0; i < lst.Count; i++)
                {
                    sDataSet = lst[i].ToString().Split(':');
                    if (isSch)
                    {
                        for (j = 1; j <= VarCnt; j++)
                        {
                            if (lbl.ActiveDocument.Variables.Item(j).Name == "NO")
                                lbl.ActiveDocument.Variables.Item(j).Value = sDataSet[0];
                            if (lbl.ActiveDocument.Variables.Item(j).Name == "Pmpp")
                                lbl.ActiveDocument.Variables.Item(j).Value = sDataSet[1];
                            if (lbl.ActiveDocument.Variables.Item(j).Name == "Voc")
                                lbl.ActiveDocument.Variables.Item(j).Value = sDataSet[2];
                            if (lbl.ActiveDocument.Variables.Item(j).Name == "Isc")
                                lbl.ActiveDocument.Variables.Item(j).Value = sDataSet[3];
                            if (lbl.ActiveDocument.Variables.Item(j).Name == "Vmpp")
                                lbl.ActiveDocument.Variables.Item(j).Value = sDataSet[4];
                            if (lbl.ActiveDocument.Variables.Item(j).Name == "Impp")
                                lbl.ActiveDocument.Variables.Item(j).Value = sDataSet[5];
                        }
                    }
                    else
                    {
                        for (j = 1; j <= VarCnt; j++)
                        {
                            if (lbl.ActiveDocument.Variables.Item(j).Name == "NO")
                                lbl.ActiveDocument.Variables.Item(j).Value = sDataSet[0];
                            if (lbl.ActiveDocument.Variables.Item(j).Name == "Model")
                                lbl.ActiveDocument.Variables.Item(j).Value = sDataSet[1];
                            if (lbl.ActiveDocument.Variables.Item(j).Name == "Size")
                                lbl.ActiveDocument.Variables.Item(j).Value = sDataSet[2];
                            if (lbl.ActiveDocument.Variables.Item(j).Name == "MP")
                                lbl.ActiveDocument.Variables.Item(j).Value = sDataSet[3];
                            if (lbl.ActiveDocument.Variables.Item(j).Name == "Color")
                                lbl.ActiveDocument.Variables.Item(j).Value = sDataSet[4];
                        }
                    }
                    doc.PrintLabel(1, 1, 1, pnum); //打印数量
                    doc.PrintDocument(0);
                }
                lbl.Quit();  //退出
                //----------------------------2011-007-14|add by alex.dong|kill lppa.exe after use
                Process[] killbyname = Process.GetProcessesByName("lppa");
                for (int i = 0; i < killbyname.Length; i++)
                {
                    killbyname[i].Kill();
                }
                //----------------------------end add
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "";
        }
        
        public static string LargeLabelPrint(string labFilePath, LargeLabel largelabel, int pnum)
        {	
        	LabelManager2.ApplicationClass lbl = new ApplicationClass();
        	try
        	{
				lbl.Documents.Open(labFilePath, false);// 调用设计好的label文件
				Document doc = lbl.ActiveDocument;
				int VarCnt = lbl.ActiveDocument.Variables.Count;

				for (int i = 1; i <= VarCnt; i++)
				{
                    for (int j = 1; j <= largelabel.arraylist.Count; j++)
					{
                    	if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "s" + (largelabel.arraylist.Count + 1 - j).ToString())
						{
							lbl.ActiveDocument.Variables.Item(i).Value = largelabel.arraylist[j-1].ToString();
                            break;//add by alex.dong 2011-07-25
						}
					}
					if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "cartonnumber")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = largelabel.cartonNumber.ToString();
					}
                    if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "packingdate")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = largelabel.packingDate.ToString();
					}
                    if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "model")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = largelabel.model.ToString();
					}
                    if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "celltype")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = largelabel.cellType.ToString();
					}
                    if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "power")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = largelabel.power.ToString();
					}
                    if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "colour")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = largelabel.colour.ToString();
					}
                    if (lbl.ActiveDocument.Variables.Item(i).Name.ToLower() == "class")
					{
						lbl.ActiveDocument.Variables.Item(i).Value = largelabel.cartonclass.ToString();
					}
			    
				}
				doc.PrintLabel(1, 1, 1, pnum); //打印数量
				doc.PrintDocument(0);
				lbl.Quit();  //退出
				Process[] killbyname = Process.GetProcessesByName("lppa");
				for (int i = 0; i < killbyname.Length; i++)
				{
					killbyname[i].Kill();
				}
        	}
       		catch (Exception ex)
            {
                return ex.Message;
            }
       		return "";
        }

    }
}
